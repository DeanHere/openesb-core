#
# BEGIN_HEADER - DO NOT EDIT
# 
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)create_instances_in_domain.cg - ver 1.1 - 10/18/2006
#
# Copyright 2004-2006 Sun Microsystems, Inc. All Rights Reserved.
# 
# END_HEADER - DO NOT EDIT
#

#create standard domains for jbi regression tests:

%ifndef FIXTREE_DEFS_INCLUDED %exit ${CG_INFILE}: you must include fixtree.defs to run this script.

#html adaptor port is enabled in set_instance_offset (fixtree.defs).
%undef HTML_ADAPTOR_PORT

%ifndef INSTANCES_LIST %echo Usage:  ${CG_INFILE}: instance1...
%ifndef INSTANCES_LIST %halt 1

create_an_instance := << EOF
#create a single instance
{
    #note - set_instance_offset is defined in fixtree.defs.
    %call set_instance_offset

    #note - call check_domain_names first to prevent this error:
    #%if $BAD_INSTANCE_NAMES %eecho create_an_instance: bad domain name, $instance_name - ABORT
    #%if $BAD_INSTANCE_NAMES %halt 1

    CG_COMPARE_SPEC = CAS-cluster1-inst1
    %if $instance_name:eq CLUSTERED_INSTANCE = 1

    %include create_an_instance.cg

    #next instance:
    %shift instance_name STACK_TMP
}
EOF

delete_an_instance := << EOF
#delete a single instance
{

    #note - call check_domain_names first to prevent this error:
    #%if $BAD_INSTANCE_NAMES %eecho create_an_instance: bad domain name, $instance_name - ABORT
    #%if $BAD_INSTANCE_NAMES %halt 1

    %include delete_an_instance.cg

    #next instance:
    %shift instance_name STACK_TMP
}
EOF


####
#instances are created only in the domain CAS
####
CG_COMPARE_SPEC = CAS
%ifnot $domain_name:eq	%return

%echo
%echo Creating instances in domain CAS

#####
#delete the instances associated with the node-agent. this is needed for deleting the node-agent
#####

STACK_TMP = $INSTANCES_LIST
%shift instance_name STACK_TMP
%whiledef instance_name %call delete_an_instance

####
#delete the node-agent config
####

%ifnot $IS_CLEAN_INSTALL  %echo DELETING node-agent-config agent1...
%ifnot $IS_CLEAN_INSTALL  %shell asadmin delete-node-agent-config --user $admin_name --passwordfile $admin_passwdfile --port $admin_port agent1

####
#delete the node-agent itself
####
%ifnot $IS_CLEAN_INSTALL  %echo DELETING node-agent agent1...
%ifnot $IS_CLEAN_INSTALL  %shell asadmin delete-node-agent -t agent1

####
#create the node-agent
####
%echo
%echo CREATING node-agent agent1 ...
%shell asadmin create-node-agent -e --port $admin_port --user $admin_name --passwordfile $admin_passwdfile agent1 
%if $CG_SHELL_STATUS  %echo create_instances_in_CAS.cg: ERROR: asadmin create-node-agent FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1


####
#create the cluster
####
%echo
%echo CREATING cluster cluster1 ...
%shell asadmin create-cluster -e --port $admin_port --user $admin_name --passwordfile $admin_passwdfile CAS-cluster1 
%if $CG_SHELL_STATUS  %echo create_instances_in_CAS.cg: ERROR: asadmin create-cluser FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1

#create the instances:
STACK_TMP = $INSTANCES_LIST
%shift instance_name STACK_TMP
%whiledef instance_name %call create_an_instance

%echo
%echo STOPPING $domain_name
%shell asadmin stop-domain $domain_name
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadminstop-domain FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1
%shell sleep 5


#! /bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)buildenv.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

###################
# INPUT PARAMETERS:
###################

# IIS_TOOLROOT    - override the default TOOLROOT setting.
# IIS_CVSROOT     - override the default CVSROOT setting.
# IIS_BRANCH_NAME - override the default CVS_BRANCH_NAME setting
# IIS_CODELINE    - override the default CODELINE setting
# IIS_AS8BASE     - override the default AS8BASE setting
#
# WARNING:  the above variables are PRIVATE and are RESERVED for
#           this definition file.  They are unexported after use.

export PRODUCT="openesb"

#these variables are needed to update the local project tool sources during toolsBuild
export CVS_SRCROOT_PREFIX="open-esb"
export CVS_CO_ROOT="${SRCROOT}/.."

#finally, set the current release tag:
export REV="SM05"

### set up source "base" variable:
if [ -n "${SRCROOT}" ]
then
    #cannonicalize $SRCROOT if it is set:
    # cd ${SRCROOT}
    export SRCROOT=$( sh -c "cd ${SRCROOT}; pwd" )
    # cd -
fi

if [ "${SRCROOT}" != "${PWD}" ]
then
    if [ -n "${PROJECT}" ] && ( [ "${SRCROOT}" = "${PROJECT}" ] || [ -z "${SRCROOT}" ] )
    then
        ## ASSUME we are using VSPMS
        export SRCROOT="${PROJECT}"
    else
        echo "SRCROOT is ${SRCROOT}"
        echo "PROJECT is ${PROJECT}"
        echo "PLEASE SET \$SRCROOT BEFORE SOURCING buildenv.csh"
        echo "SETUP ABORTED"
        return
    fi
fi


######
#allow user to override codeline var, which determines cvs branch names
#for main repository, and determines the placement of log and kit directories.
######
export CODELINE="${IIS_CODELINE:-"main"}"

#have to unset or set it to zero this if you are building in a release environment
export DEVELOPER_BUILD="1"

####
#CVS BRANCH NAMES.  Use IIS_BRANCH_NAME to override "main" when bootstraping a branch.
####
export JBI_BRANCH_NAME="${IIS_BRANCH_NAME:-"main"}"
export CVS_BRANCH_NAME="${JBI_BRANCH_NAME}"

#################
# CVS_BRANCH_NAME is used in the following scripts to denote the
# toolsBuild    - checkout tools src
# makedrv.pl    - checkout tools src
# buildenv.csh  - setup file
# buildenv.ksh  - setup file
# bldcmn.sh     - assert
# fortepj.rc    - cosmetic (sets $REV VSPMS var)
# fortepj.ksh   - cosmetic (sets $REV VSPMS var)
#################

#### CVS DEFS
export CVSREAD="1"

####### TOOLS SETUP
if [ -n "${IIS_TOOLROOT}" ]
then
    export TOOLROOT="${IIS_TOOLROOT}"
fi

if [ -x ${TOOLROOT}/boot/whatport ]
then
    export FORTE_PORT=$( ${TOOLROOT}/boot/whatport )
else
    echo "ERROR:  could not find tools - please check your IIS_TOOLROOT setting or create a copy in $SRCROOT/tools."
    exit -1
fi

export PATH="${TOOLROOT}/bin/${FORTE_PORT}:${TOOLROOT}/bin/cmn:${PATH}"

if [ -n "${PERL5_HOME}" ]
then
    if [ -d "${PERL5_HOME}" ]
    then
        #perl installations on solaris and linux differ - solaris has {bin,lib} subdirs:
        if [ -d "${PERL5_HOME}/lib" ]
        then
            export PERL_LIBPATH=".;$PERL5_HOME/lib;$TOOLROOT/lib/cmn;$TOOLROOT/lib/cmn/perl5"
        else
            export PERL_LIBPATH=".;$PERL5_HOME;$TOOLROOT/lib/cmn;$TOOLROOT/lib/cmn/perl5"
        fi
        if [ -d "${PERL5_HOME}/bin" ]
        then
            set PATH="${PERL5_HOME}/bin:${PATH}"
        else
            #otherwise, we assume perl is already in the path
            true  # no-op
        fi
    else
        echo "WARNING: not a directory, PERL5_HOME='$PERL5_HOME'. Please fix."
    fi
else
    #use port-specific perl libs in $TOOLROOT; this is for old solaris
    #and mks perl installs:
    export PERL_LIBPATH=".;${TOOLROOT}/lib/cmn;${TOOLROOT}/lib/${FORTE_PORT}/perl5;${TOOLROOT}/lib/cmn/perl5"
fi

#used by makemf utility:
export MAKEMF_LIB="${TOOLROOT}/lib/cmn"

#used by codegen utility:
export CG_TEMPLATE_PATH=".;${TOOLROOT}/lib/cmn/templates;${TOOLROOT}/lib/cmn/templates/java"

####### END TOOLS SETUP

#### set up env required for release and tools builds:
export PATHNAME=$( basename ${SRCROOT} )
export PATHREF="${SRCROOT}"
export RELEASE_ROOT="${SRCROOT}/release"
export RELEASE_DISTROOT="${SRCROOT}/release"
export HOST_NAME=$( uname -n )
export FORTE_LINKROOT="${FORTE_LINKROOT:-"${SRCROOT}/${CODELINE}/${FORTE_PORT}"}"
export DISTROOT="${DISTROOT:-"${FORTE_LINKROOT}/dist/tools"}"
export KITROOT="${KITROOT:-"${FORTE_LINKROOT}/kits"}"
export KIT_DISTROOT="${KIT_DISTROOT:-"${FORTE_LINKROOT}/kits/${PRODUCT}"}"
export KIT_REV="${KIT_REV:-"${CODELINE}"}"
export REGRESS_DISPLAY="${REGRESS_DISPLAY:-"${DISPLAY}"}"

##### JAVA SDK SETUP
export PATH="${JAVA_HOME}/bin:${PATH}"

##### JAVA TOOLS
#ant
export ANT_OPTS="${ANT_OPTS:-"-Xmx200m"}"

##### JREGRESS
export JREGRESS_TIMEOUT="${JREGRESS_TIMEOUT:-"650"}"

##### Update path for glassfish bin directory
export PATH="${AS8BASE}/bin:${PATH}"

##### Default JBIROOT:
#this can be set in the login env, as it is usually invariant
export JBI_USER_NAME="${JBI_USER_NAME:-"${USER}"}"
if [ -n "${JNET_USER}" ]
then
    export JNET_USER="${JBI_USER_NAME}"
    echo "WARNING: your java.net user name (JNET_USER) was defaulted to '${JNET_USER}'."
    echo "To remove this warning, please export JNET_USER in the environment."
fi

export JBIROOT_BASE="${JBIROOT_BASE:-"${AS8BASE}"}"
export JBIROOT="${JBIROOT:-"${JBIROOT_BASE}/jbi"}"

### useful aliases:
alias mkregresslink='ln -s ${FORTE_LINKROOT}/regress ${SRCROOT}/regress'
alias gettools='mkdir -p tools.new; cd ${SRCROOT}/tools.new;  cvs -f -d ${TOOLS_CVSROOT} co ${FORTE_PORT}tools; echo new tools are in tools.new'
alias cvstools='cvs -d ${TOOLS_CVSROOT}'

### checkstyle helpers:
#note - the \| makes '|' the delimiter in the address range.
#it is necessary because the filename expression could contain (/) delimiters.
alias cserrs='sed -n -e "\|^<file .*\!$|,\|<\/file>|p" $SRCROOT/\!^/bld/checkstyle_src_report.xml | grep -v "</file>" | sed -e "s|$SRCROOT/||g; s|<file name=.||; s/.>//" | fixcserrs | xml2ascii | nodoublequotes '
alias cserrsu='echo Usage: cserr service java_classname'
alias fixcserrs='sed -e "s|<error line=.| |; s|. column=.|/|; s|. severity=.error. message=.|	|; s|source=.*||" '
alias nodoublequotes='tr -d \\042'
#this is obviously incomplete.  RT 10/6/04
alias xml2ascii="sed -e \" s|&apos;|\'|g; s|&lt;|<|g;; s|&gt;|>|g;\""

###
#this is a hack to bootstrap the boms.  RT 9/26/05
export JBI_DOT_VERSION="1.1"

#######
#cygwin: set java versions of SRCROOT, TOOLROOT (used in ant scripts):
#######
if [ "$FORTE_PORT" != "cygwin" ]
then
    export JV_SRCROOT="${SRCROOT}"
    export JV_TOOLROOT="${TOOLROOT}"
    export JV_AS8BASE="${AS8BASE}"
else
    #warning - run cygpath on each, to convert paths of the /cygdrive form.  RT 9/13/06
    export JV_SRCROOT=`cygpath -wm "$SRCROOT"`
    export JV_TOOLROOT=`cygpath -wm "$TOOLROOT"`
    export JV_AS8BASE=`cygpath -wm "$AS8BASE"`
fi

#these variables are PRIVATE and RESERVED for this definition file:
unset IIS_TOOLROOT
unset IIS_CVSROOT
unset IIS_BRANCH_NAME
unset IIS_CODELINE
unset IIS_AS8BASE

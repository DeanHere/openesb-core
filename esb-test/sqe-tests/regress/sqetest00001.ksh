#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)sqetest00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo testname is sqetest00001.ksh

. ./regress_defs.ksh
my_test_domain=JBITest
. $SRCROOT/antbld/regress/common_defs.ksh

export S1AS_HOME
S1AS_HOME=$JV_AS8BASE
export APS_HOME
APS_HOME=$JV_SRCROOT/esb-test/sqe-tests/bld/appserv-tests
rm -rf $APS_HOME
mkdir -p $APS_HOME
cp -rf $JV_SRCROOT/esb-test/sqe-tests/regress/testdata/appserv-tests/* $APS_HOME/

rm -f $JV_SVC_BLD/test_result.log 1>&2
rm -f $APS_HOME/config.properties 1>&2

cp $APS_HOME/config.properties.template $APS_HOME/config.properties
chmod a+w $APS_HOME/config.properties
echo "s/admin.domain=.*/admin.domain=$JBI_DOMAIN_NAME/g" > $JV_SVC_BLD/command.sed
sed -f $JV_SVC_BLD/command.sed $APS_HOME/config.properties > $JV_SVC_BLD/command.sed.tmp
cp $JV_SVC_BLD/command.sed.tmp $APS_HOME/config.properties
rm $JV_SVC_BLD/command.sed 1>&2
rm $JV_SVC_BLD/command.sed.tmp 1>&2

echo "s/admin.port=.*/admin.port=$ASADMIN_PORT/g" > $JV_SVC_BLD/command.sed
sed -f $JV_SVC_BLD/command.sed $APS_HOME/config.properties > $JV_SVC_BLD/command.sed.tmp
cp $JV_SVC_BLD/command.sed.tmp $APS_HOME/config.properties
rm $JV_SVC_BLD/command.sed 1>&2
rm $JV_SVC_BLD/command.sed.tmp 1>&2

echo "s/admin.user=.*/admin.user=$AS_ADMIN_USER/g" > $JV_SVC_BLD/command.sed
sed -f $JV_SVC_BLD/command.sed $APS_HOME/config.properties > $JV_SVC_BLD/command.sed.tmp
cp $JV_SVC_BLD/command.sed.tmp $APS_HOME/config.properties
rm $JV_SVC_BLD/command.sed 1>&2
rm $JV_SVC_BLD/command.sed.tmp 1>&2

echo "s/http.port=.*/http.port=$DOMAIN_HTTP_PORT/g" > $JV_SVC_BLD/command.sed
sed -f $JV_SVC_BLD/command.sed $APS_HOME/config.properties > $JV_SVC_BLD/command.sed.tmp
cp $JV_SVC_BLD/command.sed.tmp $APS_HOME/config.properties
rm $JV_SVC_BLD/command.sed 1>&2
rm $JV_SVC_BLD/command.sed.tmp 1>&2

cd $APS_HOME/sqetests/jbi/helloca
$ANT_HOME/bin/ant > $JV_SVC_BLD/test_result.log
grep "Total DID NOT RUN" $JV_SVC_BLD/test_result.log 2>&1
grep "Total FAIL" $JV_SVC_BLD/test_result.log 2>&1

#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)security00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
. ./regress_defs.ksh

###############################################################################
#                                  Test Set Up                                #
###############################################################################

# Update the domains server.policy
ant -emacs -q -DJBI_DOMAIN_ROOT=${JV_JBI_DOMAIN_ROOT} -DSERVER_POLICY_APPEND=${SERVER_POLICY_APPEND} -DSERVER_POLICY_BAK=${SERVER_POLICY_BAK} -f ${SEC_REGRESS_DIR}/scripts/update-appserver.ant update_server_policy

# Update the domains login.conf
ant -emacs -q -DJBI_DOMAIN_ROOT=${JV_JBI_DOMAIN_ROOT} -DLOGIN_CONF_APPEND=${LOGIN_CONF_APPEND} -DLOGIN_CONF_BAK=${LOGIN_CONF_BAK} -DUSER_FILE=${USER_FILE} -f ${SEC_REGRESS_DIR}/scripts/update-login-conf.ant update_login_conf

# Change permission on the $SEC_BLD_REGRESS_DIR/keystores/client-truststore.jks file to be safe
chmod 777 $SEC_BLD_REGRESS_DIR/keystores/client-truststore.jks

# Get the AS Trusted Certificate and import into the clients trust store ( this is a trusted certificate now ).
if [ $IS_EE_INSTALL -eq 1 ]; then

    # Export the Server Certificate from the NSS Store
    certutil -L -n s1as -d $JV_JBI_DOMAIN_ROOT/config/ -a -o ${SEC_BLD_REGRESS_DIR}/s1as.cert 

else
    # Export the Server Certificate
    keytool -export -keystore $JV_JBI_DOMAIN_ROOT/config/cacerts.jks -storepass changeit -file ${SEC_BLD_REGRESS_DIR}/s1as.cert -alias s1as -noprompt
fi

    # Import the Server Certificate
    keytool -import -keystore $SEC_BLD_REGRESS_DIR/keystores/client-truststore.jks -storepass changeit -file ${SEC_BLD_REGRESS_DIR}/s1as.cert -alias s1as2 -noprompt

# Get the Client CA certificate and put it in the Application Server trust store
     # Export the Client CA Certificate
    keytool -export -keystore $SEC_BLD_REGRESS_DIR/keystores/client-truststore.jks -storepass changeit -file ${SEC_BLD_REGRESS_DIR}/cacert.cert -alias certificate-authority -noprompt

if [ $IS_EE_INSTALL -eq 1 ]; then

    # Import the Client CA Certificate in the NSS Store
    certutil -A -n certificate-authority -t "PTCu,PTCu,PTCu" -d $JV_JBI_DOMAIN_ROOT/config/ -i ${SEC_BLD_REGRESS_DIR}/cacert.cert  

else
    # Import the Client CA Certificate
    keytool -import -keystore  $JV_JBI_DOMAIN_ROOT/config/cacerts.jks -storepass changeit -file ${SEC_BLD_REGRESS_DIR}/cacert.cert -alias certificate-authority -noprompt

fi

# Start the Application Server domain
rm -f $JBI_DOMAIN_ROOT/logs/server.log

# Reset to a known state by removing previous installations and deployments:
rm -rf $JBI_DOMAIN_ROOT/jbi/engines
rm -rf $JBI_DOMAIN_ROOT/jbi/bindings
rm -rf $JBI_DOMAIN_ROOT/jbi/sharedlibraries
rm -rf $JBI_DOMAIN_ROOT/jbi/system
mkdir $JBI_DOMAIN_ROOT/jbi/engines
mkdir $JBI_DOMAIN_ROOT/jbi/bindings
mkdir $JBI_DOMAIN_ROOT/jbi/sharedlibraries
mkdir $JBI_DOMAIN_ROOT/jbi/system

# Remove persisted component meta-data:
rm -f $JBI_DOMAIN_ROOT/jbi/repository/ComponentData.xml

#Copy the Users.xml file to the config folder
cp ${SEC_REGRESS_DIR}/deployconfig/Users.xml ${JBI_DOMAIN_ROOT}/config/${USER_FILE}
chmod 777 ${JBI_DOMAIN_ROOT}/config/${USER_FILE}

# Start the domain.
cmd="asadmin start-domain -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --domaindir '$JV_JBI_DOMAIN_DIR' $JBI_DOMAIN_NAME"
1>&2 eval $cmd
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
    exit 1
fi

#wait for the admin started file to appear:
bldwait -nomsg -max 600 $JBI_DOMAIN_STARTED
if [ $? -ne 0 ]; then
    bldmsg -error -p $0 error starting JBI - admin service did not start
    exit 1
fi

# Copy the Config folder to the bld directory and replace tokens
ant -emacs -q -f ${SEC_REGRESS_DIR}/scripts/copy-config-data.ant copy_config_folder

# Create the Test Servlet War
ant -emacs -q -f ${SEC_REGRESS_DIR}/scripts/create-servlet.ant create_war

# Deploy the Test Servlet
echo Deploying the TestSecurityServlet.

asadmin deploy -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port ${ASADMIN_PORT} --contextroot security --force=true ${SEC_BLD_DIR}/${TEST_SVLT_WAR}

if [ $? -eq 0 ]; then
    echo Successfully deployed the TestSecurityServlet.
else
    echo Failed to deploy TestSecurityServlet
fi

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageSecPolicyImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  MessageSecPolicy.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on March 18, 2005, 12:05 PM
 */

package com.sun.jbi.internal.security.config;

import com.sun.enterprise.security.jauth.AuthPolicy;
import java.util.Set;

/**
 * Implementation of the Message Protection Policy.
 *
 * @author Sun Microsystems, Inc.
 */
public class MessageSecPolicyImpl
    implements MessageSecPolicy
{
    /** The Request Policy. */
    private AuthPolicy mReqPolicy;
    
    /** The Response Policy. */
    private AuthPolicy mRespPolicy;
    
    /** The Provider Name. */
    private String mProviderId;
    
    /** The Set of Operations to which this Policy applies. */
    private Set mOperationSet;
    
    /**
     * Ctor.
     * @param ops is the set of operations for the MessageSecPolicy
     * @param reqPolicy is the Request Auth Policy
     * @param respPolicy is the Response Auth Policy
     */
    public MessageSecPolicyImpl(Set ops, AuthPolicy reqPolicy, AuthPolicy respPolicy )
    {
        this(ops, reqPolicy, respPolicy, null);
    }
    
    /**
     * Ctor.
     *
     * @param ops is the set of operations for the MessageSecPolicy
     * @param reqPolicy is the Request Auth Policy
     * @param respPolicy is the Response Auth Policy
     * @param provider is the Provider Name
     */
    public MessageSecPolicyImpl(Set ops, AuthPolicy reqPolicy, AuthPolicy respPolicy, 
        String provider)
    {
        mProviderId = provider;
        mReqPolicy    = reqPolicy;
        mRespPolicy   = respPolicy;
        mOperationSet = ops;
    }
    
    /**
     * Get the RequestPolicy. The Request Policy is of the Type AuthPolicy.
     * This class is currently defined in the com.sun.enterprise.security.jauth 
     * package, this would be standardized in JSR 196.
     *
     * If the Endpoint is a Consumer (Server) Endpoint the Request Policy
     * applies to incoming requests, if its a Provider (Client) Endpoint 
     * the policy applies to the outgoing request.
     *
     * @return the request AuthPolicy
     */
    public AuthPolicy getRequestPolicy()
    {
        return mReqPolicy;
    }
    
    /**
     * Set the RequestPolicy.
     *
     * If the Endpoint is a Consumer (Server) Endpoint the Request Policy
     * applies to incoming requests, if its a Provider (Client) Endpoint 
     * the policy applies to the outgoing request.
     *
     * @param reqPolicy is the request AuthPolicy
     */
    public void setRequestPolicy(AuthPolicy reqPolicy)
    {
        mReqPolicy = reqPolicy;
    }
   
    
    /**
     * Get the ResponsePolicy. The Request Policy is of the Type AuthPolicy.
     * This class is currently defined in the com.sun.enterprise.security.jauth 
     * package, this would be standardized in JSR 196.
     *
     * If the Endpoint is a Consumer (Server) Endpoint the Response Policy
     * applies to outgoing requests, if its a Provider (Client) Endpoint 
     * the policy applies to the incoming response.
     *
     * @return the ResponsePolicy.
     */
    public AuthPolicy getResponsePolicy()
    {
        return mRespPolicy;
    }
    
    /**
     * Set the ResponsePolicy.
     *
     * If the Endpoint is a Consumer (Server) Endpoint the Response Policy
     * applies to outgoing requests, if its a Provider (Client) Endpoint 
     * the policy applies to the incoming response.
     *
     * @param respPolicy is the ResponsePolicy.
     */
    public void getResponsePolicy(AuthPolicy respPolicy)
    {
        mRespPolicy = respPolicy;
    }
    
    /**
     * Get the Id of the Provider. If a Provider is not specified in the deployment
     * configuration, an empty String is returned.
     *
     * @return the id of the Message Security Provider
     */
    public String getMessageProviderId()
    {
        return mProviderId;
    }
    
    /**
     * Set the Id of the Provider. 
     *
     * @param id is the name of the Message Security Provider
     */
    public void setMessageProviderId(String id)
    {
        mProviderId = id;
    }
    
    /**
     * Get the Set of Operations to which this MessageSecPolicy applies.
     *
     * @return the Set of Operations to which this MessageSecPolicy applies
     */
    public Set getOperations()
    {
        return mOperationSet;
    }   
}

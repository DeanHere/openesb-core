/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityService.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.binding.security.SecurityHandler;
import com.sun.jbi.component.ComponentContext;
import com.sun.jbi.internal.security.config.SecurityConfiguration;
import com.sun.jbi.internal.security.config.SecurityInstallConfig;
import com.sun.jbi.internal.security.mbeans.SecurityServiceConfigMBeanImpl;

import java.util.HashMap;
import java.util.Properties;
import java.util.Iterator;
import java.util.logging.Logger;
import javax.jbi.JBIException;

import com.sun.jbi.management.support.JbiNameInfo;

/**
 * Implementation of the SecurityService interface.
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityService
    extends com.sun.jbi.management.system.ModelSystemService
    implements com.sun.jbi.binding.security.SecurityService,
    com.sun.jbi.ServiceLifecycle   
{
  
    /**
     * The registry of ( ComponnetID: SecurityHandlers )
     */
    protected HashMap mSecurityHandlers;
    
    /**
     * The Environment Context.
     */
    protected static com.sun.jbi.EnvironmentContext sEnvCtx;
    
    /**
     * The Installation time security configuration. 
     */
    protected static SecurityConfiguration sSecInstCfg;
    
    /**
     * The String Translator.
     */
    protected static StringTranslator sTranslator;
    
    /**
     * The User Domain Registry.
     */
    protected static HashMap sUserDomains;    
    
    /**
     * The KeyStore Registry.
     */
    protected static HashMap sKeyStoreManagers;
    
    /**
     * private logger handle:
     */
    private Logger mLogger;

    /** our immutable name (TODO:  this should come from framework): */
    private final JbiNameInfo mJbiNameInfo = new JbiNameInfo("SecurityService");
    
    /** 
     * Ctor.
     */
    public SecurityService()
    {
        mSecurityHandlers = new HashMap();
        sUserDomains = new HashMap();
        sKeyStoreManagers = new HashMap();
        mLogger = Logger.getLogger (
            com.sun.jbi.internal.security.Constants.PACKAGE);
    }
    
    /**
     * Create a SecurityHandler based on the SecurityConfiguration.
     *
     * @param compCtx the ComponentContext of the Binding Component associated
     * with the Securityhandler
     * @return a instance of the newly created SecurityHandler
     * @throws IllegalStateException if the SecurityHandler is in a invalid
     * state on creation.
     */
    public SecurityHandler createSecurityHandler(ComponentContext compCtx)
        throws IllegalStateException
    {
    
        return this.createSecurityHandler (compCtx, "SOAP");
    }
    
    /**
     * Create a SecurityHandler based on the SecurityConfiguration.
     *
     * @param compCtx is the ComponentContext of the Binding Component associated
     * with the Securityhandler
     * @param authLayer is the type of layer that requires to use the security services.
     * @return a instance of the newly created SecurityHandler
     * @throws IllegalStateException if the SecurityHandler is in a invalid
     * state on creation.
     */
    public SecurityHandler createSecurityHandler(ComponentContext compCtx, 
        String authLayer)
        throws IllegalStateException
    {
    
        SecurityHandler secHandler = 
            new com.sun.jbi.internal.security.SecurityHandlerImpl(sSecInstCfg, compCtx, 
                sEnvCtx, this, authLayer);
        
        if ( compCtx != null )
        {
            mSecurityHandlers.put(compCtx.getComponentName(), secHandler);
        }
        return secHandler;
    }
    
    /**
     * Get the SecurityHandler for a Component.
     *
     * @param componentName is the Component Name
     * @return a instance of the components SecurityHandler, if it exists null otherwise.
     */
    public SecurityHandler getSecurityHandler(String componentName)
    {
       return  (SecurityHandler) mSecurityHandlers.get(componentName);
    }
    
    /**
     * Remove the SecurityHandler for the component, this method should be called when 
     * the component is shutdown or uninstalled.
     *
     * @param componentName is the Component Name
     */
    public void removeSecurityHandler(String componentName)
    {
        mSecurityHandlers.remove(componentName);
    }
    
    /**
     * Initialize the Service.
     *
     * @param aContext is the Environment Context.
     * @throws JBIException on config realted errors
     * 
     */ 
    public void initService (com.sun.jbi.EnvironmentContext aContext) 
        throws javax.jbi.JBIException
    {
        sEnvCtx = aContext;
        sTranslator = 
            sEnvCtx.getStringTranslator(this.getClass().getPackage().getName());

        // -- Initialize the model system service:
        super.initModelSystemService(sEnvCtx, mLogger, mJbiNameInfo);
        
        // -- This is a hack for now, till I fix the issue with the management tests
        this.restore();
        bootstrapConfig();
        
        sSecInstCfg = SecurityInstallConfig.createSecurityInstallConfig(this, 
            sTranslator);

        // -- Replace configuration mbean in INITIAL mbean set with mine:
        super.mInitialMBeans.replace(
            super.mConfigMBeanName,
            com.sun.jbi.internal.security.mbeans.SecurityServiceConfigMBean.class,
            createConfigMBean());

        initKeyStoreManagers();
        initUserDomains();
        checkState();
        mLogger.info(sTranslator.getString(LocalStringConstants.SS_INFO_INIT));
    }
    
    /**
     * Start the Service.
     *
     * @throws javax.jbi.JBIException if MBean registration fails.
     */
    public void startService () throws javax.jbi.JBIException
    {
        mLogger.info(sTranslator.getString(LocalStringConstants.SS_INFO_START));

        // -- perform one-time startup tasks for this model system service.
        // -- (registers lifecycle, config, and logger mbeans):
        super.bootstrap();
    }
    
    /**
     * Stop the security service.
     *
     * @throws javax.jbi.JBIException if MBean registration fails.
     */
    public void stopService () throws javax.jbi.JBIException
    {
        mLogger.info(sTranslator.getString(LocalStringConstants.SS_INFO_STOP));
        // -- note - adminService will shutdown management layer.
    }
    
    /**
     * Initialize the KeyStore Service.
     */
    protected void initKeyStoreManagers ()
    {
        HashMap services = sSecInstCfg.getKeyStoreContexts ();
        
        if ( services.isEmpty () )
        {
            mLogger.warning ( sTranslator.getString(LocalStringConstants.BC_ERR_NO_KM));
            return;
        }
        
        for ( Iterator itr = services.keySet ().iterator (); itr.hasNext ();)
        {
            String mgrName = (String) itr.next ();
            Properties props = (Properties) services.get (mgrName);
            
            mLogger.info (sTranslator.getString (
                LocalStringConstants.BC_INFO_INITIALIZING_KM, mgrName));

            KeyStoreManager mgr = 
                com.sun.jbi.internal.security.keymgt.KeyStoreManagerFactory.
                    createKeyStoreManager(props);
            try
            {
                mgr.setName (mgrName);
                setPluginStringTranslator (mgr);
            }
            catch ( IllegalStateException iex)
            {
                mLogger.warning (sTranslator.getString (
                    LocalStringConstants.BC_ERR_KM_INITIALIZATION_FAILED,
                    mgrName, iex.toString ()));
                continue;
            }
            sKeyStoreManagers.put (mgrName, mgr);
            mLogger.info ((sTranslator.getString (
                LocalStringConstants.BC_INFO_KM_REGISTERED, mgrName)));  
        }
    }
    
    /**
     * Initialize the User Domains
     */
    protected void initUserDomains ()
    {
        HashMap domains = sSecInstCfg.getUserDomainContexts ();
        if ( domains.isEmpty () )
        {
            mLogger.warning (sTranslator.getString (LocalStringConstants.BC_WRN_NO_UD));
            return;
        }
        
        for ( Iterator itr = domains.keySet().iterator (); itr.hasNext();)
        {
            String domainName = (String) itr.next ();
            Properties props = (Properties) domains.get (domainName);
            
            String authType = (String) props.get (Constants.DOMAIN);
            if ( authType == null )
            {
                authType = Constants.JAAS;
            }
           
            UserDomain domain = new 
                com.sun.jbi.internal.security.auth.AuthenticationContext(domainName);
            domain.setAuthType(authType);          
            sUserDomains.put (domainName, domain);
            mLogger.info ((sTranslator.getString (
                LocalStringConstants.BC_INFO_UD_REGISTERED, domainName)));
        }
    }
    
    /**
     * Check the state of the instance
     *
     * @throws JBIException if the state is invalid
     */
    protected void checkState () throws JBIException
    {
        if (( sSecInstCfg.getDefaultUserDomainName() != null ) &&
            ( getUserDomain(sSecInstCfg.getDefaultUserDomainName()) == null ) )
        {
            throw new JBIException( sTranslator.getString (
                LocalStringConstants.BC_ERR_DEFAULT_UD_CREATION_FAILED,
                sSecInstCfg.getDefaultUserDomainName() ));
        }
        
        if (( sSecInstCfg.getDefaultKeyStoreManagerName() != null ) &&
            ( getKeyStoreManager(sSecInstCfg.getDefaultKeyStoreManagerName ()) == null ) )
        {
            throw new JBIException(sTranslator.getString(
                LocalStringConstants.BC_ERR_DEFAULT_KM_CREATION_FAILED,
                sSecInstCfg.getDefaultKeyStoreManagerName() ));
        }
    }
    
    /**
     * Get an instance of a User Domain.
     *
     * @param name is the UserDomain name
     * @return the UserDomain instance.
     */
    public static UserDomain getUserDomain (String name)
    {
        return (UserDomain) sUserDomains.get (name);
    }
    
    /**
     * Get an instance of the KeyStore Service.
     *
     * @param name is the KeyStore Service name
     * @return the KeyStoreManager instance.
     */
    public static KeyStoreManager getKeyStoreManager (String name)
    {
        return (KeyStoreManager) sKeyStoreManagers.get (name);
    }
    
    /**
     * Set the StringTranslator for a ComponentPlugin instance.
     *
     * @param obj is the instance of ComponentPlugin.
     */
    private void setPluginStringTranslator (Object obj)
    {
        if ( obj instanceof com.sun.jbi.internal.security.ComponentPlugin)
        {
            ( (com.sun.jbi.internal.security.ComponentPlugin) obj).setStringTranslator(
                sEnvCtx.getStringTranslator( obj.getClass().getPackage().getName() ) );
        }
    }
    
    /**
     * Create the Security Service Config MBean Instance.
     *
     * @return an instance of the SecurityServiceConfigMBean.
     * @throws JBIException on errors.
     */
    private SecurityServiceConfigMBeanImpl createConfigMBean()
        throws JBIException
    {
        // -- Might need to pass the StringTranslator here
        return new SecurityServiceConfigMBeanImpl(this, sEnvCtx);
    }
    
    /**
     * If there are no properties specified then I bootstrap myself. Ideally this
     * information should be obtained from the config file.
     */
    private void bootstrapConfig()
    {
        String [] propertyKeys = this.getPropertyKeys();
        
        if ( propertyKeys.length == 0 )
        {
            this.setProperty ( "uds.ud.default", "file");
            this.setProperty ( "uds.ud1.name",  "file");
            this.setProperty ( "uds.ud1.domain", "JAAS");
            
            this.setProperty ( "kms.km.default", "SimpleManager");
            this.setProperty ( "kms.km1.name", "SimpleManager");
            this.setProperty ( "kms.km1.manager",
                "SJSAS");
            /**
            this.setProperty ( "kms.km1.param1.name", "keystore.type" );
            this.setProperty ( "kms.km1.param1.value", "JKS");
            this.setProperty ( "kms.km1.param2.name", "keystore.url" );
            this.setProperty ( "kms.km1.param2.value", sEnvCtx.getAppServerInstanceRoot() 
                + java.io.File.separator + "config"
                + java.io.File.separator  + "keystore.jks");
            this.setProperty ( "kms.km1.param3.name", "keystore.password" );
            this.setProperty ( "kms.km1.param3.value", "changeit");
            this.setProperty ( "kms.km1.param4.name", "truststore.type" );
            this.setProperty ( "kms.km1.param4.value", "JKS");
            this.setProperty ( "kms.km1.param5.name", "truststore.url" );
            this.setProperty ( "kms.km1.param5.value", sEnvCtx.getAppServerInstanceRoot() 
                + java.io.File.separator + "config" 
                + java.io.File.separator  + "cacerts.jks");
            this.setProperty ( "kms.km1.param6.name", "truststore.password" );
            this.setProperty ( "kms.km1.param6.value", "changeit");
             ***/
            
            this.setProperty ( "transport." + com.sun.jbi.internal.security.https.
                HttpConstants.PARAM_SSL_PROTOCOL , "TLS");
            this.setProperty ( "transport." + com.sun.jbi.internal.security.https.
                HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH , "false");
                   
            // This is a hack ..
            this.save();
            this.restore();
        }
    }
    
    /**
     * Get the String Translator.
     *
     * @param packageName is the name of the package for which a translator is required.
     * @return the StringTranslator for a particular package.
     */
    public static StringTranslator getStringTranslator(String packageName)
    {
        return sEnvCtx.getStringTranslator(packageName);
    }
    
    /**
     * Get the String Translator.
     *
     * @return the StringTranslator for the "com.sun.jbi.internal.security" package.
     */
    public static StringTranslator getStringTranslator()
    {
        return sEnvCtx.getStringTranslator(Constants.PACKAGE);
    }
    
    /**
     * Get the Name of the default User Domain.
     *
     * @return the name of the Default User Domain
     */
    public static String getDefaultUserDomainName()
    {
        return sSecInstCfg.getDefaultUserDomainName();
    }

    /**
     * Get the Name of the default Key Store Manager.
     *
     * @return the name of the Default Key Store Manager
     */
    public static String getDefaultKeyStoreManagerName()
    {
        return sSecInstCfg.getDefaultKeyStoreManagerName();
    }

    /**
     * Get the default Key Store Manager.
     *
     * @return the the Default Key Store Manager
     */
    public static KeyStoreManager getDefaultKeyStoreManager()
    {
        return getKeyStoreManager(getDefaultKeyStoreManagerName());
    }     

    /**
     * Get the default User Domain.
     *
     * @return the Default User Domain
     */
    public static UserDomain getDefaultUserDomain()
    {
        return getUserDomain(getDefaultUserDomainName());
    }     

    /**
     * Get the default Security Context.
     *
     * @return the Transport Security Context (SSL Configuration)
     */
    public static Properties getTransportSecurityContext()
    {
        return sSecInstCfg.getTransportSecurityContext();
    }
      
    /**
     * Get the Security Configuration established at install time.
     * @return the Security Configuration established at install time
     */
    public static SecurityConfiguration getSecurityConfiguration ()
    {
        return (SecurityConfiguration) sSecInstCfg;
    }    
    
    /**
     * Get the System Schema Directory.
     *
     * @return the path as string to the JBI Schema Folder.
     */
    public String getSchemaDir()
    {
        return getJbiInstallRoot() + java.io.File.separator + Constants.SCHEMA_DIR;
    }
    
    /**
     * @return the JBI_INSTALL_ROOT value as String
     */
    
    public String getJbiInstallRoot()
    {
        return sEnvCtx.getInitialProperties().getProperty(
            Constants.JBI_INSTALL_ROOT).trim();
    }
    
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndptSecConfig.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  EndptSecConfigImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on March 18, 2005, 12:20 PM
 */

package com.sun.jbi.internal.security.config;

import com.sun.enterprise.security.jauth.AuthPolicy;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.binding.security.Context;
import com.sun.jbi.internal.security.Constants;


import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Set;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class EndptSecConfig
    implements EndpointSecurityConfig
{
    /** The Logger. */
    private Logger mLogger;
    
    /** The String Translator. */
    private StringTranslator mTranslator;
    
    /** The Endpoint Security Environment. */
    private SecurityContext mSecurityEnv;
    
    /** The Message Protection Policy. */
    private ArrayList mMsgSecPolicies;    
    
    /** The default Message Protection Policy. */
    private MessageSecPolicy mDefMsgSecPolicy;
    
    /** The Document. */
    private Document mDom;
    
    /** The Endpoint to which this config applies. */
    private EndpointInfo mEndpoint;
     
    
    /**
     * Create a Endpoint Deployment Security Configuration with default settings.
     *
     */
    public EndptSecConfig()
    {
        mLogger = Logger.getLogger(Constants.PACKAGE);
        mTranslator = com.sun.jbi.internal.security.SecurityService.getStringTranslator 
        (Constants.PACKAGE);

        mDefMsgSecPolicy = new MessageSecPolicyImpl(new java.util.HashSet(), 
            null, null, null);
        mEndpoint = null;
        mSecurityEnv = new BindingSecurityContext(mTranslator);
    }
     
    /** 
     * Creates a new instance of EndptSecConfigImpl.
     *
     * @param translator is the StringTranslator
     * @param dom is the dom for the Security Config data.
     * @throws Exception on Errors.
     */
    public EndptSecConfig(Document dom, StringTranslator translator)
        throws Exception
    {
        mLogger = Logger.getLogger(Constants.PACKAGE);
        mTranslator = translator;
        mDom = dom;

        mDefMsgSecPolicy = new MessageSecPolicyImpl(new java.util.HashSet(), 
            null, null, null);
        // --Parse the dom and get the SecurityEnvironment and all the Message Policies
        parse(dom);
    }
    
    /**
     * Get the Message Protection Policy for a particular operation
     * provided by the Endpoint.
     *
     * @param opName is the Operation Name.
     * @return the Message Protection Policy for the Endpoint Operation.
     */
    public MessageSecPolicy getMessagePolicy (String opName)
    {
        // -- Go through all the Message Policies and get the
        // -- one for the operation
        return getMatchingMessagePolicy(opName);
    }
    
    /**
     * Get the Security Environment for the Endpoint.
     *
     * @return the SecurityContext for the Endpoint
     */
    public SecurityContext getSecurityContext()
    {
        return mSecurityEnv;        
    }
    
    /**
     * Get the Endpoint Security Environment from the Endpoint Security Configuration
     *
     * @return the Security Context for the deployed endpoint.
     * @throws Exception on errors.
     */
    private SecurityContext readSecurityContext()
        throws Exception
    {
        // -- Get the SecurityContext from the Dom 
        Element 
            ep = (Element) mDom.getElementsByTagName(Constants.ELEMENT_ENDPOINT).item(0);
        
        NodeList list = ep.getElementsByTagName(Constants.ELEMENT_SECENV);
        BindingSecurityContext bsecCtx = new BindingSecurityContext(mTranslator);
        if ( list.getLength() == 1 )
        {
            Element secElement = (Element) list.item(0);
                
            // -- Get the KeyStore information
            list = secElement.getElementsByTagName(Constants.ELEMENT_KEYSTORE_MANAGER);
            if ( list.getLength() == 1 )
            {       
                Element 
                    element = (Element) list.item(0);
                bsecCtx.setKeyStoreManagerName(
                    ((Text) element.getFirstChild()).getData());
            }

            list = secElement.getElementsByTagName(Constants.ELEMENT_USER_DOMAIN);
            if ( list.getLength() == 1 )
            {       
                Element 
                    element = (Element) list.item(0);
                bsecCtx.setUserDomainName(((Text) element.getFirstChild()).getData());
            }
            
            list = secElement.getElementsByTagName(Constants.ELEMENT_PROVIDER_ID);
            if ( list.getLength() == 1 )
            {       
                Element 
                    element = (Element) list.item(0);
                bsecCtx.setMessageProviderId(((Text) element.getFirstChild()).getData());
            }

            getTransportSecurityParameters(secElement, bsecCtx);
            
        }
        return bsecCtx;
        
    }
    
    /**
     * Get the Default Message Sec. Policy
     *
     * @return the default MessageSecurityPolicy
     */
    public MessageSecPolicy getDefaultMessagePolicy()
    {
        return mDefMsgSecPolicy;
    }
    
    /**
     * Get the MessagePolicy matching the Operation Name.
     *
     * @param opName is the name of the Endpoint Opeartion whose matching
     * policy is required.
     * @return the MessageSecPolicy matching the opName
     */
    private MessageSecPolicy getMatchingMessagePolicy(String opName)
    {
        java.util.Iterator itr = mMsgSecPolicies.iterator();
        while ( itr.hasNext() )
        {
            MessageSecPolicy secPolicy = (MessageSecPolicy) itr.next();
            
            if ( secPolicy.getOperations().contains(opName))
            {
                return secPolicy;
            }
        }
        
        mLogger.info(mTranslator.getString(
            com.sun.jbi.internal.security.LocalStringConstants.BC_INFO_NO_EP_OP_SEC_POLICY,
            new String[]{opName,
                "{" + mEndpoint.getTargetNamespace() + "}" 
                + ":" + mEndpoint.getServiceName()
                + ":" + mEndpoint.getName()} ) );
        
        return null;
    }
    
    /**
     *
     * @param dom is the Endpoint Security Config dom
     * @throws Exception on errors.
     */
    private void parse(Document dom)
        throws Exception
    {
        mEndpoint = readEndpointInfo();
        mSecurityEnv = readSecurityContext();
        mMsgSecPolicies = readMsgSecPolicies();        
    }
    
    /**
     * Get the EndpointInfo to which this config applies.
     *
     * @return the Endpoint information.
     */
    public EndpointInfo getEndpointInfo()
    {
        return mEndpoint;
    }
    
    /**
     * @return the Endpoint Information from the Deployment Configuration
     * @throws Exception on Errors.
     */
    private EndpointInfo readEndpointInfo()
        throws Exception
    {
        
        Element 
            ep = (Element) mDom.getElementsByTagName(Constants.ELEMENT_ENDPOINT).item(0);
         
        return new EndpointInfoImpl(ep.getAttribute(Constants.ATTR_NAME).trim(),
            ep.getAttribute(Constants.ATTR_SERVICE).trim(),
            new java.net.URI(ep.getAttribute(Constants.ATTR_TNS).trim()));
    }
    
    /**
     * Get all the Transport Security ( SSL ) configuration details from the
     * TransportSecurity element. The values read are set as Name/Value pairs in
     * the Security Context.
     *
     * @param current is the Element to get the parameters from
     * @param ctx is the Context to add the parameters to.
     */
    private void getTransportSecurityParameters (Element current, Context ctx)
    {
        // -- Get all the parameter name-vale pairs
        Element transportSec = (Element) current.getElementsByTagName (
            Constants.ELEMENT_TRANSPORT_SECURITY).item (0);
        
        if ( transportSec != null )
        {
            Element client = (Element) transportSec.getElementsByTagName (
                Constants.ELEMENT_CLIENT).item (0);
            
            // -- SSL Protocol value
            Element sslProtocol = (Element) client.getElementsByTagName (
                Constants.ELEMENT_SSL_PROTOCOL).item (0);
            if ( sslProtocol != null )
            {
                ctx.setValue (com.sun.jbi.internal.security.https.
                    HttpConstants.PARAM_SSL_PROTOCOL,
                    ((Text) sslProtocol.getFirstChild ()).getData ());
            }
            
            // -- Use Default
            Element useDef = (Element) client.getElementsByTagName (
                Constants.ELEMENT_SSL_USE_DEFAULT).item (0);
            if ( useDef != null )
            {
                ctx.setValue (com.sun.jbi.internal.security.https.
                    HttpConstants.PARAM_SSL_USE_DEFAULT,
                    ((Text) useDef.getFirstChild ()).getData ());
            }
            
            // -- Client Alias
            Element clientAlias = (Element) client.getElementsByTagName (
                Constants.ELEMENT_SSL_CLIENT_ALIAS).item (0);
            if ( clientAlias != null )
            {
                ctx.setValue (com.sun.jbi.internal.security.https.
                    HttpConstants.PARAM_SSL_CLIENT_ALIAS,
                    ((Text) clientAlias.getFirstChild ()).getData ());
            }
            
            Element server = (Element) transportSec.getElementsByTagName (
                Constants.ELEMENT_SERVER).item (0);
            
            if ( server != null )
            {
                // -- Require Client Auth value
                Element reqClientAuth = (Element) server.getElementsByTagName (
                    Constants.ELEMENT_SSL_REQ_CLIENT_AUTH).item (0);
                if ( reqClientAuth != null )
                {
                    ctx.setValue (com.sun.jbi.internal.security.https.
                        HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH,
                        ((Text) reqClientAuth.getFirstChild ()).getData ());
                }
            }
        }
    }
    
    /**
     * @return the List of MessageSecurityPolicies defined in the Endpoint Configuration.
     * @throws Exception on Errors
     */
    private ArrayList readMsgSecPolicies()
        throws Exception
    {
        ArrayList secPolicies = new ArrayList();
        
        Element 
            ep = (Element) mDom.getElementsByTagName(Constants.ELEMENT_ENDPOINT).item(0);
        
        NodeList 
            list = ep.getElementsByTagName(Constants.ELEMENT_OPERATION);
        
        for ( int i = 0; i < list.getLength(); i++ )
        {
            Element currentOp = (Element) list.item(i);
             
            // -- If there is a Provider then get that
            NodeList prl = currentOp.getElementsByTagName(Constants.ELEMENT_PROVIDER_ID);
            String providerId = null;
            if ( prl.getLength() == 1 )
            {
                providerId = ( (Text) prl.item(0).getFirstChild()).getData().trim();
            }
            
            Element reqPolicy = (Element) currentOp.getElementsByTagName(
                Constants.ELEMENT_REQ_POLICY).item(0);
            
            Element respPolicy = (Element) currentOp.getElementsByTagName(
                Constants.ELEMENT_RESP_POLICY).item(0);

            // -- Get all the operations the above policy applies to 
            // -- Keep the Policy with the operations.
            NodeList opNames = currentOp.getElementsByTagName(
                Constants.ELEMENT_NAME);
            
            Set opNameSet = new java.util.HashSet();
            for ( int j = 0; j < opNames.getLength(); j++ )
            {
                opNameSet.add(( (Text) opNames.item(j).getFirstChild()).getData()
                    .trim());
            }
            
            // -- Add to the list
            secPolicies.add(new MessageSecPolicyImpl(opNameSet, getAuthPolicy(reqPolicy),
                getAuthPolicy(respPolicy) , providerId));
        }   
        return secPolicies;
    }

    /**
     * @param policyElement is a Policy Element
     * @return AuthPolicy
     */
    private AuthPolicy getAuthPolicy(Element policyElement)
    {
        if ( policyElement == null )
        {
            return null;
        }
        
        // -- Source Auth Type
        String srcAuth = policyElement.getAttribute(Constants.ATTR_AUTH_SRC);
        int srcAuthType = AuthPolicy.SOURCE_AUTH_NONE;
        if ( srcAuth != null )
        {
            if ( srcAuth.equals(AuthPolicy.SENDER) )
            {
                srcAuthType = AuthPolicy.SOURCE_AUTH_SENDER;
            }
            else if ( srcAuth.equals(AuthPolicy.CONTENT) )
            {
                srcAuthType = AuthPolicy.SOURCE_AUTH_CONTENT;
            }
        }
        
        // -- Recipient Auth Type.
        boolean rcpAuthReq = false;
        String rcpAuth = policyElement.getAttribute(Constants.ATTR_AUTH_RECP);
        boolean beforeContent = false;
        if ( rcpAuth != null )
        {         
            if ( rcpAuth.equals(AuthPolicy.BEFORE_CONTENT) )
            {
                beforeContent = true;
                rcpAuthReq = true;
            }
            else if ( rcpAuth.equals(AuthPolicy.AFTER_CONTENT) )
            {
                beforeContent = false;
                rcpAuthReq = true;
            }
        }
        
        return new AuthPolicy(srcAuthType, rcpAuthReq, beforeContent );        
    }
    
}

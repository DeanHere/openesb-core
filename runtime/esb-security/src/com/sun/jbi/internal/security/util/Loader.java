/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Loader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 * Loader.java
 *
 * SUN PROPRIETARY/CONFIDENTIAL.
 * This software is the proprietary information of Sun Microsystems, Inc.
 * Use is subject to license terms.
 *
 */

package com.sun.jbi.internal.security.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Reader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

/**
 * This class is a helper class which aids in converting
 * a DOM Document to a File and vice-versa.
 *
 * @author Sun Microsystems, Inc.
 */
public class Loader
{
    
    /** Schema Language. */
    private static final String JAXP_SCHEMA_LANGUAGE = 
        "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    
    /** W3C Schema. */
    private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
    
    /** Schema Source. */
    private static final String JAXP_SCHEMA_SOURCE = 
        "http://java.sun.com/xml/jaxp/properties/schemaSource";
     
    /**
     * Given a filename load an XML document into
     * a Document object and return it.
     * 
     * @param filename is the String filename of the XML file
     * references, if this paramter is null the XML parsers default resolution is used.
     * @param validate indicates that the document is to be validated.
     * @param schemaFiles are the schema files to be used if validation is to be done, 
     * this parameter can be null if validate = false.
     * @param handler is the ErrorHandler
     * @throws java.io.FileNotFoundException if the file with name filename is not found.
     * @throws java.io.IOException on IO errors
     * @throws SAXException on XML parsing errors.
     * @throws ParserConfigurationException if the XML Parser is not configured correctly.
     * @return the XML Document object 
     */
    public Document load(String filename, boolean validate, 
        org.xml.sax.ErrorHandler handler, String[] schemaFiles)
        throws java.io.IOException,
            java.io.FileNotFoundException,
            SAXException,
            ParserConfigurationException
    {

        return load(new FileInputStream(new File(filename)), validate, 
            handler, schemaFiles); 
         
    };
     
    /**
     * Given a Inputstream load an XML document into
     * a Document object and return it.
     * 
     * @param istr is a stream to the input XML file.
     * @param validate if true indicates that the document is to be validated
     * against it's schema.
     * @param handler is the ErrorHandler which is to be delegated XML parsing Error
     * handling tasks.
     * @param schemaFiles are the schema files to be used if validation is to be done, 
     * this parameter can be null if validate = false.
     * @return the XML Document object 
     * @throws SAXException on XML Parsing errors.
     * @throws ParserConfigurationException if the parser is not configured correctly. 
     * @throws java.io.IOException on IO Errors.
     */
    public Document load(InputStream istr, boolean validate,
        org.xml.sax.ErrorHandler handler, String[] schemaFiles)
        throws java.io.IOException,
            SAXException,
            ParserConfigurationException
    { 

        return load(new InputSource(istr), validate, handler, schemaFiles); 
    };
    
    /**
     * Given a Inputstream load an XML document into
     * a Document object and return it.
     * 
     * @param reader is a reader to the input XML file
     * @param validate if true indicates that the document is to be validated
     * against it's schema.
     * @param handler is the ErrorHandler which is to be delegated XML parsing Error
     * handling tasks.
     * @param schemaFiles are the schema file to be used if validation is to be done, 
     * this parameter can be null if validate = false.
     * @return the XML Document object 
     * @throws SAXException on XML Parsing errors.
     * @throws ParserConfigurationException if the parser is not configured correctly. 
     * @throws java.io.IOException on IO Errors.
     */
    public Document load(Reader reader, boolean validate,
        org.xml.sax.ErrorHandler handler, String[] schemaFiles)
        throws java.io.IOException,
            SAXException,
            ParserConfigurationException
    { 

        return load(new InputSource(reader), validate, handler, schemaFiles); 
    };
    
    /**
     * Given a Inputstream load an XML document into
     * a Document object and return it.
     * 
     * @param ipsrc is a SAX InputSource to the XML file
     * @param validate if true indicates that the document is to be validated
     * against it's schema.
     * @param handler is the ErrorHandler which is to be delegated XML parsing Error
     * handling tasks.
     * @param schemaFiles are the schema file to be used if validation is to be done, 
     * this parameter can be null if validate = false.
     * @return the XML Document object 
     * @throws SAXException on XML Parsing errors.
     * @throws ParserConfigurationException if the parser is not configured correctly. 
     * @throws java.io.IOException on IO Errors.
     */
    public Document load(InputSource ipsrc, boolean validate,
        org.xml.sax.ErrorHandler handler, String[] schemaFiles)
        throws java.io.IOException,
            SAXException,
            ParserConfigurationException
    { 

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setValidating(validate);
        docFactory.setNamespaceAware(true);

        if ( validate )
        {
            docFactory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA); 
            docFactory.setAttribute(JAXP_SCHEMA_SOURCE, schemaFiles);
        }

        DocumentBuilder 
            bldr = docFactory.newDocumentBuilder();
        if (handler != null) 
        {
            bldr.setErrorHandler(handler);
        }   

        return bldr.parse(ipsrc);
    };
}

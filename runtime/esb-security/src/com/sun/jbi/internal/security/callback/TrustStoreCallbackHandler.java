/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TrustStoreCallbackHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecretKeyCallbackHandler.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 23, 2005, 1:53 PM
 */

package com.sun.jbi.internal.security.callback;

import com.sun.enterprise.security.jauth.callback.TrustStoreCallback;
import com.sun.jbi.internal.security.KeyStoreManager;

import java.io.IOException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * This CallbackHandler handles the SecretKey Callback.
 *
 * @author Sun Microsystems, Inc.
 */
public class TrustStoreCallbackHandler
    implements CallbackHandler
{
     /** The reference to the KeyStoreManager. */
    private KeyStoreManager mKeyMgr;
    
    /** The X509 Certificate Type. */
    private static final String X509 = "X509";
    
    /** 
     * Creates a new instance of SecretKeyCallbackHandler.
     * 
     * @param mgr - KeyStoreManager instance which provides the handle
     * to the KeyStores.
     */
    public TrustStoreCallbackHandler (KeyStoreManager mgr)
    {
        mKeyMgr = mgr;
    }
    
    /**
     * The implementation on the CallbackInterface. This method only handles 
     * SecretKeyCallback.
     *
     * @param callbacks - array of Callbacks to be handled.
     * @throws IOException - if an input or output error occurs. 
     * @throws UnsupportedCallbackException - if the implementation of this method
     * does not support one or more of the Callbacks specified in the callbacks 
     * parameter.
     */
    public void handle(Callback[] callbacks)
        throws IOException, UnsupportedCallbackException
    {
        for (int i = 0; i < callbacks.length; i++) 
        {
            CallbackHandler handler = null;
            
            if  (callbacks[i] instanceof TrustStoreCallback)
            {
                TrustStoreCallback cb = (TrustStoreCallback) callbacks[i];
                cb.setStore(mKeyMgr.getTrustStore());
            } 
            else
            {
                throw new UnsupportedCallbackException(callbacks[i]);
            }
        }
    }
}

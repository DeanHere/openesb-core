/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  LoacalStringConstants.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on January 27, 2005, 5:05 PM
 */

package com.sun.jbi.security.auth.module;

/**
 * Localization String Keys for this package.
 *
 * @author Sun Microsystems, Inc.
 */
public class LocalStringConstants
{
    /**
     * Login module is missing configuration.
     */
    public static final String AUTH_MISSING_CONFIG_OPTION = "AUTH_MISSING_CONFIG_OPTION";
    
    /**
     * Login module could not load user information.
     */
    public static final String AUTH_ERR_LOAD_USERS_FAILED = "AUTH_ERR_LOAD_USERS_FAILED";
    
    /**
     * The User information file is missing.
     */
    public static final String AUTH_MISSING_INVALID_FILE = "AUTH_MISSING_INVALID_FILE";
    
    /** Login Module Init failed. */
    public static final String AUTH_LM_INIT_FAILED = "AUTH_LM_INIT_FAILED";
    
    /** Login Module Missing Callback Handler. */
    public static final String AUTH_LM_MISSING_CBHNDLR = "AUTHLM_MISSING_CBHNDLR";
    
    /** Unsupported Callback. */
    public static final String AUTH_LM_UNSUP_CB = "AUTH_LM_UNSUP_CB";
    
    /** User auth failed. */
    public static final String AUTH_USER_AUTH_FAILED = "AUTH_USER_AUTH_FAILED";
    
    /** User auth succeeded. */
    public static final String AUTH_LM_USER_AUTH_SUCCESS = "AUTH_LM_USER_AUTH_SUCCESS";
    
    /** User auth failed. */
    public static final String AUTH_LM_USER_AUTH_FAILURE = "AUTH_LM_USER_AUTH_FAILURE";
    
    /** Unused User Prompt. */
    public static final String AUTH_USER_PROMPT = "AUTH_USER_PROMPT";
    
    /** Unused password prompt. */
    public static final String AUTH_PASSWD_PROMPT = "AUTH_PASSWD_PROMPT";
    
    
}

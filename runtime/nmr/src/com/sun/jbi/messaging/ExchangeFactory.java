/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExchangeFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import com.sun.jbi.messaging.util.Translator;
import com.sun.jbi.messaging.util.WSDLHelper;

import java.net.URI;
import java.util.HashMap;
import java.util.logging.Logger;
import javax.xml.namespace.QName;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOptionalOut;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.servicedesc.ServiceEndpoint;

import org.w3c.dom.Document;

/** A message exchange factory is used used to create new instances of 
 *  MessageExchange. Service consumers use these factories to create message 
 *  exchanges when initiating a new service request. Message exchange factories
 *  are created using the javax.jbi.component.ComponentContext given to the
 *  component during its initialization (see javax.jbi.component.ComponentLifeCycle ). 
 *  There are a variety of ways to creating such factories, each of which creates
 *  a context that is used to provide some of the default properties of 
 *  MessageExchange instances created by the factory instances. For example, 
 *  a factory can be created for a particular endpoint, ensuring that all 
 *  exchanges created by the factory have that endpoint set as the default 
 *  endpoint property of the exchange. This allows components to retain 
 *  factories as a way of aligning internal processing context with the context 
 *  contained in the factory, ensuring that the exchanges created consistently 
 *  reflect that context.
 * @author Sun Microsystems, Inc.
 */
public class ExchangeFactory implements javax.jbi.messaging.MessageExchangeFactory
{   
    private MessageService      mMsgSvc;
    private QName               mInterface;
    private QName               mService;
    private ServiceEndpoint     mEndpoint;
    private EndpointRegistry    mRegistry;
    
    ExchangeFactory(MessageService msgSvc)
    {
        mMsgSvc     = msgSvc;
        mRegistry   = EndpointRegistry.getInstance();
    }    
    
    static ExchangeFactory newInterfaceFactory(
        MessageService msgSvc, QName interfaceName)
    {
        ExchangeFactory ef;
        
        ef = new ExchangeFactory(msgSvc);
        ef.setInterface(interfaceName);
        
        return ef;
    }
    
    static ExchangeFactory newServiceFactory(
        MessageService msgSvc, QName serviceName)
    {        
        ExchangeFactory ef;
        
        ef = new ExchangeFactory(msgSvc);
        ef.setService(serviceName);
        
        return ef;
    }
    
    static ExchangeFactory newEndpointFactory(
        MessageService msgSvc, ServiceEndpoint endpoint)
    {        
        ExchangeFactory ef;
        
        ef = new ExchangeFactory(msgSvc);
        ef.setEndpoint(endpoint);
        
        return ef;
    }
    
    public MessageExchange createExchange(URI pattern) 
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchangeProxy 	proxy;
        MessageExchangeImpl 	impl;
        String                  uriStr;

        // Decide which pattern to create
        switch (ExchangePattern.valueOf(pattern))
        {
            case IN_ONLY:
                proxy = new InOnlyImpl();
                 break;
            
            case IN_OUT:
                proxy = new InOutImpl();
                break;
                
            case IN_OPTIONAL_OUT:
                proxy = new InOptionalOutImpl();
                 break;
                
            case ROBUST_IN_ONLY:
                proxy = new RobustInOnlyImpl();
                break;
                
            default:
                throw new javax.jbi.messaging.MessagingException(Translator.translate(
                        LocalStringKeys.INVALID_MEP_URI, new Object[] {pattern}));
                
        }
       
        // Associate proxy with exchange impl
        impl = new MessageExchangeImpl(proxy, mMsgSvc);
        
        // Apply any defaults from the factory
        proxy.setInterfaceName(mInterface);
        proxy.setService(mService);
        proxy.setEndpoint(mEndpoint);

        return proxy;
    }
    
    public MessageExchange createExchange(QName serviceName, QName operationName)
        throws javax.jbi.messaging.MessagingException 
    {
        MessageExchange      exchange;
        RegisteredEndpoint[] endpoints;
        Document             desc;
        HashMap              operations = null;
        URI                  opURI;
        
        // try and find an endpoint for this service
        endpoints = mRegistry.getInternalEndpointsForService(serviceName, true);
        
        if (endpoints.length == 0)
        {
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.CANT_FIND_ENDPOINT_FOR_SERVICE, 
                    new Object[] {serviceName}));
        }
        
        desc = mMsgSvc.queryDescriptor(endpoints[0]);
	if (desc != null)
        {
            operations = WSDLHelper.getOperationsForService(desc, serviceName);
        }        
        if (operations == null)
        {            
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.ENDPOINT_NO_DESCRIPTOR, 
                new Object[] {endpoints[0].getEndpointName()} ));
        }
        
        if (!operations.containsKey(operationName.toString()))
        {
            throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.NONEXISTENT_OPERATION, 
                    new Object[] {operationName.toString()} ));
        }       
        
        opURI    = createURI((String)operations.get(operationName.toString()));
        exchange = createExchange(opURI);
        
        exchange.setService(serviceName);
        exchange.setOperation(operationName);
        
        return exchange;
    }
    
    public InOnly createInOnlyExchange() 
        throws javax.jbi.messaging.MessagingException
    {
        return (InOnly)createExchange(ExchangePattern.IN_ONLY.getURI());
    }
    
    public InOptionalOut createInOptionalOutExchange() 
        throws javax.jbi.messaging.MessagingException
    {
        return (InOptionalOut)createExchange(ExchangePattern.IN_OPTIONAL_OUT.getURI());        
    }
    
    public InOut createInOutExchange()
        throws javax.jbi.messaging.MessagingException
    {
        return (InOut)createExchange(ExchangePattern.IN_OUT.getURI()); 
    }
    
    public RobustInOnly createRobustInOnlyExchange() 
        throws javax.jbi.messaging.MessagingException
    {
        return (RobustInOnly)createExchange(ExchangePattern.ROBUST_IN_ONLY.getURI()); 
    }
    
    private void setInterface(QName interfaceName)
    {
        mInterface = interfaceName;
    }
    
    private void setService(QName serviceName)
    {
        mService = serviceName;
    }
    
    private void setEndpoint(ServiceEndpoint endpoint)
    {
        mEndpoint = endpoint;
    }
    
    /** Used to construct the URI flavor of the pattern constants.
     */
    private static URI createURI(String uri)
    {
        try
        {
            return (new URI(uri));
        }
        catch (java.net.URISyntaxException uriEx)
        {
            throw new java.lang.IllegalArgumentException();
        }
    }
    
}

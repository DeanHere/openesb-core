/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import com.sun.jbi.ComponentManager;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.messaging.ExchangeIdGenerator;
import com.sun.jbi.management.support.JbiNameInfo;
import com.sun.jbi.management.support.MBeanHelper;
import com.sun.jbi.messaging.stats.METimestamps;
import com.sun.jbi.messaging.stats.Value;
import com.sun.jbi.messaging.util.Translator;
import com.sun.jbi.messaging.util.XMLUtil;

import java.lang.reflect.Constructor;
import java.net.URI;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.util.concurrent.atomic.AtomicLong;

import javax.jbi.component.Component;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.OpenType;
import javax.management.Notification;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import javax.xml.namespace.QName;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;

import org.w3c.dom.Document;

/**
 * Entry point to NMS for framework and management code.
 * @author Sun Microsystems, Inc.
 */
public class MessageService 
    extends com.sun.jbi.management.system.ModelSystemService
    implements com.sun.jbi.ServiceLifecycle,
               com.sun.jbi.management.system.MessageServiceMBean,
               NMRStatistics
{
    /** Copy of EnvironmentContext given to us at initialization.
     */
    private EnvironmentContext mEnvironmentContext;
    
    private EndpointRegistry    mRegistry;
    
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
    
    /** Hook to framework to retrieve component instances.*/
    private ComponentManager mComponentManager;
    
    /** Reference to TransactionManager if one exists. */
    private TransactionManager mTransactionManager;
    
    /** Table used to store channel references. */
    private Hashtable<String, DeliveryChannelImpl>  mChannels;
    
    /** our immutable name (TODO:  this should come from framework): */
    private final JbiNameInfo mJbiNameInfo = new JbiNameInfo("MessageService");

    /** Listener for endpoint notifications. */
    private EndpointListener        mListener;
    
    /** Listener for exchange timeout notifications. */
    private TimeoutListener         mTimeout;
    private DeliveryChannel         mTimeoutDC;
    
    /** ExchangeIdGenerator for unique exchange Ids. */
    private ExchangeIdGenerator     mGenerator;
    
    /** Basic statistics.       */   
    private long                    mStatisticsEpoch;
    private long                    mTotalChannels;
    private long                    mSendRequest;
    private long                    mReceiveRequest;
    private long                    mSendReply;
    private long                    mReceiveReply;
    private long                    mSendFault;
    private long                    mReceiveFault;
    private long                    mSendStatus;
    private long                    mReceiveStatus;
    private long                    mSendDONE;
    private long                    mReceiveDONE;
    private long                    mSendERROR;
    private long                    mReceiveERROR;
    private long                    mInOutMEPs;
    private long                    mInOnlyMEPs;
    private long                    mRobustInOnlyMEPs;
    private long                    mInOptionalOutMEPs;
    private Value                    mResponseTime;
    private Value                    mStatusTime;
    private Value                    mNMRTime;
    private Value                    mConsumerTime;
    private Value                    mProviderTime;
    private Value                    mChannelTime;
    
    private AtomicLong              mActiveExchanges;
    private AtomicLong              mTotalExchanges;
    
    /** Message Service statistics MBean implementation. */
    private MessageServiceStatistics mStatistics;

    /** XAResources private to JBI Components. */
    LinkedList                      mResources;
    
    /**
     * ArrayList to store all registered observers
     */
    private HashMap<DeliveryChannel,DeliveryChannel> mObserver;

    private static final String OBSERVER_SENDER = "com.sun.jbi.observer.sender";
    private static final String OBSERVER_RECEIVER = "com.sun.jbi.observer.receiver";
    
    /** Monitoring support. */
    private boolean                        mMonitoringEnabled;
    
    /** Create a new instance of the NMS. */
    public MessageService()
    {
        mChannels = new Hashtable();
        mResources = new LinkedList();
        mStatistics = new MessageServiceStatistics(this, mJbiNameInfo.name());
        mObserver = new HashMap();
        mActiveExchanges = new AtomicLong();
        mTotalExchanges = new AtomicLong();
        mResponseTime = new Value();
        mStatusTime = new Value();
        mNMRTime = new Value();
        mProviderTime = new Value();
        mConsumerTime = new Value();
        mChannelTime = new Value();
    }
    
    /******************************************
     *      ServiceLifecycle methods          *
     ******************************************/
    
    /** Init our environment. */
    public void initService(EnvironmentContext ctx)
        throws javax.jbi.JBIException
    {
        // Save our EnvironmentContext.
        
        mEnvironmentContext = ctx;
        mRegistry           = EndpointRegistry.getInstance();
        mComponentManager     = ctx.getComponentManager();
        
        mRegistry.setMessageService(this);

        // init string translator
        Translator.setStringTranslator(
            mEnvironmentContext.getStringTranslator("com.sun.jbi.messaging"));

        // initialize the model system service:
        super.initModelSystemService(ctx, mLog, mJbiNameInfo);

        /*
         * NOTE:  this is the place to override the generic configuration MBean.
         * EXAMPLE:
         * //replace configuration mbean in INITIAL mbean set with mine:
         * super.mInitialMBeans.replace(
         *     super.mConfigMBeanName,
         *     <YOUR_CONFIG_MBEAN>.class,
         *     <YOUR_CONFIG_IMPLEMENTATION_INSTANCE>
         *     );
         * See AdminServiceConfiguration*.java for example.  RT 3/6/05
         */
        super.mInitialMBeans.replace(super.mStatisticsMBeanName,
            com.sun.jbi.messaging.MessageServiceStatisticsMBean.class,
            mStatistics);    

        //add MessageService MBean to START/STOP mbean set.
        super.mStartMBeans.add(
            super.mMessageServiceMBeanName,
            com.sun.jbi.management.system.MessageServiceMBean.class,
            this,
            true  //create a standard mbean that can emit notifications.
            );
        
        super.bootstrap();
    }
    
    /** Start the NMS. */
    public void startService() 
        throws javax.jbi.JBIException
    {        
        mGenerator = (com.sun.jbi.messaging.ExchangeIdGenerator)new ExchangeIdGeneratorImpl(); 
        // Register the Statistics MBean as a listener on the config MBean
        startListeningToConfigurationMBean();
        mStatistics.setLastRestartTime(new Date()); 
        // Get the default timing stats enabled value from the config MBean
        if ( isMsgTimingStatsEnabledByDefault() )
        {
            mStatistics.enableTimingStatistics();
        }
        else
        {
            mStatistics.disableTimingStatistics();
        }
    }
    
    /** Stop the NMS. */
    public void stopService()
        throws javax.jbi.JBIException
    {
        // make sure all of the channels are closed
        for (DeliveryChannelImpl d : mChannels.values())
        {
            d.close();
        }
        
        // clear the channel table
        mChannels.clear();
        // clear the registry
        mRegistry.clear();
        
        // Unregister the Statistics MBean as a listener on the configuration MBean
        stopListeningToConfigurationMBean();
    }
    
    /******************************************
     *          Framework methods             *
     ******************************************/
    
    /** Create a new delivery channel.  If the channel has already been created,
     *  this method throws an exception.
     *  @param componentId component id
     *  @param classLoader class loader for component
     *  @throws javax.jbi.messaging.MessagingException attempt to activate 
     *   duplicate channel.
     */
    public DeliveryChannelImpl activateChannel(String componentId,
        ClassLoader classLoader)
        throws javax.jbi.messaging.MessagingException
    {
        DeliveryChannelImpl dc;
        
        if (mChannels.containsKey(componentId))
        {
            dc = mChannels.get(componentId);
        }
        else
        {
            if (mLog.isLoggable(Level.FINE))
            {
                mLog.finer("Creating delivery channel for component " + componentId);
            }
            dc = new DeliveryChannelImpl(componentId, classLoader, this, mListener);
            addChannel(dc);
        }
        
        return dc;
    }       
    
    /** Get a reference to the NMR ConnectionManager.
     *  @return instance of ConnectionManager.
     */
    public ConnectionManager getConnectionManager()
    {
        return mRegistry;
    }
    
    /******************************************
     *         Internal NMS methods           *
     ******************************************/
    
    /** Adds a channel to the NMS routing table.
     */
    void addChannel(DeliveryChannelImpl channel)
    {
        mChannels.put(channel.getChannelId(), channel);
        mTotalChannels++;
    }
    
    /** Removes a channel from the NMS routing table.
     */
    void removeChannel(String channelId)
    {
        mChannels.remove(channelId);
    }
    
    /** Retrieves a channel based on it's ID from the NMS routing table.
     */
    DeliveryChannelImpl getChannel(String channelId)
    {
        return  mChannels.get(channelId);
    }
    
    /** Get the TransactionManager
     */
    javax.transaction.TransactionManager getTransactionManager()
    {
        // See if we can find a TransactionManager
        if (mTransactionManager == null)
        {
            if (mEnvironmentContext != null)
            {
                mTransactionManager = (javax.transaction.TransactionManager)
                    mEnvironmentContext.getTransactionManager();
            } 
        }
        return (mTransactionManager);
    }
    
    public void addXAResource(XAResource resource)
    {
        mResources.add(resource);
    }
    
    public javax.transaction.xa.XAResource[] getXAResources()
    {
        XAResource[]        resources = new XAResource[mResources.size()];
        
        mResources.toArray(resources);
        return (resources);
    }
    
    public void purgeXAResources()
    {
        mResources.clear();
    }
    
    /**
     * Set the TransactionManager. Used by the junit test harness to bypass using
     * EnvironmentContext (which is not available.)
     */
    void setTransactionManager(javax.transaction.TransactionManager tm)
    {
        mTransactionManager = tm;
    }
    
    /** Perform the exchange operation.
     */
    boolean doExchange(DeliveryChannelImpl channel, MessageExchangeProxy exchange)
        throws javax.jbi.messaging.MessagingException
    {
        DeliveryChannelImpl     targetChannel;
        MessageExchangeProxy    targetExchange;
	boolean			isLast = false;

        addressExchange(exchange, channel);
        exchange.validate(channel, false);
        handleObserver(exchange, channel);
        targetChannel = exchange.getSendChannel();
        targetExchange = exchange.getTwin();
        if (exchange.handleSend(channel))
        {
            channel.removeSendReference(exchange);
            mActiveExchanges.decrementAndGet();
	    isLast = true;
        }
        else
        {
            channel.addSendReference(exchange);
            mActiveExchanges.incrementAndGet();
        }
        targetChannel.queueExchange(targetExchange);
	return (isLast);
    }
    
    /** Perform the exchange operation.
     */
    boolean doSynchExchange(DeliveryChannelImpl channel, 
            MessageExchangeProxy exchange,
            long timeout)
        throws javax.jbi.messaging.MessagingException
    {
        DeliveryChannelImpl     targetChannel;
        MessageExchangeProxy    targetExchange;
        boolean                valid = true;
        boolean                timedout = false;
        
        addressExchange(exchange, channel);
        exchange.validate(channel, true);
        handleObserver(exchange, channel);
        targetChannel = exchange.getSendChannel();
        targetExchange = exchange.getTwin();
        exchange.handleSendSync(channel);
        channel.addSendSyncReference(exchange);
        mStatistics.getMessagingStatisticsInstance().incrementSendSyncs();
        synchronized (exchange)
        {
            exchange.setSynchState((timeout != 0 && mTimeout != null && mTimeoutDC != targetChannel)
                ? MessageExchangeProxy.WAIT_TIMEOUT : MessageExchangeProxy.WAIT);
            targetChannel.queueExchange(targetExchange);
            try
            {
                for (;;)
                {
                    int		state;
					
                    exchange.wait(timeout);
                    state = exchange.getSynchState();
                    if (state == MessageExchangeProxy.HALF_DONE)
                    {
                        exchange.setSynchState(MessageExchangeProxy.DONE);
                        break;
                    }
                    else if (state == MessageExchangeProxy.ERROR)
                    {
                        valid = false;
                        break;
                    }
                    if (timeout != 0)
                    {
                        if (state == MessageExchangeProxy.WAIT)
                        {
                            exchange.terminate();
                            valid = false;
                            break;
                        }
                        else if (state == MessageExchangeProxy.WAIT_TIMEOUT)
                        {
                            timedout = true;
                            break;
                        }
                    }
                }
            }
            catch (java.lang.InterruptedException iEx)
            {
                exchange.terminate();
                valid = false;
            }
        }
        if (timedout)
        {
            if (mTimeout.checkTimeout(channel, exchange))
            {
                exchange.terminate();
            }
            else
            {
                exchange.setSynchState(MessageExchangeProxy.NONE);
                valid = false;
            }
        }
        return (valid);
    }
    
    
    void addressExchange(MessageExchangeProxy exchange, DeliveryChannelImpl channel)
        throws javax.jbi.messaging.MessagingException
    {        
        RegisteredEndpoint  endpoint;
        QName               serviceName;
        QName               interfaceName;
        
	endpoint = (RegisteredEndpoint)exchange.getActualEndpoint();

        //
        // Check addressing of exchange in this order:
        //    (1) endpoint
        //    (2) service name
        //    (3) interface name
        //
        if (endpoint == null)
        {
            if ((serviceName = exchange.getService()) != null)
            {
                RegisteredEndpoint[]    endpoints;
                RegisteredEndpoint      match;
                
                endpoints = mRegistry.getInternalEndpointsForService(serviceName, true);
                if (endpoints.length != 0)
                {                    
                    endpoint = matchConsumerAndProvider(channel.getChannelId(), endpoints, exchange);
                }
                else
                {
                    throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(
                            LocalStringKeys.CANT_FIND_ENDPOINT_FOR_SERVICE,
                            new Object[] { serviceName.toString() }));
                }
            }
            else if ((interfaceName = exchange.getInterfaceName()) != null)
            {                
                RegisteredEndpoint[]    endpoints;
                RegisteredEndpoint      match;
                
                endpoints = mRegistry.getInternalEndpointsForInterface(interfaceName);
                if (endpoints.length != 0)
                {
                    endpoint = matchConsumerAndProvider(channel.getChannelId(), endpoints, exchange);
                }
                else
                {
                    throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(
                            LocalStringKeys.CANT_FIND_ENDPOINT_FOR_INTERFACE,
                            new Object[] { interfaceName.toString() }));
                }
            }
            else
            {      
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.ADDR_NO_ENDPOINT));
            }
            exchange.setEndpoint(endpoint);
        }
            
        /** 
         *  Check to see if we are dealing with a real endpoint or a 
         *  linked endpoint from a service connection.
         */
        if (endpoint.isLinked())
        {            
            exchange.setEndpointLink(endpoint);
            endpoint = mRegistry.resolveLinkedEndpoint((LinkedEndpoint)endpoint);
            
            // check for resolution failure
            if (endpoint == null)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.SERVICE_CONNECTION_NO_ENDPOINT,
                        new Object[] {exchange.getEndpointLink().getServiceName(),
                                      exchange.getEndpointLink().getEndpointName()})); 
            }
            exchange.setEndpoint(endpoint);
        }

	//
	// The final endpoint can't be and ExternalEndpoint.
	//
	if (endpoint.getType() == RegisteredEndpoint.EXTERNAL)
	{
	    throw new javax.jbi.messaging.MessagingException(
	        Translator.translate(LocalStringKeys.EXTERNAL_ENDPOINT_NOT_VALID,
	    	    new Object[] {endpoint.getServiceName(), endpoint.getEndpointName()})); 
    	}
    }
    
   /**
     * Processes an Observed Message Exchange for all components that are designated
     * as Observers.
     *
     * @param exchange             - The exchange to Observe
     * @param channel              - The DeliveryChannel performing the send.
     * @throws MessagingException
     */
     private void handleObserver(MessageExchangeProxy exchange, DeliveryChannelImpl channel)
        throws javax.jbi.messaging.MessagingException
    {
        if (mObserver != null)
        {
            for (DeliveryChannel odc : mObserver.keySet()) 
            {
                MessageExchangeProxy observedExchange = new Observer(exchange);

                MessageExchangeImpl mei = new MessageExchangeImpl(observedExchange,
                        exchange.getMessageExchange(), this);
                observedExchange.setMessageExchange(mei, false);
                observedExchange.setEndpointLink(exchange.getEndpointLink());
                observedExchange.setProperty(OBSERVER_SENDER, channel.getChannelId());
                observedExchange.setProperty(OBSERVER_RECEIVER, exchange.getSendChannel().getChannelId());
                ((DeliveryChannelImpl)odc).queueObserved(observedExchange);
            }
        }
    }

    /** Facilitates intercomponent meta data queries.
     */
    Document queryDescriptor(ServiceEndpoint ref)
    {
        Component   component;
        Document    descriptor = null;
        
        if (ref instanceof LinkedEndpoint)
        {
            ref = mRegistry.resolveLinkedEndpoint((LinkedEndpoint)ref);
            // return immediately if this is a dead link
            if (ref == null)
            {
                return null;
            }
        }
        
        component = mComponentManager.getComponentInstance(
            ((RegisteredEndpoint)ref).getOwnerId());
        
        if (component != null)
        {
            descriptor = component.getServiceDescription(ref);
        }
        
        return descriptor;
    }
    
    DynamicEndpoint resolveEndpointReference(org.w3c.dom.DocumentFragment reference)
    {
        Set             channels;
        DynamicEndpoint dep = null;
        
        channels = ((Hashtable)mChannels.clone()).keySet();
        for (String id : ((Hashtable<String,DeliveryChannelImpl>)mChannels.clone()).keySet())
        {
            Component        comp = mComponentManager.getComponentInstance(id);
            ServiceEndpoint  ep = comp.resolveEndpointReference(reference);
            
            if (ep != null)
            {
                dep = new DynamicEndpoint(ep, id, reference);
                break;
            }
        }
        
        return dep;
    }
    
    /** Convenience method used in unit tests. */
    void setComponentManager(ComponentManager compMgr)
    {
        mComponentManager = compMgr;
    }
    
    /** Make sure that consumer and provider are cool with exchange. */
    RegisteredEndpoint matchConsumerAndProvider(String consumerId,
        RegisteredEndpoint[] endpoints, MessageExchangeProxy exchange)
        throws javax.jbi.messaging.MessagingException
    {
        RegisteredEndpoint  match = null;
        Component           consumer;
        Component           provider;
        
        consumer = mComponentManager.getComponentInstance(consumerId);        
        
        if (endpoints.length == 1)
        {
            // No room to be picky
            match = endpoints[0];
        }
        else
        {            
            try
            {
                for (int i = 0; i < endpoints.length; i++)
                {
                    provider = mComponentManager.getComponentInstance(
                        endpoints[i].getOwnerId());
                    exchange.beforeCapabilityCheck(endpoints[i]);
                    
                    // make sure provider and consumer are cool with values
                    if (consumer.isExchangeWithProviderOkay(endpoints[i], exchange) &&
                        provider.isExchangeWithConsumerOkay(endpoints[i], exchange))
                    {                    
                        match = endpoints[i];
                        break;
                    }
                }
            }
            finally
            {
                exchange.afterCapabilityCheck();
            }
        }
        
        if (match == null)
        {            
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.CAPABILITY_NO_MATCH));
        }
        
        return match;
    }    
    
    //-----------------------------MessageServiceMBean--------------------------------
    
    /** Returns the total number of DeliveryChannels that have been activated
     *  in the NMR.
     *  @return number of active DeliveryChannels
     */
    public int getActiveChannelCount()
    {
        return mChannels.size();
    }
    
    /** Returns the identifiers of all the active channels.
     *  @return names of all the active channels.
     */
    public String[] getActiveChannels()
    {
        return (String[])mChannels.keySet().toArray(new String[0]);
    }
    
    /** Returns a list of component IDs corresponding to active channels in the NMR.
     *  @return list of component IDs
     */
    public int getActiveEndpointCount()
    {
        return mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL);
    }
    
    /** Returns a list of active endpoints in the NMR.  
     *  @return list of activated endpoints
     */
    public String[] getActiveEndpoints()
    {
        return (getEndpointNames());
    }
    
    /** Returns a list of active consuming endpoints in the NMR.  
     *  @return list of activated consuming endpoints
     */
    public String[] getActiveConsumingEndpoints()
    {
        return (getConsumingEndpointNames());
    }
    
    /** Identical to getActiveEndpoints(), but list is limited to endpoints 
     *  registered by the specified component.
     *  @param ownerId component identifier
     *  @return list of activated endpoints
     */
    public String[] getActiveEndpoints(String ownerId)
    {
        DeliveryChannelImpl     dc = (DeliveryChannelImpl)this.getChannel(ownerId);
        
        return (dc.getEndpointNames());
    }
    
    /** Identical to getActiveEndpoints(), but list is limited to endpoints 
     *  registered by the specified component.
     *  @param ownerId component identifier
     *  @return list of activated endpoints
     */
    public String[] getActiveConsumingEndpoints(String ownerId)
    {
        DeliveryChannelImpl     dc = (DeliveryChannelImpl)this.getChannel(ownerId);
        
        return (dc.getConsumingEndpointNames());
     }
    
    /** Provides metadata query facility for endpoints registered with the NMR.
     *  This method returns the contents of an XML descriptor as a string.
     *  @param service string representation of service QName
     *  @param endpoint endpoint name
     *  @return XML descriptor as string
     */
    public String getDescriptor(String service, String endpoint)
    {
        RegisteredEndpoint      re;
        Document                desc;
        String                  descStr = null;
        
        re = mRegistry.getInternalEndpoint(new QName(service), endpoint);
        
        try
        {
            desc = queryDescriptor(re);

            if (desc != null)
            {
                descStr = XMLUtil.getInstance().asString(desc);
            }
        }
        catch (javax.jbi.messaging.MessagingException msgEx)
        {
            mLog.warning(msgEx.getMessage());
        }
        
        return descStr;
    }

    /**
     * Dump the state of the MessageService to the log.
     */
    public void dumpState()
    {
        mLog.info(toString());
    }
    
    void setEndpointListener(EndpointListener listener)
    {
        mListener = listener;
    }
    
    EndpointListener getEndpointListener()
    {
        return (mListener);
    }
    
    void setTimeoutListener(DeliveryChannel dc, TimeoutListener timeout)
    {
        mTimeout = timeout;
        mTimeoutDC = dc;
    }

    void setExchangeIdGenerator(ExchangeIdGenerator generator)
    {
        mGenerator = generator;
    }
    
    String generateNextId()
    {
        return (mGenerator.nextId());
    }
    
    boolean isExchangeOkay(MessageExchange me)
    {
        Component       provider;
        
        provider = mComponentManager.getComponentInstance(((RegisteredEndpoint)((MessageExchangeProxy)me).getActualEndpoint()).getOwnerId());
        return (provider.isExchangeWithConsumerOkay(me.getEndpoint(), me));

    }
 
    /**
     * Add a component as an observer of the NMR
     *
     * @param channel - The Delivery Channel of the observer component.
     */
    synchronized public void addObserver(DeliveryChannel channel) 
    {
        HashMap newObserver = (HashMap)mObserver.clone();
        newObserver.put(channel, channel);
        mObserver = newObserver;
     }

    /**
     * Remove a component as an observer of the NMR
     *
     * @param channel
     */
    synchronized public void removeObserver(DeliveryChannel channel) 
    {
        HashMap newObserver = (HashMap)mObserver.clone();
        newObserver.remove(channel);
        mObserver = newObserver;
    }

    void updateStatistics(MessageExchangeProxy mep)
    {
        int             phaseMask = mep.getPhaseMask();        
        METimestamps    ts = mep.getTimestamps();
        

        synchronized  (this)
        {
            if ((phaseMask & MessageExchangeProxy.PM_SEND_REQUEST) != 0)
            {
                mSendRequest++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_SEND_REPLY) != 0)
            {
                mSendReply++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_SEND_FAULT) != 0)
            {
                mSendFault++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_SEND_DONE) != 0)
            {
                mSendDONE++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_SEND_ERROR) != 0)
            {
                mSendERROR++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_REQUEST) != 0)
            {
                mReceiveRequest++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_REPLY) != 0)
            {
                mReceiveReply++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_FAULT) != 0)
            {
                mReceiveFault++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_DONE) != 0)
            {
                mReceiveDONE++;
            }
            if ((phaseMask & MessageExchangeProxy.PM_RECEIVE_ERROR) != 0)
            {
                mReceiveERROR++;
            }
            if (ts != null)
            {
                mResponseTime.addSample(ts.mResponseTime);
                mNMRTime.addSample(ts.mNMRTime);
                mProviderTime.addSample(ts.mProviderTime);
                mChannelTime.addSample(ts.mProviderChannelTime);                
                mChannelTime.addSample(ts.mConsumerChannelTime);   
                if (ts.mConsumerTime != 0)
                {
                    mConsumerTime.addSample(ts.mConsumerTime);
                }
                if (ts.mStatusTime != 0)
                {
                    mStatusTime.addSample(ts.mStatusTime);
                }
            }
            if (mep instanceof InOnlyImpl)
            {
                mInOnlyMEPs++;
            }
            else if (mep instanceof InOutImpl)
            {
                mInOutMEPs++;

            }
            else if (mep instanceof RobustInOnlyImpl)
            {
                mRobustInOnlyMEPs++;

            }
            else if (mep instanceof InOptionalOutImpl)
            {
                mInOptionalOutMEPs++;
            }
        }
       
     }
 
    //-----------------------------NMRStatistics--------------------------------
    
    synchronized public void enableStatistics()
    {
        mMonitoringEnabled = true;
    }
    
    synchronized public void disableStatistics()
    {
        mMonitoringEnabled = false;

    }    
    synchronized public boolean areStatisticsEnabled()
    {
        return (mMonitoringEnabled);
    }
    
    synchronized public void zeroStatistics()
    {
        mSendRequest = 0;
        mReceiveRequest = 0;
        mSendReply = 0;
        mReceiveReply = 0;
        mSendFault = 0;
        mReceiveFault = 0;
        mSendStatus = 0;
        mReceiveStatus = 0;
        mSendDONE = 0;
        mReceiveDONE = 0;
        mSendERROR = 0;
        mReceiveERROR = 0;
        mInOutMEPs = 0;
        mInOnlyMEPs = 0;
        mRobustInOnlyMEPs = 0;
        mInOptionalOutMEPs = 0;
        mResponseTime.zero();
        mStatusTime.zero();
        mNMRTime.zero();
        mConsumerTime.zero();
        mProviderTime.zero();
        mChannelTime.zero();
        
       for (DeliveryChannelImpl d : ((Hashtable<String,DeliveryChannelImpl>)mChannels.clone()).values())
       {
            d.zeroStatistics();
       }
       mRegistry.zeroStatistics();
    }
        
    public String[] getChannelNames()
    {
        return (getActiveChannels());
    }
 
    public ChannelStatistics  getChannelStatistics(String name)
    {
        return (getChannel(name));
    }
        
    public String[] getEndpointNames()
    {
        ServiceEndpoint[] endpoints;
        ServiceEndpoint[] lendpoints;
        String[]          list;
        
        endpoints   = mRegistry.listEndpoints(RegisteredEndpoint.INTERNAL);
        lendpoints   = mRegistry.listEndpoints(RegisteredEndpoint.LINKED);
        list        = new String[endpoints.length + lendpoints.length];
        
        for (int i = 0; i < endpoints.length; i++)
        {
            list[i] = ((RegisteredEndpoint)endpoints[i]).toExternalName();
        }
        for (int i = 0; i < lendpoints.length; i++)
        {
            list[endpoints.length + i] = ((RegisteredEndpoint)lendpoints[i]).toExternalName();
        }
        
        return list;
    }
    
    public String[] getConsumingEndpointNames()
    {
        ServiceEndpoint[] endpoints;
        String[]          list;
        
        endpoints   = mRegistry.listEndpoints(RegisteredEndpoint.LINKED);
        list        = new String[endpoints.length];
        
        for (int i = 0; i < list.length; i++)
        {
            list[i] = ((RegisteredEndpoint)endpoints[i]).toExternalName();
        }
        
        return list;
    }
    
    public EndpointStatistics  getEndpointStatistics(String name)
    {
        EndpointStatistics  es;
        
        es = mRegistry.getLinkedEndpointByName(name);
        if (es == null)
        {
            es = mRegistry.getInternalEndpointByName(name);
        }
        return (es);
    }
         
   /**
     * List of item names for CompositeData construction.
     */
    private static final int ITEMS_BASE = 16;
    private static final int ITEMS_EXTRA = 24;
    private static final String[] ITEM_NAMES = {
        "SendRequest",
        "ReceiveRequest",
        "SendReply",
        "ReceiveReply",
        "SendFault",
        "ReceiveFault",
        "SendDONE",
        "ReceiveDONE",
        "SendERROR",
        "ReceiveERROR",
        "InOnlyMEPs",
        "RobustInOnlyMEPs",
        "InOutMEPs",
        "InOptionalOutMEPs",
        "DeliveryChannels",
        "Endpoints",
        "ResponseTimeMin (ns)",
        "ResponseTimeAvg (ns)",
        "ResponseTimeMax (ns)",
        "ResponseTimeStd (ns)",
        "StatusTimeMin (ns)",
        "StatusTimeAvg (ns)",
        "StatusTimeMax (ns)",
        "StatusTimeStd (ns)",
        "NMRTimeMin (ns)",
        "NMRTimeAvg (ns)",
        "NMRTimeMax (ns)",
        "NMRTimeStd (ns)",
        "ConsumerTimeMin (ns)",
        "ConsumerTimeAvg (ns)",
        "ConsumerTimeMax (ns)",
        "ConsumerTimeStd (ns)",
        "ProviderTimeMin (ns)",
        "ProviderTimeAvg (ns)",
        "ProviderTimeMax (ns)",
        "ProviderTimeStd (ns)",
        "ChannelTimeMin (ns)",
        "ChannelTimeAvg (ns)",
        "ChannelTimeMax (ns)",
        "ChannelTimeStd (ns)"
    };

    /**
     * List of descriptions of items for ComponsiteData construction.
     */
    private static final String ITEM_DESCRIPTIONS[] = {
        "Number of requests sent",
        "Number of requests received",
        "Number of replies sent",
        "Number of replies received",
        "Number of faults sent",
        "Number of faults received",
        "Number of DONE requests sent",
        "Number of DONE requests received",
        "Number of ERROR requests sent",
        "Number of ERROR requests received",
        "Number of InOnly MEP's",
        "Number of RobustInOnly MEP's",
        "Number of InOut MEP's",
        "Number of InOptionalOut MEP's",
        "Number of DeliveryChannels",
        "Number of Endpoints",
        "Response Time Min",
        "Response Time Avg",
        "Response Time Max",
        "Response Time Std",
        "Status Time Min",
        "Status Time Avg",
        "Status Time Max",
        "Status Time Std",
        "NMR Time Min",
        "NMR Time Avg",
        "NMR Time Max",
        "NMR Time Std",
        "Consumer Time Min",
        "Consumer Time Avg",
        "Consumer Time Max",
        "Consumer Time Std",
        "Provider Time Min",
        "Provider Time Avg",
        "Provider Time Max",
        "Provider Time Std",
        "Channel Time Min",
        "Channel Time Avg",
        "Channel Time Max",
        "Channel Time Std"
    };

    /**
     * List of types of items for CompositeData construction.
     */
    private static final OpenType ITEM_TYPES[] = {
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG,
         SimpleType.LONG
    };
        
    public CompositeData        getStatistics()
    {
        try
        {
            Object      values[];
            String      names[];
            String      descs[];
            OpenType    types[];
            boolean     enabled = // areStatisticsEnabled() ||
                                    mChannelTime.getCount() != 0;
            
            if (enabled)
            {
                values = new Object[ITEMS_BASE + ITEMS_EXTRA];
                types = ITEM_TYPES;
                names = ITEM_NAMES;
                descs = ITEM_DESCRIPTIONS;
            }
            else
            {
                values = new Object[ITEMS_BASE];
                types = new OpenType[ITEMS_BASE];
                System.arraycopy(ITEM_TYPES, 0, types, 0, ITEMS_BASE);
                names = new String[ITEMS_BASE];
                System.arraycopy(ITEM_NAMES, 0, names, 0, ITEMS_BASE);
                descs = new String[ITEMS_BASE];
                System.arraycopy(ITEM_DESCRIPTIONS, 0, descs, 0, ITEMS_BASE);
            }
            
            
            values[0] = mSendRequest;
            values[1] = mReceiveRequest;
            values[2] = mSendReply;
            values[3] = mReceiveReply;
            values[4] = mSendDONE;
            values[5] = mReceiveDONE;
            values[6] = mSendERROR;
            values[7] = mReceiveERROR;
            values[8] = mSendDONE;
            values[9] = mReceiveDONE;
            values[10] = mInOnlyMEPs;
            values[11] = mRobustInOnlyMEPs;
            values[12] = mInOutMEPs;
            values[13] = mInOptionalOutMEPs;
            values[14] = (long)mChannels.size();
            values[15] = (long)mRegistry.countEndpoints(RegisteredEndpoint.INTERNAL);
            if (enabled)
            {
                values[16] = mResponseTime.getMin();
                values[17] = (long)mResponseTime.getAverage();
                values[18] = mResponseTime.getMax();
                values[19] = (long)mResponseTime.getSd();
                values[20] = mStatusTime.getMin();
                values[21] = (long)mStatusTime.getAverage();
                values[22] = mStatusTime.getMax();
                values[23] = (long)mStatusTime.getSd();
                values[24] = mNMRTime.getMin();
                values[25] = (long)mNMRTime.getAverage();
                values[26] = mNMRTime.getMax();
                values[27] = (long)mNMRTime.getSd();
                values[28] = mConsumerTime.getMin();
                values[29] = (long)mConsumerTime.getAverage();
                values[30] = mConsumerTime.getMax();
                values[31] = (long)mConsumerTime.getSd();
                values[32] = mProviderTime.getMin();
                values[33] = (long)mProviderTime.getAverage();
                values[34] = mProviderTime.getMax();
                values[35] = (long)mProviderTime.getSd();
                values[36] = mChannelTime.getMin();
                values[37] = (long)mChannelTime.getAverage();
                values[38] = mChannelTime.getMax();
                values[39] = (long)mChannelTime.getSd();
            }

            return new CompositeDataSupport(
                new CompositeType(
                    "EndpointStatistics",
                    "Endpoint statistics",
                    names, descs, types), names, values);
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            System.out.println(odEx.toString()); // ignore this for now
        }
        return (null);
    }
    
    //-------------------------------Object-------------------------------------
   
    public String toString()
    {
        StringBuilder       sb = new StringBuilder();
        
        sb.append("\nMessage Service Status\n");
        sb.append("  InOnlyMEPs:       " + mInOnlyMEPs);
        sb.append("  InOutMEPs:         " + mInOutMEPs);
        sb.append("\n  RobustInOnlyMEPs: " + mRobustInOnlyMEPs);
        sb.append("  InOptionalOutMEPs: " + mInOptionalOutMEPs);
        sb.append("\n  SendReply: " + mSendReply);
        sb.append("  RecvReply: " + mReceiveReply);
        sb.append("\n  SendDONE: " + mSendDONE);
        sb.append("  RecvDONE: " + mReceiveDONE);
        sb.append("  SendERROR: " + mSendERROR);
        sb.append("  RecvERROR: " + mReceiveERROR);
        sb.append("\n  SendStatus: " + mSendStatus);
        sb.append("  RecvStatus: " + mReceiveStatus);
        sb.append("  SendFault: " + mSendFault);
        sb.append("  RecvFault: " + mReceiveFault);
        if (mChannelTime.getCount() != 0)
        {
            sb.append("\n   ResponseTime:  " + mResponseTime.toString());
            sb.append("\n   StatusTime:    " + mStatusTime.toString());
            sb.append("\n   ConsumerTime: " + mConsumerTime.toString());
            sb.append("\n   ProviderTime: " + mProviderTime.toString());
            sb.append("\n   ChannelTime:   " + mChannelTime.toString());
            sb.append("\n   NMRTime:       " + mNMRTime.toString());
        }
        sb.append("\n  Delivery Channel Count: ");
        sb.append(mChannels.size());
        sb.append("\n");
        
        //
        //  Compute a quick summary of MEP's showing participants,
        //  owner of the ME, and the service and operation.
        //
        SortedMap<String,String> sm = new TreeMap();
        for (Iterator i = mChannels.values().iterator(); i.hasNext(); )
        {
            ((DeliveryChannelImpl)i.next()).activeSummary(sm);
        }
        if (sm.size() != 0)
        {
            sb.append("  Active Exchange Summary Count: " + sm.size() + "\n");
            for (Map.Entry<String,String> e : sm.entrySet())
            {
                sb.append("    " + e.getKey() + "\n      " + e.getValue() + "\n");
            }
        }
        
        for (Iterator i = mChannels.values().iterator(); i.hasNext(); )
        {
            sb.append(i.next().toString());
        }
        sb.append(mRegistry.toString());
        return (sb.toString());
    }
    
        
    /**
     * Register as a listener for attribute change events
     */
    private void startListeningToConfigurationMBean()
    {
        javax.management.MBeanServer mbeanServer =
                mEnvironmentContext.getMBeanServer();
         
        /**
         * Create a AttributeChangeNotificationFilter 
         */
        javax.management.AttributeChangeNotificationFilter filter =
                new javax.management.AttributeChangeNotificationFilter();
        filter.disableAllAttributes();
        filter.enableAttribute(com.sun.jbi.management.config.
                SystemConfigurationFactory.MSG_SVC_STATS_ENABLED);
        
        try
        {
            mbeanServer.addNotificationListener(
                MBeanHelper.getSystemConfigMBeanName(), 
                mStatistics, filter, null);
        }
        catch( Exception ex )
        {
            mLog.fine("Exception occured in startListeningToConfigurationMBean: " + ex);
        }
    }
    
        
    /**
     * Stop listening to attribute change events
     */
    private void stopListeningToConfigurationMBean()
    {
        javax.management.MBeanServer mbeanServer =
                mEnvironmentContext.getMBeanServer();
        
        
        try
        {
            mbeanServer.removeNotificationListener(
                MBeanHelper.getSystemConfigMBeanName(), mStatistics);
        }
        catch( Exception ex )
        {
            mLog.fine("Exception occured in stopListeningToConfigurationMBean: " + ex);
        }
    }
    
    /**
     * Get the value of a configuration attribute
     *
     * @param category - configuration category the attribute is defined in
     * @param attrName - name of the attribute
     * @return the value of the requested configuration attribute
     */
    private boolean isMsgTimingStatsEnabledByDefault()
    {     
        ObjectName configMBeanName = MBeanHelper.getSystemConfigMBeanName();
        javax.management.MBeanServer mbeanServer =
                mEnvironmentContext.getMBeanServer();
        
        Boolean isEnabled = new Boolean(false);
        String attrName = com.sun.jbi.management.config.
                SystemConfigurationFactory.MSG_SVC_STATS_ENABLED;
        try
        {
            if ( mbeanServer.isRegistered(configMBeanName) )
            {
                isEnabled = (Boolean) mbeanServer.getAttribute(
                        configMBeanName, attrName);
            }
        }
        catch ( javax.management.JMException jmex)
        {
            mLog.fine("Exception occured in isMsgTimingStatsEnabledByDefault: " + jmex);
        }
        return isEnabled.booleanValue();
    }
}

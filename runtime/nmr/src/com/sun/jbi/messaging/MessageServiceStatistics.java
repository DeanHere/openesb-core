/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageServiceStatistics.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.util.Date;

import javax.management.AttributeChangeNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.openmbean.CompositeData;

import javax.xml.namespace.QName;

import com.sun.jbi.monitoring.StatisticsBase;
import com.sun.jbi.management.config.SystemConfigurationFactory;

/**
 * This class implements the MBean for collection of statistics for the
 * messaging service. All statistics are since the last message service startup;
 * they are all reset when the message service is restarted.
 *
 * @author Sun Microsystems, Inc.
 */
public class MessageServiceStatistics
    implements MessageServiceStatisticsMBean, NotificationListener
{
    private MessageService      mMsgSvc;
    
    /**
     * Instance of StatisticsBase for manipulating the object tree.
     */
    private StatisticsBase mStatisticsBase;

    /**
     * Time the message service was last successfully started.
     */
    private Date mLastRestartTime;

    /**
     * Count of current number of services registered with the message service.
     */
    private int mRegisteredServices;

    /**
     * Count of current number of endpoints registered with the message service.
     */
    private int mRegisteredEndpoints;

    /**
     * Instance of MessagingStatistics for holding messaging statistics.
     */
    private MessagingStatistics mMessagingStatistics;

    /**
     * Constant used to compute percentages.
     */
    private static final int ONE_HUNDRED = 100;

    /**
     * Constant used to compute rates.
     */
    private static final int MILLISECONDS_PER_HOUR = 3600000;


    /**
     * Constructor to create the StatisticsBase and MessagingStatistics
     * instances.
     * @param key the string value to use as the key for this statistics
     * instance.
     */
    MessageServiceStatistics(MessageService msgSvc, String key)
    {
        mMsgSvc = msgSvc;
        mStatisticsBase =
            new com.sun.jbi.util.monitoring.StatisticsBaseImpl(key);
        mMessagingStatistics = new MessagingStatistics();
    }

    //
    // Methods defined in StatisticsMBean must delegate to StatisticsBase.
    //

    /**
     * Disable statistics collection. This method causes collection for this
     * object and all its child objects to be disabled.
     */
    public void setDisabled()
    {
         mMsgSvc.disableStatistics();
    }
    public void disableTimingStatistics()
    {
        mMsgSvc.disableStatistics();
    }

    /**
     * Enable statistics collection. This method causes collection for this
     * object and all its child objects to be enabled.
     */
    public boolean isEnabled()
    {
        return (mMsgSvc.areStatisticsEnabled());
    }
    public void setEnabled()
    {
        mMsgSvc.enableStatistics();
    }
    public void enableTimingStatistics()
    {
        mMsgSvc.enableStatistics();
    }

    //
    // Methods defined in the MessageServiceStatisticsMBean interface. These
    // methods provide all the MBean attributes visible to JMX clients.
    //

    /**
     * Get the time that the message service was last started.
     * @return The time of the last successful start() call.
     */
    public Date getLastRestartTime()
    {
        return mLastRestartTime;
    }

    /**
     * Get the current number of services registered with the message service.
     * @return The total number of registered services.
     */
    public int getRegisteredServices()
    {
        return mRegisteredServices;
    }

    /**
     * Get the current number of endpoints registered with the message service.
     * @return The total number of registered endpoints.
     */
    public int getRegisteredEndpoints()
    {
        return mRegisteredEndpoints;
    }

    /** Returns the identifiers of all the active channels.
     *  @return names of all the active channels.
     */
    public String[] getActiveChannels()
    {
        return (mMsgSvc.getChannelNames());
    }
       
    /** Returns a list of active endpoints in the NMR.  
     *  @return list of activated endpoints
     */
    public String[] getActiveEndpoints()
    {
        return (mMsgSvc.getEndpointNames());
    }
    
    /**
     * Get the list of endpoints for a specific DeliveryChannel.
     * @return The delivery channel statistics in a CompositedData instance
     */
    public String[]  getEndpointsForDeliveryChannel(String dcName)
    {
        return (mMsgSvc.getActiveEndpoints(dcName));
    }
    
    /**
     * Get the list of consuming endpoints for a specific DeliveryChannel.
     * @return The delivery channel statistics in a CompositedData instance
     */
    public String[]  getConsumingEndpointsForDeliveryChannel(String dcName)
    {
        return (mMsgSvc.getActiveConsumingEndpoints(dcName));
    }
    
    /**
     * Get the CompositeData instance that represents the current values for
     * the MessagingStatistics instance.
     * @return The messaging statistics in a CompositedData instance
     */
    public CompositeData getMessagingStatistics()
    {
        CompositeData mscd = null;
        try
        {
            mscd = mMessagingStatistics.toCompositeData();
        }
        catch ( javax.management.openmbean.OpenDataException odEx )
        {
            odEx.printStackTrace();
        }
        return mscd;
    }

    /**
     * Get the CompositeData instance that represents the current values for
     * the specific DeliveryChannel.
     * @return The delivery channel statistics in a CompositedData instance
     */
    public CompositeData getDeliveryChannelStatistics(String dcName)
    {
        CompositeData mscd = null;
        ChannelStatistics       cs = mMsgSvc.getChannelStatistics(dcName);

        if (cs != null)
        {
            mscd = cs.getStatistics();
        }
        return mscd;
    }

     /**
     * Get the CompositeData instance that represents the current values for
     * the specific Endpoint.
     * @return The endpoint statistics in a CompositedData instance
     */
    public CompositeData getEndpointStatistics(String epName)
    {
        CompositeData mscd = null;
        EndpointStatistics       es = mMsgSvc.getEndpointStatistics(epName);

        if (es != null)
        {
            mscd = es.getStatistics();
        }
        return mscd;
     }
        
    //
    // Methods used only within the message service code to set statistics
    // values.
    //

    /**
     * Get the MessagingStatistics instance.
     * @return the MessagingStatisticsBase instance for this object.
     */
    MessagingStatistics getMessagingStatisticsInstance()
    {
        return mMessagingStatistics;
    }

    /**
     * Get the StatisticsBase instance.
     * @return the StatisticsBase instance for this object.
     */
    StatisticsBase getStatisticsBase()
    {
        return mStatisticsBase;
    }

    /**
     * Set the time that this component was last started.
     * @param startTime The time of the last successful start() call.
     */
    void setLastRestartTime(Date startTime)
    {
        mLastRestartTime = startTime;
        mMessagingStatistics.setLastRestartTime(startTime);
    }

    /**
     * Decrement the current number of endpoints registered with the message
     * service.
     */
    void decrementRegisteredEndpoints()
    {
        --mRegisteredEndpoints;
    }

    /**
     * Increment the current number of endpoints registered with the message
     * service.
     */
    void incrementRegisteredEndpoints()
    {
        ++mRegisteredEndpoints;
    }

    /**
     * Decrement the current number of services registered with the message
     * service.
     */
    void decrementRegisteredServices()
    {
        --mRegisteredServices;
    }

    /**
     * Increment the current number of services registered with the message
     * service.
     */
    void incrementRegisteredServices()
    {
        ++mRegisteredServices;
    }

    /**
     * Reset all statistics.
     */
    public void resetStatistics()
    {
        mRegisteredServices = 0;
        mRegisteredEndpoints = 0;
        mMessagingStatistics.resetStatistics();
        if (mMsgSvc != null)
        {
            mMsgSvc.zeroStatistics();
        }
    }
    
        
    //
    // Notification Listener Interface
    //
    
    /**
     *  handle a notification from the Logger Configuration MBean
     *
     * @param notification - the notification 
     * @param the callback passed in 
     */
    public void handleNotification(Notification notification, Object handback)
    {
        if ( notification instanceof AttributeChangeNotification )
        {
            AttributeChangeNotification notif = 
                    (AttributeChangeNotification) notification;
           
            Boolean enableTimingStatsStr = (Boolean) notif.getNewValue();
            boolean enableTimingStats = enableTimingStatsStr.booleanValue();
            
            if ( enableTimingStats  )
            {
                enableTimingStatistics();
            }
            else
            {
                disableTimingStatistics();
            }
        }
    }
}

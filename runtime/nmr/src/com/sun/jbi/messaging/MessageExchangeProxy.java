/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageExchangeProxy.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import com.sun.jbi.messaging.stats.METimestamps;

import com.sun.jbi.messaging.util.Translator;

import java.net.URI;

import java.util.HashMap;
import java.util.Set;

import java.util.logging.Logger;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;


import javax.xml.namespace.QName;
//import org.openesb.runtime.tracking.MessageExchangeTracker;
//import org.openesb.runtime.tracking.MessageTrackingInfo;

/** 
 *  This abstract class is used as the base for a MessageExchange reference 
 *  given to a Binding or Engine. The binding and engine are given different
 *  instances, because the states naviagated during execution are different.
 *  Each instance also tracks its twin.
 *
 *  It's mostly just a proxy to the underlying MessageExchangeImpl. The proxy
 *  tracks the status of the reference to determine which operations are legal
 *  at any point in time. Legal operations are forwarded to the underlying
 *  MessageExchangeImpl.
 *
 *  Each subclass is expected to specify a state machine that will be used
 *  by this proxy to sequence the actions. The state machine is very simple.
 *  There are only 4 actions: SEND, ACCEPT, SET_STATUS, SET_FAULT.
 *  Along with each state there is a set (implemented as a bit-mask) of 
 *  operations that are legal in the current state. The structure is
 *  represented as a simple [][]. The first dimension is the array of 
 *  states, the second dimension contains the valid set, followed by the
 *  next array index for each action. An index of -1 means an illegal
 *  request (the code in here should only request legal transisions.)
 *
 *  Each subclass implements 2 state machines, one for the SOURCE side and
 *  one for the TARGET side of a message exchange.
 *
 * @author Sun Microsystems, Inc.
 */
public abstract class MessageExchangeProxy
        implements com.sun.jbi.messaging.MessageExchange {

    /*********************************************
     *      State machine operations             *
     ********************************************/
    /** Allowed to set transaction. */
    public static final int SET_TRANSACTION = 0x0000001;
    /** Category of actions used to address a message exchange:
     *  SET_SERVICE, SET_OPERATION, SET_ENDPOINT, SET_INTERFACE
     */
    public static final int ADDRESS = 0x0000002;
    /** Allowed to set property. */
    public static final int SET_PROPERTY = 0x0000010;
    /** Allowed to set fault. */
    public static final int SET_FAULT = 0x0000020;
    /** Allowed to set done status. */
    public static final int SET_DONE = 0x0000040;
    /** Allowed to set error status. */
    public static final int SET_ERROR = 0x0000080;
    /** Allowed to set IN message */
    public static final int SET_IN = 0x0001000;
    /** Allowed to set OUT message */
    public static final int SET_OUT = 0x0002000;
    /** Allowed to create fault. */
    public static final int CREATE_FAULT = 0x0004000;
    /** Allowed to SENDSYNCH. */
    public static final int DO_SENDSYNCH = 0x0008000;
    /** Allowed to SEND */
    public static final int DO_SEND = 0x0010000;
    /** Allowed to ACCEPT */
    public static final int DO_ACCEPT = 0x0020000;
    /** Hint SUSPEND_TX */
    public static final int SUSPEND_TX = 0x0100000;
    /** Hint RESUME_TX */
    public static final int RESUME_TX = 0x0200000;
    /** Hint DONE */
    public static final int MARK_DONE = 0x0400000;
    /** Hint ACTIVE */
    public static final int MARK_ACTIVE = 0x0800000;
    /** Hint COMPLETE */
    public static final int COMPLETE = 0x1000000;
    /** Hint check STATUS or FAULT */
    public static final int CHECK_STATUS_OR_FAULT = 0x2000000;
    /** Hint is a REQUEST. */
    public static final int REQUEST = 0x4000000;
    /** Hint is a STATUS. */
    public static final int STATUS = 0x8000000;
    /*********************************************
     *      State machine actions                *
     ********************************************/
    /** Column 1: Legal operation MASK. */
    public static final int ACTION_MASK = 0;
    /** Column 2: Action SEND	*/
    public static final int ACTION_SEND = 1;
    /** Column 3: Action ACCEPT */
    public static final int ACTION_ACCEPT = 2;
    /** Column 4: Action STATUS */
    public static final int ACTION_STATUS = 3;
    /** Column 5: Action FAULT */
    public static final int ACTION_FAULT = 4;
    /*********************************************
     *      Message names                        *
     ********************************************/
    /** Name of IN message. */
    static final String IN_MSG = new String("in");
    /** Name of OUT message. */
    static final String OUT_MSG = new String("out");
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
    /**
     * The underlying MessageExchangeImpl that we are proxying
     */
    private MessageExchangeImpl mMEI;
    /**
     * The state machine used to control this exchange.
     */
    private int[][] mState;
    /**
     * The current index into the state machine.
     */
    private int mStateIndex;
    /**
     * Our twin MessageExchangeProxy.
     */
    private MessageExchangeProxy mTwin;
    /**
     * Delivery channel for our twins side.
     */
    private DeliveryChannelImpl mSendChannel;
    /**
     * Linked endpoint enabled through a service connection.  This endpoint
     * is only visible to the service consumer; the provider sees the 
     * actual endpoint that was activated.
     */
    private ServiceEndpoint mEndpointLink;
    private boolean mStatisticsEnabled;
    /**
     * Synchronous support.
     */
    private int mSynchState;
    static final int NONE = 0;
    static final int WAIT = 1;
    static final int WAIT_TIMEOUT = 2;
    static final int HALF_DONE = 3;
    static final int DONE = 4;
    static final int ERROR = 5;
    /**
     * Messaging phase (used for statistics routing.)
     */
    private int mPhase;
    static final int PHASE_NONE = 0;
    static final int PHASE_DONE = 1;
    static final int PHASE_ERROR = 3;
    static final int PHASE_REQUEST = 4;
    static final int PHASE_REPLY = 5;
    static final int PHASE_FAULT = 6;
    private int mPhaseMask;
    static final int PM_SEND_DONE = 0x0001;
    static final int PM_SEND_ERROR = 0x0002;
    static final int PM_SEND_REQUEST = 0x0004;
    static final int PM_SEND_REPLY = 0x0008;
    static final int PM_SEND_FAULT = 0x0010;
    static final int PM_RECEIVE_DONE = 0x0100;
    static final int PM_RECEIVE_ERROR = 0x0200;
    static final int PM_RECEIVE_REQUEST = 0x0400;
    static final int PM_RECEIVE_REPLY = 0x0800;
    static final int PM_RECEIVE_FAULT = 0x1000;

    /**
     * Constructor.
     */
    MessageExchangeProxy(int[][] states) {
        mState = states;
        mStateIndex = 0;
        mSynchState = NONE;
    }

    /**
     * Create a twin of ourselves in the target role.
     */
    abstract MessageExchangeProxy newTwin();

    /**
     * Get ExchangeId associated with this MessageExchange.
     */
    public String getExchangeId() {
        return (mMEI.getExchangeId());
    }

    /**
     * Get status of this MessageExchange.
     */
    public ExchangeStatus getStatus() {
        return (mMEI.getStatus());
    }

    /**
     * Set status of this MessageExchange.
     */
    public void setStatus(ExchangeStatus status)
            throws javax.jbi.messaging.MessagingException {
        boolean isDone = status.equals(ExchangeStatus.DONE);
        boolean isError = status.equals(ExchangeStatus.ERROR);

        if ((isDone && can(SET_DONE)) || (isError && can(SET_ERROR))) {
            if (isDone) {
                if (can(SET_OUT) && getMessage(OUT_MSG) != null) {
                    throw new javax.jbi.messaging.MessagingException(
                            Translator.translate(LocalStringKeys.STATUS_ON_MSG, this.getPattern().toString()));
                }
                if (mMEI.getStatus().equals(ExchangeStatus.ERROR)) {
                    throw new javax.jbi.messaging.MessagingException(
                            Translator.translate(LocalStringKeys.ERROR_STATUS));
                }
            } else {
                mMEI.setMessage(null, OUT_MSG);
            }
            mMEI.setStatus(status);
            mPhase = isDone ? PHASE_DONE : PHASE_ERROR;
            mPhaseMask |= (isDone ? PM_SEND_DONE : PM_SEND_ERROR);
            nextState(ACTION_STATUS);
            return;
        }

        throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.PATTERN_INCONSISTENT, this.getPattern().toString(),
                this == mMEI.getSource()
                ? Translator.translate(LocalStringKeys.SOURCE)
                : Translator.translate(LocalStringKeys.TARGET),
                Integer.valueOf(mStateIndex)));
    }

    /**
     * Get Exception describing the exchanges error status.
     */
    public Exception getError() {
        return (mMEI.getError());
    }

    /**
     * Set Exception describing reason for error status.
     */
    public void setError(Exception error) {
        mMEI.setMessage(null, OUT_MSG);
        mMEI.setStatus(ExchangeStatus.ERROR);
        mMEI.setError(error);
        mPhase = PHASE_ERROR;
        mPhaseMask |= PM_SEND_ERROR;
        nextState(ACTION_STATUS);
    }

    /**
     * Get the Fault message for this exchange.
     */
    public Fault getFault() {
        return (mMEI.getFault());
    }

    /**
     * Set Fault message for this exchange.
     */
    public void setFault(Fault fault)
            throws MessagingException {
        if (!can(SET_FAULT)) {
            throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.FAULT_NOT_SUPPORTED, this.getPattern().toString()));
        }
        if ((can(SET_IN) && getMessage(IN_MSG) != null)
                || (can(SET_OUT) && getMessage(OUT_MSG) != null)) {
            throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.FAULT_ON_MSG, this.getPattern().toString()));
        }

        mMEI.setFault(fault);
        mPhase = PHASE_FAULT;
        mPhaseMask |= PM_SEND_FAULT;
        nextState(ACTION_FAULT);
    }

    /**
     * Get the Endpoint for this exchange.  If a service connection was used
     * to address this exchange, that endpoint is returned to the consumer.  The
     * provider always sees the 'real' endpoint that it activated.  If a service
     * connection was not used, the provider and consumer see the same endpoint.
     */
    public ServiceEndpoint getEndpoint() {
        if (mEndpointLink != null
                && getRole().equals(MessageExchange.Role.CONSUMER)) {
            return mEndpointLink;
        } else {
            ServiceEndpoint se = mMEI.getEndpoint();
            RegisteredEndpoint re = (RegisteredEndpoint) se;

            if (re != null
                    && getRole().equals(MessageExchange.Role.PROVIDER)
                    && re.isDynamic()) {
                se = ((DynamicEndpoint) re).getDelegate();
            }
            return se;
        }
    }

    /**
     * Set Endpoint for this exchange.
     */
    public void setEndpoint(ServiceEndpoint endPoint) {
        if (can(ADDRESS)) {
            mMEI.setEndpoint(endPoint);
        }
    }

    public ServiceEndpoint getActualEndpoint() {
        return (mMEI.getEndpoint());
    }

    /**
     * Get the Service for this exchange.
     */
    public QName getService() {
        return (mMEI.getService());
    }

    /**
     * Set Service for this exchange.
     */
    public void setService(QName service) {
        if (can(ADDRESS)) {
            mMEI.setService(service);
        }
    }

    /**
     * Get the Service for this exchange.
     */
    public QName getOperation() {
        return (mMEI.getOperation());
    }

    /**
     * Set Operation for this exchange.
     */
    public void setOperation(QName operation) {
        if (can(ADDRESS)) {
            mMEI.setOperation(operation);
        }
    }

    public QName getInterfaceName() {
        return mMEI.getInterfaceName();
    }

    public void setInterfaceName(QName interfaceName) {
        if (can(ADDRESS)) {
            mMEI.setInterfaceName(interfaceName);
        }
    }

    /**
     * Get a NormalizedMEssage by reference.
     */
    public NormalizedMessage getMessage(String name) {
        return (mMEI.getMessage(name));
    }

    /**
     * Set a NormalizedMessage by reference.
     */
    public void setMessage(NormalizedMessage message, String name)
            throws MessagingException {
        if ((can(SET_IN) && name.equals(IN_MSG))
                || (can(SET_OUT) && name.equals(OUT_MSG))) {
            mMEI.setMessage(message, name);
            if (name.equals(IN_MSG)) {
                mPhase = PHASE_REQUEST;
                mPhaseMask |= PM_SEND_REQUEST;
            } else {
                mPhase = PHASE_REPLY;
                mPhaseMask |= PM_SEND_REPLY;
            }

            /*final String txt = Translator.translate(LocalStringKeys.TRACK_SET_NMESSAGE, name.toUpperCase(), this.getProperty(MessageExchangeTracker.TRACKING_ID));
            MessageTrackingInfo info = new MessageTrackingInfo(this, txt);
//            info.setSourceComponent(this.getSourceComponent());
//            info.setDestinationComponent(this.getTargetComponent());
            MessageExchangeTracker.track(info);*/
        } else {
            throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.PATTERN_INCONSISTENT,
                    this.getPattern().toString(),
                    this == mMEI.getSource()
                    ? Translator.translate(LocalStringKeys.SOURCE)
                    : Translator.translate(LocalStringKeys.TARGET),
                    Integer.valueOf(mStateIndex)));

        }
    }

    /**
     * Get a property.
     */
    public Object getProperty(String name) {
        return (mMEI.getProperty(name));
    }

    /**
     * Set a property.
     */
    public void setProperty(String name, Object object) {
        if (can(SET_PROPERTY)) {
            if (name.equals(JTA_TRANSACTION_PROPERTY_NAME)) {
                if (can(SET_TRANSACTION)) {
                    mMEI.setProperty(name, object);
                }
            } else {
                mMEI.setProperty(name, object);
            }
        }
    }

    public java.util.Set getPropertyNames() {
        return mMEI.getPropertyNames();
    }

    /** 
     * Return the role to take in this exchange.
     */
    public Role getRole() {
        return ((mState[0][ACTION_MASK] & DO_ACCEPT) == 0
                ? Role.CONSUMER : Role.PROVIDER);
    }

    public String getSourceComponent() {
        DeliveryChannelImpl dc = mSendChannel;

        if ((mState[0][ACTION_MASK] & DO_ACCEPT) != 0) {
            dc = mTwin.mSendChannel;
        }

        return (dc.getChannelId());
    }

    public String getTargetComponent() {
        DeliveryChannelImpl dc = mSendChannel;

        if ((mState[0][ACTION_MASK] & DO_ACCEPT) == 0) {
            dc = mTwin.mSendChannel;
        }

        return (dc.getChannelId());
    }

    /**
     * Check existence of a transaction.
     */
    public boolean isTransacted() {
        return (mMEI.isTransacted());
    }

    /**
     * Create a Fault.
     */
    public Fault createFault()
            throws MessagingException {
        if (!can(CREATE_FAULT)) {
            throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.FAULT_NOT_SUPPORTED,
                    this.getPattern().toString()));
        }
        return (new FaultImpl());
    }

    /**
     * Create a Message.
     */
    public NormalizedMessage createMessage()
            throws MessagingException {
        return (new MessageImpl());
    }

    /**
     * Set the identity of our twin.
     */
    void setTwin(MessageExchangeProxy mep) {
        mTwin = mep;
    }

    /**
     * Get the identity of our twin.
     */
    MessageExchangeProxy getTwin() {
        return (mTwin);
    }

    void setSynchState(int state) {
        mSynchState = state;
    }

    int getSynchState() {
        return (mSynchState);
    }

    public int getPhase() {
        int phase = mPhase;

        mPhase = PHASE_NONE;
        return (phase);
    }

    public int getPhaseMask() {
        return (mPhaseMask);
    }

    /**
     * Set our MessageExchange.
     */
    void setMessageExchange(MessageExchangeImpl me, boolean statsEnabled) {
        mMEI = me;
        mStatisticsEnabled = statsEnabled;
    }

    /**
     * Get our MessageExchange.
     */
    MessageExchangeImpl getMessageExchange() {
        return (mMEI);
    }

    boolean isSource() {
        return (this == mMEI.getSource());
    }

    /**
     * Validate that we can send.
     */
    void validate(DeliveryChannelImpl channel, boolean isSynch)
            throws javax.jbi.messaging.MessagingException {
        String message = null;

        //
        // Perform any first time checks.
        //
        if (mSendChannel == null) {
            ServiceEndpoint endpoint = getActualEndpoint();

            if (endpoint == null) {
                message = Translator.translate(LocalStringKeys.ADDR_NO_ENDPOINT);
            } else if (!((RegisteredEndpoint) endpoint).isActive()) {
                message = Translator.translate(LocalStringKeys.INACTIVE_ENDPOINT,
                        endpoint.getServiceName(), endpoint.getEndpointName());
            }
        }

        //
        //  Checks made every time.
        //
        if (message == null) {
            if (!can(DO_SEND)) {
                message = Translator.translate(LocalStringKeys.SEND_NOT_LEGAL, this.getPattern().toString(),
                        this == mMEI.getSource()
                        ? Translator.translate(LocalStringKeys.SOURCE)
                        : Translator.translate(LocalStringKeys.TARGET),
                        Integer.valueOf(mStateIndex));

            } else if (isSynch && !can(DO_SENDSYNCH)) {
                message = Translator.translate(LocalStringKeys.SENDSYNCH_NOT_LEGAL, this.getPattern().toString(),
                        this == mMEI.getSource()
                        ? Translator.translate(LocalStringKeys.SOURCE)
                        : Translator.translate(LocalStringKeys.TARGET),
                        Integer.valueOf(mStateIndex));
            }
        }

        //
        //  Throw if we end up here with error message.
        //
        if (message != null) {
            throw new MessagingException(message);
        }

        if (mSendChannel == null) {
            setupChannels(channel);
        }
    }

    /**
     * Setup channels between twins.
     */
    private void setupChannels(DeliveryChannelImpl dc)
            throws javax.jbi.messaging.MessagingException {
        String target;
        DeliveryChannelImpl targetDc;
        RegisteredEndpoint endpoint;
        boolean invertedTwin = false;

        endpoint = (RegisteredEndpoint) mMEI.getEndpoint();
        target = endpoint.getOwnerId();

        targetDc = dc.getChannel(target);
        if (targetDc == null) {
            throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.INVALID_DESTINATION));
        }

        bindChannels(dc, targetDc);
    }

    /**
     * Set the sendChannel.
     */
    DeliveryChannelImpl getSendChannel() {
        return (mSendChannel);
    }

    /**
     * Bind source and target channels and create target MessageExchange.
     */
    void bindChannels(DeliveryChannelImpl source, DeliveryChannelImpl target) {
        MessageExchangeProxy twin;

        twin = newTwin();
        twin.setMessageExchange(mMEI, mStatisticsEnabled);
        twin.mTwin = this;
        mTwin = twin;
        mTwin.mSendChannel = source;
        mSendChannel = target;
    }

    /**
     * Test if an Operation is legal in the current state.
     * @param operation the operation to test
     */
    private boolean can(int operation) {
        return ((mStateIndex >= 0) && ((mState[mStateIndex][ACTION_MASK] & operation) != 0));
    }

    /**
     * Force exchange into DONE state. Typically used after a sendSynch timeout
     * or the closing of the source or target channel.
     */
    public synchronized boolean terminate() {
        boolean terminated = true;

        if (can(SET_ERROR)) {
            mStateIndex = mState[mStateIndex][ACTION_STATUS];
            mMEI.setStatus(ExchangeStatus.ERROR);
            terminated = !can(DO_SEND);
        } else if (can(DO_ACCEPT)) {
            mMEI.setStatus(ExchangeStatus.ERROR);
            while (!can(MARK_DONE)) {
                if (can(DO_SEND)) {
                    terminated = false;
                    break;
                }
                mStateIndex++;
            }
        }
        while (!can(MARK_DONE)) {
            mStateIndex++;
        }
        if (mSynchState == MessageExchangeProxy.WAIT
                || mSynchState == MessageExchangeProxy.WAIT_TIMEOUT) {
            this.notify();
            terminated = true;
        }
        if (terminated) {
            mSynchState = ERROR;
        }
        return (terminated);
    }

    /**
     * Change to next state.
     */
    private void nextState(int state) {
        int newStateIndex = mState[mStateIndex][state];

        if (newStateIndex > 0) {
            mStateIndex = newStateIndex;
        } else {
            throw new IllegalStateException(
                    Translator.translate(LocalStringKeys.ILLEGAL_STATE_CHANGE, this.getPattern().toString(),
                    this == mMEI.getSource()
                    ? Translator.translate(LocalStringKeys.SOURCE)
                    : Translator.translate(LocalStringKeys.TARGET),
                    Integer.valueOf(mStateIndex)));
        }
    }

    /**
     * Handle any special processing after an accept().
     * @return boolean that signals if processing is complete.
     */
    boolean handleAccept(DeliveryChannelImpl channel)
            throws javax.jbi.messaging.MessagingException {

        /*final String txt = Translator.translate(LocalStringKeys.TRACK_ME_ACCEPTED,
                this.getProperty(MessageExchangeTracker.TRACKING_ID),
                channel.getName());
        MessageTrackingInfo info = new MessageTrackingInfo(this, txt);
        info.setDestinationComponent(this.mTwin.mSendChannel.getChannelId());
        info.setSourceComponent(this.getSendChannel().getChannelId());
        MessageExchangeTracker.track(info);*/

        mSynchState = MessageExchangeProxy.NONE;
        if (channel.isTransactional() && can(RESUME_TX)) {
            mMEI.resumeTX();
        }
        if (this.can(CHECK_STATUS_OR_FAULT)) {
            if (this.getFault() != null) {
                mPhase = PHASE_FAULT;
                mPhaseMask |= PM_RECEIVE_FAULT;
                this.nextState(ACTION_FAULT);
            } else if (!this.getStatus().equals(ExchangeStatus.ACTIVE)) {
                if (this.getStatus().equals(ExchangeStatus.DONE)) {
                    mPhase = PHASE_DONE;
                    mPhaseMask |= PM_RECEIVE_DONE;
                } else {
                    mPhase = PHASE_ERROR;
                    mPhaseMask |= PM_RECEIVE_ERROR;
                }
                this.nextState(ACTION_STATUS);
            } else {
                mPhase = PHASE_REPLY;
                mPhaseMask |= PM_RECEIVE_REPLY;
                this.nextState(ACTION_ACCEPT);
            }
        } else {
            if (can(REQUEST)) {
                mPhase = PHASE_REQUEST;
                mPhaseMask |= PM_RECEIVE_REQUEST;
            } else if (can(STATUS)) {
                if (this.getStatus().equals(ExchangeStatus.DONE)) {
                    mPhase = PHASE_DONE;
                    mPhaseMask |= PM_RECEIVE_DONE;
                } else {
                    mPhase = PHASE_ERROR;
                    mPhaseMask |= PM_RECEIVE_ERROR;
                }
            }
            this.nextState(ACTION_ACCEPT);
        }

        return (can(MARK_DONE));
    }

    void setEndpointLink(ServiceEndpoint se) {
        mEndpointLink = se;
    }

    ServiceEndpoint getEndpointLink() {
        return mEndpointLink;
    }

    /**
     * Handle any special processing after an send().
     * @return boolean that signals if processing is complete.
     */
    boolean handleSend(DeliveryChannelImpl channel)
            throws javax.jbi.messaging.MessagingException {

   /*     final String txt = Translator.translate(LocalStringKeys.TRACK_ME_SENT,
                this.getProperty(MessageExchangeTracker.TRACKING_ID),
                channel.getName());
        MessageTrackingInfo info = new MessageTrackingInfo(this, txt);
        info.setSourceComponent(this.mTwin.getSendChannel().getChannelId());
        info.setDestinationComponent(this.getSendChannel().getChannelId());
        MessageExchangeTracker.track(info);*/
        mSynchState = MessageExchangeProxy.NONE;
        if (channel.isTransactional() && can(SUSPEND_TX)) {
            mMEI.suspendTX();
        }

        this.nextState(ACTION_SEND);
        if (can(MARK_ACTIVE)) {
            mMEI.setStatus(ExchangeStatus.ACTIVE);
        }
        mMEI.setSyncProperty(false);
        return (can(MARK_DONE));
    }

    /**
     * Handle any special processing after an sendSync().
     */
    void handleSendSync(DeliveryChannelImpl channel)
            throws javax.jbi.messaging.MessagingException {

        /*final String txt = Translator.translate(LocalStringKeys.TRACK_ME_SENT_SYNC,
                this.getProperty(MessageExchangeTracker.TRACKING_ID),
                channel.getName());
        MessageTrackingInfo info = new MessageTrackingInfo(this, txt);
        info.setSourceComponent(this.mTwin.getSendChannel().getChannelId());
        info.setDestinationComponent(this.getSendChannel().getChannelId());
        MessageExchangeTracker.track(info);*/

        mSynchState = MessageExchangeProxy.NONE;
        if (channel.isTransactional() && can(SUSPEND_TX)) {
            mMEI.suspendTX();
        }

        this.nextState(ACTION_SEND);
        if (can(MARK_ACTIVE)) {
            mMEI.setStatus(ExchangeStatus.ACTIVE);
        }
        mMEI.setSyncProperty(true);
    }

    void beforeCapabilityCheck(ServiceEndpoint se) {
        mStateIndex = -1;
        mMEI.setEndpoint(se);
    }

    void afterCapabilityCheck() {
        mStateIndex = 0;
        mMEI.setEndpoint(null);
    }

    public boolean isRemoteInvocation() {
        return (mMEI.isRemote());
    }

    public boolean checkTimeout() {
        boolean timedout = false;

        synchronized (this) {
            if (mSynchState == MessageExchangeProxy.NONE) {
                mSynchState = MessageExchangeProxy.ERROR;
                timedout = true;
            }
        }
        return (timedout);
    }

    public Set getDeltaProperties() {
        return (mMEI.getDeltaProperties());
    }

    public void mergeProperties() {
        mMEI.mergeProperties();
    }

    void setInUse(String ownerId) {
        if (getRole().equals(MessageExchange.Role.CONSUMER)) {
            if (mEndpointLink != null) {
                ((LinkedEndpoint) mEndpointLink).setInUse(ownerId);
            }
        } else {
            ((RegisteredEndpoint) mMEI.getEndpoint()).setInUse();
        }
    }

    void resetInUse() {
        if (getRole().equals(MessageExchange.Role.CONSUMER)) {
            if (mEndpointLink != null) {
                ((RegisteredEndpoint) mEndpointLink).resetInUse();
            }
        } else {
            ((RegisteredEndpoint) mMEI.getEndpoint()).resetInUse();
        }
    }

    boolean capture(byte consumerTag, byte providerTag) {
        if (mStatisticsEnabled) {
            mMEI.capture((mState[0][ACTION_MASK] & DO_ACCEPT) == 0 ? consumerTag : providerTag);
        }
        return (can(COMPLETE));
    }
//        RegisteredEndpoint  re = ((RegisteredEndpoint)mep.getEndpoint());
//        if (re != null)
//        {
//            re.updateStatistics(mep);
//        }
//        re = ((RegisteredEndpoint)mep.getEndpointLink());
//        if (re != null)
//        {
//            re.updateStatistics(mep);
//        }

//                if (can(COMPLETE))
//            {
//                if ((mState[0][ACTION_MASK] & DO_ACCEPT) == 0)
//                {
//                    mPhaseMask = mTwin.mPhaseMask;
//                }
//            }
    void updateStatistics() {
        METimestamps ts = mMEI.getTimestamps();
        boolean isConsumer = (mState[0][ACTION_MASK] & DO_ACCEPT) == 0;

        if (ts != null) {
            ts.compute();
            if (isConsumer) {
                mSendChannel.updateProviderStatistics(ts);
                mTwin.mSendChannel.updateConsumerStatistics(ts);
            } else {
                mSendChannel.updateConsumerStatistics(ts);
                mTwin.mSendChannel.updateProviderStatistics(ts);
            }
        }
        if (isConsumer) {
            if (mEndpointLink != null) {
                ((RegisteredEndpoint) mEndpointLink).updateStatistics(this);
            }
            ((RegisteredEndpoint) mMEI.getEndpoint()).updateStatistics(mTwin);
        } else {
            if (mTwin.mEndpointLink != null) {
                ((RegisteredEndpoint) mTwin.mEndpointLink).updateStatistics(mTwin);
            }
            ((RegisteredEndpoint) mMEI.getEndpoint()).updateStatistics(this);
        }

        mSendChannel = null;
        mTwin.mSendChannel = null;
        mTwin.mTwin = null;
        mTwin = null;
    }

    METimestamps getTimestamps() {
        return (mMEI.getTimestamps());
    }

    String getSummary() {
        StringBuilder sb = new StringBuilder();
        String consumer;
        String provider;
        MessageExchangeProxy c = null;
        MessageExchangeProxy p = null;
        ServiceEndpoint se = mMEI.getEndpoint();
        String service = "null";
        String operation = "null";

        if (getRole().equals(Role.CONSUMER)) {
            c = this;
            p = mTwin;
            consumer = ((mTwin == null || mTwin.mSendChannel == null)
                    ? "Null" : mTwin.mSendChannel.getChannelId());
            provider = ((mSendChannel == null) ? "Null" : mSendChannel.getChannelId());
        } else {
            p = this;
            c = mTwin;
            consumer = ((mSendChannel == null) ? "Null" : mSendChannel.getChannelId());
            provider = ((mTwin == null || mTwin.mSendChannel == null)
                    ? "Null" : mTwin.mSendChannel.getChannelId());
        }
        if (se != null && se.getServiceName() != null && se.getServiceName().getLocalPart() != null) {
            service = se.getServiceName().getLocalPart();
        }
        if (mMEI.getOperation() != null && mMEI.getOperation().getLocalPart() != null) {
            operation = mMEI.getOperation().getLocalPart();
        }
        sb.append(consumer).append(mepState(c)).append(" -> ").append(provider).append(mepState(p)).append(" (").append(service).append(")(").append(operation).append(")");
        return (sb.toString());

    }

    String mepState(MessageExchangeProxy mep) {
        String result = "[?]";

        if (mep != null) {
            if ((mep.mState[mep.mStateIndex][ACTION_MASK] & DO_ACCEPT) != 0) {
                result = "[ACCEPT]";
            } else if ((mep.mState[mep.mStateIndex][ACTION_MASK] & MARK_DONE) != 0) {
                result = "[DONE]";
            } else {
                result = "[RUN]";
            }
        }
        return (result);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("      ExchangeId: ");
        sb.append(mMEI.getExchangeId());
        sb.append("\n        Pattern: ");
        sb.append(getPattern());
        sb.append("\n        Role: " + getRole().toString() + "\n");
        if (getRole().equals(Role.CONSUMER)) {
            sb.append("        Consumer: "
                    + ((mTwin == null || mTwin.mSendChannel == null)
                    ? "Null" : mTwin.mSendChannel.getChannelId()));
            sb.append("\n        Provider: "
                    + ((mSendChannel == null) ? "Null" : mSendChannel.getChannelId()) + "\n");
        } else {
            sb.append("\n        Provider: "
                    + ((mTwin == null || mTwin.mSendChannel == null)
                    ? "Null" : mTwin.mSendChannel.getChannelId()) + "\n");
            sb.append("        Consumer: "
                    + ((mSendChannel == null) ? "Null" : mSendChannel.getChannelId()));
        }
        sb.append("        Index: ");
        sb.append(mStateIndex);
        sb.append("  SynchState: ");
        sb.append(mSynchState == NONE ? "NONE"
                : (mSynchState == WAIT ? "WAIT"
                : (mSynchState == WAIT_TIMEOUT ? "WAIT_TIMEOUT"
                : (mSynchState == HALF_DONE ? "HALF_DONE"
                : (mSynchState == DONE ? "DONE" : "ERROR")))));
        sb.append("\n        EndpointLink: ");
        sb.append(mEndpointLink == null ? "Null" : ((RegisteredEndpoint) mEndpointLink).toExternalName());
        sb.append(mMEI.toString());
        return (sb.toString());
    }

    int[][] getStates() {
        return mState;
    }
}

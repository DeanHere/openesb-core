/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EngineLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package TestEngine;

import java.io.File;
import java.io.FileWriter;

import java.util.logging.Logger;
import java.util.Random;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.messaging.InOptionalOut;

import javax.management.ObjectName;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;

import javax.xml.namespace.QName;

/**
 * This class implements ComponentLifeCycle. The JBI framework will start this
 * engine class automatically when JBI framework starts up.
 *
 * @author Sun Microsystems, Inc.
 */
public class EngineLifeCycle
    implements ComponentLifeCycle, Component, Runnable, ServiceUnitManager
{
    /**
     * Engine context passed down from framework to this transformation engine.
     */
    private ComponentContext mContext = null;

    /**
     * Engine context passed down from framework to this transformation engine.
     */
    private DeliveryChannel mChannel = null;

    /**
     * Refernce to logger.
     */
    private Logger mLogger = null;

    /**
     *    
     */
    private boolean mInitSuccess = false;

    private Thread                      mRunThread = null;
    private boolean                    mStopping = false;
    private Random                      mRandom = new Random(System.nanoTime());
    private QName                       mService = new QName("NMR-Test-Service");
    private QName                       mService2 = new QName("NMR-Test-Service2");
    private QName                       mStop = new QName("STOP");
    private String                      mEndpoint = "NMR-Engine-Endpoint";
    private String                      mEndpoint2 = "NMR-Engine-Endpoint2";
    private long			mCount;
    private long			mStartTime;
    private long 			mStopTime;

    //-------------------------ComponentLifeCycle------------------------------
    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the additional MBean or null
     *         if there is no additional MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        return null;
    }

    public ServiceUnitManager getSUManager()
    {
       return this;
    }

    //javax.jbi.component.ComponentLifeCycle methods

    /**
     * Initialize the transformation engine. This performs initialization
     * required by  the transformation engine but does not make it ready to
     * process messages.  This method is called immediately after installation
     * of the Transformation engine. It is also called when the JBI framework
     * is starting up, and any time the transformation engine is being
     * restarted after previously being shut down through a call to
     * shutDown().
     *
     * @param context the JBI environment mContext
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         initialize.
     */
    public void init(javax.jbi.component.ComponentContext context)
        throws javax.jbi.JBIException
    {
        mLogger = Logger.getLogger(this.getClass().getPackage().getName()); 
        
        if (context == null)
        {
            throw (new javax.jbi.JBIException("Context is null", new NullPointerException()));
        }

        mContext = context;
        
        mInitSuccess = true;
    }

    /**
     * Shutdown the transformation engine. This performs cleanup before the BPE
     * is terminated. Once this method has been called, init() must be called
     * before the transformation engine can be started again with a call to
     * start().
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         shut down.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mContext = null;
    }

    /**
     * Start the transformation engine. This makes the BPE ready to  process
     * messages. This method is called after init() completes when the JBI
     * framework is starting up, and when the transformation engine is being
     * restarted after a previous call to shutDown().  If stop() was called
     * previously but shutDown() was not, start() can be called without a call
     * to init().
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         start.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLogger.info("Engine is started");

        if (!mInitSuccess)
        {
            mLogger.severe("Init failed");

            return;
        }

        mChannel = mContext.getDeliveryChannel();
        mContext.activateEndpoint(mService, mEndpoint);
        mContext.activateEndpoint(mService2, mEndpoint2);

        if (mChannel == null)
        {
            throw (new javax.jbi.JBIException("Channel is null",
                new NullPointerException()));
        }
        
        if (mRunThread == null)
        {
            mStopping = false;
            mRunThread = new Thread(this, "NMR-Test-Engine");
            mRunThread.setDaemon(true);
            mRunThread.start();
        }
        

    }

    /**
     * Stop the transformation engine. This makes the BPE stop accepting
     * messages for processing. After a call to this method, start() can be
     * called again without first calling init().
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         stop.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mLogger.info("NMR TestEngine stop engine");
        
        if (mRunThread != null)
        {
            mStopping = true;
            try
            {
                mRunThread.join(50);
		mRunThread.interrupt();
		mRunThread.join();
            }
            catch (InterruptedException ie)
            {
                
            }
        }

	try 
	{
            FileWriter      fw = new FileWriter(mContext.getWorkspaceRoot() + File.separator + "engine.stats");
            fw.write("Engine saw: ");
            fw.write(Long.valueOf(mCount).toString());
            fw.write(" Exchanges, ");
            fw.write(Long.valueOf((mCount * 1000) / (mStopTime - mStartTime)).toString());
            fw.write(" Per/Second\n");
            fw.write(mCount == 0 ? "FAILED\n" : "SUCCESSFUL\n");
            fw.close();
            fw = new FileWriter(mContext.getWorkspaceRoot() + File.separator + "engine.info");
            fw.write(mChannel.toString());
            fw.close();
	}
	catch (java.io.IOException ioEx)
	{
	}

        // To add code to stop all services
        try
        {
            if (mChannel != null)
            {
                mLogger.info("NMR TestEngine : " + mChannel.toString());
                mChannel.close();
            }
        }
        catch (Throwable ex)
        {
            ex.printStackTrace();
        }

        mLogger.info("NMR TestEngine stopped");
    }
    
     //-------------------------------Component---------------------------------
   
    /**
     * Returns an object of TransformationEngineLifeCycle.
     *
     * @return ComponentLifeCycle Object implementing
     *         javax.jbi.component.ComponentLifeCycle interface.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        return this;
    }

    /**
     * Returns an object implementing javax.jbi.component.ServiceUnitManager 
     * interface.
     *
     * @return ServiceUnitManager Object implementing
     *         javax.jbi.component.ServiceUnitManager interface.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        return this;
    }
    
    /**
     *
     * @param ref ServiceEndpoint object
     *
     * @return Descriptor Object implementing javax.jbi.servicedesc.Descriptor
     *         interface.
     */
    public Document getServiceDescription(ServiceEndpoint ref)
    {
	org.w3c.dom.Document desc = null;

	try
	{
	}
	catch (Exception e)
	{
    		e.printStackTrace();
	}
	return desc;

    }

    
    /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. The consumer is described by the given 
     *  capabilities, and JBI has already ensured that a fit exists between the 
     *  set of required capabilities of the provider and the available 
     *  capabilities of the consumer, and vice versa. This matching consists of
     *  simple set matching based on capability names only. <br><br>
     *  Note that JBI assures matches on capability names only; it is the 
     *  responsibility of this method to examine capability values to ensure a 
     *  match with the consumer.
     *  @param endpoint the endpoint to be used by the consumer
     *  @param exchange the proposed message exchange to be performed
     *  @param consumerCapabilities the consumers capabilities and requirements
     *  @return true if this provider component can perform the the given 
     *   exchange with the described consumer
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. Ths provider is described 
     *  by the given capabilities, and JBI has already ensure that a fit exists 
     *  between the set of required capabilities of the consumer and the 
     *  available capabilities of the provider, and vice versa. This matching 
     *  consists of simple set matching based on capability names only. <br><br>
     *  Note that JBI assures matches on capability names only; it is the 
     *  responsibility of this method to examine capability values to ensure a 
     *  match with the provider.
     *  @param exchange the proposed message exchange to be performed
     *  @param providerCapabilities the providers capabilities and requirements
     *  @return true if this consurer component can interact with the described
     *   provider to perform the given exchange
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }

    //---------------------------ServiceUnitManager--------------------------
    public String deploy(String serviceUnitName, String serviceUnitRootPath)
    {
	return ("<component-task-result>" +
        "<component-name>TestEngine</component-name>" +
        "<component-task-result-details" +
        "  xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">" +
        "    <task-result-details>" +
        "        <task-id>deploy</task-id>" +
        "        <task-result>SUCCESS</task-result>" +
        "    </task-result-details>" +
        "</component-task-result-details>" +
      "</component-task-result>");
    }
    public void init(String serviceUnitName, String serviceUnitRootPath)
    {
    }
    public void start(String serviceUnitName)
    {
    }
    public void stop(String serviceUnitName)
    {
    }
    public void shutDown(String serviceUnitName)
    {
    }
    public String undeploy(String serviceUnitName, String serviceUnitRootPath)
    {
	return ("<component-task-result>" +
        "<component-name>TestEngine</component-name>" +
        "<component-task-result-details" +
        "  xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">" +
        "    <task-result-details>" +
        "        <task-id>deploy</task-id>" +
        "        <task-result>SUCCESS</task-result>" +
        "    </task-result-details>" +
        "</component-task-result-details>" +
      "</component-task-result>");
    }

    public void run()
    {
        try
        {
            while (!mStopping)
            {
                try
                {
		    if (mCount == 0)
		    {
			mStartTime = System.currentTimeMillis();
		    }
                    handleRequest();
		    mCount++;
		    mStopTime = System.currentTimeMillis();
                }
                catch (javax.jbi.messaging.MessagingException mex)
                {
                    mex.printStackTrace();                    
                }
            }
        }
        catch (Exception e)
        {
	    mLogger.info("NMR TestEngine-run : Exception: " + e.toString());
        }
        mStopping = true;
    }
    
    void handleRequest()
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     me;
        
        me = mChannel.accept();
        if (me instanceof InOnly)
        {
            doInOnly((InOnly)me);
        }
        else if (me instanceof InOut)
        {
            doInOut((InOut)me);
        }
        else if (me instanceof RobustInOnly)
        {
            doRobustInOnly((RobustInOnly)me);
        }
        else if (me instanceof InOptionalOut)
        {
            doInOptionalOut((InOptionalOut)me);
        }
    }
    
    void doInOnly(InOnly me)
        throws javax.jbi.messaging.MessagingException
    {
	if (me.getOperation().equals(mStop))
	{
	    mLogger.info("NMR TestEngine-run : Saw STOP");
	    me.setStatus(ExchangeStatus.DONE);
	    mStopTime = System.currentTimeMillis();
	    mStopping = true;
	}
	else
	{
	    if (mRandom.nextInt(100) <= 95)
	    {
	        me.setStatus(ExchangeStatus.DONE);
	    }
	    else
	    {
	        me.setStatus(ExchangeStatus.ERROR);
	    }
       }
        mChannel.send(me);

    }

    void doInOut(InOut me)
        throws javax.jbi.messaging.MessagingException
    {
        int     rand;
        
        if ((rand = mRandom.nextInt(100)) >= 95)
        {
            me.setStatus(ExchangeStatus.ERROR);
            mChannel.send(me);
        }
        else if (rand >= 90)
        {
            me.setFault(me.createFault());
            mChannel.sendSync(me);
        }
        else
        {
            me.setOutMessage(me.createMessage());
            mChannel.sendSync(me);
        }        
    }
    
    void doRobustInOnly(RobustInOnly me)
        throws javax.jbi.messaging.MessagingException
    {
        int     rand;
        
        if ((rand = mRandom.nextInt(100)) >= 95)
        {
            me.setStatus(ExchangeStatus.ERROR);
            mChannel.send(me);
        }
        else if (rand >= 90)
        {
            me.setFault(me.createFault());
            mChannel.sendSync(me);
        }
        else
        {
            me.setStatus(ExchangeStatus.DONE);
            mChannel.send(me);
        }        
        
    }
    
    void doInOptionalOut(InOptionalOut me)
        throws javax.jbi.messaging.MessagingException
    {
        int     rand;
        
        if ((rand = mRandom.nextInt(100)) >= 95)
        {
            me.setStatus(ExchangeStatus.ERROR);
            mChannel.send(me);
        }
        else if (rand >= 90)
        {
            me.setFault(me.createFault());
            mChannel.sendSync(me);
        }
        else
        {
            me.setOutMessage(me.createMessage());
            mChannel.sendSync(me);
            if (me.getFault() != null)
            {
                me.setStatus(ExchangeStatus.DONE);
                mChannel.send(me);
            }
        }        
        
    }

}

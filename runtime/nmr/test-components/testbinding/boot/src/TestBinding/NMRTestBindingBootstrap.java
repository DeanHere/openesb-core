/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRTestBindingBootstrap.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package TestBinding;

import java.util.logging.Logger;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.InstallationContext;


/**
 * Implemented by a BC Component (BC) to provide any special processing
 * required at install/uninstall time. Things such as creation/deletion of
 * directories, files, database tables could be done by the onInstall() and
 * onUninstall() methods, respectively. Also allows the BC to terminate the
 * installation or uninstallation in the event of an error.
 *
 * @author Sun Microsystems, Inc
 */
public class NMRTestBindingBootstrap
    implements Bootstrap
{
    /**
     * This is the installation context given by the framework.
     */
    private InstallationContext mContext;

    /**
     * Internal handle to the logger instance
     */
    private Logger mLog;

    /**
     * Creates a new instance of ProxyBindingBootstrap.
     */
    public NMRTestBindingBootstrap()
    {
        mLog = Logger.getLogger(this.getClass().getPackage().getName());
    }

//
// ---------- Methods defined in javax.jbi.component.Bootstrap ---------
//          

    /**
     * Get the JMX ObjectName for the optional installation configuration MBean
     * for this BC. If there is none, the value is null.
     * @return ObjectName the JMX object name of the installation configuration
     *         MBean or null if there is no MBean.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Called to initialize the BC bootstrap.
     * @param installContext is the context containing information from the
     *        install command and from the BC jar file.
     * @throws javax.jbi.JBIException when there is an error requiring that the
     *         installation be terminated.
     */
    public void init(InstallationContext installContext)
        throws javax.jbi.JBIException
    {
        mContext = installContext;
    }

    /**
     * Cleans up any resources allocated by the bootstrap implementation,
     * including performing deregistration of the extension MBean, if 
     * applicable.
     * <p>
     * This method must be called after the onInstall() or onUninstall() method
     * is called, whether it succeeds or fails. It must be called after
     * init() is called, if init() fails by throwing an exception.
     * 
     * @exception JBIException if the bootstrap cannot clean up allocated
     *            resources
     */
    public void cleanUp() 
        throws javax.jbi.JBIException
    {
        
    }
    
    /**
     * Called at the beginning of installation of Proxy Binding . For the Proxy
     * Binding there is nothing to do.
     * @throws javax.jbi.JBIException when there is an error requiring that the
     *         installation be terminated.
     */
    public void onInstall()
        throws javax.jbi.JBIException
    {
    }

    /**
     * Called at the beginning of uninstallation of ProxyBinding . For the Proxy
     * Binding there is nothing to do.
     * @throws javax.jbi.JBIException when there is an error requiring that the
     *         uninstallation be terminated.
     */
    public void onUninstall()
        throws javax.jbi.JBIException
    {
    }
    
}

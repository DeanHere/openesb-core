#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)startengine.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "startengine.ksh- Install and start NMR TestEngine"

. ./regress_defs.ksh

$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.install.file=$JV_SRCROOT/install/as8/jbi/lib/nmrtestengine.jar install-component


$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=NMRTestEngine start-component


echo Completed Starting NMR TestEngine

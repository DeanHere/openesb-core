/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestMessageService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import javax.jbi.component.Component;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.OpenType;
import javax.transaction.xa.XAResource;

import javax.xml.namespace.QName;

/**
 * Tests for the MessageService class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestMessageService extends junit.framework.TestCase
{
    /** Path to endpoint descriptor. */
    public static final String ENDPOINT_XML_PATH = 
        System.getProperty("junit.srcroot") + "/nms/regress/endpoint.wsdl";
    
    /** Component ID for test channel. */
    private static final String ID_A = "ChannelA";
    /** Component ID for test channel. */
    private static final String ID_B = "ChannelB";
    
    /** Service and endpoint constants. */
    private static final QName  SERVICE_A       = new QName("service-a");
    private static final String ENDPOINT_A      = "endpoint-a";    
    private static final QName  SERVICE_B       = new QName("service-b");
    private static final String ENDPOINT_B      = "endpoint-b";
    private static final QName  INTERFACE_FB    = new QName("foobar");
    private static final QName  SERVICE_FOO     = new QName("foo");
    private static final String ENDPOINT_BAR    = "bar";
    
    /** NMS impl */
    private MessageService mMsgSvc;
     /** NMR Environment Context */
    private NMRContext mContext;
    /** Test channel which is created/destroyed for each test. */
    private DeliveryChannelImpl mChannelA;
    /** Test channel which is created/destroyed for each test. */
    private DeliveryChannelImpl mChannelB;
    /** Endpoint Reference on Channel A */    
    private ServiceEndpoint   mEndpointA;
    /** Endpoint Reference on Channel B */    
    private ServiceEndpoint   mEndpointB;
    /** Exchange factory */    
    private ExchangeFactory   mFactory;
    /** Endpoint registry */    
    private EndpointRegistry  mRegistry;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestMessageService(String aTestName)
    {
        super(aTestName);
        
        mMsgSvc = new MessageService();
        mContext = new NMRContext(mMsgSvc);
        mFactory = new ExchangeFactory(mMsgSvc);
        mRegistry = EndpointRegistry.getInstance();
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mMsgSvc.initService(mContext);
        mMsgSvc.startService();
        
        // create test channels and add them to the NMS routing table
        mChannelA  = new DeliveryChannelImpl(ID_A, null, mMsgSvc, null);
        mChannelB  = new DeliveryChannelImpl(ID_B, null, mMsgSvc, null);
        mMsgSvc.addChannel(mChannelA);
        mMsgSvc.addChannel(mChannelB);
        
        mEndpointA = mChannelA.activateEndpoint(SERVICE_A, ENDPOINT_A);
        mEndpointB = mChannelB.activateEndpoint(SERVICE_B, ENDPOINT_B);        
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
        mChannelA.close();
        mChannelB.close();
        
        mMsgSvc.stopService();
        mContext.reset();
    }

// =============================  test methods ================================

    /**
     * testGetActiveChannelCount
     * @throws Exception if an unexpected error occurs
     */
    public void testGetActiveChannelCount()
           throws Exception
    {
        // we activate two channels in test setup()
        assertTrue(mMsgSvc.getActiveChannelCount() == 2);
        
        // activate a channel and test to see if count is incremented
        mMsgSvc.activateChannel("foo", null);
        assertTrue(mMsgSvc.getActiveChannelCount() == 3);
    }
    
    /**
     * testGetActiveChannels
     * @throws Exception if an unexpected error occurs
     */
    public void testGetActiveChannels()
           throws Exception
    {
        String[] channels;
        List     list;
        
        channels = mMsgSvc.getActiveChannels();
        
        // verify size
        assertTrue(channels.length == 2);
        
        // sort the array and search for the endpoints that should be there
        list = Arrays.asList(channels);
        assertTrue(list.contains(ID_A));
        assertTrue(list.contains(ID_B));
    }
    
    /**
     * testGetActiveEndpointCount
     * @throws Exception if an unexpected error occurs
     */
    public void testGetActiveEndpointCount()
           throws Exception
    {        
        // we activate two endpoints in test setup()
        assertTrue(mMsgSvc.getActiveEndpointCount() == 2);
        
        // activate a channel and test to see if count is incremented
        mChannelA.activateEndpoint(SERVICE_FOO, ENDPOINT_BAR);
        assertTrue(mMsgSvc.getActiveEndpointCount() == 3);
    }
    
    /**
     * testGetActiveEndpoints
     * @throws Exception if an unexpected error occurs
     */
    public void testGetActiveEndpoints()
           throws Exception
    {
        String[] endpoints;
        List       list;
        
        list      = new ArrayList();
        endpoints = mMsgSvc.getActiveEndpoints();
        
        // index 1 = endpoint name
        for (int i = 0; i < endpoints.length; i++)
        {
            list.add(endpoints[i]);
        }
        
        // verify size
        assertTrue(list.size() == 2);
        
        // verify contents
        assertTrue(list.contains(((RegisteredEndpoint)mEndpointA).toExternalName()));
        assertTrue(list.contains(((RegisteredEndpoint)mEndpointB).toExternalName()));
    }
        
    
    /** Send using a interface connection. */
    public void testSendInterfaceConnection()
           throws Exception
    {
        MessageExchange exchange;
        
        // point interface at endpoint on channel A
        mRegistry.addInterfaceConnection(INTERFACE_FB, SERVICE_A, ENDPOINT_A);
        
        exchange = mFactory.createInOnlyExchange();
        exchange.setInterfaceName(INTERFACE_FB);
        exchange.setOperation(new QName("op"));
        
        mChannelB.send(exchange);
        
        assertTrue(mChannelA.accept(1000) != null);
    }
    
    /** Send using an endpoint connection with service name. */
    public void testSendEndpointConnection()
           throws Exception
    {
        MessageExchange exchange;
        
        // point interface at endpoint on channel A
        mRegistry.addEndpointConnection(SERVICE_FOO, ENDPOINT_BAR, 
            SERVICE_A, ENDPOINT_A, Link.STANDARD);
        
        exchange = mFactory.createInOnlyExchange();
        exchange.setService(SERVICE_FOO);
        exchange.setOperation(new QName("op"));
        
        mChannelB.send(exchange);
        
        assertTrue(mChannelA.accept(1000) != null);
    }
    
    /** Send using an endpoint connection using service endpoint. */
    public void testSendEndpointConnection2()
           throws Exception
    {
        ServiceEndpoint endpoint;
        MessageExchange consumerEx;
        MessageExchange providerEx;
        
        // point interface at endpoint on channel A
        mRegistry.addEndpointConnection(SERVICE_FOO, ENDPOINT_BAR, 
            SERVICE_A, ENDPOINT_A, Link.STANDARD);
        
        endpoint = mRegistry.getInternalEndpoint(SERVICE_FOO, ENDPOINT_BAR);
        
        consumerEx = mFactory.createInOnlyExchange();
        consumerEx.setEndpoint(endpoint);
        consumerEx.setOperation(new QName("op"));
        
        mChannelB.send(consumerEx);
        
        // make sure it got to the other side
        providerEx = mChannelA.accept(1000);
        assertTrue(providerEx != null);
        
        // verify that send() didn't overwrite the endpoint link for consumer
        assertEquals(endpoint, consumerEx.getEndpoint());
        assertEquals(mEndpointA, providerEx.getEndpoint());
        assertFalse(endpoint.equals(mEndpointA));
        
    }
    
    /** Send using an endpoint connection which does not have a corresponding
     *  activated endpoint. 
     */
    public void testSendEndpointConnection3()
           throws Exception
    {
        ServiceEndpoint endpoint;
        MessageExchange consumerEx;
        MessageExchange providerEx;
        
        // point interface at endpoint on channel A
        mRegistry.addEndpointConnection(SERVICE_FOO, ENDPOINT_BAR, 
            SERVICE_A, "blahblah", Link.STANDARD);
        
        endpoint = mRegistry.getInternalEndpoint(SERVICE_FOO, ENDPOINT_BAR);
        
        consumerEx = mFactory.createInOnlyExchange();
        consumerEx.setEndpoint(endpoint);
        consumerEx.setOperation(new QName("op"));
        
        try
        {
            mChannelB.send(consumerEx);        
            fail("Able to send using unlinked service connection");
        }
        catch (javax.jbi.messaging.MessagingException msgEx)
        {
            // we should end up here
        }
    }    
    
    /** Test eh XAResource registration primitives.
     */
    public void testXAResourceRegistration()
           throws Exception
    {
        XAResource  res1 = new XAtest();
        XAResource  res2 = new XAtest();
        XAResource[] ress;
        
        mMsgSvc.addXAResource(res1);
        mMsgSvc.addXAResource(res2);
        ress =  mMsgSvc.getXAResources();
        assertTrue(ress.length == 2);
        assertTrue(ress[0] == res1 || ress[1] == res1);
        assertTrue(ress[0] == res2 || ress[1] == res2);
        mMsgSvc.purgeXAResources();
        ress =  mMsgSvc.getXAResources();
        assertTrue(ress.length == 0);
    }    

    public void testMessageServiceStatistics()
        throws Exception
    {
        MessageExchange me1;
        
        me1 = mFactory.createInOnlyExchange();
        me1.setService(SERVICE_B);
        me1.setOperation(new QName("op"));
        mChannelA.send(me1);
        assertTrue((me1 = mChannelB.accept()) != null);
        me1.setStatus(ExchangeStatus.DONE);
        mChannelB.send(me1);
        assertTrue(mChannelA.accept() != null);
        
        CompositeData   cd = mMsgSvc.getStatistics();
        assertTrue(cd != null);
        assertTrue(cd.values().size() == 16);
        mMsgSvc.enableStatistics();
        assertTrue(mMsgSvc.areStatisticsEnabled());
        me1 = mFactory.createInOnlyExchange();
        me1.setService(SERVICE_B);
        me1.setOperation(new QName("op"));
        mChannelA.send(me1);
        assertTrue((me1 = mChannelB.accept()) != null);
        me1.setStatus(ExchangeStatus.DONE);
        mChannelB.send(me1);
        assertTrue(mChannelA.accept() != null);
        
        cd = mMsgSvc.getStatistics();
        assertTrue(cd != null);
        assertTrue(cd.values().size() == 40);
        mMsgSvc.disableStatistics();
        assertTrue(!mMsgSvc.areStatisticsEnabled());
        
        
    }
    
    public void testDeliveryChannelStatistics()
        throws Exception
    {
        MessageExchange me1;
        
        me1 = mFactory.createInOnlyExchange();
        me1.setService(SERVICE_B);
        me1.setOperation(new QName("op"));
        mChannelA.send(me1);
        assertTrue((me1 = mChannelB.accept()) != null);
        me1.setStatus(ExchangeStatus.DONE);
        mChannelB.send(me1);
        assertTrue(mChannelA.accept() != null);
        
        String[] c = mMsgSvc.getChannelNames();
        for (int i = 0; i < c.length; i++)
        {
            ChannelStatistics   cs = mMsgSvc.getChannelStatistics(c[i]);
            assertTrue(cs != null);
            CompositeData   cd = cs.getStatistics();
            assertTrue(cd != null);
            assertTrue(cd.values().size() == 20);
        }
    }
    
    public void testEndpointStatistics()
        throws Exception
    {
         MessageExchange me1;
        
        me1 = mFactory.createInOnlyExchange();
        me1.setService(SERVICE_B);
        me1.setOperation(new QName("op"));
        mChannelA.send(me1);
        assertTrue((me1 = mChannelB.accept()) != null);
        me1.setStatus(ExchangeStatus.DONE);
        mChannelB.send(me1);
        assertTrue(mChannelA.accept() != null);
        
        String[] c = mMsgSvc.getEndpointNames();
        for (int i = 0; i < c.length; i++)
        {
            EndpointStatistics  es = mMsgSvc.getEndpointStatistics(c[i]);
            assertTrue(es != null);
            CompositeData   cd = es.getStatistics();
            assertTrue(cd != null);
            assertTrue(cd.values().size() == 14);
        }
    }
}

class XAtest implements XAResource
{
    public void start(javax.transaction.xa.Xid tx, int id)
    {
        
    }
    public boolean setTransactionTimeout(int timeout)
    {
        return (false);
    }
    public void rollback(javax.transaction.xa.Xid tx)
    {
        
    }
    public javax.transaction.xa.Xid[] recover(int id)
    {
        return (null);
    }
    public int prepare(javax.transaction.xa.Xid id)
    {
        return (0);
    }
    public boolean isSameRM(javax.transaction.xa.XAResource resource)
    {
        return (false);
    }
    public int getTransactionTimeout()
    {
        return (0);
    }
    public void forget(javax.transaction.xa.Xid id)
    {
        
    }
    public void end(javax.transaction.xa.Xid xid, int id)
    {
        
    }
    public void commit(javax.transaction.xa.Xid xid, boolean x)
    {
        
    }
}

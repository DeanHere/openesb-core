/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WsdlDocument.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import com.sun.jbi.wsdl2.*;
import com.sun.jbi.wsdl2.impl.WsdlFactory;
import java.io.File;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

/** This class generates the default WSDL definition used by NMR tests.  
 *  Constants are supplied to protect individual tests from name changes
 *  in the underlying WSDL document.
 * @author Sun Microsystems, Inc.
 */
public class WsdlDocument
{
    /** Path to default WSDL definition. */
    public static final String WSDL_20_DEFINITION = 
        System.getProperty("junit.srcroot") + "/nms/regress/regress20.wsdl";
    
    public static final String WSDL_11_DEFINITION = 
        System.getProperty("junit.srcroot") + "/nms/regress/regress11.wsdl";
    
    public static final String NS_URI = "http://example.com/wsdl";
    /** NCNames */
    public static final String HELLO_C_INTERFACE    = "hellocity";
    public static final String HELLO_W_INTERFACE    = "helloworld";
    public static final String HELLO_C_OPERATION_1  = "helloPhoenix";
    public static final String HELLO_C_OPERATION_2  = "helloTempe";
    public static final String HELLO_W_OPERATION    = "helloEarth";
    public static final String HELLO_BINDING        = "hellobinding";
    public static final String HELLO_SERVICE        = "helloservice";
    public static final String HELLO_ENDPOINT_1     = "helloendpoint1";
    public static final String HELLO_ENDPOINT_2     = "helloendpoint2";
    
    public static final String STOCK_INTERFACE      = "StockQuote";
    public static final String STOCK_OPERATION      = "GetLastTradePrice";
    public static final String STOCK_BINDING        = "StockQuoteBinding";
    public static final String STOCK_SERVICE        = "StockQuoteService";
    public static final String STOCK_ENDPOINT       = "StockQuotePort";
    
    /** QNames */
    public static final QName HELLO_C_INTERFACE_Q 
        = new QName(NS_URI, HELLO_C_INTERFACE);
    public static final QName HELLO_W_INTERFACE_Q 
        = new QName(NS_URI, HELLO_W_INTERFACE);
    public static final QName STOCK_INTERFACE_Q 
        = new QName(NS_URI, STOCK_INTERFACE);
    public static final QName HELLO_C_OPERATION_1_Q 
        = new QName(NS_URI, HELLO_C_OPERATION_1);
    public static final QName HELLO_C_OPERATION_2_Q 
        = new QName(NS_URI, HELLO_C_OPERATION_2);
    public static final QName HELLO_W_OPERATION_Q 
        = new QName(NS_URI, HELLO_W_OPERATION);
    public static final QName STOCK_OPERATION_Q 
        = new QName(NS_URI, STOCK_OPERATION);
    public static final QName HELLO_SERVICE_Q 
        = new QName(NS_URI, HELLO_SERVICE);
    public static final QName STOCK_SERVICE_Q 
        = new QName(NS_URI, STOCK_SERVICE);
    
    
    public static void main(String[] args)
        throws Exception
    {
        WsdlFactory factory;
        Description defs;
        
        factory = WsdlFactory.newInstance();
        
        defs    = createDefinition(factory);
        
        if (args.length == 1)
        {
            writeDefinition(factory, defs, args[0]);
        }
        else
        {
            writeDefinition(factory, defs, WSDL_20_DEFINITION);
        }
        
        /*
        defs = readDefinition(factory, args[0]);
        System.out.println(defs.getService(0).getQName());
         */
    }
     
    public static Description createDefinition(WsdlFactory wf)
        throws Exception
    {
        Description                 d;        
        Interface                   hellocity;
        Interface                   helloworld;
        Interface                   stockquote;        
        InterfaceOperation          hellocity1;
        InterfaceOperation          hellocity2;     
        InterfaceOperation          helloworld1;  
        InterfaceOperation          stockquote1;    
        com.sun.jbi.wsdl2.Binding   hellobinding;
        com.sun.jbi.wsdl2.Binding   stockbinding;
        Service                     helloservice;
        Service                     stockservice;        
        Endpoint                    helloendpoint1;
        Endpoint                    helloendpoint2;
        Endpoint                    stockendpoint1;
        
        
        d = wf.newDescription(NS_URI);
        
        /**
         *   INTERFACES
         */
        hellocity  = d.addNewInterface(HELLO_C_INTERFACE);
        helloworld = d.addNewInterface(HELLO_W_INTERFACE);
        stockquote = d.addNewInterface(STOCK_INTERFACE);
        /** The next line doesn't seem to work, so I just edit the output by hand. */
        //helloworld.appendInterface(hellocity);
           
        /**
         *   BINDINGS
         */
        hellobinding = d.addNewBinding(HELLO_BINDING);
        stockbinding = d.addNewBinding(STOCK_BINDING);        
        hellobinding.setType(HELLO_BINDING);
        stockbinding.setType(STOCK_BINDING);
        
        /**
         *   OPERATIONS
         */
        hellocity1  = hellocity.addNewOperation();
        hellocity2  = hellocity.addNewOperation();
        helloworld1 = helloworld.addNewOperation();
        stockquote1 = stockquote.addNewOperation();    
        hellocity1.setPattern(ExchangePattern.IN_ONLY.toString());
        hellocity2.setPattern(ExchangePattern.IN_OUT.toString());
        helloworld1.setPattern(ExchangePattern.IN_OPTIONAL_OUT.toString());
        stockquote1.setPattern(ExchangePattern.ROBUST_IN_ONLY.toString());
        hellocity1.setName(HELLO_C_OPERATION_1);
        hellocity2.setName(HELLO_C_OPERATION_2);
        helloworld1.setName(HELLO_W_OPERATION);
        stockquote1.setName(STOCK_OPERATION);
        
        
        /**
         *   SERVICES
         */
        helloservice = d.addNewService(HELLO_SERVICE);
        stockservice = d.addNewService(STOCK_SERVICE);
        helloservice.setInterface(helloworld);        
        stockservice.setInterface(stockquote);
           
        
        /**
         *  ENDPOINTS
         */
        helloendpoint1 = helloservice.addNewEndpoint(HELLO_ENDPOINT_1, hellobinding);
        helloendpoint2 = helloservice.addNewEndpoint(HELLO_ENDPOINT_2, hellobinding);
        stockendpoint1 = stockservice.addNewEndpoint(STOCK_ENDPOINT, stockbinding);
        helloendpoint1.setBinding(hellobinding);
        helloendpoint2.setBinding(hellobinding);
        stockendpoint1.setBinding(stockbinding);
        
        return d;
    }
    
    public static void writeDefinition(WsdlFactory wf, Description def, String path)
        throws Exception
    {
        WsdlWriter ww = wf.newWsdlWriter();
        ww.writeDescription(def, new java.io.FileOutputStream(path));        
    }
    
    public static Description readDefinition(WsdlFactory wf, String path)
        throws Exception
    {
        WsdlReader wr = wf.newWsdlReader();
        return wr.readDescription("c:/development/jbi/nms/bld/regress/regress.wsdl");
    }    
    
    public static Document readDocument(String path)
        throws Exception
    {
        DocumentBuilderFactory  dbf;
        DocumentBuilder         db;
        Document                d;
        
        dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        db  = dbf.newDocumentBuilder();
        d   = db.parse(new File(path));
        
        return d;
    }
    
    public static Document readDefaultDocument()
        throws Exception
    {
        return readDocument(WSDL_20_DEFINITION);
    }
}

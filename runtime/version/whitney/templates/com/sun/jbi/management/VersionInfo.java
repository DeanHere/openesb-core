/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)VersionInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

/**
 * VersionInfo - Version info class for JBI_Preview.
 */
public class VersionInfo implements com.sun.jbi.VersionInfo
{
    /**
     * @return the full product name.
     */
    public String fullProductName()
    {
        return ("@FULL_PRODUCT_NAME@");
    }

    /**
     * @return the short product name.
     */
    public String shortProductName()
    {
        return ("@SHORT_PRODUCT_NAME@");
    }

    /**
     * @return the major version number.
     */
    public String majorVersion()
    {
        return ("@MAJOR_VERSION@");
    }

    /**
     * @return the minor version number.
     */
    public String minorVersion()
    {
        return ("@MINOR_VERSION@");
    }

    /**
     * @return the build number for this version.
     */
    public String buildNumber()
    {
        return ("@BUILD_NUMBER@");
    }

    /**
     * @return the Copyright for this version.
     */
    public String copyright()
    {
        return ("Under Common Development and Distribution License (CDDL). For more information, please visit www.open-esb.net");
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SoapFaultError.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SoapFaultError.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 26, 2004, 6:12 PM
 */

package com.sun.jbi.binding.security;

import javax.xml.namespace.QName;
import javax.xml.soap.Detail;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public interface SoapFaultError 
    extends Error  
{
    
    /** Gets the <code>faultcode</code> element. The <code>faultcode</code>
     *  element provides an algorithmic mechanism for identifying the
     *  fault. The fault codes for WS-Security are defined in Section 12 of
     *  the WS-Security Specification.
     *
     *  @return QName of the faultcode element
     */
    QName getFaultCode();
  

    /** 
     * Gets the <code>faultstring</code> element. The <code>faultstring</code>
     *  provides a human-readable description of the SOAP fault and 
     *  is not intended for algorithmic processing.
     *
     *  @return faultstring element of the SOAP fault
     */
    String getFaultString();
    

   /**
    * Gets the <code>faultactor</code> element. The <code>faultactor</code>
    *  element provides information about which SOAP node on the 
    *  SOAP message path caused the fault to happen. It indicates 
    *  the source of the fault.
    * 
    *  @return <code>faultactor</code> element of the SOAP fault 
    */
    String getFaultActor(); 


   /** 
    * Gets the detail element. The detail element is intended for
    *  carrying application specific error information related to
    *  the SOAP Body.
    *
    *  @return <code>detail</code> element of the SOAP fault
    *  @see javax.xml.soap.Detail
    */
    Detail getFaultDetail(); 

}

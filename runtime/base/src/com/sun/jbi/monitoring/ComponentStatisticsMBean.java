/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentStatisticsMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.monitoring;
import java.util.Date;
import javax.management.openmbean.CompositeData;

/**
 * This interface defines the MBean for collection of statistics for a single
 * installed component. All statistics are since the last JBI startup; they
 * are all reset when JBI is restarted.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentStatisticsMBean
    extends StatisticsMBean
{
    //
    // API statistics collected by the framework
    //

    /**
     * Get the time that this component was last started.
     * @return The time of the last successful start() call.
     */
    Date getLastRestartTime();

    /**
     * Get the count of calls to the life cycle init() method.
     * @return The total number of calls to init().
     */
    int getInitRequests();

    /**
     * Get the count of calls to the life cycle start() method.
     * @return The total number of calls to start().
     */
    int getStartRequests();

    /**
     * Get the count of calls to the life cycle stop() method.
     * @return The total number of calls to stop().
     */
    int getStopRequests();

    /**
     * Get the count of calls to the life cycle shutDown() method.
     * @return The total number of calls to shutDown().
     */
    int getShutDownRequests();

    /**
     * Get the count of failed calls to life cycle methods (excluding timeouts).
     * @return The total number of failed calls.
     */
    int getFailedRequests();

    /**
     * Get the count of timed out calls to life cycle methods.
     * @return The total number of timed out calls.
     */
    int getTimedOutRequests();

    /**
     * Get the current number of Service Units deployed.
     * @return The current number of deployed SUs.
     */
    short getDeployedSUs();

    /**
     * Get the count of calls to the ServiceUnitManager deploy() method.
     * @return The total number of calls to deploy().
     */
    int getDeploySURequests();

    /**
     * Get the count of calls to the ServiceUnitManager undeploy() method.
     * @return The total number of calls to undeploy().
     */
    int getUndeploySURequests();

    /**
     * Get the count of calls to the ServiceUnitManager init() method.
     * @return The total number of calls to init().
     */
    int getInitSURequests();

    /**
     * Get the count of calls to the ServiceUnitManager start() method.
     * @return The total number of calls to start().
     */
    int getStartSURequests();

    /**
     * Get the count of calls to the ServiceUnitManager stop() method.
     * @return The total number of calls to stop().
     */
    int getStopSURequests();

    /**
     * Get the count of calls to the ServiceUnitManager shutDown() method.
     * @return The total number of calls to shutDown().
     */
    int getShutDownSURequests();

    /**
     * Get the count of failed calls to ServiceUnitManager methods (excluding
     * timeouts).
     * @return The total number of failed calls.
     */
    int getFailedSURequests();

    /**
     * Get the count of timed out calls to ServiceUnitManager methods.
     * @return The total number of timed out calls.
     */
    int getTimedOutSURequests();

    /**
     * Get the current number of services (for an SE) or endpoints (for a BC)
     * registered with the NMR.
     * @return The total number registered services or endpoints.
     */
    short getRegisteredServicesOrEndpoints();
}

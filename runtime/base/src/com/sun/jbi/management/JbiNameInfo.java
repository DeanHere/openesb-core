/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiNameInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

/**
 * JbiNameInfo keeps track of the unique information about a
 * JBI Framework system service or installed component.
 *
 * Once instantiated, this object is immutable.
 *
 * @author Sun Microsystems, Inc.
 */
public interface JbiNameInfo
{
    /**
     * Return the name of the JBI Framework Service or Installed component.
     * @return name of service or component
     */
    String name();

    /**
     * Return the alternative name of a JBI Framework installed component.
     * @return alternative name of the installed component
     */
    String altName();

    /**
     * True iff this object identifies an SE.
     * By definition, isEngine() => !isBinding() && !isSystemService()
     *
     * @return true iff object identifies an SE.
     */
    boolean isEngine();

    /**
     * True iff this object identifies an SE.
     * By definition, isBinding() => !isEngine() && !isSystemService()
     *
     * @return true iff object identifies an SE.
     */
    boolean isBinding();

    /**
     * True iff this object identifies a JBI Framework System Service.
     * By definition, isSystemService() => !isEngine() && !isBinding()
     *
     * @return true iff object identifies an JBI Framework System Service.
     */
    boolean isSystemService();
}

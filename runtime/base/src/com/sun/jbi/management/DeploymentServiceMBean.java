/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentServiceMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

/**
 * This DeploymentServiceMBean extends the public interface. This has additional 
 * operations to :
 * <br/>
 * <ul>
 * <li> Get the state of a particular service unit </li>
 * <li> Deploy a service assembly from the repository </li>
 * <li> allow forceful shutdown and undeploy of service assemblies </li>
 * </ul>
 * @author Sun Microsystems, Inc.
 */
public interface DeploymentServiceMBean
    extends javax.jbi.management.DeploymentServiceMBean
{
    /**
     * Get the state of a service unit deployed to a component. 
     * <br/>
     * If the service unit is deployed to the component then one of the states :
     * "Started", "Stopped" or "Shutdown" is returned. 
     *
     * @return the actual state of the deployed service unit or "Unknown". 
     * @param componentName - the component name
     * @param serviceUnitName - name of the service unit
     * @exception javax.jbi.JBIException if the component is not installed or if the 
     * service unit is not deployed to the component.
     * @see javax.jbi.management.DeploymentServiceMBean
     */
    String getServiceUnitState(String componentName, String serviceUnitName) 
        throws javax.jbi.JBIException;

    /**
     * Returns the deployment descriptor of the Service Unit that was 
     * deployed to the JBI enviroment, serialized to a <code>String</code>.
     * 
     * @param serviceAssemblyName name of the service assembly to be queried;
     *        must be non-null and non-empty
     * @param serviceUnitName name of the service unit to be queried;
     *        must be non-null and non-empty
     * @return descriptor of the Service Unit; must be non-null
     * @exception Exception if a processing error occurs
     */
    public String getServiceUnitDescriptor(String serviceAssemblyName, String serviceUnitName) throws Exception;
    
    /**
     * Deploy a Service Assembly from the Repository. 
     * <br/>
     * If the service assembly is not there in the DAS repository a exception will
     * be thrown. 
     * 
     * @param serviceAssemblyName - name of the registered service assembly.
     * @return a status management message with details on the service assembly deployment.
     * @exception javax.jbi.JBIException if the service assembly is not registered or
     * deploy fails.
     */
     String deployFromRepository(String serviceAssemblyName)
        throws javax.jbi.JBIException;
     
    /**
     * Undeploys the given Service Assembly from the JBI environment.
     * 
     * @param serviceAssemblyName name of the Service Assembly that is to be 
     *        undeployed; must be non-null and non-empty
     * @param force if this flag is set to true any undeployment errors thrown 
     * from the component will be ignored as the service assembly is forcefully
     * undeployed.
     * @return Result/Status of the current undeployment; must conform to 
     *         JBI management result/status XML schema; must be non-null and
     *         non-empty
     * @exception Exception if compelete undeployment fails      
     */
    String undeploy(String serviceAssemblyName, boolean force) throws Exception;
    
    /**
     * Undeploys the given Service Assembly from the JBI environment.
     * 
     * @param serviceAssemblyName name of the Service Assembly that is to be 
     *        undeployed; must be non-null and non-empty
     * @param force if this flag is set to true any undeployment errors thrown 
     *        from the component will be ignored as the service assembly is forcefully
     *        undeployed.
     * @param keep if true the service assembly archive should not be removed
     *        from the domain. If false the archive is removed if the service 
     *        assembly is not deployed on any instances.
     * @return Result/Status of the current undeployment; must conform to 
     *         JBI management result/status XML schema; must be non-null and
     *         non-empty
     * @exception Exception if compelete undeployment fails      
     */
    String undeploy(String serviceAssemblyName, boolean force, boolean keep) 
        throws Exception;
    
    
    /**
     * Shut down the service assembly. 
     * 
     * @param serviceAssemblyName name of the assembly to be shut down; must be
     *        non-null and non-empty
     * @param force if this flag is true, the service assembly is shutdown forcefully.
     * Any exceptions thrown by the component for service unit shutdown are ignored.
     * @return result / status string giving the results of shutting down  
     *         each service unit in the assembly; must be non-null and non-empty
     * @exception Exception if there is no such assembly
     * @exception Exception if the assembly fails to shut down
     */
    String shutDown(String serviceAssemblyName, boolean force) throws Exception;
    
    /**
     * Get the Service Assembly name for a specific service unit deployed to a 
     * specific component.
     * 
     * @param serviceUnitName - service unit identifier
     * @param componentName - component identifier
     */
    String getServiceAssemblyName(String serviceUnitName, String componentName) throws Exception;
}

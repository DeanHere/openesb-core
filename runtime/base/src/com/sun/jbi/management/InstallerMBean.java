/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallerMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

/**
 * This InstallerMBean extends the public interface to add a 'force' option
 * to the uninstall() method.
 *
 * @author Sun Microsystems, Inc.
 */
public interface InstallerMBean
    extends javax.jbi.management.InstallerMBean
{
    /**
     * Uninstall the component. This completely removes the component from the
     * JBI system. If the <code>force</code> flag is set to <code>true</code>,
     * the uninstall operation succeeds regardless of any exception thrown by
     * the component itself.
     *
     * @param force set to true to ignore any failures and proceed with the
     * uninstall.
     * @exception javax.jbi.JBIException if some other failure occurs.
     */
    void uninstall(boolean force) throws javax.jbi.JBIException;
}

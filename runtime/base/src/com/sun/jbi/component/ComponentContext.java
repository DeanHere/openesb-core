/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.component;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.ManagementMessageFactory;
import com.sun.jbi.security.KeyStoreUtil;
import com.sun.jbi.wsdl2.WsdlFactory;
import com.sun.jbi.wsdl2.WsdlException;

import java.util.logging.Logger;

import javax.jbi.JBIException;

import javax.transaction.xa.XAResource;

/**
 * This context provides access to data needed by all components running in the
 * JBI framework. It is created when the component is initialized and destroyed
 * when the component is shut down.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentContext
    extends javax.jbi.component.ComponentContext
{
    /**
     * Get the management message factory which enables JBI components
     * to construct status and exception messages.
     * @return the management message factory.
     */
    ManagementMessageFactory getManagementMessageFactory();

    /**
     * Get a StringTranslator for a specific package name.
     * @param packageName the name of the package that contains the resource
     * bundle for this translator.
     * @return the StringTranslator instance for the specified package.
     */
    StringTranslator getStringTranslator(String packageName);

    /**
     * Get a StringTranslator for a specific object.
     * @param object an object that is in the same package that contains the
     * resource bundle for this translator.
     * @return the StringTranslator instance for the object.
     */
    StringTranslator getStringTranslatorFor(Object object);

    /**
     * Get a copy of the WSDL factory. This needs to be done before
     * any reading, writing, or manipulation of WSDL documents can
     * be performed using the WSDL API.
     *
     * @return An instance of the WSDL factory.
     * @exception WsdlException If the factory cannot be instantiated.
     */
    WsdlFactory getWsdlFactory() throws WsdlException; 
    
    /**
     * Used by a component to register an internal XAResource with the JBI runtime.
     * @param resource to be registered
     * @exception JBIException if something is wrong with the XAResource.
     */
    void registerXAResource(XAResource resource) throws JBIException;

    /**
     * Returns a KeyStoreUtil object that allows components to create,
     * read, update, and delete keys.  The KeyStoreUtil also allows the
     * encryption and decryption of text using the keys stored in the
     * private KeyStore.
     *
     * @return       An instance of a KeyStoreUtil or null if the KeyStoreUtil
     * service is not available
     */
    KeyStoreUtil getKeyStoreUtil();
}

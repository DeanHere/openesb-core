/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageContentModel.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

/**
 * This class enumerates the possible WSDL message content model values.
 * 
 * @author Sun Microsystems, Inc.
 */
public final class MessageContentModel
{
    /** The #ANY content model constant. */
    public static final MessageContentModel ANY =
        new MessageContentModel("#ANY");

    /** The #ELEMENT content model constant. */
    public static final MessageContentModel ELEMENT =
        new MessageContentModel("#ELEMENT");

    /** The #NONE content model constant. */
    public static final MessageContentModel NONE =
        new MessageContentModel("#NONE");

    /** 
     * Convert this context model to a string (#ANY etc.).
     * 
     * @return A string representation of this content model.
     */
    public String toString()
    {
        return mValue;
    }

    /** Value of this content model. */
    private String mValue;

    /**
     * Construct a MessageContentModel instance with the given value.
     * 
     * @param value the value to give to the new instance.
     */
    private MessageContentModel(String value)
    {
        this.mValue = value;
    }
}

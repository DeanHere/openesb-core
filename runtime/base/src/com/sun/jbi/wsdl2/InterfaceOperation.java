/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterfaceOperation.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import javax.xml.namespace.QName;
import org.w3c.dom.DocumentFragment;

/**
 * API for  WSDL 2.0 interface operation component.
 *
 * @author ApiGen AX.00
 */
public interface InterfaceOperation extends ExtensibleDocumentedComponent
{
    /**
     * Get name of this interface operation component.
     *
     * @return Name of this interface operation component
     */
    String getName();

    /**
     * Set name of this interface operation component.
     *
     * @param theName Name of this interface operation component
     */
    void setName(String theName);

    /**
     * Get message exchange pattern URI for this operation.
     *
     * @return Message exchange pattern URI for this operation
     */
    String getPattern();

    /**
     * Set message exchange pattern URI for this operation.
     *
     * @param thePattern Message exchange pattern URI for this operation
     */
    void setPattern(String thePattern);

    /**
     * Get a URI identifying the message construction rules for a message
     * reference.
     *
     * @return A URI identifying the message construction rules for a message
     * reference
     */
    String getStyle();

    /**
     * Set a URI identifying the message construction rules for a message
     * reference.
     *
     * @param theStyle A URI identifying the message construction rules for a
     * message reference
     */
    void setStyle(String theStyle);

    /**
     * Get true if the operation is safe, as defined by the W3C Web Services
     * Architecture document.
     *
     * @return True if the operation is safe, as defined by the W3C Web
     * Services Architecture document
     */
    boolean getSafe();

    /**
     * Set true if the operation is safe, as defined by the W3C Web Services
     * Architecture document.
     *
     * @param isSafe True if the operation is safe, as defined by the W3C Web
     * Services Architecture document
     */
    void setSafe(boolean isSafe);

    /**
     * Get the number of MessageReference items in inputs.
     *
     * @return The number of MessageReference items in inputs
     */
    int getInputsLength();

    /**
     * Get input message references by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Input message references at given <code>index</code> position.
     */
    MessageReference getInput(int index);

    /**
     * Set input message references by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theInput Item to add at position <code>index</code>.
     */
    void setInput(int index, MessageReference theInput);

    /**
     * Append an item to input message references.
     *
     * @param theInput Item to append to inputs
     */
    void appendInput(MessageReference theInput);

    /**
     * Remove input message references by index position.
     *
     * @param index The index position of the input to remove
     * @return The MessageReference removed, if any.
     */
    MessageReference removeInput(int index);

    /**
     * Get the number of MessageReference items in outputs.
     *
     * @return The number of MessageReference items in outputs
     */
    int getOutputsLength();

    /**
     * Get output message references by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Output message references at given <code>index</code>
     * position.
     */
    MessageReference getOutput(int index);

    /**
     * Set output message references by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theOutput Item to add at position <code>index</code>.
     */
    void setOutput(int index, MessageReference theOutput);

    /**
     * Append an item to output message references.
     *
     * @param theOutput Item to append to outputs
     */
    void appendOutput(MessageReference theOutput);

    /**
     * Remove output message references by index position.
     *
     * @param index The index position of the output to remove
     * @return The MessageReference removed, if any.
     */
    MessageReference removeOutput(int index);

    /**
     * Get the number of MessageFaultReference items in inFaults.
     *
     * @return The number of MessageFaultReference items in inFaults
     */
    int getInFaultsLength();

    /**
     * Get in fault message references by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return In fault message references at given <code>index</code>
     * position.
     */
    MessageFaultReference getInFault(int index);

    /**
     * Set in fault message references by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theInFault Item to add at position <code>index</code>.
     */
    void setInFault(int index, MessageFaultReference theInFault);

    /**
     * Append an item to in fault message references.
     *
     * @param theInFault Item to append to inFaults
     */
    void appendInFault(MessageFaultReference theInFault);

    /**
     * Remove in fault message references by index position.
     *
     * @param index The index position of the inFault to remove
     * @return The MessageFaultReference removed, if any.
     */
    MessageFaultReference removeInFault(int index);

    /**
     * Get the number of MessageFaultReference items in outFaults.
     *
     * @return The number of MessageFaultReference items in outFaults
     */
    int getOutFaultsLength();

    /**
     * Get out fault message references by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Out fault message references at given <code>index</code>
     * position.
     */
    MessageFaultReference getOutFault(int index);

    /**
     * Set out fault message references by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theOutFault Item to add at position <code>index</code>.
     */
    void setOutFault(int index, MessageFaultReference theOutFault);

    /**
     * Append an item to out fault message references.
     *
     * @param theOutFault Item to append to outFaults
     */
    void appendOutFault(MessageFaultReference theOutFault);

    /**
     * Remove out fault message references by index position.
     *
     * @param index The index position of the outFault to remove
     * @return The MessageFaultReference removed, if any.
     */
    MessageFaultReference removeOutFault(int index);

    /**
     * Get qualified name of this interface operation.
     *
     * @return Qualified name of this interface operation
     */
    QName getQualifiedName();

    /**
     * Create a new input message reference for this operation, and append it
     * to this operation's input list.
     *
     * @return The newly created input message reference.
     */
    MessageReference addNewInput();

    /**
     * Create a new output message reference for this operation, and append
     * it to this operation's output list.
     *
     * @return The newly created input message reference.
     */
    MessageReference addNewOutput();

    /**
     * Create a new messsage in-fault reference for this operation, and
     * append it to the operation's in-fault list.
     *
     * @param fault Fault referenced by the new in-fault
     * @return The newly created in-fault reference.
     */
    MessageFaultReference addNewInFault(
        InterfaceFault fault);

    /**
     * Create a new messsage out-fault reference for this operation, and
     * append it to the operation's out-fault list.
     *
     * @param fault Fault referenced by the new out-fault
     * @return The newly created out-fault reference.
     */
    MessageFaultReference addNewOutFault(
        InterfaceFault fault);

    /**
     * Return this WSDL interface operation as an XML string.
     *
     * @return This operation, serialized as an XML string.
     */
    String toXmlString();

    /**
     * Return this interface operation as a DOM document fragment. The DOM
     * subtree is a copy; altering it will not affect this service.
     *
     * @return This operation, as a DOM document fragment.
     */
    DocumentFragment toXmlDocumentFragment();

}

// End-of-file: InterfaceOperation.java

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Constants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

/**
 * This interface serves to gather WSDL 2.0 related constants together that
 * are applicable across this WSDL API, and have no logical home in a particular
 * component.
 * 
 * @author Sun Microsystems, Inc.
 */
public interface Constants 
{
    /** The XML namespace for WSDL 2.0 documents recognized by this implementation. */
    String WSDL_NAMESPACE_NAME  = "http://www.w3.org/ns/wsdl";

    /** The root for package-specific names. */
    String ROOT_PUBLIC_NAME     = "com.sun.jbi.wsdl2";

    /** The name of the logger used in the API implementation. */
    String LOGGER_NAME          = ROOT_PUBLIC_NAME + ".logger";

    /** The URI for specifying RPC operation style. */
    String STYLE_RPC            = WSDL_NAMESPACE_NAME + "/style/rpc";

    /** The URI for specifying set-attribute operation style. */
    String STYLE_SET_ATTRIBUTE  = WSDL_NAMESPACE_NAME + "/style/set-attribute";
  
    /** The URI for specifying get-attribute operation style. */
    String STYLE_GET_ATTRIBUTE  = WSDL_NAMESPACE_NAME + "/style/get-attribute";
}

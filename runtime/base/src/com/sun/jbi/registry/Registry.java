/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Registry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.registry;

/** 
 * This interface provides read/write access to the persisted JBI registry
 * through a set of query interfaces (ComponentQuery, GenericQuery, 
 * ServiceAssemblyQuery) and a update interface (Updater).
 * <br><br>
 * <b>
 * NOTE: The initial integration of this interface does not contain 
 *  any methods.  This is a checkpoint version which can be used to migrate
 *  methods in the <code>com.sun.jbi.management.registry.Registry<code>
 *  interface to IPI.
 *  </b>
 *
 * @author Sun Microsystems, Inc.
 */
public interface Registry
{
    
}

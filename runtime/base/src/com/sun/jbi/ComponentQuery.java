/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentQuery.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

import java.util.List;

/**
 * This interface provides services for query of information about Binding
 * Components, Service Engines, and Shared Libraries.
 * 
 * @author Sun Microsystems, Inc.
 */
public interface ComponentQuery
{
   /**
    * Get a list of component IDs for all registered components of a specified
    * type.
    * @param type The ComponentType: { BINDING, ENGINE, SHARED_LIBRARY, 
    * BINDINGS_AND_ENGINES, ALL }. ALL includes allcomponents regardless of their types.
    * ComponentInfo.BINDINGS_AND_ENGINES includes both bindings and engines.
    * @return A List of component IDs ( String ) of all registered components
    * of the requested type. Returns an empty list of no components were found.
    */
    List<String> getComponentIds(ComponentType type);
 
   /**
    * Get a list of component IDs for all components of a specified type with
    * a specified status.
    * @param type The ComponentType: { BINDING, ENGINE, SHARED_LIBRARY, 
    * BINDINGS_AND_ENGINES, ALL }. ALL includes allcomponents regardless of their types.
    * ComponentInfo.BINDINGS_AND_ENGINES includes both bindings and engines.
    * @param status The Component status: ComponentState{LOADED, INSTALLED, STARTED, 
    * STOPPED}
    * @return A List of component IDs ( String ) of all registered components
    * of the requested type with the requested status. Returns an empty list if
    * no components were found.
    */
    List<String> getComponentIds(ComponentType type, ComponentState status);
 
   /**
    * Get the ComponentInfo for a particular Component.
    * @param componentName The unique name of the component being retrieved.
    * @return The ComponentInfo for the requested component or null if the
    * component is not registered.
    */
    ComponentInfo getComponentInfo(String componentName);
 
   /**
    * Get the ComponentInfo for a particular Shared Library.
    * @param sharedLibraryName The unique name of the Shared Library being
    * retrieved.
    * @return The ComponentInfo for the requested Shared Library or null if the
    * Shared Library is not registered.
    */
    ComponentInfo getSharedLibraryInfo(String sharedLibraryName);

   /**
    * Get a list of component IDs that depend upon a specified Shared Library.
    * @param sharedLibraryName The unique name of the Shared Library.
    * @return A list of the component IDs of all components that depend upon the
    * Shared Library. If none are found, the list is empty.
    */
    List<String> getDependentComponentIds(String sharedLibraryName);
    
   /**
    * Get the current status of a component.
    * @param componentName The unique component name.
    * @return The current status of the component: {INSTALLED, LOADED, STARTED, STOPPED}
    * @throws javax.jbi.JBIException if the component name is not registered.
    */
    ComponentState getStatus(String componentName)
        throws javax.jbi.JBIException;
}

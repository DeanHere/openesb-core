/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestCustomClassLoader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.framework.ScaffoldPlatformContext;
import java.io.File;
import java.io.InputStream;

import java.net.URL;
import java.net.URLClassLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.jbi.JBIException;

import com.sun.jbi.JBIProvider;
/**
 * Tests for the various methods on the CustomClassLoader class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestCustomClassLoader
    extends junit.framework.TestCase
{
    /**
     * Value of the $SRCROOT environment variable
     */
    private String mSrcroot;

    /**
     * ClassLoaderFactory
     */
     private CustomClassLoader mCustomClassLoader ;

    /**
     * EnvironmentContext
     */
     private EnvironmentContext mEnvironmentContext ;
     
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestCustomClassLoader(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the ClassLoaderFactory instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mSrcroot = System.getProperty("junit.srcroot"); 
	mEnvironmentContext = new EnvironmentContext(new ScaffoldPlatformContext(), 
                new JBIFramework(), new Properties());
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }


// =====================  Custom classloader test methods ==================

    /**
     * testCreateCustomClassLoader
     * tests the creation of a CustomClassloader with the System Classloader
     * as the parent and the "selfFirst" flag set to 'false'
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateCustomClassLoader()
    {
	String testName = "testCreateCustomClassLoader";
	try
	{
            // create a set of URLs for this loader
	    String path = getTestJarsPath() + File.separator +  "a.jar" ;
            List l = new ArrayList();
	    l.add ( path ) ;
            URL[] urls = CLUtils.list2URLArray(l);
	    
	    // parent of current classloader = null --> System classloader
	    ClassLoader parent = null ;
	    boolean selfFirst = false ;
	    
	    // create a custom classloader
            CustomClassLoader ccl = new CustomClassLoader( urls , parent ,
			selfFirst);
            log ("created custom classloader with selfFirst=false") ;
	    
	    log (testName + ":passed");
	}
	catch (Exception e)
	{
            fail (testName + ":error creating custom classloader") ;
	}
    }

    /**
     * testCCLLoadSystemClassSelfFirstFalse
     * tests the loading of a class with the system classloader
     * as the parent and the "selfFirst" flag set to 'false'
     * <code>java.lang.String</code> is not overloaded locally 
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLLoadSystemClassSelfFirstFalse()
    {

	String testName = "testCCLLoadSystemClassSelfFirstFalse";
	String className = "java.util.HashMap" ; // J2SE Class
	boolean selfFirst = false ;
	
	try
	{
            // create a set of URLs for this loader
	    String path = getTestJarsPath() + File.separator +  "a.jar" ;
            List l = new ArrayList();
	    l.add ( path ) ;
            URL[] urls = CLUtils.list2URLArray(l);

	    // create Custom ClassLoader instance
            CustomClassLoader ccL = new CustomClassLoader(urls , null ,
			                               selfFirst);
            try
	    {
	        Class c = ccL.loadClass (className);
                log (testName + " Class :" + className + " was loaded by:" +
			c.getClassLoader());

		// The JVM Bootstrap ClassLoader should load "java.XXX.XXX
		// classes . Sun JVM impl represents this as null
		// @see java.lang.ClassLoader 
		if ( c.getClassLoader() == null) 
		{

	            log (testName + ":passed");
		}
		else
		{
                    fail (testName + className + " was loaded with wrong" +  
				  "  classloader =" + c.getClassLoader());
		}
	    }
	    catch (ClassNotFoundException cnfe)
	    {
                fail (testName + ":error loading class:" + className) ;
	    }
	}
	catch (Exception e)
	{
            fail (testName + e.toString() ) ;

	}
    }

    /**
     * testCCLLoadSystemClassSelfFirstTrue
     * tests the loading of a class with the system classloader
     * as the parent and the "selfFirst" flag set to 'true'
     * <code>java.lang.String</code> is not overloaded locally 
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLLoadSystemClassSelfFirstTrue()
    {

	String testName = "testCCLLoadClassSelfFirstTrue";
	String className = "java.util.HashMap" ; // J2SE Class
        boolean selfFirst = true ;
	
	try
	{
            // create a set of URLs for this loader
	    String path = getTestJarsPath() + File.separator +  "a.jar" ;
            List l = new ArrayList();
	    l.add ( path ) ;
            URL[] urls = CLUtils.list2URLArray(l);

	    // create Custom ClassLoader instance
            CustomClassLoader ccL = new CustomClassLoader(urls , null ,
			                               selfFirst);
            try
	    {
	        Class c = ccL.loadClass (className);
                log (testName + " Class :" + className + " was loaded by:" +
			c.getClassLoader());

		// The JVM Bootstrap ClassLoader should load "java.XXX.XXX
		// classes . Sun JVM impl represents this as null
		// @see java.lang.ClassLoader 
		if ( c.getClassLoader() == null) 
		{

	            log (testName + ":passed");
		}
		else
		{
                    fail (testName + className + " was loaded with wrong" +  
				  "  classloader =" + c.getClassLoader());
		}
	    }
	    catch (ClassNotFoundException cnfe)
	    {
                fail (testName + ":error loading class:" + className) ;
	    }
	}
	catch (Exception e)
	{
            fail (testName + e.toString() ) ;

	}
    }

    /**
     * testCCLLoadLocalClassSelfFirstFalse
     * tests the loading of a class with the system classloader
     * as the parent and the "selfFirst" flag set to 'false'
     * The class is visible to both the current as well as the 
     * parent (system) classloader (workflow2.jar is in the ANT
     * class path) .
     * The System (Ant) class loader should load this class . 
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLLoadLocalClassSelfFirstFalse()
    {
	String testName = "testCCLLoadLocalClassSelfFirstFalse";
	boolean selfFirst = false ;
	String expectedClassLoader = "sun.misc" ;
	String className = "com.sun.workflow.classes.Utils" ; // Custom class
	String library = "utilsv2.jar" ; // Custom library

        testCCLLoadLocalClass ( testName , 
			selfFirst , expectedClassLoader,
			library, className) ;
    }

    /**
     * testCCLLoadLocalClassSelfFirstTrue
     * tests the loading of a class with the system classloader
     * as the parent and the "selfFirst" flag set to 'true'
     * The class is visible to both the current as well as the 
     * parent (system) classloader (workflow2.jar is in the ANT
     * class path) .
     * The Custom class loader should load this class . 
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLLoadLocalClassSelfFirstTrue()
    {
	String testName = "testCCLLoadLocalClassSelfFirstTrue";
	boolean selfFirst = true ;
	String expectedClassLoader = "CustomClassLoader" ;
	String className = "com.sun.workflow.classes.Utils" ; // Custom class
	String library = "utilsv2.jar" ; // Custom library

        testCCLLoadLocalClass ( testName , selfFirst ,
			expectedClassLoader, library, className) ;
    }

    /**
     * testCCLLoadLocalClass
     * tests the loading of a class from a library (JAR) with a specific 
     * class name , value of "selfFirst" . It matches
     * the classloader used to load the class with the one
     * passed in and accordingly passes/fails the test .
     * @throws Exception if an unexpected error occurs
     */
    private void testCCLLoadLocalClass(String testName ,
		    boolean selfFirst , String expectedClassLoader,
		    String library, String className )
		    
    {
	//String className = "com.sun.workflow.classes.Utils" ; // Custom class
	// Classes in this local JAR file(utilsv2.jar) are duplicates 
	// of classes found in the System (ANT) class path i.e. utils.jar
	//String library = "utilsv2.jar" ;

	try
	{
            // create a set of URLs for this loader
	    String path = getTestJarsPath() + File.separator + library ;
            List l = new ArrayList();
	    l.add ( path ) ;
            URL[] urls = CLUtils.list2URLArray(l);

	    // create Custom ClassLoader instance
            CustomClassLoader ccL = new CustomClassLoader(urls , null ,
			                               selfFirst);
            try
	    {
	        Class c = ccL.loadClass (className);
                log (testName + " Class :" + className + " was loaded by:" + 
				c.getClassLoader() );

		ClassLoader cLoader = c.getClassLoader() ;
                 
		if (cLoader != null)  
                {
		    String clName = cLoader.toString();
		    if (clName.indexOf(expectedClassLoader) != -1)
		    {
	                log (testName + ":passed");
		    }
	    	    else
		    {
                        fail (testName + className + " was loaded with wrong" +  
		    		  "  classloader =" + clName);
		    }
		}
		// Sun JVM Bootstrap class loader is NULL
		else if ( expectedClassLoader == null)
		{
                    // if both current , expected class loaders are NULL
		    // we are probably dealing with the loading of a core 
		    // class such as java.lang.String
		    
                    log (testName + ":passed");
		}
	    }
	    catch (ClassNotFoundException cnfe)
	    {
                fail (testName + ":error loading class:" + className) ;
	    }
	}
	catch (Exception e)
	{
            CLUtils.dumpStackTrace(e);
            fail (testName + e.toString() ) ;

	}
    }


    /**
     * testCCLFilterJavaClassSelfFirstTrue
     * tests the loading of a java.XXX class with the system classloader
     * as the parent and the "selfFirst" flag set to 'true'
     * The parent class loader should load this class even though the class
     * has been overloaded and is made available in the local Class Path.
     * This local version should never be loaded .
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLFilterJavaClassSelfFirstTrue()
    {
	String testName = "testCCLFilterJavaClassSelfFirstTrue";
	boolean selfFirst = true ;
	// the JVM bootstrap class loader is represented as NULL 
	// in the Sun JVM
	String expectedStrClassLoader = null  ;
	String classNameJavaStr   = "java.lang.String";
	String classNameJavaXStr   = "javax.swing.Action";

	// DummyMarkerCLTest - marker class inside the JAR file to make sure 
	// the JAR file is actually being used . Else java.lang.String
	// could still get loaded by the correct loader but the test would
	// not be True test of filtering 
	String classNameDummy = "DummyMarkerCLTest";
	String expectedDummyClassLoader = "Custom" ;

	String javaLibrary  = "mystring.jar" ; // contains java.lang.String
	String javaxLibrary = "myjavax.jar" ;  // contains javax.swing.Action

	// if this bombs with a ClassNotFoundException, that means
	// that "mystring.jar" is not really being used .
        testCCLLoadLocalClass ( testName , selfFirst ,
			expectedDummyClassLoader, javaLibrary, classNameDummy) ;

	// check java.XXX classes
        testCCLLoadLocalClass ( testName , selfFirst ,
			expectedStrClassLoader, javaLibrary, classNameJavaStr) ;
        
	log (testName + " Java.XX test Passed") ;

	// check javax.XXX classes
        testCCLLoadLocalClass ( testName , selfFirst ,
			expectedStrClassLoader, javaxLibrary, classNameJavaXStr) ;

	log (testName + " JavaX.XX test Passed") ;
    } 
   
    /**
     * testCCLFilterWSDLSelfFirstTrue
     * tests the loading of a java.XXX class with the system classloader
     * as the parent and the "selfFirst" flag set to 'true'
     * The Custom class loader should load this class since this is a 
     * case of a javax.XXX class not being available in the parent's 
     * classloader. 
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLFilterWSDLClassSelfFirstTrue()
    {
	String testName = "testCCLFilterWSDLClassSelfFirstTrue";
	boolean selfFirst = true ;
	String expectedStrClassLoader = "CustomClassLoader" ;
	String classNameJavaXStr   = "javax.my.wsdl.xml.WSDLLocator";
	String javaxLibrary = "ws.jar"; //contains javax.my.wsdl.xml.WSDLLocator

/* NB -- commented on on 2/16/2006 so that all junit tests pass.
 * Raghavan Srinivasan has promised to (help) fix this test when he
 * gets back from vacation.   -- MYK
	// check javax.wsdl classes
        testCCLLoadLocalClass ( testName , selfFirst ,
			expectedStrClassLoader, javaxLibrary, classNameJavaXStr) ;
*/

	log (testName + " JavaX.wsdl.XX test Passed") ;
    } 

    /**
     * testCCLFilterJavaClassSelfFirstFalse
     * tests the loading of a java.XXX class with the system classloader
     * as the parent and the "selfFirst" flag set to 'false'
     * The Custom class loader should load this class . The version of class
     * has been overloaded and is made available in the local Class Path.
     * @throws Exception if an unexpected error occurs
     */
    /*public void testCCLFilterJavaClassSelfFirstFalse()
    {
	String testName = "testCCLFilterJavaClassSelfFirstFalse";
	boolean selfFirst = false ;
	// the JVM bootstrap class loader is represented as NULL 
	// in the Sun JVM
	String expectedStrClassLoader = null  ;
	String classNameJavaStr   = "java.lang.String";
	String classNameJavaXStr   = "javax.swing.Action";

	// DummyMarkerCLTest - marker class inside the JAR file to make sure 
	// the JAR file is actually being used . Else java.lang.String
	// could still get loaded by the correct loader but the test would
	// not be True test of filtering 
	String classNameDummy = "DummyMarkerCLTest";
	String expectedDummyClassLoader = "Custom" ;

	String library = "mystring.jar" ; // Custom library

	// if this bombs with a ClassNotFoundException, that means
	// that "mystring.jar" is not really being used .
        testCCLLoadLocalClass ( testName , selfFirst ,
			expectedDummyClassLoader, library, classNameDummy) ;

	// check java.XXX classes
        testCCLLoadLocalClass ( testName , selfFirst ,
			expectedStrClassLoader, library, classNameJavaStr) ;
        
	log (testName + " Java.XX test Passed") ;

	// check javax.XXX classes
        testCCLLoadLocalClass ( testName , selfFirst ,
			expectedStrClassLoader, library, classNameJavaXStr) ;
	log (testName + " JavaX.XX test Passed") ;
    } */

    /**
     * testCCLLoadLocalResource
     * tests the loading of a resource from a library (JAR) with a specific 
     * class name , value of "selfFirst". It matches
     * the classloader used to load the class with the one
     * passed in and accordingly passes/fails the test .
     * @throws Exception if an unexpected error occurs
     */
    private void testCCLLoadLocalResource(String testName ,
		    boolean selfFirst , String key , String expectedVal,
		    String library, String resourceName )
		    
    {
	// Resources in this local JAR file(resources.jar) are duplicates 
	// of classes found in the System (ANT) class path i.e. resources.jar

	try
	{
            // create a set of URLs for this loader
	    String path = getTestResourcePath() ;
            List l = new ArrayList();
	    l.add ( path ) ;
	    if (library !=null)
	    {
		   String lPath = getTestJarsPath() + java.io.File.separator + library;
		   System.err.println ("LPATH=" + lPath);
		   l.add (lPath) ;
	    }

            URL[] urls = CLUtils.list2URLArray(l);

	    // create Custom ClassLoader instance
            CustomClassLoader ccL = new CustomClassLoader(urls , null ,
			                               selfFirst);
	    InputStream i = ccL.getResourceAsStream(resourceName);
	    if (i == null)
	    {
                fail ("Resource:" + resourceName + " was not loaded") ;
	    }

	    Properties p = new Properties();
	    p.load(i); 

            String keyVal = p.getProperty(key) ;
	    log (testName + "Resource Val:" + key + "=" + keyVal) ;
             
	    boolean failMe = false ;

	    // Java Bootstrap classloader  is represented
	    // as null
	    if (keyVal == null)
	    {
                if (expectedVal == null)
		{
                   // test passes  
		}
		else // expectedVal == false
		{
                    failMe = true ;
		}
	    }
	    else // keyVal !=null
	    {
                if (expectedVal ==null)
		{
                    failMe = true ;
		}
		else // expectedVal !=null
		{
                    failMe = !(keyVal.equals(expectedVal)) ;
		}
	    }

            if (failMe)
	    {
               fail (testName + resourceName + " was loaded with wrong" +  
	           "  classloader:" + keyVal);
	    }
		
            log (testName + " Resource :" + resourceName + " was loaded") ;
            log (testName + ":passed");
	}
	catch (Exception e)
	{
            CLUtils.dumpStackTrace(e);
            fail (testName + ":error loading class:" + resourceName + " due to:" + e.toString()); ;

	}
    }

    /**
     * testCCLLoadLocalResourceSelfFirstTrue
     * tests the loading of a resource with the system classloader
     * as the parent and the "selfFirst" flag set to 'true'
     * The resource is visible to both the current as well as the 
     * parent (system) classloader (resources.jar is in the ANT
     * class path) .
     * The Custom class loader should load this resource . 
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLLoadLocalResourceSelfFirstTrue()
    {
	String testName = "testCCLLoadLocalResourceSelfFirstTrue";
	boolean selfFirst = true ;
	String key = "PHONE_NUMBER" ;
	String expectedVal = "(###)###-####" ;
	String resName = "resources.properties" ; // Custom resource
	String library = null;         // Custom library


        testCCLLoadLocalResource(testName ,selfFirst , 
			key , expectedVal, library, resName );
    }

    /**
     * testCCLLoadLocalResourceSelfFirstFalse
     * tests the loading of a resource with the system classloader
     * as the parent and the "selfFirst" flag set to 'false'
     * The resource is visible to both the current as well as the 
     * parent (system) classloader (resources.jar is in the ANT
     * class path) .
     * The System (sun.misc.XXX) class loader should load this resource . 
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLLoadLocalResourceSelfFirstFalse()
    {
	String testName = "testCCLLoadLocalResourceSelfFirstFalse";
	boolean selfFirst = false ;
	String key = "PHONE_NUMBER" ;
	String expectedVal = null ;
	String resName = "resources.properties" ; // Custom resource
	String library = null;         // Custom library

        testCCLLoadLocalResource(testName ,selfFirst , 
			key , expectedVal, library, resName );
    }

    /**
     * testCCLLoadLocalJavaxResourceSelfFirstFalse
     * tests the loading of a resource with the system classloader
     * as the parent and the "selfFirst" flag set to 'false'. 
     * The resource is present in the component class path only.
     * The resource is a javax resource which means that the 
     * parent-first is always enforced.
     *
     * The Custom class loader should load this class . 
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLLoadLocalJavaxResourceSelfFirstFalse()
    {
	String testName = "testCCLLoadLocalJavaxResourceSelfFirstFalse";
	boolean selfFirst = false ;
	String key = "PRODUCT_NAME" ;
	String expectedVal = "JBI" ;
	String resName = "javax/test/resources.properties" ; // Custom resource
	String library = "image4j.jar" ;         // Custom library

        testCCLLoadLocalResource(testName ,selfFirst , 
			key , expectedVal, library, resName );
    }

    /**
     * testCCLLoadLocalJavaxResourceSelfFirstTrue
     * tests the loading of a resource with the system classloader
     * as the parent and the "selfFirst" flag set to 'true'. 
     * The resource is present in the component class path only.
     * The resource is a javax resource which means that the 
     * parent-first is always enforced.
     *
     * The Custom class loader should load this class . 
     * @throws Exception if an unexpected error occurs
     */
    public void testCCLLoadLocalJavaxResourceSelfFirstTrue()
    {
	String testName = "testCCLLoadLocalJavaxResourceSelfFirstTrue";
	boolean selfFirst = true ;
	String key = "PRODUCT_NAME" ;
	String expectedVal = "JBI" ;
	String resName = "javax/test/resources.properties" ; // Custom resource
	String library = "image4j.jar" ;         // Custom library

        testCCLLoadLocalResource(testName ,selfFirst , 
			key , expectedVal, library, resName );
    } 
    //-------------------------------------------------
    // private methods -- refactor into Util class
    //
    /*  
     * private logging method
     */ 
    private static void log ( String msg )
    {
        System.out.println ("[TestCustomClassLoader]-" + msg) ;
    }

    /*  
     * private method
     */ 
    private String getTestJarsPath ()
    {
        return ( CLUtils.getTestJarsPath (mSrcroot) ) ;
    }

    /*  
     * private method
     */ 
    private String getTestResourcePath ()
    {
        return ( CLUtils.getTestResourcePath (mSrcroot) ) ;
    }

    /*  
     * private logging method to indicate start of test
     */ 
    private void start (String testName)
    {
        log (testName + "-" + "Started") ;
    }

    /*  
     * private logging method to indicate normal start of test
     */ 
    private void endOK (String testName)
    {
        log (testName + "-" + "Ended OK") ;
    }

    /*  
     * private logging method to indicate end of test due to an exception
     */ 
    private void endException (String testName , Exception e)
    {
        log (testName + "-" + "Ended With Following Exception") ;
        log (e.toString()) ;
    }


    private String dump (Class c)
    {
        String source = c.getProtectionDomain().getCodeSource().toString();
        System.out.println ("Class :" + c + " was loaded from :" +
			source);
	return source;
    }
}

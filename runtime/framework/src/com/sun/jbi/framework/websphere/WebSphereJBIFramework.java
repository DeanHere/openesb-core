/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WebSphereJBIFramework.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.websphere;

import com.sun.jndi.rmi.registry.RegistryContextFactory;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;
import javax.transaction.TransactionManager;
import javax.naming.InitialContext;

/**
 *  JBI framework wrapper for WebSphere
 *  <br><br>
 *  A JSEJBIFramework instance cannot be loaded multiple times in the same
 *  VM.  If multiple instances of the framework are required in a VM, 
 *  instantiate multiple instances of JSEJBIFramework and load each one
 *  independently.  There is no limit on the number of uniquely named
 *  JSEJBIFramework instances in the same VM.  A specific JSEJBIFramework instance 
 *  can be loaded and unloaded multiple times in a VM.
 *
 *  @author Sun Microsystems, Inc.
 */
public class WebSphereJBIFramework 
        extends com.sun.jbi.framework.JBIFramework
        implements WebSphereJBIFrameworkMBean
{
    /** key for the install root property **/
    public static final String INSTALL_ROOT = "install.root";
    
    /** key for the instance name property **/
    public static final String INSTANCE_NAME = "instance.name";
    
    /** Configuration defaults. */
    private static final String DEFAULT_INSTALL_ROOT =
            System.getProperty("user.dir");
    private static final String DEFAULT_INSTANCE_NAME = 
            "server";
    
    private WebSpherePlatformContext  mPlatformContext;
    private boolean             mLoaded;
    private Properties          mEnvironment;
    private Logger              mLog = 
            Logger.getLogger(this.getClass().getPackage().getName());
        
    /** 
     * Creates a new instance of the JBI framework.
     * @param environment the properties from servlet context
     */
    public WebSphereJBIFramework(Properties environment)
    {
        super();
        
        mEnvironment = environment;
        mPlatformContext = new WebSpherePlatformContext(
                mEnvironment.getProperty(INSTANCE_NAME, DEFAULT_INSTANCE_NAME),
                mEnvironment.getProperty(INSTALL_ROOT, DEFAULT_INSTALL_ROOT));
    }
    
    /**
     * Load the JBI framework with the specified environment.  When this method
     *  retuns, all public interfaces and system services have completely 
     *  initialized.  If a connector port is specified in the environment 
     *  properties, a remote JMX connector server is created.
     *  @throws Exception failed to load JBI framework
     */
    public synchronized void load()
        throws Exception
    {
        try {
  
        if (mLoaded)
        {
            throw new IllegalStateException("JBI framework already loaded!");
        }
        
        // Register a management MBean for this framework instance
        ObjectName fwMBeanName = new ObjectName("com.sun.jbi", 
                "instance", mPlatformContext.getInstanceName());
        MBeanServer mbs = mPlatformContext.getMBeanServer();

        if (mbs.isRegistered(fwMBeanName))
        {
            if (mbs.getAttribute(fwMBeanName, "Loaded").equals(Boolean.TRUE))
            {
                // Framework already loaded from a separate thread/process
                throw new IllegalStateException("JBI framework instance " + 
                        mPlatformContext.getInstanceName() + " has already been loaded");
            }
            else
            {
                // MBean should not be registered - try to clean up
                mbs.unregisterMBean(fwMBeanName);                
            }
        }
        try
        {
            mbs.registerMBean(this, fwMBeanName);
        } catch (Exception ex)
        {
            mLog.log(Level.SEVERE, ex.getMessage(), ex);
        }
  
        mEnvironment.setProperty("com.sun.jbi.home", 
                mPlatformContext.getInstallRoot());
        init(mPlatformContext, mEnvironment);        
        startup(mPlatformContext.getNamingContext(), "");       
        prepare();
        ready(true); 
     
        // JBI framework has been loaded
        mLoaded = true;
        } catch (Exception ex)
        {
            mLog.severe(ex.getMessage());
        }
    }
    
    /** Queries the state of the JBI Framework.
     *  @return true if the JBI framework is loaded, false otherwise.
     */
    public boolean isLoaded()
    {
        return mLoaded;
    }
    
    /**
     * Unloads the JBI framework.  When this method retuns, all 
     *  public interfaces, system services, and JMX connector (if configured)
     *  have been destroyed.
     * @throws java.lang.Exception failed to unload JBI framework
     */
    public synchronized void unload()
        throws Exception
    {
        if (!mLoaded)
        {
            return;
        }
        
        shutdown();
        terminate();
        
        mLoaded = false;
    }
    
    /**
     * This method returns the JMX Service URL for this server
     * @throws java.lang.Exception if not able to get MBeanServerConnection
     * @return MBeanServerConnection a connection to the MBeanServer
     */ 
    public MBeanServerConnection getMBeanServerConnection()
        throws Exception
    {
        return mPlatformContext.getMBeanServerConnection(null);
    }    
    
 
}

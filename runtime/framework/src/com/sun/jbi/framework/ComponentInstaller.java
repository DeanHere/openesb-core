/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentInstaller.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentState;
import com.sun.jbi.component.InstallationContext;
import com.sun.jbi.management.InstallerMBean;
import javax.management.ObjectName;

/**
 * This class implements the InstallerMBean for a Component (BC or SE).
 * This MBean acts as an agent between the JMX management service and the
 * Component Framework to allow the Installer Service to control the
 * installation of the component.
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentInstaller implements InstallerMBean
{
    /**
     * ComponentFramework handle.
     */
    private ComponentFramework mComponentFramework;

    /**
     * ComponentRegistry handle.
     */
    private ComponentRegistry mComponentRegistry;

    /**
     * InstallationContext handle.
     */
    private InstallationContext mInstallContext;

    /**
     * Constructor.
     * @param installContext - the installation context for this component.
     * @param componentFramework - the ComponentFramework handle.
     * @param componentRegistry - the ComponentRegistry handle.
     */
    ComponentInstaller(InstallationContext installContext,
                       ComponentFramework componentFramework,
                       ComponentRegistry componentRegistry)
    {
        mInstallContext = installContext;
        mComponentFramework = componentFramework;
        mComponentRegistry = componentRegistry;
    }

    /**
     * Get the installer configuration MBean name for this component.
     * @return the MBean object name of the Installer Configuration MBean.
     * @throws javax.jbi.JBIException if the component is not in the LOADED
     * state or any error occurs during processing.
     */
    public ObjectName getInstallerConfigurationMBean()
        throws javax.jbi.JBIException
    {
        return mComponentFramework.getInstallerConfigurationMBeanName(
            mInstallContext.getComponentName());
    }

    /**
     * Get the installation root directory path for this component.
     * @return the full installation path of this component.
     */
    public String getInstallRoot()
    {
        return mInstallContext.getInstallRoot();
    }

    /**
     * Install this component.
     * @return JMX ObjectName representing the ComponentLifeCycleMBean for this
     * component.
     * @throws javax.jbi.JBIException if the installation fails.
     */
    public ObjectName install()
        throws javax.jbi.JBIException
    {
        return mComponentFramework.installComponent(mInstallContext);
    }

    /**
     * Determine whether or not this component is installed.
     * @return true if this component is currently installed, false if not.
     */
    public boolean isInstalled()
    {
        ComponentState s;
        try
        {
            s = mComponentRegistry.getStatus(mInstallContext.getComponentName());
        }
        catch ( javax.jbi.JBIException ex )
        {
            return false;
        }
        if ( s == ComponentState.LOADED )
        {
            return false;
        }
        return true;
    }

    /**
     * Uninstall this component. This completely removes the component from the
     * JBI system.
     * @throws javax.jbi.JBIException if the uninstallation fails.
     */
    public void uninstall()
        throws javax.jbi.JBIException
    {
        uninstall(false);
    }

    /**
     * Uninstall this component. This completely removes the component from the
     * JBI system. If the <code>force</code> flag is set to <code>true</code>,
     * the uninstall proceeds regardless of any exception thrown by the
     * component.
     *
     * @param force set to <code>true</code> to uninstall even if errors occur.
     * @throws javax.jbi.JBIException if the uninstallation fails.
     */
    public void uninstall(boolean force)
        throws javax.jbi.JBIException
    {
        mComponentFramework.uninstallComponent(mInstallContext, force);
    }
}

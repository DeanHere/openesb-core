/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ClassLoaderFactory.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.platform.PlatformContext;

import java.io.File; 
import java.net.URL; 
import java.util.ArrayList; 
import java.util.HashMap; 
import java.util.Iterator; 
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger; 

import javax.jbi.JBIException;

/**
 * This is the implementation of the ClassLoaderFactory.
 * It acts as a factory  and cache to create and retrieve different types of
 * classloaders and classloader chains for use in the JBI Framework. Examples
 * include Component ClassLoaders, Bootstrap Classloaders, Shared Classloaders.
 *  
 * @author Sun Microsystems, Inc.
 */
public final class ClassLoaderFactory
{        
    /** 
     * Component classloaders
     */  
    private HashMap mComponentClassLoaderCache;

    /** 
     * Shared classloaders 
     */  
    private HashMap mSharedClassLoaderCache;

    /** 
     * Handle to StringTranslator for message translation
     */  
    private StringTranslator mTranslator;

    /** 
     * Handle to the environment logger 
     */  
    private Logger mLogger;

    /** 
     * Classloader factory
     */  
    private static ClassLoaderFactory sClassLoaderFactory;

    /** 
     * Constructor
     * Private constructor to facilitate a singleton.
     */
    private ClassLoaderFactory() 
    { 
        mComponentClassLoaderCache = new HashMap();
        mSharedClassLoaderCache    = new HashMap();
        EnvironmentContext envCtx  = EnvironmentContext.getInstance();
        mTranslator = (StringTranslator) envCtx.getStringTranslatorFor(this);
        mLogger     = (Logger) envCtx.getLogger();
    }
    
    /**
     * Returns a singleton instance of the ClassLoaderFactory class.
     *
     * @return The instance of the ClassLoaderFactory 
     */
    public static ClassLoaderFactory getInstance() 
    { 
        if (null == sClassLoaderFactory)
        {
            sClassLoaderFactory = new ClassLoaderFactory(); 
        }
        return sClassLoaderFactory;
    }

    /**
     * Creates and returns the installation bootstrap classloader for a JBI
     * component.
     * This instance is not cached by the ClassLoaderFactory as these are 
     * created and used infrequently. The bootstrap classloader is used to 
     * bootstrap the installation of a binding/engine.
     *
     * @param component runtime representation of the component.
     * @throws javax.jbi.JBIException if the classloader could not be created.
     * @throws java.lang.IllegalArgumentException if a null or empty list of
     * class path elements is found.
     * @return An instance of the Bootstrap classloader.
     */
    public ClassLoader createBootstrapClassLoader(Component component) 
        throws JBIException
    {
        ClassLoader cl;

        if (null == component)
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "component"));
        }

        List bootCpElements = component.getBootstrapClassPathElements();

        if ( null == bootCpElements )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "paths"));
        }
        if ( bootCpElements.isEmpty() )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_LIST_ARGUMENT, "paths"));
        }

        try
        {
            URL[] urls = list2URLArray(bootCpElements);
               
            // Set the "selfFirst" flag to what is set in the component.
            boolean selfFirst = component.isBootstrapClassLoaderSelfFirst();

            // The parent classloader is the JBI shared class loader that has
            // access to all of the public JBI APIs and SPIs.

            cl = new CustomClassLoader(urls, getJBISystemClassLoader(), selfFirst);

            mLogger.finer("Created bootstrap CustomClassLoader for " +
                component.getComponentTypeAsString() + " " +
                component.getName() + " with class path " + bootCpElements);
            return cl;
        }
        catch (Throwable e)
        {
            throw new JBIException(mTranslator.getString(
                LocalStringKeys.CL_BOOTSTRAP_CREATE_FAILED, e.toString()), e);
        }
    }

    /**
     * Creates and returns the classloader for a Shared Library.
     * This classloader is used to load classes belonging to libraries
     * that are shared between two or more JBI components.
     * @param  sharedLib A SharedLibrary object representing the library.
     * @throws javax.jbi.JBIException If the shared classloader cannot be
     * successfully created.
     * @throws java.lang.IllegalArgumentException If a null argument is
     * received.
     * @return The shared library classloader.
     */
    public ClassLoader createSharedClassLoader( SharedLibrary sharedLib) 
        throws JBIException
    {
        ClassLoader cl;

        if ( null == sharedLib )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "sharedLib"));
        }

        try
        {
            cl = (ClassLoader) mSharedClassLoaderCache.get(sharedLib.getName());
            if ( null == cl )
            {
                List slPaths = sharedLib.getClassPathElements();
                URL[] urls = list2URLArray( slPaths );

                // Set the "selfFirst" flag to what is set for the Shared
                // Library.
                boolean selfFirst = sharedLib.isClassLoaderSelfFirst();

                // The parent classloader is the JBI shared class loader that
                // has access to all of the public JBI APIs and SPIs.

                cl = new CustomClassLoader( urls, getJBISystemClassLoader(),
                    selfFirst);
                mSharedClassLoaderCache.put( sharedLib.getName(), cl );
            }
            mLogger.finer("Created shared CustomClassLoader for " +
                sharedLib.getName() + " with class path " +
                sharedLib.getClassPathElements());
            return cl;
        }
        catch (Throwable e)
        {
            throw new JBIException(mTranslator.getString(
                LocalStringKeys.CL_SHARED_CREATE_FAILED, e.toString()), e);
        }
    }

    /**
     * Destroys the classloader for a Shared Library.
     * This method should be called only when the corresponding Shared
     * Library is being destroyed. There should never be a condition where
     * a shared classloader does not exist for an active Shared Library.
     * @param  sharedLibId Shared Library ID.
     * @throws javax.jbi.JBIException If a shared classloader cannot be removed.
     * @throws java.lang.IllegalArgumentException If a null argument is received.
     */
    public void removeSharedClassLoader( String sharedLibId) 
        throws JBIException
    {
        CustomClassLoader cl;

        if ( null == sharedLibId )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "sharedLibId"));
        }
        if ( sharedLibId.length() == 0 )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "sharedLibId"));
        }

        cl = (CustomClassLoader) mSharedClassLoaderCache.remove( sharedLibId );
        
        if (null != cl)
        {
            cl.releaseResources();
            mLogger.finer("Removed shared CustomClassLoader for " + sharedLibId);
        }
        else
        {
            throw new JBIException(mTranslator.getString(
                LocalStringKeys.CL_SHARED_CLASSLOADER_NOT_FOUND, sharedLibId));
        }
    }

    /**
     * Returns the classloader for a Shared Library.
     * This shared class loader is shared between two or more components 
     * installed in the JBI namespace.
     * This method is typically called by components such as NMR after the 
     * initial class loader creation by the ComponentFramework. Hence there
     * should always be a valid classloader for a valid Shared Library.
     * If this method throws an exception,it probably means that the library
     * was not installed correctly or that an invalid Shared Library ID was
     * passed in.
     *
     * @param sharedLibId The id of the Shared Library.
     * @throws JBIException If there is no class loader available for the 
     * Shared Library. 
     * @return The Shared Library classloader.
     */
    public ClassLoader getSharedClassLoader( String sharedLibId ) 
        throws JBIException
    {
        if ( null == sharedLibId )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "sharedLibId"));
        }
        if ( sharedLibId.length() == 0 )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "sharedLibId"));
        }

        ClassLoader sharedLoader = (ClassLoader) mSharedClassLoaderCache.
                                                         get(sharedLibId);
        if (null == sharedLoader)
        {
            throw new JBIException(mTranslator.getString(
                LocalStringKeys.CL_SHARED_CLASSLOADER_NOT_FOUND, sharedLibId));
        }

        return sharedLoader;
    }

    /**
     * Creates and returns the classloader for a JBI component.
     * It is used to load classes in the classpath of a component installed
     * in the JBI namespace. It is important that all shared libraries that
     * are referenced by this component are already installed by the time
     * a call to this method is made as it needs these to create the
     * <code>DelegatingClassLoader</code> chain.
     *
     * NOTE: An enhancement has been made to allow dynamic addition of
     * libraries to the component's class path. This is done by creating
     * a directory in the JBI install root called "libext/<comp>" where
     * "<comp>" is the component name. Jar and zip files to be added to the 
     * component's class path can be placed in that directory. Only files with
     * names ending in ".jar" or ".zip" are picked up, and the files must be
     * readable and must not be hidden files. Non-jar/zip files, non-readable
     * files, and hidden files are ignored. The files are added in the
     * order in which the File.listFiles() method returns them. If specific
     * ordering is required, the way to achieve that is to use a top-level
     * jar file with a classpath in its manifest, and to put the actual
     * jar files in a subdirectory referenced by the top-level jar file.
     * Note that the original component's class path entries always remain
     * in their original order and the added entries always follow the
     * original entries. This is required to prevent user-added libraries
     * from causing malfunctions of the component.
     *
     * @param component reference to the component object
     * @throws java.lang.SecurityException If the current thread does not have 
     * permissions to create a class loader.
     * @throws JBIException if class loader cannot be created.
     * create a class loader.
     * @throws IllegalArgumentException if a null component is passed in
     * @return The component classloader.
     */
    public ClassLoader createComponentClassLoader( Component component)
        throws JBIException
    {
        ClassLoader compClassLoader;

        if (null == component)
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "component"));
        }

        try
        {
            compClassLoader = (ClassLoader) mComponentClassLoaderCache.get(
                 component.getName());
            if (null == compClassLoader)
            {
                List lifeCycleCpElements = addUserLibs(component.getName(),
                    component.getClassPathElements());
                URL[] cpURLs = list2URLArray( lifeCycleCpElements );

                // Set the "selfFirst" flag to what is set for the component.
                boolean selfFirst = component.isClassLoaderSelfFirst();

                // Create a DelegatingClassLoader instance, which is the parent
                // of the component classloader.
                DelegatingClassLoader dcl = createDelegatingClassLoader(component);
                compClassLoader = new CustomClassLoader( cpURLs, dcl, selfFirst );
                mComponentClassLoaderCache.put( component.getName(),
                       compClassLoader );
                mLogger.finer("Created life cycle CustomClassLoader for " +
                    component.getComponentTypeAsString() + " " +
                    component.getName() + " with class path " +
                    lifeCycleCpElements);
            }
        }
        catch (JBIException je)
        {
            throw je;
        }
        catch (Throwable e) 
        {
            throw new JBIException(mTranslator.getString(
                LocalStringKeys.CL_COMPONENT_CREATE_FAILED, e.toString()), e);
        }
        return compClassLoader;
    }

    /**
     * Removes the classloader of a JBI component.
     * This should be called when a component is being shut down.
     *
     * @param componentId The component ID
     * @throws JBIException If the classloader cannot be removed.
     * @throws IllegalArgumentException If a null component ID is passed in.
     */
    public void removeComponentClassLoader( String componentId)
        throws JBIException
    {
        CustomClassLoader compClassLoader;

        if ( null == componentId )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentId"));
        }
        if ( componentId.length() == 0 )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "componentId"));
        }

        compClassLoader = (CustomClassLoader) mComponentClassLoaderCache.remove(
            componentId);
        if (null != compClassLoader)
        {
            compClassLoader.releaseResources();
            mLogger.finer("Removed life cycle CustomClassLoader for " +
                componentId);
        }
        else
        {
            throw new JBIException(mTranslator.getString(
                LocalStringKeys.CL_COMPONENT_CLASSLOADER_NOT_FOUND, componentId));
        }
    }

    /**
     * Returns the classloader of a JBI component (binding or engine).
     * This method is typically called by components such as NMS after the 
     * initial class loader creation by the ComponentFramework. Hence there
     * should always be a valid classloader for a valid component. If this
     * method throws an exception,it probably means that the component was not
     * installed correctly or that an invalid component id was passed in.
     *
     * @param  componentId the id of the binding or engine
     * @throws JBIException If there is no class loader available for the
     * component. 
     * @return The classloader for the component.
     */
    public ClassLoader getComponentClassLoader( String componentId )
        throws JBIException
    {
        if ( null == componentId )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "componentId"));
        }
        if ( componentId.length() == 0 )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.EMPTY_STRING_ARGUMENT, "componentId"));
        }

        ClassLoader compLoader = (ClassLoader) mComponentClassLoaderCache.
            get(componentId);
        if (null == compLoader)
        {
           throw new JBIException(mTranslator.getString(
               LocalStringKeys.CL_COMPONENT_CLASSLOADER_NOT_FOUND, componentId));
        }
        return compLoader;
    }

    /**
     * Private method to create a DelegatingClassLoader. This class loader has
     * a chain of all Shared Library class loaders required by the component.
     *
     * @param comp the Component instance for the component.
     * @return an instance of DelegatingClassLoader that has access to all
     * the Shared Library class loaders required for the component.
     * @throws Exception If the class loader creation is unsucessful.
     */
    private DelegatingClassLoader createDelegatingClassLoader (Component comp)
        throws Exception
    {
        mLogger.finest("Creating DelegatingClassLoader for " +
            comp.getComponentTypeAsString() + " " +
            comp.getName());

        DelegatingClassLoader dcl =
            new DelegatingClassLoader(getJBISystemClassLoader());

        List sharedLibraries = comp.getSharedLibraryNames();

        // Add any Shared Library class loaders required by the component.

        if ( null != sharedLibraries)
        {
            for ( Iterator slItr = sharedLibraries.listIterator(); slItr.hasNext();)
            {
                String slName = (String) slItr.next(); 
                ClassLoader sharedLibLoader =
                    getSharedClassLoader( slName );

                // set a reference to the SharedLibrary for this component
                // into the Delegating ClassLoader

                dcl.addSharedClassLoader( sharedLibLoader );
                mLogger.finer("Added shared CustomClassLoader for " +
                    slName + " to DelegatingClassLoader");
            }
        }
        return dcl;
    }

    /**
     * Private method to convert a List into a URL array.
     *
     * @param paths list of String elements representing paths and JAR file
     * names
     * @return java.net.URL[] array representing the URLs corresponding to the
     * paths, or null if the list is null or if there is an exception creating
     * the array.
     * @throws Exception If the array creation is unsucessful.
     */
    private URL[] list2URLArray (List paths) throws Exception
    {
        File f;
        URL[] urls = new URL[paths.size()];
        int i = 0;
        for (Iterator itr = paths.listIterator(); itr.hasNext();)
        {
            String nextCPElement = (String) itr.next();
            f = new File(nextCPElement);
            urls[i++] = f.toURL();
        }
        return urls;
    }

    /**
     * Private method to add user libraries to a component class loader.
     * No exception is thrown by this method; if any exception occurs it
     * is logged and processing continues.
     *
     * @param compName the name of the component
     * @param paths list of String elements representing paths and JAR file
     * names
     * @return a list of String elements representing the original paths
     * and any additional jar files found in the libext/<compName> directory.
     */
    private List<String> addUserLibs (String compName, List<String> paths)
    {
        List<String> elements = paths;

        // Check for user-provided additional libraries in the
        // $INSTALL_ROOT/libext/<componentname> directory. If any are
        // found, add them to the end of the array of paths.

        String ext = EnvironmentContext.getInstance().getJbiInstallRoot() +
            File.separator + "libext" + File.separator + compName;
        File extFile = new File(ext);
        if ( extFile.exists() )
        {
            mLogger.finer("found class path extension directory " + ext);
            File[] extLibs = extFile.listFiles();
            if ( null != extLibs )
            {
                mLogger.finer("found files in class path extension directory");
                elements = new ArrayList(paths);
                for ( int i = 0; i < extLibs.length; i++ )
                {
                    File extLib = extLibs[i];
                    try
                    {
                        String name = extLib.getCanonicalPath();
                        mLogger.finest("checking file " + name);
                        if ( extLib.isFile() && extLib.canRead() && !extLib.isHidden() )
                        {
                            if ( name.endsWith(".jar") || name.endsWith(".zip") )
                            {
                                elements.add(name);
                                mLogger.finest(name + "added to class path");
                            }
                        }
                    }
                    catch ( java.io.IOException ioEx )
                    {
                        mLogger.log(Level.WARNING, mTranslator.getString(
                            LocalStringKeys.CL_COMPONENT_EXTENSION_ERROR,
                            extLib.getName()), ioEx);
                    }
                }
                if ( mLogger.isLoggable(Level.FINE) )
                {
                    mLogger.fine("Component " + compName +
                        " original class path:");
                    for ( String entry : paths )
                    {
                        mLogger.fine("     " + entry);
                    }
                    mLogger.fine("Component " + compName +
                        " updated class path:");
                    for ( String entry : elements )
                    {
                        mLogger.info("     " + entry);
                    }
                }
            }
        }
        return elements;
    }

    /**
     * Private method that returns the JBI "System class loader". This method
     * delegates to the PlatformContext for the current platform to obtain
     * the appropriate class loader.
     *
     * @return ClassLoader representing the JBI common class loader.
     * @throws SecurityException If access to the class loader was denied.
     */
    private ClassLoader getJBISystemClassLoader()
    {
        ClassLoader systemClassLoader = null;

        PlatformContext platform =
            EnvironmentContext.getInstance().getPlatformContext();
        systemClassLoader = platform.getSystemClassLoader();

        // Special case for unit testing only
        if (null != System.getProperty("junit.srcroot"))
        {
            systemClassLoader = this.getClass().getClassLoader(); 
        }

        return systemClassLoader;
    }
}

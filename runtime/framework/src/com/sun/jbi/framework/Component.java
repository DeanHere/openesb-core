/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Component.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.net.URLEncoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This class holds information about a Binding Component (BC) or a Service
 * Engine (SE). This information is used by the JBI framework to install and
 * manage BCs and SEs. Some of the information is permanent, and is persisted
 * by the Component Registry across restarts of the JBI framework. Other
 * information is only relevant for the duration of a JBI framework lifecycle.
 * All attributes that are not persisted are marked <CODE>transient</CODE>.
 *
 * @author Sun Microsystems, Inc.
 */
public class Component
    implements com.sun.jbi.ComponentInfo
{
    //
    // Data items that are persisted across JBI restarts.
    //

   /**
    * Indicator as to whether the bootstrap class loader is to be "inverted".
    * Its value is true if the search order for the bootstrap class loader is
    * to be inverted relative to the normal search order - self-first rather
    * than parent-first. This is persisted across restarts of the JBI framework.
    */
    private boolean mBootstrapClassLoaderSelfFirst;
 
   /**
    * The name of the class that implements the
    * <CODE>javax.jbi.component.Bootstrap</CODE> interface
    * for this BC or SE. This is persisted across restarts of the JBI framework.
    */
    private String mBootstrapClassName;

   /**
    * The class path that the bootstrap implementation needs in its runtime
    * environment, stored as an array of elements. Each element is a
    * <CODE>String</CODE>
    * containing an absolute path to a jar file or a directory that contains
    * class files. This is persisted across restarts of the JBI framework.
    */
    private ArrayList mBootstrapClassPathElements;
 
   /**
    * Indicator as to whether the component class loader is to be "inverted".
    * Its value is true if the search order for the component class loader is
    * to be inverted relative to the normal search order - self-first rather
    * than parent-first. This is persisted across restarts of the JBI framework.
    */
    private boolean mComponentClassLoaderSelfFirst;
 
   /**
    * The name of the class that implements the
    * <CODE>javax.jbi.component.Component</CODE>
    * interface for this BC or SE. This is persisted across restarts of the
    * JBI framework.
    */
    private String mComponentClassName;

   /**
    * The class path that the component implementation needs in its
    * runtime environment, stored as an array of elements. Each element
    * represents either an absolute path to a jar file or a directory
    * that contains class files. This is persisted across restarts of
    * the JBI framework.
    */
    private ArrayList mComponentClassPathElements;
 
   /**
    * The current state of this BC or SE. This is persisted across restarts
    * of the JBI framework. Valid states are:
    * <UL>
    * <LI><CODE>LOADED</CODE> - the component's installer has been loaded
    * but the installation has not yet completed</LI>
    * <LI><CODE>SHUTDOWN</CODE> - the component is installed but is not
    * currently initialized (this is what the JBI specification refers to as
    * <CODE>SHUTDOWN</CODE> state)</LI>
    * <LI><CODE>STOPPED</CODE> - the component has been initialized but either
    * has not yet been started or has been stopped after a previous start</LI>
    * <LI><CODE>STARTED</CODE> - the component has been started and is able
    * to process requests</LI>
    * </UL>
    * <BR>
    * The state changes that are allowed are:
    * <UL>
    * <LI><CODE>LOADED</CODE> --&GT <CODE>SHUTDOWN<CODE/></LI>
    * <LI><CODE>SHUTDOWN</CODE> --&GT <CODE>STOPPED</CODE></LI>
    * <LI><CODE>STOPPED</CODE> --&GT <CODE>STARTED</CODE></LI>
    * <LI><CODE>STARTED</CODE> --&GT <CODE>STOPPED</CODE></LI>
    * <LI><CODE>STOPPED</CODE> --&GT <CODE>SHUTDOWN</CODE></LI>
    * </UL>
    * <BR>
    * Note that the <CODE>LOADED</CODE> state is a transient state during the
    * installation of a component, and it is never persisted. When a component
    * is in this state, it has not yet been stored in the registry. Once it is
    * stored in the registry, it is in <CODE>SHUTDOWN</CODE> state until it is
    * initialized.
    */
    private ComponentState mComponentState;
 
   /**
    * The type of this component (BC or SE). This is persisted across restarts
    * of the JBI framework.
    */
    private ComponentType mComponentType;

   /**
    * The description of this BC or SE. This is persisted across restarts of the
    * JBI framework.
    */
    private String mDescription;
 
   /**
    * The desired state of this BC or SE. This is persisted across restarts of
    * the JBI framework. This state is set when a request to change the state
    * is received from an administrator in the form of a start, stop, or shut
    * down command.
    */
    private ComponentState mDesiredState;
 
   /**
    * The root installation directory path for this BC or SE. This is
    * persisted across restarts of the JBI framework.
    */
    private String mInstallRoot;
 
   /**
    * The name for this BC or SE. This is persisted across restarts of the
    * JBI framework.
    */
    private String mName;
 
   /**
    * The list of ServiceUnit objects representing the Service Units that are
    * deployed to this BC or SE. This is persisted across restarts of the JBI
    * framework.
    */
    private Hashtable mServiceUnitList;

   /**
    * The list of names of Shared Libraries that are required by this
    * BC or SE. This is persisted across restarts of the JBI framework.
    */
    private ArrayList mSharedLibraryNames;

   /**
    * The workspace root directory path for this BC or SE. This is persisted
    * across restarts of the JBI framework.
    */
    private String mWorkspaceRoot;
 
    //
    // Data items that are NOT persisted across JBI restarts.
    //

   /**
    * The bootstrap class loader for this BC or SE. This is NOT persisted.
    */
    private transient ClassLoader mBootstrapClassLoader;
 
   /**
    * When true, indicates that the bootstrap cleanUp() method needs to be
    * called by unloadBootstrap(). This is NOT persisted.
    */
    private transient boolean mBootstrapCleanUpNeeded;
 
   /**
    * The instance of the class that implements the
    * <CODE>javax.jbi.component.Bootstrap</CODE>
    * interface for this BC or SE. This is NOT persisted.
    */
    private transient javax.jbi.component.Bootstrap mBootstrapInstance;
 
   /**
    * A flag that indicates that a life cycle request is currently active.
    * This is NOT persisted.
    */
    private transient boolean mBusy;
 
   /**
    * The instance of the class that implements the
    * <CODE>javax.jbi.component.Component</CODE>
    * interface for this BC or SE. This is NOT persisted.
    */
    private transient javax.jbi.component.Component mComponentInstance;

   /**
    * The instance of the ComponentContext for this BC or SE. This is NOT
    * persisted.
    */
    private transient ComponentContext mContext;

   /**
    * The instance of the Deployer for this BC or SE. This is NOT persisted.
    */
    private transient Deployer mDeployerInstance;

   /**
    * The JMX ObjectName of the DeployerMBean for this BC or SE. This is NOT
    * persisted.
    */
    private transient javax.management.ObjectName mDeployerMBeanName;

   /**
    * The JMX ObjectName of the bootstrap ExtensionMBean for this BC or SE.
    * This is NOT persisted.
    */
    private transient javax.management.ObjectName mExtensionMBeanName;
 
   /**
    * The JMX ObjectName of the InstallerMBean for this BC or SE. This is NOT
    * persisted.
    */
    private transient javax.management.ObjectName mInstallerMBeanName;
    
    /**
    * The JMX ObjectName of the Configuration for this BC or SE. This is NOT
    * persisted.
    */
    private transient javax.management.ObjectName mConfigMBeanName;
 
   /**
    * The jbi.xml descriptor string. This is NOT persisted.
    */
    private transient String mJbiXmlString;

   /**
    * The jbi.xml JAXB model wrapper. This is NOT persisted.
    */
    private transient com.sun.jbi.management.descriptor.ComponentDescriptor mJbiXmlModel;

   /**
    * The instance of the class that implements the
    * <CODE>javax.jbi.component.ComponentLifeCycle</CODE>
    * interface for this BC or SE. This is NOT persisted.
    */
    private transient javax.jbi.component.ComponentLifeCycle mLifeCycleInstance;

   /**
    * The JMX ObjectName of the LifeCycleMBean for this BC or SE. This is NOT
    * persisted.
    */
    private transient javax.management.ObjectName mLifeCycleMBeanName;

   /**
    * The logger for logging errors.
    */
    private transient Logger mLog;

   /**
    * The instance of the logger MBean class for this BC or SE. This is NOT
    * persisted.
    */
    private transient ComponentLogger mLoggerInstance;

   /**
    * The JMX ObjectName of the LoggerMBean for this BC or SE. This is NOT
    * persisted.
    */
    private transient javax.management.ObjectName mLoggerMBeanName;

   /**
    * Flag to indicate that this component is an NMR observer. This is NOT
    * persisted.
    */
    private transient Boolean mObserver;

   /**
    * The instance of the class that implements the
    * <CODE>javax.jbi.component.ServiceUnitManager</CODE>
    * interface for this BC or SE. This is NOT persisted.
    */
    private transient javax.jbi.component.ServiceUnitManager mSUManagerInstance;

   /**
    * The instance of the statistics class for this BC or SE. This is NOT
    * persisted.
    */
    private transient ComponentStatistics mStatisticsInstance;

   /**
    * The JMX ObjectName of the ComponentStatisticsMBean for this BC or SE.
    * This is NOT persisted.
    */
    private transient javax.management.ObjectName mStatisticsMBeanName;

   /**
    * The StringTranslator to be used for constructing messages. This is NOT
    * persisted.
    */
    private transient StringTranslator mTranslator;

   /**
    * A flag that indicates that the component is being updated.
    * This is NOT persisted.
    */
    private transient boolean mUpdating;
 
   /**
    * XML namespace URI for the NMR observer element in the installation
    * descriptor extension in jbi.xml for this component.
    */
    private static String OBSERVER_NS = "http://www.sun.com/jbi/observer";
 
   /**
    * XML tag for the NMR observer element in the installation descriptor
    * extension in jbi.xml for this component.
    */
    private static String OBSERVER_TAG = "Observer";
 
   /**
    * Create a new Component instance.
    */
    public Component()
    {
        mServiceUnitList = new Hashtable();
        mTranslator = (StringTranslator)
            EnvironmentContext.getInstance().getStringTranslatorFor(this);
        mLog = EnvironmentContext.getInstance().getLogger();
        mComponentState = ComponentState.UNKNOWN;
        mDesiredState = ComponentState.UNKNOWN;
        mObserver = null;
    }

   /**
    * Constructor for building an instance from a ComponentInfo instance
    * @param compInfo - the ComponentInfo instance
    */
    public Component(com.sun.jbi.ComponentInfo compInfo)
    {
        mServiceUnitList = new Hashtable();
        mTranslator = (StringTranslator)
            EnvironmentContext.getInstance().getStringTranslatorFor(this);
        mLog = EnvironmentContext.getInstance().getLogger();

        mComponentType = compInfo.getComponentType();
        mName          = compInfo.getName();
        mDescription   = compInfo.getDescription();
        mInstallRoot   = compInfo.getInstallRoot();
        mWorkspaceRoot = compInfo.getWorkspaceRoot();
        mComponentState= ComponentState.SHUTDOWN;
        mDesiredState  = compInfo.getStatus();       
        mObserver = null;

        mBootstrapClassLoaderSelfFirst = compInfo.isBootstrapClassLoaderSelfFirst();
        mBootstrapClassName            = compInfo.getBootstrapClassName();
        mBootstrapClassPathElements    = new ArrayList(compInfo.getBootstrapClassPathElements());
        
        mComponentClassName            = compInfo.getComponentClassName();
        mComponentClassPathElements    = new ArrayList(compInfo.getClassPathElements());
        mComponentClassLoaderSelfFirst = compInfo.isClassLoaderSelfFirst();
        List<com.sun.jbi.ServiceUnitInfo> suList   = compInfo.getServiceUnitList();
        for(com.sun.jbi.ServiceUnitInfo suInfo: suList)
        {
            ServiceUnit su = new ServiceUnit(suInfo);
            mServiceUnitList.put(suInfo.getName(), su);
        }
        mSharedLibraryNames            = new ArrayList(compInfo.getSharedLibraryNames());
    }

   /**
    * Add a ServiceUnit object to the list of Service Units deployed to this
    * BC or SE.
    * @param serviceUnit The ServiceUnit object representing the Service Unit.
    * @throws javax.jbi.JBIException if the Service Unit is already in the list.
    */
    public void addServiceUnit(ServiceUnit serviceUnit)
        throws javax.jbi.JBIException
    {
        if ( null == serviceUnit )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "serviceUnit"));
        }
        if ( !mServiceUnitList.containsKey(serviceUnit.getName()) )
        {
            mServiceUnitList.put(serviceUnit.getName(), serviceUnit);
        }
        else
        {
            String msg = mTranslator.getString(
                LocalStringKeys.SU_ALREADY_EXISTS,
                serviceUnit.getName(),
                getComponentTypeAsString(),
                getName());
            throw new javax.jbi.JBIException(msg);
        }
    }

   /**
    * Clear the instance reference for the <CODE>Bootstrap</CODE> implementation
    * for this BC or SE.
    */
    public void clearBootstrapInstance()
    {
        mBootstrapInstance = null;

        // Destroy the class loader if it exists.
        if ( null != mBootstrapClassLoader )
        {
            ((CustomClassLoader) mBootstrapClassLoader).releaseResources();
            mBootstrapClassLoader = null;
        }
    }

   /**
    * Clear the "busy" flag.
    */
    public void clearBusy()
    {
        mBusy = false;
    }

   /**
    * Clear the instance reference for the <CODE>Component</CODE> implementation
    * for this BC or SE. Also remove the class loader if it exists.
    */
    public void clearLifeCycleInstance()
    {
        // Clear the class instance references.
        mComponentInstance = null;
        mLifeCycleInstance = null;
        mSUManagerInstance = null;

        // Destroy the class loader. The only reason for an exception here
        // is that the class loader did not exist, and that is not treated
        // as an error.
        try
        {
            ClassLoaderFactory.getInstance().removeComponentClassLoader(mName);
        }
        catch ( javax.jbi.JBIException ex )
        {
            ;
        }
    }

   /**
    * Clear the reference to the <CODE>ComponentStatistics</CODE> instance
    * for this BC or SE.
    */
    public void clearStatisticsInstance()
    {
        mStatisticsInstance = null;
    }

   /**
    * Clear the <code>Updating</code> flag. This also clears the <code>Busy
    * </code> flag.
    */
    public void clearUpdating()
    {
        mUpdating = false;
        clearBusy();
    }

   /**
    * Compare another Object with this one for equality.
    * @param object The object to be compared with this one.
    * @return True if the object is equal to this Component, false
    * if they are not equal.
    */
    public boolean equals(Object object)
    {
        if ( null == object || !(object instanceof Component) )
        {
            return false;
        }
        Component component = (Component) object;
        if ( !mName.equals(component.getName()) )
        {
            return false;
        }
        if ( mComponentType != component.getComponentType() )
        {
            return false;
        }
        if ( null == mDescription )
        {
            if ( null != component.getDescription() )
            {
                return false;
            }
        }
        else
        {
            if ( !mDescription.equals(component.getDescription()) )
            {
                return false;
            }
        }
        if ( null == mBootstrapClassName )
        {
            if ( null != component.getBootstrapClassName() )
            {
                return false;
            }
        }
        else
        {
            if ( !mBootstrapClassName.equals(component.getBootstrapClassName()) )
            {
                return false;
            }
        }
        if ( null == mBootstrapClassPathElements )
        {
            if ( null != component.getBootstrapClassPathElements() )
            {
                return false;
            }
        }
        else
        {
            if ( !mBootstrapClassPathElements.equals(
                component.getBootstrapClassPathElements()) )
            {
                return false;
            }
        }
        if ( null == mComponentClassName )
        {
            if ( null != component.getComponentClassName() )
            {
                return false;
            }
        }
        else
        {
            if ( !mComponentClassName.equals(component.getComponentClassName()) )
            {
                return false;
            }
        }
        if ( null == mComponentClassPathElements )
        {
            if ( null != component.getComponentClassPathElements() )
            {
                return false;
            }
        }
        else
        {
            if ( !mComponentClassPathElements.equals(
                component.getComponentClassPathElements()) )
            {
                return false;
            }
        }
        if ( null == mInstallRoot )
        {
            if ( null != component.getInstallRoot() )
            {
                return false;
            }
        }
        else
        {
            if ( !mInstallRoot.equals(
                component.getInstallRoot()) )
            {
                return false;
            }
        }
        if ( null == mWorkspaceRoot )
        {
            if ( null != component.getWorkspaceRoot() )
            {
                return false;
            }
        }
        else
        {
            if ( !mWorkspaceRoot.equals(
                component.getWorkspaceRoot()) )
            {
                return false;
            }
        }
        if ( null == mSharedLibraryNames )
        {
            if ( null != component.getSharedLibraryNames() &&
                !component.getSharedLibraryNames().isEmpty() )
            {
                return false;
            }
        }
        else
        {
            if ( !mSharedLibraryNames.equals(component.getSharedLibraryNames()) )
            {
                return false;
            }
        }
        if ( null == mServiceUnitList )
        {
            if ( null != component.getServiceUnitList() &&
                !component.getServiceUnitList().isEmpty() )
            {
                return false;
            }
        }
        else
        {
            if ( !getServiceUnitList().equals(component.getServiceUnitList()) )
            {
                return false;
            }
        }
        return true;
    }

   /**
    * Get the class name of the bootstrap implementation for this BC or SE.
    * @return The bootstrap class name.
    */
    public String getBootstrapClassName()
    {
        return mBootstrapClassName;
    }

   /**
    * Get the class path elements that this BC or SE needs in its bootstrap
    * runtime environment.
    * @return A list of the elements of the bootstrap class path as strings.
    */
    public java.util.List<String> getBootstrapClassPathElements()
    {
        return mBootstrapClassPathElements;
    }

   /**
    * Get an instance of the bootstrap class for this BC or SE. This method
    * can create a new instance if necessary. This is controlled by the
    * <CODE>canCreate</CODE> parameter. If the value of <CODE>canCreate</CODE>
    * is <code>true</CODE>, then if there is no instance of the bootstrap
    * class a new one is created. In this case, the return value is guaranteed
    * to be non-null. If the value of <CODE>canCreate</CODE> is <CODE>false</CODE>,
    * then a new instance of the bootstrap class is never created. In this
    * case, the return value can be null if there is no bootstrap instance.
    * @param canCreate Is true if a new instance of the bootstrap class can
    * be created if one does not already exist, and false if no new instance
    * can be created.
    * @return The bootstrap class instance.
    * @throws javax.jbi.JBIException if the class cannot be loaded.
    */
    public javax.jbi.component.Bootstrap getBootstrapInstance(boolean canCreate)
        throws javax.jbi.JBIException
    {
        if ( canCreate )
        {
            // Create a new instance if one is not already present.
            if ( null == mBootstrapInstance )
            {
                ClassLoader cl;
                Object o;
                try
                {
                    cl = ClassLoaderFactory.getInstance().
                        createBootstrapClassLoader(this);
                    mBootstrapClassLoader = cl;
                    o = cl.loadClass(mBootstrapClassName).newInstance();
                    if ( o instanceof javax.jbi.component.Bootstrap )
                    {
                        mBootstrapInstance = (javax.jbi.component.Bootstrap) o;
                    }
                    else
                    {
                        String msg = mTranslator.getString(
                            LocalStringKeys.COMP_CLASS_NOT_VALID,
                            mBootstrapClassName,
                            "javax.jbi.component.Bootstrap");
                        throw new javax.jbi.JBIException(msg);
                    }
                }
                catch ( javax.jbi.JBIException jbiEx )
                {
                    throw new javax.jbi.JBIException(mTranslator.getString(
                        LocalStringKeys.COMP_BOOTSTRAP_LOAD_FAILED,
                        mName, jbiEx.getMessage()));
                }
                catch ( ClassNotFoundException cnfEx )
                {
                    String msg = mTranslator.getString(
                        LocalStringKeys.COMP_CLASS_NOT_FOUND,
                        cnfEx.getMessage());
                    if ( null != cnfEx.getCause() )
                    {
                        mLog.log(Level.WARNING, msg, cnfEx.getCause());
                        throw new javax.jbi.JBIException(mTranslator.getString(
                            LocalStringKeys.COMP_BOOTSTRAP_LOAD_FAILED,
                            mName, msg), cnfEx.getCause());
                    }
                    else
                    {
                        throw new javax.jbi.JBIException(mTranslator.getString(
                            LocalStringKeys.COMP_BOOTSTRAP_LOAD_FAILED,
                            mName, msg));
                    }
                }
                catch ( Throwable ex )
                {
                    String msg = mTranslator.getString(
                        LocalStringKeys.COMP_EXCEPTION,
                        ex.getClass().getName());
                    mLog.log(Level.WARNING, msg, ex);
                    throw new javax.jbi.JBIException(mTranslator.getString(
                        LocalStringKeys.COMP_BOOTSTRAP_LOAD_FAILED,
                        mName, msg), ex);
                }
            }
        }
        return mBootstrapInstance;
    }

   /**
    * Get the class path elements that this BC or SE needs in its runtime
    * environment.
    * @return A list of the elements of the component's class path as strings.
    */
    public List<String> getClassPathElements()
    {
        return mComponentClassPathElements;
    }

   /**
    * Get the class name of the component implementation for this
    * BC or SE.
    * @return The component class name.
    */
    public String getComponentClassName()
    {
        return mComponentClassName;
    }

   /**
    * Get the complete classpath for this component as a String.
    * @return The full classpath value.
    */
    public String getComponentClassPathAsString()
    {
        String sep = System.getProperty("path.separator");
        Iterator i = mComponentClassPathElements.iterator();
        StringBuffer sb = new StringBuffer("");
        boolean first = true;
        while ( i.hasNext() )
        {
            if ( !first )
            {
                sb.append(sep);
            }
            else
            {
                first = false;
            }
            sb.append((String) i.next());
        }
        return new String(sb);
    }

   /**
    * Get the class path elements that this BC or SE needs in its runtime
    * environment.
    * @return A list of the elements of the component's class path as strings.
    */
    public List<String> getComponentClassPathElements()
    {
        return mComponentClassPathElements;
    }

   /**
    * Get the javax.jbi.component.Component instance for the BC or SE. If none
    * is available, returns null.
    * @return The instance of javax.jbi.component.Component or null if no
    * instance is currently available.
    */
    public javax.jbi.component.Component getComponentInstance()
    {
        return mComponentInstance;
    }

   /**
    * Get the component type for this BC or SE.
    * @return The component type (BINDING or ENGINE).
    */
    public ComponentType getComponentType()
    {
        return mComponentType;
    }

   /**
    * Get the component type for this BC or SE as a string.
    * @return The component type as a string ("binding" or "engine").
    */
    public String getComponentTypeAsString()
    {
        if ( isBinding() )
        {
            return mTranslator.getString(LocalStringKeys.BINDING);
        }
        if ( isEngine() )
        {
            return mTranslator.getString(LocalStringKeys.ENGINE);
        }
        return mTranslator.getString(LocalStringKeys.UNKNOWN);
    }

   /**
    * Get the ComponentContext for this BC or SE.
    * @return The context for the component.
    */
    public ComponentContext getContext()
    {
        return mContext;
    }

   /**
    * Get the JMX ObjectName for the component configuration Dynamic MBean.
    * @return the objectName The JMX ObjectName of the component configuration
    * Dynamic MBean.
    */
    public javax.management.ObjectName getConfigurationMBeanName()
    {
        return mConfigMBeanName;
    }
    
   /**
    * Get the Deployer instance for the BC or SE.
    * If none is available, returns null.
    * @return The instance of Deployer or null if no instance is currently
    * available.
    */
    public Deployer getDeployerInstance()
    {
        return mDeployerInstance;
    }

   /**
    * Get the JMX ObjectName for the Deployer MBean.
    * @return The ObjectName if the MBean is registered, null if not.
    */
    public javax.management.ObjectName getDeployerMBeanName()
    {
        return mDeployerMBeanName;
    }

   /**
    * Get the description for this BC or SE.
    * @return The component description.
    */
    public String getDescription()
    {
        return mDescription;
    }

   /**
    * Get the desired state of this component.
    * @return The state, either, <CODE>SHUTDOWN</CODE>, <CODE>STARTED</CODE>,
    * or <CODE>STOPPED</CODE>.
    */
    public ComponentState getDesiredState()
    {
        return mDesiredState;
    }

   /**
    * Get the JMX ObjectName for the bootstrap extension MBean.
    * @return The ObjectName if the MBean is registered, null if not.
    */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return mExtensionMBeanName;
    }

   /**
    * Get the JMX ObjectName for the Installer MBean.
    * @return The ObjectName if the MBean is registered, null if not.
    */
    public javax.management.ObjectName getInstallerMBeanName()
    {
        return mInstallerMBeanName;
    }

   /**
    * Get the root installation directory for this BC or SE.
    * @return The component installation root directory.
    */
    public String getInstallRoot()
    {
        return mInstallRoot;
    }

   /**
    * Get an instance of the life cycle class for this BC or SE. This method
    * can create a new instance if necessary. This is controlled by the
    * <CODE>canCreate</CODE> parameter. If the value of <CODE>canCreate</CODE>
    * is <code>true</CODE>, then if there is no instance of the life cycle
    * class a new one is created. In this case, the return value is guaranteed
    * to be non-null. If the value of <CODE>canCreate</CODE> is <CODE>false</CODE>,
    * then a new instance of the life cycle class is never created. In this
    * case, the return value can be null if there is no life cycle instance.
    * @param canCreate is true if a new instance of the life cycle class can
    * be created if one does not already exist, and false if no new instance
    * can be created.
    * @return The life cycle class instance.
    * @throws javax.jbi.JBIException if the class cannot be loaded.
    */
    public javax.jbi.component.ComponentLifeCycle getLifeCycleInstance(
        boolean canCreate)
        throws javax.jbi.JBIException
    {
        if ( canCreate )
        {
            // Create a new instance if one is not already present.
            if ( null == mComponentInstance )
            {
                ClassLoader cl;
                Object o;
                try
                {
                    cl = ClassLoaderFactory.getInstance().
                        createComponentClassLoader(this);
                    o = cl.loadClass(mComponentClassName).newInstance();
                    if ( o instanceof javax.jbi.component.Component )
                    {
                        mComponentInstance = (javax.jbi.component.Component) o;
                    }
                    else
                    {
                        String msg = mTranslator.getString(
                            LocalStringKeys.COMP_CLASS_NOT_VALID,
                            mComponentClassName,
                            "javax.jbi.component.Component");
                        throw new javax.jbi.JBIException(msg);
                    }
                    mLifeCycleInstance = mComponentInstance.getLifeCycle();
                    if ( null == mLifeCycleInstance )
                    {
                        String msg = mTranslator.getString(
                            LocalStringKeys.COMP_INSTANCE_NOT_PROVIDED,
                            "getLifeCycle()",
                            "javax.jbi.component.ComponentLifeCycle");
                        throw new javax.jbi.JBIException(msg);
                    }
                }
                catch ( javax.jbi.JBIException jbiEx )
                {
                    throw new javax.jbi.JBIException(mTranslator.getString(
                        LocalStringKeys.COMP_LIFECYCLE_LOAD_FAILED,
                        mName, jbiEx.getMessage()));
                }
                catch ( ClassNotFoundException cnfEx )
                {
                    String msg = mTranslator.getString(
                        LocalStringKeys.COMP_CLASS_NOT_FOUND,
                        cnfEx.getMessage());
                    if ( null != cnfEx.getCause() )
                    {
                        mLog.log(Level.WARNING, msg, cnfEx.getCause());
                        throw new javax.jbi.JBIException(mTranslator.getString(
                            LocalStringKeys.COMP_LIFECYCLE_LOAD_FAILED,
                            mName, msg), cnfEx.getCause());
                           
                    }
                    else
                    {
                        throw new javax.jbi.JBIException(mTranslator.getString(
                            LocalStringKeys.COMP_LIFECYCLE_LOAD_FAILED,
                           mName, msg));
                    }
                }
                catch ( NoClassDefFoundError ncdfEx )
                {
                    String msg = mTranslator.getString(
                        LocalStringKeys.COMP_CLASS_NOT_FOUND,
                        ncdfEx.getMessage());
                    if ( null != ncdfEx.getCause() )
                    {
                        mLog.log(Level.WARNING, msg, ncdfEx.getCause());
                        throw new javax.jbi.JBIException(mTranslator.getString(
                            LocalStringKeys.COMP_LIFECYCLE_LOAD_FAILED,
                            mName, msg), ncdfEx.getCause());
                    }
                    else
                    {
                        throw new javax.jbi.JBIException(mTranslator.getString(
                            LocalStringKeys.COMP_LIFECYCLE_LOAD_FAILED,
                            mName, msg));
                    }
                }
                catch ( Throwable ex )
                {
                    String msg = mTranslator.getString(
                        LocalStringKeys.COMP_EXCEPTION,
                        ex.getClass().getName());
                    mLog.log(Level.WARNING, msg, ex);
                    throw new javax.jbi.JBIException(mTranslator.getString(
                        LocalStringKeys.COMP_LIFECYCLE_LOAD_FAILED,
                        mName, msg), ex);
                }
            }
        }
        if ( null != mComponentInstance )
        {
            return mLifeCycleInstance;
        }
        else
        {
            return null;
        }
    }

   /**
    * Get the JMX ObjectName for the LifeCycle MBean.
    * @return The ObjectName if the MBean is registered, null if not.
    */
    public javax.management.ObjectName getLifeCycleMBeanName()
    {
        return mLifeCycleMBeanName;
    }

   /**
    * Get the logger MBean instance.
    * @return the ComponentLogger object.
    */
    public ComponentLogger getLoggerInstance()
    {
        return mLoggerInstance;
    }

   /**
    * Get the JMX ObjectNames for the logger MBean.
    * @return the LoggerMBean object name.
    */
    public javax.management.ObjectName getLoggerMBeanName()
    {
        return mLoggerMBeanName;
    }

   /**
    * Get the unique name for this BC or SE.
    * @return The name.
    */
    public String getName()
    {
        return mName;
    }

   /**
    * Get the ServiceUnit object for a specified Service Unit name. If none
    * is found, return null.
    * @param serviceUnitName The unique name of the Service Unit.
    * @return The Service Unit or null if not found.
    */
    public ServiceUnit getServiceUnit(String serviceUnitName)
    {
        return (ServiceUnit) mServiceUnitList.get(serviceUnitName);
    }

   /**
    * Get the list of ServiceUnit objects representing all Service Units
    * currently deployed to this BC or SE.
    * @return A list of the ServiceUnit objects.
    */
    public List<com.sun.jbi.ServiceUnitInfo> getServiceUnitList()
    {
        return new ArrayList(mServiceUnitList.values());
    }

   /**
    * Get the ServiceUnitManager instance for this component, if one is
    * available.
    * @return the Service Unit Manager or null if there is none.
    */
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager()
    {
        if ( null == mSUManagerInstance )
        {
            if ( null != mComponentInstance )
            {
                mSUManagerInstance = mComponentInstance.getServiceUnitManager();
            }
        }
        return mSUManagerInstance;
    }

   /**
    * Get the list of Shared Library names required by this BC or SE.
    * @return A list of the Shared Library names as strings.
    */
    public List<String> getSharedLibraryNames()
    {
        return mSharedLibraryNames;
    }

   /**
    * Get the instance of the statistics MBean. If there is none, return null.
    * @return Instance of com.sun.jbi.monitoring.ComponentStatisticsBase
    * or null if no instance is currently available.
    */
    public ComponentStatistics getStatisticsInstance()
    {
        return mStatisticsInstance;
    }

   /**
    * Get the JMX ObjectName for the statistics MBean.
    * @return The ObjectName if the MBean is registered, null if not.
    */
    public javax.management.ObjectName getStatisticsMBeanName()
    {
        return mStatisticsMBeanName;
    }

   /**
    * Get the status of this component.
    * @return The status, either <CODE>LOADED</CODE>, <CODE>SHUTDOWN</CODE>,
    * <CODE>STARTED</CODE>, or <CODE>STOPPED</CODE>.
    */
    public ComponentState getStatus()
    {
        return mComponentState;
    }

   /**
    * Get the status of this component as a string value.
    * @return The status as a string, either "loaded", "installed", "started",
    * or "stopped".
    */
    public String getStatusAsString()
    {
        return mComponentState.toString();
    }

   /**
    * Get a component state as a string value.
    * @param status A state, either <CODE>LOADED</CODE>, <CODE>SHUTDOWN</CODE>,
    * <CODE>STARTED</CODE>, or <CODE>STOPPED</CODE>.
    * @return The status as a string, either "loaded", "installed", "started",
    * or "stopped".
    */
    public String getStatusAsString(ComponentState status)
    {
        if ( status == ComponentState.SHUTDOWN )
        {
            return mTranslator.getString(LocalStringKeys.COMP_STATE_SHUTDOWN);
        }
        if ( status == ComponentState.LOADED )
        {
            return mTranslator.getString(LocalStringKeys.COMP_STATE_LOADED);
        }
        if ( status == ComponentState.STARTED )
        {
            return mTranslator.getString(LocalStringKeys.COMP_STATE_STARTED);
        }
        if ( status == ComponentState.STOPPED )
        {
            return mTranslator.getString(LocalStringKeys.COMP_STATE_STOPPED);
        }
        return mTranslator.getString(LocalStringKeys.UNKNOWN);
    }

   /**
    * Get the workspace root directory for this BC or SE.
    * @return The workspace root directory.
    */
    public String getWorkspaceRoot()
    {
        return mWorkspaceRoot;
    }

    /**
     * Get the installation descriptor (jbi.xml) for the component as a String.
     * @return the Installation Descriptor for the component.
     */
    public String getInstallationDescriptor()
    {
        if ( null == mJbiXmlString )
        {
            com.sun.jbi.ComponentInfo ci = EnvironmentContext.getInstance().
                getComponentQuery().getComponentInfo(mName);
            if ( null != ci )
            {
                mJbiXmlString = ci.getInstallationDescriptor();
            }
        }
        return mJbiXmlString;
    }

    /**
     * Get the updated wrapped JAXB model for the installation descriptor (jbi.xml) for
     * the component.  This method will only be called during the component upgrade operation.
     * @return the updated wrapped JAXB model for the installation descriptor.
     */
    public com.sun.jbi.management.descriptor.ComponentDescriptor getInstallationDescriptorModel()
    {
        return getInstallationDescriptorModel(false);
    }

    /**
     * Get the wrapped JAXB model for the installation descriptor (jbi.xml) for
     * the component.
     * @param refreshFlag indicating whether cached descriptor needs to be refreshed or not.
     *                    this will be used in component upgrade case.
     * @return the wrapped JAXB model for the installation descriptor.
     */
    public com.sun.jbi.management.descriptor.ComponentDescriptor getInstallationDescriptorModel(boolean refreshFlag)
    {
        if (refreshFlag == true)
        {
             mJbiXmlModel = null;
        }

        if ( null == mJbiXmlModel )
        {
            com.sun.jbi.management.registry.Registry reg =
                (com.sun.jbi.management.registry.Registry) 
                EnvironmentContext.getInstance().getRegistry();
            if ( null != reg )
            {
                try
                {
                    mJbiXmlModel = reg.getGenericQuery().getComponentDescriptor(mName);
                }
                catch ( Throwable ex )
                {
                    mLog.log(Level.FINE,
                        "Unable to get installation descriptor for " + mName, ex);
                }
            }
        }
        return mJbiXmlModel;
    }
    
   /**
    * Get the hash code for this Component instance.
    * @return The hash code.
    */
    public int hashCode()
    {
        int hashCode = 0;

        hashCode += mName.hashCode();
        hashCode += mComponentType.hashCode();
        hashCode += mBootstrapClassName.hashCode();
        hashCode += mBootstrapClassPathElements.hashCode();
        hashCode += mComponentClassName.hashCode();
        hashCode += mComponentClassPathElements.hashCode();
        if ( null != mSharedLibraryNames )
        {
            hashCode += mSharedLibraryNames.hashCode();
        }

        return hashCode;
    }

   /**
    * Check to see if this component is a BC.
    * @return True if the component is a BC, false if not.
    */
    public boolean isBinding()
    {
        return (mComponentType == ComponentType.BINDING);
    }

   /**
    * Check to see if this component is processing a life cycle request.
    * @return True if the component busy, false if not.
    */
    public boolean isBusy()
    {
        return mBusy;
    }

   /**
    * Check to see if the bootstrap cleanUp() method needs to be called.
    * @return true if the bootstrap cleanUp() method should be called, or
    * false if it has already been called.
    */
    public boolean isBootstrapCleanUpNeeded()
    {
        return mBootstrapCleanUpNeeded;
    }

   /**
    * Check to see if the bootstrap class loader should use a self-first
    * search hierarchy.
    * @return True if the bootstrap class loader should use a self-first
    * search hierarchy, false if it should use parent-first.
    */
    public boolean isBootstrapClassLoaderSelfFirst()
    {
        return mBootstrapClassLoaderSelfFirst;
    }

   /**
    * Check to see if the component class loader should use a self-first
    * search hierarchy.
    * @return True if the component class loader should use a self-first
    * search hierarchy, false if it should use parent-first.
    */
    public boolean isComponentClassLoaderSelfFirst()
    {
        return mComponentClassLoaderSelfFirst;
    }

   /**
    * Check to see if this component is a SE.
    * @return True if the component is a SE, false if not.
    */
    public boolean isEngine()
    {
        return (ComponentType.ENGINE == mComponentType);
    }

   /**
    * Check to see if this component is initialized.
    * @return True if the component is initialized, false if not.
    */
    public boolean isInitialized()
    {
        return (ComponentState.STOPPED == mComponentState);
    }

   /**
    * Check to see if this component is installed.
    * @return True if the component is installed, false if not.
    */
    public boolean isInstalled()
    {
        return (ComponentState.SHUTDOWN == mComponentState);
    }

   /**
    * Check to see if this component is loaded.
    * @return True if the component is loaded, false if not.
    */
    public boolean isLoaded()
    {
        return (ComponentState.LOADED == mComponentState);
    }

   /**
    * Check to see if this component is an NMR observer.
    * @return <code>true</code> if the component is an observer,
    * <code>false</code> if not.
    */
    public boolean isObserver()
    {
        if ( null == mObserver )
        {
            setObserver(false);
            com.sun.jbi.management.descriptor.ComponentDescriptor cd =
                getInstallationDescriptorModel();
            if ( null != cd )
            {
                org.w3c.dom.Element obs = cd.getComponentObserverXml();
                if ( null != obs )
                {
                    if ( null != obs.getElementsByTagNameNS(OBSERVER_NS,
                        OBSERVER_TAG) )
                    {
                        mLog.fine(mName + " is an NMR Observer");
                        setObserver(true);
                    }
                }
            }
        }
        return mObserver.booleanValue();
    }

   /**
    * Check to see if a Service Unit is registered to this component.
    * @param serviceUnitName the unique name of the Service Unit.
    * @return true if the Service Unit is registered, false if not.
    */
    public boolean isServiceUnitRegistered(String serviceUnitName)
    {
        return mServiceUnitList.containsKey(serviceUnitName);
    }

   /**
    * Check to see if this component is shut down.
    * @return True if the component is shut down, false if not.
    */
    public boolean isShutDown()
    {
        return (ComponentState.SHUTDOWN == mComponentState);
    }

   /**
    * Check to see if this component is started.
    * @return True if the component is started, false if not.
    */
    public boolean isStarted()
    {
        return (ComponentState.STARTED == mComponentState);
    }

   /**
    * Check to see if this component is in the process of starting.
    * @return True if the component is starting, false if not.
    public boolean isStarting()
    {
        return (ComponentState.STARTING == mComponentState);
    }
    */

   /**
    * Check to see if this component is stopped.
    * @return True if the component is stopped, false if not.
    */
    public boolean isStopped()
    {
        return (ComponentState.STOPPED == mComponentState);
    }

   /**
    * Check to see if this component is in the process of stopping.
    * @return True if the component is stopping, false if not.
    public boolean isStopping()
    {
        return (ComponentState.STOPPING == mComponentState);
    }
    */

   /**
    * Check to see if this component is being updated.
    * @return True if the component is being updated, false if not.
    */
    public boolean isUpdating()
    {
        return mUpdating;
    }

   /**
    * Remove a ServiceUnit object from the list of Service Units deployed to
    * this BC or SE.
    * @param serviceUnitName The name of the Service Unit.
    * @throws javax.jbi.JBIException if no service unit is found by the
    * specified name.
    */
    public void removeServiceUnit(String serviceUnitName)
        throws javax.jbi.JBIException
    {
        if ( mServiceUnitList.containsKey(serviceUnitName) )
        {
            mServiceUnitList.remove(serviceUnitName);
        }
        else
        {
            String msg = mTranslator.getString(
                LocalStringKeys.SU_NOT_FOUND,
                serviceUnitName,
                getComponentTypeAsString(),
                getName());
            throw new javax.jbi.JBIException(msg);
        }
    }

   /**
    * Set the flag that indicates whether the bootstrap cleanUp() method needs
    * to be called.
    * @param isCleanUpNeeded true if the bootstrap cleanUp() method should be
    * called, or false if it has already been called.
    */
    public void setBootstrapCleanUpNeeded(boolean isCleanUpNeeded)
    {
        mBootstrapCleanUpNeeded = isCleanUpNeeded;
    }

   /**
    * Set the flag that determines the hierarchy for the bootstrap class loader
    * which is true for self-first or false for parent-first.
    * @param isSelfFirst is true for a self-first class loading hierarchy, or
    * false for a parent-first hierarchy.
    */
    public void setBootstrapClassLoaderSelfFirst(boolean isSelfFirst)
    {
        mBootstrapClassLoaderSelfFirst = isSelfFirst;
    }

   /**
    * Set the class name of the bootstrap implementation for this
    * BC or SE.
    * @param className The bootstrap class name.
    */
    public void setBootstrapClassName(String className)
    {
        mBootstrapClassName = className;
    }

   /**
    * Set the class path elements for the bootstrap implementation for this
    * BC or SE.
    * @param classPathElements The list of class path elements as strings.
    */
    public void setBootstrapClassPathElements(List classPathElements)
    {
        mBootstrapClassPathElements = new ArrayList(classPathElements);
    }

   /**
    * Set the flag indicating the component is busy processing a life cycle
    * operation. This method is synchronized so that the flag can be set by
    * only one caller at any time. If the flag is already set, an exception
    * is thrown indicating that an operation is already in progress on the
    * component.
    *
    * @throws javax.jbi.JBIException when the component is already busy.
    */
    public synchronized void setBusy() throws javax.jbi.JBIException
    {
        if ( mBusy )
        {
            throw new javax.jbi.JBIException(mTranslator.getString(
                LocalStringKeys.COMP_BUSY, mName));
        }
        mBusy = true;
    }

   /**
    * Set the flag indicating the component is busy processing a life cycle
    * operation. This method is used only when processing a forced shutdown
    * of the component. No checking is done for the busy flag already being
    * set. Do NOT use this method for any purpose other than during a forced
    * shutdown of the component, or the results will be unpredictable.
    */
    public void setBusyForce()
    {
        mBusy = true;
    }

   /**
    * Set the flag that determines the hierarchy for the component class loader
    * which is true for self-first or false for parent-first.
    * @param isSelfFirst is true for a self-first class loading hierarchy, or
    * false for a parent-first hierarchy.
    */
    public void setComponentClassLoaderSelfFirst(boolean isSelfFirst)
    {
        mComponentClassLoaderSelfFirst = isSelfFirst;
    }

   /**
    * Set the root installation directory for this BC or SE.
    * @param installRoot The component's install root directory.
    */
    public void setInstallRoot(String installRoot)
    {
        if ( null != installRoot )
        {
            mInstallRoot = installRoot.replace('\\', '/');
        }
        else
        {
            mInstallRoot = installRoot;
        }
    }

   /**
    * Set the component type.
    * @param type The component type.
    */
    public void setComponentType(ComponentType type)
    {
        mComponentType = type;
    }

   /**
    * Set the component type for a BC.
    */
    public void setComponentTypeBinding()
    {
        mComponentType = ComponentType.BINDING;
    }

   /**
    * Set the component type for a SE.
    */
    public void setComponentTypeEngine()
    {
        mComponentType = ComponentType.ENGINE;
    }

   /**
    * Set the component's Deployer instance.
    * @param deployer the instance of Deployer for the component.
    */
    public void setDeployerInstance(Deployer deployer)
    {
        mDeployerInstance = deployer;
    }

   /**
    * Set the desired state of a component.
    * @param state The desired state to be set.
    * Valid states are <CODE>SHUTDOWN</CODE>, <CODE>STOPPED</CODE>, and
    * <CODE>STARTED</CODE>.
    */
    public void setDesiredState(ComponentState state)
    {
        if ( ComponentState.SHUTDOWN != state && 
             ComponentState.STOPPED != state && 
             ComponentState.STARTED != state )
        {
            throw new java.lang.IllegalArgumentException(
                mTranslator.getString(LocalStringKeys.INVALID_ARGUMENT,
                "state", state.toString()));
        }
        mDesiredState = state;
    }
    
   /**
    * Set the component state. This method is used ONLY by startup and unit
    * test code. It does no validation of the state.
    * @param status The ComponentState to be set.
    * Valid states are <CODE>LOADED</CODE>, <CODE>SHUTDOWN</CODE>,
    * <CODE>STOPPED</CODE>, and <CODE>STARTED</CODE>.
    */
    public void setStatus(ComponentState status)
    {
        mComponentState = status;
    }
    
   /**
    * Set the component state to loaded. The only time a component can be in
    * this state is when its bootstrap class is loaded for an install or an
    * uninstall that has not yet completed.
    */
    public void setLoaded()
    {
        if ( isStopped() )
        {
            throw new java.lang.IllegalStateException(mTranslator.getString(
                LocalStringKeys.COMP_INVALID_STATE_CHANGE,
                mTranslator.getString(LocalStringKeys.COMP_STATE_STOPPED),
                mTranslator.getString(LocalStringKeys.COMP_STATE_LOADED)));
        }
        if ( isStarted() )
        {
            throw new java.lang.IllegalStateException(mTranslator.getString(
                LocalStringKeys.COMP_INVALID_STATE_CHANGE,
                mTranslator.getString(LocalStringKeys.COMP_STATE_STARTED),
                mTranslator.getString(LocalStringKeys.COMP_STATE_LOADED)));
        }

        mComponentState = ComponentState.LOADED;
    }

   /**
    * Set the class name of the Component implementation for this component.
    * @param className The life cycle class name.
    */
    public void setComponentClassName(String className)
    {
        if ( null != className )
        {
            mComponentClassName = className;
        }
    }

   /**
    * Set the class instance of the Component implementation for this BC or SE.
    * @param classInstance The Component class instance.
    */
    public void setComponentInstance(javax.jbi.component.Component classInstance)
    {
        if ( null != classInstance )
        {
            mComponentInstance = classInstance;
            mLifeCycleInstance = classInstance.getLifeCycle();
        }
    }

   /**
    * Set the class path elements that this BC or SE needs in its runtime
    * environment.
    * @param classPathElements A list containing the class path elements as
    * strings.
    */
    public void setComponentClassPathElements(List classPathElements)
    {
        if ( null != classPathElements )
        {
            mComponentClassPathElements = new ArrayList(classPathElements);
        }
    }

   /**
    * Set the JMX ObjectName for the component configuration Dynamic MBean.
    * @param objectName The JMX ObjectName of the component configuration
    * Dynamic MBean.
    */
    public void setConfigurationMBeanName(javax.management.ObjectName objectName)
    {
        mConfigMBeanName = objectName;
    }

   /**
    * Set the ComponentContext for this BC or SE.
    * @param context The componet's context.
    */
    public void setContext(ComponentContext context)
    {
        mContext = context;
    }

   /**
    * Set the description of this BC or SE.
    * @param description The description.
    */
    public void setDescription(String description)
    {
        mDescription = description;
    }

   /**
    * Set the JMX ObjectName for the DeployerMBean.
    * @param objectName The JMX ObjectName of the DeployerMBean.
    */
    public void setDeployerMBeanName(javax.management.ObjectName objectName)
    {
        mDeployerMBeanName = objectName;
    }

   /**
    * Set the JMX ObjectName for the bootstrap ExtensionMBean.
    * @param objectName The JMX ObjectName of the ExtensionMBean.
    */
    public void setExtensionMBeanName(javax.management.ObjectName objectName)
    {
        mExtensionMBeanName = objectName;
    }

   /**
    * Set the JMX ObjectName for the InstallerMBean.
    * @param objectName The JMX ObjectName of the InstallerMBean.
    */
    public void setInstallerMBeanName(javax.management.ObjectName objectName)
    {
        mInstallerMBeanName = objectName;
    }

   /**
    * Set the JMX ObjectName for the LifeCycleMBean.
    * @param objectName The JMX ObjectName of the LifeCycleMBean.
    */
    public void setLifeCycleMBeanName(javax.management.ObjectName objectName)
    {
        mLifeCycleMBeanName = objectName;
    }
    
   /**
    * Set the ComponentLoggerMBean instance for this component.
    * @param instance The ComponentLogger instance.
    */
    public void setLoggerInstance(ComponentLogger instance)
    {
        mLoggerInstance = instance;
    }

   /**
    * Set the LoggerMBean ObjectName for this component.
    * @param objectName The ObjectName for the logger MBean.
    */
    public void setLoggerMBeanName(javax.management.ObjectName objectName)
    {
        mLoggerMBeanName = objectName;
    }

   /**
    * Set the name for this BC or SE.
    * @param name The name.
    */
    public void setName(String name)
    {
        mName = name;
    }

   /**
    * Set the observer flag for this component. This flag indicates whether
    * the component is an NMR observer.
    * @param isObserver <code>true</code> indicates that the component is an
    * NMR observer, <code>false</code> indicates that it is not.
    */
    public void setObserver(boolean isObserver)
    {
        mObserver = Boolean.valueOf(isObserver);
    }

   /**
    * Set the list of Shared Library names required by this BC or SE.
    * The names must be in the list in the order in which the Shared
    * Libraries should appear in the class path.
    * @param nameList A list containing the names as Strings.
    */
    public void setSharedLibraryNames(List<String> nameList)
    {
        if ( null != nameList )
        {
            mSharedLibraryNames = new ArrayList(nameList);
        }
    }

   /**
    * Set the component state to shutdown (installed but not active).
    */
    public void setShutdown()
    {
        // If this is an install, clear the reference to the bootstrap
        // instance so that it can be garbage-collected.

        if ( isLoaded() )
        {
            mBootstrapInstance = null;
        }

        // If the component is running it cannot be set to shutdown, only
        // to stopped.

        else if ( isStarted() )
        {
            throw new java.lang.IllegalStateException(mTranslator.getString(
                LocalStringKeys.COMP_INVALID_STATE_CHANGE,
                mTranslator.getString(LocalStringKeys.COMP_STATE_STARTED),
                mTranslator.getString(LocalStringKeys.COMP_STATE_SHUTDOWN)));
        }

        // If this is a shutdown of a component, clear the reference to the
        // life cycle instance so that it can be garbage-collected.

        else if ( isStopped() )
        {
            mComponentInstance = null;
        }

        mComponentState = ComponentState.SHUTDOWN;
    }

   /**
    * Set the component state to shutting down.
    public void setShuttingDown()
    {
        mComponentState = ComponentState.SHUTTINGDOWN;
    }
    */

   /**
    * Set the component state to started.
    */
    public void setStarted()
    {
        if ( isLoaded() )
        {
            throw new java.lang.IllegalStateException(mTranslator.getString(
                LocalStringKeys.COMP_INVALID_STATE_CHANGE,
                mTranslator.getString(LocalStringKeys.COMP_STATE_LOADED),
                mTranslator.getString(LocalStringKeys.COMP_STATE_STARTED)));
        }
        if ( isShutDown() )
        {
            throw new java.lang.IllegalStateException(mTranslator.getString(
                LocalStringKeys.COMP_INVALID_STATE_CHANGE,
                mTranslator.getString(LocalStringKeys.COMP_STATE_SHUTDOWN),
                mTranslator.getString(LocalStringKeys.COMP_STATE_STARTED)));
        }

        mComponentState = ComponentState.STARTED;
    }

   /**
    * Set the component state to starting.
    public void setStarting()
    {
        mComponentState = ComponentState.STARTING;
    }
    */

   /**
    * Set the instance of ComponentStatistics for this BC or SE.
    * @param classInstance The ComponentStatistics instance.
    */
    public void setStatisticsInstance(ComponentStatistics classInstance)
    {
        if ( null != classInstance )
        {
            mStatisticsInstance = classInstance;
        }
    }

   /**
    * Set the JMX ObjectName for the StatisticsMBean.
    * @param objectName The JMX ObjectName of the StatisticsMBean.
    */
    public void setStatisticsMBeanName(javax.management.ObjectName objectName)
    {
        mStatisticsMBeanName = objectName;
    }

   /**
    * Set the component state to stopped.
    */
    public void setStopped()
    {
        if ( isLoaded() )
        {
            throw new java.lang.IllegalStateException(mTranslator.getString(
                LocalStringKeys.COMP_INVALID_STATE_CHANGE,
                mTranslator.getString(LocalStringKeys.COMP_STATE_LOADED),
                mTranslator.getString(LocalStringKeys.COMP_STATE_STOPPED)));
        }

        mComponentState = ComponentState.STOPPED;
    }

   /**
    * Set the component state to stopping.
    public void setStopping()
    {
        mComponentState = ComponentState.STOPPING;
    }
    */

   /**
    * Set the <code>Updating</code> flag indicating the component is being
    * updated. This requires first setting the <code>Busy</code> flag to prevent
    * any life cycle operations from being processed for the component while
    * it is being updated. If the <code>Busy</code> flag is already set, this
    * means that a life cycle operation is in progress and the update cannot
    * proceed. In this case an exception is thrown.
    */
    public void setUpdating() throws javax.jbi.JBIException
    {
        setBusy();  // throws exception if already busy
        mUpdating = true;
    }

   /**
    * Set the workspace root directory for this BC or SE.
    * @param workspaceRoot The component workspace root directory.
    */
    public void setWorkspaceRoot(String workspaceRoot)
    {
        if ( null != workspaceRoot )
        {
            mWorkspaceRoot = workspaceRoot.replace('\\', '/');
        }
        else
        {
            mWorkspaceRoot = workspaceRoot;
        }
    }
    
    /**
     * Set the installation descriptor for this component.
     * @param jbiXml - the string representation of the installation descriptor
     * for this Component
     */
    public void setInstallationDescriptor(String jbiXml)
    {
        mJbiXmlString = jbiXml;
    } 

   /**
    * Convert to a String.
    * @return The String representation of this Component.
    */
    public String toString()
    {
        String sep = ",\n";
        StringBuffer b = new StringBuffer();
        b.append("Component Name = " + mName);
        b.append(sep);
        b.append("ComponentType = " + mComponentType.toString());
        b.append(" (" + getComponentTypeAsString() + ")");
        b.append(sep);
        b.append("Description = " +
                 (null != mDescription ? mDescription : ""));
        b.append(sep);
        b.append("State = " + mComponentState);
        b.append(" (" + getStatusAsString(mComponentState) + ")");
        b.append(sep);
        b.append("Desired State = " + mDesiredState.toString());
        b.append(" (" + mDesiredState.toString() + ")");
        b.append(sep);
        b.append("InstallRoot = " +
                 (null != mInstallRoot ? mInstallRoot : ""));
        b.append(sep);
        b.append("WorkspaceRoot = " +
                 (null != mWorkspaceRoot ? mWorkspaceRoot : ""));
        b.append(sep);
        b.append("BootstrapClassName = " +
                 (null != mBootstrapClassName ? mBootstrapClassName : ""));
        b.append(sep);
        b.append("BootstrapClassPathElements = " +
                 (null != mBootstrapClassPathElements ?
                 mBootstrapClassPathElements : new ArrayList()));
        b.append(sep);
        b.append("BootstrapClassLoaderSelfFirst = " +
                 mBootstrapClassLoaderSelfFirst);
        b.append(sep);
        b.append("ComponentClassName = " +
                 (null != mComponentClassName ? mComponentClassName : ""));
        b.append(sep);
        b.append("ComponentClassPathElements = " +
                 (null != mComponentClassPathElements ?
                 mComponentClassPathElements : new ArrayList()));
        b.append(sep);
        b.append("ComponentClassLoaderSelfFirst = " +
                 mComponentClassLoaderSelfFirst);
        b.append(sep);
        b.append("ServiceUnitList = " +
                 (null != mServiceUnitList ?
                 mServiceUnitList : new Hashtable()));
        b.append(sep);
        b.append("SharedLibraryNames = " +
                 (null != mSharedLibraryNames ?
                 mSharedLibraryNames : new ArrayList()));
        return b.toString();
    }

    /**
     * Return an indication of whether the class loaders for this component
     * should use the self-first hierarchy.
     * @return true/false depending on the value of the ClassLoaderSelfFirst 
     * attribute.
     */
    public boolean isClassLoaderSelfFirst()
    {
        return mComponentClassLoaderSelfFirst;
    }

    /**
     * Get the component properties. TODO
     * @return the component properties
     */
    public Map getProperties()
    {
        return new HashMap();
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentState;
import com.sun.jbi.management.registry.Updater;
import javax.management.ObjectName;

/**
 * This class implements the LifeCycleMBean for a Component (BC or SE). This
 * MBean acts as an agent between the JMX management service and the Component 
 * Framework to allow the Component Framework to keep track of the state of the
 * component. Some methods in this MBean delegate to methods in the Component
 * Framework, which in turn interact directly with the component. Other methods
 * in this MBean interact directly with the component.
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentLifeCycle
    implements com.sun.jbi.framework.ComponentLifeCycleMBean
{
    /**
     * Component instance handle.
     */
    private Component mComponent;

    /**
     * Component Framework handle.
     */
    private ComponentFramework mComponentFramework;

    /**
     * Local handle to StringTranslator.
     */
    private StringTranslator mTranslator;

    /**
     * Local handle to registry updater.
     */
    private Updater mUpdater;

    /**
     * Constructor.
     * @param component is the Component instance for this component.
     * @param componentFramework is the ComponentFramework service handle.
     */
    ComponentLifeCycle(Component component,
                       ComponentFramework componentFramework)
    {
        mComponent = component;
        mComponentFramework = componentFramework;
        mTranslator = (StringTranslator)
            EnvironmentContext.getInstance().getStringTranslatorFor(this);
    }

    /**
     * Get the current state of this component.
     * @return The current state of the component as a string.
     */
    public String getCurrentState()
    {
        String state;
        if ( mComponent.isStarted() )
        {
            state = STARTED;
        }
        else if ( mComponent.isStopped() )
        {
            state = STOPPED;
        }
        else if ( mComponent.isShutDown() )
        {
            state = SHUTDOWN;
        }
        else
        {
            state = UNKNOWN;
        }
        return state;
    }

    /**
     * Get the JMX ObjectName for the DeployerMBean for this component. If
     * there is none, return null.
     * @throws javax.jbi.JBIException if there is a failure getting component
     * information from the Component Registry.
     * @return ObjectName the JMX object name of the DeployerMBean or null
     * if there is no DeployerMBean.
     */
    public ObjectName getDeploymentMBeanName()
        throws javax.jbi.JBIException
    {
        ObjectName name = null;
        name = mComponent.getDeployerMBeanName();
        return name;
    }

    /**
     * Get the JMX ObjectName for any additional MBean for this component. If
     * there is none, return null.
     * @throws javax.jbi.JBIException if there is a failure getting component
     * information from the Component Registry.
     * @return ObjectName the JMX object name of the additional MBean or null
     * if there is no additional MBean.
     */
    public ObjectName getExtensionMBeanName()
        throws javax.jbi.JBIException
    {
        ObjectName name = null;
        javax.jbi.component.ComponentLifeCycle instance =
            mComponent.getLifeCycleInstance(false);
        if ( instance != null )
        {
            name = instance.getExtensionMBeanName();
        }
        return name;
    }

    /**
     * Start command. This starts up the component, calling its init() method
     * if necessary.
     * @throws javax.jbi.JBIException if the component fails to start or if
     * another life cycle operation is already in progress.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mComponent.setBusy();
        mComponent.setDesiredState(ComponentState.STARTED);
        persistState();
        try
        {
            // The boolean argument indicates that this call originated from a
            // JMX client. This affects how exceptions from the component are
            // handled.
            mComponentFramework.startComponent(mComponent, true);
        }
        finally
        {
            mComponent.clearBusy();
        }
        return;
    }

    /**
     * Stop command. This stops the component but does not completely shut
     * it down. It can be restarted without going through init() again.
     * @throws javax.jbi.JBIException if the component fails to stop or if
     * another life cycle operation is already in progress.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mComponent.setBusy();
        mComponent.setDesiredState(ComponentState.STOPPED);
        persistState();
        try
        {
            mComponentFramework.stopComponent(mComponent);
        }
        finally
        {
            mComponent.clearBusy();
        }
        return;
    }

    /**
     * Shutdown command. This completely shuts down the component and unloads
     * its classes from the JBI Framework. The next start command will
     * require init() to be called again.
     * @throws javax.jbi.JBIException if the component fails to shut down or if
     * another life cycle operation is already in progress.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        shutDown(false);
    }

    /**
     * Shutdown command. This completely shuts down the component and unloads
     * its classes from the JBI Framework. The next start command will require
     * init() to be called again. If the force flag is set to true, the
     * shutdown succeeds regardless of the success or failure of the component's
     * shutDown() method. 
     * @throws javax.jbi.JBIException if any failure occurs or if another life
     * cycle operation is already in progress.
     */
    public void shutDown(boolean force)
        throws javax.jbi.JBIException
    {
        if ( force )
        {
            mComponent.setBusyForce();
        }
        else
        {
            mComponent.setBusy();
        }
        mComponent.setDesiredState(ComponentState.SHUTDOWN);
        persistState();
        try
        {
            // The boolean argument indicates that this call originated from a
            // JMX client. This affects how exceptions from the component are
            // handled.
            mComponentFramework.shutdownComponent(mComponent, force, true);
        }
        finally
        {
            mComponent.clearBusy();
        }
        return;
    }

    /**
     * Persist a change in the desired state of a component.
     */
    private void persistState()
    {
        try
        {
            if ( null == mUpdater )
            {
                mUpdater = ((com.sun.jbi.management.registry.Registry)
                    EnvironmentContext.getInstance().getRegistry()).getUpdater();
            }   
            mUpdater.setComponentState(mComponent.getDesiredState(),
                mComponent.getName());
        }
        catch ( com.sun.jbi.management.registry.RegistryException rEx )
        {
            EnvironmentContext.getInstance().getLogger().log(
                java.util.logging.Level.WARNING, rEx.getMessage(), rEx);
        }
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentOperation.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

/**
 * This class extends the Operation abstract class, and is designed to run on a
 * separate thread. The main purpose of this class is to provide a way to run
 * operations on multiple components (BCs and SEs) in parallel, and optionally
 * wait for all of them to complete.
 * 
 * To use this class, create an instance for each BC or SE, providing the
 * appropriate Component instances and and operation types to the constructor.
 * Then, create a thread for each instance and call its start() method.
 *
 * If the parent thread needs to wait for all of the operations to complete,
 * a single OperationCounter instance must be created and provided to the
 * constructor when creating the instances of this class. Then after all of the
 * threads have been started, the parent thread calls wait() on the instance
 * of OperationCounter. Optionally, the wait() call can have a timeout parameter
 * to limit the amount of time the parent thread will wait for completion of
 * all of the threads.
 *
 * In the Component Framework, this class is used in conjunction with the
 * OperationCounter class to multi-thread startup and shutdown of BCs and SEs
 * during AppServer startup and shutdown. This design prevents an ill-behaved
 * BC or SE from hanging the AppServer startup or shutdown should the BC or
 * SE hang.
 *
 * @author Sun Microsystems, Inc.
 */
class ComponentOperation extends Operation
{
    /**
     * The Component to be operated upon.
     */
    private Component mComponent;

    /**
     * Flag indicating whether the admin service needs notification of
     * operation completion.
     */
    private boolean mNotify;
 	 
    /**
     * The type of operation to be performed.
     */
    private int mOperation;

    /**
     * The JBI environment context.
     */
    private EnvironmentContext mEnv;

    /**
     * The message string translator.
     */
    private StringTranslator mTranslator;

    /**
     * Value for mOperation for an initialize operation.
     */
    public static final int INITIALIZE = 1;

    /**
     * Value for mOperation for a startup operation.
     */
    public static final int STARTUP = 2;

    /**
     * Value for mOperation for a shutdown operation.
     */
    public static final int SHUTDOWN = 3;

    /**
     * Constructor.
     *
     * @param component the Component instance that represents the BC or SE
     * to be operated upon.
     * @param operation the type of operation to be performed, either
     * INITIALIZE, STARTUP, or SHUTDOWN.
     */
    ComponentOperation(Component component, int operation)
    {
        super(null, null);
        init(component, operation);
    }

    /**
     * Constructor.
     *
     * @param counter the OperationCounter instance associated with this
     * operation.
     * @param component the Component instance that represents the BC or SE
     * to be operated upon.
     * @param operation the type of operation to be performed, either
     * INITIALIZE, STARTUP, or SHUTDOWN.
     */
    ComponentOperation(OperationCounter counter,
                       Component component,
                       int operation)
    {
        super(counter, null);
        init(component, operation);
    }

    /**
     * Constructor.
     *
     * @param counter the OperationCounter instance associated with this
     * operation.
     * @param component the Component instance that represents the BC or SE
     * to be operated upon.
     * @param operation the type of operation to be performed, either
     * INITIALIZE, STARTUP, or SHUTDOWN.
     * @param notify set to True if the framework should notify the admin
     * service when the operation is complete.
     */
    ComponentOperation(OperationCounter counter,
                       Component component,
                       int operation,
                       boolean notify)
    {
        super(counter, null);
        init(component, operation);
        mNotify = notify;
    }

    /**
     * Common constructor initialization code.
     *
     * @param component the Component instance that represents the BC or SE
     * to be operated upon.
     * @param operation the type of operation to be performed, either
     * INITIALIZE, STARTUP, or SHUTDOWN.
     */
    void init(Component component, int operation)
    {
        mEnv = EnvironmentContext.getInstance();
        mTranslator = (StringTranslator) mEnv.getStringTranslatorFor(this);

        if ( null == component )
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT,
                "component"));
        }
        mComponent = component;

        if ( INITIALIZE == operation
            || STARTUP == operation
            || SHUTDOWN == operation )
        {
            mOperation = operation;
        }
        else
        {
            throw new IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.INVALID_ARGUMENT,
                "operation", new Integer(operation)));
        }
    }

    /**
     * Return the Component instance for which this ComponentOperation was
     * created.
     *
     * @return the instance of Component for this operation.
     */
    Component getComponent()
    {
        return mComponent;
    }

    /**
     * Return the operation that this instance represents.
     *
     * @return the integer value of the operation code.
     */
    int getOperation()
    {
        return mOperation;
    }
 	 
    /**
     * Overridden method to call the ComponentFramework to initialize, startup
     * or shut down the BC or SE.
     *
     * @param argumentList the arguments for the operation. For this class this
     * is always null.
     * @return the returned string from operations that return a string or null
     * for operations that have no return value.
     * @throws Throwable if any error occurs.
     */
    Object process(Object argumentList[]) throws Throwable
    {
        ComponentFramework cf = mEnv.getComponentFramework();
        try
        {
            if ( mComponent.isBinding() || mComponent.isEngine() )
            {
                if ( INITIALIZE == mOperation )
                {
                    cf.initializeComponent(mComponent);
                }
                else if ( STARTUP == mOperation )
                {
                    cf.startComponent(mComponent);
                }
                else if ( SHUTDOWN == mOperation )
                {
                    cf.shutdownComponent(mComponent);
                }
            }
            else
            {
                cf.getLogger().warning(mTranslator.getString(
                    LocalStringKeys.CF_COMP_UNRECOGNIZED_TYPE,
                    mComponent.getName(),
                    mComponent.getComponentType().toString()));
            }
        }
        catch (javax.jbi.JBIException jbiEx)
        {
            // All exceptions caught here already have a formatted
            // message that requires no further formatting.
            cf.getLogger().warning(jbiEx.getMessage());
        }

        // If requested, notify the admin service of completion.
 	 
        if ( mNotify )
        {
            mEnv.getManagementService().updateComponentState(
                mComponent.getName());
        }
        return null;
    }

}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Constants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

/**
 * Global constants shared by framework junit tests.
 *
 * @author Sun Microsystems, Inc.
 */
public class Constants
{
    //
    // Binding Component / Service Engine names
    //

    /**
     * Constant for BC component name.
     */
    public static final String BC_NAME =
        "TestBinding";

    /**
     * Constant for BC description.
     */
    public static final String BC_DESCRIPTION =
        "This is a binding component used for unit testing";

    /**
     * Constant for BC component name to force bad bootstrap init() call.
     */
    public static final String BC_NAME_BAD_BOOTSTRAP_INIT =
        "BC_BadBootInit";

    /**
     * Constant for BC component name to force bad bootstrap init() call
     * and a bad bootstrap cleanUp() call.
     */
    public static final String BC_NAME_BAD_BOOTSTRAP_INIT_CLEANUP =
        "BC_BadBootInitAndCleanup";

    /**
     * Constant for BC component name to force bad bootstrap cleanUp() call
     * after onInstall().
     */
    public static final String BC_NAME_BAD_BOOTSTRAP_INSTALL_CLEANUP =
        "BC_BadInstallCleanup";

    /**
     * Constant for BC component name to force bad bootstrap onInstall() call.
     */
    public static final String BC_NAME_BAD_BOOTSTRAP_ONINSTALL =
        "BC_BadOnInstall";

    /**
     * Constant for BC component name to force bad bootstrap onInstall() and
     * cleanUp() calls.
     */
    public static final String BC_NAME_BAD_BOOTSTRAP_ONINSTALL_CLEANUP =
        "BC_BadOnInstallAndCleanup";

    /**
     * Constant for BC component name to force bad bootstrap onUninstall() call.
     */
    public static final String BC_NAME_BAD_BOOTSTRAP_ONUNINSTALL =
        "BC_BadOnUninstall";

    /**
     * Constant for BC component name to force bad bootstrap onUninstall() and
     * cleanUp() calls.
     */
    public static final String BC_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP =
        "BC_BadOnUninstallAndCleanup";

    /**
     * Constant for BC component name to force bad bootstrap cleanUp() call
     * after onUninstall().
     */
    public static final String BC_NAME_BAD_BOOTSTRAP_UNINSTALL_CLEANUP =
        "BC_BadUninstallCleanup";

    /**
     * Constant for BC component name to force bad bootstrap upgrade() call.
     */
    public static final String BC_NAME_BAD_BOOTSTRAP_UPGRADE =
        "BC_BadUpgrade";

    /**
     * Constant for BC component name to force bootstrap onInstall() to
     * modify the life cycle class path.
     */
    public static final String BC_NAME_BOOTSTRAP_MODIFY_CLASS_PATH =
        "BC_ModifyClassPath";

    /**
     * Constant for BC component name to force bad init() call.
     */
    public static final String BC_NAME_BAD_INIT =
        "BC_BadInit";

    /**
     * Constant for BC component name to force bad start() call.
     */
    public static final String BC_NAME_BAD_START =
        "BC_BadStart";

    /**
     * Constant for BC component name to force bad stop() call.
     */
    public static final String BC_NAME_BAD_STOP =
        "BC_BadStop";

    /**
     * Constant for BC component name to force bad shutDown() call.
     */
    public static final String BC_NAME_BAD_SHUTDOWN =
        "BC_BadShutDown";

    /**
     * Constant for BC component name to force slow init() call.
     */
    public static final String BC_NAME_SLOW_INIT =
        "BC_SlowInit";

    /**
     * Constant for BC component name to force slow start() call.
     */
    public static final String BC_NAME_SLOW_START =
        "BC_SlowStart";

    /**
     * Constant for BC component name to force slow stop() call.
     */
    public static final String BC_NAME_SLOW_STOP =
        "BC_SlowStop";

    /**
     * Constant for BC component name to force slow shutDown() call.
     */
    public static final String BC_NAME_SLOW_SHUTDOWN =
        "BC_SlowShutDown";

    /**
     * Constant for BC component name to force timeout on init() call.
     */
    public static final String BC_NAME_TIMEOUT_INIT =
        "BC_TimeoutInit";

    /**
     * Constant for BC component name to force timeout on start() call.
     */
    public static final String BC_NAME_TIMEOUT_START =
        "BC_TimeoutStart";

    /**
     * Constant for BC component name to force timeout on stop() call.
     */
    public static final String BC_NAME_TIMEOUT_STOP =
        "BC_TimeoutStop";

    /**
     * Constant for BC component name to force timeout on shutDown() call.
     */
    public static final String BC_NAME_TIMEOUT_SHUTDOWN =
        "BC_TimeoutShutDown";

    /**
     * Constant for BC bootstrap class name.
     */
    public static final String BC_BOOTSTRAP_CLASS_NAME =
        "com.sun.jbi.framework.BindingBootstrap";

    /**
     * Constant for BC bootstrap class name for upgrade.
     */
    public static final String BC_BOOTSTRAP_CLASS_NAME_UPGRADE =
        "com.sun.jbi.framework.BindingBootstrapUpgrade";

    /**
     * Constant for BC bootstrap class path.
     */
    public static final String BC_BOOTSTRAP_CLASS_PATH =
        "runtime/framework/bld/test-classes/com/sun/jbi/framework/";

    /**
     * Constant for BC lifecycle class name.
     */
    public static final String BC_LIFECYCLE_CLASS_NAME =
        "com.sun.jbi.framework.Binding";

    /**
     * Constant for BC lifecycle class name for upgrade.
     */
    public static final String BC_LIFECYCLE_CLASS_NAME_UPGRADE =
        "com.sun.jbi.framework.BindingUpgrade";

    /**
     * Constant for alternate BC lifecycle class name.
     */
    public static final String BC_LIFECYCLE_CLASS_NAME_ALTERNATE =
        "testbinding.Binding";

    /**
     * Constant for BC lifecycle class name that throws an exception when
     * getLifeCycle() is called.
     */
    public static final String BC_LIFECYCLE_CLASS_NAME_BAD_GET_LIFECYCLE =
        "com.sun.jbi.framework.BindingBadGetLifeCycle";

    /**
     * Constant for BC lifecycle class name that returns null when
     * getLifeCycle() is called.
     */
    public static final String BC_LIFECYCLE_CLASS_NAME_NULL_GET_LIFECYCLE =
        "com.sun.jbi.framework.BindingNullGetLifeCycle";

    /**
     * Constant for BC lifecycle class path.
     */
    public static final String BC_LIFECYCLE_CLASS_PATH =
        "runtime/framework/bld/test-classes/com/sun/jbi/framework/";

    /**
     * Constant for BC lifecycle class path.
     */
    public static final String BC_LIFECYCLE_CLASS_PATH_NEW =
        "runtime/framework/bld/classes/com/sun/jbi/framework/";

    /**
     * Constant for SE component name.
     */
    public static final String SE_NAME =
        "TestEngine";

    /**
     * Constant for SE description.
     */
    public static final String SE_DESCRIPTION =
        "This is a service engine used for unit testing";

    /**
     * Constant for SE component name to force bad bootstrap init() call.
     */
    public static final String SE_NAME_BAD_BOOTSTRAP_INIT =
        "SE_BadBootInit";

    /**
     * Constant for SE component name to force bad bootstrap init() call
     * and bad bootstrap cleanUp() call.
     */
    public static final String SE_NAME_BAD_BOOTSTRAP_INIT_CLEANUP =
        "SE_BadBootInitAndCleanup";

    /**
     * Constant for SE component name to force bad bootstrap cleanUp() call
     * after onInstall().
     */
    public static final String SE_NAME_BAD_BOOTSTRAP_INSTALL_CLEANUP =
        "SE_BadInstallCleanup";

    /**
     * Constant for SE component name to force bad bootstrap onInstall() call.
     */
    public static final String SE_NAME_BAD_BOOTSTRAP_ONINSTALL =
        "SE_BadOnInstall";

    /**
     * Constant for SE component name to force bad bootstrap onInstall() and
     * cleanUp() calls.
     */
    public static final String SE_NAME_BAD_BOOTSTRAP_ONINSTALL_CLEANUP =
        "SE_BadOnInstallAndCleanup";

    /**
     * Constant for SE component name to force bad bootstrap onUninstall() call.
     */
    public static final String SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL =
        "SE_BadOnUninstall";

    /**
     * Constant for SE component name to force bad bootstrap onUninstall() and
     * cleanUp() calls.
     */
    public static final String SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP =
        "SE_BadOnUninstallAndCleanup";

    /**
     * Constant for SE component name to force bad bootstrap cleanUp() call
     * after onUninstall().
     */
    public static final String SE_NAME_BAD_BOOTSTRAP_UNINSTALL_CLEANUP =
        "SE_BadUninstallCleanup";

    /**
     * Constant for SE component name to force bootstrap onInstall() to
     * modify the life cycle class path.
     */
    public static final String SE_NAME_BOOTSTRAP_MODIFY_CLASS_PATH =
        "SE_ModifyClassPath";

    /**
     * Constant for SE component name to force bad init() call.
     */
    public static final String SE_NAME_BAD_INIT =
        "SE_BadInit";

    /**
     * Constant for SE component name to force bad start() call.
     */
    public static final String SE_NAME_BAD_START =
        "SE_BadStart";

    /**
     * Constant for SE component name to force bad stop() call.
     */
    public static final String SE_NAME_BAD_STOP =
        "SE_BadStop";

    /**
     * Constant for SE component name to force bad shutDown() call.
     */
    public static final String SE_NAME_BAD_SHUTDOWN =
        "SE_BadShutDown";

    /**
     * Constant for SE component name to force bad shutDown() and bootstrap
     * onUninstall() calls.
     */
    public static final String SE_NAME_BAD_SHUTDOWN_BOOTSTRAP_ONUNINSTALL =
        "SE_BadShutDownOnUninstall";

    /**
     * Constant for SE component name to force timeout on init() call.
     */
    public static final String SE_NAME_TIMEOUT_INIT =
        "SE_TimeoutInit";

    /**
     * Constant for SE component name to force timeout on start() call.
     */
    public static final String SE_NAME_TIMEOUT_START =
        "SE_TimeoutStart";

    /**
     * Constant for SE component name to force timeout on stop() call.
     */
    public static final String SE_NAME_TIMEOUT_STOP =
        "SE_TimeoutStop";

    /**
     * Constant for SE component name to force timeout on shutDown() call.
     */
    public static final String SE_NAME_TIMEOUT_SHUTDOWN =
        "SE_TimeoutShutDown";

    /**
     * Constant for SE bootstrap class name.
     */
    public static final String SE_BOOTSTRAP_CLASS_NAME =
        "com.sun.jbi.framework.EngineBootstrap";

    /**
     * Constant for SE bootstrap class path.
     */
    public static final String SE_BOOTSTRAP_CLASS_PATH =
        "runtime/framework/bld/test-classes/com/sun/jbi/framework/";

    /**
     * Constant for SE lifecycle class name.
     */
    public static final String SE_LIFECYCLE_CLASS_NAME =
        "com.sun.jbi.framework.Engine";

    /**
     * Constant for alternate SE lifecycle class name.
     */
    public static final String SE_LIFECYCLE_CLASS_NAME_ALTERNATE =
        "testengine.Engine";

    /**
     * Constant for SE lifecycle class name that throws an exception when
     * getLifeCycle() is called.
     */
    public static final String SE_LIFECYCLE_CLASS_NAME_BAD_GET_LIFECYCLE =
        "com.sun.jbi.framework.EngineBadGetLifeCycle";

    /**
     * Constant for SE lifecycle class name that returns null when
     * getLifeCycle() is called.
     */
    public static final String SE_LIFECYCLE_CLASS_NAME_NULL_GET_LIFECYCLE =
        "com.sun.jbi.framework.EngineNullGetLifeCycle";

    /**
     * Constant for SE lifecycle class path.
     */
    public static final String SE_LIFECYCLE_CLASS_PATH =
        "runtime/framework/bld/test-classes/com/sun/jbi/framework/";

    /**
     * Constant for shared library 1 component name.
     */
    public static final String SL_1_NAME = "SL01";

    /**
     * Constant for shared library 1 description.
     */
    public static final String SL_1_DESC = "Shared library for testing";

    /**
     * Constant for shared library 1 root.
     */
    public static final String SL_1_ROOT = "/SL01";

    /**
     * Constant for shared library 2 component name.
     */
    public static final String SL_2_NAME = "SL02";

    /**
     * Constant for shared library 2 description.
     */
    public static final String SL_2_DESC = "Shared library for testing";

    /**
     * Constant for shared library 2 root.
     */
    public static final String SL_2_ROOT = "/SL02";

    //
    // Service Assembly / Service Unit names
    //

    /**
     * Constant for Service Unit name that's always lowest in the collating
     * sequence.
     */
    public static final String SU_NAME_FIRST =
        "SU_A";

    /**
     * Constant for Service Unit name to cause an exception on the deploy.
     */
    public static final String SU_NAME_DEPLOY_EXCEPTION =
        "SU_DeployException";

    /**
     * Constant for Service Unit name to cause a timeout on the deploy.
     */
    public static final String SU_NAME_DEPLOY_TIMEOUT =
        "SU_DeployTimeout";

    /**
     * Constant for Service Unit name to cause an exception on the init.
     */
    public static final String SU_NAME_INIT_EXCEPTION =
        "SU_InitException";

    /**
     * Constant for Service Unit name to cause a timeout on the init.
     */
    public static final String SU_NAME_INIT_TIMEOUT =
        "SU_InitTimeout";

    /**
     * Constant for Service Unit name to cause an exception on the shutdown.
     */
    public static final String SU_NAME_SHUTDOWN_EXCEPTION =
        "SU_ShutDownException";

    /**
     * Constant for Service Unit name to cause a timeout on the shutdown.
     */
    public static final String SU_NAME_SHUTDOWN_TIMEOUT =
        "SU_ShutDownTimeout";

    /**
     * Constant for Service Unit name to cause an exception on the start.
     */
    public static final String SU_NAME_START_EXCEPTION =
        "SU_StartException";

    /**
     * Constant for Service Unit name to cause a timeout on the start.
     */
    public static final String SU_NAME_START_TIMEOUT =
        "SU_StartTimeout";

    /**
     * Constant for Service Unit name to cause an exception on the stop.
     */
    public static final String SU_NAME_STOP_EXCEPTION =
        "SU_StopException";

    /**
     * Constant for Service Unit name to cause a timeout on the stop.
     */
    public static final String SU_NAME_STOP_TIMEOUT =
        "SU_StopTimeout";

    /**
     * Constant for Service Unit name to cause an exception on the undeploy.
     */
    public static final String SU_NAME_UNDEPLOY_EXCEPTION =
        "SU_UndeployException";

    /**
     * Constant for Service Unit name to cause a timeout on the undeploy.
     */
    public static final String SU_NAME_UNDEPLOY_TIMEOUT =
        "SU_UndeployTimeout";

    /**
     * Constant for Service Unit root path.
     */
    public static final String SU_ROOT =
        "framework/regress/TestServiceUnit.zip";

    /**
     * Constant for Service Assembly name.
     */
    public static final String SA_NAME =
        "Test_SA";

    /**
     * Constant for Service Unit name.
     */
    public static final String SU_NAME =
        "Test_SA_SU1";

    /**
     * Constant for upgrade file name.
     */
    public static final String UPGRADED_FILE_NAME =
        "upgraded.dat";

    //
    // System property names
    //

    /**
     * Constant for system property for component name.
     */
    public static final String PROPERTY_COMPONENT_NAME =
        "com.sun.jbi.framework.test.componentName";
}

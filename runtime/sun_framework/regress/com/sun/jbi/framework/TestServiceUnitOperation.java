/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceUnitOperation.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.management.ComponentInstallationContext;

import java.util.ArrayList;

import javax.jbi.component.ServiceUnitManager;

/**
 * Tests for the ServiceUnitOperation.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServiceUnitOperation
    extends junit.framework.TestCase
{
    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * Current SRCROOT path.
     */
    private String mSrcroot;

    /**
     * Helper class to setup environment for testing.
     */
    private EnvironmentSetup mSetup;

    /**
     * Instance of the EnvironmentContext class.
     */
    private EnvironmentContext mContext;

    /**
     * Instance of Component.
     */
    private Component mComponent;

    /**
     * Instance of ServiceUnitManager.
     */
    private ServiceUnitManager mServiceUnitManager;

    /**
     * Local instance of the ComponentInstallationContext class.
     */
    private ComponentInstallationContext mInstallContext;

    /**
     * Instance of the ComponentFramework class.
     */
    private ComponentFramework mCompFW;

    /**
     * Local instance of the ComponentRegistry class.
     */
    private ComponentRegistry mCompReg;

    /**
     * Arguments for ServiceUnitOperation.
     */
    private Object mArgs[];

    /**
     * Local instance of the OperationCounter class.
     */
    private OperationCounter mCounter;

    /**
     * Local instance of the ServiceUnitOperation class.
     */
    private ServiceUnitOperation mOperation;

    /**
     * Service Unit root file path.
     */
    private String mServiceUnitRoot;

    /**
     * Constant for invalid operation value.
     */
    private static final int BADOPER = 999;

    /**
     * Constant for Service Unit name.
     */
    private static final String SERVICE_UNIT_NAME =
        "TestServiceUnit";

    /**
     * Constant for Service Unit root path.
     */
    private static final String SERVICE_UNIT_ROOT =
        "framework/regress/TestServiceUnit.zip";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestServiceUnitOperation(String aTestName)
    {
        super(aTestName);
        mTestName = aTestName;
    }

    /**
     * Setup for the test. This creates the EnvironmentContext instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);
        mSrcroot = System.getProperty("junit.srcroot") + "/";

        // Create and initialize the EnvironmentContext. Create, initialize,
        // and start up the Component Registry and Component Framework.

        mSetup = new EnvironmentSetup();
        mContext = mSetup.getEnvironmentContext();
        mSetup.startup(true, true);
        mCompReg = mContext.getComponentRegistry();
        mCompFW = mContext.getComponentFramework();

        // Create component class path lists

        ArrayList bootstrapClassPath = new ArrayList();
        bootstrapClassPath.add(mSrcroot + Constants.BC_BOOTSTRAP_CLASS_PATH);
        ArrayList componentClassPath = new ArrayList();
        componentClassPath.add(Constants.BC_LIFECYCLE_CLASS_PATH);

        // Create installation context

        mInstallContext =
            new ComponentInstallationContext(
                Constants.BC_NAME,
                ComponentInstallationContext.BINDING,
                Constants.BC_LIFECYCLE_CLASS_NAME,
                componentClassPath,
                null);
        mInstallContext.setInstallRoot(mSrcroot);
        mInstallContext.setIsInstall(true);

        // Install the component and start it

        mCompFW.loadBootstrap(
            mInstallContext,
            Constants.BC_BOOTSTRAP_CLASS_NAME,
            bootstrapClassPath,
            null, false);
        mCompFW.installComponent(mInstallContext);
        mComponent = mCompReg.getComponent(Constants.BC_NAME);
        mCompFW.startComponent(mComponent);

        // Get the ServiceUnitManager instance.
        mServiceUnitManager = mComponent.getServiceUnitManager();

        // Set up argument list for the ServiceUnitOperation
        mArgs = new Object[2];
        mArgs[0] = (Object) SERVICE_UNIT_NAME;
        mArgs[1] = (Object) mServiceUnitRoot;
        mCounter = new OperationCounter();

        // Set the service unit root path
        mServiceUnitRoot = mSrcroot + SERVICE_UNIT_ROOT;
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        mSetup.shutdown(true, true);
        System.err.println("***** END of test " + mTestName);
    }

// =============================  test methods ================================

    /**
     * Tests constructor with bad operation parameter. An exception is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testConstructorBadOperation()
        throws Exception
    {
        try
        {
            mOperation = new ServiceUnitOperation(mCounter,
                mComponent.getName(), mServiceUnitManager, BADOPER, mArgs);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW0062")));
        }
    }

    /**
     * Tests constructor with null Service Unit manager parameter. An exception
     * is expected.
     * @throws Exception if an unexpected error occurs.
     */
    public void testConstructorNullSuManager()
        throws Exception
    {
        try
        {
            mOperation = new ServiceUnitOperation(mCounter,
                mComponent.getName(), null, ServiceUnitOperation.DEPLOY, mArgs);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Unexpected exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW0063")));
        }
    }

    /**
     * Tests process method calling deploy on the ServiceUnitManager.
     * @throws Exception if an unexpected error occurs.
     * @throws Throwable if an unexpected error occurs.
     */

    public void testProcessDeploy()
        throws Exception, Throwable
    {
        mOperation = new ServiceUnitOperation(mCounter,
            mComponent.getName(), mServiceUnitManager,
            ServiceUnitOperation.DEPLOY, mArgs);
        String msg = (String) mOperation.process(mArgs);
        assertNotNull("Failure deploying Service Unit",
                      msg);
        assertTrue("Failure deploying Service Unit",
                   (-1 < msg.indexOf(SERVICE_UNIT_NAME)));
    }

    /**
     * Tests process method calling init on the ServiceUnitManager. The test
     * binding does no special processing, so if no exception occurs, the
     * test was successful.
     * @throws Exception if an unexpected error occurs.
     * @throws Throwable if an unexpected error occurs.
     */

    public void testProcessInit()
        throws Exception, Throwable
    {
        mOperation = new ServiceUnitOperation(mCounter,
            mComponent.getName(), mServiceUnitManager,
            ServiceUnitOperation.INIT, mArgs);
        mOperation.process(mArgs);
    }

    /**
     * Tests process method calling start on the ServiceUnitManager. The test
     * binding does no special processing, so if no exception occurs, the
     * test was successful.
     * @throws Exception if an unexpected error occurs.
     * @throws Throwable if an unexpected error occurs.
     */

    public void testProcessStart()
        throws Exception, Throwable
    {
        mOperation = new ServiceUnitOperation(mCounter,
            mComponent.getName(), mServiceUnitManager,
            ServiceUnitOperation.START, mArgs);
        mOperation.process(mArgs);
    }

    /**
     * Tests process method calling stop on the ServiceUnitManager. The test
     * binding does no special processing, so if no exception occurs, the
     * test was successful.
     * @throws Exception if an unexpected error occurs.
     * @throws Throwable if an unexpected error occurs.
     */

    public void testProcessStop()
        throws Exception, Throwable
    {
        mOperation = new ServiceUnitOperation(mCounter,
            mComponent.getName(), mServiceUnitManager,
            ServiceUnitOperation.STOP, mArgs);
        mOperation.process(mArgs);
    }

    /**
     * Tests process method calling shutDown on the ServiceUnitManager. The test
     * binding does no special processing, so if no exception occurs, the
     * test was successful.
     * @throws Exception if an unexpected error occurs.
     * @throws Throwable if an unexpected error occurs.
     */

    public void testProcessShutDown()
        throws Exception, Throwable
    {
        mOperation = new ServiceUnitOperation(mCounter,
            mComponent.getName(), mServiceUnitManager,
            ServiceUnitOperation.SHUTDOWN, mArgs);
        mOperation.process(mArgs);
    }

    /**
     * Tests undeploy with a good result.
     * @throws Exception if an unexpected error occurs.
     * @throws Throwable if an unexpected error occurs.
     */

    public void testProcessUndeploy()
        throws Exception, Throwable
    {
        mOperation = new ServiceUnitOperation(mCounter,
            mComponent.getName(), mServiceUnitManager,
            ServiceUnitOperation.UNDEPLOY, mArgs);
        String msg = (String) mOperation.process(mArgs);
        assertNotNull("Failure undeploying Service Unit",
                      msg);
        assertTrue("Failure undeploying Service Unit",
                   (-1 < msg.indexOf(SERVICE_UNIT_NAME)));
    }

}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestWsdlReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import com.sun.jbi.wsdl2.Description;
import com.sun.jbi.wsdl2.Definitions;
import com.sun.jbi.wsdl2.WsdlException;

/**
 * Unit tests for the WSDL factory.
 * 
 * @author Sun Microsystems Inc.
 */
public class TestWsdlReader extends junit.framework.TestCase
{
    /** Reader to be used in various tests */
    private com.sun.jbi.wsdl2.WsdlReader mReader = null;

    /** Old system property value for java.protocol.handler.pkgs */
    private String mOldJavaProtocolHandlerPkgs = null;

    /**
     * Set up for each test. 
     * 
     * @exception Exception when set up fails for any reason. This tells JUnit 
     *                      that it cannot run the test.
     */
    public void setUp() throws Exception
    {
        super.setUp();

        com.sun.jbi.wsdl2.WsdlFactory factory = WsdlFactory.newInstance();

        this.mReader = factory.newWsdlReader();

        this.mOldJavaProtocolHandlerPkgs = System.setProperty( 
            "java.protocol.handler.pkgs", 
            "com.sun.jbi.wsdl2.impl");
    }

    /**
     * Clean up after each test case.
     * 
     * @exception Exception when clean up fails for any reason.
     */
    public void tearDown() throws Exception
    {
        if (this.mOldJavaProtocolHandlerPkgs != null)
        {
            System.setProperty(
                "java.protocol.handler.pkgs",
                this.mOldJavaProtocolHandlerPkgs);
        }
    }

    /**
     * Test creation of a WsldFactory instance.
     * 
     * @exception Exception when any unexpected error occurs.
     */
    public void testReadFromUri() throws Exception
    {
        //
        //  First pass -- read a non-WSDL (but still XML) file.
        //
        try
        {
            Description defs = mReader.readDescription("test:///notwsdl.wsdl");
            assertTrue(false);  // If we get here, the reader failed to object.

            Definitions defs1 = mReader.readWsdl("test:///notwsdl.wsdl");
            assertTrue(false);  // If we get here, the reader failed to object.
        }
        catch (WsdlException ex)
        {
            String  msg = ex.getMessage();
            ex.printStackTrace();
            assertTrue(true);   // We got some sort of objection!
            System.out.println(msg);
            assertTrue(msg.indexOf(
                "The document is not a description@http://www.w3.org/") != -1);
        }

        //
        //  Second pass -- read a valid WSDL file
        //
        Description defs = mReader.readDescription("test:///test1.wsdl");
        assertTrue(defs != null);

        Definitions defs1 = mReader.readWsdl("test:///test1.wsdl");
        assertTrue(defs1 != null);
    }

    /**
     * Test reading a WSDL document from the wrong namespace.
     * 
     * @exception Exception when any unexpected error occurs.
     */
    public void testReadFromWrongNamespace() throws Exception
    {
        try
        {
            Description defs = mReader.readDescription("test:///test2.wsdl");
            assertTrue(false); // If we get here, the reader failed to object

            Definitions defs1 = mReader.readWsdl("test:///test2.wsdl");
            assertTrue(false); // If we get here, the reader failed to object
        }
        catch (WsdlException ex)
        {
            String msg = ex.getMessage();

            assertTrue(true);   // We got some sort of objection!
            System.out.println(msg);
            assertTrue(msg.indexOf(
                "document element namespace mismatch") != -1);
        }
    }

    /**
     * Test reading an ill-formed WSDL document.
     * 
     * @exception Exception when any unexpected error occurs.
     */
    public void testReadIllFormed() throws Exception
    {
        try
        {
            Description defs = mReader.readDescription("test:///test3.wsdl");
            assertTrue(false); // If we get here, the reader failed to object

            // Definitions defs1 = mReader.readWsdl("test:///test3.wsdl");
            // assertTrue(false); // If we get here, the reader failed to object
        }
        catch (WsdlException ex)
        {
            String  msg = ex.getMessage();
            assertTrue(true);   // We got some sort of objection!
            System.out.println(msg);
            assertTrue(msg.indexOf("</service>") != -1);
        }
    }

    /**
     * Test reading an invalid WSDL document.
     * 
     * @exception Exception when any unexpected error occurs.
     */
    public void testInvalid() throws Exception
    {
        try
        {
            Description defs = mReader.readDescription("test:///test4.wsdl");
            assertTrue(false); // If we get here, the reader failed to object

            Definitions defs1 = mReader.readWsdl("test:///test4.wsdl");
            assertTrue(false); // If we get here, the reader failed to object
        }
        catch (WsdlException ex)
        {
            String  msg = ex.getMessage();

            assertTrue(true);   // We got some sort of objection!
            System.out.println(msg);
            assertTrue(msg.indexOf(
                "instead of 'operation@http://www.w3.org/ns/wsdl'") != -1);
        }
    }

    /**
     * Test creation of a WsldFactory instance.
     *
     * @exception Exception when any unexpected error occurs.
     */
    public void testReadFromUriForBackwardCompatibility() throws Exception
    {
        //
        //  pass -- read a valid WSDL file, but with namespace of x2006x01 version
        //
        Description defs = mReader.readDescription("test:///test5.wsdl");
        assertTrue(defs != null);

        Definitions defs1 = mReader.readWsdl("test:///test5.wsdl");
        assertTrue(defs1 != null);
    }
}

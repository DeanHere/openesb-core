/*
 * XML Type:  DescriptionType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.DescriptionType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML DescriptionType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class DescriptionTypeImpl extends org.w3.ns.wsdl.impl.ExtensibleDocumentedTypeImpl implements org.w3.ns.wsdl.DescriptionType
{
    
    public DescriptionTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPORT$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "import");
    private static final javax.xml.namespace.QName INCLUDE$2 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "include");
    private static final javax.xml.namespace.QName TYPES$4 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "types");
    private static final javax.xml.namespace.QName INTERFACE$6 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "interface");
    private static final javax.xml.namespace.QName BINDING$8 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "binding");
    private static final javax.xml.namespace.QName SERVICE$10 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "service");
    private static final javax.xml.namespace.QName TARGETNAMESPACE$12 = 
        new javax.xml.namespace.QName("", "targetNamespace");
    
    
    /**
     * Gets array of all "import" elements
     */
    public org.w3.ns.wsdl.ImportType[] getImportArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(IMPORT$0, targetList);
            org.w3.ns.wsdl.ImportType[] result = new org.w3.ns.wsdl.ImportType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "import" element
     */
    public org.w3.ns.wsdl.ImportType getImportArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ImportType target = null;
            target = (org.w3.ns.wsdl.ImportType)get_store().find_element_user(IMPORT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "import" element
     */
    public int sizeOfImportArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(IMPORT$0);
        }
    }
    
    /**
     * Sets array of all "import" element
     */
    public void setImportArray(org.w3.ns.wsdl.ImportType[] ximportArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(ximportArray, IMPORT$0);
        }
    }
    
    /**
     * Sets ith "import" element
     */
    public void setImportArray(int i, org.w3.ns.wsdl.ImportType ximport)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ImportType target = null;
            target = (org.w3.ns.wsdl.ImportType)get_store().find_element_user(IMPORT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(ximport);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "import" element
     */
    public org.w3.ns.wsdl.ImportType insertNewImport(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ImportType target = null;
            target = (org.w3.ns.wsdl.ImportType)get_store().insert_element_user(IMPORT$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "import" element
     */
    public org.w3.ns.wsdl.ImportType addNewImport()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ImportType target = null;
            target = (org.w3.ns.wsdl.ImportType)get_store().add_element_user(IMPORT$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "import" element
     */
    public void removeImport(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(IMPORT$0, i);
        }
    }
    
    /**
     * Gets array of all "include" elements
     */
    public org.w3.ns.wsdl.IncludeType[] getIncludeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INCLUDE$2, targetList);
            org.w3.ns.wsdl.IncludeType[] result = new org.w3.ns.wsdl.IncludeType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "include" element
     */
    public org.w3.ns.wsdl.IncludeType getIncludeArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.IncludeType target = null;
            target = (org.w3.ns.wsdl.IncludeType)get_store().find_element_user(INCLUDE$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "include" element
     */
    public int sizeOfIncludeArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INCLUDE$2);
        }
    }
    
    /**
     * Sets array of all "include" element
     */
    public void setIncludeArray(org.w3.ns.wsdl.IncludeType[] includeArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(includeArray, INCLUDE$2);
        }
    }
    
    /**
     * Sets ith "include" element
     */
    public void setIncludeArray(int i, org.w3.ns.wsdl.IncludeType include)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.IncludeType target = null;
            target = (org.w3.ns.wsdl.IncludeType)get_store().find_element_user(INCLUDE$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(include);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "include" element
     */
    public org.w3.ns.wsdl.IncludeType insertNewInclude(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.IncludeType target = null;
            target = (org.w3.ns.wsdl.IncludeType)get_store().insert_element_user(INCLUDE$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "include" element
     */
    public org.w3.ns.wsdl.IncludeType addNewInclude()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.IncludeType target = null;
            target = (org.w3.ns.wsdl.IncludeType)get_store().add_element_user(INCLUDE$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "include" element
     */
    public void removeInclude(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INCLUDE$2, i);
        }
    }
    
    /**
     * Gets array of all "types" elements
     */
    public org.w3.ns.wsdl.TypesType[] getTypesArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(TYPES$4, targetList);
            org.w3.ns.wsdl.TypesType[] result = new org.w3.ns.wsdl.TypesType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "types" element
     */
    public org.w3.ns.wsdl.TypesType getTypesArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.TypesType target = null;
            target = (org.w3.ns.wsdl.TypesType)get_store().find_element_user(TYPES$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "types" element
     */
    public int sizeOfTypesArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TYPES$4);
        }
    }
    
    /**
     * Sets array of all "types" element
     */
    public void setTypesArray(org.w3.ns.wsdl.TypesType[] typesArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(typesArray, TYPES$4);
        }
    }
    
    /**
     * Sets ith "types" element
     */
    public void setTypesArray(int i, org.w3.ns.wsdl.TypesType types)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.TypesType target = null;
            target = (org.w3.ns.wsdl.TypesType)get_store().find_element_user(TYPES$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(types);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "types" element
     */
    public org.w3.ns.wsdl.TypesType insertNewTypes(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.TypesType target = null;
            target = (org.w3.ns.wsdl.TypesType)get_store().insert_element_user(TYPES$4, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "types" element
     */
    public org.w3.ns.wsdl.TypesType addNewTypes()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.TypesType target = null;
            target = (org.w3.ns.wsdl.TypesType)get_store().add_element_user(TYPES$4);
            return target;
        }
    }
    
    /**
     * Removes the ith "types" element
     */
    public void removeTypes(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TYPES$4, i);
        }
    }
    
    /**
     * Gets array of all "interface" elements
     */
    public org.w3.ns.wsdl.InterfaceType[] getInterfaceArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INTERFACE$6, targetList);
            org.w3.ns.wsdl.InterfaceType[] result = new org.w3.ns.wsdl.InterfaceType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "interface" element
     */
    public org.w3.ns.wsdl.InterfaceType getInterfaceArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType target = null;
            target = (org.w3.ns.wsdl.InterfaceType)get_store().find_element_user(INTERFACE$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "interface" element
     */
    public int sizeOfInterfaceArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTERFACE$6);
        }
    }
    
    /**
     * Sets array of all "interface" element
     */
    public void setInterfaceArray(org.w3.ns.wsdl.InterfaceType[] xinterfaceArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(xinterfaceArray, INTERFACE$6);
        }
    }
    
    /**
     * Sets ith "interface" element
     */
    public void setInterfaceArray(int i, org.w3.ns.wsdl.InterfaceType xinterface)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType target = null;
            target = (org.w3.ns.wsdl.InterfaceType)get_store().find_element_user(INTERFACE$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(xinterface);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "interface" element
     */
    public org.w3.ns.wsdl.InterfaceType insertNewInterface(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType target = null;
            target = (org.w3.ns.wsdl.InterfaceType)get_store().insert_element_user(INTERFACE$6, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "interface" element
     */
    public org.w3.ns.wsdl.InterfaceType addNewInterface()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType target = null;
            target = (org.w3.ns.wsdl.InterfaceType)get_store().add_element_user(INTERFACE$6);
            return target;
        }
    }
    
    /**
     * Removes the ith "interface" element
     */
    public void removeInterface(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTERFACE$6, i);
        }
    }
    
    /**
     * Gets array of all "binding" elements
     */
    public org.w3.ns.wsdl.BindingType[] getBindingArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(BINDING$8, targetList);
            org.w3.ns.wsdl.BindingType[] result = new org.w3.ns.wsdl.BindingType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "binding" element
     */
    public org.w3.ns.wsdl.BindingType getBindingArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingType target = null;
            target = (org.w3.ns.wsdl.BindingType)get_store().find_element_user(BINDING$8, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "binding" element
     */
    public int sizeOfBindingArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BINDING$8);
        }
    }
    
    /**
     * Sets array of all "binding" element
     */
    public void setBindingArray(org.w3.ns.wsdl.BindingType[] bindingArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(bindingArray, BINDING$8);
        }
    }
    
    /**
     * Sets ith "binding" element
     */
    public void setBindingArray(int i, org.w3.ns.wsdl.BindingType binding)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingType target = null;
            target = (org.w3.ns.wsdl.BindingType)get_store().find_element_user(BINDING$8, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(binding);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "binding" element
     */
    public org.w3.ns.wsdl.BindingType insertNewBinding(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingType target = null;
            target = (org.w3.ns.wsdl.BindingType)get_store().insert_element_user(BINDING$8, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "binding" element
     */
    public org.w3.ns.wsdl.BindingType addNewBinding()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingType target = null;
            target = (org.w3.ns.wsdl.BindingType)get_store().add_element_user(BINDING$8);
            return target;
        }
    }
    
    /**
     * Removes the ith "binding" element
     */
    public void removeBinding(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BINDING$8, i);
        }
    }
    
    /**
     * Gets array of all "service" elements
     */
    public org.w3.ns.wsdl.ServiceType[] getServiceArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(SERVICE$10, targetList);
            org.w3.ns.wsdl.ServiceType[] result = new org.w3.ns.wsdl.ServiceType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "service" element
     */
    public org.w3.ns.wsdl.ServiceType getServiceArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ServiceType target = null;
            target = (org.w3.ns.wsdl.ServiceType)get_store().find_element_user(SERVICE$10, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "service" element
     */
    public int sizeOfServiceArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SERVICE$10);
        }
    }
    
    /**
     * Sets array of all "service" element
     */
    public void setServiceArray(org.w3.ns.wsdl.ServiceType[] serviceArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(serviceArray, SERVICE$10);
        }
    }
    
    /**
     * Sets ith "service" element
     */
    public void setServiceArray(int i, org.w3.ns.wsdl.ServiceType service)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ServiceType target = null;
            target = (org.w3.ns.wsdl.ServiceType)get_store().find_element_user(SERVICE$10, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(service);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "service" element
     */
    public org.w3.ns.wsdl.ServiceType insertNewService(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ServiceType target = null;
            target = (org.w3.ns.wsdl.ServiceType)get_store().insert_element_user(SERVICE$10, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "service" element
     */
    public org.w3.ns.wsdl.ServiceType addNewService()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ServiceType target = null;
            target = (org.w3.ns.wsdl.ServiceType)get_store().add_element_user(SERVICE$10);
            return target;
        }
    }
    
    /**
     * Removes the ith "service" element
     */
    public void removeService(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SERVICE$10, i);
        }
    }
    
    /**
     * Gets the "targetNamespace" attribute
     */
    public java.lang.String getTargetNamespace()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(TARGETNAMESPACE$12);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "targetNamespace" attribute
     */
    public org.apache.xmlbeans.XmlAnyURI xgetTargetNamespace()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnyURI target = null;
            target = (org.apache.xmlbeans.XmlAnyURI)get_store().find_attribute_user(TARGETNAMESPACE$12);
            return target;
        }
    }
    
    /**
     * Sets the "targetNamespace" attribute
     */
    public void setTargetNamespace(java.lang.String targetNamespace)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(TARGETNAMESPACE$12);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(TARGETNAMESPACE$12);
            }
            target.setStringValue(targetNamespace);
        }
    }
    
    /**
     * Sets (as xml) the "targetNamespace" attribute
     */
    public void xsetTargetNamespace(org.apache.xmlbeans.XmlAnyURI targetNamespace)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnyURI target = null;
            target = (org.apache.xmlbeans.XmlAnyURI)get_store().find_attribute_user(TARGETNAMESPACE$12);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlAnyURI)get_store().add_attribute_user(TARGETNAMESPACE$12);
            }
            target.set(targetNamespace);
        }
    }
}

/*
 * XML Type:  BindingOperationType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.BindingOperationType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML BindingOperationType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class BindingOperationTypeImpl extends org.w3.ns.wsdl.impl.ExtensibleDocumentedTypeImpl implements org.w3.ns.wsdl.BindingOperationType
{
    
    public BindingOperationTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName INPUT$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "input");
    private static final javax.xml.namespace.QName OUTPUT$2 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "output");
    private static final javax.xml.namespace.QName INFAULT$4 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "infault");
    private static final javax.xml.namespace.QName OUTFAULT$6 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "outfault");
    private static final javax.xml.namespace.QName REF$8 = 
        new javax.xml.namespace.QName("", "ref");
    
    
    /**
     * Gets array of all "input" elements
     */
    public org.w3.ns.wsdl.BindingOperationMessageType[] getInputArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INPUT$0, targetList);
            org.w3.ns.wsdl.BindingOperationMessageType[] result = new org.w3.ns.wsdl.BindingOperationMessageType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "input" element
     */
    public org.w3.ns.wsdl.BindingOperationMessageType getInputArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationMessageType target = null;
            target = (org.w3.ns.wsdl.BindingOperationMessageType)get_store().find_element_user(INPUT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "input" element
     */
    public int sizeOfInputArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INPUT$0);
        }
    }
    
    /**
     * Sets array of all "input" element
     */
    public void setInputArray(org.w3.ns.wsdl.BindingOperationMessageType[] inputArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(inputArray, INPUT$0);
        }
    }
    
    /**
     * Sets ith "input" element
     */
    public void setInputArray(int i, org.w3.ns.wsdl.BindingOperationMessageType input)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationMessageType target = null;
            target = (org.w3.ns.wsdl.BindingOperationMessageType)get_store().find_element_user(INPUT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(input);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "input" element
     */
    public org.w3.ns.wsdl.BindingOperationMessageType insertNewInput(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationMessageType target = null;
            target = (org.w3.ns.wsdl.BindingOperationMessageType)get_store().insert_element_user(INPUT$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "input" element
     */
    public org.w3.ns.wsdl.BindingOperationMessageType addNewInput()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationMessageType target = null;
            target = (org.w3.ns.wsdl.BindingOperationMessageType)get_store().add_element_user(INPUT$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "input" element
     */
    public void removeInput(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INPUT$0, i);
        }
    }
    
    /**
     * Gets array of all "output" elements
     */
    public org.w3.ns.wsdl.BindingOperationMessageType[] getOutputArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(OUTPUT$2, targetList);
            org.w3.ns.wsdl.BindingOperationMessageType[] result = new org.w3.ns.wsdl.BindingOperationMessageType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "output" element
     */
    public org.w3.ns.wsdl.BindingOperationMessageType getOutputArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationMessageType target = null;
            target = (org.w3.ns.wsdl.BindingOperationMessageType)get_store().find_element_user(OUTPUT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "output" element
     */
    public int sizeOfOutputArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OUTPUT$2);
        }
    }
    
    /**
     * Sets array of all "output" element
     */
    public void setOutputArray(org.w3.ns.wsdl.BindingOperationMessageType[] outputArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(outputArray, OUTPUT$2);
        }
    }
    
    /**
     * Sets ith "output" element
     */
    public void setOutputArray(int i, org.w3.ns.wsdl.BindingOperationMessageType output)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationMessageType target = null;
            target = (org.w3.ns.wsdl.BindingOperationMessageType)get_store().find_element_user(OUTPUT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(output);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "output" element
     */
    public org.w3.ns.wsdl.BindingOperationMessageType insertNewOutput(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationMessageType target = null;
            target = (org.w3.ns.wsdl.BindingOperationMessageType)get_store().insert_element_user(OUTPUT$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "output" element
     */
    public org.w3.ns.wsdl.BindingOperationMessageType addNewOutput()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationMessageType target = null;
            target = (org.w3.ns.wsdl.BindingOperationMessageType)get_store().add_element_user(OUTPUT$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "output" element
     */
    public void removeOutput(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OUTPUT$2, i);
        }
    }
    
    /**
     * Gets array of all "infault" elements
     */
    public org.w3.ns.wsdl.BindingOperationFaultType[] getInfaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(INFAULT$4, targetList);
            org.w3.ns.wsdl.BindingOperationFaultType[] result = new org.w3.ns.wsdl.BindingOperationFaultType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "infault" element
     */
    public org.w3.ns.wsdl.BindingOperationFaultType getInfaultArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationFaultType target = null;
            target = (org.w3.ns.wsdl.BindingOperationFaultType)get_store().find_element_user(INFAULT$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "infault" element
     */
    public int sizeOfInfaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INFAULT$4);
        }
    }
    
    /**
     * Sets array of all "infault" element
     */
    public void setInfaultArray(org.w3.ns.wsdl.BindingOperationFaultType[] infaultArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(infaultArray, INFAULT$4);
        }
    }
    
    /**
     * Sets ith "infault" element
     */
    public void setInfaultArray(int i, org.w3.ns.wsdl.BindingOperationFaultType infault)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationFaultType target = null;
            target = (org.w3.ns.wsdl.BindingOperationFaultType)get_store().find_element_user(INFAULT$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(infault);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "infault" element
     */
    public org.w3.ns.wsdl.BindingOperationFaultType insertNewInfault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationFaultType target = null;
            target = (org.w3.ns.wsdl.BindingOperationFaultType)get_store().insert_element_user(INFAULT$4, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "infault" element
     */
    public org.w3.ns.wsdl.BindingOperationFaultType addNewInfault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationFaultType target = null;
            target = (org.w3.ns.wsdl.BindingOperationFaultType)get_store().add_element_user(INFAULT$4);
            return target;
        }
    }
    
    /**
     * Removes the ith "infault" element
     */
    public void removeInfault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INFAULT$4, i);
        }
    }
    
    /**
     * Gets array of all "outfault" elements
     */
    public org.w3.ns.wsdl.BindingOperationFaultType[] getOutfaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(OUTFAULT$6, targetList);
            org.w3.ns.wsdl.BindingOperationFaultType[] result = new org.w3.ns.wsdl.BindingOperationFaultType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "outfault" element
     */
    public org.w3.ns.wsdl.BindingOperationFaultType getOutfaultArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationFaultType target = null;
            target = (org.w3.ns.wsdl.BindingOperationFaultType)get_store().find_element_user(OUTFAULT$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "outfault" element
     */
    public int sizeOfOutfaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OUTFAULT$6);
        }
    }
    
    /**
     * Sets array of all "outfault" element
     */
    public void setOutfaultArray(org.w3.ns.wsdl.BindingOperationFaultType[] outfaultArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(outfaultArray, OUTFAULT$6);
        }
    }
    
    /**
     * Sets ith "outfault" element
     */
    public void setOutfaultArray(int i, org.w3.ns.wsdl.BindingOperationFaultType outfault)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationFaultType target = null;
            target = (org.w3.ns.wsdl.BindingOperationFaultType)get_store().find_element_user(OUTFAULT$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(outfault);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "outfault" element
     */
    public org.w3.ns.wsdl.BindingOperationFaultType insertNewOutfault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationFaultType target = null;
            target = (org.w3.ns.wsdl.BindingOperationFaultType)get_store().insert_element_user(OUTFAULT$6, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "outfault" element
     */
    public org.w3.ns.wsdl.BindingOperationFaultType addNewOutfault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationFaultType target = null;
            target = (org.w3.ns.wsdl.BindingOperationFaultType)get_store().add_element_user(OUTFAULT$6);
            return target;
        }
    }
    
    /**
     * Removes the ith "outfault" element
     */
    public void removeOutfault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OUTFAULT$6, i);
        }
    }
    
    /**
     * Gets the "ref" attribute
     */
    public javax.xml.namespace.QName getRef()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(REF$8);
            if (target == null)
            {
                return null;
            }
            return target.getQNameValue();
        }
    }
    
    /**
     * Gets (as xml) the "ref" attribute
     */
    public org.apache.xmlbeans.XmlQName xgetRef()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlQName target = null;
            target = (org.apache.xmlbeans.XmlQName)get_store().find_attribute_user(REF$8);
            return target;
        }
    }
    
    /**
     * Sets the "ref" attribute
     */
    public void setRef(javax.xml.namespace.QName ref)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(REF$8);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(REF$8);
            }
            target.setQNameValue(ref);
        }
    }
    
    /**
     * Sets (as xml) the "ref" attribute
     */
    public void xsetRef(org.apache.xmlbeans.XmlQName ref)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlQName target = null;
            target = (org.apache.xmlbeans.XmlQName)get_store().find_attribute_user(REF$8);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlQName)get_store().add_attribute_user(REF$8);
            }
            target.set(ref);
        }
    }
}

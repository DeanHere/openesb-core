/*
 * An XML document type.
 * Localname: endpoint
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.EndpointDocument
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * A document containing one endpoint(@http://www.w3.org/ns/wsdl) element.
 *
 * This is a complex type.
 */
public class EndpointDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.EndpointDocument
{
    
    public EndpointDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ENDPOINT$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "endpoint");
    
    
    /**
     * Gets the "endpoint" element
     */
    public org.w3.ns.wsdl.EndpointType getEndpoint()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.EndpointType target = null;
            target = (org.w3.ns.wsdl.EndpointType)get_store().find_element_user(ENDPOINT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "endpoint" element
     */
    public void setEndpoint(org.w3.ns.wsdl.EndpointType endpoint)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.EndpointType target = null;
            target = (org.w3.ns.wsdl.EndpointType)get_store().find_element_user(ENDPOINT$0, 0);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.EndpointType)get_store().add_element_user(ENDPOINT$0);
            }
            target.set(endpoint);
        }
    }
    
    /**
     * Appends and returns a new empty "endpoint" element
     */
    public org.w3.ns.wsdl.EndpointType addNewEndpoint()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.EndpointType target = null;
            target = (org.w3.ns.wsdl.EndpointType)get_store().add_element_user(ENDPOINT$0);
            return target;
        }
    }
}

/*
 * XML Type:  MessageRefType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.MessageRefType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML MessageRefType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class MessageRefTypeImpl extends org.w3.ns.wsdl.impl.ExtensibleDocumentedTypeImpl implements org.w3.ns.wsdl.MessageRefType
{
    
    public MessageRefTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MESSAGELABEL$0 = 
        new javax.xml.namespace.QName("", "messageLabel");
    private static final javax.xml.namespace.QName ELEMENT$2 = 
        new javax.xml.namespace.QName("", "element");
    
    
    /**
     * Gets the "messageLabel" attribute
     */
    public java.lang.String getMessageLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(MESSAGELABEL$0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "messageLabel" attribute
     */
    public org.apache.xmlbeans.XmlNCName xgetMessageLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(MESSAGELABEL$0);
            return target;
        }
    }
    
    /**
     * True if has "messageLabel" attribute
     */
    public boolean isSetMessageLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(MESSAGELABEL$0) != null;
        }
    }
    
    /**
     * Sets the "messageLabel" attribute
     */
    public void setMessageLabel(java.lang.String messageLabel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(MESSAGELABEL$0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(MESSAGELABEL$0);
            }
            target.setStringValue(messageLabel);
        }
    }
    
    /**
     * Sets (as xml) the "messageLabel" attribute
     */
    public void xsetMessageLabel(org.apache.xmlbeans.XmlNCName messageLabel)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(MESSAGELABEL$0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlNCName)get_store().add_attribute_user(MESSAGELABEL$0);
            }
            target.set(messageLabel);
        }
    }
    
    /**
     * Unsets the "messageLabel" attribute
     */
    public void unsetMessageLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(MESSAGELABEL$0);
        }
    }
    
    /**
     * Gets the "element" attribute
     */
    public java.lang.Object getElement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ELEMENT$2);
            if (target == null)
            {
                return null;
            }
            return target.getObjectValue();
        }
    }
    
    /**
     * Gets (as xml) the "element" attribute
     */
    public org.w3.ns.wsdl.ElementReferenceType xgetElement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ElementReferenceType target = null;
            target = (org.w3.ns.wsdl.ElementReferenceType)get_store().find_attribute_user(ELEMENT$2);
            return target;
        }
    }
    
    /**
     * True if has "element" attribute
     */
    public boolean isSetElement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(ELEMENT$2) != null;
        }
    }
    
    /**
     * Sets the "element" attribute
     */
    public void setElement(java.lang.Object element)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(ELEMENT$2);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(ELEMENT$2);
            }
            target.setObjectValue(element);
        }
    }
    
    /**
     * Sets (as xml) the "element" attribute
     */
    public void xsetElement(org.w3.ns.wsdl.ElementReferenceType element)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ElementReferenceType target = null;
            target = (org.w3.ns.wsdl.ElementReferenceType)get_store().find_attribute_user(ELEMENT$2);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.ElementReferenceType)get_store().add_attribute_user(ELEMENT$2);
            }
            target.set(element);
        }
    }
    
    /**
     * Unsets the "element" attribute
     */
    public void unsetElement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(ELEMENT$2);
        }
    }
}

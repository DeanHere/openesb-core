/*
 * XML Type:  DocumentationType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.DocumentationType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML DocumentationType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class DocumentationTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.DocumentationType
{
    
    public DocumentationTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}

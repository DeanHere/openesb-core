/*
 * XML Type:  InterfaceType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.InterfaceType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl;


/**
 * An XML InterfaceType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public interface InterfaceType extends org.w3.ns.wsdl.ExtensibleDocumentedType
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(InterfaceType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s2EAECD9BB08C57F25B7B261051DD8E7E").resolveHandle("interfacetypecc24type");
    
    /**
     * Gets array of all "operation" elements
     */
    org.w3.ns.wsdl.InterfaceOperationType[] getOperationArray();
    
    /**
     * Gets ith "operation" element
     */
    org.w3.ns.wsdl.InterfaceOperationType getOperationArray(int i);
    
    /**
     * Returns number of "operation" element
     */
    int sizeOfOperationArray();
    
    /**
     * Sets array of all "operation" element
     */
    void setOperationArray(org.w3.ns.wsdl.InterfaceOperationType[] operationArray);
    
    /**
     * Sets ith "operation" element
     */
    void setOperationArray(int i, org.w3.ns.wsdl.InterfaceOperationType operation);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "operation" element
     */
    org.w3.ns.wsdl.InterfaceOperationType insertNewOperation(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "operation" element
     */
    org.w3.ns.wsdl.InterfaceOperationType addNewOperation();
    
    /**
     * Removes the ith "operation" element
     */
    void removeOperation(int i);
    
    /**
     * Gets array of all "fault" elements
     */
    org.w3.ns.wsdl.InterfaceFaultType[] getFaultArray();
    
    /**
     * Gets ith "fault" element
     */
    org.w3.ns.wsdl.InterfaceFaultType getFaultArray(int i);
    
    /**
     * Returns number of "fault" element
     */
    int sizeOfFaultArray();
    
    /**
     * Sets array of all "fault" element
     */
    void setFaultArray(org.w3.ns.wsdl.InterfaceFaultType[] faultArray);
    
    /**
     * Sets ith "fault" element
     */
    void setFaultArray(int i, org.w3.ns.wsdl.InterfaceFaultType fault);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "fault" element
     */
    org.w3.ns.wsdl.InterfaceFaultType insertNewFault(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "fault" element
     */
    org.w3.ns.wsdl.InterfaceFaultType addNewFault();
    
    /**
     * Removes the ith "fault" element
     */
    void removeFault(int i);
    
    /**
     * Gets the "name" attribute
     */
    java.lang.String getName();
    
    /**
     * Gets (as xml) the "name" attribute
     */
    org.apache.xmlbeans.XmlNCName xgetName();
    
    /**
     * Sets the "name" attribute
     */
    void setName(java.lang.String name);
    
    /**
     * Sets (as xml) the "name" attribute
     */
    void xsetName(org.apache.xmlbeans.XmlNCName name);
    
    /**
     * Gets the "extends" attribute
     */
    java.util.List getExtends();
    
    /**
     * Gets (as xml) the "extends" attribute
     */
    org.w3.ns.wsdl.InterfaceType.Extends xgetExtends();
    
    /**
     * True if has "extends" attribute
     */
    boolean isSetExtends();
    
    /**
     * Sets the "extends" attribute
     */
    void setExtends(java.util.List xextends);
    
    /**
     * Sets (as xml) the "extends" attribute
     */
    void xsetExtends(org.w3.ns.wsdl.InterfaceType.Extends xextends);
    
    /**
     * Unsets the "extends" attribute
     */
    void unsetExtends();
    
    /**
     * Gets the "styleDefault" attribute
     */
    java.util.List getStyleDefault();
    
    /**
     * Gets (as xml) the "styleDefault" attribute
     */
    org.w3.ns.wsdl.InterfaceType.StyleDefault xgetStyleDefault();
    
    /**
     * True if has "styleDefault" attribute
     */
    boolean isSetStyleDefault();
    
    /**
     * Sets the "styleDefault" attribute
     */
    void setStyleDefault(java.util.List styleDefault);
    
    /**
     * Sets (as xml) the "styleDefault" attribute
     */
    void xsetStyleDefault(org.w3.ns.wsdl.InterfaceType.StyleDefault styleDefault);
    
    /**
     * Unsets the "styleDefault" attribute
     */
    void unsetStyleDefault();
    
    /**
     * An XML extends(@).
     *
     * This is a list type whose items are org.apache.xmlbeans.XmlQName.
     */
    public interface Extends extends org.apache.xmlbeans.XmlAnySimpleType
    {
        java.util.List getListValue();
        java.util.List xgetListValue();
        void setListValue(java.util.List list);
        /** @deprecated */
        java.util.List listValue();
        /** @deprecated */
        java.util.List xlistValue();
        /** @deprecated */
        void set(java.util.List list);
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Extends.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s2EAECD9BB08C57F25B7B261051DD8E7E").resolveHandle("extendsffe5attrtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static org.w3.ns.wsdl.InterfaceType.Extends newValue(java.lang.Object obj) {
              return (org.w3.ns.wsdl.InterfaceType.Extends) type.newValue( obj ); }
            
            public static org.w3.ns.wsdl.InterfaceType.Extends newInstance() {
              return (org.w3.ns.wsdl.InterfaceType.Extends) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static org.w3.ns.wsdl.InterfaceType.Extends newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (org.w3.ns.wsdl.InterfaceType.Extends) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML styleDefault(@).
     *
     * This is a list type whose items are org.apache.xmlbeans.XmlAnyURI.
     */
    public interface StyleDefault extends org.apache.xmlbeans.XmlAnySimpleType
    {
        java.util.List getListValue();
        java.util.List xgetListValue();
        void setListValue(java.util.List list);
        /** @deprecated */
        java.util.List listValue();
        /** @deprecated */
        java.util.List xlistValue();
        /** @deprecated */
        void set(java.util.List list);
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(StyleDefault.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s2EAECD9BB08C57F25B7B261051DD8E7E").resolveHandle("styledefault7594attrtype");
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static org.w3.ns.wsdl.InterfaceType.StyleDefault newValue(java.lang.Object obj) {
              return (org.w3.ns.wsdl.InterfaceType.StyleDefault) type.newValue( obj ); }
            
            public static org.w3.ns.wsdl.InterfaceType.StyleDefault newInstance() {
              return (org.w3.ns.wsdl.InterfaceType.StyleDefault) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static org.w3.ns.wsdl.InterfaceType.StyleDefault newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (org.w3.ns.wsdl.InterfaceType.StyleDefault) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.w3.ns.wsdl.InterfaceType newInstance() {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.w3.ns.wsdl.InterfaceType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.w3.ns.wsdl.InterfaceType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.w3.ns.wsdl.InterfaceType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.w3.ns.wsdl.InterfaceType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.w3.ns.wsdl.InterfaceType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.w3.ns.wsdl.InterfaceType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.w3.ns.wsdl.InterfaceType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}

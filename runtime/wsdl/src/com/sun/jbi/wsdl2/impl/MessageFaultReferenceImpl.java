/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageFaultReferenceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import com.sun.jbi.wsdl2.Direction;

import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;

import org.w3.ns.wsdl.MessageRefFaultType;

/**
 * Implementation of WSDL 2.0 Fault Message Reference Component.
 * 
 * @author Sun Microsystems, Inc.
 */
final class MessageFaultReferenceImpl extends MessageFaultReference
{
    /** The container for this component */
    private DescriptionImpl   mContainer;
  
    /**
     * Get the container for this component.
     * 
     * @return The component for this component
     */
    protected DescriptionImpl getContainer()
    {
        return this.mContainer;
    }
  
    /**
     * Get fault component describing the fault referred to by the component.
     *
     * @return Name of the fault component describing the fault referred to by
     * the component
     */
    public QName getRef()
    {
        return getBean().getRef();
    }

    /**
     * Set fault component reference describing the fault referred to by the 
     * component.
     *
     * @param theRef Name of the Fault component describing the fault 
     * referred to by the component
     */
    public void setRef(QName theRef)
    {
        getBean().setRef(theRef);
    }

    /**
     * Construct a message fault reference component implementation from an
     * XML Bean.
     * @param bean The message reference fault XML bean to construct this
     *             component from.
     * @param defs The container for the component.
     */
    MessageFaultReferenceImpl(MessageRefFaultType bean, DescriptionImpl defs)
    {
        super(bean);
        this.mContainer = defs;
    }

    /** Map of WSDL-defined attribute QNames. Keyed by QName.toString value */
    private static java.util.Map sWsdlAttributeQNames = null;

    /** 
     * Worker class method for {@link #getWsdlAttributeNameMap()}.
     * 
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    static synchronized java.util.Map getAttributeNameMap()
    {
        if (sWsdlAttributeQNames == null)
        {
            sWsdlAttributeQNames = XmlBeansUtil.getAttributesMap(
                MessageRefFaultType.type);
        }

        return sWsdlAttributeQNames;    
    }
  
    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed by 
     * canonical QName string (see {@link javax.xml.namespace.QName#toString()}.
     *
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    public java.util.Map getWsdlAttributeNameMap()
    {
        return getAttributeNameMap();
    }
  
    /**
     * Get message exchange pattern role identifier.
     *
     * @return Message exchange pattern role identifier
     */
    public String getMessageLabel()
    {
        return getBean().getMessageLabel();
    }

    /**
     * Set message exchange pattern role identifier.
     *
     * @param theMessageLabel Message exchange pattern role identifier
     */
    public void setMessageLabel(String theMessageLabel)
    {
        if (theMessageLabel != null)
        {
            getBean().setMessageLabel(theMessageLabel);
        }
        else
        {
            getBean().unsetMessageLabel();
        }
    }

    /**
     * Get direction of this message in the exchange.
     *
     * @return Direction of this message in the exchange
     */
    public Direction getDirection()
    {
        XmlCursor     cursor   = getBean().newCursor();
        QName         name     = cursor.getName();

        cursor.dispose();

        return INFAULT.equals(name.getLocalPart()) ? Direction.IN : Direction.OUT;
    }

    /** WSDL element local name for in-faults */
    private static final String INFAULT  = "infault";

    /** WSDL element local name for out-faults */
    private static final String OUTFAULT = "outfault";

    /**
     * Set direction of this message in the exchange.
     *
     * @param theDirection Direction of this message in the exchange
     */
    public void setDirection(Direction theDirection)
    {
        XmlCursor     cursor     = getBean().newCursor();
        QName         name       = cursor.getName();
        QName         newName    = new QName(name.getNamespaceURI(),
            theDirection == Direction.IN ? INFAULT : OUTFAULT);

        cursor.setName(newName);
        cursor.dispose();
    }

    /**
     * A factory class for creating / finding components for given XML beans.
     * <p>
     * This factory guarantees that there will only be one component for each
     * XML bean instance.
     */
    static class Factory
    {
        /**
         * Find the WSDL message fault reference component associated with the given XML
         * bean, creating a new component if necessary.
         * <p>
         * This is thread-safe.<p>
         * 
         * @param bean The XML bean to find the component for.
         * @param defs The container for the component.
         * @return The WSDL message fault reference component for the given 
         *         <code>bean</code> (null if the <code>bean</code> is null).
         */
        static MessageFaultReferenceImpl getInstance(MessageRefFaultType bean, 
            DescriptionImpl defs)
        {
            MessageFaultReferenceImpl   result;

            if (bean != null)
            {
                Map     map = defs.getMessageFaultReferenceMap();
                
                synchronized (map)
                {
                    result = (MessageFaultReferenceImpl) map.get(bean);

                    if (result == null)
                    {
                        result = new MessageFaultReferenceImpl(bean, defs);
                        map.put(bean, result);
                    }
                }
            }
            else
            {
                result = null;
            }

            return result;
        }
    }
}

// End-of-file: MessageFaultReferenceImpl.java

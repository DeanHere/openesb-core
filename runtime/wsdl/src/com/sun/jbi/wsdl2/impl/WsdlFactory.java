/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WsdlFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import com.sun.jbi.wsdl2.WsdlException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.Properties;

/**
 * This class is the default WSDL factory. This supplies a factory for the
 * default versions of the top-level WSDL component and ancillary classes
 * for reading and writing WSDL.
 * <p>
 * This class is public, for the sake of the JBI framework. The framework
 * makes available, through the environment context, the WsdlFactory. Other
 * components, such as the WSDL registry, can then use it. Such client
 * components only use the interfaces exposed in the base service, plus, of
 * course, the ever-popular WsdlException.
 * 
 * @author Sun Microsystems, Inc.
 */
public class WsdlFactory implements com.sun.jbi.wsdl2.WsdlFactory 
{
    /** The property name for the WSDL factory */
    private static final String PROPERTY_NAME =
        Constants.ROOT_PUBLIC_NAME + ".WsdlFactory";

    /** The file name for the WSDL2 properties */
    private static final String PROPERTY_FILE_NAME =
        "jbi.wsdl2.properties";

    /** The default factory class for use by clients */
    private static final String DEFAULT_FACTORY_IMPL_NAME =
        Constants.ROOT_PRIVATE_NAME + ".WsdlFactory";

    /** The full path/name of the property file */
    private static String sFullPropertyFileName = null;

    /**
     * Create a new instance of a WSDL reader object. This can be used to
     * create WSDL definitions components based on XML sources.
     * 
     * @return A WSDL 2.0 reader object.
     */
    public com.sun.jbi.wsdl2.WsdlReader newWsdlReader()
    {
        return new WsdlReader();
    }

    /**
     * Create a new instance of a WSDL writer.
     * 
     * @return A new instance of a WSDL 2.0 writer.
     */
    public com.sun.jbi.wsdl2.WsdlWriter newWsdlWriter()
    {
        return new WsdlWriter();
    }

    /**
     * Create a new instance of a WSDL Description component. This is for
     * programmatic creation of service descriptions.
     * 
     * @param tns Target name space for the new component.
     * @return A new, empty WSDL Description component.
     */
    public com.sun.jbi.wsdl2.Description newDescription(String tns)
    {
        com.sun.jbi.wsdl2.Description result = 
            DescriptionImpl.Factory.newInstance("");

        if (tns != null)
        {
            result.setTargetNamespace(tns);
        }

        return result;
    }

    /**
     * Create a new instance of a WSDL Description component. This is for
     * programmatic creation of service descriptions.
     *
     * @deprecated - replaced by newDescription(String tns)
     * @param tns Target name space for the new component.
     * @return A new, empty WSDL Description component.
     */
    public com.sun.jbi.wsdl2.Definitions newDefinitions(String tns)
    {
        return (com.sun.jbi.wsdl2.Definitions) newDescription(tns);
    }

    /**
     * Get a new instance of a WsdlFactory. This method
     * follows (almost) the same basic sequence of steps that JAXP
     * follows to determine the fully-qualified class name of the
     * class which implements WsdlFactory. The steps (in order)
     * are:
     *<ol>
     *  <li>Check the com.sun.jbi.wsdl2.WsdlFactory system property.</li>
     *  <li>Check the lib/jbi.wsdl2.properties file in the JRE directory. The key
     * will have the same name as the above system property.</li>
     *  <li>Use the default value.</li>
     *</ol>
     * Once an instance of a WsdlFactory is obtained, invoke
     * newDescription(), newWsdlReader(), or newWsdlWriter(), to create
     * the desired instances.
     * 
     * @return An instance of a WsdlFactory.
     * @exception WsdlException if the factory cannot be instantiated.
     */
    public static WsdlFactory newInstance() throws WsdlException
    {
        String factoryImplName = findFactoryImplName();

        return newInstance(factoryImplName);
    }

    /**
     * Get a new instance of a WsdlFactory. This method returns an instance 
     * of the class factoryImplName. Once an instance of a WsdlFactory is 
     * obtained, invoke newWSDLReader() (and other factory methods in future)
     * to create the desired instances.
     *
     * @param factoryImplName The fully-qualified class name of the
     *                        class which provides a concrete implementation 
     *                        of the abstract class WsdlFactory.
     * @return An instance of a WsdlFactory.
     * @exception WsdlException if the factory cannot be instantiated.
     */
    public static WsdlFactory newInstance(String factoryImplName)
        throws WsdlException
    {
        if (factoryImplName != null)
        {
            try
            {
                Class cl = Class.forName(factoryImplName);

                return (WsdlFactory) cl.newInstance();
            }
            catch (Exception ex)
            {
                throw new WsdlException(
                    "Cannot instantiate WsdlFactory implementation\n"
                        + "\tclass name = {0}\n"
                        + "\terror      = {2}: {1}\n",
                    new Object[] 
                    {
                        factoryImplName,
                        ex.getMessage(),
                        ex.getClass().getName(),
                    });
            }
        }
        else
        {
            throw new WsdlException("Unable to find name of WsdlFactory implementation.");
        }
    }

    /**
     * Find the class name of the WSDL factory implementation, using
     * the search rules enumerated in newInstance().
     * 
     * @return Fully qualified name of the WSDL factory to use.
     * @see #newInstance()
     */
    private static String findFactoryImplName()
    {
        String factoryImplName = null;

        // First, check the system property.
        try
        {
            factoryImplName = System.getProperty(PROPERTY_NAME);

            if (factoryImplName != null)
            {
                return factoryImplName;
            }
        }
        catch (SecurityException e)
        {
            //  We aren't allowed to get system properties, so we fall 
            //  back to plan 'B'.
            ;
        }

        // Second, check the properties file.
        String propFileName = getFullPropertyFileName();

        if (propFileName != null)
        {
            try
            {
                Properties properties = new Properties();
                File propFile = new File(propFileName);
                FileInputStream fis = new FileInputStream(propFile);

                properties.load(fis);
                fis.close();

                factoryImplName = properties.getProperty(PROPERTY_NAME);

                if (factoryImplName != null)
                {
                    return factoryImplName;
                }
            }
            catch (IOException e)
            {
                //  It appears that the properties file doesn't exist, 
                //  or is malformed.
                ;
            }
        }

        // Third, return the default.
        return DEFAULT_FACTORY_IMPL_NAME;
    }

    /**
     * Get the full path / name of the property file.
     * 
     * @return The full path/name of the property file; null if security
     *         settings disallow reading system properties.
     */
    private static String getFullPropertyFileName()
    {
        if (sFullPropertyFileName == null)
        {
            try
            {
                String javaHome = System.getProperty("java.home");

                sFullPropertyFileName = javaHome + File.separator + "lib" +
                    File.separator + PROPERTY_FILE_NAME;
            }
            catch (SecurityException e)
            {
                //  Security settings are such that we can't read System 
                //  properties.
                ;
            }
        }

        return sFullPropertyFileName;
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Endpoint.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import org.w3c.dom.DocumentFragment;
import org.w3.ns.wsdl.EndpointType;

/**
 * Abstract implementation of
 * WSDL 2.0 Endpoint component
 *
 * @author Sun Microsystems, Inc.
 */
abstract class Endpoint extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.Endpoint
{
    /**
     * Get the Xml bean for this component.
     *
     * @return Xml Bean for this component.
     */
    EndpointType getBean()
    {
        return (EndpointType) this.mXmlObject;
    }

    /**
     * Construct an abstract endpoint implementation base component.
     * 
     * @param bean      The XML bean for this endpoint component
     */
    Endpoint(EndpointType bean)
    {
        super(bean);
    }

    /**
     * Return this WSDL endpoint as an XML string.
     *
     * @return This endpoint, serialized as an XML string.
     */
    public abstract String toXmlString();

    /**
     * Return this endpoint as a DOM document fragment. The DOM subtree is a
     * copy; altering it will not affect this endpoint.
     *
     * @return This endpoint, as a DOM document fragment.
     */
    public abstract DocumentFragment toXmlDocumentFragment();

}

// End-of-file: Endpoint.java

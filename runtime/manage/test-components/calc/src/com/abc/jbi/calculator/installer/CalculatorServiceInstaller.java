/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CalculatorServiceInstaller.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.abc.jbi.calculator.installer;

import java.util.logging.Logger;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.InstallationContext;
import javax.jbi.management.MBeanNames;

/**
 * The CalculatorServiceInstaller class uses the functionality of the model
 * Component Installer to do all its work.
 */
public class CalculatorServiceInstaller extends com.sun.jbi.management.engine.ModelEngineInstaller implements com.abc.jbi.calculator.installer.CalculatorServiceInstallerConfigMBean
{
    /**
     * Store the InstallationContext
     */
    private InstallationContext mContext;

    private ComponentContext mEnv;

    /** handle to our MBean Namer: */
    protected com.sun.jbi.management.MBeanNames mMBeanNames;

    /**
     * Store the proxy host -- CalculatorServiceInstallerConfigMBean
     */
    private String mProxyHost = "unspecified";

    /**
     * Store the proxy port -- CalculatorServiceInstallerConfigMBean
     */
    private int mProxyPort = -1;

    private Logger mLog;

    /**
     * Initializes the installation environment for a SE. This method is
     * expected to save any information from the installation context that
     * may be needed by other methods.
     * @param installContext is the context containing information from the
     * install command and from the SE jar file.
     * @throws javax.jbi.JBIException when there is an error requiring that the
     * installation be terminated.
     */
    public void init(InstallationContext installContext)
        throws javax.jbi.JBIException
    {
        StandardMBean mbean = null;
        ObjectName mbname = null;
        mContext = installContext;

try {
        //why doesn't this work for model components????
        //mMBeanNames = (com.sun.jbi.management.MBeanNames) mEnv.getMBeanNames();

        //work-around for above:
        //com.sun.jbi.management.support.MBeanNamesImpl mbnamesImpl  = new com.sun.jbi.management.support.MBeanNamesImpl(mEnv.getMBeanNames().getJmxDomainName());
        com.sun.jbi.management.support.MBeanNamesImpl mbnamesImpl = 
                new com.sun.jbi.management.support.MBeanNamesImpl("com.sun.jbi",
                    System.getProperty("com.sun.jbi.instanceName"));
        mMBeanNames = (com.sun.jbi.management.MBeanNames) mbnamesImpl;

        mbname = this.getExtensionMBeanName();
        mEnv = mContext.getContext();

        mLog = Logger.getLogger(this.getClass().getPackage().getName());

        mLog.info("Calculator Installer init() called.");
        mLog.info("my getExtensionMBeanName is : " + mbname);
        mLog.info("Here's where I register the mbean...");
} catch (Exception e) {
    e.printStackTrace();
}

        try
        {
            mbean = new StandardMBean(this,
                com.abc.jbi.calculator.installer.CalculatorServiceInstallerConfigMBean.class);
            mEnv.getMBeanServer().registerMBean(mbean, mbname);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtains the optional installer configuration MBean ObjectName. If none
     * is provided by this SE, returns null.
     * @return ObjectName which represents the MBean registered by the init()
     * method. If none was registered, returns null.
     */
    public ObjectName getExtensionMBeanName()
    {
        ObjectName aname = null;

try {
        aname = mMBeanNames.getCustomEngineMBeanName(
            "InstallerExtensionMBean",
            mContext.getComponentName()
            );
} catch (Exception e) {
        e.printStackTrace();
}

        return aname;
    }

    /**
     * Called at the beginning of installation of a SE to perform any special
     * installation tasks required by the SE.
     * @throws javax.jbi.JBIException when there is an error requiring that the
     * installation be terminated.
     */
    public void onInstall()
        throws javax.jbi.JBIException
    {
        super.onInstall();
        Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
        ObjectName mbname = this.getExtensionMBeanName();

        mLog.info("Calculator Installer onInstall() called.");
        mLog.info("Proxy Host is " + this.getProxyHost());
        mLog.info("Proxy Port is " + this.getProxyPort());
        mLog.info("Here's where I unregister the " + mbname + " MBean...");
        try
        {
            mEnv.getMBeanServer().unregisterMBean(mbname);
            mLog.info("unregistration successful.");
        }
        catch (javax.management.InstanceNotFoundException infe)
        {
            mLog.warning("InstanceNotFoundException: " + infe.getMessage());
            infe.printStackTrace();
        }
        catch (javax.management.MBeanRegistrationException mre)
        {
            mLog.warning("MBeanRegistrationException: " + mre.getMessage());
            mre.printStackTrace();
        }
    }

    /**
     * Called at the beginning of uninstallation of a SE to perform any special
     * installation tasks required by the SE.
     * @throws javax.jbi.JBIException when there is an error requiring that
     * the uninstallation be terminated.
     */
    public void onUninstall()
        throws javax.jbi.JBIException
    {
        super.onUninstall();
        Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
        ObjectName mbname = this.getExtensionMBeanName();

        mLog.info("Calculator Installer onunInstall() called.");
        mLog.info("Proxy Host is " + this.getProxyHost());
        mLog.info("Proxy Port is " + this.getProxyPort());
        mLog.info("Here's where I unregister the " + mbname + " MBean...");
        try
        {
            mEnv.getMBeanServer().unregisterMBean(mbname);
            mLog.info("unregistration successful.");
        }
        catch (javax.management.InstanceNotFoundException infe)
        {
            mLog.warning("InstanceNotFoundException: " + infe.getMessage());
            infe.printStackTrace();
        }
        catch (javax.management.MBeanRegistrationException mre)
        {
            mLog.warning("MBeanRegistrationException: " + mre.getMessage());
            mre.printStackTrace();
        }
    }

    /* methods from CalculatorServiceInstallerConfigMBean */
    /**
     * Return the proxy host.
     *
     * @return - the proxy host.
     */
    public String getProxyHost()
    {
        return mProxyHost;
    }


    /**
     * Return the proxy port.
     *
     * @return - the proxy port.
     */
    public int getProxyPort()
    {
        return mProxyPort;
    }


    /**
     * Specify the proxy host.
     *
     * @param aHost - the proxy host.
     */
    public void setProxyHost(String aHost)
    {
        mProxyHost = aHost;
    }


    /**
     * Specify the proxy port.
     *
     * @param aPort - the proxy port.
     */
    public void setProxyPort(int aPort)
    {
        mProxyPort = aPort;
    }


}

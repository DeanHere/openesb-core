/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RuntimeConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.management.registry.xml.RegistryDocument;
import com.sun.jbi.management.util.StringHelper;

import java.util.Map;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanException;
import javax.management.ReflectionException;

import javax.management.MBeanInfo;
import javax.management.MBeanAttributeInfo;
import javax.management.modelmbean.ModelMBeanInfo;
import javax.management.modelmbean.ModelMBeanAttributeInfo;

/**
 * GlobalConfiguration is a model facade MBean. An instance of this MBean is registered
 * for each configuration category. The RuntimeConfiguration MBean on the DAS uses the
 * GlobalConfiguration MBean to configure the instance. 
 * 
 * @author Sun Microsystems, Inc.
 */
public class GlobalConfiguration 
    extends  com.sun.jbi.management.config.RuntimeConfiguration
{   
    /** Domain literal */
    private static final String DOMAIN = "domain";
        
    /** Creates a new GlobalConfiguration MBean.
     *  @param mbeanInfo metadata returned by the 'real' MBean in the 
     *   target instance.
     */
    public GlobalConfiguration(ModelMBeanInfo mbeanInfo, ConfigurationCategory category)
        throws Exception
    {
        super(mbeanInfo, category, null);
    }
    
    
    /*---------------------------------------------------------------------------------*\
     *                   Dynamic MBean Attribute getter/setter                         *
    \*---------------------------------------------------------------------------------*/

    /**
     * Set the value of a specific attribute of the Dynamic MBean.
     * 
     * @param attribute The identification of the attribute to
     *        be set and  the value it is to be set to.
     * @exception AttributeNotFoundException
     * @exception InvalidAttributeValueException
     * @exception MBeanException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's setter.
     * @exception ReflectionException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown while trying to invoke the MBean's setter.
     * @see #getAttribute
     */
    public void setAttribute(Attribute attribute) 
        throws AttributeNotFoundException, InvalidAttributeValueException, 
            MBeanException, ReflectionException 
    {

            
        // -- Persist global changes to the registry for all instances except "server"
        //    For the server instance the domain facade persists the domain config and 
        //    new attribute override.         
        try
        {
            if (!getEnvironmentContext().getPlatformContext().isAdminServer())
            {
                // If the domain configuration is not persisted, persist it now. 
                persistDomainConfig();
        
                Updater updater = getRegistry().getUpdater();
                String attrValue = ( attribute.getValue() == null ? "null" : attribute.getValue().toString());
                updater.setAttribute(DOMAIN, mCategory, attribute.getName(),  attrValue);
            }
        }
        catch ( com.sun.jbi.management.registry.RegistryException ex)
        {
            
            String errMsg = getTranslator().getString(
                LocalStringKeys.JBI_ADMIN_SET_GLOBAL_CFG_ATTRIB_FAILED,
                attribute.getName(), mCategory.toString(), ex.getMessage());
            javax.jbi.JBIException jbiEx = new javax.jbi.JBIException(errMsg);
            throw new MBeanException(jbiEx, errMsg);
        }
    }
    
    /**
     * Persist this configuration in the registry for the current category.
     */
    public void persist()
        throws Exception
    {
     
        super.persist();
    }
    
     /**
      * This operation is called when a configuration attribute is to use the
      * global configuration, the overriden attribute needs to be deleted from the
      * target configiration 
      *
      * @param attrName - attribute to be deleted
      */
     public void deleteOverride(String attrName)
        throws Exception
     {         
         // Delete the attribute from the registry
         Updater updater = getRegistry().getUpdater();
         updater.deleteAttribute(mCategory, attrName); 
     }

}

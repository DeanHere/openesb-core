/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RegistryDiff.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  RegistryDiff.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *
 */

package com.sun.jbi.management.registry;


import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceAssemblyInfo;
import com.sun.jbi.ServiceAssemblyQuery;
import com.sun.jbi.ServiceAssemblyState;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.management.ConfigurationCategory;

import com.sun.jbi.management.ComponentInfo.Variable;
import com.sun.jbi.management.registry.data.ComponentInfoImpl;
import com.sun.jbi.management.registry.xml.ConfigCategoryType;
import com.sun.jbi.management.registry.xml.PropertyType;
import com.sun.jbi.management.registry.xml.GenericQueryImpl;
import com.sun.jbi.management.registry.data.ServiceAssemblyInfoImpl;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 *  This class performs a logical difference between two open registry instances and
 *  maintains state information about the changes that can be queried afterwards.
 *
 * @author Sun Microsystems, Inc.
 */
public class RegistryDiff
{
    List<String>                        mAddComponents;
    List<String>                        mRemoveComponents;
    List<String>                        mReplaceComponents;
    List<String>                        mUpdateComponents;
    List<String>                        mAddSharedLibraries;
    List<String>                        mRemoveSharedLibraries;
    List<String>                        mReplaceSharedLibraries;
    List<String>                        mAddServiceAssemblies;
    List<String>                        mRemoveServiceAssemblies;
    List<String>                        mReplaceServiceAssemblies;
    List<String>                        mAllTargetComponents;
    List<String>                        mAllMasterComponents;
    List<String>                        mComponentsAffectedByChanges;
    List<String>                        mServiceAssembliesAffectedByChanges;
    List<String>                        mChangedLifecycleComponents;
    List<String>                        mChangedLifecycleServiceAssemblies;
    List<String>                        mChangedConfigComponents;
    Map<String, ComponentInfo>          mMasterSharedLibraries;
    Map<String, ComponentInfo>          mTargetSharedLibraries;
    Map<String, ComponentInfo>          mMasterComponents;
    Map<String, ComponentInfo>          mTargetComponents;
    Map<String, ServiceAssemblyInfo>    mMasterServiceAssemblies;
    Map<String, ServiceAssemblyInfo>    mTargetServiceAssemblies;
    Map<String, HashMap>                mDomainConfigChanges;
    Map<String, HashMap>                mInstanceConfigChanges;
    Map<String, HashMap>                mDomainConfigValues;
    Map<String, Properties>             mComponentProperties;
    Map<String, String[]>               mRemoveComponentAppVars;
    Map<String, Variable[]>             mAddComponentAppVars;
    Map<String, String>                 mRemoveComponentConfig;
    Map<String, Map<String,Properties>> mAddComponentConfig;
    ComponentQuery                      mMasterQuery;
    ComponentQuery                      mTargetQuery;
    ServiceAssemblyQuery                mMasterSAQuery;
    ServiceAssemblyQuery                mTargetSAQuery;
    GenericQueryImpl                    mMasterCQuery;
    GenericQueryImpl                    mTargetCQuery;
    String                              mTarget;
    boolean                            mChanges;
    
    /**
     * Construct a RegistryDiff object using two given registries and the target
     * processing entity of interest.
     *
     * @param master - Registry that is considered to be the reference
     * @param target - Registry that is being upgraded to be consistent with the master
     * @param targetName - The target processing entity (instance, cluster)
     */
    public RegistryDiff(Registry master, Registry target, String targetName)
    {
        mTarget = targetName;
        mAddComponents = new LinkedList();
        mAddSharedLibraries = new LinkedList();
        mAddServiceAssemblies = new LinkedList();
        mRemoveComponents = new LinkedList();
        mRemoveSharedLibraries = new LinkedList();
        mRemoveServiceAssemblies = new LinkedList();
        mReplaceComponents = new LinkedList();
        mReplaceSharedLibraries = new LinkedList();
        mReplaceServiceAssemblies = new LinkedList(); 
        mUpdateComponents = new LinkedList();
        mAllTargetComponents = new LinkedList();
        mAllMasterComponents = new LinkedList();
        mComponentsAffectedByChanges = new LinkedList();
        mServiceAssembliesAffectedByChanges = new LinkedList();
        mChangedLifecycleComponents = new LinkedList();
        mChangedLifecycleServiceAssemblies = new LinkedList();
        mChangedConfigComponents = new LinkedList();
        mMasterComponents = new HashMap();
        mTargetComponents = new HashMap();
        mMasterSharedLibraries = new HashMap();
        mTargetSharedLibraries = new HashMap();
        mMasterServiceAssemblies = new HashMap();
        mTargetServiceAssemblies = new HashMap();
        mDomainConfigChanges = new HashMap();
        mInstanceConfigChanges = new HashMap();
        mDomainConfigValues = new HashMap();
        mComponentProperties = new HashMap();
        mRemoveComponentAppVars = new HashMap();
        mAddComponentAppVars = new HashMap();
        mAddComponentConfig = new HashMap();
        mRemoveComponentConfig = new HashMap();
        mChanges = false;
        try
        {
            mMasterQuery = master.getComponentQuery(targetName);
            mTargetQuery = target.getComponentQuery(targetName);
            mMasterSAQuery = master.getServiceAssemblyQuery(targetName);
            mTargetSAQuery = target.getServiceAssemblyQuery(targetName);
            mMasterCQuery = (GenericQueryImpl)master.getGenericQuery();
            mTargetCQuery = (GenericQueryImpl)target.getGenericQuery();
        }
        catch (RegistryException reX)
        {
            
        }
    }
    
    /**
     * Perform the actual difference computation.
     * @return indication if a difference was detected.
     */
    public boolean computeDiff()
    {
        //
        //  Compute the differences between the master and the current for each of the entities.
        //
        diffSharedLibraries();
        diffComponents();
        diffServiceAssemblies();
        diffConfigs();
        diffComponentProperties();
        diffComponentAppVars();
        diffComponentConfigs();
        
        //
        //  Analyze the differences. Basically tries to limit the work to be performed.
        //
        analyzeSharedLibraries();
        analyzeComponents();
        analyzeServiceAssemblies();
        analyzeConfigs();
        analyzeComponentConfig();
        return (mChanges);
    }
    
    /**
     * Compute differences in the components.
     */
    void diffComponents()
    {
        List<String>    masterIds;
        List<String>    targetIds;
        
        masterIds = mMasterQuery.getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        targetIds = mTargetQuery.getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        mAllMasterComponents = masterIds;
        mAllTargetComponents = targetIds;
        
        for (String component : masterIds)
        {
            if (!targetIds.contains(component))
            {
                mAddComponents.add(component);
                mChanges = true;
            }
            else
            {
                mReplaceComponents.add(component);
            }
        }
        for (String component : targetIds)
        {
            if (!masterIds.contains(component))
            {
                mRemoveComponents.add(component);
                mChanges = true;
            }
        }
   }

    /**
     * Compute differences in the shared libraries
     */
    void diffSharedLibraries()
    {
        List<String>    masterIds;
        List<String>    targetIds;
        
        masterIds = mMasterQuery.getComponentIds(ComponentType.SHARED_LIBRARY);
        targetIds = mTargetQuery.getComponentIds(ComponentType.SHARED_LIBRARY);
        
        for (String component : masterIds)
        {
            if (!targetIds.contains(component))
            {
                mAddSharedLibraries.add(component);
                mChanges = true;
            }
            else
            {
                mReplaceSharedLibraries.add(component);
            }
        }
        for (String component : targetIds)
        {
            if (!masterIds.contains(component))
            {
                mRemoveSharedLibraries.add(component);
                mChanges = true;
            }
        }
    }

    /**
     * Compute differences in the service assemblies.
     */
    void diffServiceAssemblies()
    {
        List<String>    masterIds;
        List<String>    targetIds;
        
        masterIds = mMasterSAQuery.getServiceAssemblies();
        targetIds = mTargetSAQuery.getServiceAssemblies();
        
        for (String component : masterIds)
        {
            if (!targetIds.contains(component))
            {
                mAddServiceAssemblies.add(component);
                mChanges = true;
            }
            else
            {
                mReplaceServiceAssemblies.add(component);
            }
        }
        for (String component : targetIds)
        {
            if (!masterIds.contains(component))
            {
                mRemoveServiceAssemblies.add(component);
                mChanges = true;
            }
        }
   }

    /**
     * Compute differences in the configs
     */
    void diffConfigs()
    {
        getConfigForCategory("domain", ConfigurationCategory.Deployment, mDomainConfigChanges, mDomainConfigValues);
        getConfigForCategory("domain", ConfigurationCategory.Installation, mDomainConfigChanges, mDomainConfigValues);
        getConfigForCategory("domain", ConfigurationCategory.System, mDomainConfigChanges, mDomainConfigValues);
//        getConfigForCategory("domain", ConfigurationCategory.Logger, mDomainConfigChanges, mDomainConfigValues);
        if (mDomainConfigChanges.size() != 0)
        {
            mChanges = true;
        }
        getConfigForCategory(mTarget, ConfigurationCategory.Deployment, mInstanceConfigChanges, null);
        getConfigForCategory(mTarget, ConfigurationCategory.Installation, mInstanceConfigChanges, null);
        getConfigForCategory(mTarget, ConfigurationCategory.System, mInstanceConfigChanges, null);
//        getConfigForCategory(mTarget, ConfigurationCategory.Logger, mInstanceConfigChanges, null);       
        if (mInstanceConfigChanges.size() != 0)
        {
            mChanges = true;
        }
     }

    void diffComponentProperties()
    {
        List<String>    masterIds;
        
        masterIds = mMasterQuery.getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        for (String component : masterIds)
        {
            ComponentInfoImpl       master = (ComponentInfoImpl)mMasterQuery.getComponentInfo(component);
            ComponentInfoImpl       target = (ComponentInfoImpl)mTargetQuery.getComponentInfo(component);
            Properties              mProps = master.getConfiguration();
            Properties              tProps = null;
            Properties              props = new Properties();
            
            if (target != null)
            {
                tProps = target.getConfiguration();
            }
            for (Object mProp : mProps.keySet())
            {
                if (tProps != null && tProps.getProperty((String)mProp) != null)
                {
                    String  mValue = mProps.getProperty((String)mProp);
                    String  tValue = tProps.getProperty((String)mProp);

                    if (mValue != null && tValue != null && mValue.equals(tValue))
                    {
                        continue;
                    }
                }
                props.setProperty((String)mProp, mProps.getProperty((String)mProp));
            }
            if (props.size() != 0)
            {
                mComponentProperties.put(component, props);
                mChanges = true;
            }
        }        
    }
    
    void diffComponentAppVars()
    {
        List<String>    masterIds;
        
        masterIds = mMasterQuery.getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        for (String component : masterIds)
        {
            ComponentInfoImpl   master = (ComponentInfoImpl)mMasterQuery.getComponentInfo(component);
            ComponentInfoImpl   target = (ComponentInfoImpl)mTargetQuery.getComponentInfo(component);
            Variable            mVars[] = master.getVariables();
            Variable            tVars[] = null;
            boolean            same = false;
            boolean            found;
            
            if (target != null)
            {
                tVars = target.getVariables();
                
                //
                // Targets can have there own configuration if the master
                // has no configuration.
                //
                if (mVars.length == 0)
                {
                    continue;
                }

                //
                //  Check if they are the same.
                //
                if (tVars.length == mVars.length)
                {
                    if (tVars.length == 0)
                    {
                        continue;
                    }
                    for (int i = 0; i < tVars.length; i++)
                    {
                       found = false;
                       same = true;
                       for (int j = 0; j < mVars.length; j++)
                        {
                            if (mVars[j].getName() == tVars[i].getName())
                            {
                                found = true;
                                if (mVars[j].getType().equals(tVars[i].getType()) &&
                                    mVars[j].getValue().equals(tVars[i].getValue()))
                                {
                                    continue;
                                }
                                same = false;                                
                                break;
                            }
                        }
                        if (!found)
                        {
                            same = false;
                        }
                        if (!same)
                        {
                            break;
                        }
                    }
                }               
            }
            
            //
            //  Remember target names for delete and values for insert.
            //
            if (!same)
            {
                if (target != null)
                {
                    String[]    vars = new String[tVars.length];
                    for (int i = 0; i < tVars.length; i++)
                    {
                        vars[i] = tVars[i].getName();
                    }
                    mRemoveComponentAppVars.put(component, vars);
                    mChanges = true;
                }
                if (mVars.length > 0)
                {
                    mAddComponentAppVars.put(component, mVars);
                    mChanges = true;
                }
            }
        }
    }
    
    /**
     * Compute the actions needed to sync the config configuration changes. We don't try and
     * compute the changes at the configuration value level. If the configuration is different
     * in any way we just delete the old configuration and apply the new one. This is most likely
     * faster since each configuration operation is a registry commit.
     * meaning that it should be deleted.
     */
    void diffComponentConfigs()
    {
        List<String>    masterIds;
        
        masterIds = mMasterQuery.getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        for (String component : masterIds)
        {
            ComponentInfoImpl   master = (ComponentInfoImpl)mMasterQuery.getComponentInfo(component);
            ComponentInfoImpl   target = (ComponentInfoImpl)mTargetQuery.getComponentInfo(component);
            String              mNames[] = master.getApplicationConfigurationNames();
            HashSet<String>     tNames = new HashSet();
            
            if (target != null)
            {
                String      n[] = target.getApplicationConfigurationNames();

                for (int i = 0; i < n.length; i++)
                {
                    tNames.add(n[i]);
                }
            }
            if (mNames.length > 0)
            {
                HashMap<String, Properties> result = new HashMap();
                
                for (int i = 0; i < mNames.length; i++)
                {
                    Properties          mProps = master.getApplicationConfiguration(mNames[i]);;
       
                    if (tNames.remove(mNames[i]))
                    {
                        Properties          tProps = target.getApplicationConfiguration(mNames[i]);                        
                        
                        if (mProps.size() == tProps.size())
                        {
                            boolean            same = true;
                            
                            for (Object prop : mProps.keySet())
                            {
                                String      mValue = mProps.getProperty((String)prop);
                                String      tValue = tProps.getProperty((String)prop);

                                if (mValue != null && tValue != null && mValue.equals(tValue))
                                {
                                    continue;
                                }
                                same = false;
                                break;
                            }
                            if (same)
                            {
                                continue;
                            }
                        }
                        mRemoveComponentConfig.put(component, mNames[i]);
                    }
                    mChanges = true;
                    result.put(mNames[i], mProps);                    
                }
                if (!result.isEmpty())
                {
                    mAddComponentConfig.put(component, result);       
                }
            }
            //
            // We may get here with tNames being not empty. This is okay, since
            // a local configuration supersedes an empty master configuration.
            //
        }
    }
    
    /**
     * Compute the actions needed to sync the config changes. In this case we
     * create a map with the (name, value) pairs for the change with a value==null
     * meaning that it should be deleted.
     */
    private void getConfigForCategory(String target, ConfigurationCategory cc, Map<String,HashMap> map, Map<String, HashMap> valueMap)
    {
        ConfigCategoryType      mcct = mMasterCQuery.getConfigCategory(target, cc);
        ConfigCategoryType      tcct = mTargetCQuery.getConfigCategory(target, cc);
        List<PropertyType>      mprop = null;
        List<PropertyType>      tprop = null;
        HashMap<String, String> changes = new HashMap();
        HashMap<String, String> values = new HashMap();
        
        if (mcct != null)
        {
            mprop = mcct.getProperty();
        }
        if (tcct != null)
        {
            (tprop = new LinkedList()).addAll(tcct.getProperty());
        }
        if (mprop != null)
        {
            for (PropertyType mp : mprop)
            {
                String              name = mp.getName();
                String              value = mp.getValue();
                PropertyType        match = null;
                
                values.put(name, value);
                if (tprop != null)
                {
                    for (Iterator<PropertyType> i = tprop.iterator(); i.hasNext(); )
                    {
                        PropertyType tp = i.next();
                        if (tp.getName().equals(name))
                        {
                            if (value.equals(tp.getValue()))
                            {
                                i.remove();
                                match = tp;
                            }
                            break;
                        }
                    }
                }
                if (match == null)
                {
                    changes.put(name, value);
                }
            }
        }
        if (tprop != null)
        {
            for (PropertyType tp : tprop)
            {
                changes.put(tp.getName(), null);
            }
        }
        map.put(cc.name(), changes);
        
        //
        //  Save the values by category if requested.
        //
        if (valueMap != null)
        {
            valueMap.put(cc.name(), values);
        }
    }
    
    /**
     * Analyze SharedLibraries to determine if a library has been replaced.
     * Also collect ComponentInfo for future use.
     */
    void analyzeSharedLibraries()
    {
        List<String>        noChange = new LinkedList();

        //
        //  Load ComponentInfo from the appropriate registry.
        //
        for (String component : mAddSharedLibraries)
        {
            ComponentInfo   master = mMasterQuery.getSharedLibraryInfo(component);
            
            mMasterSharedLibraries.put(component, master);
        }
        for (String component : mRemoveSharedLibraries)
        {
            ComponentInfo   target = mTargetQuery.getSharedLibraryInfo(component);
            
            mTargetSharedLibraries.put(component, target);
        }
        for (String component : mReplaceSharedLibraries)
        {
            ComponentInfo   master = mMasterQuery.getSharedLibraryInfo(component);
            ComponentInfo   target = mTargetQuery.getSharedLibraryInfo(component);
            
            //
            //  If the timestamp of the archive is identical there is nothing to do.
            //
            mMasterSharedLibraries.put(component, master);
            mTargetSharedLibraries.put(component, target);
            if (((ComponentInfoImpl)master).getTimestamp() == 
                    ((ComponentInfoImpl)target).getTimestamp())
            {
                noChange.add(component);
                continue;
            }            
            mChanges = true;
        }
        
        //
        //  Make any deferred changes to the replace set.
        //
        mReplaceSharedLibraries.removeAll(noChange);
    }
    
    /**
     * Analyze Components to determine if a component has been replaced.
     * Also collect ComponentInfo for future use.
     */
    void analyzeComponents()
    {
        List<String>        noChange = new LinkedList();
        
        for (String component : mAddComponents)
        {
            ComponentInfo   master = mMasterQuery.getComponentInfo(component);
            
            mMasterComponents.put(component, master);
        }
        for (String component : mRemoveComponents)
        {
            ComponentInfo   target = mTargetQuery.getComponentInfo(component);
            
            mTargetComponents.put(component, target);
        }
        for (String component : mReplaceComponents)
        {
            ComponentInfo   master = mMasterQuery.getComponentInfo(component);
            ComponentInfo   target = mTargetQuery.getComponentInfo(component);
            
            //
            //  If the timestamp of the archive is identical then check for lifecycle changes
            //
            mMasterComponents.put(component, master);
            mTargetComponents.put(component, target);
            if (((ComponentInfoImpl)master).getTimestamp() == 
                    ((ComponentInfoImpl)target).getTimestamp())
            {
                noChange.add(component);
                
                if (((ComponentInfoImpl)master).getUpgradeNumber() != 
                    ((ComponentInfoImpl)target).getUpgradeNumber())
                {
                    mUpdateComponents.add(component);
                    continue;
                }
                
                //
                //  If the lifecycle is the same than we are done. Otherwise note that we need
                //  to correct the lifecycle state of this component.
                //
                if (master.getStatus().equals(target.getStatus()))
                {
                    continue;
                }
                mChangedLifecycleComponents.add(component);
            }           
            mChanges = true;
        }
        
        //
        //  Make any deferred changes to the replace set.
        //
        mReplaceComponents.removeAll(noChange);
        
        //
        //  Compute set of components using replaced shared libraries.
        //
        for (String component : mAllTargetComponents )
        {
            ComponentInfo           comp = mTargetComponents.get(component);
            
            for (String sl : comp.getSharedLibraryNames())
            {
                if (mReplaceSharedLibraries.contains(sl))
                {
                    if (!mComponentsAffectedByChanges.contains(component))
                    {
                        mComponentsAffectedByChanges.add(component);
                    }
                }
            }
        }
    }

    /**
     * Analyze ServiceAssemblies to determine if a service assembly has been replaced.
     * Compute the set of service assemblies that may be effected by components/shared-libraries 
     * that have been replaced. Also collect ServiceAssemblyInfo for future use.
     */
    void analyzeServiceAssemblies()
    {
        List<String>        noChange = new LinkedList();
        
        for (String sa : mAddServiceAssemblies)
        {
            ServiceAssemblyInfo   master = mMasterSAQuery.getServiceAssemblyInfo(sa);
            
            mMasterServiceAssemblies.put(sa, master);
        }
        for (String sa : mRemoveServiceAssemblies)
        {
            ServiceAssemblyInfo   target = mTargetSAQuery.getServiceAssemblyInfo(sa);
            
            mTargetServiceAssemblies.put(sa, target);
        }
        for (String assembly : mReplaceServiceAssemblies)
        {
            ServiceAssemblyInfo   master = mMasterSAQuery.getServiceAssemblyInfo(assembly);
            ServiceAssemblyInfo   target = mTargetSAQuery.getServiceAssemblyInfo(assembly);
            
            //
            //  If the timestamp of the archive is identical there is nothing to do.
            //
            mMasterServiceAssemblies.put(assembly, master);
            mTargetServiceAssemblies.put(assembly, target);
            if (((ServiceAssemblyInfoImpl)master).getTimestamp() == 
                    ((ServiceAssemblyInfoImpl)target).getTimestamp())
            {
                noChange.add(assembly);
                
                //
                //  If the lifecycle is the same than we are done. Otherwise note that we need
                //  to correct the lifecycle state of this service assembly.
                //
                if (master.getStatus().equals(target.getStatus()))
                {
                    continue;
                }
                mChangedLifecycleServiceAssemblies.add(assembly);
            }            
            mChanges = true;
        }
        
        //
        //  Make any deferred changes to the replace set.
        //
        mReplaceServiceAssemblies.removeAll(noChange);      

        //
        //  Compute set of service assemblies referenced by replaced components/shared-libraries.
        //
        List<String>            comps = new LinkedList();
        comps.addAll(mReplaceComponents);
        comps.addAll(mComponentsAffectedByChanges);
        
        for (String compName : comps )
        {
            ComponentInfo           comp = mTargetComponents.get(compName);
            
            for (ServiceUnitInfo sui : comp.getServiceUnitList())
            {
                if (!mServiceAssembliesAffectedByChanges.contains(sui.getServiceAssemblyName()))
                {
                    mServiceAssembliesAffectedByChanges.add(sui.getServiceAssemblyName());
                }
            }
        }

    }
    
    /**
     * Analyze Config changes. The one special case is a delete of a instance value is 
     * replayed as a setting of the instance value to the global value.
     */
    void analyzeConfigs()
    {
         for (Map.Entry<String, HashMap> m : mInstanceConfigChanges.entrySet())
         {
            String              category = m.getKey();
            Map<String,String>  props = m.getValue();
            for (Map.Entry<String, String> p : props.entrySet())
            {
                if (p.getValue() == null)
                {
                    p.setValue((String)mDomainConfigValues.get(category).get(p.getKey()));
                }
            }
         }
    }
    

    /**
     * Compute the components that need to be started to make configuration changes.
     */
    void analyzeComponentConfig()
    {
        for (String comp : mAddComponentAppVars.keySet())
        {
            if (!mChangedConfigComponents.contains(comp))
            {
                mChangedConfigComponents.add(comp);
            }
        }
        for (String comp : mRemoveComponentAppVars.keySet())
        {
            if (!mChangedConfigComponents.contains(comp))
            {
                mChangedConfigComponents.add(comp);
            }
        }
        for (String comp : mAddComponentConfig.keySet())
        {
            if (!mChangedConfigComponents.contains(comp))
            {
                mChangedConfigComponents.add(comp);
            }
        }
        for (String comp : mRemoveComponentConfig.keySet())
        {
            if (!mChangedConfigComponents.contains(comp))
            {
                mChangedConfigComponents.add(comp);
            }
        }
        for (String comp : mComponentProperties.keySet())
        {
            if (!mChangedConfigComponents.contains(comp))
            {
                mChangedConfigComponents.add(comp);
            }
        }
        
    }
    
    /**
     * Compute the set of components that need to be started so that a undeploy service assembly
     * operation can execute.
     * @return List of components name that need to be started.
     */
    public List<String> componentsToStartForUndeploy()
    {
        List<String>        componentsToStart = new LinkedList();
         
        //
        //  Find all old components with service units that reference removed or replaced
        //  service assemblies.
        //
        for (String component : mAllTargetComponents )
        {
            ComponentInfo           comp = mTargetComponents.get(component);
            
            for (ServiceUnitInfo su : comp.getServiceUnitList())
            {
                if (mRemoveServiceAssemblies.contains(su.getServiceAssemblyName()) ||
                    mReplaceServiceAssemblies.contains(su.getServiceAssemblyName()) ||
                    mServiceAssembliesAffectedByChanges.contains(su.getServiceAssemblyName()))
                {
                    componentsToStart.add(component);
                    break;                    
                }
            }
        }
        return (componentsToStart);
    }
        
    /**
     * Compute the set of components that need to be started so that a deploy service assembly
     * operation can execute.
     * @return List of components name that need to be started.
     */
    public List<String> componentsToStartForDeploy()
    {
        List<String>        componentsToStart = new LinkedList();
                
        //
        //  Find all components with service units that reference the new service assemblies.
        //
        for (String component : mAllMasterComponents )
        {
            ComponentInfo           comp = mMasterComponents.get(component);
            
            for (ServiceUnitInfo su : comp.getServiceUnitList())
            {
                if (mAddServiceAssemblies.contains(su.getServiceAssemblyName()))
                {
                    componentsToStart.add(component);
                    break;                    
                }
            }
        }
        
        return (componentsToStart);       
    }
    
    /**
     * Compute the state of a new service assembly (exists in master) from the state of the service units.
     * @param saName - name of the service assembly
     * @return service assembly state
     */
    public ServiceAssemblyState getNewServiceAssemblyState(String saName)
    {
        List<String>            componentsToStart = new LinkedList();
        List<ServiceUnitState>  suStates = new LinkedList();
        
        //
        //  Find all components with service units that reference the new service assemblies.
        //
        for (String component : mAllMasterComponents )
        {
            ComponentInfo           comp = mMasterComponents.get(component);
            
            for (ServiceUnitInfo su : comp.getServiceUnitList())
            {
                if (su.getServiceAssemblyName().equals(saName))
                {
                    suStates.add(su.getState());
                    break;
                }
            }
        }
        
        return (ServiceAssemblyState.computeServiceAssemblyState(suStates));       
    }
    
    /**
     * Get the list of new shared libraries in the master.
     * @return List of shared library names.
     */
    public List<String> getNewSharedLibraries()
    {
        return (mAddSharedLibraries);
    }
    
    /**
     * Get ComponentInfo about a new SharedLibrary
     * @parm slName - name of shared library in master
     * @return ComponentInfo for the given shared library.
     */
    public ComponentInfo getNewSharedLibraryInfo(String slName)
    {
        return (mMasterSharedLibraries.get(slName));
    }
    
    /**
     * Get the list of old shared libraries in the target.
     * @return List of shared library names.
     */
    public List<String> getOldSharedLibraries()
    {
        return (mRemoveSharedLibraries);
    }
    
    /**
     * Get the list of replaced shared libraries in the target.
     * @return List of shared library names.
     */
    public List<String> getReplacedSharedLibraries()
    {
        return (mReplaceSharedLibraries);
    }
    
    /**
     * Get the list of replaced service assemblies in the target.
     * @return List of shared library names.
     */
    public List<String> getReplacedServiceAssemblies()
    {
        return (mReplaceServiceAssemblies);
    }
    
    /**
     * Get the list of new components  in the master.
     * @return List of component names.
     */
    public List<String> getNewComponents()
    {
        return (mAddComponents);
    }
    
    /**
     * Get ComponentInfo about a new Component
     * @parm slName - name of component in master
     * @return ComponentInfo for the given component
     */
    public ComponentInfo getNewComponentInfo(String compName)
    {
        return (mMasterComponents.get(compName));
    }
    
    /**
     * Get the list of old components in the target.
     * @return List of component names.
     */
    public List<String> getOldComponents()
    {
        return (mRemoveComponents);
    }
    
    /**
     * Get the list of components affected by other changes. This typically
     * means a dependent shared libaray has changed.
     * @return List of components names.\
     */
    public List<String> getAffectedComponents()
    {
        return (mComponentsAffectedByChanges);
    }
    
    /**
     * Get the list of updated components
     * @return List of components names.
     */
    public List<String> getUpdatedComponents()
    {
        return (mUpdateComponents);
    }
    
    /**
     * Get the list of replaced components
     * @return List of components names.
     */
    public List<String> getReplacedComponents()
    {
        return (mReplaceComponents);
    }
    
     /**
     * Get the list of components with lifecycle changes. 
     * @return List of components names.\
     */
    public List<String> getChangedLifeCycleComponents()
    {
        return (mChangedLifecycleComponents);
    }
    
    /**
     * Get ServiceAssemblyInfo about a new ServiceAssembly
     * @parm saName - name of service assmebly in master
     * @return ComponentInfo for the given service assembly
     */
    public ServiceAssemblyInfo getNewServiceAssemblyInfo(String saName)
    {
        return (mMasterServiceAssemblies.get(saName));
    }
    
    /**
     * Get the list of new service assemblies in the master.
     * @return List of service assemblies names.
     */
    public List<String> getNewServiceAssemblies()
    {
        return (mAddServiceAssemblies);
    }
    
    /**
     * Get the list of old service assemblies in the target.
     * @return List of service assemblies names.
     */
    public List<String> getOldServiceAssemblies()
    {
        return (mRemoveServiceAssemblies);
    }
    
    /**
     * Get the list of service assemblies affected by other changes. This typically
     * means a dependent component has changed.
     * @return List of service assembly names.
     */
    public List<String> getAffectedServiceAssemblies()
    {
        return (mServiceAssembliesAffectedByChanges);
    }
    
    /**
     * Get the list of service assemblies with lifecycle changes. 
     * @return List of service assembly names.
     */
    public List<String> getChangedLifeCycleServiceAssemblies()
    {
        return (mChangedLifecycleServiceAssemblies);
    }
   
    public Map<String, String> getGlobalConfigChanges(String category)
    {
        return (mDomainConfigChanges.get(category));
    }

    public Map<String, String> getConfigChanges(String category)
    {
        return (mInstanceConfigChanges.get(category));
    }
    
    public List<String> getChangedConfigComponents()
    {
        return (mChangedConfigComponents);
    }
    
    public Map<String, Properties> getComponentPropertyUpdates()
    {
        return (mComponentProperties);
    }
    
    public Map<String, Variable[]> getAddComponentAppVars()
    {
        return (mAddComponentAppVars);
    }
    
    public Map<String, String[]> getRemoveComponentAppVars()
    {
        return (mRemoveComponentAppVars);
    }
    
    public Map<String, Map<String, Properties>> getAddComponentConfigs()
    {
        return (mAddComponentConfig);
    }
    
    public Map<String, String> getRemoveComponentConfigs()
    {
        return (mRemoveComponentConfig);
    }
        
    /**
     * Return a printable status that summarizes the changes and actions.
     * @return String describing changes.
     */
    public String toString()
    {
        StringBuilder       sb = new StringBuilder();
        
        sb.append("Registry Differences for Target: " + mTarget + "\n  Remove Shared Libraries:\n");
        for (String i : mRemoveSharedLibraries)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Add Shared Libraries:\n");
        for (String i : mAddSharedLibraries)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Replace Shared Libraries:\n");
        for (String i : mReplaceSharedLibraries)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Remove Components:\n");
        for (String i : mRemoveComponents)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Add Components:\n");
        for (String i : mAddComponents)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Update Components:\n");
        for (String i : mUpdateComponents)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Affected Components:\n");
        for (String i : mComponentsAffectedByChanges)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Replace Components:\n");
        for (String i : mReplaceComponents)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Component properties:\n");
        for (String i : mComponentProperties.keySet())
        {
            Properties  props = mComponentProperties.get(i);
            
            sb.append("    Component: " + i + "\n");
            for (Object prop : props.keySet())
            {
                sb.append("      Name: " + (String)prop + "  Value: " + props.get(prop) + "\n");                
            }
        }
        sb.append("  Remove component application variables:\n");
        for (String i : mRemoveComponentAppVars.keySet())
        {
            String[]  vars = mRemoveComponentAppVars.get(i);
            sb.append("    Component: " + i + "\n");
            for (int j = 0; j < vars.length; j++)
            {
                sb.append("      Name: " + vars[j] + "\n");
            }
        }
        sb.append("  Add component application variables:\n");
        for (String i : mAddComponentAppVars.keySet())
        {
            Variable[]  vars = mAddComponentAppVars.get(i);
            sb.append("    Component: " + i + "\n");
            for (int j = 0; j < vars.length; j++)
            {
                sb.append("      Name: " + vars[j].getName() + "\n");
                sb.append("        Type: " + vars[j].getType() + "\n");
                sb.append("        Value: " + vars[j].getValue() + "\n");                
            }
        }
        sb.append("  Remove component configuration:\n");
        for (String i : mRemoveComponentConfig.keySet())
        {
            sb.append("    Component: " + i + "  Configuration: " + mRemoveComponentConfig.get(i) + "\n" );
        }
        sb.append("  Add component configuration:\n");
        for (String i : mAddComponentConfig.keySet())
        {
            Map<String, Properties>     config = mAddComponentConfig.get(i);
            sb.append("    Component: " + i + "\n");
            for (String configuration : config.keySet())
            {
                Properties  props;
                       
                sb.append("      Configuration Name: " + configuration + "\n");               
                for (Object prop : (props = config.get(configuration)).keySet())
                {
                    sb.append("        Name: " + (String)prop + "\n          Value: " + props.getProperty((String)prop) + "\n");
                }
            }
        }
        sb.append("  Change Components Lifecycle:\n");
        for (String i : mChangedLifecycleComponents)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Remove Service Assemblies:\n");
        for (String i : mRemoveServiceAssemblies)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Add Service Assemblies:\n");
        for (String i : mAddServiceAssemblies)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Replace Service Assemblies:\n");
        for (String i : mReplaceServiceAssemblies)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Change Service Assemblies Lifecycle:\n");
        for (String i : mChangedLifecycleServiceAssemblies)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Affected Service Assemblies:\n");
        for (String i : mServiceAssembliesAffectedByChanges)
        {
            sb.append("    " + i + "\n");
        }
        sb.append("  Components to Start for Undeploy:\n");
        for (String i : componentsToStartForUndeploy())
        {
            sb.append("    " + i + "\n");
            
        }
        sb.append("  Components to Start for Deploy:\n");
        for (String i : componentsToStartForDeploy())
        {
            sb.append("    " + i + "\n");
            
        }
        sb.append("  Components to Start for Config Changes:\n");
        for (String i : mChangedConfigComponents)
        {
            sb.append("    " + i + "\n");           
        }
        sb.append("  Domain config changes:\n");
        for (Map.Entry<String, HashMap> e : mDomainConfigChanges.entrySet())
        {
            sb.append("    Category: " + e.getKey() + "\n");
            java.util.Set<Map.Entry> x = e.getValue().entrySet(); 
            for (Map.Entry c : x)
            {
                if (c.getValue() == null)
                {
                    sb.append("      Delete Name(" + c.getKey() + ")\n");
                }
                else
                {
                    sb.append("      Change Name(" + c.getKey() + ") Value(" + c.getValue() + ")\n");
                }
            }
        }
        sb.append("  Instance config changes:\n");
        for (Map.Entry<String, HashMap> e : mInstanceConfigChanges.entrySet())
        {
            sb.append("    Category: " + e.getKey() + "\n");
            java.util.Set<Map.Entry> x = e.getValue().entrySet(); 
            for (Map.Entry c : x)
            {
                if (c.getValue() == null)
                {
                    sb.append("      Delete Name(" + c.getKey() + ")\n");
                }
                else
                {
                    sb.append("      Change Name(" + c.getKey() + ") Value(" + c.getValue() + ")\n");
                }
            }
        }
        return (sb.toString());
    }

}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyInfoImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.data;

import java.util.List;
import java.util.ArrayList;
import com.sun.jbi.ServiceAssemblyState;
import com.sun.jbi.ServiceUnitInfo;

/**
 * This interface provides information on Service Assemblys.
 *
 * @author Sun Microsystems, Inc.
 */
public class ServiceAssemblyInfoImpl
    implements com.sun.jbi.ServiceAssemblyInfo
{
    
    private String                mName;
    private String                mDescription;
    private ServiceAssemblyState  mServiceAssemblyState;
    private List<ServiceUnitInfo> mServiceUnitList;
    private long                 mTimestamp;
    
    /**
     * Determine if the supplied Object is equal to this one.
     * @param object - the Object to be compared with this one.
     * @return true if the supplied Object is equal to this one.
     */
    public boolean equals(Object object)
    {
        return ( object.hashCode() == this.hashCode() );
    }
    
    /**
     * Get a hash code for this ServiceAssemblyInfo.
     * @return the hash code for the object.
     */
    public int hashCode()
    {
        return 0;
    }

    /**
     * Get the name of the Service Assembly.
     * @return the name of the Service Assembly.
     */
    public String getName()
    {
        return mName;
    }
    
    public void setName(String name)
    {
        mName = name;
    }
    
    /**
     * Get the timestamp associated with archive.
     * @return the timestamp of the archive.
     */
    public long getTimestamp()
    {
        return (mTimestamp);
    }
    
    public void setTimestamp(long timestamp)
    {
        mTimestamp = timestamp;
    }
    
    public String getDescription()
    {
        return mDescription;
    }
    
    public void setDescription(String descr)
    {
        mDescription = descr;
    }

    /**
     * Get a List<ServiceUnitInfo> of all service units in this service assembly 
     * @return the Service Unit Info List.
     */
    public List<ServiceUnitInfo> getServiceUnitList()
    {
        if ( mServiceUnitList == null )
        {
            mServiceUnitList = new ArrayList();
        }
        return mServiceUnitList;
    }
    
    public void setServiceUnitList(List<ServiceUnitInfo> suList)
    {
        mServiceUnitList = suList;
    }

    /**
     * Get the state of the Service Assembly.
     * @return current state, one of {SHUTDOWN, STOPPED, STARTED, UNKNOWN}
     */
    public ServiceAssemblyState getStatus()
    {
        return mServiceAssemblyState;
    }
    
    public void setStatus(ServiceAssemblyState state)
    {
        mServiceAssemblyState = state;
    }
}

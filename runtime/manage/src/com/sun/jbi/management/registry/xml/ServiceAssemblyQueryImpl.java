/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyQueryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ServiceAssemblyQueryImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 23, 2006, 5:17 PM
 */

package com.sun.jbi.management.registry.xml;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Logger;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.message.MessageHelper;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ServiceAssemblyInfo;
import com.sun.jbi.ServiceAssemblyState;
import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.management.registry.data.ServiceAssemblyInfoImpl;
import com.sun.jbi.management.registry.data.ServiceUnitInfoImpl;
import com.sun.jbi.management.descriptor.ServiceAssemblyDescriptor;

import javax.xml.bind.JAXBException;


/**
 * This class encapsulates queries which are common to 
 * all targets ( domain / server / cluster ).
 *
 * @author Sun Microsystems, Inc.
 */
public class ServiceAssemblyQueryImpl
    implements com.sun.jbi.ServiceAssemblyQuery
{
 
    private Jbi                mJbiRegistry;
    private Registry           mRegistry;
    private boolean            mValidate;
    private ManagementContext  mMgtCtx;
    private Logger             mLogger;
    private StringTranslator   mTranslator;
    private GenericQueryImpl   mGenQuery;
    private ObjectFactory      mObjectFactory;
    private String             mTarget;
    private ComponentQueryImpl mComponentQuery;
    
    private static final String DOMAIN = "domain";
    
    public ServiceAssemblyQueryImpl(Jbi jbi, ManagementContext mgtCtx, boolean validate,
        String targetName, Registry registry)
        throws RegistryException
    {
        mTarget = targetName;
        mJbiRegistry = jbi;
        mRegistry = registry;
        mValidate = validate;
        mMgtCtx = mgtCtx;
        mLogger = mMgtCtx.getLogger();
        mTranslator = 
            mMgtCtx.getEnvironmentContext().getStringTranslator("com.sun.jbi.management");
        
        mGenQuery = (GenericQueryImpl) mRegistry.getGenericQuery();
        mComponentQuery = (ComponentQueryImpl) mRegistry.getComponentQuery(mTarget);
        
        mObjectFactory = new ObjectFactory();
    }

    /*--------------------------------------------------------------------------------*\
     *                          Query Ops                                             *
    \*--------------------------------------------------------------------------------*/
 
    /**
    * Get a list of all deployed service assembly names.
    *
    * @return A List<String> of all deployed service assemblies.
    */
    public List<String> getServiceAssemblies()
        
    {
        List<String> serviceAssemblies = new ArrayList(); 
        try
        {
            if ( mTarget.equals(DOMAIN) )
            {
                serviceAssemblies = mGenQuery.getRegisteredServiceAssemblies();
            }
            else
            {

                InstalledComponentsListType target = null;
                try
                {
                    target = mGenQuery.getInstalledEntities(mTarget);
                }
                catch ( RegistryException rex)
                {
                    throw rex;
                }

                List<ServiceAssemblyRefType> saRefs =  target.getServiceAssemblyRef();
                for( ServiceAssemblyRefType saRef : saRefs )
                {
                    String saName = saRef.getNameRef();
                    serviceAssemblies.add(saName);
                }
            }
        }
        catch ( RegistryException rex )
        {
            mLogger.warning(MessageHelper.getMsgString(rex));
        }
        finally
        {
            return serviceAssemblies;
        }
    }
 
   /**
    * Get the ServiceAssemblyInfo for a particular Service Assembly.
    * @param serviceAssemblyName The unique name of the service assembly being retrieved.
    * @return The ServiceAssemblyInfo for the requested service assembly or null if the
    * service assembly is not registered.
    */
    public ServiceAssemblyInfo getServiceAssemblyInfo(String serviceAssemblyName)
    {
        try
        {
            if ( mGenQuery.isServiceAssemblyRegistered(serviceAssemblyName))
            {
                
                mLogger.finest("Getting Service Assembly " + serviceAssemblyName + " on target " + mTarget);
        
                ServiceAssemblyInfoImpl saInfo = new ServiceAssemblyInfoImpl();
                
                saInfo.setName(serviceAssemblyName);
                com.sun.jbi.management.descriptor.Jbi jbi = mGenQuery.getServiceAssemblyJbi(serviceAssemblyName);
                if (jbi == null)
                {
                    return (saInfo);
                }
                
                ServiceAssemblyDescriptor descr = new ServiceAssemblyDescriptor(jbi);
                
                saInfo.setDescription(descr.getDescription());
                List<ServiceUnitInfo> resultSus = new ArrayList();
                
                if ( mTarget != DOMAIN )
                {
                    List<ServiceUnitInfo> suList = descr.getServiceUnits();
                    List<ServiceUnitState> suStates = new ArrayList();
                    
                    for ( ServiceUnitInfo su : suList)
                    {

                        ComponentInfo compInfo = mComponentQuery.getComponentInfo(su.getTargetComponent());
                        
                        if (compInfo != null)
                        {
                            List<ServiceUnitInfo> compSus = compInfo.getServiceUnitList();
                            for ( ServiceUnitInfo suInfo : compSus )
                            {
                                
                                if ( suInfo.getName().equals(su.getName()) 
                                    && suInfo.getServiceAssemblyName().equals(serviceAssemblyName))
                                {
                                    resultSus.add(suInfo);
                                    suStates.add(suInfo.getState());
                                }
                            }
                        }
                    }
                    
                    if ( !suStates.isEmpty() )
                    {
                        ServiceAssemblyState 
                            saState = ServiceAssemblyState.computeServiceAssemblyState(suStates);
                        saInfo.setStatus(saState);
                    }
                    else
                    {
                        ServiceAssemblyRefType saRef = mGenQuery.getServiceAssembly(serviceAssemblyName, mTarget); 
                        if ( saRef != null && saRef.getState() != null )
                        {
                            // Service assembly deployed with zero service units
                            saInfo.setStatus(ServiceAssemblyState.valueOfDeploymentServiceState(saRef.getState().value())); 
                        } 
                        else
                        {
                            // -- Service assembly is not deployed to the target
                            saInfo.setStatus(ServiceAssemblyState.UNKNOWN);
                        }
                    }
                       
                }
                else
                {
                    // Create a Service Unit list based on information from  the jbi.xml
                    List<ServiceUnitInfo> suList = descr.getServiceUnits();
                    
                    for ( ServiceUnitInfo su : suList )
                    {
                        ServiceUnitInfoImpl suInfo = (ServiceUnitInfoImpl) su;
                        
                        // Set the path to the archive
                        String suRelPath = serviceAssemblyName + java.io.File.separator 
                            + su.getName();;
                        
                        String suArchiveDir = mRegistry.getRepository().
                            findArchiveDirectory(ArchiveType.SERVICE_UNIT, suRelPath);
            
                        String filePath = suArchiveDir + java.io.File.separator 
                            + su.getTargetComponent();
                        suInfo.setFilePath(filePath);
                        resultSus.add(suInfo);
                    }
                    
                    List<ServiceAssemblyState> 
                        saStates = getAllServiceAssemblyStates(serviceAssemblyName);
                    saInfo.setStatus(ServiceAssemblyState.getEffectiveServiceAssemblyState(saStates));
                }
                saInfo.setServiceUnitList(resultSus);
                return saInfo;
            }
            else
            {
                mLogger.finest("Service Assembly " + serviceAssemblyName + " not registered in domain");
            }
            
        }
        catch ( Exception rex )
        {
            rex.printStackTrace();
            mLogger.severe(rex.getMessage());
        }
        return null;
    }

   /**
    * Get the current status of a service assembly.
    *
    * @param serviceAssemblyName The unique service assembly name.
    * @return The current status of the service assembly: ServiceAssemblyState 
    *         {SHUTDOWN, STOPPED, STARTED, UNKNOWN}
    * @throws javax.jbi.JBIException if the service assembly is not registered.
    */
    public com.sun.jbi.ServiceAssemblyState getStatus(String serviceAssemblyName)
        throws javax.jbi.JBIException
    {
        return getServiceAssemblyInfo(serviceAssemblyName).getStatus();
    }

    
    /**
     * @return a List conating the state of the service assembly on each server/
     * custer in the domain.
     * @param saName - service assembly name
     */
    private List<ServiceAssemblyState> getAllServiceAssemblyStates(String saName)
        throws RegistryException
    {
        List<String> servers  = mGenQuery.getServers();
        List<String> clusters = mGenQuery.getClusters();
        List<ServiceAssemblyState> saStates = new ArrayList();
        
        for ( String server : servers )
        {
            ServiceAssemblyState saState = getServiceAssemblyStateOnTarget(saName, server);
            
            if ( !(ServiceAssemblyState.UNKNOWN == saState) )
            {
                saStates.add(saState);
            }
        }
        
        for ( String cluster : clusters )
        {
         
            ServiceAssemblyState saState = getServiceAssemblyStateOnTarget(saName, cluster);
            
            if ( !(ServiceAssemblyState.UNKNOWN == saState) )
            {
                saStates.add(saState);
            }   
        }
        
        return saStates;
    }
    
    /**
     * Query the state of the service assembly on a server / cluster.
     *
     * @return the service assembly state.
     */
    private ServiceAssemblyState 
    getServiceAssemblyStateOnTarget(String saName, String target) 
        throws RegistryException
    {
        com.sun.jbi.ServiceAssemblyQuery
            saQuery = mRegistry.getServiceAssemblyQuery(target);

        ServiceAssemblyInfo saInfo = saQuery.getServiceAssemblyInfo(saName);

        if ( saInfo != null )
        {
            return saInfo.getStatus();
        }
        
        return ServiceAssemblyState.UNKNOWN;
    }
}

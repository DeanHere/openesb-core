/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ReentrantReadWriteLock.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

import java.util.ArrayList;

/**
 * This is a implementation of a tailored subset of java.util.concurrent.locks.ReentrantReadWriteLock for
 * pre J2SE 5.0 systems. The implemented class/interfaces/methods are compatible at compile time.
 *
 * @author Sun Microsystems, Inc
 */
public class ReentrantReadWriteLock
{
    private Object      mSync;
    private ReadLock    mReadLock;
    private WriteLock   mWriteLock;
    private ArrayList   mReaders;
    private int         mReadCount;
    private Thread      mWriter;
    private int        mWriterCount;
    private ArrayList   mWaitingReaders;
    private ArrayList   mWaitingWriters;
    private int        mState;
    static final int    NONE = 0;
    static final int    READ = 1;
    static final int    WRITE = 2;
    
    /** Creates a new instance of RentrantReadWriteLock */
    public ReentrantReadWriteLock()
    {
        mWriteLock = new WriteLock();
        mReadLock = new ReadLock();
        mReaders = new ArrayList();
        mWaitingReaders = new ArrayList();
        mWaitingWriters = new ArrayList();
        mWriter = null;
        mState = NONE;
        mWriterCount = 0;
        mReadCount = 0;
        mSync = this;
    }
    
    public ReadLock readLock()
    {
        return (mReadLock);
    }
    
    public WriteLock writeLock()
    {
        return (mWriteLock);
    }
    
    public boolean isWriteLockedByCurrentThread()
    {
        return (mWriter == Thread.currentThread());
    }
    
    synchronized int getReaders()
    {
        return (mReaders.size());
    }
    
    synchronized int getWaitingReaders()
    {
        return (mWaitingReaders.size());
    }
    
    synchronized int getWriters()
    {
        return (mWriterCount);
    }
    
    synchronized int getWaitingWriters()
    {
        return (mWaitingWriters.size());
    }
    
    synchronized int getState()
    {
        return (mState);
    }
    
    public interface Lock
    {
        public void lock();
        public void unlock();
    }
    
    public class ReadLock implements Lock
    {
        
        public boolean tryLock(long interval, TimeUnit unit)
            throws java.lang.InterruptedException
        {
            Thread      t = Thread.currentThread();
            
            synchronized (mSync)
            {
                long       endTime = 0;
                
                while (!doLock(t))
                {
                    try
                    {
                        if (endTime == 0)
                        {
                            endTime = System.currentTimeMillis() + unit.getMilliseconds() * interval;
                        }
                        
                        mSync.wait(unit.getMilliseconds() * interval);
                    }
                    finally
                    {
                        mWaitingReaders.remove(t);
                    }
                    if (System.currentTimeMillis() > endTime)
                    {
                        return (false);
                    }
                }                    
            }
            return (true);
        }
        
        public void lock()
        {
            Thread      t = Thread.currentThread();
            
            synchronized (mSync)
            {                
                while (!doLock(t))
                {
                    try
                    {
                        try
                        {
                            mSync.wait();
                        }
                        catch (java.lang.InterruptedException ie)
                        {                            
                        }
                    }
                    finally
                    {
                        mWaitingReaders.remove(t);
                    }
                }                    
            }
        }
                
        private boolean doLock(Thread t)
        {
            if (mState == NONE)
            {
                mReaders.add(t);
                mState = READ;
                mReadCount++;
                return (true);
            }
            else if (mState == READ)
            {
                if (mWaitingWriters.isEmpty() || mReaders.contains(t) || mReadCount < 4)
                {
                    mReaders.add(t);
                    return (true);
                }
            }
            else
            {
                if (mWriter == t)
                {
                    mReaders.add(t);
                    return (true);
                }
            }
            mWaitingReaders.add(t);
            return (false);
        }
        
        
        public void unlock()
        {
            Thread          t = Thread.currentThread();
            
            synchronized (mSync)
            {
                if (mState == NONE || !mReaders.remove(t))
                {
                    throw new RuntimeException("Lock not held");                    
                }
                if (mState == READ)
                {
                    if (mReaders.isEmpty())
                    {
                        mState = NONE;
                        mSync.notifyAll();
                    }
                }
            }
        }
    }
    
    public class WriteLock implements Lock
    {
        public boolean tryLock(long interval, TimeUnit unit)
            throws java.lang.InterruptedException
        {
            Thread      t = Thread.currentThread();
            
            synchronized (mSync)
            {
                long       endTime = 0;
                
                while (!doLock(t))
                {
                    try
                    {
                        if (endTime == 0)
                        {
                            endTime = System.currentTimeMillis() + unit.getMilliseconds() * interval;
                        }
                        
                        mSync.wait(unit.getMilliseconds() * interval);
                    }
                    finally
                    {
                        mWaitingWriters.remove(t);
                    }
                    if (System.currentTimeMillis() > endTime)
                    {
                        return (false);
                    }
                }                    
            }
            return (true);
        }

        public void lock()
        {
            Thread      t = Thread.currentThread();
        
            synchronized (mSync)
            {
                while (!doLock(t))
                {
                    try
                    {
                        try
                        {
                            mSync.wait();
                        }
                        catch (java.lang.InterruptedException iE)
                        {
                        }
                    }
                    finally
                    {
                        mWaitingWriters.remove(t);
                    }
                }
            }
        }
        
        private boolean doLock(Thread t)
        {
            if (mState == NONE)
            {
                mWriter = t;
                mWriterCount++;
                mState = WRITE;
                mReadCount = 0;
                return (true);
            }
            else
            {
                if (mState == WRITE && mWriter == t)
                {
                    mWriterCount++;
                    return (true);
                }
                mWaitingWriters.add(t);
            }
            return (false);
        }
        
        public void unlock()
        {
            Thread          t = Thread.currentThread();
            boolean        wakeReaders = false;
            
            synchronized (mSync)
            {
                if (mState == WRITE && mWriter == t)
                {
                    if (--mWriterCount > 0)
                    {
                        return;
                    }
                    mWriter = null;
                    if (mReaders.contains(t))
                    {
                        mState = READ;
                    }
                    else
                    {
                        mState = NONE;
                    }
                    mSync.notifyAll();
                }
                else
                {
                    throw new RuntimeException("Lock not held");
                }
            }
        }
    }

}

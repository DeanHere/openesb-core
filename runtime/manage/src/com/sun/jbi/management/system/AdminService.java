/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import com.sun.jbi.VersionInfo;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.support.JbiNameInfo;
import com.sun.jbi.management.message.MessageHelper;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jbi.JBIException;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;

/**
 * AdminServiceMBean defines the Sun Extentions to AdminServiceMBean.
 *
 * The AdminService is responsible for bootstrapping the
 * JBI Framework management layer for all other system services,
 * starting and stoping the system, and providing information
 * that remote clients can use to access other JBI Framework
 * system services and installed components.
 *
 * @author Sun Microsystems, Inc.
 */
public class AdminService extends ModelSystemService implements AdminServiceMBean, NotificationListener
{

    /**
     * milliseconds between heartbeats
     */
    private long mHeartBeatInterval;

    /**
     * The heart beat interval notification id.
     */
    private Integer mNotificationId;
    
    /**
     * instance of the AutoAdminTask class
     */
    private static AutoAdminTask sAutoAdminTask = null;

    /** JMX object names for all JBI system "service" lifecycle MBeans: */
    private ObjectName mAdminServiceLifeCycleMBeanName;
    private ObjectName mConfigServiceLifeCycleMBeanName;
    private ObjectName mDeployServiceLifeCycleMBeanName;
    private ObjectName mInstallServiceLifeCycleMBeanName;
    private ObjectName mLoggingServiceLifeCycleMBeanName;
    private ObjectName mMessageServiceLifeCycleMBeanName;

    /** our immutable name: */
    private final JbiNameInfo mJbiNameInfo =
            new JbiNameInfo("AdminService");

    /**
     * Management context
     */
    private ManagementContext mContext = null;

    /**
     * Management Message Object
     */
    private BuildManagementMessageImpl mMMImpl = null;
    
    /**
     * The System Bootstrap instance.
     */
    private SystemBootstrap mEsbSys = null;

    /**
     * Constructs a AdminService.
     * @param anEnv is the ManagementContext.
     */
    public AdminService(ManagementContext anEnv)
    {
        mContext = anEnv;

        /*
         * Local initialization of this service.
         * Local routine is responsible for calling super.initModelSystemService(..).
         */
        initModelSystemService(anEnv.getEnvironmentContext());

        mMMImpl = mContext.getManagementMessageObject();
    }

    /** local model init - called by constructor - create custom mbeans. */
    protected void initModelSystemService(com.sun.jbi.EnvironmentContext anEnv)
    {
        String loggerName = 
            com.sun.jbi.management.config.LoggerConfigurationFactory.ADMIN_LOGGER;
       
        Logger logger = Logger.getLogger(loggerName);
        
        //initialize the super.
        super.initModelSystemService(anEnv, logger, mJbiNameInfo);

        //initialize system service lifecycle mbean names,
        //needed by global startup/shutdown ops:
        initSystemServiceLifeCycleMBeanNames();
        
        //add AdminService MBean to START/STOP mbean set:
        mStartMBeans.add(mAdminServiceMBeanName, com.sun.jbi.management.system.AdminServiceMBean.class, this);
        
        // get the configured heartbeat interval 
        Object interval = getConfigurationAttribute(ConfigurationCategory.System, 
            com.sun.jbi.management.config.SystemConfigurationFactory.HEART_BEAT_INTERVAL);
        
        mHeartBeatInterval = ( interval == null ? 5500 : ((Integer) interval).intValue() );

        mLogger.fine("JBI Admin Service heartbeat interval is " + mHeartBeatInterval + " ms.");
    }

    /**
     * override model start() method. 
     * @throws JBIException never
     */
    public synchronized void start() throws JBIException
    {
        String statusMsg = mTranslator.getString(
            LocalStringKeys.AS_START_SERVICE,
            "AdminService.start()",
            "JBI Framework");
        mLogger.fine(statusMsg);
        
        // Auto admin tasks are only supported on the DAS
        if (mEnv.getPlatformContext().isAdminServer())
        {
            startAutoTasks();
        }
   
        //register mbean start set:
        super.start();  
        
    }

    /**
     * override model stop() method. 
     * @throws JBIException never
     */
    public synchronized void stop() throws JBIException
    {
        if (sAutoAdminTask != null)
        {
            stopAutoTasks();
        }

        //unregister mbean start set:
        super.stop();        
    }

    /* methods inherited from NotificationListener */

    /**
     * Handle timer notifications.
     * @param aNotification is the received (timer) notification
     * @param anObject is the notification object (ignored)
     */
    public void handleNotification(Notification aNotification, Object anObject)
    {
        heartBeat();
        
        // Switch to a new heart beat if it has changed
        checkHeartBeat();
    }

/////////
//JSR 208 AdminServiceMBean Implementation:
/////////

    /**
     * Lookup a system service {@link LifeCycleMBean} by name. System services 
     * are implementation-defined services which can administered through JMX,
     * and have a life cycle.
     * <p>
     * System services are not related to service engines.
     * 
     * @param serviceName name of the system service; must be non-null and non-
     *        empty; values are implementation-dependent
     * @return JMX object name of the system service's LifeCycleMBean, or 
     *         <code>null</code> if there is no system service with the given
     *         <code>name</code>.
     */
    public ObjectName getSystemService(String serviceName)
    {
        MBeanNames mbn = mContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        //see if the request is for a management system service:
        String controltype = managementServiceControl(serviceName);

        //for non-management system services (NMS, etc), lookup the lifecycle mbean:
        if (controltype == null)
        {
            controltype = mbn.CONTROL_TYPE_LIFECYCLE;
        }

        tmp += ":" + mbn.SERVICE_NAME_KEY + "=" + serviceName;
        tmp += "," + mbn.CONTROL_TYPE_KEY + "=" + controltype;
        tmp += "," + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_SYSTEM;
        tmp += "," + mbn.INSTANCE_NAME_KEY + "=" + mbn.getJbiInstanceName();
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query:
        ObjectName[] names = queryHelper(tmp);

        if (names.length >= 1)
        {
            if (names.length > 1)
            {
                String statusMsg = mTranslator.getString(
                    LocalStringKeys.AS_GETSYSTEMSERVICE_TOO_MANY_MBEANS,
                    tmp);

                mLogger.severe(statusMsg);
            }

            //always return the first mbean name, even if many:
            return names[0];
        }
        else
        {
                String statusMsg = mTranslator.getString(
                    LocalStringKeys.AS_GETSYSTEMSERVICE_LOOKUP_FAILED,
                    tmp);

                mLogger.warning(statusMsg);
        }

        return null;
    }

    /**
     * Looks up all JBI system services {@link LifeCycleMBean}'s currently 
     * installed. System services are implementation-defined services which can 
     * administered through JMX. System services are not related to service 
     * engines.
     * 
     * @return array of LifecycleMBean JMX object names of system services 
     *         currently installed in the JBI implementation; must be non-null;
     *         may be empty
     */
    public ObjectName[] getSystemServices()
    {
        MBeanNames mbn = mContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_SYSTEM;
        tmp += "," + mbn.INSTANCE_NAME_KEY + "=" + mbn.getJbiInstanceName();
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query:

        ObjectName[] names = queryHelper(tmp);

        if (names.length >= 1)
        {
            ArrayList namelist = new ArrayList();
            String ctype;

            //filter out the "standard" controls:
            for (int ii = 0; ii < names.length; ii++)
            {
                ctype  = names[ii].getKeyProperty(mbn.CONTROL_TYPE_KEY);
                if (ctype != null && ctype.endsWith("Service"))
                {
                    namelist.add(names[ii]);
                }
            }

            if (namelist.size() > 0)
            {
                ObjectName[] newnames = new ObjectName[namelist.size()];
                Iterator itr; int ii;
                for (itr = namelist.iterator(), ii = 0; itr.hasNext(); ii++)
                {
                    newnames[ii] = (ObjectName) itr.next();
                }

                return newnames;
            }
            else
            {
                String statusMsg = mTranslator.getString(
                    LocalStringKeys.AS_GETSYSTEMSERVICE_NO_SERVICES);
                mLogger.severe(statusMsg);
            }
        }
        else
        {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.AS_GETSYSTEMSERVICE_LOOKUP_FAILED,
                tmp);
            mLogger.warning(statusMsg);
        }

        return (new ObjectName[0]);
    }

    /**
     * Find the {@link ComponentLifeCycleMBean} of a JBI Installable Component 
     * by its unique name.
     * 
     * @param name the name of the engine or binding component; must be non-
     *        null and non-empty
     * @return the JMX object name of the component's life cycle MBean, or 
     *         <code>null</code> if there is no such component with the given 
     *         <code>name</code>
     */
    public ObjectName getComponentByName(String name)
    {
        /*
         * EXAMPLE:
com.sun.jbi:ComponentName=ABC,ControlType=Lifecycle,ComponentType=Installed,*
         */

        com.sun.jbi.util.EnvironmentAccess.getContext().isFrameworkReady(true);
        
        MBeanNames mbn = mContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + mbn.COMPONENT_ID_KEY + "=" + name;
        tmp += "," + mbn.CONTROL_TYPE_KEY + "=" + mbn.CONTROL_TYPE_LIFECYCLE;
        tmp += "," + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_INSTALLED;
        tmp += "," + mbn.INSTANCE_NAME_KEY + "=" + mbn.getJbiInstanceName();
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query:
        ObjectName[] names = queryHelper(tmp);

        if (names.length >= 1)
        {
            if (names.length > 1)
            {
                String statusMsg = mTranslator.getString(
                    LocalStringKeys.AS_GETCOMPONENTBYID_TOO_MANY_MBEANS,
                    tmp);
                mLogger.severe(statusMsg);
            }

            //always return the first mbean name, even if many:
            return names[0];
        }
        else
        {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.AS_GETCOMPONENTBYID_LOOKUP_FAILED,
                tmp);
            mLogger.warning(statusMsg);
        }

        return null;
    }

    /**
     * Get a list of {@link ComponentLifeCycleMBean}s for all binding components 
     * currently installed in the JBI system.
     * 
     * @return array of JMX object names of component life cycle MBeans for all 
     *         installed binding components; must be non-null; may be empty
     */
    public ObjectName[] getBindingComponents()
    {
        MBeanNames mbn = mContext.getMBeanNames();
        return getInstalledComponents(mbn.INSTALLED_TYPE_BINDING);
    }

    /**
     * Get a list of {@link ComponentLifeCycleMBean}s for all service engines 
     * currently installed in the JBI system.
     * 
     * @return array of JMX object names of component life cycle MBeans for all 
     *         installed service engines; must be non-null; may be empty
     */
    public ObjectName[] getEngineComponents()
    {
        MBeanNames mbn = mContext.getMBeanNames();
        return getInstalledComponents(mbn.INSTALLED_TYPE_ENGINE); 
    }

    /**
     * Check if a given JBI component is a Binding Component.
     * 
     * @param componentName the unique name of the component; must be non-null
     *        and non-empty
     * @return <code>true</code> if the component is a binding component; 
     *         <code>false</code> if the component is a service engine or if
     *         there is no component with the given <code>componentName</code>
     *         installed in the JBI system
     */
    public boolean isBinding(String componentName)
    {
        MBeanNames mbn = mContext.getMBeanNames();
        ObjectName compObj = this.getComponentByName(componentName);
        if (null == compObj)
        {
            return false;
        }
        String compType = compObj.getKeyProperty(mbn.INSTALLED_TYPE_KEY);
        return (mbn.INSTALLED_TYPE_BINDING.equals(compType));
    }

    /**
     * Check if a given JBI component is a Service Engine.
     * 
     * @param componentName the unique name of the component; must be non-null
     *        and non-empty
     * @return <code>true</code> if the component is a service engine;
     *         <code>false</code> if the component is a binding component, or if
     *         there is no component with the given <code>componentName</code>
     *         installed in the JBI system
     */
    public boolean isEngine(String componentName)
    {
        MBeanNames mbn = mContext.getMBeanNames();
        ObjectName compObj = this.getComponentByName(componentName);
        if (null == compObj)
        {
            return false;
        }
        String compType = compObj.getKeyProperty(mbn.INSTALLED_TYPE_KEY);
        return (mbn.INSTALLED_TYPE_ENGINE.equals(compType));
    }

    /**
     * Return current version and other info about this JBI implementation. The 
     * contents of the returned string are implementation dependent.
     * 
     * @return information string about the JBI implementation, including 
     *         version information; must be non-null and non-empty
     */
    public String getSystemInfo()
    {
        VersionInfo vers = mContext.getVersionInfo();

        return (
            vers.fullProductName()
            + "\n"
            + vers.majorVersion() +
                "." + vers.minorVersion() +
                "(" + vers.buildNumber() + ")"
            + "\n"
            + vers.copyright()
            + "\n"
        );
    }


/////
//SUN extensions to JRS  208 AdminServiceMBean:
/////

    /**
     * Get the DeployerMBean for the <CODE>componentName</CODE> associated with
     * <code>artifactUrl</code>.
     *
     * @return JMX object name of Component's DeployerMBean or null
     */
    public ObjectName[] getDeployerMBeanNames(String componentName)
    {
        MBeanNames mbn = mContext.getMBeanNames();

        String qhdr = mbn.getJmxDomainName();
        qhdr += ":" + mbn.COMPONENT_ID_KEY + "=" + componentName;
        qhdr += "," + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_INSTALLED;
        qhdr += "," + mbn.CONTROL_TYPE_KEY + "=" + mbn.CONTROL_TYPE_DEPLOYER;
        qhdr += "," + mbn.INSTANCE_NAME_KEY + "=" + mbn.getJbiInstanceName();
        //wildcard goes at the end:
        qhdr += ",*";
                
        //exec the query and return the result:
        return queryHelper(qhdr);
    }

    /**
     * Return the name of this JBI Framework runtime
     *
     * @return the instance name of this runtime.
     */
    public String getJbiInstanceName()
    {
        MBeanNames mbn = mContext.getMBeanNames();
        return mbn.getJbiInstanceName();
    }

    /**
     * Lookup all active MBeans associated with <CODE>componentName</CODE>.
     * @param componentName - is the name of the BC or SE.
     * @return the array of JMX object names for the component.
     */
    public ObjectName[] getComponentMBeans(String componentName)
    {
        MBeanNames mbn = mContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + mbn.COMPONENT_ID_KEY + "=" + componentName;
        tmp += "," + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_INSTALLED;
        tmp += "," + mbn.INSTANCE_NAME_KEY + "=" + mbn.getJbiInstanceName();
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query and return the result:
        return queryHelper(tmp);
    }

    /**
     * Return URL that can be used to upload and deploy a remote  
     * Service Assembly.
     * @return URL String
     */
    public String getRemoteFileUploadURL() throws Exception
    {
        String uploadURL = null;
        String aHttpServerIpAddr = null;
        String aHttpServerPort = null;
        ObjectName HTTPListenerConfig = null;
        
        try
        {
            aHttpServerIpAddr = InetAddress.getLocalHost().getHostAddress();
        
            HTTPListenerConfig = new ObjectName (
                    "amx:j2eeType=X-HTTPListenerConfig"
                    + ",X-ConfigConfig=server-config"
                    + ",name=http-listener-1"
                    + ",X-HTTPServiceConfig=na"
                );

            aHttpServerPort = mContext.getMBeanServer()
                                .getAttribute(HTTPListenerConfig, "Port").toString();

            uploadURL = "http://" + aHttpServerIpAddr + ":" 
                + aHttpServerPort + "/JBIFileUpload/UploadServlet";
        }
        catch(Exception e)
        {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.AS_GETREMOVEFILEUPLOADURL_EXCEP,
                e.toString());
            mLogger.severe(statusMsg);
            
            ManagementMessageHolder mmHolder = new ManagementMessageHolder("EXCEPTION_MSG");
            mmHolder.setTaskName ("getRemoteFileUploadURL");
            mmHolder.setExceptionObject (e);
            mmHolder.setLocMessage (1, statusMsg);
            mmHolder.setLocToken (1, "UNABLE_TO_GET_UPLOAD_URL");
            String jbiTaskStr = mMMImpl.buildCompleteExceptionMessage (mmHolder);
            
            /*
            String jbiTaskStr = mMMImpl.buildCompleteExceptionMessage(
                                    "getRemoteFileUploadURL", e.getStackTrace(),
                                    "UNABLE_TO_GET_UPLOAD_URL", statusMsg);
             */
            
            throw new Exception(jbiTaskStr);
        }
        
        return uploadURL;
    }

    /**
     * @return the full product name.
     */
    public String getFullProductName()
    {
        return (mContext.getVersionInfo().fullProductName());
    }

    /**
     * @return the short product name.
     */
    public String getShortProductName()
    {
        return (mContext.getVersionInfo().shortProductName());
    }

    /**
     * @return the major version number.
     */
    public String getMajorVersion()
    {
        return (mContext.getVersionInfo().majorVersion());
    }

    /**
     * @return the minor version number.
     */
    public String getMinorVersion()
    {
        return (mContext.getVersionInfo().minorVersion());
    }

    /**
     * @return the build number for this version.
     */
    public String getBuildNumber()
    {
        return (mContext.getVersionInfo().buildNumber());
    }

    /**
     * @return the Copyright for this version.
     */
    public String getCopyright()
    {
        return (mContext.getVersionInfo().copyright());
    }

    /**
     * Start all Management service agents
     * @return true if successful.
     */
    public boolean startManagementServices()
    {
        mLogger.fine("JBI runtime management services starting");
        //for each management LC MBean (except adminService), invoke start on it:
        try 
        { 
            mMBeanServer.invoke(mConfigServiceLifeCycleMBeanName,
                "start", new Object[0], new String[0]);
            mMBeanServer.invoke(mDeployServiceLifeCycleMBeanName,
                "start", new Object[0], new String[0]);
            mMBeanServer.invoke(mInstallServiceLifeCycleMBeanName,
                "start", new Object[0], new String[0]);
            mMBeanServer.invoke(mLoggingServiceLifeCycleMBeanName,
                "start", new Object[0], new String[0]);
            mMBeanServer.invoke(mMessageServiceLifeCycleMBeanName,
                "start", new Object[0], new String[0]);
        }
        catch (Exception e)
        {
            mLogger.severe(e.toString());
            return false;
        }

        mLogger.fine("JBI runtime management services startup complete");
        return true;
    }

    /**
     * Stop all Management service agents and deregister Service Mbeans.
     * @return true if successful.
     */
    public boolean stopManagementServices()
    {
        //for each management LC MBean (except adminService), invoke stop on it:
        try 
        { 
            mMBeanServer.invoke(mConfigServiceLifeCycleMBeanName,
                "stop", new Object[0], new String[0]);
            mMBeanServer.invoke(mDeployServiceLifeCycleMBeanName,
                "stop", new Object[0], new String[0]);
            mMBeanServer.invoke(mInstallServiceLifeCycleMBeanName,
                "stop", new Object[0], new String[0]);
            mMBeanServer.invoke(mLoggingServiceLifeCycleMBeanName,
                "stop", new Object[0], new String[0]);
            mMBeanServer.invoke(mMessageServiceLifeCycleMBeanName,
                "stop", new Object[0], new String[0]);
        }
        catch (Exception e)
        {
            mLogger.severe(e.toString());
            return false;
        }
        return true;
    }

    /**
     * Shutdown all Management service agents and deregister all MBeans..
     * @return true if successful.
     */
    public boolean shutdownManagementServices()
    {
        int nerrors = 0;

        //for each management LC MBean (except adminService), invoke shutdown on it:
        try 
        { 
            mMBeanServer.invoke(mConfigServiceLifeCycleMBeanName,
                "shutDown", new Object[0], new String[0]);
            mMBeanServer.invoke(mDeployServiceLifeCycleMBeanName,
                "shutDown", new Object[0], new String[0]);
            mMBeanServer.invoke(mInstallServiceLifeCycleMBeanName,
                "shutDown", new Object[0], new String[0]);
            mMBeanServer.invoke(mLoggingServiceLifeCycleMBeanName,
                "shutDown", new Object[0], new String[0]);
            mMBeanServer.invoke(mMessageServiceLifeCycleMBeanName,
                "shutDown", new Object[0], new String[0]);
        }
        catch (Exception e)
        {
            mLogger.severe(e.toString());
            ++nerrors;
        }

        //shutdown myself last:
        try 
        { 
            mMBeanServer.invoke(mAdminServiceLifeCycleMBeanName,
                "shutDown", new Object[0], new String[0]);
        }
        catch (Exception e2)
        {
            mLogger.severe(e2.toString());
            ++nerrors;
        }

        return (nerrors == 0);
    }


/////////
//methods private to AdminService
/////////    
    /**
     * map a management system service name to the unique bean providing
     * the managment services.
     * @param svcName is the name of the management service to map.
     * @return management service CONTROL_TYPE corresponding svcName.
     */
    private String managementServiceControl(String svcName)
    {
        MBeanNames mbn = mContext.getMBeanNames();

        if (mbn.SERVICE_NAME_ADMIN_SERVICE.equals(svcName))
        {
            return mbn.CONTROL_TYPE_ADMIN_SERVICE;
        }
        else if (mbn.SERVICE_NAME_CONFIG_SERVICE.equals(svcName))
        {
            return mbn.CONTROL_TYPE_CONFIG_SERVICE;
        }
        else if (mbn.SERVICE_NAME_DEPLOY_SERVICE.equals(svcName))
        {
            return mbn.CONTROL_TYPE_DEPLOY_SERVICE;
        }
        else if (mbn.SERVICE_NAME_INSTALL_SERVICE.equals(svcName))
        {
            return mbn.CONTROL_TYPE_INSTALL_SERVICE;
        }
        else if (mbn.SERVICE_NAME_LOGGING_SERVICE.equals(svcName))
        {
            return mbn.CONTROL_TYPE_LOGGING_SERVICE;
        }
        else if (mbn.SERVICE_NAME_MESSAGE_SERVICE.equals(svcName))
        {
            return mbn.CONTROL_TYPE_MESSAGE_SERVICE;
        }


        return null;
    }

    /**
     * Execute a query and return array of mbean object names with result.
     * This form of queryHelper always executes in the local mbean server.
     *
     * @param qStr is a JMX formatted query string
     * @return array of JMX object names, possibly of zero length.
     */
    public ObjectName[] queryHelper(String qStr)
    {
        return queryHelper(qStr, mContext.getMBeanServer());
    }

    /**
     * Execute a query and return array of mbean object names with result.
     * @param qStr is a JMX formatted query string
     * @return array of JMX object names, possibly of zero length.
     */
    public ObjectName[] queryHelper(String qStr, MBeanServerConnection rmbs)
    {
        ObjectName mbpat = null;
        Set result = null;

        try
        {
            mbpat = new ObjectName(qStr);
        }
        catch (Exception e)
        {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.AS_QUERYHELPER_EXCEP,
                "AdminService.queryHelper",
                qStr);
            mLogger.severe(statusMsg);
            mLogger.log(Level.FINEST, "Exception in AdminService:queryHelper ", e);
            return (new ObjectName[0]);
        }

        //next, query the mbean server for this component:
        //signature: queryNames(ObjectName name, QueryExp query): Set
        try {
            result = rmbs.queryNames(mbpat, null);
        } catch (Exception e) {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.AS_QUERYHELPER_EXCEP,
                "AdminService.queryHelper",
                qStr);
            mLogger.severe(statusMsg);
           mLogger.log(Level.FINEST, "Exception in AdminService:queryHelper ", e);
            return (new ObjectName[0]);
        }

        if (result.size() >= 1)
        {
            //copy results into array (result.toArray() not working!! RT 4/21/04):
            ObjectName[] names = new ObjectName[result.size()];
            Iterator itr; int ii;
            for (itr = result.iterator(), ii = 0; itr.hasNext(); ii++)
            {
                names[ii] = (ObjectName) itr.next();
            }

            return (names);
        }

        return (new ObjectName[0]);
    }

    /**
     * Helper to get the list of installed engines or bindings.
     * @param ctype is the control type
     * @return array of JMX object names or an empty array if
     * there are no matching object names.
     */
    private ObjectName[] getInstalledComponents(String ctype)
    {
        MBeanNames mbn = mContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_INSTALLED;
        tmp += "," + mbn.CONTROL_TYPE_KEY + "=" + mbn.CONTROL_TYPE_LIFECYCLE;
        tmp += "," + mbn.INSTALLED_TYPE_KEY + "=" + ctype;
        tmp += "," + mbn.INSTANCE_NAME_KEY + "=" + mbn.getJbiInstanceName();
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query:
        ObjectName[] names = queryHelper(tmp);

        if (names.length >= 1)
        {
            return names;
        }
        else
        {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.AS_GETINSTALLEDCOMPONENTS_FAILURE,
                tmp);
            mLogger.warning(statusMsg);
        }

        return (new ObjectName[0]);
    }

    private void startAutoTasks()
    {
        ObjectName  timerName;
        Boolean     isCAS;
        
        mLogger.fine(mTranslator.getString(LocalStringKeys.AS_HEARTBEAT_START));
        
        timerName = mContext.getMBeanNames().
            getSystemServiceMBeanName("AdminService", "HeartBeat");
        
        // initialize the auto admin infrastructure
        sAutoAdminTask = new AutoAdminTask(mContext);
        
        if (null == timerName)
        {
            mLogger.severe(mTranslator.getString(LocalStringKeys.AS_HEARTBEAT_NULL));
            return;
        }

        //Create HeartBeat MBean:
        try
        {
            mContext.getMBeanServer().createMBean(
                "javax.management.timer.Timer", timerName);
            mContext.getMBeanServer().invoke(timerName,
                                             "start",
                                             new Object[] {},
                                             new String[] {});
            
            // Schedule a heart beat every mHeartBeatInterval milliseconds
            mNotificationId = (Integer) mContext.getMBeanServer().invoke(
                timerName,
                "addNotification",
                new Object[] { "AdminService.HeartBeat",
                               "heartBeat",
                               null,
                               nextBeat(),
                               new Long(mHeartBeatInterval) },
                new String[] { "java.lang.String",
                               "java.lang.String",
                               "java.lang.Object",
                               "java.util.Date",
                               "long" });

            // Listen for timer notifications
            mContext.getMBeanServer().addNotificationListener(
                timerName,
                this,
                null,
                new Object());
        }
        catch (Exception e)
        {
            /* throws:
             *    InstanceAlreadyExistsException,
             *    MBeanRegistrationException,
             *    NotCompliantMBeanException
             */
            mLogger.log(Level.FINEST, "Exception in AdminService:startAutoTasks ", e);
        }
    }

    /**
     * This method is invoked every heart beat interval.
     *
     */
    private void heartBeat()
    {
        try
        {
            // guard against race condition of shutdown during any phase --
            // the sAutoAdminTask can become null while we're performing something.
            if (null != sAutoAdminTask)
            {
                if (Thread.currentThread().getContextClassLoader() == null) {
                    Thread.currentThread().setContextClassLoader(
                            AdminService.class.getClassLoader());
                }
                
                sAutoAdminTask.performAutoFunctions();
            }
        }
        catch (Exception e)
        {
            mLogger.log(Level.FINEST, "Exception in AdminService:heartBeat ", e);
        }
    }

    private Date nextBeat()
    {
        return new Date(System.currentTimeMillis() + mHeartBeatInterval);
    }

    private void stopAutoTasks()
    {
        String statusMsg = mTranslator.getString(LocalStringKeys.AS_HEARTBEAT_STOP);
        mLogger.fine(statusMsg);

        sAutoAdminTask = null;

        MBeanNames mbn = mContext.getMBeanNames();
        ObjectName timerName = mbn.
            getSystemServiceMBeanName("AdminService", "HeartBeat");
        
        if (null == timerName)
        {
            statusMsg = mTranslator.getString(LocalStringKeys.AS_HEARTBEAT_NULL);
            mLogger.severe(statusMsg);
            return;
        }

        //Create HeartBeat MBean:
        try
        {
            // Stop listening for timer notifications
            mContext.getMBeanServer().removeNotificationListener(timerName, this);

            // and unregister the MBean.
            mContext.getMBeanServer().unregisterMBean(timerName);
        }
        catch (Exception e)
        {
            /* throws:
             *    InstanceAlreadyExistsException,
             *    MBeanRegistrationException,
             *    NotCompliantMBeanException
             */
            mLogger.log(Level.FINEST, "Exception in AdminService:stopAutoTasks", e);
        }
    }

    /**
     * create Object names for all System Service LifeCycle MBeans.
     */
    private void initSystemServiceLifeCycleMBeanNames()
    {
        mAdminServiceLifeCycleMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_ADMIN_SERVICE,
                mMBeanNames.CONTROL_TYPE_LIFECYCLE
            );
        mConfigServiceLifeCycleMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_CONFIG_SERVICE,
                mMBeanNames.CONTROL_TYPE_LIFECYCLE
            );
        mDeployServiceLifeCycleMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_DEPLOY_SERVICE,
                mMBeanNames.CONTROL_TYPE_LIFECYCLE
            );
        mInstallServiceLifeCycleMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_INSTALL_SERVICE,
                mMBeanNames.CONTROL_TYPE_LIFECYCLE
            );
        mLoggingServiceLifeCycleMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_LOGGING_SERVICE,
                mMBeanNames.CONTROL_TYPE_LIFECYCLE
            );
        mMessageServiceLifeCycleMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_MESSAGE_SERVICE,
                mMBeanNames.CONTROL_TYPE_LIFECYCLE
            );        
    }    
    
    /**
     * Get the value of a configuration attribute
     *
     * @param category - configuration category the attribute is defined in
     * @param attrName - name of the attribute
     * @return the value of the requested configuration attribute
     */
    private Object getConfigurationAttribute(ConfigurationCategory category, String attrName)
    {
        
        MBeanNames.ServiceType svcType = MBeanNames.ServiceType.valueOf(category.toString());
        
        ObjectName configMBeanName = mMBeanNames.
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    com.sun.jbi.management.config.ConfigurationBuilder.getControlType(svcType));
        
        
        Object attrValue = null;
        try
        {
            if ( mMBeanServer.isRegistered(configMBeanName) )
            {
                attrValue = mMBeanServer.getAttribute(configMBeanName, attrName);
            }
        }
        catch ( javax.management.JMException jmex)
        {
            mLogger.warning(jmex.toString());
        }
        
        return attrValue;
    }   
    
    /**
     * Move to a new beat if different from the current one.
     */
    private void checkHeartBeat()
    {
        
        ObjectName timerName = mContext.getMBeanNames().
            getSystemServiceMBeanName("AdminService", "HeartBeat");

        Object interval = getConfigurationAttribute(ConfigurationCategory.System, 
            com.sun.jbi.management.config.SystemConfigurationFactory.HEART_BEAT_INTERVAL);
        long currHeartBeat = mHeartBeatInterval;
        try
        {
            if ( interval != null )
            {
                currHeartBeat = ((Integer)interval).intValue();
            }
            
            if ( currHeartBeat != mHeartBeatInterval )
            {
                if ( mContext.getMBeanServer().isRegistered(timerName) )
                {
                    mLogger.fine("Switching to new heart beat interval of " 
                        + currHeartBeat + " ms old heart beat was " 
                        + mHeartBeatInterval + " ms.");
                    
                    // -- Remove the old notification
                    mContext.getMBeanServer().invoke(
                        timerName,
                        "removeNotification",
                        new Object[] { mNotificationId },
                        new String[] { "java.lang.Integer"});

                    // -- Add new notfication
                    mHeartBeatInterval = currHeartBeat;

                    mNotificationId = (Integer) mContext.getMBeanServer().invoke(
                        timerName,
                        "addNotification",
                        new Object[] { "AdminService.HeartBeat",
                                       "heartBeat",
                                       null,
                                       new Date(System.currentTimeMillis()),
                                       new Long(mHeartBeatInterval) },
                        new String[] { "java.lang.String",
                                       "java.lang.String",
                                       "java.lang.Object",
                                       "java.util.Date",
                                       "long" });
                }
            }
        }
        catch(Exception ex)
        {
            mLogger.warning(MessageHelper.getMsgString(ex));
        }
    }
    
    
}

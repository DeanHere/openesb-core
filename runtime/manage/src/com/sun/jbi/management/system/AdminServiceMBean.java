/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminServiceMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import javax.management.ObjectName;
import javax.management.MBeanServerConnection;

/**
 * AdminServiceMBean defines the Sun Extentions to AdminServiceMBean.
 *
 * The AdminService is responsible for bootstrapping the
 * JBI Framework management layer for all other system services,
 * starting and stoping the system, and providing information
 * that remote clients can use to access other JBI Framework
 * system services and installed components.
 *
 * @author Sun Microsystems, Inc.
 */
public interface AdminServiceMBean extends javax.jbi.management.AdminServiceMBean
{
    /**
     * Get the DeployerMBean for the <CODE>componentName</CODE> associated with
     * <code>artifactUrl</code>.
     *
     * @return JMX object name of Component's DeployerMBean or null
     */
    ObjectName[] getDeployerMBeanNames(String componentName);

    /**
     * Return the name of this JBI Framework runtime
     *
     * @return the instance name of this runtime.
     */
    String getJbiInstanceName();

    /**
     * Lookup all active MBeans associated with <CODE>componentName</CODE>.
     * @param componentName - is the name of the BC or SE.
     * @return the array of JMX object names for the component.
     */
    ObjectName[] getComponentMBeans(String componentName);

    /**
     * Return URL that can be used to upload and deploy a remote  
     * Service Assembly.
     * @return URL String
     */
    String getRemoteFileUploadURL() throws Exception;

    /**
     * @return the full product name.
     */
    String getFullProductName();

    /**
     * @return the short product name.
     */
    String getShortProductName();

    /**
     * @return the major version number.
     */
    String getMajorVersion();

    /**
     * @return the minor version number.
     */
    String getMinorVersion();

    /**
     * @return the build number for this version.
     */
    String getBuildNumber();

    /**
     * @return the Copyright for this version.
     */
    String getCopyright();

    /**
     * Start all Management service agents
     * @return true if successful.
     */
    boolean startManagementServices();

    /**
     * Stop all Management service agents and deregister Service Mbeans.
     * @return true if successful.
     */
    boolean stopManagementServices();

    /**
     * Shutdown all Management service agents and deregister all MBeans..
     * @return true if successful.
     */
    boolean shutdownManagementServices();
}

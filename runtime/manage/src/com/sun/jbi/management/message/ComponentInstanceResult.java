/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentInstanceResult.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  MessageBuilder.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 23, 2006, 5:17 PM
 */

package com.sun.jbi.management.message;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * Value object to hold the results of deploying a service unit to the component installed
 * on various instances.
 *
 * For each component a map of instance name and the corresponding component task result 
 * is maintained, the map is keyed by instance name and the value is the component task 
 * result details for the component on that instance. 
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentInstanceResult
{
    // -- The name of the component
    private String                                   mComponentName;
    
    // -- The Map of [ instance | Task result details ]
    private Map<String, TaskResultDetailsElement>    mInstanceResults;
    
    /**
     * Create a component instance result.
     *
     * @param compName - component name
     * @param instRslt - map of intance names to the task result details
     */
    public ComponentInstanceResult(String compName, Map<String, TaskResultDetailsElement> instRslt)
    {
        mComponentName = compName;
        mInstanceResults = instRslt;
    }

    /**
     * @return the instance results map for the component
     */
    public Map<String, TaskResultDetailsElement> getInstanceResults()
    {
        if ( mInstanceResults == null ) 
        {
            mInstanceResults = new HashMap<String, TaskResultDetailsElement>();
        }
        return mInstanceResults;
    }
    
    /**
     * @return the component name
     */
    public String getComponentName()
    {
        return mComponentName;
    }
    
}



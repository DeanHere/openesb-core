/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MBeanHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.support;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.util.EnvironmentAccess;
import javax.management.ObjectName;
import javax.management.MBeanServer;
import javax.management.StandardMBean;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Implementation of MBeanHelper interface.
 *
 * @author Sun Microsystems, Inc.
 */
public class MBeanHelper 
        implements com.sun.jbi.management.MBeanHelper, 
                   com.sun.jbi.util.Constants
{

   /**
    * Handle to the main MBean server
    */
    private final MBeanServer mMBeanServer;

   /**
    * Handle to the MBeanNames interface
    */
    private final MBeanNames mMBeanNames;

   /**
    * Handle to the logger where this class will log errors.
    */
    private Logger mLogger;

     /**
     * Handle to StringTranslator for message translation
     */
     private StringTranslator mTranslator;
     
     /**
      * Environment Context
      */
     private com.sun.jbi.EnvironmentContext mCtx;
    
    /**
     * Constructs a <CODE>MBeanHelper</CODE>.
     * This class is meant to be instantiated only once by the framework.
     * @param aMBeanServer is the MBean server provided by the framework.
     * @param aMBeanNames is the MBeanNames instance created by the framework.
     * @param aLogger is where this class should log messages.
     */
    public MBeanHelper(MBeanServer aMBeanServer, com.sun.jbi.management.MBeanNames aMBeanNames, Logger aLogger) {
        mMBeanServer = aMBeanServer;
        mMBeanNames = aMBeanNames;
        mLogger = aLogger;
        mCtx =
                com.sun.jbi.util.EnvironmentAccess.getContext();
        mTranslator = mCtx.getStringTranslator("com.sun.jbi.management");
    }

    /**
     * Create a LoggerMBean and associate it with <CODE>aLogger</CODE>.
     * @param aService is the name of the system service that owns the logger.
     * @param aLogger is the Logger to associate with the LoggerMBean.
     * @return true if successful, otherwise false.
     */
    public boolean createSystemServiceLoggerMBean(String aService, Logger aLogger)
    {
        mLogger.fine( "createSystemServiceLoggerMBean for Service="
            + aService + " logger=" + aLogger.getName() );

        // Create the object name for this service:
        ObjectName mbname = mMBeanNames.getSystemServiceLoggerMBeanName(
                                aService, aLogger);
        if (null == mbname)
        {
            String statusMsg = mTranslator.getString(
                    LocalStringKeys.MBEAN_HELPER_FAILED_OBJECT_CREATION, 
                    aService, mMBeanNames.CONTROL_TYPE_LOGGER);
            mLogger.severe(statusMsg);
            return false;
        }

        try
        {
            //create a logger mbean implementation:
            SystemServiceLoggerMBeanImpl loggerImpl 
                    = new SystemServiceLoggerMBeanImpl(aLogger, aService);
        
            //register a standard mbean:
            StandardMBean mbean = loggerImpl; 
            mMBeanServer.registerMBean(mbean, mbname);
        }
        catch ( Exception ex )
        {
            String[] params = new String[]{aService, 
                                           aLogger.getName(), 
                                           ex.getMessage()}; 
            String locMsg = mTranslator.getString(
                    com.sun.jbi.management.LocalStringKeys.
                        JBI_ADMIN_LOGGER_MBN_CREATE_FAILED, params);
            
            mLogger.warning(locMsg);
            return false;
        }

        return true;
    }

    /**
     * Destroy the LoggerMBean registered for <CODE>aService</CODE>.
     * @param aService is the name of the system service that owns the logger.
     * @return true if successful, otherwise false.
     */
    public boolean destroySystemServiceLoggerMBean(String aService)
    {
        mLogger.fine("destroySystemServiceLoggerMBean for Service=" + aService);
        //create the object name for this service:
        ObjectName mbname = mMBeanNames.getSystemServiceMBeanName(aService,
                                mMBeanNames.CONTROL_TYPE_LOGGER);
        if (null == mbname)
        {
            String statusMsg = mTranslator.getString(
                    LocalStringKeys.MBEAN_HELPER_FAILED_OBJECT_CREATION,
                    aService, mMBeanNames.CONTROL_TYPE_LOGGER);
            mLogger.severe(statusMsg);
            return false;
        }
        try
        {
            mMBeanServer.unregisterMBean(mbname);
        }
        catch (Exception e)
        {
            /* throws:
             *    InstanceNotFoundException,
             *    MBeanRegistrationException,
             *    RuntimeOperationsException
             */
            return false;
        }
        return true;
    }

    /**
     * Destroy a LoggerMBean registered for <CODE>aService</CODE>.
     * @param aService is the name of the system service that owns the logger.
     * @param aLogger is the Logger associated with the LoggerMBean.
     * @return true if successful, otherwise false.
     */
    public boolean destroySystemServiceLoggerMBean(String aService, Logger aLogger)
    {
        mLogger.fine("destroySystemServiceLoggerMBean for Service=" + aService);
        // Create the object name for this service:
        ObjectName mbname = mMBeanNames.getSystemServiceLoggerMBeanName(
                                aService, aLogger);
        if (null == mbname)
        {
            String statusMsg = mTranslator.getString(
                    LocalStringKeys.MBEAN_HELPER_FAILED_OBJECT_CREATION,
                    aService, mMBeanNames.CONTROL_TYPE_LOGGER);
            mLogger.severe(statusMsg);
            return false;
        }
        try
        {
            mMBeanServer.unregisterMBean(mbname);
        }
        catch (Exception e)
        {
            /* throws:
             *    InstanceNotFoundException,
             *    MBeanRegistrationException,
             *    RuntimeOperationsException
             */
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Get the value of the management log level
     *
     * @param envCtx  - management context
     * @param attrName - name of the logger
     * @return the log level of the requested logger or null if logger not found
     */
    public static Level getLogLevel(String attrName, com.sun.jbi.EnvironmentContext envCtx )
    {
        MBeanNames.ServiceType svcType = MBeanNames.ServiceType.valueOf(
            com.sun.jbi.management.ConfigurationCategory.Logger.toString());
        
        ObjectName configMBeanName = envCtx.getMBeanNames().
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    com.sun.jbi.management.config.ConfigurationBuilder.getControlType(svcType));
        
        Level level = null;
        try
        {
            if ( envCtx.getMBeanServer().isRegistered(configMBeanName) )
            {
                String attrValue = (String) envCtx.getMBeanServer().
                    getAttribute(configMBeanName, attrName);
                
                if ( attrValue == null || 
                     "null".equalsIgnoreCase(attrValue) || 
                     LOG_LEVEL_DEFAULT.equalsIgnoreCase(attrValue))
                {
                    level = null;
                }
                else
                {
                    
                    level = Level.parse(attrValue);
                }
            }
            else
            {
                Logger.getLogger("com.sun.jbi").finest("MBean " + configMBeanName + "is not registered");
            }
        }
        catch ( javax.management.JMException jmex)
        {
            Logger.getLogger("com.sun.jbi").warning(jmex.toString());   
        }
        
        return level;
    } 
    
    /**
     * Set the value of the management log level
     *
     * @param envCtx  - environment context
     * @param logName - name of the logger
     * @param logLevel - the log level string
     * @return the log level of the requested logger or null if logger not found
     */
    public static void setLogLevel(String logName, String logLevel, 
        com.sun.jbi.EnvironmentContext envCtx, Logger aLogger)
    {
        MBeanNames.ServiceType svcType = MBeanNames.ServiceType.valueOf(
            com.sun.jbi.management.ConfigurationCategory.Logger.toString());
        
        ObjectName configMBeanName = envCtx.getMBeanNames().
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    com.sun.jbi.management.config.ConfigurationBuilder.getControlType(svcType));
        try
        {
            if ( envCtx.getMBeanServer().isRegistered(configMBeanName) )
            {
                javax.management.Attribute attrib = 
                    new javax.management.Attribute(logName, logLevel);
                envCtx.getMBeanServer().setAttribute(configMBeanName, attrib);
            }
            else
            {
                aLogger.finest("MBean " + configMBeanName + "is not registered");
            }
        }
        catch ( javax.management.JMException jmex)
        {
            aLogger.warning(jmex.toString());
        }
    } 
    
    /**
     * Revert to the global log level. This causes the persisted attribute to 
     * be deleted from the target for the instance. The global log level ( which
     * is the same as the default / com.sun.jbi log level. 
     *
     * @param loggerName - name of the logger whose override is to be deleted
     * @param envCtx - environment context
     * @param aLogger - logger to log any warnings to
     */
    public static void revertToGlobalLogLevel(String logName,
        com.sun.jbi.EnvironmentContext envCtx, Logger aLogger)
    {
        MBeanNames.ServiceType svcType = MBeanNames.ServiceType.valueOf(
            com.sun.jbi.management.ConfigurationCategory.Logger.toString());
        
        ObjectName configMBeanName = envCtx.getMBeanNames().
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    com.sun.jbi.management.config.ConfigurationBuilder.getControlType(svcType));
        try
        {
            if ( envCtx.getMBeanServer().isRegistered(configMBeanName) )
            {
                envCtx.getMBeanServer().invoke(configMBeanName, "deleteOverride", 
                    new Object[]{logName}, new String[]{"java.lang.String"});
            }
            else
            {
                aLogger.finest("MBean " + configMBeanName + "is not registered");
            }
        }
        catch ( javax.management.JMException jmex)
        {
            aLogger.warning(jmex.toString());
        }
    }
    
    /**
     * @return the object name of the Logger Configuration MBean on the instance
     */
    public static ObjectName getLoggerConfigMBeanName()
    {
        MBeanNames.ServiceType svcType = MBeanNames.ServiceType.valueOf(
            com.sun.jbi.management.ConfigurationCategory.Logger.toString());
        
        return com.sun.jbi.util.EnvironmentAccess.getContext().
                getMBeanNames().
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    com.sun.jbi.management.config.ConfigurationBuilder.getControlType(svcType));
    }
    
    /**
     * @return the object name of the System Configuration MBean on the instance
     */
    public static ObjectName getSystemConfigMBeanName()
    {
        MBeanNames.ServiceType svcType = MBeanNames.ServiceType.valueOf(
            com.sun.jbi.management.ConfigurationCategory.System.toString());
        
        return com.sun.jbi.util.EnvironmentAccess.getContext().
                getMBeanNames().
                getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_CONFIG_SERVICE , 
                    com.sun.jbi.management.config.ConfigurationBuilder.getControlType(svcType));
    }
}

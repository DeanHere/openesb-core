/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MBeanNamesImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.support;

import com.sun.jbi.ComponentType;
import java.util.logging.Logger;

import javax.management.ObjectName;

/**
 * Implementation of MBeanNames interface.
 * 
 * @author Sun Microsystems, Inc.
 */
public class MBeanNamesImpl implements com.sun.jbi.management.MBeanNames {

    public static final String JBI_DOMAIN = "com.sun.jbi";

    public static final String SERVICE_TYPE_KEY = "ServiceType";
    
    public static final String SERVICE_NAME_KEY = "ServiceName";    

    private static final String TARGET_KEY = "Target";

    private static final String COMPONENT_NAME_KEY = "ComponentName";

    private static final String EQUAL = "=";

    private static final String COLON = ":";

    private static final String COMMA = ",";

    /**
     * The JMX domain name for our implementation.
     */
    private String mDomainName;

    /**
     * The instance name of this runtime.
     */
    private String mInstanceName;

    /**
     * Constructor.
     * 
     * @param jmxDomain -
     *            JMX Domain Name.
     * @param instanceName -
     *            instance name of the runtime.
     */
    public MBeanNamesImpl(String jmxDomain, String instanceName) 
    {
        mDomainName = jmxDomain;
        mInstanceName = instanceName;
    }

    // ///////////////////////////////////
    // Implementation of public interface
    // ///////////////////////////////////

    /**
     * Retrieve the default JMX Domain Name for MBeans registered in this
     * instance of the JBI implementation.
     * 
     * @return the JMX domain name for this instance of the JBI implemention;
     *         must be non-null and non-empty
     */
    public String getJmxDomainName() {
        return (mDomainName);
    }

    /**
     * Formulate and return an MBean ObjectName for a custom control of this
     * name creator's JBI component.
     * <p>
     * This is used by components to create JMX names for their own JMX
     * controls, allowing the JBI implementation to prefix the created name to
     * fit within the implementation's own naming scheme.
     * <p>
     * Standard extensions must use the following custom name constants:
     * <ul>
     * <li>Bootstrap (installer) extension: {@link #BOOTSTRAP_EXTENSION}.</li>
     * <li>Component life cycle extension:
     * {@link #COMPONENT_LIFE_CYCLE_EXTENSION}. </li>
     * </ul>
     * All other custom component MBeans must use custom names that do not
     * collide with the standard extension names.
     * 
     * @param customName
     *            the name of the custom control; must be non-null and
     *            non-empty; must be legal for use in a JMX object name
     * @return the JMX ObjectName of the MBean, or <code>null</code> if the
     *         <code>customName</code> is invalid
     */
    public ObjectName createCustomComponentMBeanName(String customName) {
        return null;
    }

    // /////////////////////////////////////////
    // Implementation of Sun internal interface
    // /////////////////////////////////////////

    /**
     * Utility method to get a component's MBean name based on the component's
     * type (binding or engine).  This saves clients from coding the same
     * conditional (e.g. if type == binding call getBindingMBeanName) over
     * and over again.
     * @param componentName component name
     * @param serviceType type of MBean
     * @param target administration target
     * @return ObjectName in the component's namespace for the given service
     */
    public ObjectName getComponentMBeanName(String compName, ComponentType compType,
            ComponentServiceType serviceType, String target)
    {
        ObjectName mbn;
        
        switch (compType)
        {
            case BINDING:
                mbn = getBindingMBeanName(compName, serviceType, target);
                break;
            
            case ENGINE:
                mbn = getEngineMBeanName(compName, serviceType, target);
                break;
            
            default:
                throw new IllegalArgumentException(compType.toString());
        }
        
        return mbn;
    }

    /**
     * Formulate and return the MBean ObjectName of a control MBean for a
     * Binding Component.
     * 
     * @param bindingName
     *            the name of the Binding Component.
     * @param controlType
     *            the type of control (MBean type).
     * @return the JMX ObjectName of the MBean, or <code>null</code> if
     *         <code>controlType</code> is invalid.
     */
    public ObjectName getBindingMBeanName(String bindingName, String controlType) {
        String tmp = createBindingMBeanName(null, bindingName, controlType);

        ObjectName mbname = null;
        try {
            mbname = new ObjectName(tmp);
        } catch (Exception e) {
            mbname = null;
            e.printStackTrace();
        }

        return (mbname);
    }

    /**
     * Formulate and return the MBean ObjectName of custom control MBean for a
     * Binding Component.
     * 
     * @param customName
     *            the name of the custom control.
     * @param bindingName
     *            the name of the Binding Component.
     * @return the JMX ObjectName or <code>null</code> if illegal name.
     */
    public ObjectName getCustomBindingMBeanName(String customName,
            String bindingName) {
        String tmp = createBindingMBeanName(customName, bindingName,
                CONTROL_TYPE_CUSTOM);

        ObjectName mbname = null;
        try {
            mbname = new ObjectName(tmp);
        } catch (Exception e) {
            mbname = null;
            e.printStackTrace();
        }

        return (mbname);
    }

    /**
     * Formulate and return the MBean ObjectName of custom control MBean for a
     * Service Engine.
     * 
     * @param customName
     *            the name of the custom control.
     * @param engineName
     *            the name of the Service Engine.
     * @return the JMX ObjectName or <code>null</code> if illegal name.
     */
    public ObjectName getCustomEngineMBeanName(String customName,
            String engineName) {
        String tmp = createEngineMBeanName(customName, engineName,
                CONTROL_TYPE_CUSTOM);

        ObjectName mbname = null;
        try {
            mbname = new ObjectName(tmp);
        } catch (Exception e) {
            mbname = null;
            e.printStackTrace();
        }

        return (mbname);
    }

    /**
     * Formulate and return the MBean ObjectName of a control MBean for a
     * Service Engine.
     * 
     * @param engineName
     *            the name of the Service Engine.
     * @param controlType
     *            the type of control (MBean type).
     * @return the JMX ObjectName of the MBean, or <code>null</code> if
     *         <code>controlType</code> is invalid.
     */
    public ObjectName getEngineMBeanName(String engineName, String controlType) {
        String tmp = createEngineMBeanName(null, engineName, controlType);

        ObjectName mbname = null;
        try {
            mbname = new ObjectName(tmp);
        } catch (Exception e) {
            mbname = null;
            e.printStackTrace();
        }

        return (mbname);
    }

    /**
     * Formulate and return the MBean ObjectName of control MBean for a JBI
     * system service.
     * 
     * @param serviceName
     *            the name of the system service.
     * @param type
     *            the type of the MBean.
     * @return the JMX ObjectName of the specified MBean, or null if the
     *         serviceName or type is illegal.
     */
    public ObjectName getSystemServiceMBeanName(String serviceName, String type) {
        String tmp = createSystemServiceMBeanName(serviceName, type);

        // System.out.println("getSystemServiceMBeanName: tmp='" + tmp + "'");

        ObjectName mbname = null;
        try {
            mbname = new ObjectName(tmp);
        } catch (Exception e) {
            mbname = null;
            e.printStackTrace();
        }

        return (mbname);
    }

    /**
     * Formulate and return the MBean ObjectName of control MBean for a JBI
     * system service for the specified instance name.
     * 
     * @param serviceName
     *            the name of the system service.
     * @param type
     *            the type of the MBean.
     * @param instanceName
     *            the name of the server instance.
     * @return the JMX ObjectName of the specified MBean, or null if the
     *         serviceName or type is illegal.
     */
    public ObjectName getSystemServiceMBeanName(String serviceName, String type,        String instanceName)
    {
        String tmp = createSystemServiceMBeanNameForInstance(serviceName,
            type, instanceName);

        // System.out.println("getSystemServiceMBeanName: tmp='" + tmp + "'");

        ObjectName mbname = null;
        try {
            mbname = new ObjectName(tmp);
        } catch (Exception e) {
            mbname = null;
            e.printStackTrace();
        }

        return (mbname);
    }

    /**
     * Formulate and return the LoggerMBean ObjectName of a JBI Framework system
     * service.
     * 
     * @param name -
     *            the name of the system service.
     * @param logger -
     *            the Logger instance.
     * @return the JMX ObjectName of the service, or null if illegal name.
     */
    public ObjectName getSystemServiceLoggerMBeanName(String name, Logger logger) {
        String tmp = createSystemServiceLoggerMBeanName(name, logger.getName());

        ObjectName mbname = null;
        try {
            mbname = new ObjectName(tmp);
        } catch (Exception e) {
            mbname = null;
            e.printStackTrace();
        }

        return (mbname);
    }

    /**
     * Return the name of this JBI Framework runtime
     * 
     * @return the instance name of this runtime.
     */
    public String getJbiInstanceName() {
        return mInstanceName;
    }

    /**
     * Formulate and return the MBean ObjectName of a local JBI Framework system
     * service. A local services does not include the jbi runtime instance name.
     * 
     * @param name -
     *            the name of the system service
     * @param type -
     *            the type of the MBean
     * @return the JMX ObjectName of the service, or null if illegal name
     */
    public ObjectName getLocalSystemServiceMBeanName(String name, String type) {
        String tmp = createLocalSystemServiceMBeanName(name, type);

        // System.out.println("getSystemServiceMBeanName: tmp='" + tmp + "'");

        ObjectName mbname = null;
        try {
            mbname = new ObjectName(tmp);
        } catch (Exception e) {
            mbname = null;
            e.printStackTrace();
        }

        return (mbname);
    }

    // ////////////////
    // PRIVATE METHODS
    // ////////////////

    /**
     * formulate and return a String representing the MBean name of a JBI
     * Framework system service.
     * 
     * @param aServiceName -
     *            the name of the system service
     * @param aControlType -
     *            the type of control (mbean type)
     * @return a String to be used to create the ObjectName for the system
     *         service MBean.
     */
    private String createSystemServiceMBeanName(String aServiceName,
            String aControlType) {
        String tmp = mDomainName; // domain name

        tmp += ":" + INSTANCE_NAME_KEY + "=" + mInstanceName;
        tmp += "," + SERVICE_NAME_KEY + "=" + aServiceName;
        tmp += "," + CONTROL_TYPE_KEY + "=" + aControlType;
        tmp += "," + COMPONENT_TYPE_KEY + "=" + COMPONENT_TYPE_SYSTEM;

        return (tmp);
    }

    /**
     * formulate and return a String representing the MBean name of a JBI
     * Framework system service.
     * 
     * @param aServiceName -
     *            the name of the system service
     * @param aControlType -
     *            the type of control (mbean type)
     * @param anInstanceName -
     *            the name of the server instance
     * @return a String to be used to create the ObjectName for the system
     *         service MBean.
     */
    private String createSystemServiceMBeanNameForInstance(String aServiceName,
            String aControlType, String anInstanceName) {
        String tmp = mDomainName; // domain name

        tmp += ":" + INSTANCE_NAME_KEY + "=" + anInstanceName;
        tmp += "," + SERVICE_NAME_KEY + "=" + aServiceName;
        tmp += "," + CONTROL_TYPE_KEY + "=" + aControlType;
        tmp += "," + COMPONENT_TYPE_KEY + "=" + COMPONENT_TYPE_SYSTEM;

        return (tmp);
    }

    /**
     * formulate and return a String representing the MBean name of a JBI
     * Framework system service.
     * 
     * @param aServiceName -
     *            the name of the system service
     * @param aControlType -
     *            the type of control (mbean type)
     * @return a String to be used to create the ObjectName for the system
     *         service MBean.
     */
    private String createLocalSystemServiceMBeanName(String aServiceName,
            String aControlType) {
        String tmp = mDomainName; // domain name

        tmp += ":" + SERVICE_NAME_KEY + "=" + aServiceName;
        tmp += "," + CONTROL_TYPE_KEY + "=" + aControlType;
        tmp += "," + COMPONENT_TYPE_KEY + "=" + COMPONENT_TYPE_SYSTEM;

        return (tmp);
    }

    /**
     * formulate and return a String representing the LoggerMBean name of a JBI
     * Framework system service.
     * 
     * @param aServiceName -
     *            the name of the system service
     * @param aLoggerName -
     *            the name of the logger instance
     * @return a String to be used to create the ObjectName for the system
     *         service MBean.
     */
    private String createSystemServiceLoggerMBeanName(String aServiceName,
            String aLoggerName) {
        String tmp = mDomainName; // domain name

        tmp += ":" + INSTANCE_NAME_KEY + "=" + mInstanceName;
        tmp += "," + SERVICE_NAME_KEY + "=" + aServiceName;
        tmp += "," + CONTROL_TYPE_KEY + "=" + CONTROL_TYPE_LOGGER;
        tmp += "," + COMPONENT_TYPE_KEY + "=" + COMPONENT_TYPE_SYSTEM;
        tmp += "," + LOGGER_NAME_KEY + "=" + aLoggerName;

        return (tmp);
    }

    /**
     * formulate and return a String representing the MBean name of a JBI
     * Framework engine component
     * 
     * @param aCustomName -
     *            the CustomControlName. can be null.
     * @param theName -
     *            the unique name of the engine component
     * @param aControlType -
     *            the type of control (mbean type)
     * @return a String to be used to create the ObjectName for the engine
     *         MBean.
     */
    private String createEngineMBeanName(String aCustomName, String theName,
            String aControlType) {
        return createComponentMBeanName(aCustomName, INSTALLED_TYPE_ENGINE,
                theName, aControlType);
    }

    /**
     * formulate and return a String representing the MBean name of a JBI
     * Framework binding component
     * 
     * 
     * @param aCustomName -
     *            the CustomControlName. can be null.
     * @param theName -
     *            the unique name of the binding component
     * @param aControlType -
     *            the type of control (mbean type)
     * @return a String to be used to create the ObjectName for the engine
     *         MBean.
     */
    private String createBindingMBeanName(String aCustomName, String theName,
            String aControlType) {
        return createComponentMBeanName(aCustomName, INSTALLED_TYPE_BINDING,
                theName, aControlType);
    }

    /**
     * Internal method to create an engine or binding MBean Name.
     * 
     * @param aCustomName -
     *            if non-null, add CustomControlName pair.
     * @param aType -
     *            either InstalledTypeEngine or InstalledTypeBinding
     * @param theName -
     *            the unique name of the component
     * @param aControlType -
     *            the type of control (mbean type)
     * @return a String to be used to create the ObjectName for the engine
     *         MBean.
     */
    private String createComponentMBeanName(String aCustomName,
            String aType, String theName, String aControlType) {
        String tmp = mDomainName; // domain name

        tmp += ":" + INSTANCE_NAME_KEY + "=" + mInstanceName;

        if (null != aCustomName && CONTROL_TYPE_LOGGER != aControlType) {
            tmp += "," + CUSTOM_CONTROL_NAME_KEY + "=" + aCustomName;
        }

        tmp += "," + COMPONENT_ID_KEY + "=" + theName;
        tmp += "," + CONTROL_TYPE_KEY + "=" + aControlType;
        tmp += "," + COMPONENT_TYPE_KEY + "=" + COMPONENT_TYPE_INSTALLED;
        tmp += "," + INSTALLED_TYPE_KEY + "=" + aType;

        if (CONTROL_TYPE_LOGGER == aControlType) {
            if (null != aCustomName) {
                tmp += "," + LOGGER_NAME_KEY + "=" + aCustomName;
            }
        }

        return (tmp);
    }

    // ////////////////////////////
    // -- Overloaded methods --
    // ////////////////////////////
    /**
     * Returns object name of type a Binding Object Name:
     * com.sun.jbi:Target={target}, ServiceType={serviceType},
     * ComponentName="bindingName"
     * 
     * @param bindingName -
     *            binding component name
     * @param serviceType
     * @param target -
     *            administration target
     * @return the Object name of the facade AdminServiceMBean for the desired
     *         target.
     */
    public ObjectName getBindingMBeanName(String bindingName,
            ComponentServiceType serviceType, String target) {
        // -- only allowed svc types are ComponentLifeCycle and Installer
        String adminMBeanName = JBI_DOMAIN + COLON + TARGET_KEY + EQUAL
        + target + COMMA + SERVICE_TYPE_KEY + EQUAL
        + serviceType.toString() + COMMA + COMPONENT_NAME_KEY + EQUAL
        + bindingName;
        try {
            return new ObjectName(adminMBeanName);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns object name of type a Engine Object Name:
     * com.sun.jbi:Target={target}, ServiceType={serviceType},
     * ComponentName="engineName"
     * 
     * @param engineName -
     *            service engine name
     * @param serviceType
     * @param target -
     *            administration target
     * @return the Object name of the facade AdminServiceMBean for the desired
     *         target.
     */
    public ObjectName getEngineMBeanName(String engineName,
            ComponentServiceType serviceType, String target) {
        // -- only allowed svc types are ComponentLifeCycle and Installer
        String adminMBeanName = JBI_DOMAIN + COLON + TARGET_KEY + EQUAL
        + target + COMMA + SERVICE_TYPE_KEY + EQUAL
        + serviceType.toString() + COMMA + COMPONENT_NAME_KEY + EQUAL
        + engineName;
        try {
            return new ObjectName(adminMBeanName);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns object name of type a SystemService Object Name:
     * com.sun.jbi:Target={target},ServiceName={serviceName},ServiceType={serviceType}
     * 
     * @param serviceName the system service name
     * @param serviceType the system service type
     * @param target -
     *            administration target
     * @return the Object name of the facade AdminServiceMBean for the desired
     *         target.
     */
    public ObjectName getSystemServiceMBeanName(ServiceName serviceName,
            ServiceType serviceType,
            String target) {
        String adminMBeanName = JBI_DOMAIN + COLON + TARGET_KEY + EQUAL
        + target + COMMA + SERVICE_NAME_KEY + EQUAL + serviceName.toString() 
        + COMMA +  SERVICE_TYPE_KEY + EQUAL + serviceType.toString();

        try {
            return new ObjectName(adminMBeanName);
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Returns the ObjectName to be used to filter component registered custom MBeans : 
     * com.sun.jbi:JbiName={instanceName}, ControlType=Custom
     *
     * @param instanceName  - instance name
     * @param customName    - custom control name
     * @param componentName - component name
     * @return the custom component MBean ObjectName
     */
    public ObjectName getCustomComponentMBeanNameFilter(String instanceName, String customName, String componentName)
    {
        StringBuffer customMBeanFilter = new StringBuffer(mDomainName); // domain name

        customMBeanFilter.append(":" + INSTANCE_NAME_KEY + "=" + instanceName);
        customMBeanFilter.append("," + CUSTOM_CONTROL_NAME_KEY  + "=" + customName);
        customMBeanFilter.append("," + COMPONENT_ID_KEY  + "=" + componentName);
        customMBeanFilter.append("," + CONTROL_TYPE_KEY + "=" + CONTROL_TYPE_CUSTOM);
        customMBeanFilter.append(",*");
        
        try 
        {
            return new ObjectName(customMBeanFilter.toString());
        } 
        catch (Exception e) 
        {
            return null;
        }
    }
    
    
    /**
     * Returns the ObjectName to be used to filter Logger MBeans registered for a component : 
     * com.sun.jbi:JbiName=instanceName,CustomControlName=logName,ComponentName=mComponentName,ControlType=Logger,*
     *
     * @param instanceName - target instance name
     * @param componentName - target component name
     * @return the ObjectName to be used to filter component registered custom MBeans
     */
    public ObjectName getComponentLoggerMBeanNameFilter(String instanceName, 
        String componentName)
    {
        StringBuffer customMBeanFilter = new StringBuffer(mDomainName); // domain name

        customMBeanFilter.append(":" + INSTANCE_NAME_KEY + "=" + instanceName);
        customMBeanFilter.append("," + COMPONENT_ID_KEY  + "=" + componentName);
        customMBeanFilter.append("," + CONTROL_TYPE_KEY + "=" + CONTROL_TYPE_LOGGER);
        customMBeanFilter.append(",*");
        
        try 
        {
            return new ObjectName(customMBeanFilter.toString());
        } 
        catch (Exception e) 
        {
            return null;
        }
    }
    
    /**
     * Returns the ObjectName of the ComponentExtension facade MBean registered for
     * the component.
     *
     * @param target - target name
     * @param componentName - target component name
     * @return the ObjectName of the component extension facade MBean.
     */
    public ObjectName getComponentExtensionFacadeMBeanName(String componentName, String target)
    {
        ObjectName extensionMBeanObjectName = null;
        
        StringBuffer objNameBuff = new StringBuffer(JBI_DOMAIN + ":");
            objNameBuff.append(TARGET_KEY + "=" + target );
            objNameBuff.append("," + COMPONENT_NAME_KEY + "=" + componentName );
            objNameBuff.append("," + SERVICE_TYPE_KEY + "=" 
                    + ComponentServiceType.Extension );
        
        try 
        {
            extensionMBeanObjectName = new ObjectName(objNameBuff.toString());
        } 
        catch (javax.management.MalformedObjectNameException exception) 
        {
            extensionMBeanObjectName = null;
        } 
        return extensionMBeanObjectName;
    }    

}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestParallelAccess.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.xml;

import com.sun.jbi.ComponentType;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.system.Util;

import java.io.File;

public class TestParallelAccess extends junit.framework.TestCase
{
     
     /**
      * Test Component Name.
      */
     private String mTstComp = null;
     private String mConfigDir = null;
     private File mRegFile;
     private File mRegBkupFile;
     String mRegFilePath;
     String mRegBkupFilePath;
     
     /**
      * Number of tests.
      */
     private static final int NUM_THREADS = 10;
     
     /**
      * The Registry.
      */
     private Registry mRegistry = null;
     
     public TestParallelAccess (String aTestName)
        throws Exception
    {
        super(aTestName);
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        mConfigDir = srcroot + manage + "/target/test-classes/testdata/";

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            mConfigDir = srcroot + manage + "/target/regress/testdata/";
        }
        
        mRegFilePath        = mConfigDir + File.separator + "jbi-registry.xml";
        mRegBkupFilePath    = mConfigDir + File.separator + "jbi-registry-backup.xml";
        mTstComp = "FileBinding";   
        
        mRegFile = new File(mRegFilePath);
        mRegBkupFile = new File(mRegBkupFilePath);
        

     }

    public void setUp()
        throws Exception
    {
        super.setUp();
        
        if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        if ( mRegBkupFile.exists())
        {
            mRegBkupFile.delete();
        }
        
        com.sun.jbi.management.registry.RegistryBuilder.destroyRegistry();
        mRegistry = Util.createRegistry(true);
        mRegistry.getUpdater().addServer("another-server");
    }

    public void tearDown()
        throws Exception
    {
               if ( mRegFile.exists())
        {
            mRegFile.delete();
        }
        if ( mRegBkupFile.exists())
        {
            mRegBkupFile.delete();
        }
        
        com.sun.jbi.management.registry.RegistryBuilder.destroyRegistry();
    }
    
   
    /**
     * Test adding components to the Registry in parallel.
     *
     * @throws Exception if an unexpected error occurs, test fails.
     */
     public void testParallelComponentAddition()
           throws Exception
    {                
        Thread[] threads = new Thread[NUM_THREADS];
        
        // -- Create the Threads
        for ( int i = 0; i < NUM_THREADS; i++ )
        {
            ComponentRunnable cr =  new ComponentRunnable(mRegistry, mTstComp + i , true);
            threads[i] = new Thread(cr);
        }

        // -- Start the Threads
        for ( int i = 0; i < NUM_THREADS; i++ )
        {
            threads[i].start();
        }
        
        // -- Wait for the Threads
        for ( int i = 0; i < NUM_THREADS; i++ )
        {
            threads[i].join();
            
        }
        
        // -- Verify Result
        java.util.List<String> comps = mRegistry.getComponentQuery("domain").getComponentIds(ComponentType.ALL);
        assertTrue( comps.size() == NUM_THREADS);
        java.util.List<String> instComps = mRegistry.getComponentQuery("another-server").getComponentIds(ComponentType.ALL);
        assertTrue( instComps.size() == NUM_THREADS);
    } 
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestRegistryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.xml;

import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ServiceAssemblyQuery;
import com.sun.jbi.management.registry.GenericQuery;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryBuilder;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.repository.ArchiveType;
 
import com.sun.jbi.management.system.Util;
import com.sun.jbi.management.system.ScaffoldEnvironmentContext;


import java.io.File;
import java.util.List;

public class TestRegistryImpl 
    extends junit.framework.TestCase
{
    /**
     * The sample Configuration Directory.
     */
    private String mConfigDir = null;
    private File mRegFile;
    private File mRegSaveFile;
    private File mRegBackupFile;
    String mRegFilePath;
    String mRegSaveFilePath;
    String mRegCorruptFilePath;
    String mRegBackupFilePath;
    String mRegErrorFolderPath;
    String mRegGoodFilePath;
    String mCompPath;
    String mSAPath;
    
    private static final String COMPONENT_NAME =
            "SunSequencingEngine";
    
    private static final String SA_NAME =
            "CompositeApplication";
    
    public TestRegistryImpl (String aTestName)
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        mConfigDir = srcroot + manage + "/target/test-classes/testdata/";

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            mConfigDir = srcroot + manage + "/target/regress/testdata/";
        }
        
        mRegFilePath        = mConfigDir + File.separator + "jbi-registry.xml";
        mRegGoodFilePath    = mConfigDir + File.separator + "jbi-registry-good.xml";
        mRegSaveFilePath    = mConfigDir + File.separator + "jbi-registry-save.xml";
        mRegCorruptFilePath = mConfigDir + File.separator + "jbi-registry-corrupt.xml";        
        mRegBackupFilePath  = mConfigDir + File.separator + "jbi-registry-backup.xml";   
        mRegErrorFolderPath = mConfigDir + File.separator + "error";
        mCompPath           = mConfigDir + File.separator + "component.zip"; 
        mSAPath             = mConfigDir + File.separator + "service-assembly.zip";
        
        mRegFile = new File(mRegFilePath);
        mRegSaveFile = new File(mRegSaveFilePath);
        mRegBackupFile = new File(mRegBackupFilePath);
        
    }

    public void setUp()
        throws Exception
    {
        super.setUp();
        
        if ( mRegSaveFile.exists() )
        {
            mRegSaveFile.delete();
        }
        Util.fileCopy(mRegGoodFilePath, mRegSaveFilePath);
    }

    public void tearDown()
        throws Exception
    {
        // -- restore registry.xml
       RegistryBuilder.destroyRegistry(); 
       Util.fileCopy(mRegSaveFilePath, mRegFilePath );
    }

    /**
     * Test Registry Creation, when the registry.xml and registry-backup.xml have
     * not been created. i.e. build the registry from scratch.
     *
     * @throws Exception if an unexpected error occurs
     */
    public void testRegistryCreation()
           throws Exception
    {
        if ( mRegFile.exists() )
        {
            mRegFile.delete();
        }
        
        if ( mRegBackupFile.exists() )
        {
            mRegBackupFile.delete();
        }
        Registry reg = Util.createRegistry(true);

        assertNotNull( reg ); 
    }
        
    /**
     * The repository is empty so all the registered entities in the registry
     * are expected to be cleaned up. This tests the cleanup logic when the registry
     * has registered entities which are missing in the repository.
     */
    public void testInitializingRegistryWithRegistryCleanup()
        throws Exception
    {   
        // -- This should cause the registry to be cleaned up, since the repository
        // -- is empty.
        Registry reg = Util.createRegistry(false);
        
        GenericQuery genQuery = reg.getGenericQuery();
        
        List<String> sas        = genQuery.getRegisteredServiceAssemblies();
        List<String> components = genQuery.getRegisteredComponents();
        List<String> sls        = genQuery.getRegisteredSharedLibraries();
        
        assertTrue(sas.isEmpty());
        assertTrue(components.isEmpty());
        assertTrue(sls.isEmpty());
    }
    
    /**
     * The repository in this case will have only one component. When the registry is
     * initialized all the registered entities in the registry
     * are expected to be cleaned up except the SunSequencingEngine component. 
     * This tests the cleanup logic when the registry has registered entities 
     * most of which are missing in the repository, except one component.
     */
    public void testInitializingRegistryWithPartialRegistryCleanup()
        throws Exception
    {   
        Repository repos = Util.createManagementContext().getRepository();
        repos.addArchive(ArchiveType.COMPONENT, mCompPath);
        
        // -- This should cause the registry to be cleaned up, since the repository
        // -- is empty.
        Registry reg = Util.createRegistry(false);
        
        GenericQuery genQuery = reg.getGenericQuery();
        
        List<String> sas        = genQuery.getRegisteredServiceAssemblies();
        List<String> components = genQuery.getRegisteredComponents();
        List<String> sls        = genQuery.getRegisteredSharedLibraries();
        
        // -- Verify : Registry should only have the SunSequencingEngine
        assertTrue(sas.isEmpty());
        assertFalse(components.isEmpty());
        assertTrue( components.contains(COMPONENT_NAME) );
        assertTrue(sls.isEmpty());
        
        String[] targets = new String[]{"server", "clusterA"};
        // -- The component should not have any service units, since the SA is cleaned up
        for ( String target : targets )
        {
            ComponentQuery compQuery = reg.getComponentQuery(target);
            List sus = compQuery.getComponentInfo(COMPONENT_NAME).getServiceUnitList();
            assertTrue(sus.isEmpty());
        }
        
        // -- The service assembly ref from the instance should also be cleaned up
        for ( String target : targets )
        {
            ServiceAssemblyQuery saQuery = reg.getServiceAssemblyQuery(target);
            List<String> serverSas = saQuery.getServiceAssemblies();
            assertTrue(serverSas.isEmpty());
        }
        
        repos.purge();
    }
    
    /**
     * The repository in this case will have a service assemmbly which is deployed to
     * various instances in the registry. The target components are missing in the repos.
     * When the registry is initialized the orphaned service assembly should be removed 
     * from all the instances. The repository should still have the assembly.
     *
     * This tests the cleanup logic when the registry has orphan service assemblies in
     * the server/cluster refs.
     */
    public void testInitializingRegistryWithOrphanedServiceAssembly()
        throws Exception
    {   
        Repository repos = Util.createManagementContext().getRepository();
        repos.addArchive(ArchiveType.SERVICE_ASSEMBLY, mSAPath);
        
        // -- This should cause the components from the registry to be cleaned up
        Registry reg = Util.createRegistry(false);
        
        GenericQuery genQuery = reg.getGenericQuery();
        
        List<String> sas        = genQuery.getRegisteredServiceAssemblies();
        List<String> components = genQuery.getRegisteredComponents();
        List<String> sls        = genQuery.getRegisteredSharedLibraries();
        
        // -- Verify : Registry should only have the CompositApplication
        assertFalse(sas.isEmpty());
        assertTrue(sas.contains(SA_NAME));
        assertTrue(components.isEmpty());
        assertTrue(sls.isEmpty());
        
        // -- The orphaned service assembly should be removed from all instances
        String[] targets = new String[]{"server", "clusterA"};
        for ( String target : targets )
        {
            ServiceAssemblyQuery saQuery = reg.getServiceAssemblyQuery(target);
            List<String> serverSas = saQuery.getServiceAssemblies();
            assertTrue(serverSas.isEmpty());
        }
        
        repos.purge();
    }
    
    /**
     * This tests the case when a service assembly is there in the repository,
     * but not the registry. The repository should be cleanedup.
     */
    public void testInitializingRegistryWithRepositoryCleanup()
        throws Exception
    {
        
        Repository repos = Util.createManagementContext().getRepository();
        repos.addArchive(ArchiveType.SERVICE_ASSEMBLY, mSAPath);
        
        // Verify : service assembly added properly
        List<String> sas = repos.getArchiveEntityNames(ArchiveType.SERVICE_ASSEMBLY);
        assertFalse(sas.isEmpty());
        assertTrue(sas.contains(SA_NAME));
        
        // -- Create an empty registry
        if ( mRegFile.exists() )
        {
            mRegFile.delete();
        }
        
        if ( mRegBackupFile.exists() )
        {
            mRegBackupFile.delete();
        }
        Registry reg = Util.createRegistry(false);
        
        // -- Verify : Repository should be clean
        sas = repos.getArchiveEntityNames(ArchiveType.SERVICE_ASSEMBLY);
        List<String> components = repos.getArchiveEntityNames(ArchiveType.COMPONENT);
        List<String> sls = repos.getArchiveEntityNames(ArchiveType.SHARED_LIBRARY);
        assertTrue(sas.isEmpty());
        assertTrue(components.isEmpty());
        assertTrue(sls.isEmpty());
        
        repos.purge();
    }
    
    /**
     * Test reading from a bad registry and a missing-backup. The expected
     * result is a failure to initialize the registry.
     */
    public void testReadingCorruptRegistry()
        throws Exception
    {
        
        if ( mRegFile.exists() )
        {
            mRegFile.delete();
        }
        Util.fileCopy(mRegCorruptFilePath, mRegFilePath);

        try
        {
            Registry reg = Util.createRegistry(true);
            fail();
        }
        catch( RegistryException rex )
        {
            File errorFolder = new File(mRegErrorFolderPath);
            assertTrue(errorFolder.exists());
            assertTrue(errorFolder.list().length != 0 );
        }
    }
    
    
    /**
     * Test reading from a missing registry file when a backup file exists.
     * Expected result is a successful to initialization of the registry.
     */
    public void testInitializingRegistryFromBackup()
        throws Exception
    {
        Util.fileCopy(mRegGoodFilePath, mRegBackupFilePath);   
        Registry reg = Util.createRegistry(true);
        assertNotNull( reg );
    }
    
    /**
     * Test reading from a missing registry file when a backup file exists, but is
     * corrupted. Expected result is a failure to initialize the registry.
     */
    public void testInitializingRegistryFromBadBackup()
        throws Exception
    {       
        if ( mRegFile.exists() )
        {
            mRegFile.delete();
        }
        Util.fileCopy(mRegCorruptFilePath, mRegBackupFilePath);
        
        try
        {
            Registry reg = Util.createRegistry(true);
            fail(" A Registry Exception was expected.");
        }
        catch(RegistryException rex)
        {
            System.out.println(rex.getMessage());
        }
    }
    

}

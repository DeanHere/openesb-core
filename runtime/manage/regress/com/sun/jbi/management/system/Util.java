/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Util.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  Util.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 17, 2004, 2:26 PM
 */

package com.sun.jbi.management.system;

import java.io.File;
import java.util.Properties;
import java.util.HashMap;

import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryBuilder;
import com.sun.jbi.management.registry.RegistryType;
import com.sun.jbi.management.registry.RegistrySpecImpl;
import com.sun.jbi.management.registry.xml.RegistryImpl;
import com.sun.jbi.management.repository.Repository;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class Util
{
    
    /**
     * @param packageName is the pkgname.
     * @return  a String Translator. 
     */
    public static com.sun.jbi.StringTranslator getStringTranslator (String packageName)
    {
        Class clazz;
        java.lang.reflect.Constructor ctor;
        try
        {
            clazz = Class.forName("com.sun.jbi.management.StringTranslator");
            ctor = clazz.getDeclaredConstructors()[0];
            ctor.setAccessible(true);
            return (com.sun.jbi.StringTranslator)
                ctor.newInstance(new Object[] {packageName, null});
                    //this.getClass().getClassLoader()});
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }    
        
    }

    /**
     * Make a copy of the source file in the destination file.
     * @return true if the operation was a success, false otherwise
     */
    public static boolean fileCopy(String source, String destination)
        throws java.io.IOException
    {
        File sourceFile = new File(source);
        File destFile   = new File(destination);       

        if ( !destFile.exists() )
        {
            int len = (int) sourceFile.length ();
            byte[] buf = new byte[len];
            java.io.FileInputStream fis = new java.io.FileInputStream(sourceFile);
            fis.read(buf, 0, len);
            if ( fis != null )
            {
                fis.close();
            }

            java.io.FileOutputStream fos = new java.io.FileOutputStream(destFile);
            fos.write (buf, 0, len);
            if ( fos != null )
            {
                fos.flush();
                fos.close();
            } 
            return true;
        }
        else
        {
            return false;
        }
    }  
    
    
    /**
     * Create a Registry Instance.
     *
     * @return a Registry Instance
     * @throws Exception if an unexpected error occurs
     */
    public static Registry createRegistry()
           throws Exception
    {    
        return createRegistry(getRegistryDirPath(), false);
    }
    
    
    /**
     * Create a Registry Instance.
     *
     * @return a Registry Instance
     * @throws Exception if an unexpected error occurs
     */
    public static Registry createRegistry(boolean readOnly)
           throws Exception
    {    
        return createRegistry(getRegistryDirPath(), readOnly);
    }
    
    /**
     * Create a Registry Instance.
     *
     * @return a Registry Instance
     * @throws Exception if an unexpected error occurs
     */
    public static Registry createRegistry(String regFolder)
           throws Exception
    {
        return createRegistry(regFolder, false);
    }
    /**
     * Create a Registry Instance.
     *
     * @return a Registry Instance
     * @throws Exception if an unexpected error occurs
     */
    public static Registry createRegistry(String regFolder, boolean readOnly)
           throws Exception
    {
        try 
        {
            
            // -- Create a XML Registry Spec.
            Properties props = new Properties();
            props.setProperty(Registry.REGISTRY_FOLDER_PROPERTY, regFolder );
            if ( readOnly )
            {
                props.setProperty(Registry.REGISTRY_READONLY_PROPERTY, Boolean.toString(readOnly) );
            }
            props.setProperty(Registry.REGISTRY_LOCK_INTERVAL_PROPERTY, "120" );
            return RegistryBuilder.buildRegistry( new RegistrySpecImpl(
                RegistryType.XML, props, createManagementContext()));
        }
        catch (Exception aEx)
        {
            aEx.printStackTrace();
            throw aEx;
        }
    }
    
    
    /**
     * @return the path to the Registry Folder
     */
    public static String getRegistryDirPath()
    {
        String srcroot = System.getProperty("junit.srcroot");
            
        java.io.File f = new java.io.File(srcroot + "/runtime/manage");
        if (! f.exists())
        {
            // -- mainline/whitney build
            return srcroot + "/shasta/manage/target/regress/testdata";       
        }
        else
        {
            // -- open-esb build
            return srcroot + "/runtime/manage/target/test-classes/testdata";    
        }
    }
    
    public static ManagementContext createManagementContext()
        throws Exception
    {
        ScaffoldEnvironmentContext envCtx = new ScaffoldEnvironmentContext();
        com.sun.jbi.util.EnvironmentAccess.setContext(envCtx);
        String testPath = System.getProperty("junit.srcroot") + "/runtime/manage/target/";
        envCtx.setJbiInstanceRoot(testPath);
        envCtx.setAppServerInstanceRoot(testPath);
        envCtx.setJbiInstallRoot(System.getProperty("junit.as8base") + "/jbi");
        ManagementContext ctx =  new ManagementContext(envCtx);
        Repository repository = new Repository(ctx);
        
        ctx.setRepository(repository);
        
        return ctx;
       
    }
}

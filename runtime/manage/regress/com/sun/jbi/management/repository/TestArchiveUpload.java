/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestArchiveUpload.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

import com.sun.jbi.management.system.ScaffoldEnvironmentContext;
import com.sun.jbi.management.system.ManagementContext;

import java.io.File;
import java.io.FileInputStream;

public class TestArchiveUpload extends junit.framework.TestCase
{
    private static final String JBI_ROOT_PATH =
            "/target";
    
    private static final String COMPONENT_ARCHIVE_PATH =
            "/testdata/component.zip";
    
    private static final String COMPONENT_ARCHIVE_NAME =
            "SunSequencingEngine";
    
    private ManagementContext       mCtx;
    private String                  mJbiRoot;
    private ArchiveUpload           mUploader;
            
    public TestArchiveUpload (String aTestName)
        throws Exception
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        String blddir = "/target/test-classes";          // open-esb build

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            blddir = "/target/regress";         // mainline/whitney build
        }

        mJbiRoot = srcroot + manage + JBI_ROOT_PATH;

        ScaffoldEnvironmentContext envCtx = new ScaffoldEnvironmentContext();
        envCtx.setJbiInstanceRoot(mJbiRoot);
        envCtx.setAppServerInstanceRoot(mJbiRoot);
        mJbiRoot = srcroot + manage + blddir;
        mCtx = new ManagementContext(envCtx);
        mCtx.setRepository(new Repository(mCtx));
        
        mUploader = new ArchiveUpload(mCtx);
    }
    
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mCtx.getRepository().purge();
    }

    public void tearDown()
        throws Exception
    {
    }
    
    public void testUploadArchive()
        throws Exception
    {
        Object          upId;
        FileInputStream fis;
        byte[]          buf;
        int             count;
        File            archive;
        
        buf  = new byte[4096];
        fis  = new FileInputStream(mJbiRoot + COMPONENT_ARCHIVE_PATH);
        upId = mUploader.initiateUpload("component.zip");
        
        while ((count = fis.read(buf)) != -1)
        {
            if (buf.length != count)
            {
                byte[] tmp = new byte[count];
                System.arraycopy(buf, 0, tmp, 0, count);
                buf = tmp;
            }
            mUploader.uploadBytes(upId, buf);
        }
        
        assertNotNull(mUploader.getArchiveFilePath(upId));
        mUploader.terminateUpload(upId, System.currentTimeMillis()); 
        
        archive = new File(mUploader.getArchiveFilePath(upId));
        assertTrue(mUploader.removeArchive(upId));    
    }
}

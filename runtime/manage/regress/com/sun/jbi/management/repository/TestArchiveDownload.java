/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestArchiveDownload.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

import com.sun.jbi.management.system.ScaffoldEnvironmentContext;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.system.Util;

import java.io.File;
import java.io.FileOutputStream;

public class TestArchiveDownload extends junit.framework.TestCase
{
    private static final String JBI_ROOT_PATH =
            "/target";
    
    private static final String TEMP_FILE =
            "/tmp/download.zip";
    
    private static final String COMPONENT_ARCHIVE_PATH =
            "/testdata/component.zip";
    
    private static final String COMPONENT_ARCHIVE_NAME =
            "SunSequencingEngine";
    
    private ManagementContext       mCtx;
    private String                  mJbiRoot, mTestClassesRoot;
    private ArchiveDownload         mDownloader;
            
    public TestArchiveDownload (String aTestName)
        throws Exception
    {
        super(aTestName);
        
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        String blddir = "/target/test-classes";          // open-esb build

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            blddir = "/target/regress";         // mainline/whitney build
        }

        mJbiRoot = srcroot + manage + JBI_ROOT_PATH;
        mTestClassesRoot = srcroot + manage + blddir;

        //this is a bad dependency on ManagementContext. 
        //This should be fixed when validation logic is removed from Archive
        mCtx = Util.createManagementContext();        
        
        mDownloader = new ArchiveDownload(mCtx);
    }
    
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mCtx.getRepository().purge();
    }

    public void tearDown()
        throws Exception
    {
    }
    
    public void testDownloadArchive()
        throws Exception
    {
        Archive             archive;
        Object              downId;
        FileOutputStream    fos;
        byte[]              buf;
                
        // add the archive we are about to fetch
        archive = mCtx.getRepository().addArchive(ArchiveType.COMPONENT, 
                    mTestClassesRoot + COMPONENT_ARCHIVE_PATH);
        
        // start the download session
        downId = mDownloader.initiateDownload(archive.getPath());
        
        // grab the bytes
        fos = new FileOutputStream(mJbiRoot + TEMP_FILE);
        buf = mDownloader.downloadBytes(downId, 4096);        
        while (buf.length > 0)
        {
            fos.write(buf);
            buf = mDownloader.downloadBytes(downId, 4096);
        }
        
        mDownloader.terminateDownload(downId);
        
        fos.flush();
        fos.close();
        
        File tmpFile = new File(mJbiRoot, TEMP_FILE);
        assertTrue(tmpFile.exists());     
        assertTrue(tmpFile.length() > 0);
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDirectoryUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;

import com.sun.jbi.management.util.FileHelper;

import java.io.File;
import java.io.FileInputStream;

public class TestJarFactory 
        extends junit.framework.TestCase
{
    private final static String TEST_ROOT = System.getProperty("junit.srcroot") + 
            "/runtime/manage/target/test-classes/jar-tmp";
    
    private final static String SRC_JAR = System.getProperty("junit.srcroot") + 
            "/runtime/manage/regress/testdata/jmsbinding.jar";
    
    private final static String DST_JAR = TEST_ROOT
            + File.separator + "\u4e2d\u6587\u76ee\u5f55.jar";

    
    private File mTestRoot;    
    
    public TestJarFactory(String aTestName)
        throws Exception
    {
        super(aTestName);
        mTestRoot = new File(TEST_ROOT); 
        
        if ( !mTestRoot.exists())
        {
            mTestRoot.mkdir();
        }
        
        
    }
    
    public void setUp()
        throws Exception
    {
        super.setUp();   

        // -- Copy the service assembly zip to the test root and
        //    rename it to "\u4e2d\u6587\u76ee\u5f55.jar"
        FileHelper.fileCopy(SRC_JAR, DST_JAR, true);
 
    }

    public void tearDown()
        throws Exception
    {
        // Cleanup the test folder
        FileHelper.cleanDirectory(mTestRoot);
    }
    
    /** 
     * Test extracting an archive which has ideographic characters in its name.
     */
    public void testUnJar()
        throws Exception
    {
        JarFactory jf = new JarFactory(TEST_ROOT);
        //String fileName = "D:\\nikita\\dev\\jsr208\\jbinew\\open-esb\\install\\as8\\domains\\CAS\\jbi\\service-assemblies\\CompositeApp1\\CompositeApp1-\u4e2d\u6587\u76ee\u5f55\\\u4e2d\u6587\u76ee\u5f55.jar";
        File file = new File(DST_JAR);
        jf.unJar(file);
        
        // Verify
        File descrFile = new File(TEST_ROOT + "/META-INF/jbi.xml");
        assertTrue(descrFile.exists());
    }    
    
   
    
}

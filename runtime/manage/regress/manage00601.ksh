#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00601.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#manage00601 - test partial deployment functionality.

echo testname is manage00601
. ./regress_defs.ksh

# package component and service assembly archives
ant -q -emacs -propertyfile "$JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f manage00601.xml

# install the test binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist/deploy-binding1.jar install-component

# start the binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=manage-binding-1 start-component

# deploy the service assembly
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.deploy.file=$JV_SVC_TEST_CLASSES/dist/deploy-sa.jar deploy-service-assembly

# start the service assembly
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.service.assembly.name="manage00601-sa" start-service-assembly

# stop the service assembly
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.service.assembly.name="manage00601-sa" stop-service-assembly

# shutdown the service assembly
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.service.assembly.name="manage00601-sa" shut-down-service-assembly

# undeploy the service assembly
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.service.assembly.name="manage00601-sa" undeploy-service-assembly

# stop the binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=manage-binding-1 stop-component

# shutdown the binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=manage-binding-1 shut-down-component

# uninstall the binding
asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name=manage-binding-1 uninstall-component

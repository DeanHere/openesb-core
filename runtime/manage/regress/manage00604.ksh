#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00604.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#manage00604 - test added with fix for issue tracker #43

echo testname is manage00604
. ./regress_defs.ksh

#note - this directory normally created in 601 ant script:
mkdir -p $JV_SVC_TEST_CLASSES/dist
#this prevents permission failures on subsequent runs:
chmod +w $JV_SVC_TEST_CLASSES/dist/*

cp $JV_SVC_REGRESS/testdata/sequencingSA1.zip $JV_SVC_TEST_CLASSES/dist 2>&1
cp $JV_SVC_REGRESS/testdata/sequencingSA2.zip $JV_SVC_TEST_CLASSES/dist 2>&1
cp $JV_SVC_REGRESS/testdata/sequencingengine.jar $JV_SVC_TEST_CLASSES/dist 2>&1

asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/sequencingengine.jar 2>&1
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT  SunSequencingEngine 2>&1

asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/sequencingSA1.zip 2>&1

echo "Expect failure"
asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/sequencingSA2.zip 2>&1

asadmin undeploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sea51feb-0fc3-4ff6-9104-cfd59d2a7815 2>&1

asadmin stop-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT  SunSequencingEngine 2>&1
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT  SunSequencingEngine 2>&1
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT SunSequencingEngine 2>&1

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdministrationServiceMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.administration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import com.sun.esb.management.api.administration.AdministrationService;
import com.sun.esb.management.base.services.AbstractListStateServiceMBeansImpl;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.JBIDescriptor;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.runtime.GenericsSupport;

/**
 * Defines MBean operations for common administration services.
 * 
 * @author graj
 */
public class AdministrationServiceMBeanImpl extends
AbstractListStateServiceMBeansImpl implements AdministrationService,
Serializable {
    
    static final long serialVersionUID = -1L;
    
    /**
     * Constructor - Constructs a new instance of AdministrationServiceMBeanImpl
     * 
     * @param anEnvContext
     */
    public AdministrationServiceMBeanImpl(EnvironmentContext anEnvContext) {
        super(anEnvContext);
    }
    
    /**
     * Returns a map of target names to an array of target instance names.
     * In the case cluster targets, the key contains the cluster target name, 
     * and the the value contains an array of the "target instance" names.
     * If it is not a cluster target, the key contains the targetName, and 
     * the value is null.
     * 
     * @return map of target names to array of target instance names
     * @throws ManagementRemoteException
     */
    public Map<String /*targetName*/, String[] /*targetInstanceNames*/> listTargetNames() throws ManagementRemoteException {
        return super.listTargetNames();
    }
    
    /**
     * Retrieve runtime version information
     * @return map of targetName to Properties elements detailing version info
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    public Map<String /*targetName*/, Properties /*version information*/> getRuntimeDetails() throws ManagementRemoteException {
        Map<String /*targetName*/, Properties /*version information*/> result = null;
        result = new HashMap<String /*targetName*/, Properties /*version information*/>();
        Map<String /*targetName*/, String[] /*targetInstanceNames*/> targetNameToInstancesMap = null; 
        targetNameToInstancesMap = listTargetNames();
        
        if(targetNameToInstancesMap != null) {
            for(String targetName : targetNameToInstancesMap.keySet()) {
                ObjectName objectName = null;
                String value = null;
                Properties properties = new Properties();
                if(targetName.equals(JBIAdminCommands.SERVER_TARGET_KEY) == true) {
                    objectName = super.getAdminServiceMBeanObjectName();
                    if(objectName != null) {
                        value = (String)this.getAttributeValue(objectName, SYSTEM_INFORMATION_KEY);
                        if(value != null) {
                            properties.setProperty(SYSTEM_INFORMATION_KEY, value);
                        }
                        value = (String)this.getAttributeValue(objectName, BUILD_NUMBER_KEY);
                        if(value != null) {
                            properties.setProperty(BUILD_NUMBER_KEY, value);
                        }
                        value = (String)this.getAttributeValue(objectName, COPYRIGHT_KEY);
                        if(value != null) {
                            properties.setProperty(COPYRIGHT_KEY, value);
                        }
                        value = (String)this.getAttributeValue(objectName, FULL_PRODUCT_NAME_KEY);
                        if(value != null) {
                            properties.setProperty(FULL_PRODUCT_NAME_KEY, value);
                        }
                        value = (String)this.getAttributeValue(objectName, MAJOR_VERSION_KEY);
                        if(value != null) {
                            properties.setProperty(MAJOR_VERSION_KEY, value);
                        }
                        value = (String)this.getAttributeValue(objectName, MINOR_VERSION_KEY);
                        if(value != null) {
                            properties.setProperty(MINOR_VERSION_KEY, value);
                        }
                        value = (String)this.getAttributeValue(objectName, SHORT_PRODUCT_NAME_KEY);
                        if(value != null) {
                            properties.setProperty(SHORT_PRODUCT_NAME_KEY, value);
                        }
                        result.put(targetName, properties);
                    }
                } else {
                    objectName = this.getAdminServiceMBeanObjectName(targetName);
                    if(objectName != null) {
                        value = (String)this.getAttributeValue(objectName, SYSTEM_INFORMATION_KEY);
                        if(value != null) {
                            properties.setProperty(SYSTEM_INFORMATION_KEY, value);
                        }
                        result.put(targetName, properties);
                    }
                }
            }
        }
        
        return result;
    }
    
    
    
    /**
     * Checks to see if the Target (server, cluster) is up or down.
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return true if Target is up, false if not
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#isTargetUp(java.lang.String)
     */
    public boolean isTargetUp(String targetName)
    throws ManagementRemoteException {
        return super.isTargetUp(targetName);
    }
    
    
    /**
     * Retrieve the Component Installation descriptor for the server target.
     * 
     * The Installation descriptor will never be different for a component name
     * in a JBI system because there's only one component allowed with that same
     * name. Therefore, this implies a registry lookup for the server target.
     * 
     * @param Component
     *            name as a string
     * @return the Component Installation descriptor as a string
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getComponentInstallationDescriptor(java.lang.String)
     */
    public String getComponentInstallationDescriptor(String componentName)
    throws ManagementRemoteException {
        // The deployment descriptor will never be different for a
        // component name in a JBI system because there's
        // only one component allowed with that same name.
        // Therefore, this implies a registry lookup for the domain target.
        String targetName = JBIAdminCommands.DOMAIN_TARGET_KEY;
        // String targetName = JBIAdminCommands.SERVER_TARGET_KEY;
        String jbiXml = this.getInstallationDescriptor(componentName,
                targetName);
        return jbiXml;
    }
    
    /**
     * Gets the component state
     * 
     * @param componentName
     * @param targetName
     * @return the state of the component
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getComponentState(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public String getComponentState(String componentName, String targetName)
    throws ManagementRemoteException {
        return super.getComponentState(componentName, targetName);
    }
    
    /**
     * Gets the component type
     * 
     * @param componentName
     * @param targetName
     * @return the type of the component (binding-component, service-engine, or
     *         shared-library)
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getComponentType(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public String getComponentType(String componentName, String targetName)
    throws ManagementRemoteException {
        String result = null;
        String xmlText = null;
        List<JBIComponentInfo> infoList = null;
        String state = null, sharedLibraryName = null, serviceAssemblyName = null;
        try {
            xmlText = this.listComponents(ComponentType.BINDING, state,
                    sharedLibraryName, serviceAssemblyName, targetName);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = JBIComponentInfo.readFromXmlText(xmlText);
                for (JBIComponentInfo info : infoList) {
                    if (info.getName().equals(componentName) == true) {
                        return info.getType();
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore exception
        }
        try {
            xmlText = this.listComponents(ComponentType.ENGINE, state,
                    sharedLibraryName, serviceAssemblyName, targetName);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = JBIComponentInfo.readFromXmlText(xmlText);
                for (JBIComponentInfo info : infoList) {
                    if (info.getName().equals(componentName) == true) {
                        return info.getType();
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore exception
        }
        try {
            // ////////////////////////////////////////
            // Start Check to make sure target is valid
            // ////////////////////////////////////////
            ObjectName installerServiceObjectName = this
            .getInstallationServiceMBeanObjectName(targetName);
            this.checkForValidTarget(installerServiceObjectName, targetName);
            // ////////////////////////////////////////
            // End Check to make sure target is valid
            // ////////////////////////////////////////
            
            List frameworkCompInfoList = this
            .getFrameworkComponentInfoListForSharedLibraries(null,
                    targetName);
            List uiCompInfoList = new ArrayList();
            uiCompInfoList = this.toUiComponentInfoList(frameworkCompInfoList,
                    targetName);
            xmlText = JBIComponentInfo.writeAsXmlText(uiCompInfoList);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = JBIComponentInfo.readFromXmlText(xmlText);
                for (JBIComponentInfo info : infoList) {
                    if (info.getName().equals(componentName) == true) {
                        return info.getType();
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore exception
        }
        return result;
    }
    
    /**
     * Get the list of consuming endpoints
     * 
     * @param componentName
     * @param targetName
     * @return Returns the consumingEndpoints.
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getConsumingEndpoints(java.lang.String,
     *      java.lang.String)
     */
    public String[] getConsumingEndpoints(String componentName,
            String targetName) throws ManagementRemoteException {
        List<String> endpointsList = new ArrayList<String>();
        
        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        targetToObjectNamesMap = findLiveExtensionMBeanObjectNames(componentName, CUSTOM_STATISTICS_NAME_KEY, targetName);
        if(targetToObjectNamesMap != null) {
            for(String instanceName : targetToObjectNamesMap.keySet()) {
                ObjectName[] objectNames = targetToObjectNamesMap.get(instanceName);
                if (objectNames != null) {
                    for(ObjectName objectName : objectNames) {
                        String[] consumingEndpoints = null;
                        consumingEndpoints = (String[]) getAttributeValue(targetName,
                                objectName, "ConsumingEndpoints");
                        if(consumingEndpoints != null) {
                            for(String endpoint : consumingEndpoints) {
                                if(endpointsList.contains(endpoint) == false) {
                                    endpointsList.add(endpoint);
                                }
                            }
                        }
                    }
                }
            }
        }
        return GenericsSupport.toArray(endpointsList, String.class);

    }
    
    /**
     * Get the list of provisioning endpoints
     * 
     * @param componentName
     * @param targetName
     * @return Returns the provisioningEndpoints.
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getProvisioningEndpoints(java.lang.String,
     *      java.lang.String)
     */
    public String[] getProvisioningEndpoints(String componentName,
            String targetName) throws ManagementRemoteException {
        List<String> endpointsList = new ArrayList<String>();
        
        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        targetToObjectNamesMap = findLiveExtensionMBeanObjectNames(componentName, CUSTOM_STATISTICS_NAME_KEY, targetName);
        if(targetToObjectNamesMap != null) {
            for(String instanceName : targetToObjectNamesMap.keySet()) {
                ObjectName[] objectNames = targetToObjectNamesMap.get(instanceName);
                if (objectNames != null) {
                    for(ObjectName objectName : objectNames) {
                        String[] provisioningEndpoints = null;
                        provisioningEndpoints = (String[]) getAttributeValue(targetName,
                                objectName, "ProvisioningEndpoints");
                        if(provisioningEndpoints != null) {
                            for(String endpoint : provisioningEndpoints) {
                                if(endpointsList.contains(endpoint) == false) {
                                    endpointsList.add(endpoint);
                                }
                            }
                        }
                    }
                }
            }
        }
        return GenericsSupport.toArray(endpointsList, String.class);
    }
    
    /**
     * Retrieve the Service Assembly Deployment descriptor for the domain
     * target.
     * 
     * The deployment descriptor will never be different for a service assembly
     * name in a JBI system because there's only one Service Assembly allowed
     * with that same name. Therefore, this implies a registry lookup for the
     * domain target.
     * 
     * @param service
     *            assembly name as a string
     * @return the service assembly deployment descriptor as a string
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getServiceAssemblyDeploymentDescriptor(java.lang.String)
     */
    public String getServiceAssemblyDeploymentDescriptor(
            String serviceAssemblyName) throws ManagementRemoteException {
        // The deployment descriptor will never be different for a
        // service assembly name in a JBI system because there's
        // only one Service Assembly allowed with that same name.
        // Therefore, this implies a registry lookup for the domain target.
        String targetName = JBIAdminCommands.DOMAIN_TARGET_KEY;
        
        ObjectName deploymentServiceObjectName = this
        .getDeploymentServiceMBeanObjectName(targetName);
        
        logDebug("Calling deploy on DeploymentServiceMBean = "
                + deploymentServiceObjectName);
        
        Object resultObject = this.invokeMBeanOperation(
                deploymentServiceObjectName, "getServiceAssemblyDescriptor",
                serviceAssemblyName);
        
        if (resultObject != null) {
            return resultObject.toString();
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.deploy.error", null, null);
            throw new ManagementRemoteException(exception);
        }
    }
    
    /**
     * Gets the Service Assembly state
     * 
     * @param serviceAssemblyName
     * @param targetName
     * @return the state of the Service Assembly
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getServiceAssemblyState(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public String getServiceAssemblyState(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        String result = ServiceAssemblyInfo.UNKNOWN_STATE;
        String xmlText = null;
        List<ServiceAssemblyInfo> infoList = null;
        String state = null, componentName = null;
        try {
            // ////////////////////////////////////////
            // Start Check to make sure target is valid
            // ////////////////////////////////////////
            ObjectName installerServiceObjectName = this
            .getInstallationServiceMBeanObjectName(targetName);
            this.checkForValidTarget(installerServiceObjectName, targetName);
            // ////////////////////////////////////////
            // End Check to make sure target is valid
            // ////////////////////////////////////////
            
            this.validateUiServiceAssemblyInfoState(state);
            
            List saInfoList = this.getServiceAssemblyInfoList(
                    toFrameworkServiceAssemblyState(state), componentName,
                    targetName);
            
            xmlText = ServiceAssemblyInfo.writeAsXmlTextWithProlog(saInfoList);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = ServiceAssemblyInfo
                .readFromXmlTextWithProlog(xmlText);
                for (ServiceAssemblyInfo info : infoList) {
                    if (info.getName().equals(serviceAssemblyName) == true) {
                        return info.getState();
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore exception
        }
        return result;
    }
    
    /**
     * Returns the jbi.xml Deployment Descriptor for a Service Unit.
     * 
     * @param the
     *            name of the Service Assembly
     * @param the
     *            name of the Service Unit
     * @return the jbi.xml deployment descriptor of the archive
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getServiceUnitDeploymentDescriptor(java.lang.String,
     *      java.lang.String)
     */
    public String getServiceUnitDeploymentDescriptor(
            String serviceAssemblyName, String serviceUnitName)
    throws ManagementRemoteException {
        // The deployment descriptor will never be different for a
        // service assembly name in a JBI system because there's
        // only one Service Assembly allowed with that same name.
        // Therefore, this implies a registry lookup for the domain target.
        String targetName = JBIAdminCommands.DOMAIN_TARGET_KEY;
        
        ObjectName deploymentServiceObjectName = this
        .getDeploymentServiceMBeanObjectName(targetName);
        
        logDebug("Calling deploy on DeploymentServiceMBean = "
                + deploymentServiceObjectName);
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = serviceUnitName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        Object resultObject = invokeMBeanOperation(deploymentServiceObjectName,
                "getServiceUnitDescriptor", params, signature);
        
        if (resultObject != null) {
            return resultObject.toString();
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.deploy.error", null, null);
            throw new ManagementRemoteException(exception);
        }
    }
    
    /**
     * Retrieve the Shared Library Installation descriptor for the server
     * target.
     * 
     * The Installation descriptor will never be different for a Shared Library
     * name in a JBI system because there's only one Shared Library allowed with
     * that same name. Therefore, this implies a registry lookup for the server
     * target.
     * 
     * @param Shared
     *            Library name as a string
     * @return the Shared Library Installation descriptor as a string
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getSharedLibraryInstallationDescriptor(java.lang.String)
     */
    public String getSharedLibraryInstallationDescriptor(
            String sharedLibraryName) throws ManagementRemoteException {
        // The deployment descriptor will never be different for a
        // Shared Library name in a JBI system because there's
        // only one Shared Library allowed with that same name.
        // Therefore, this implies a registry lookup for the domain target.
        String targetName = JBIAdminCommands.DOMAIN_TARGET_KEY;
        // String targetName = JBIAdminCommands.SERVER_TARGET_KEY;
        String jbiXml = this.getSharedLibraryDescriptor(sharedLibraryName,
                targetName);
        return jbiXml;
    }
    
    /**
     * Retrieves the primary WSDL associated with the specified endpoint.
     * 
     * @param componentName
     * @param endpoint
     * @param targetName
     * @return primary WSDL associated with the endpoint
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getWSDLDefinition(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String getWSDLDefinition(String componentName, String endpoint,
            String targetName) throws ManagementRemoteException {
        String wsdlDefinition = "";
        
        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        targetToObjectNamesMap = findLiveExtensionMBeanObjectNames(componentName, CUSTOM_STATISTICS_NAME_KEY, targetName);
        if(targetToObjectNamesMap != null) {
            for(String instanceName : targetToObjectNamesMap.keySet()) {
                ObjectName[] objectNames = targetToObjectNamesMap.get(instanceName);
                if (objectNames != null) {
                    for(ObjectName objectName : objectNames) {
                        Object[] params = new Object[1];
                        params[0] = endpoint;
                        
                        String[] signature = new String[1];
                        signature[0] = "java.lang.String";
                        
                        Object resultObject = this.invokeMBeanOperation(targetName,
                                objectName, "getWSDLDefinition", params, signature);
                        
                        if (resultObject != null) {
                            wsdlDefinition = resultObject.toString();
                            if((wsdlDefinition != null) && (wsdlDefinition.length() > 0)) {
                                break;
                            }
                        }
                    }
                }
                if((wsdlDefinition != null) && (wsdlDefinition.length() > 0)) {
                    break;
                }
            }
        }
        return wsdlDefinition;
        
    }
    
    /**
     * Retrieves the WSDL or XSD associated with the specified endpoint and
     * targetNamespace
     * 
     * @param componentName
     * @param endpoint
     * @param targetNamespace
     * @param targetName
     * @return wsdl or xsd
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getWSDLImportedResource(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String getWSDLImportedResource(String componentName,
            String endpoint, String targetNamespace, String targetName)
    throws ManagementRemoteException {
        String importedResource = "";

        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        targetToObjectNamesMap = findLiveExtensionMBeanObjectNames(componentName, CUSTOM_STATISTICS_NAME_KEY, targetName);
        if(targetToObjectNamesMap != null) {
            for(String instanceName : targetToObjectNamesMap.keySet()) {
                ObjectName[] objectNames = targetToObjectNamesMap.get(instanceName);
                if (objectNames != null) {
                    for(ObjectName objectName : objectNames) {
                        Object[] params = new Object[2];
                        params[0] = endpoint;
                        params[1] = targetNamespace;
                        
                        String[] signature = new String[2];
                        signature[0] = "java.lang.String";
                        signature[1] = "java.lang.String";
                        
                        Object resultObject = this.invokeMBeanOperation(targetName,
                                objectName, "getWSDLImportedResource", params, signature);
                        
                        if (resultObject != null) {
                            importedResource = resultObject.toString();
                        }
                        if((importedResource != null) && (importedResource.length() > 0)) {
                            break;
                        }
                    }
                }
                if((importedResource != null) && (importedResource.length() > 0)) {
                    break;
                }
            }
        }
        
        return importedResource;
    }
    
    /**
     * check for BindingComponent
     * 
     * @param the
     *            name of the ServiceEngine or Binding Component
     * @return true if it is a Binding Component else false.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isBindingComponent(java.lang.String)
     */
    public boolean isBindingComponent(String componentName)
    throws ManagementRemoteException {
        boolean isBindingComponent = false;
        String descriptorString = this
        .getComponentInstallationDescriptor(componentName);
        try {
            JBIDescriptor descriptor = JBIDescriptor
            .createJBIDescriptor(descriptorString);
            isBindingComponent = descriptor.isBindingComponentDescriptor();
        } catch (Exception e) {
            throw new ManagementRemoteException(e);
        }
        return isBindingComponent;
    }
    
    /**
     * Tests is a component/library is installed on a target
     * 
     * @param componentName
     * @param targetName
     * @return true if installed, false if not
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isJBIComponentInstalled(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public boolean isJBIComponentInstalled(String componentName,
            String targetName) throws ManagementRemoteException {
        String xmlText = null;
        List<JBIComponentInfo> infoList = null;
        String state = null, sharedLibraryName = null, serviceAssemblyName = null;
        
        try {
            xmlText = this.listComponents(ComponentType.BINDING, state,
                    sharedLibraryName, serviceAssemblyName, targetName);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = JBIComponentInfo.readFromXmlText(xmlText);
                for (JBIComponentInfo info : infoList) {
                    if (componentName.equals(info.getName()) == true) {
                        return true;
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore the exception
        }
        try {
            xmlText = this.listComponents(ComponentType.ENGINE, state,
                    sharedLibraryName, serviceAssemblyName, targetName);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = JBIComponentInfo.readFromXmlText(xmlText);
                for (JBIComponentInfo info : infoList) {
                    if (componentName.equals(info.getName()) == true) {
                        return true;
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore the exception
        }
        try {
            // ////////////////////////////////////////
            // Start Check to make sure target is valid
            // ////////////////////////////////////////
            ObjectName installerServiceObjectName = this
            .getInstallationServiceMBeanObjectName(targetName);
            this.checkForValidTarget(installerServiceObjectName, targetName);
            // ////////////////////////////////////////
            // End Check to make sure target is valid
            // ////////////////////////////////////////
            
            List frameworkCompInfoList = this
            .getFrameworkComponentInfoListForSharedLibraries(null,
                    targetName);
            List uiCompInfoList = new ArrayList();
            uiCompInfoList = this.toUiComponentInfoList(frameworkCompInfoList,
                    targetName);
            xmlText = JBIComponentInfo.writeAsXmlText(uiCompInfoList);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = JBIComponentInfo.readFromXmlText(xmlText);
                for (JBIComponentInfo info : infoList) {
                    if (componentName.equals(info.getName()) == true) {
                        return true;
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore the exception
        }
        return false;
    }
    
    /**
     * Checks to see if the JBI Runtime is enabled.
     * 
     * @return true if JBI Runtime Framework is available, false if not
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isJBIRuntimeEnabled()
     */
    public boolean isJBIRuntimeEnabled() throws ManagementRemoteException {
        return true;
    }
    
    /**
     * Tests is a component/library is installed on a target
     * 
     * @param name
     * @param targetName
     * @return true if installed, false if not
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isServiceAssemblyDeployed(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public boolean isServiceAssemblyDeployed(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        String xmlText = null;
        List<ServiceAssemblyInfo> infoList = null;
        String state = null, componentName = null;
        try {
            // ////////////////////////////////////////
            // Start Check to make sure target is valid
            // ////////////////////////////////////////
            ObjectName installerServiceObjectName = this
            .getInstallationServiceMBeanObjectName(targetName);
            this.checkForValidTarget(installerServiceObjectName, targetName);
            // ////////////////////////////////////////
            // End Check to make sure target is valid
            // ////////////////////////////////////////
            
            this.validateUiServiceAssemblyInfoState(state);
            
            List saInfoList = this.getServiceAssemblyInfoList(
                    toFrameworkServiceAssemblyState(state), componentName,
                    targetName);
            
            xmlText = ServiceAssemblyInfo.writeAsXmlTextWithProlog(saInfoList);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = ServiceAssemblyInfo
                .readFromXmlTextWithProlog(xmlText);
                for (ServiceAssemblyInfo info : infoList) {
                    if (serviceAssemblyName.equals(info.getName()) == true) {
                        return true;
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore the exception
        }
        return false;
    }
    
    /**
     * check for ServiceEngine
     * 
     * @param the
     *            name of the ServiceEngine or Binding Component
     * @return true if it is a Service Engine else false.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isServiceEngine(java.lang.String)
     */
    public boolean isServiceEngine(String componentName)
    throws ManagementRemoteException {
        boolean isServiceEngine = false;
        String descriptorString = this
        .getComponentInstallationDescriptor(componentName);
        try {
            JBIDescriptor descriptor = JBIDescriptor
            .createJBIDescriptor(descriptorString);
            isServiceEngine = descriptor.isServiceEngineDescriptor();
        } catch (Exception e) {
            throw new ManagementRemoteException(e);
        }
        return isServiceEngine;
    }
    
    /**
     * Retrieve the Component Installation descriptor for the server target.
     * 
     * The Installation descriptor will never be different for a component name
     * in a JBI system because there's only one component allowed with that same
     * name. Therefore, this implies a registry lookup for the server target.
     * 
     * @param Component
     *            name as a string
     * @param targetName
     * @return the Component Installation descriptor as a string
     * @throws ManagementRemoteException
     *             on error
     */
    protected String getInstallationDescriptor(String componentName,
            String targetName) {
        ComponentQuery componentQuery = null;
        ComponentInfo componentInfo = null;
        String jbiXml = null;
        
        componentQuery = this.environmentContext.getComponentQuery(targetName);
        if (componentQuery != null) {
            componentInfo = componentQuery.getComponentInfo(componentName);
            if (componentInfo != null) {
                jbiXml = componentInfo.getInstallationDescriptor();
            }
        }
        return jbiXml;
    }
    
    /**
     * Retrieve the Shared Library Installation descriptor for the server
     * target.
     * 
     * The Installation descriptor will never be different for a library name in
     * a JBI system because there's only one library allowed with that same
     * name. Therefore, this implies a registry lookup for the domain target.
     * 
     * @param libraryName
     *            name as a string
     * @param targetName
     * @return the Library Installation descriptor as a string
     * @throws ManagementRemoteException
     *             on error
     */
    protected String getSharedLibraryDescriptor(String libraryName,
            String targetName) {
        ComponentQuery componentQuery = null;
        ComponentInfo componentInfo = null;
        String jbiXml = null;
        
        componentQuery = this.environmentContext.getComponentQuery(targetName);
        if (componentQuery != null) {
            componentInfo = componentQuery.getSharedLibraryInfo(libraryName);
            if (componentInfo != null) {
                jbiXml = componentInfo.getInstallationDescriptor();
            }
        }
        return jbiXml;
    }
    
    /**
     * Get the instance name of the platform's administration server.  If the
     * platform does not provide a separate administration server, then this 
     * method returns the name of the local instance.
     * @return instance name of the administration server
     * @throws ManagementRemoteException
     */
    public String getAdminServerName() throws ManagementRemoteException {
        return super.getPlatformContextAdminServerName();
    }
    
    
    /**
     * Determine whether this instance is the administration server instance.
     * @return <CODE>true</CODE> if this instance is the administration server,
     * <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isAdminServer() throws ManagementRemoteException {
        return super.isPlatformContextAdminServer();
    }
    
    
    /**
     * Get the name of this instance.
     * @return the name of this server instance.
     * @throws ManagementRemoteException
     */
    public String getInstanceName() throws ManagementRemoteException {
        return super.getPlatformContextInstanceName();
    }
    
    
    /**
     * Determine if the specified instance is up.
     * @return true if the instance is up and running, false otherwise
     * @throws ManagementRemoteException
     */
    public boolean isInstanceUp(String instanceName) throws ManagementRemoteException {
        return super.isPlatformContextInstanceUp(instanceName);
    }
    
    
    /**
     * Determine whether multiple servers are permitted within this AS
     * installation.
     * @return true if multiple servers are permitted.
     * @throws ManagementRemoteException
     */
    public boolean supportsMultipleServers() throws ManagementRemoteException {
        return super.platformContextSupportsMultipleServers();
    }
    
    
    /**
     * Get the Target Name. If the instance is not a clustered instance then
     * the target name is the instance name. If the instance is part of a
     * cluster then the target name is the cluster name.
     *
     * @return the target name. 
     * @throws ManagementRemoteException
     */
    public String getTargetName() throws ManagementRemoteException {
        return super.getPlatformContextTargetName();
    }
    
    
    /**
     * Get the Target Name for a specified instance. If the instance is not
     * clustered the instance name is returned. This operation is invoked by
     * the JBI instance MBeans only.
     *
     * @return the target name. 
     * @throws ManagementRemoteException
     */
    public String getTargetName(String instanceName) throws ManagementRemoteException {
        return super.getPlatformContextTargetName(instanceName);
    }
    
    
    /**
     * Get a set of the names of all the standalone servers in the domain.
     * @return a set of names of standalone servers in the domain.
     * @throws ManagementRemoteException
     */
    public Set<String> getStandaloneServerNames() throws ManagementRemoteException {
        return super.getPlatformContextStandaloneServerNames();
    }
    
    
    /**
     * Get a set of the names of all the clustered servers in the domain.
     * @return a set of names of clustered servers in the domain.
     * @throws ManagementRemoteException
     */
    public Set<String> getClusteredServerNames() throws ManagementRemoteException {
        return super.getPlatformContextClusteredServerNames();
    }
    
    /**
     * Get a set of the names of all the clusters in the domain.
     * @return a set of names of clusters in the domain.
     * @throws ManagementRemoteException
     */
    public Set<String> getClusterNames() throws ManagementRemoteException {
        return super.getPlatformContextClusterNames();
    }
    
    
    /**
     * Get a set of the names of all the servers in the specified cluster.
     * @return a set of names of servers in the cluster.
     * @throws ManagementRemoteException
     */
    public Set<String> getServersInCluster(String clusterName) throws ManagementRemoteException {
        return super.getPlatformContextServersInCluster(clusterName);
    }
    
    
    /**
     * Determine whether a target is a valid server or cluster name.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a valid
     * standalone server name or cluster name, <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isValidTarget(String targetName) throws ManagementRemoteException {
        return super.isPlatformContextValidTarget(targetName);
    }
    
    
    /**
     * Determine whether a target is a cluster.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a cluster,
     * <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isCluster(String targetName) throws ManagementRemoteException {
        return super.isPlatformContextCluster(targetName);
    }
    
    
    /**
     * Determine whether a target is a standalone server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a standalone
     * server, <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isStandaloneServer(String targetName) throws ManagementRemoteException {
        return super.isPlatformContextStandaloneServer(targetName);
    }
    
    
    /**
     * Determine whether the target is a clustered server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a clustered
     * server, <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isClusteredServer(String targetName) throws ManagementRemoteException {
        return super.isPlatformContextClusteredServer(targetName);
    }
    
    
    /**
     * Determine whether or not an instance is clustered.
     * @return <CODE>true</CODE> if the instance is clustered,
     * <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isInstanceClustered(String instanceName) throws ManagementRemoteException {
        return super.isPlatformContextInstanceClustered(instanceName);
    }
    
    
    /**
     * Get a string representation of the DAS JMX RMI connector port.
     * @return the JMX RMI connector port as a (CODE>String</CODE>.
     * @throws ManagementRemoteException
     */
    public String getJmxRmiPort() throws ManagementRemoteException {
        return super.getPlatformContextJmxRmiPort();
    }
    
    
    
    /**
     * Get the full path to the platform's instance root directory.
     * @return platform instance root
     * @throws ManagementRemoteException
     */
    public String getInstanceRoot() throws ManagementRemoteException {
        return super.getPlatformContextInstanceRoot();
    }
    
    
    /**
     * Get the full path to the platform's instaall root directory.
     * @return platform install root
     * @throws ManagementRemoteException
     */
    public String getInstallRoot() throws ManagementRemoteException {
        return super.getPlatformContextInstallRoot();
    }

    /**
     * Get the MBean server connection for a particular instance.
     * @return the <CODE>MBeanServerConnection</CODE> for the specified instance.
     * @throws ManagementRemoteException
     */
    public MBeanServerConnection getConnectionForInstance(String instanceName) throws ManagementRemoteException {
        MBeanServerConnection connection = null;
        try {
            connection = this.getPlatformContext().getMBeanServerConnection(instanceName);
        } catch (Exception e) {
            throw new ManagementRemoteException(e);
        }
        return connection;
    }
    
}

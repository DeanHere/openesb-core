/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentServiceMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.deployment;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.management.ObjectName;

import com.sun.esb.management.api.deployment.DeploymentService;
import com.sun.esb.management.base.services.AbstractServiceMBeansImpl;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIArchive;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.ServiceAssemblyDD;

/**
 * Defines operations for common deployment/undeployment services.
 * 
 * @author graj
 *
 */
public class DeploymentServiceMBeanImpl extends AbstractServiceMBeansImpl
        implements DeploymentService, Serializable {
    
    static final long serialVersionUID = -1L;
    
    /**
     * Constructor - Constructs a new instance of DeploymentServiceMBeanImpl
     * 
     * @param anEnvContext
     */
    public DeploymentServiceMBeanImpl(EnvironmentContext anEnvContext) {
        super(anEnvContext);
    }
    
    /**
     * deploys service assembly
     * 
     * @return result as a management message xml text
     * @param zipFilePath
     *            fie path
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.deployment.DeploymentService#deployServiceAssembly(java.lang.String, java.lang.String)
     */
    public String deployServiceAssembly(String zipFilePath, String targetName)
            throws ManagementRemoteException {
        /*
         * ==================================================================
         * According to IN=100359 at
         * http://inf.central.sun.com/inf/integrationReport.jsp?id=100359
         * 
         * NOTE: Facade MBean operations for the InstallationService: When the
         * Installer is loaded for a component, it is added to the domain. When
         * the Installer is unloaded the component is removed from the domain if
         * it is not installed on any instances. For the domain targetName, the
         * ComponentLifeCycle MBean is not available.
         * 
         * NOTE: Facade MBean operations for the DeploymentService: On deploy, a
         * Service Assembly is added to the domain and on undeploy it is removed
         * from the domain.
         * ==================================================================
         */

        logDebug("deploying Service Assembly Zip file " + zipFilePath);
        String result = null;
        Object resultObject = null;
        String assemblyName = this.validateServiceAssembly(zipFilePath);
        
        File file = new File(zipFilePath);
        String fileURLString = null;
        
        fileURLString = file.getAbsolutePath();
        
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        
        this.checkForValidTarget(deploymentServiceObjectName, targetName);
        
        logDebug("Calling deploy on DeploymentServiceMBean = "
                + deploymentServiceObjectName);
        
        resultObject = this.invokeMBeanOperation(deploymentServiceObjectName,
                "deploy", fileURLString);
        
        if (resultObject != null) {
            result = resultObject.toString();
            if ((assemblyName == null) || (result.contains("FAILED") == true)
                    || (result.contains("WARNING") == true)) {
                result = resultObject.toString();
            } else {
                result = assemblyName;
            }
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.deploy.error", null, null);
            throw new ManagementRemoteException(exception);
            
        }
        
        return result;
    }
    
    /**
     * Retrieve the Service Assembly Name
     * 
     * @param zipFilePath
     * @return the name of the Service Assembly or null
     */
    String validateServiceAssembly(String zipFilePath) {
        JBIArchive archive = null;
        ServiceAssemblyDD descriptor = null;
        String name = null;
        
        try {
            archive = new JBIArchive(zipFilePath);
            if (archive.isServiceAssemblyArchive() == true) {
                descriptor = (ServiceAssemblyDD) archive.getJbiDescriptor();
                name = descriptor.getName();
            }
        } catch (IOException ioException) {
        } catch (Exception exception) {
        }
        
        return name;
    }
    
    /**
     * deploys service assembly
     * 
     * @param assemblyName
     *            service assembly name
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and management message xml text strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.deployment.DeploymentService#deployServiceAssemblyFromDomain(java.lang.String, java.lang.String)
     */
    public String deployServiceAssemblyFromDomain(String assemblyName,
            String targetName) throws ManagementRemoteException {
        if (true == targetName.equals(JBIAdminCommands.DOMAIN_TARGET_KEY)) {
            return assemblyName;
        }
        String result = null;
        Object resultObject = null;
        
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        if (deploymentServiceObjectName != null) {
            this.checkForValidTarget(deploymentServiceObjectName, targetName);
            
            logDebug("Invoking deployServiceAssemblyFromRepository which "
                    + "internally invokes deploy on DeploymentServiceMBean = "
                    + deploymentServiceObjectName);
            
            resultObject = this.invokeMBeanOperation(
                    deploymentServiceObjectName, "deployFromRepository",
                    assemblyName);
        }
        
        if (resultObject != null) {
            result = resultObject.toString();
            if ((assemblyName == null) || (result.contains("FAILED") == true)
                    || (result.contains("WARNING") == true)) {
                result = resultObject.toString();
            } else {
                result = assemblyName;
            }
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.deploy.error", null, null);
            throw new ManagementRemoteException(exception);
            
        }
        
        return result;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.deployment.DeploymentService#undeployServiceAssembly(java.lang.String, boolean, boolean, java.lang.String)
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            boolean forceDelete, boolean retainInDomain, String targetName)
            throws ManagementRemoteException {
        return this.undeployServiceAssemblyInternal(serviceAssemblyName,
                forceDelete, retainInDomain, targetName);
    }
    
    /**
     * Undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.deployment.DeploymentService#undeployServiceAssembly(java.lang.String, boolean, java.lang.String)
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            boolean forceDelete, String targetName)
            throws ManagementRemoteException {
        if (false == forceDelete) {
            return this.undeployServiceAssembly(serviceAssemblyName, targetName);
        }
        boolean retainInDomian = false;
        return this.undeployServiceAssembly(serviceAssemblyName, forceDelete, retainInDomian, targetName);
    }
    
    /**
     * Undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @return result as a management message xml text
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.deployment.DeploymentService#undeployServiceAssembly(java.lang.String, java.lang.String)
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        return this.undeployServiceAssemblyInternal(serviceAssemblyName,
                targetName);
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param force
     * @param retainInDomain
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#undeployServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    String undeployServiceAssemblyInternal(String serviceAssemblyName,
            boolean force, boolean retainInDomain, String targetName)
            throws ManagementRemoteException {
        
        logDebug("Undeploying Service Assembly " + serviceAssemblyName);
        
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(deploymentServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        logDebug("Calling undeploy on DeploymentServiceMBean = "
                + deploymentServiceObjectName);
        
        Object[] params = new Object[3];
        params[0] = serviceAssemblyName;
        params[1] = Boolean.valueOf(force);
        params[2] = Boolean.valueOf(retainInDomain);
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "boolean";
        
        Object resultObject = this.invokeMBeanOperation(
                deploymentServiceObjectName, "undeploy", params, signature);
        
        String result = null;
        if (resultObject != null) {
            result = resultObject.toString();
            if ((serviceAssemblyName == null)
                    || (result.contains("FAILED") == true)
                    || (result.contains("WARNING") == true)) {
                result = resultObject.toString();
            } else {
                result = serviceAssemblyName;
            }
            return result;
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.undeploy.error", null, null);
            throw new ManagementRemoteException(exception);
        }
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#undeployServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    String undeployServiceAssemblyInternal(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        
        logDebug("Undeploying Service Assembly " + serviceAssemblyName);
        
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(deploymentServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        logDebug("Calling undeploy on DeploymentServiceMBean = "
                + deploymentServiceObjectName);
        
        Object resultObject = this.invokeMBeanOperation(
                deploymentServiceObjectName, "undeploy", serviceAssemblyName);
        
        String result = null;
        if (resultObject != null) {
            result = resultObject.toString();
            if ((serviceAssemblyName == null)
                    || (result.contains("FAILED") == true)
                    || (result.contains("WARNING") == true)) {
                result = resultObject.toString();
            } else {
                result = serviceAssemblyName;
            }
            return result;
        } else {
            Exception exception = this.createManagementException(
                    "ui.mbean.undeploy.error", null, null);
            throw new ManagementRemoteException(exception);
        }
    }
    
    /////////////////////////////////////////////
    // Start of Cumulative Operation Definitions
    /////////////////////////////////////////////
    /**
     * deploys service assembly
     * 
     * @param zipFilePath
     *            fie path
     * @param targetNames
     * @return result as a management message xml text [targetName,xmlString]
     *         map
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String, String> deployServiceAssembly(String zipFilePath,
            String[] targetNames) throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.deployServiceAssembly(zipFilePath,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
        }
        return (Map<String, String>) result;
    }    

    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @return result as a management message xml text as [targetName, string]
     *         map
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> undeployServiceAssembly(
            String serviceAssemblyName, String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.undeployServiceAssembly(serviceAssemblyName,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * deploys service assembly
     * 
     * @param assemblyName
     *            name of the service assembly
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and management message xml text strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> deployServiceAssemblyFromDomain(
            String assemblyName, String[] targetNames)
            throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.deployServiceAssemblyFromDomain(assemblyName,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> undeployServiceAssembly(
            String serviceAssemblyName, boolean forceDelete,
            String[] targetNames) throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.undeployServiceAssembly(serviceAssemblyName,
                        forceDelete, targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> undeployServiceAssembly(
            String serviceAssemblyName, boolean forceDelete,
            boolean retainInDomain, String[] targetNames)
            throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.undeployServiceAssembly(serviceAssemblyName,
                        forceDelete, retainInDomain, targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
        
    }
    
    
    /////////////////////////////////////////////
    // End of Cumulative Operation Definitions
    /////////////////////////////////////////////
    
}

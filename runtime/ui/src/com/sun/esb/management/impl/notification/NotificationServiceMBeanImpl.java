/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NotificationServiceMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.notification;

import java.io.Serializable;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import com.sun.esb.management.api.notification.EventNotificationListener;
import com.sun.esb.management.api.notification.NotificationService;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.ui.common.I18NBundle;

/**
 * @author graj
 *
 */
public class NotificationServiceMBeanImpl extends
        NotificationBroadcasterSupport implements NotificationListener,
        DynamicMBean, Serializable, NotificationService {
    static final long serialVersionUID = -1L;
    
    private String name = "NotificationService";
    private String description = "Notification Service Broadcaster";
    
    /** i18n */
    private static I18NBundle              I18NBUNDLE                          = null;
    
    /** Jbi Environment context. */
    protected EnvironmentContext           environmentContext;
    
    /**
     * Events that generate notifications.
     */
    enum EventType {
        Installed,
        Upgraded,
        Uninstalled,
        Deployed,
        Undeployed,
        Ready,
        Started,
        Stopped,
        ShutDown
    }

    /**
     * Sources of notifications.
     */
    enum SourceType {
        JBIRuntime,
        BindingComponent,
        ServiceEngine,
        SharedLibrary, 
        ServiceUnit,
        ServiceAssembly
    }    
    
    /**
     * Common prefix for all notification messages.
     */
    static final String JBI_PREFIX = "com.sun.jbi.";

    /**
     * Notification types emitted by the runtime.
     */
    static final String[] NOTIFICATION_TYPES = {
        JBI_PREFIX + EventType.Ready + "." + SourceType.JBIRuntime,
        JBI_PREFIX + EventType.Started + "." + SourceType.JBIRuntime,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.JBIRuntime,
        JBI_PREFIX + EventType.Installed + "." + SourceType.SharedLibrary,
        JBI_PREFIX + EventType.Uninstalled + "." + SourceType.SharedLibrary,
        JBI_PREFIX + EventType.Installed + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.Started + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.ShutDown + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.Uninstalled + "." + SourceType.BindingComponent,
        JBI_PREFIX + EventType.Installed + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.Started + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.ShutDown + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.Uninstalled + "." + SourceType.ServiceEngine,
        JBI_PREFIX + EventType.Deployed + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.Started + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.ShutDown + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.Undeployed + "." + SourceType.ServiceAssembly,
        JBI_PREFIX + EventType.Deployed + "." + SourceType.ServiceUnit,
        JBI_PREFIX + EventType.Started + "." + SourceType.ServiceUnit,
        JBI_PREFIX + EventType.Stopped + "." + SourceType.ServiceUnit,
        JBI_PREFIX + EventType.ShutDown + "." + SourceType.ServiceUnit,
        JBI_PREFIX + EventType.Undeployed + "." + SourceType.ServiceUnit
    };

    /**
     * Class name for notifications emitted by the runtime.
     */
    static final String NOTIFICATION_CLASS_NAME =
        "javax.management.Notification";

    /**
     * Desription for notifications emitted by the runtime.
     */
    static final String NOTIFICATION_DESCRIPTION =
        "JBI runtime event";    
    
    static final String[]      ITEM_NAMES         = { "EventType",
        "SourceType", "TargetName", "SourceName", "ServiceAssemblyName",
        "ComponentName"                      };

    /** default constructor */
    public NotificationServiceMBeanImpl(EnvironmentContext anEnvContext) {
        this.environmentContext = anEnvContext;
        this.registerForLocalNotifications();
    }
    
    /**
     * Handles event notifications from the runtime and sends to remote clients
     * @param a notification
     * @param a handback
     * 
     * @see javax.management.NotificationListener#handleNotification(javax.management.Notification, java.lang.Object)
     */
    public void handleNotification(Notification notification, Object handback) {
        super.sendNotification(notification);
    }
    
    /**
     * Retrieve an attribute
     * @param the attribute name
     * @return the attribute value
     * 
     * @see javax.management.DynamicMBean#getAttribute(java.lang.String)
     */
    public Object getAttribute(String attribute)
            throws AttributeNotFoundException, MBeanException,
            ReflectionException {
        if(attribute.equals("name")) {
            return getName();
        }
        if(attribute.equals("description")) {
            return getDescription();
        }
        return null;
    }
    
    /**
     * Get an attribute list
     * @param the list of attribute names
     * @return the attributelist
     * @see javax.management.DynamicMBean#getAttributes(java.lang.String[])
     */
    public AttributeList getAttributes(String[] attributes) {
        // TODO Auto-generated method stub
        return null;
    }
    
    /**
     * Get the MBean Info
     * @return the MBeanInfo
     * 
     * @see javax.management.DynamicMBean#getMBeanInfo()
     */
    public MBeanInfo getMBeanInfo() {
        MBeanNotificationInfo[] notifications = getNotificationInfo();

        MBeanInfo beanInfo = new MBeanInfo(
                this.getClass().getName(),
                "ESBNotificationBroadcaster",
                null,
                null,
                null,
                notifications); // notifications
        return beanInfo;
    }
    
    @Override
    public MBeanNotificationInfo[] getNotificationInfo() { 
        return new MBeanNotificationInfo[] {
                new MBeanNotificationInfo(NOTIFICATION_TYPES,
                        NOTIFICATION_CLASS_NAME, NOTIFICATION_DESCRIPTION)
        };
    }
    
    /**
     * Invoke an operation
     * @param operationName
     * @param object parameters
     * @param param signatures
     * @return result of the invoke
     * 
     * @see javax.management.DynamicMBean#invoke(java.lang.String, java.lang.Object[], java.lang.String[])
     */
    public Object invoke(String actionName, Object[] params, String[] signature)
            throws MBeanException, ReflectionException {
        // TODO Auto-generated method stub
        return null;
    }
    
    /**
     * Set an attribute value
     * @param the attribute to set
     * 
     * @see javax.management.DynamicMBean#setAttribute(javax.management.Attribute)
     */
    public void setAttribute(Attribute attribute)
            throws AttributeNotFoundException, InvalidAttributeValueException,
            MBeanException, ReflectionException {
        if(attribute.getName().equals("name")) {
            setName((String)attribute.getValue());
        } else if(attribute.getName().equals("description")) {
            setDescription((String)attribute.getValue());
        }
    }
    
    /**
     * Set a list of attribute values
     * @param the list of attributes to set
     * @return the attribute list
     * 
     * @see javax.management.DynamicMBean#setAttributes(javax.management.AttributeList)
     */
    public AttributeList setAttributes(AttributeList attributes) {
        // TODO Auto-generated method stub
        return null;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * Add a notification listener
     * @param event notification listener object
     * 
     * @see com.sun.esb.management.api.notification.NotificationService#addNotificationEventListener(com.sun.esb.management.api.notification.EventNotificationListener)
     */
    public void addNotificationEventListener(EventNotificationListener listener)
            throws ManagementRemoteException {
    }

    /**
     * Remove a notification listener
     * @param event notification listener object
     * 
     * @see com.sun.esb.management.api.notification.NotificationService#removeNotificationEventListener(com.sun.esb.management.api.notification.EventNotificationListener)
     */
    public void removeNotificationEventListener(
            EventNotificationListener listener)
            throws ManagementRemoteException {
    }

    /**
     * Get the Framework Notification MBean
     * @return
     */
    protected ObjectName getFrameworkDomainNotificationMBean() {
        return this.environmentContext.getMBeanNames().getSystemServiceMBeanName(
                MBeanNames.ServiceName.Framework,
                MBeanNames.ServiceType.Notification, "domain");        
    }
    
    /**
     * Register for notifications from the framework MBean
     */
    protected void registerForLocalNotifications() {
        ObjectName mbeanName = getFrameworkDomainNotificationMBean();

        try {
            Object[] params = { this, null, null };
                String[] signature = { "javax.management.NotificationListener",
                        "javax.management.NotificationFilter", "java.lang.Object" };
                this.environmentContext.getMBeanServer().invoke(mbeanName, 
                        "addNotificationListener", params, signature);
        } catch (InstanceNotFoundException e) {
            // Could not register for local notifications
        } catch (MBeanException e) {
            // Could not register for local notifications
        } catch (ReflectionException e) {
            // Could not register for local notifications
        }
    }
}

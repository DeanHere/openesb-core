/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PerformanceMeasurementServiceMBeanImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.performance;

import java.io.Serializable;
import java.util.Map;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.openmbean.TabularData;

import com.sun.esb.management.api.performance.PerformanceMeasurementService;
import com.sun.esb.management.base.services.AbstractServiceMBeansImpl;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.ComponentStatisticsData;
import com.sun.esb.management.common.data.EndpointStatisticsData;
import com.sun.esb.management.common.data.FrameworkStatisticsData;
import com.sun.esb.management.common.data.IEndpointStatisticsData;
import com.sun.esb.management.common.data.NMRStatisticsData;
import com.sun.esb.management.common.data.PerformanceData;
import com.sun.esb.management.common.data.ServiceAssemblyStatisticsData;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ui.common.JBIJMXObjectNames;

/**
 * Defines operations to measure performance statistics. e.g., time taken to
 * normalize/denormalize, encode/decode, wire-to-NMR on the endpoints, etc.
 * 
 * @author graj
 */
public class PerformanceMeasurementServiceMBeanImpl extends
        AbstractServiceMBeansImpl implements PerformanceMeasurementService,
        Serializable {
    
    static final long serialVersionUID = -1L;
    
    static final String STATISTICS_KEY = "Statistics";
    
    
    /**
     * Constructor - Constructs a new instance of
     * PerformanceMeasurementServiceMBeanImpl
     * 
     * @param anEnvContext
     */
    public PerformanceMeasurementServiceMBeanImpl(
            EnvironmentContext anEnvContext) {
        super(anEnvContext);
    }
    
    /**
     * Resets the performance measurements on the endpoint.
     * 
     * @param componentName
     * @param endpoint
     * @param targetName
     * @param targetInstanceName 
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#clearPeformaceInstrumentationMeasurement(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public void clearPeformaceInstrumentationMeasurement(String componentName,
            String endpoint, String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        if(false == this.isTargetUp(targetName)) {
            return;
        }
        
        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        targetToObjectNamesMap = findLiveExtensionMBeanObjectNames(componentName, CUSTOM_STATISTICS_NAME_KEY, targetName);
        if(targetToObjectNamesMap != null) {
            for(String instanceName : targetToObjectNamesMap.keySet()) {
                ObjectName[] objectNames = targetToObjectNamesMap.get(instanceName);
                if(targetInstanceName != null) {
                    if(instanceName.equals(targetInstanceName) == false) {
                        continue;
                    }
                }
                
                if (objectNames != null) {
                    for(ObjectName objectName : objectNames) {
                        Object[] params = new Object[1];
                        params[0] = endpoint;
                        
                        String[] signature = new String[1];
                        signature[0] = "java.lang.String";
                        
                        this.invokeMBeanOperation(objectName,
                                "clearPeformaceInstrumentationMeasurement", params,
                                signature);
                    }
                }
            }
        }
    }
    
    /**
     * Retrieves the performance measurement enabling flag.
     * 
     * @param componentName
     * @param targetName
     * @param targetInstanceName 
     * @return true if measurement enabled, false if not
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#getPerformanceInstrumentationEnabled(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public boolean getPerformanceInstrumentationEnabled(String componentName,
            String targetName, String targetInstanceName) throws ManagementRemoteException {
        Boolean result = Boolean.FALSE;
        if(false == this.isTargetUp(targetName)) {
            return result.booleanValue();
        }
        
        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        targetToObjectNamesMap = findLiveExtensionMBeanObjectNames(componentName, CUSTOM_STATISTICS_NAME_KEY, targetName);
        if(targetToObjectNamesMap != null) {
            for(String instanceName : targetToObjectNamesMap.keySet()) {
                ObjectName[] objectNames = targetToObjectNamesMap.get(instanceName);
                if(targetInstanceName != null) {
                    if(instanceName.equals(targetInstanceName) == false) {
                        continue;
                    }
                }
                
                if (objectNames != null) {
                    for(ObjectName objectName : objectNames) {
                        Object[] params = null;
                        String[] signature = null;
                        
                        result = (Boolean) this.invokeMBeanOperation(objectName,
                                "getPerformanceInstrumentationEnabled", params,
                                signature);
                    }
                }
            }
        }
        return result.booleanValue();
    }
    
    /**
     * Retrieves the performance measurement data for the specified endpoint.
     * 
     * @param componentName
     * @param endpoint
     * @param targetName
     * @param targetInstanceName 
     * @return XML string representing PerformanceData map
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#getPerformanceInstrumentationMeasurement(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String getPerformanceInstrumentationMeasurement(
            String componentName, String endpoint, String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        String xmlText = null;
        if(false == this.isTargetUp(targetName)) {
            return xmlText;
        }
        Map<String /* Category */, PerformanceData> performanceDataMap = null;
        
        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        targetToObjectNamesMap = findLiveExtensionMBeanObjectNames(componentName, CUSTOM_STATISTICS_NAME_KEY, targetName);
        if(targetToObjectNamesMap != null) {
            for(String instanceName : targetToObjectNamesMap.keySet()) {
                ObjectName[] objectNames = targetToObjectNamesMap.get(instanceName);
                if(targetInstanceName != null) {
                    if(instanceName.equals(targetInstanceName) == false) {
                        continue;
                    }
                }
                if (objectNames != null) {
                    for(ObjectName objectName : objectNames) {
                        Object[] params = new Object[1];
                        params[0] = endpoint;
                        
                        String[] signature = new String[1];
                        signature[0] = "java.lang.String";
                        
                        Object resultObject = this.invokeMBeanOperation(objectName,
                                "getPerformanceInstrumentationMeasurement", params,
                                signature);
                        
                        if (resultObject != null) {
                            performanceDataMap = PerformanceData
                                    .retrieveDataMap((TabularData) resultObject);
                            xmlText = PerformanceData
                                    .convertDataMapToXML(performanceDataMap);
                            
                        }
                    }
                }
            }
        }
        return xmlText;
    }
    
    /**
     * Retrieves the performance statistics categories. Each item in the array
     * is the key to the composite performance data, which also indicates the
     * type (e.g. normalization) of measurement.
     * 
     * @param componentName
     * @param targetName
     * @param targetInstanceName 
     * @return array of performance measurement categories
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#getPerformanceMeasurementCategories(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String[] getPerformanceMeasurementCategories(String componentName,
            String targetName, String targetInstanceName) throws ManagementRemoteException {
        String[] measurementCategories = null;
        if(false == this.isTargetUp(targetName)) {
            return measurementCategories;
        }
        
        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        targetToObjectNamesMap = findLiveExtensionMBeanObjectNames(componentName, CUSTOM_STATISTICS_NAME_KEY, targetName);
        if(targetToObjectNamesMap != null) {
            for(String instanceName : targetToObjectNamesMap.keySet()) {
                ObjectName[] objectNames = targetToObjectNamesMap.get(instanceName);
                if(targetInstanceName != null) {
                    if(instanceName.equals(targetInstanceName) == false) {
                        continue;
                    }
                }
                
                if (objectNames != null) {
                    for(ObjectName objectName : objectNames) {
                        Object[] params = null;
                        String[] signature = null;
                        
                        measurementCategories = (String[]) this.invokeMBeanOperation(
                                objectName, "getPerformanceMeasurementCategories", params,
                                signature);
                    }
                }
            }
        }
        return measurementCategories;
    }
    
    /**
     * Sets the performance measurement enabling flag.
     * 
     * @param componentName
     * @param flag
     * @param targetName
     * @param targetInstanceName
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#setPerformanceInstrumentationEnabled(java.lang.String,
     *      boolean, java.lang.String, java.lang.String)
     */
    public void setPerformanceInstrumentationEnabled(String componentName,
            boolean flag, String targetName, String targetInstanceName) throws ManagementRemoteException {
        if(false == this.isTargetUp(targetName)) {
            return;
        }
        Map<String /*instanceName*/, ObjectName[]> targetToObjectNamesMap = null;
        targetToObjectNamesMap = findLiveExtensionMBeanObjectNames(componentName, CUSTOM_STATISTICS_NAME_KEY, targetName);
        if(targetToObjectNamesMap != null) {
            for(String instanceName : targetToObjectNamesMap.keySet()) {
                ObjectName[] objectNames = targetToObjectNamesMap.get(instanceName);
                if(targetInstanceName != null) {
                    if(instanceName.equals(targetInstanceName) == false) {
                        continue;
                    }
                }
                
                if (objectNames != null) {
                    for(ObjectName objectName : objectNames) {
                        Object[] params = new Object[1];
                        params[0] = flag;
                        
                        String[] signature = new String[1];
                        signature[0] = "java.lang.boolean";
                        
                        this.invokeMBeanOperation(
                                objectName, "setPerformanceInstrumentationEnabled", params,
                                signature);
                    }
                }
            }
        }
    }
    
    /**
     * This method is used to provide JBIFramework statistics in the given
     * target.
     * 
     * @param target
     *            target name.
     * @return TabularData table of framework statistics in the given target.
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public String getFrameworkStatistics(String targetName)
            throws ManagementRemoteException {
        String xmlText = null;
        Map<String /* instanceName */, FrameworkStatisticsData> map = null;
        TabularData tabularResult = this.getFrameworkStatisticsAsTabularData(targetName);
        if (tabularResult != null) {
            map = FrameworkStatisticsData.retrieveDataMap(tabularResult);
            xmlText = FrameworkStatisticsData.convertDataMapToXML(map);
        }
        return xmlText;
    }
    
    /**
     * This method is used to provide JBIFramework statistics in the given
     * target.
     * 
     * @param target
     *            target name.
     * @return TabularData table of framework statistics in the given target.
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getFrameworkStatisticsAsTabularData(String targetName)
            throws ManagementRemoteException {
        TabularData tabularResult = null;
        String FRAMEWORK_STATS_METHOD = "getFrameworkStats";
        try {
            tabularResult = (TabularData) invokeMBeanOperation(
                    JBIJMXObjectNames.getJbiStatisticsMBeanObjectName(),
                    FRAMEWORK_STATS_METHOD, new Object[] { targetName },
                    new String[] { "java.lang.String" });
        } catch (MalformedObjectNameException me) {
            throw new ManagementRemoteException(me);
        }
        return tabularResult;
    }
    
    /**
     * This method is used to provide statistics for the given component in the
     * given target
     * 
     * @param targetName
     *            target name
     * @param componentName
     *            component name
     * @return TabularData table of component statistics
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public String getComponentStatistics(String componentName, String targetName)
            throws ManagementRemoteException {
        String xmlText = null;
        Map<String /* instanceName */, ComponentStatisticsData> map = null;
        TabularData tabularResult = getComponentStatisticsAsTabularData(
                componentName, targetName);
        if (tabularResult != null) {
            map = ComponentStatisticsData.retrieveDataMap(tabularResult);
            xmlText = ComponentStatisticsData.convertDataMapToXML(map);
        }
        return xmlText;
    }
    
    /**
     * This method is used to provide statistics for the given component in the
     * given target
     * 
     * @param targetName
     *            target name
     * @param componentName
     *            component name
     * @return TabularData table of component statistics
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     * 
     */
    public TabularData getComponentStatisticsAsTabularData(
            String componentName, String targetName)
            throws ManagementRemoteException {
        String COMPONENT_STATS_METHOD = "getComponentStats";
        TabularData tabularResult = null;
        try {
            tabularResult = (TabularData) invokeMBeanOperation(
                    JBIJMXObjectNames.getJbiStatisticsMBeanObjectName(),
                    COMPONENT_STATS_METHOD, new Object[] { componentName,
                            targetName }, new String[] { "java.lang.String",
                            "java.lang.String" });
        } catch (MalformedObjectNameException me) {
            throw new ManagementRemoteException(me);
        }
        return tabularResult;
    }
    
    /**
     * This method is used to provide statistic information about the given
     * endpoint in the given target
     * 
     * @param targetName
     *            target name
     * @param endpointName
     *            the endpoint Name
     * @return TabularData table of endpoint statistics
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public String getEndpointStatistics(String endpointName, String targetName)
            throws ManagementRemoteException {
        String xmlText = null;
        Map<String /* instanceName */, IEndpointStatisticsData> map = null;
        TabularData tabularResult = this.getEndpointStatisticsAsTabularData(endpointName, targetName);
        if (tabularResult != null) {
            map = EndpointStatisticsData.retrieveDataMap(tabularResult);
            xmlText = EndpointStatisticsData.convertDataMapToXML(map);
        }
        return xmlText;
    }
    
    /**
     * This method is used to provide statistic information about the given
     * endpoint in the given target
     * 
     * @param targetName
     *            target name
     * @param endpointName
     *            the endpoint Name
     * @return TabularData table of endpoint statistics
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getEndpointStatisticsAsTabularData(String endpointName, String targetName)
            throws ManagementRemoteException {
        TabularData tabularResult = null;
        String ENDPOINT_STATS_METHOD = "getEndpointStats";
        try {
            tabularResult = (TabularData) invokeMBeanOperation(
                    JBIJMXObjectNames.getJbiStatisticsMBeanObjectName(),
                    ENDPOINT_STATS_METHOD, new Object[] { endpointName,
                            targetName }, new String[] { "java.lang.String",
                            "java.lang.String" });
        } catch (MalformedObjectNameException me) {
            throw new ManagementRemoteException(me);
        }
        return tabularResult;
    }

    /**
     * This method is used to provide statistics about the message service in
     * the given target.
     * 
     * @param target
     *            target name.
     * @return TabularData table of NMR statistics in the given target.
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public String getNMRStatistics(String targetName)
            throws ManagementRemoteException {
        String xmlText = null;
        Map<String /* instanceName */, NMRStatisticsData> map = null;
        TabularData tabularResult = this.getNMRStatisticsAsTabularData(targetName);
        if (tabularResult != null) {
            map = NMRStatisticsData.retrieveDataMap(tabularResult);
            xmlText = NMRStatisticsData.convertDataMapToXML(map);
        }
        return xmlText;
    }
    
    /**
     * This method is used to provide statistics about the message service in
     * the given target.
     * 
     * @param target
     *            target name.
     * @return TabularData table of NMR statistics in the given target.
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getNMRStatisticsAsTabularData(String targetName)
            throws ManagementRemoteException {
        TabularData tabularResult = null;
        String NMR_STATS_METHOD = "getNMRStats";
        try {
            tabularResult = (TabularData) invokeMBeanOperation(
                    JBIJMXObjectNames.getJbiStatisticsMBeanObjectName(),
                    NMR_STATS_METHOD, new Object[] { targetName },
                    new String[] { "java.lang.String" });
        } catch (MalformedObjectNameException me) {
            throw new ManagementRemoteException(me);
        }
        
        return tabularResult;
    }

    /**
     * This method is used to provide statistics about a Service Assembly in the
     * given target.
     * 
     * @param target
     *            target name.
     * @param assemblyName
     *            the service assembly name.
     * @return TabularData table of NMR statistics in the given target.
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public String getServiceAssemblyStatistics(String assemblyName,
            String targetName) throws ManagementRemoteException {
        String xmlText = null;
        Map<String /* instanceName */, ServiceAssemblyStatisticsData> map = null;
        TabularData tabularResult = this.getServiceAssemblyStatisticsAsTabularData(assemblyName, targetName);
        if (tabularResult != null) {
            map = ServiceAssemblyStatisticsData.retrieveDataMap(tabularResult);
            xmlText = ServiceAssemblyStatisticsData.convertDataMapToXML(map);
        }
        return xmlText;
    }
    
    /**
     * This method is used to provide statistics about a Service Assembly in the
     * given target.
     * 
     * @param target
     *            target name.
     * @param assemblyName
     *            the service assembly name.
     * @return TabularData table of NMR statistics in the given target.
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getServiceAssemblyStatisticsAsTabularData(String assemblyName,
            String targetName) throws ManagementRemoteException {
        TabularData tabularResult = null;
        String SA_STATS_METHOD = "getServiceAssemblyStats";
        try {
            tabularResult = (TabularData) invokeMBeanOperation(
                    JBIJMXObjectNames.getJbiStatisticsMBeanObjectName(),
                    SA_STATS_METHOD, new Object[] { assemblyName, targetName },
                    new String[] { "java.lang.String", "java.lang.String" });
        } catch (MalformedObjectNameException me) {
            throw new ManagementRemoteException(me);
        }
        return tabularResult;
    }
    
   /**
     * This method is used to provide a list of consuming endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of consuming endpoints 
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "InstanceName",
     *  String[]          - "Endpoints",
     */
    public TabularData getConsumingEndpointsForComponentAsTabularData(String componentName, String targetName)
    throws ManagementRemoteException
    {
        TabularData tabularResult = null;
        String ENDPOINTS_LIST_METHOD = "getConsumingEndpointsForComponent";
        try {
            tabularResult = (TabularData) invokeMBeanOperation(
                    JBIJMXObjectNames.getJbiStatisticsMBeanObjectName(),
                    ENDPOINTS_LIST_METHOD, new Object[] { componentName, targetName },
                    new String[] { "java.lang.String", "java.lang.String" });
        } catch (MalformedObjectNameException me) {
            throw new ManagementRemoteException(me);
        }
        return tabularResult;
       
    }    
    
    
    /**
     * This method is used to provide a list of provisioning endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of provisioning endpoints 
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "InstanceName",
     *  String[]          - "Endpoints",
     */
    public TabularData getProvidingEndpointsForComponentAsTabularData(String componentName, String targetName)
    throws ManagementRemoteException
    {
        TabularData tabularResult = null;
        String ENDPOINTS_LIST_METHOD = "getProvidingEndpointsForComponent";
        try {
            tabularResult = (TabularData) invokeMBeanOperation(
                    JBIJMXObjectNames.getJbiStatisticsMBeanObjectName(),
                    ENDPOINTS_LIST_METHOD, new Object[] { componentName, targetName },
                    new String[] { "java.lang.String", "java.lang.String" });
        } catch (MalformedObjectNameException me) {
            throw new ManagementRemoteException(me);
        }
        return tabularResult;      
    }    

    /**
     * This method is used to enable monitoring of timing information about
     * message exchanges
     * 
     * @param targetName
     *            the target name
     */
    public void enableMessageExchangeMonitoring(String targetName)
            throws ManagementRemoteException {
        String ENABLE_TIMINGS_METHOD = "enableMessageExchangeMonitoring";
        try {
            invokeMBeanOperation(JBIJMXObjectNames
                    .getJbiStatisticsMBeanObjectName(), ENABLE_TIMINGS_METHOD,
                    new Object[] { targetName },
                    new String[] { "java.lang.String" });
        } catch (MalformedObjectNameException me) {
            throw new ManagementRemoteException(me);
        }
        
    }
    
    /**
     * This method is used to disable monitoring of timing information about
     * message exchanges
     * 
     * @param targetName
     *            the target name
     */
    public void disableMessageExchangeMonitoring(String targetName)
            throws ManagementRemoteException {
        String DISABLE_TIMINGS_METHOD = "disableMessageExchangeMonitoring";
        try {
            invokeMBeanOperation(JBIJMXObjectNames
                    .getJbiStatisticsMBeanObjectName(), DISABLE_TIMINGS_METHOD,
                    new Object[] { targetName },
                    new String[] { "java.lang.String" });
        } catch (MalformedObjectNameException me) {
            throw new ManagementRemoteException(me);
        }
        
    }
}

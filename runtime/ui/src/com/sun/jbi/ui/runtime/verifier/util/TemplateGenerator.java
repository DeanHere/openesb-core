/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TemplateGenerator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.verifier.util;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ui.runtime.verifier.LocalStringKeys;
import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.runtime.verifier.VerifierException;
import com.sun.jbi.ui.common.JarFactory;
import com.sun.jbi.ui.common.ToolsLogManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level; 
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.management.openmbean.CompositeType;

/**
 * This class has utility methods for template generation
 */
public class TemplateGenerator
{
 
    /**
     * Environment Context
     */
    private EnvironmentContext mEnvCtx;
    
    /**
     * verifier utils
     */
    private VerifierUtils mVerifierUtil;
    
    /**
     * tmp dir
     */
    private static String TMP = "tmp";
    
    /**
     * verifier
     */
    private static String VERIFIER = "verifier";
    
    /**
     * file suffix for app var template
     */
    private static String APPVAR_TEMPLATE_SUFFIX = "-app-var.properties";
    
    /**
     * file suffix for app var template
     */
    private static String APPCONFIG_TEMPLATE_SUFFIX = ".properties";
    
    /** hyphen*/
    private static String HYPHEN= "-";
    
        
    
    /**
     * map to store property file names for app vars
     */
    private Map<String, File> mAppVarFile = new HashMap<String, File>();
    
    /**
     * list to store property file names for app configs
     */
    private Map<String, File[]> mAppConfigFile = new HashMap<String, File[]>();    
    
    /**
     * ant script file name
     */
    private static String ANT_FILE_NAME = "configure.xml";
    
    /**
     * properties file name
     */
    private static String PROPERTIES_FILE_NAME = "connection.properties";    
    
    /**
     * template zip suffix
     */
    private static String TEMPLATE_ZIP_SUFFIX = "-templates.zip";
  
    /**
     * template dir suffix
     */    
    private static String TEMPLATE_DIR_SUFFIX = "-templates";
    
    /**
     * property name for the configuration name in the configuration file
     */
    private static final String CONFIGURATION_NAME = "configurationName";
    
    /**
     * resource bundle
     */
    private I18NBundle mResourceBundle;    
            
    /** Constructor */
    public TemplateGenerator (EnvironmentContext envCtx, I18NBundle resourceBundle)
    {
        this.mEnvCtx = envCtx;
        this.mResourceBundle = resourceBundle;
        this.mVerifierUtil = new VerifierUtils(resourceBundle);
    }
 
    
    /**
     * This method is used to generate template files for 
     * setting missing application varibles and application configs
     * @param mapAppVars map of component name and missing application variables
     * @param mapAppConfigs map of component name and missing application config composite type
     * @param includeDeployCommand if deploy command has to be included
     * @param target the target for which commands have to be generated
     * @param isAnt true if the clinet is ant
     * @param saName the saName
     * @returns String the name of the zip file that has the templates and the script to run them
     * 
     */
    public String  generateTemplates(
            Map<String, List<String>> mapAppVars, 
            Map<String, List<String>> mapAppConfigs, 
            Map<String, CompositeType> mapAppConfigTypes,
            boolean includeDeployCommand,
            String target,
            boolean isAnt,
            String saName,
            String saPath)
    {
        return generateTemplates(
                mapAppVars,
                mapAppConfigs,
                mapAppConfigTypes,
                includeDeployCommand,
                target,
                isAnt,
                saName,
                saPath,
                false,
                null,
                null);
    }   
    
    
    /**
     * This method is used to generate template files for 
     * setting missing application varibles and application configs
     * @param mapAppVars map of component name and missing application variables
     * @param mapAppConfigs map of component name and missing application config composite type
     * @param includeDeployCommand if deploy command has to be included
     * @param target the target for which commands have to be generated
     * @param isAnt true if the clinet is ant
     * @param saName the saName
     * @param fillInValues true if the generated files should include values for app. vars and app. configs
     * @param mapAppVarValues map of application vars
     * @param mapAppConfigValues of application configs
     * @returns String the name of the zip file that has the templates and the script to run them
     * 
     */
    public String  generateTemplates(
            Map<String, List<String>> mapAppVars, 
            Map<String, List<String>> mapAppConfigs, 
            Map<String, CompositeType> mapAppConfigTypes,
            boolean includeDeployCommand,
            String target,
            boolean isAnt,
            String saName,
            String saPath,
            boolean fillInValues,
            Map<String, Map<String, Object>> mapAppVarValues,
            Map<String, Map<String, Properties>> mapAppConfigValues)
    {
        try
        {
            File templatesDir = createTemplatesDir(saName);
            Set<String> components = mapAppVars.keySet();
            for(String component: components)
            {
                Map<String, Object> appVarValues = new HashMap<String, Object>();
                if (fillInValues && mapAppVarValues != null)
                {
                    if (mapAppVarValues.containsKey(component))
                    {
                        appVarValues = mapAppVarValues.get(component);
                    }
                }
                        
                File propFile =
                        createPropertiesFile(
                        templatesDir,
                        component, 
                        mapAppVars.get(component),
                        fillInValues,
                        appVarValues);
                if (propFile != null)
                {
                    mAppVarFile.put(component, propFile);
                }
                
                ToolsLogManager.getRuntimeLogger().finer(
                "Generated app. var. file " + propFile + " for component " + component);                                     

            }
            
            components = mapAppConfigs.keySet();
            for (String component: components)
            {
                List<String> appConfigs = mapAppConfigs.get(component);
                List propFileList = new ArrayList();
                for (String config: appConfigs)
                {
                    Map<String, Properties> appConfigValues = new HashMap<String, Properties>();
                    if (fillInValues && mapAppConfigValues != null)
                    {
                        if (mapAppConfigValues.containsKey(component))
                        {
                            appConfigValues = mapAppConfigValues.get(component);
                        }
                    }                    
                    File propFile =
                      createPropertiesFile(
                            templatesDir,
                            component,
                            mapAppConfigTypes.get(component),
                            config,
                            fillInValues,
                            appConfigValues);
                    propFileList.add(propFile);
                }
                File[] propFileArray = new File[propFileList.size()];
                mAppConfigFile.put(
                        component,
                        (File[])propFileList.toArray(propFileArray));
            }
                
            if (isAnt)
            {
                generateAntScript(
                        templatesDir,
                        includeDeployCommand,
                        target,
                        mAppVarFile, 
                        mAppConfigFile,
                        saPath);
                ToolsLogManager.getRuntimeLogger().finer(
                    "Generated ant script for app. vars");                                     

            }            
            
            try
            {
                JarFactory jarFactory = new JarFactory(templatesDir.getPath());
                File zipFile = new File(templatesDir, saName + TEMPLATE_ZIP_SUFFIX);
                if (zipFile.exists())
                {
                    if (!zipFile.delete())
                    {
                        throw new VerifierException(
                                mResourceBundle.getMessage(
                                LocalStringKeys.VERIFIER_TEMPLATE_ZIP_EXISTS));
                    }
                }   
                ToolsLogManager.getRuntimeLogger().finer(
                    "Creating zip file with templates in " + templatesDir);                  
                return jarFactory.zip(zipFile.getName());
            }
            catch(IOException ex)
            {
                String message = 
                        mResourceBundle.getMessage(
                                LocalStringKeys.VERIFIER_TEMPLATE_ZIP_NOT_CREATED) 
                                + ex.getMessage();                        
                ToolsLogManager.getRuntimeLogger().warning(message);
            }
            return null;
        }
        catch (VerifierException ex)
        {
            ToolsLogManager.getRuntimeLogger().log(Level.WARNING, ex.getMessage(), ex);
            return null;
        }

    }
    
    /**
     * This method is used to create a property file for a given component
     * with the given list of app vars/app config fields
     * @param templatesDir the templatesDir
     * @param componentName component name
     * @param varList list of app var / app config fields
     * @param fillInValues true if the values of app. vars have to be exported
     * @param mapVarValues map of app. var names and values.
     * @return file the file handle to the properties file, null if varList is empty
     */
     private File createPropertiesFile(
             File templatesDir,
             String componentName, 
             List<String> varList,
             boolean fillInValues,
             Map<String, Object> mapVarValues)
     throws VerifierException             
     {
         try
         {
             //if the list of missing app. vars is empty return null
             if (varList != null && varList.size() == 0)
             {
                 return null;
             }
             Properties properties = new Properties();
             for(String property : varList)
             {
                 //set the key first. (best effort for export)
                 properties.setProperty(property,"");
                 if(fillInValues && mapVarValues != null)
                 {
                     //try to overwrite the property
                     try
                     {
                        properties.put(
                                property,
                                mapVarValues.get(property));
                     }
                     catch (Throwable th)
                     {
                         String message = 
                            mResourceBundle.getMessage(
                            LocalStringKeys.VERIFIER_ISSUE_EXPORTING_APP_VAR_VALUE,
                            new Object[]{property});
                         ToolsLogManager.getRuntimeLogger().warning(message);  
                         if (th.getMessage() != null)
                         {
                            ToolsLogManager.getRuntimeLogger().warning(th.getMessage());                               
                         }
                     }

                 }
                 
             }
             File propertyFile =  
                     new File(
                         templatesDir,
                         componentName+APPVAR_TEMPLATE_SUFFIX);
             ToolsLogManager.getRuntimeLogger().finer("Creating " + propertyFile +
                     " with properties: " + properties);                           
             FileOutputStream fileoutputstream = 
                     new FileOutputStream(propertyFile);

             String headerStr = " " +
                 mResourceBundle.getMessage(LocalStringKeys.VERIFIER_PROP_FILE_HEADER) +
                 " " +
                 mResourceBundle.getMessage(LocalStringKeys.VERIFIER_PROP_FILE_HEADER1);
             properties.store(fileoutputstream, headerStr);
             return propertyFile;
         }
         catch(IOException ex)
         {
             throw new VerifierException(ex);
         }
     
     }
     
     
    /**
     * This method is used to create a property file for a given component
     * with the composite type for the component application configuration type
     * @param templatesDir the templatesDir
     * @param componentName component name
     * @param appConfigType the composite type
     * @param configName the name of the missing app config
     * @param fillInValues true if the values have to be exported
     * @param mapAppConfigValues map of config names and values
     * @return file the file handle to the properties file
     */
     private File createPropertiesFile(
             File templatesDir,
             String componentName, 
             CompositeType appConfigType,
             String configName,
             boolean fillInValues,
             Map<String, Properties> mapAppConfigValues)
     throws VerifierException             
     {
         try
         {
             Set<String> appConfigItemNames = new HashSet();
             if (appConfigType != null)
             {
                 appConfigItemNames = appConfigType.keySet();
             }
             
             Properties properties = new Properties();
             for(String configItem : appConfigItemNames)
             {
                 properties.setProperty(configItem,"");
                 if (configItem.equals(CONFIGURATION_NAME))
                 {
                     properties.setProperty(CONFIGURATION_NAME, configName);
                 }
             }
             
             //if export try to overwrite the properties object
             if (fillInValues && mapAppConfigValues != null)
             {
                 try
                 {
                     if(mapAppConfigValues.get(configName) != null)
                     {
                        properties = mapAppConfigValues.get(configName);
                     }
                 }
                 catch (Throwable th)
                 {
                     //only best case effort to overwrite. otherwise the
                     //values will be empty
                     String message = 
                        mResourceBundle.getMessage(
                        LocalStringKeys.VERIFIER_ISSUE_EXPORTING_APP_CONFIG_VALUE,
                        new Object[]{configName});
                     ToolsLogManager.getRuntimeLogger().warning(message);  
                     if (th.getMessage() != null)
                     {
                        ToolsLogManager.getRuntimeLogger().warning(th.getMessage());                               
                     }
                 }
             }
             
             
             
             File propertyFile =  
                     new File(
                         templatesDir,
                         componentName+ HYPHEN + configName + 
                         APPCONFIG_TEMPLATE_SUFFIX);
             ToolsLogManager.getRuntimeLogger().finer("Creating " + propertyFile +
                     " with properties: " + properties);                           
             FileOutputStream fileoutputstream = 
                     new FileOutputStream(propertyFile);

             String headerStr = " " +
                 mResourceBundle.getMessage(LocalStringKeys.VERIFIER_PROP_FILE_HEADER) +
                 " " +
                 mResourceBundle.getMessage(LocalStringKeys.VERIFIER_PROP_FILE_HEADER1);
             properties.store(fileoutputstream, headerStr);
             return propertyFile;
         }
         catch(IOException ex)
         {
             throw new VerifierException(ex);
         }
     
     }
     
     
     /**
      * This method is used to create a templates dir for the given SA
      * @param saName sa name
      * @return File the dir created
      */
     private File createTemplatesDir(String saName)
     {
             File tmpDir = new File(mEnvCtx.getJbiInstanceRoot(), TMP);
             File verifierDir = new File(
                     tmpDir, 
                     VERIFIER + File.separator + saName + TEMPLATE_DIR_SUFFIX);
             if (verifierDir.exists())
             {
                 mVerifierUtil.cleanup(verifierDir.getPath());
             }
             verifierDir.mkdirs();
             ToolsLogManager.getRuntimeLogger().finer(
                     "Created templates dir in server " + verifierDir);               
             return verifierDir;

     }
     
     /**
      * This method is used to generate an ant script to set
      * the application vars and application configs provided
      * in the given list of files
      * @param templateDir the template dir
      * @param includeDeployCommand include deploy command
      * @param target target
      * @param mapAppVarFile app vars properties file
      * @param mapAppConfigFile app config properties file
      */
     private void generateAntScript(
             File templateDir,
             boolean includeDeployCommand,
             String target,
             Map<String, File> mapAppVarFile,
             Map<String, File[]> mapAppConfigFile,
             String saPath)
     throws VerifierException
     {
         try
         {
             File connPropertiesFile =
                     new File(templateDir, PROPERTIES_FILE_NAME);                     
             File antScriptFile =
                     new File(templateDir, ANT_FILE_NAME);
             
             createConnectionPropertiesFile(connPropertiesFile, target);
             
             FileWriter fileWriter = new FileWriter(antScriptFile);
             fileWriter.write(
                mResourceBundle.getMessage(LocalStringKeys.ANT_SCRIPT_BEGIN_HEADERS) +
                ANT_SCRIPT_BEGIN);

             fileWriter.write(TASKDEF_CREATE_APP_VARIABLE);
             fileWriter.write(TASKDEF_CREATE_APP_CONFIG);
             if (includeDeployCommand)
             {
                fileWriter.write(TASKDEF_DEPLOY_APP);
             }
             fileWriter.write(ANT_SCRIPT_CONFIGURE_TARGET_BEGIN);
 
             Set<String> components = mapAppVarFile.keySet();
             for(String component: components)
             {
                 fileWriter.write(
                    constructAddAppVariableTask(
                         component, 
                         mapAppVarFile.get(component)));
                ToolsLogManager.getRuntimeLogger().finer(
                     "Added set task for " + component);                     
             }
             
             components = mapAppConfigFile.keySet();
             for(String component: components)
             {
                 File[] configFiles = mapAppConfigFile.get(component);
                 for (int i=0; i<configFiles.length; i++)
                 {
                    fileWriter.write(
                        constructAddAppConfigurationTask(
                         component, 
                         configFiles[i]));
                    ToolsLogManager.getRuntimeLogger().finer(
                         "Added set task for " + component);                     
                 }
             }
             
             if (includeDeployCommand)
             {
                 fileWriter.write(
                    constructDeployTask(target, saPath));
                 ToolsLogManager.getRuntimeLogger().finer(
                     "Added deploy task for " + saPath);                    
             }
             
             fileWriter.write(ANT_SCRIPT_END);
             fileWriter.flush();
             fileWriter.close();
         }
         catch (IOException ex)
         {
             throw new VerifierException(ex);
         }
     }
     
     /**
      * This method is used to create an ant task to write the
      * ant script file
      * @param component Name
      * @param file name
      * @returns String the ant task
      */
     private String constructAddAppVariableTask(
             String componentName,
             File propertyFile)
     {
         String fileName = null;
         try{
             
             fileName = propertyFile.getName();
         }
         catch(Exception ex)
         {
             ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
             //filename will be null
         }
         return
         "\t\t<antcall target=\"create-app-variable\">\n"+
         "\t\t\t<param name=\"component.name\" value=\"" + componentName + "\"/>\n" +
	 "\t\t\t<param name=\"properties.file.name\" value=\"" + fileName + "\"/>\n" +
         "\t\t</antcall>\n";
                 
     }
     
     
     /**
      * This method is used to create an ant task to deploy the SA
      * @param target Name
      * @param file name
      * @returns String the ant task
      */
     private String constructDeployTask(
             String targetName,
             String saPath)
     {

         return
         "\t\t<antcall target=\"deploy-application\">\n"+
         "\t\t\t<param name=\"deploy.file\" value=\"" + saPath + "\"/>\n " +
         "\t\t</antcall>\n";
     }
     
     
     /**
      * This method is used to create an ant task to write the
      * ant script file
      * @param component Name
      * @param file name
      * @returns String the ant task
      */
     private String constructAddAppConfigurationTask(
             String componentName,
             File propertyFile)
     {
         String fileName = null;
         String configName = "CONFIG";
         try{
             
             fileName = propertyFile.getName();
             //filename is constructed as
             //c:/tmp/component1-config1.properties
             configName = fileName.substring(
                fileName.indexOf(componentName) + componentName.length() + 1,
                fileName.lastIndexOf(".properties"));
         }
         catch(Exception ex)
         {
             ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
             //filename will be null
         }
         return
         "\t\t<antcall target=\"create-app-configuration\">\n"+
         "\t\t\t<param name=\"component.name\" value=\"" + componentName + "\"/>\n" +
         "\t\t\t<param name=\"config.name\" value=\"" + configName + "\"/>\n" +                 
	 "\t\t\t<param name=\"properties.file.name\" value=\"" + fileName + "\"/>\n " +
         "\t\t</antcall>\n";
                 
     }
     
     /**
      * This method is used to create the properties file that contains
      * properties for the connection settings
      * @param propertiesFileName file name
      * @param targetName target name
      *
      */
     private void createConnectionPropertiesFile(File propertiesFile, String targetName)
     {
         try
         {
             String newLine = "\n";
             StringBuffer connectionProperties = new StringBuffer();
                connectionProperties.append(
                   mResourceBundle.getMessage(
                       LocalStringKeys.VERIFIER_PROP_FILE_HEADER));
                connectionProperties.append(newLine);
                connectionProperties.append("jbi.username=admin");
                connectionProperties.append(newLine);
                connectionProperties.append("jbi.password=adminadmin");
                connectionProperties.append(newLine);
                connectionProperties.append("jbi.host=localhost");                
                connectionProperties.append(newLine);
                connectionProperties.append("jbi.port=4848");                
                connectionProperties.append(newLine);
                connectionProperties.append("jbi.secure=false");
                connectionProperties.append(newLine);
                connectionProperties.append("jbi.target=" + targetName);

             FileWriter fileWriter = new FileWriter(propertiesFile);
             fileWriter.write(connectionProperties.toString());
             fileWriter.flush();
             fileWriter.close();             
         }
         catch(Throwable ex)
         {
             ToolsLogManager.getRuntimeLogger().log(Level.WARNING, ex.getMessage(), ex);            
         }
     }     

     /**
      * contents at the beginning of the ant script file
      */
     private String ANT_SCRIPT_BEGIN = 
 "<project name=\"configure\" default=\"configure\" basedir=\".\">\n"+
 "\t<property file=\"connection.properties\"/>\n"+
 "\t<taskdef resource=\"com/sun/jbi/ui/ant/antlib.xml\" />\n";
 
     /**
      * configure target begin of the ant script file
      */
     private String ANT_SCRIPT_CONFIGURE_TARGET_BEGIN =
                                    "\t<target name=\"configure\">\n";     
     
 /**
  * contents at the end of the ant file
  */
 private String ANT_SCRIPT_END =
         "\t</target>\n</project>\n";
 
 /** end for the taskdefs file */
 private String TASKDEFS_TAIL =
    "\n</project>\n";
         
 /** task def for create app variable */
 private String TASKDEF_CREATE_APP_VARIABLE =
    "\t<target name=\"create-app-variable\">\n"+
    "\t\t<jbi-create-application-variables\n"+
    "\t\t\tusername=\"${jbi.username}\"\n"+
    "\t\t\tpassword=\"${jbi.password}\"\n"+
    "\t\t\thost=\"${jbi.host}\"\n"+
    "\t\t\tport=\"${jbi.port}\"\n"+
    "\t\t\ttarget=\"${jbi.target}\"\n"+
    "\t\t\tsecure=\"${jbi.secure}\"\n"+
    "\t\t\tfailOnError=\"false\"\n"+
    "\t\t\tcomponentName=\"${component.name}\"\n"+
    "\t\t\tappvariables=\"${properties.file.name}\"/>\n"+
    "\t</target>\n"; 
 
 /** task def for create app config */
 private String TASKDEF_CREATE_APP_CONFIG =
    "\t<target name=\"create-app-configuration\">\n"+
    "\t\t<jbi-create-application-configuration\n"+
                "\t\t\tusername=\"${jbi.username}\"\n"+
                "\t\t\tpassword=\"${jbi.password}\"\n"+
                "\t\t\thost=\"${jbi.host}\"\n"+
                "\t\t\tport=\"${jbi.port}\"\n"+
                "\t\t\ttarget=\"${jbi.target}\"\n"+
                "\t\t\tsecure=\"${jbi.secure}\"\n"+
                "\t\t\tfailOnError=\"false\"\n"+
                "\t\t\tcomponentName=\"${component.name}\"\n"+
                "\t\t\tname=\"${config.name}\"\n"+ 
                "\t\t\tparams=\"${properties.file.name}\"/>\n"+
    "\t</target>\n"; 
 
 /** task def for deploy application */
 private String TASKDEF_DEPLOY_APP =
    "\t<target name=\"deploy-application\">\n"+         
    "\t\t<jbi-deploy-service-assembly \n"+
         "\t\t\tusername=\"${jbi.username}\"\n"+ 
         "\t\t\tpassword=\"${jbi.password}\"\n"+ 
         "\t\t\thost=\"${jbi.host}\"\n"+ 
         "\t\t\tport=\"${jbi.port}\"\n"+ 
         "\t\t\ttarget=\"${jbi.target}\"\n"+ 
         "\t\t\tsecure=\"${jbi.secure}\"\n"+                  
         "\t\t\tfailOnError=\"true\"\n"+
         "\t\t\tfile=\"${deploy.file}\">\n"+
         "\t\t</jbi-deploy-service-assembly>\n"+
    "\t</target>\n";   
    
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIApplicationVerifier.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.verifier;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level; 
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.ReflectionException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanInfo;
import javax.management.MBeanAttributeInfo;
import javax.management.AttributeNotFoundException;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.RuntimeMBeanException;
import javax.management.RuntimeOperationsException;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.TabularDataSupport;

import com.sun.jbi.ui.common.JBIJMXObjectNames;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ServiceAssemblyQuery;
import com.sun.jbi.ServiceAssemblyInfo;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.management.MBeanNames.ComponentServiceType;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.MBeanNames.ServiceName;
import com.sun.jbi.management.MBeanNames.ServiceType;
import com.sun.jbi.ui.common.JBIArchive;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.ServiceAssemblyDD;
import com.sun.jbi.ui.common.ToolsLogManager;
import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.runtime.verifier.util.TemplateGenerator;
import com.sun.jbi.ui.runtime.verifier.util.VerifierUtils;
import com.sun.jbi.util.ComponentConfigurationHelper;


/**
 * This class implements JBI Application Verification functionality.
 */
public class JBIApplicationVerifier {
    
     
    /**
     * Environment Context
     */
    private EnvironmentContext mEnvCtx;
     
    /**
     * ResourceBundle Context
     */
    private I18NBundle mResourceBundle;
     
    /**
     * target
     */
    private String mTarget;
            
    /**
     * list of components that were started
     */
    private Map<String, ComponentState> mStartedComponents = 
            new HashMap<String, ComponentState>();
    
    /**
     * list of components that are not installed
     */
    private List<String> mMissingComponents = new ArrayList();
    
    /**
     * Utility class
     */
    private VerifierUtils mVerifierUtil;
    
    
    /**equal */
    private static final String   EQUAL = "=";
    
    /** colon */
    private static final String   COLON = ":";
    
    /** comma */
    private static final String   COMMA = ",";
    
    /** JavaEEServiceEngine component name */
    private static final String JAVAEE_SERVICE_ENGINE = "sun-javaee-engine";
    
    /** Service Name for JavaEE Verifier MBean */
    private static final String JAVAEE_VERIFIER = "JavaEEVerifier";
            
    /**
     * indicates if templates have to be generated
     */
    private boolean mGenerateTemplates = false;
    
    /**
     * indiacates if deploy command has to be included
     */
    private boolean mIncludeDeployCommand = false;
    
    /**
     * map of component name and list of unresolved app. vars
     */
    private Map<String, List<String>> mAppVarTemplatesMap = new HashMap<String, List<String>>();
    
    /**
     * map of component name and composite type of unresolved app. config
     */
    private Map<String, List<String>> mAppConfigTemplatesMap = new HashMap<String, List<String>>();
    
    /**
     * this map is used to store the composite type of the application configuration of 
     * a component
     */
    private Map<String, CompositeType> mAppConfigTypeMap = new HashMap<String, CompositeType>();
    
    /** 
     * this map is used to store component names mapped to a map of app. var names and
     *  and app. var values.
     */
    private Map<String, Map<String,Object>> mAppVarValuesMap;

    /** 
     * this map is used to store component names mapped to a map of app. config names and
     *  and app. config values.
     */
    private Map<String, Map<String, Properties>> mAppConfigValuesMap;

    /**
     * this map is used to store the map between a SU name and its SU zip entry in SA zip
     */
    private Map<String, String> mSUNameToZipEntryName;
    
    
    /**
     * location of the sa store in DAS repository
     */
    private String REPOSITORY_SA_STORE = "jbi" + File.separator + "service-assemblies";
        
    /**
     * MBeanNames
     */
    private MBeanNames mBeanNames = null;
    
    /**
     * template dir suffix
     */    
    private static String TEMPLATE_DIR_SUFFIX = "-templates";    

   /**
    * EndpointInfo CompositeType items
    */
    
    static String[] ENDPOINT_DATA_ITEM_NAMES = {
        "EndpointName",
        "ServiceUnitName",
        "ComponentName",
        "Status",
        "MissingApplicationVariables",
        "MissingApplicationConfigurations"
    };
    
   /**
    * EndpointInfo CompositeType descriptions
    */
    static String[] ENDPOINT_DATA_ITEM_DESCRIPTIONS = {
        "Endpoint Name",
        "Service Unit Name",
        "Component Name",
        "Status of the endpoint",
        "List of missing application variables",
        "List of missing application configurations"
                
    };
    
    /**
     * JavaEE Verifier report item names
     */
    static String[] JAVAEE_VERIFIER_ITEM_NAMES = new String[] {
        "ServiceUnitName",
        "JavaEEVerifierReport"
    };
                   
    static String[] JAVAEE_VERIFIER_ITEM_DESCRIPTIONS = new String[] {
        "Service Unit Name",
        "JavaEE Verification Report"
    };                    

  
    /** status string for unresolved SAs */
    static String STATUS_UNRESOLVED = "UNRESOLVED";
    
    /** status string for resolved SAs */
    static String STATUS_RESOLVED = "RESOLVED";

    /** status unknown for endpoint corresponding to components that are not installed / could not be started*/
    static String STATUS_UNKNOWN = "UNKNOWN";
    
    /** status string for endpoints that are neither configured in jbi.xml nor in wsdl */
    static String STATUS_UNCONFIGURED = "UNRESOLVED";
            
    /**
     * Constructs an instance of JBIApplicationVerifier
     * @param envCtx EnvironmentContext
     */
    public JBIApplicationVerifier(EnvironmentContext envCtx)
    {
         mEnvCtx = envCtx;
         mResourceBundle = new I18NBundle("com.sun.jbi.ui.runtime.verifier");        
         mVerifierUtil = new VerifierUtils(mResourceBundle);
         mBeanNames = envCtx.getMBeanNames();

    }
    
     
    /**
     * This method is used to verify if the application variables and 
     * application configuration objects used in the given 
     * application are available in JBI runtime in the specified target. 
     * Also this method verifies if all necessary  components are installed.
     * If generateTemplates is true templates for missing application variables 
     * and application configurations are generated. A command script that uses
     * the template files to set configuration objects is generated.
     *
     * @param applicationURL the URL for the application zip file
     * @param targetName the target on which the application has to be verified
     * @param generateTemplates true if templates have to be generated
     * @param templateDir the dir to store the generated templates
     * @param includeDeployCommand true if the generated script should include
     * deploy command 
     * @param clientSAFilePath path to the SA in the client file system
     *
     * @returns CompositeData the verification report
     * 
     * CompositeType of verification report
     *  String          - "ServiceAssemblyName",
     *  String          - "ServiceAssemblyDescription",
     *  Integer         - "NumServiceUnits",
     *  Boolean         - "AllComponentsInstalled",
     *  String[]        - "MissingComponentsList",
     *  CompositeData[] - "EndpointInfo",
     *  CompositeData[] - "JavaEEVerifierReport"
     *  String          - "TemplateZIPID"
     * 
     * CompositeType of each EndpointInfo
     *  String    - "EndpointName",
     *  String    - "ServiceUnitName",
     *  String    - "ComponentName",
     *  String    - "Status"
     *  String[]  - "MissingApplicationVariables"
     *  String[]  - "MissingApplicationConfigurations"
     * 
     * CompositeType of each JavaEEVerifierReport
     *  String        - "ServiceUnitName"
     *  TabularData   - "JavaEEVerifierReport"
     *
     * TabularType of each JavaEEVerifierReport
     * 
     * SimpleType.STRING  - "Ear Filename"
     * SimpleType.STRING  - "Referrence By"
     * SimpleType.STRING  - "Referrence Class"
     * SimpleType.STRING  - "JNDI Name"
     * SimpleType.STRING  - "JNDI Class Type"
     * SimpleType.STRING  - "Message"
     * SimpleType.INTEGER - "Status"
     * 
     * @throws JBIRemoteException if the application could not be verified
     * 
     * Note: param templateDir is used between ant/cli and common client client
     * TemplateZIPID is used between common client server and common client client
     */
    public CompositeData verifyApplication(
            String applicationURL, 
            String targetName,
            boolean generateTemplates,
            String templateDir,
            boolean includeDeployCommand,
            String clientSAFilePath)
    throws JBIRemoteException
    {

        ToolsLogManager.getRuntimeLogger().finer("Verifier entered with SA " + applicationURL);            

        if (applicationURL == null || applicationURL.length() == 0)
        {
            String message = 
                    mResourceBundle.getMessage(LocalStringKeys.VERIFIER_INVALID_INPUT);
            ToolsLogManager.getRuntimeLogger().info(message);            
            throw new JBIRemoteException(message);
        }

        mTarget = targetName;
        mGenerateTemplates = generateTemplates;
        mIncludeDeployCommand = includeDeployCommand;
        JBIArchive archive;
        try
        {
            archive = new JBIArchive(applicationURL);
            mSUNameToZipEntryName = mVerifierUtil.getSUJarEntryMap(archive);
        }
        catch(Exception ex)
        {
            ToolsLogManager.getRuntimeLogger().info(ex.getMessage());            
            throw new JBIRemoteException(ex.getMessage());
        }
        if (!archive.isServiceAssemblyArchive())
        {
            String message =
                mResourceBundle.getMessage(LocalStringKeys.VERIFIER_INVALID_SA);
            ToolsLogManager.getRuntimeLogger().info(message);                        
            throw new JBIRemoteException(message);
        }


        try
        {
             String[] saInfo = getSAInfo(archive);
             String[] missingComponents = validateComponentList(archive, targetName);
             for(int i=0; i< missingComponents.length; i++)
             {
                mMissingComponents.add(missingComponents[i]);
             }
             
             CompositeData[] javaEEVerifierReports = null;
                     
             if (isJavaEEVerifierAvailable())
             {
                javaEEVerifierReports = verifyJavaEEServiceUnits(applicationURL, archive, targetName);
             }
             else
             {
                ToolsLogManager.getRuntimeLogger().fine("JavaEE Verifier MBean is not available");
             }
                         
             CompositeData endpointData[] = getEndpointsInfo(archive);
             String zipFile = null;
             String saName = saInfo[0];                              
             if (mGenerateTemplates)
             {
                 boolean isAnt = true;

                 TemplateGenerator generator = new TemplateGenerator(
                         mEnvCtx,
                         mResourceBundle);
                 zipFile = generator.generateTemplates(
                         mAppVarTemplatesMap, 
                         mAppConfigTemplatesMap,
                         mAppConfigTypeMap,
                         mIncludeDeployCommand,
                         mTarget,
                         isAnt,
                         saName,
                         clientSAFilePath);
             }
             
             CompositeData response = composeResponse(
                    saInfo,
                    missingComponents, 
                    endpointData, 
                    javaEEVerifierReports,
                    zipFile);        
             return response;
        }
        catch (Exception ex)
        {
             if (ex.getMessage() != null)
             {
                ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());                     
             }
             throw new JBIRemoteException(ex);
        }

    }
     
    //private methods     
     
   /**
    * This method is used to verify if the components needed by a given SA 
    * are installed
    *
    * @param archive the service assembly archive
    * @returns String[] the list of missing components
    * @throws JBIRemoteException
    */
    public String[] validateComponentList(JBIArchive archive, String targetName)  
    throws JBIRemoteException
    {
        ToolsLogManager.getRuntimeLogger().finer("Validating component list");            
        try
        {
            List<String> components = getComponentListForSA(archive);
            List<String> missingComponents = new java.util.ArrayList();

            ComponentQuery componentQuery = 
                 mEnvCtx.getComponentQuery(targetName);     
            List existingComponents = componentQuery
                    .getComponentIds(ComponentType.BINDINGS_AND_ENGINES);         

            for ( String component : components)
            {
                if (!existingComponents.contains(component))
                {
                    missingComponents.add(component);
                    ToolsLogManager.getRuntimeLogger().finer(
                            "Component " + component + " is not installed");                                            
                }
            }
            return missingComponents.toArray(new String[missingComponents.size()]);
            
        }
        catch(Exception ex)
        {
            throw new JBIRemoteException(ex.getMessage());
        }
    }
    
    /**
     * This method is used to retrieve information about the SA from 
     * the archive
     * @param archive the SA archive
     * @returns String[] with saName, saDesc, numSUs.
     * @throws JBIRemoteException
     */
    private String[] getSAInfo(JBIArchive archive)
    throws JBIRemoteException
    {
        ToolsLogManager.getRuntimeLogger().finer("Getting info from the SA ");                    
        try
        {
            ServiceAssemblyDD saDesc = (ServiceAssemblyDD)archive.getJbiDescriptor();
            return  new String[] {
                saDesc.getName(),
                saDesc.getDescription(),
                Integer.toString(saDesc.getServiceUnitDDList().size())

            };
        }
        catch (Exception ex)
        {
            throw new JBIRemoteException(ex.getMessage());
        }
    }
 
    /**
     * This method is used to compose the verification report
     * @param saInfo information about the SA, saName, saDesc, numSUs
     * @param missingComponents list of components neede by SA not in target
     * @param endpointData information about each endpoint in SA
     * @param javaEEVerifierReports verifier reports from Java EE Service Assembly
     * @param zipID the id for the zip file that has the generated templates
     */
    private CompositeData composeResponse(
            String[] saInfo, 
            String[] missingComponents,
            CompositeData[] endpointData,
            CompositeData[] javaEEVerifierReports,
            String zipID)
    throws JBIRemoteException
    {
        ToolsLogManager.getRuntimeLogger().finer("Composing verifier response ");                    
          
        try
        {        
            OpenType[] ENDPOINT_DATA_ITEM_TYPES = {
                SimpleType.STRING,
                SimpleType.STRING,
                SimpleType.STRING,
                SimpleType.STRING,
                new ArrayType(1, SimpleType.STRING),
                new ArrayType(1, SimpleType.STRING)        
            };

            CompositeType endpointInfoType = new CompositeType(
                "EndpointInfoType",
                "Provides information about an endpoint",
                ENDPOINT_DATA_ITEM_NAMES,
                ENDPOINT_DATA_ITEM_DESCRIPTIONS,
                ENDPOINT_DATA_ITEM_TYPES
            );
            ToolsLogManager.getRuntimeLogger().finer("Composed endpoint info type");                    
            
            //deduce the type for JavaEE Verifier reports
            CompositeType javaEEVerifierReportType = null;
            if (javaEEVerifierReports != null && javaEEVerifierReports.length > 0
                    && javaEEVerifierReports[0] != null)
            {
                javaEEVerifierReportType = javaEEVerifierReports[0].getCompositeType();
                ToolsLogManager.getRuntimeLogger().finer("Composed javaee verifier report type");                    
            }
                    
            
            ArrayList<String> verifierReportItemNames = new ArrayList<String>();
            verifierReportItemNames.add("ServiceAssemblyName");
            verifierReportItemNames.add("ServiceAssemblyDescription");
            verifierReportItemNames.add("NumServiceUnits");
            verifierReportItemNames.add("AllComponentsInstalled");
            verifierReportItemNames.add("MissingComponentsList");
            verifierReportItemNames.add("EndpointInfo");
            verifierReportItemNames.add("TemplateZIPID");
            if(javaEEVerifierReportType != null)
            {
                verifierReportItemNames.add("JavaEEVerifierReport");
            }
        
            ArrayList<String> verifierReportItemDescriptions = new ArrayList<String>();
            verifierReportItemDescriptions.add("Name of the Service Assembly");
            verifierReportItemDescriptions.add("Description of the Service Assembly");
            verifierReportItemDescriptions.add("Number of Service Units");
            verifierReportItemDescriptions.add("Are all necessary components installed");
            verifierReportItemDescriptions.add("List of missing components");
            verifierReportItemDescriptions.add("Information about the endpoints");
            verifierReportItemDescriptions.add("Id for the zip file with configuration templates");
            if(javaEEVerifierReportType != null)
            {
                verifierReportItemDescriptions.add("Java EE Verifier Reports");
            }            
    
            ArrayList<OpenType> verifierReportItemTypes = new ArrayList<OpenType>();
            verifierReportItemTypes.add(SimpleType.STRING);
            verifierReportItemTypes.add(SimpleType.STRING);
            verifierReportItemTypes.add(SimpleType.INTEGER);
            verifierReportItemTypes.add(SimpleType.BOOLEAN);
            verifierReportItemTypes.add(new ArrayType(1, SimpleType.STRING));
            verifierReportItemTypes.add(new ArrayType(1, endpointInfoType));
            verifierReportItemTypes.add(SimpleType.STRING);
            if (javaEEVerifierReportType != null)
            {
                verifierReportItemTypes.add(new ArrayType(1, javaEEVerifierReportType));
            }
            
            CompositeType verifierReportType = new CompositeType(
                "VerifierReportType",
                "Type of the verification report",
                (String[])verifierReportItemNames.toArray(new String[]{}),
                (String[])verifierReportItemDescriptions.toArray(new String[]{}),
                (OpenType[])verifierReportItemTypes.toArray(new OpenType[]{})
            );        

            ArrayList<Object> verifierReportValues = new ArrayList<Object>();
            verifierReportValues.add(saInfo[0]);
            verifierReportValues.add(saInfo[1]);
            verifierReportValues.add(Integer.valueOf(saInfo[2]));
            verifierReportValues.add(new Boolean(missingComponents.length == 0));
            verifierReportValues.add(missingComponents);
            verifierReportValues.add(endpointData);
            verifierReportValues.add(zipID);
            if (javaEEVerifierReportType != null)
            {
                verifierReportValues.add(javaEEVerifierReports);
            }
            
                            
            CompositeData cData = new CompositeDataSupport(
                    verifierReportType,
                    (String[])verifierReportItemNames.toArray(new String[]{}),
                    (Object[])verifierReportValues.toArray(new Object[]{}));
            
            ToolsLogManager.getRuntimeLogger().finer("Verifier Report: " + cData);
            return cData;
        } 
        catch(OpenDataException ode)
        {
            if (ode.getMessage() != null)
            {
                ToolsLogManager.getRuntimeLogger().warning(ode.getMessage());
            }
            throw new JBIRemoteException(ode.getMessage());
        }
                
    }
    
    
    /**
     * This method is used to get information about the status of 
     * the endpoint
     * @param archive the JBIArchive
     * @returns the endpointinfo 
     * @throws JBIRemoteException if the endpoint info type could not be obtained
     */
    private CompositeData[] getEndpointsInfo(JBIArchive archive)
    throws JBIRemoteException
    {
        try{      
            OpenType[] ENDPOINT_DATA_ITEM_TYPES = {
                SimpleType.STRING,
                SimpleType.STRING,
                SimpleType.STRING,
                SimpleType.STRING,
                new ArrayType(1, SimpleType.STRING),
                new ArrayType(1, SimpleType.STRING)        
            };            
            CompositeType endpointInfoType = new CompositeType(
                    "EndpointInfoType",
                    "Provides information about an endpoint",
                    ENDPOINT_DATA_ITEM_NAMES,
                    ENDPOINT_DATA_ITEM_DESCRIPTIONS,
                    ENDPOINT_DATA_ITEM_TYPES
                );            
            
            Map<String, EndpointInfo> endpointInfo = getEndpointDetails(archive);            
            CompositeData[] endpointArray = new CompositeData[endpointInfo.keySet().size()];            
            
            Iterator endpointsIter = endpointInfo.keySet().iterator();
            int counter = 0;
            EndpointInfo endpoint = null;
            while(endpointsIter.hasNext())
            {

                try 
                {
                    endpoint = endpointInfo.get(endpointsIter.next());
                    String status = null;
                    String[] missingVars = new String[]{};
                    String[] missingConfigs = new String[]{};
                    
                    if (!isComponentRunning(endpoint.getComponentName(), mTarget))
                    {
                        status = STATUS_UNKNOWN;
                    }
                    else
                    {
                        if ( supportsAppVars(endpoint.getComponentName(), mTarget) )
                        {
                            missingVars =  getMissingApplicationVariables(endpoint);
                        }
                        if ( supportsAppConfig(endpoint.getComponentName(), mTarget) )
                        {
                            missingConfigs = getMissingApplicationConfigurations(endpoint);
                        }
                        status = getEndpointStatus(endpoint, missingVars, missingConfigs);
                    }
                       
                    Object[] endpointValues = new Object[6];            
                    endpointValues[0] = endpoint.getEndpointName();
                    endpointValues[1] = endpoint.getServiceUnitName();
                    endpointValues[2] = endpoint.getComponentName();
                    endpointValues[3] = status;
                    endpointValues[4] = missingVars;
                    endpointValues[5] = missingConfigs;
               
                    CompositeData endpointData = 
                        new CompositeDataSupport(
                            endpointInfoType,
                            ENDPOINT_DATA_ITEM_NAMES,
                            endpointValues);   
                    endpointArray[counter]=endpointData;
                    counter++;
                                   
                }
                catch (VerifierException ve)
                {
                    //continue with next endpoint
                    ToolsLogManager.getRuntimeLogger().warning(
                            mResourceBundle.getMessage(
                            LocalStringKeys.VERIFIER_ENDPOINT_STATUS_NOT_DETERMINED,
                            new Object[] {endpoint.getEndpointName()}));       
                    if (ve.getMessage() != null)
                    {
                        ToolsLogManager.getRuntimeLogger().warning(ve.getMessage());                    
                    }
                }
                catch (Throwable jRE)
                {
                    //continue with next endpoint
                    ToolsLogManager.getRuntimeLogger().warning(
                            mResourceBundle.getMessage(
                            LocalStringKeys.VERIFIER_ENDPOINT_STATUS_NOT_DETERMINED,
                            new Object[] {endpoint.getEndpointName()}));   
                    if (jRE.getMessage() != null)
                    {
                        ToolsLogManager.getRuntimeLogger().warning(jRE.getMessage());                       
                    }
                }
            }
            for (String componentName: mStartedComponents.keySet())
            {
                revertComponentState(componentName, mStartedComponents.get(componentName), mTarget);
            }
            return endpointArray;

    }
        catch (OpenDataException ode)
        {
            throw new JBIRemoteException(ode.getMessage());            
        }
        catch(VerifierException ve)
        {
            throw new JBIRemoteException(ve.getMessage());            
        }
    }
    
    /**
     * This method is used to get a list of components needed by 
     * the given SA
     * @param archive the JBIArchive
     * @returns List<String> component list
     */
    private List getComponentListForSA(JBIArchive archive)  
    throws JBIRemoteException
    {
        try
        {
            ServiceAssemblyDD saDesc = (ServiceAssemblyDD)archive.getJbiDescriptor();
            List<ServiceAssemblyDD.ServiceUnitDD> suDescs = 
                 saDesc.getServiceUnitDDList();
            List<String> components = new java.util.ArrayList();

            for (ServiceAssemblyDD.ServiceUnitDD suDesc: suDescs)
            {
                components.add(suDesc.getTargetName());
            }    
            return components;
        }
        catch (Exception ex)
        {
            throw new JBIRemoteException(ex.getMessage());
        }
    }
    
    /**
     * This method is used to get the target component name for the given SU
     * @param archive the JBIArchive
     * @param suName the su Name
     * @returns String the component name
     * @returns List<String> component list
     */
    private String getTargetComponentForSU(JBIArchive archive, String suName)  
    throws VerifierException
    {
        try
        {
            ServiceAssemblyDD saDesc = (ServiceAssemblyDD)archive.getJbiDescriptor();
            List<ServiceAssemblyDD.ServiceUnitDD> suDescs = 
                 saDesc.getServiceUnitDDList();
            List<String> components = new java.util.ArrayList();

            for (ServiceAssemblyDD.ServiceUnitDD suDesc: suDescs)
            {
                if (suDesc.getName().equals(suName))
                {
                    return suDesc.getTargetName();
                }
            }    
            return null;
        }
        catch (Exception ex)
        {
            throw new VerifierException(ex);
        }
    }

    /**
     * This method is used to get the information about all the endpoints in 
     * an archive
     * @param archive JBIArchive
     * @return Map<String, EndpointInfo> a map of endpoint names to endpoint details
     * @throws JBIRemoteException if the information could not be extracted
     */
    private Map<String, EndpointInfo> getEndpointDetails(JBIArchive archive)
    throws VerifierException, JBIRemoteException
    {
    
        try
        {
            Map<String, EndpointInfo> endpointDetails = new HashMap();
            Map<String, String[]> wsdlMap = mVerifierUtil.getWSDLs(archive);
            Iterator iterSuNames = wsdlMap.keySet().iterator();
            while(iterSuNames.hasNext())
            {
                try
                {
                    String suName = (String)iterSuNames.next();
                    String targetComponent = getTargetComponentForSU(archive, suName);
                    if (targetComponent != null && targetComponent.equals(JAVAEE_SERVICE_ENGINE))
                    {
                        //javaee SUs will not have application variables or configurations
                        continue;
                    }
                    

                    ToolsLogManager.getRuntimeLogger().finer("Collecting details of endpoints in " + suName);                                    
                    String[] wsdls = wsdlMap.get(suName);
                    Map<String, List<String>> appConfigMap = mVerifierUtil.getEndpointConfigMap(archive,
                                                                          mSUNameToZipEntryName.get(suName));
                          
                    for (int j=0; j<wsdls.length; j++)
                    {
                        try
                        {
                            if ("".compareTo(wsdls[j]) == 0)
                            {
                                continue;
                            }
                            
                            String[] endpointNames = mVerifierUtil.getEndpointName(wsdls[j]);   
                            for (int i =0; i<endpointNames.length; i++)
                            {
                                String componentName = getTargetComponentForSU(archive, suName);
                                List appVarList = mVerifierUtil.getApplicationVariables(wsdls[j], endpointNames[i]);
                                List appConfigList = appConfigMap.get(endpointNames[i]);

                                EndpointInfo endpointInfo = new EndpointInfo();
                                endpointInfo.setEndpointName(endpointNames[i]);
                                endpointInfo.setServiceUnitName(suName);
                                endpointInfo.setComponentName(componentName);
                                endpointInfo.setApplicationVariables(appVarList);
                                endpointInfo.setApplicationConfigurations(appConfigList);
                                
                                //handle endpoints that are neither configured in wsdl nor in jbi.xml
                                if ( (appVarList == null || appVarList.size() == 0 ) && 
                                     (appConfigList == null || appConfigList.size() == 0))
                                {
                                    endpointInfo.setEndpointConfigured(
                                            mVerifierUtil.isPortConfiguredForEndpoint(wsdls[j], endpointNames[i]));
                                }
                                else
                                {
                                    endpointInfo.setEndpointConfigured(true);
                                }
                                
                                //if this endpoint is already available, give preference to the one that came from a BC
                                if (endpointDetails.containsKey(endpointNames[i]))
                                {
                                    ComponentType newComponentType = getComponentType(componentName);
                                    ComponentType oldComponentType = getComponentType(endpointDetails.get(endpointNames[i]).getComponentName());
                                    if (oldComponentType.equals(ComponentType.ENGINE) && 
                                            newComponentType.equals(ComponentType.BINDING))
                                    {
                                        endpointDetails.put(endpointNames[i], endpointInfo);
                                        ToolsLogManager.getRuntimeLogger().finer("Endpoint found in more than 1 wsdl, adding endpoint for component " + componentName);                                                                                                        
                                    } 
                                }
                                else
                                {
                                    endpointDetails.put(endpointNames[i], endpointInfo);
                                }
                                ToolsLogManager.getRuntimeLogger().finer("Discovered endpoint: " + endpointInfo);
                            }
                        }
                        catch (Exception ex)
                        {
                            //continue with next endpoint
                            if (ex.getMessage() != null)
                            {
                                ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());                    
                            }
                        }
                    }
                }
                catch(Exception ex) 
                {
                    //continue with next wsdl
                    if (ex.getMessage() != null)
                    {
                        ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());                    
                    }
                }
            }
            return endpointDetails;
        }
        catch (VerifierException ex)
        {
            if (ex.getMessage() != null)
            {
                ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());                    
            }
            throw ex;
        }
    }
    
    /**
     * This method is used to find out the type of the given component.
     * @param componentName the component name
     * @return ComponentType the componentType
     * @throws VerifierException if the type could not be determined
     */
    private ComponentType getComponentType(String componentName)
    throws VerifierException
    {
        try
        {
            ComponentQuery componentQuery = 
                    mEnvCtx.getComponentQuery(mTarget);     
            ComponentInfo compInfo =
                    componentQuery.getComponentInfo(componentName);
            return compInfo.getComponentType();
        }
        catch(Exception ex)
        {
            throw new VerifierException(ex.getMessage());
        }        
    }
    
    /**
     * This method is used to return the status of an endpoint by determining if
     * the application variables and application configurations used could be resolved.
     * @param endpointInfo endpointinfo
     * @param missingAppVars list of unresolved app vars
     * @param missingAppConfigs list of unresolved app configs
     * @return String status
     * @throws JBIRemoteException
     * @throws VerifierException
     */
    private String getEndpointStatus(
            EndpointInfo endpointInfo, 
            String[] missingAppVars,
            String[] missingAppConfigs)
    throws VerifierException, JBIRemoteException
    {
        
        if (mMissingComponents.contains(endpointInfo.getComponentName()))
        {
            return STATUS_UNKNOWN;
        }       
        
        if (!endpointInfo.getEndpointConfigured())
        {
            return STATUS_UNCONFIGURED;
        }
            
        if ( (missingAppVars == null || 
             missingAppVars.length == 0 ) &&
            (missingAppConfigs == null || 
             missingAppConfigs.length == 0))
        {
            return STATUS_RESOLVED;
        }        
        else
        {
            return STATUS_UNRESOLVED;
        }
        
    }
    
    /**
     * This method is used to verify if a set of application variables 
     * are set in the given target for a given component
     * This methods returns the list of missing application variables 
     * from the given list. 
     * @param endpoint EndpointInfo
     * @return String[] the list of missing application variables
     */
    private String[] getMissingApplicationVariables(EndpointInfo endpoint)
    throws VerifierException,
           JBIRemoteException,
           InstanceNotFoundException,
           IntrospectionException,
           ReflectionException
    {
        List<String> unresolvedVars = new ArrayList();
        String componentName = endpoint.getComponentName();
        List<String> tmpAppVarList = endpoint.getApplicationVariables();
        List<String> appVarList = removeComponentConfigurations(tmpAppVarList,
                                                                componentName);
                
        //if the component is not available, then return empty list
        if (mMissingComponents.contains(componentName))
        {
            return new String[]{};
        }             
        Properties applicationVariables = 
                getApplicationVariables(
                    componentName, 
                    mTarget);
        Set existingApplicationVariables = applicationVariables.keySet();
        ToolsLogManager.getRuntimeLogger().finer(
                "List of application vars for component" +
                existingApplicationVariables);                    
        
        //iterate over the list of app. vars used to confirm if they exist
        for ( String variable: appVarList)
        {
            if (!existingApplicationVariables.contains(variable))
            {
                unresolvedVars.add(variable);
            }
        }
        
        //iterate over the list of app. configs to find out if they contain 
        //an app. var
        List<String> appConfigList = endpoint.getApplicationConfigurations();
        if (appConfigList != null)
        {
            Set applicationConfigurations = 
                getApplicationConfigurations(
                    componentName, 
                    mTarget);     
            
            for ( String config:appConfigList )
            {
                if (applicationConfigurations.contains(config))
                {
                    //config used in the SA exists in the component. examine the contents and look
                    //for referrenced application variables
                    List<String> referredVars = 
                            getAppVarReferencedInAppConfig(componentName, config);
                    for (String var : referredVars)
                    {
                        if (!existingApplicationVariables.contains(var))
                        {
                            unresolvedVars.add(var);
                        }
                    }
                }
            }
        }
        
        if (mGenerateTemplates)
        {
            List currentVars;
            if (mAppVarTemplatesMap.containsKey(componentName))
            {
                currentVars = mAppVarTemplatesMap.get(componentName);
            }
            else
            {
                currentVars = new ArrayList<String>();
            }
            currentVars.addAll(unresolvedVars);                    
            mAppVarTemplatesMap.put(componentName, currentVars);
        }                  
        return (String[])unresolvedVars.toArray(new String[]{});
    
    }
    
   /**
     * This method is used to verify if a set of application configurations 
     * are set in the given target for a given component
     * @param endpoint EndpointInfo
     * @return String[] the list of missing application configurations
     */
    private String[] getMissingApplicationConfigurations(EndpointInfo endpoint)
    throws VerifierException, JBIRemoteException
    {
        List<String> unresolvedConfigs = new ArrayList();
        String componentName = endpoint.getComponentName();
        List<String> appConfigList = endpoint.getApplicationConfigurations();
        
        //if the component is not available, then return empty list
        if (mMissingComponents.contains(componentName))
        {
            return new String[]{};
        }  
        
        Set applicationConfigurations = 
                getApplicationConfigurations(
                    componentName, 
                    mTarget);
        ToolsLogManager.getRuntimeLogger().finer(
                "List of application configs for component" +
                applicationConfigurations);                    
        
        if(appConfigList != null)
        {
            for (String config: appConfigList)
            {
                if (!applicationConfigurations.contains(config))
                {
                    unresolvedConfigs.add(config);
                }
            }
        }

        if (mGenerateTemplates)
        {
            List currentConfigs;
            if (mAppConfigTemplatesMap.containsKey(componentName))
            {
                currentConfigs = mAppConfigTemplatesMap.get(componentName);
            }   
            else
            {
                currentConfigs = new ArrayList<String>();
            }
            currentConfigs.addAll(unresolvedConfigs);
            mAppConfigTemplatesMap.put(componentName, currentConfigs);
            mAppConfigTypeMap.put(componentName, getAppConfigType(componentName, mTarget));                
        }
        return (String[])unresolvedConfigs.toArray(new String[]{});
        
    }    
    
    /**
     * This method is used to get a list of unresolved application variables
     * used in the given application configuration
     * @param component component name
     * @param configName config name
     */
    private List<String> getAppVarReferencedInAppConfig
            (String component, String configName)
    {
        ArrayList<String> varsList = new ArrayList<String>();
        try
        {
            Map<String, Properties> appConfigMap = getApplicationConfigurationsAsMap(component, mTarget);
            Properties configValue = appConfigMap.get(configName);
            if (configValue !=null)
            {
                for (Object key:configValue.keySet())
                {
                     varsList.addAll(mVerifierUtil.getAppVars((String)configValue.get(key)));
                }
            }
        }
        catch (VerifierException vex)
        {
            //if we could not dereference app. vars in app. config we do not let
            //that fail the verify process
            if (vex.getMessage() != null)
            {
                ToolsLogManager.getRuntimeLogger().warning(vex.getMessage());                                
            }
        }
        return varsList;
    }

    /**
     * This method removes the component configurations from the input list
     * @param theList list which contains application variables and
     *                component configuration names
     * @param componentName
     * @returns list which contains application variables only 
     */
    private List<String> removeComponentConfigurations(List<String> theList,
                                                String componentName)
        throws InstanceNotFoundException,
               IntrospectionException,
               ReflectionException,
               JBIRemoteException,
               VerifierException
    {
        List<String> returnList = new ArrayList<String>(theList);

        ObjectName configFacadeMBean =
            this.getComponentConfigurationFacadeMBeanName(componentName, mTarget);
        MBeanServer mbeanServer = mEnvCtx.getMBeanServer();
        MBeanInfo mbeanInfo = mbeanServer.getMBeanInfo(configFacadeMBean);
        MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
        Iterator iter = theList.iterator();
        while (iter.hasNext())
        {
             String varName = (String) iter.next();
             if (varName != null)
             {
                 for (MBeanAttributeInfo attributeInfo : mbeanAttrInfoArray) {
                    String key = "" + attributeInfo.getName();

                    if ((varName != null) && (varName.trim().compareTo(key)==0))
                    {
                        returnList.remove(varName);
                    }
                }
            }
        }

        return returnList;
    }

    /**
     * This method is used to get a list of application variables 
     * set for a component in the given target
     * @param componentName component Name
     * @param targetName target Name
     * @returns Properties application variables 
     */
    private Properties getApplicationVariables(
            String componentName, 
            String targetName)
    throws VerifierException, JBIRemoteException
    {

        Properties appVarProps = new Properties();
        ObjectName configFacadeMBean = this
                .getComponentConfigurationFacadeMBeanName(componentName, targetName);
        ToolsLogManager.getRuntimeLogger().finer(
                "getApplicationVariables(" + componentName + "," + targetName
                 + "): configMBean = " + configFacadeMBean);

        if ((configFacadeMBean != null) && (true == this.isValidTarget(configFacadeMBean))) 
        {
            try 
            {
                TabularData appVarTable = (TabularData) this.getAttributeValue(
                    configFacadeMBean, "ApplicationVariables");

                appVarProps = 
                    new ComponentConfigurationHelper().
                        convertToApplicationVariablesProperties(appVarTable);
                 ToolsLogManager.getRuntimeLogger().finer(
                         "getApplicationVariables(): result = " + appVarProps);
                            } 
            catch (JBIRemoteException jbiRE) 
            {
                throw new VerifierException(jbiRE);
            }
            catch (Exception ex) 
            {
                throw new VerifierException(ex);
            }
        }
        return appVarProps;
    }
    
    /**
     * This method is used to get a list of application configurations 
     * available for a component in the given target
     * @pram componentName component name
     * @param target target name
     * @returns Set list of application configurations available
     */
    private Set getApplicationConfigurations(
            String componentName, 
            String target)
    throws VerifierException, JBIRemoteException
    {
        Map<String, Properties> appConfigMap = new HashMap();
        ObjectName configFacadeMBean = 
                this.getComponentConfigurationFacadeMBeanName(
                    componentName, 
                    target);
        ToolsLogManager.getRuntimeLogger().finer(
                "getApplicationConfigurations" + componentName + "," + target
                 + "): configMBean = " + configFacadeMBean);

        if ((configFacadeMBean != null) && 
                ( true == this.isValidTarget(configFacadeMBean))) 
        {
            try 
            {
                TabularData appConfigTable = 
                        (TabularData) 
                        this.getAttributeValue(
                            configFacadeMBean, 
                            "ApplicationConfigurations");
                
                if ( appConfigTable != null )
                {
                    // Convert the application configuration Tabular Data to a Map
                    appConfigMap = getApplicationConfigurationsMap(appConfigTable);
                }

            } 
            catch (Throwable ex) 
            {
                if (ex.getMessage() != null)
                {
                    ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
                }
            }
        }
        return appConfigMap.keySet();        
    }
    
    
   /**
     * This method is used to get the composite type for the application
     * configuration for a component 
     * @pram componentName component name
     * @param target target name
     * @returns CompositeType type of application configuration
     */
    private CompositeType getAppConfigType(
            String componentName, 
            String target)
    throws VerifierException, JBIRemoteException
    {
        CompositeType appConfigType = null;
        ObjectName configFacadeMBean = 
                this.getComponentConfigurationFacadeMBeanName(
                    componentName, 
                    target);
        ToolsLogManager.getRuntimeLogger().finer(
                "getAppConfigType" + componentName + "," + target
                 + "): configMBean = " + configFacadeMBean);

        if ((configFacadeMBean != null) && 
                ( true == this.isValidTarget(configFacadeMBean))) 
        {
            try 
            {
               
                appConfigType = (CompositeType) invokeMBeanOperation(
                    configFacadeMBean, 
                    "queryApplicationConfigurationType",
                    new Object[0],
                    new String[0]);
            } 
            catch (Exception ex) 
            {
                if (ex.getMessage() != null)
                {
                    ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
                }
            }
        }
        return appConfigType;        
    }
    
    
    /**
     * This method is used to find out if a component is started in 
     * the target. If not, the runtime configuration property is checked to 
     * see if the component could be started. If so, the component is started.
     * If not this method retunrs false as the status of the endpoint in this
     * target could not be obtained
     * @param componentName componentName
     * @param target target
     * @returns boolean true if the component is started false otherwise
     */
    public boolean isComponentRunning(String componentName, String target)
    {

        ComponentQuery compQuery = mEnvCtx.getComponentQuery(target);
        ComponentInfo compInfo  = compQuery.getComponentInfo(componentName);
        if (compInfo == null)
        {
            //component not installed
            return false;
        }
        ComponentState compState = compInfo.getStatus();
        if (compState.equals(ComponentState.STARTED))
        {
            return true;
        }
        if (mEnvCtx.isStartOnVerifyEnabled())
        {
            if (startComponent(componentName, target))
            {
                mStartedComponents.put(componentName, compState);
                return true;
            }
        }
        return false;

    }
    
    /**
     * This method is used to start a component in the given target
     * @param componentName component Name
     * @param targetName target name
     * @returns boolean if the component is started successfully false otherwise
     */
    private boolean startComponent(String componentName, String targetName)
    {
        try
        {
            ObjectName lifecycleObjectName = 
                    getComponentLifeCycleMBeanObjectName(
                    componentName, targetName);
        
            Object result = null;
            ToolsLogManager.getRuntimeLogger().finer(
                    "Calling start on Component LifecycleMBean "
                    + lifecycleObjectName);
            result = invokeMBeanOperation(
                    lifecycleObjectName, 
                    "start",
                    new Object[0],
                    new String[0]);
            return true;
        }
        catch (Exception ex)
        {
            ToolsLogManager.getRuntimeLogger().warning(
                    mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_COMPONENT_NOT_STARTED,
                    new Object[]{componentName}));         
            if (ex.getMessage() != null)
            {
                ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
            }
            return false;
        }
    }
    
      
    /**
     * This method is used to determine whether a component supports
     * application configuration.
     * @pram componentName component name
     * @param target target name
     * @returns <code>true</code> if the component supports application
     * configuration, <code>false</code> if not.
     */
    private boolean supportsAppConfig(
            String componentName, 
            String target)
    throws VerifierException, JBIRemoteException
    {
        ObjectName configFacadeMBean = 
                this.getComponentConfigurationFacadeMBeanName(
                    componentName, 
                    target);
        ToolsLogManager.getRuntimeLogger().finer(
                "supportsAppConfig" + componentName + "," + target
                 + "): configMBean = " + configFacadeMBean);

        Boolean supported = Boolean.FALSE;

        if ((configFacadeMBean != null) && 
                ( true == this.isValidTarget(configFacadeMBean))) 
        {
            try 
            {
                supported = (Boolean) invokeMBeanOperation(
                    configFacadeMBean,  "isAppConfigSupported",
                    new Object[0], new String[0]);
            } 
            catch (Throwable ex) 
            {
                if (ex.getMessage() != null)
                {
                    ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
                }
            }
        }
        return supported.booleanValue();
    }
    
    
    /**
     * This method is used to determine whether a component supports
     * application variables.
     * @pram componentName component name
     * @param target target name
     * @returns <code>true</code> if the component supports application
     * variables, <code>false</code> if not.
     */
    private boolean supportsAppVars(
            String componentName, 
            String target)
    throws VerifierException, JBIRemoteException
    {
        ObjectName configFacadeMBean = 
                this.getComponentConfigurationFacadeMBeanName(
                    componentName, 
                    target);
        ToolsLogManager.getRuntimeLogger().finer(
                "supportsAppVars" + componentName + "," + target
                 + "): configMBean = " + configFacadeMBean);

        Boolean supported = Boolean.FALSE;

        if ((configFacadeMBean != null) && 
                ( true == this.isValidTarget(configFacadeMBean))) 
        {
            try 
            {
                supported = (Boolean) invokeMBeanOperation(
                    configFacadeMBean,  "isAppVarsSupported",
                    new Object[0], new String[0]);
            } 
            catch (Throwable ex) 
            {
                if (ex.getMessage() != null)
                {
                    ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
                }
            }
        }
        return supported.booleanValue();
    }
    
   /**
     * This method is used to revert the component back to its own state
     * @param componentName component Name
     * @param compState the component state desired
     * @param targetName target name
     * @returns boolean if the component is shutdown successfully false otherwise
     */
    private boolean revertComponentState(String componentName, ComponentState compState, String targetName)
    {
        try
        {
            ObjectName lifecycleObjectName = 
                    getComponentLifeCycleMBeanObjectName(
                    componentName, targetName);
            
            //ComponentState here could be - Loaded, Unknown, Shutdown, Stopped
            //If stopped, call stop method, otherwise call shutDown
            String lifecycleMethodName = "shutDown";
            if (compState.equals(ComponentState.STOPPED)) {
                lifecycleMethodName = "stop";
            }
            Object result = null;
            ToolsLogManager.getRuntimeLogger().finer(
                    "Calling shutdown on Component LifecycleMBean "
                    + lifecycleObjectName);
            result = invokeMBeanOperation(
                    lifecycleObjectName, 
                    lifecycleMethodName,
                    new Object[0],
                    new String[0]);
            return true;
        }
        catch (Exception ex)
        {
            ToolsLogManager.getRuntimeLogger().warning(
                    mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_COMPONENT_NOT_SHUTDOWN,
                    new Object[]{componentName}));     
            if (ex.getMessage() != null)
            {
                ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
            }
            return false;
        }
    }
        
    /**
     * returns the ObjectName for the lifecycle Mbean of this component.
     * 
     * @return the ObjectName for the lifecycle Mbean.
     * @param componentName
     *            of a binding or engine component.
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     */
    private ObjectName getComponentLifeCycleMBeanObjectName(
            String componentName, String targetName) 
    throws VerifierException 
    {
        ObjectName lifecycleObjectNamePattern = null;
        try 
        {
            lifecycleObjectNamePattern = 
                JBIJMXObjectNames.getComponentLifeCycleMBeanObjectNamePattern(
                        componentName,
                        targetName);
        } 
        catch (MalformedObjectNameException ex) 
        {
            throw new VerifierException(ex);
        }
        
        MBeanServer mbeanServer = mEnvCtx.getMBeanServer();
        
        Set objectNames = mbeanServer.queryNames(
                lifecycleObjectNamePattern,
                null);
        
        if (objectNames.isEmpty()) 
        {
            throw new VerifierException(
               mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_NO_COMPONENT_LIFECYCLE_OBJECT,
                    new Object[]{componentName, targetName}));
        }
        
        if (objectNames.size() > 1) 
        {
            throw new VerifierException(            
                 mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_MANY_COMPONENT_LIFECYCLE_OBJECT,
                    new Object[]{componentName, targetName}));
        }
        
        ObjectName lifecyleObjectName = 
                (ObjectName) objectNames.iterator().next();
        return lifecyleObjectName;
        
    }       
    
    /**
     * invokes the operation on mbean
     * 
     * @return result object
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @param params
     *            parameters
     * @param signature
     *            signature of the parameters
     * @throws JBIRemoteException
     *             on user error
     */
     protected Object invokeMBeanOperation(
             ObjectName objectName,
             String operationName, 
             Object[] params, 
             String[] signature)
     throws JBIRemoteException 
     {
        MBeanServer mbeanServer = mEnvCtx.getMBeanServer();
        Object result = null;
        
        try {
            
            result = mbeanServer.invoke(
                    objectName, 
                    operationName, 
                    params,
                    signature);
            
        } catch (InstanceNotFoundException notFoundEx) {
            throw new JBIRemoteException(notFoundEx);
        } catch (ReflectionException rEx) {
            throw new JBIRemoteException(rEx);
        } catch (MBeanException mbeanEx) {
            throw JBIRemoteException.filterJmxExceptions(mbeanEx);
        } catch (RuntimeMBeanException rtEx) {
            throw JBIRemoteException.filterJmxExceptions(rtEx);
        } catch (RuntimeOperationsException rtOpEx) {
            throw JBIRemoteException.filterJmxExceptions(rtOpEx);
        } catch (Exception ex) {
            throw JBIRemoteException.filterJmxExceptions(ex);
        }
        
        return result;
        
    }
     
    /**
     * Get the component configuration facade MBean name for the target. If the component
     * is not installed on the target, this returns a null. 
     * @param componentName - component id
     * @param targetName - target name
     * @return the ObjectName of the components facade Configuration MBean for a target.
     */
    private ObjectName getComponentConfigurationFacadeMBeanName(
            String componentName, String targetName)
    throws JBIRemoteException, VerifierException
    {
        ObjectName objName = null;
         
        ToolsLogManager.getRuntimeLogger().finer("Get Component Configuration MBean Name for component " + componentName);
        
        ObjectName extensionMBeanObjectName = null;
        try 
        {
            extensionMBeanObjectName = this.getExtensionMBeanObjectName(
                    componentName, targetName);
        } 
        catch (VerifierException exception) 
        {
            if (exception.getMessage() != null)
            {
                ToolsLogManager.getRuntimeLogger().warning(exception.getMessage());
            }
        }
        
        if (extensionMBeanObjectName == null) 
        {
            return objName;
        }
        
        this.checkForValidTarget(extensionMBeanObjectName, targetName);
        
        ToolsLogManager.getRuntimeLogger().finer(
                "Calling getComponentConfigurationFacadeMBeanName on extensionMBeanObjectName = "
                + extensionMBeanObjectName);
        
        Object[] params = new Object[1];
        params[0] = targetName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        objName = (ObjectName) this.invokeMBeanOperation(
                extensionMBeanObjectName, 
                "getComponentConfigurationFacadeMBeanName", 
                params,
                signature);
        
        return objName;
    }     
    
    /**
     * returns the ObjectName for the Extension Mbean of this component.
     * 
     * @param componentName
     * @param targetName
     * 
     * @return the ObjectName of the Extension MBean or null.
     */
    public ObjectName getExtensionMBeanObjectName(
            String componentName,
            String targetName) 
    throws VerifierException 
    {
        ObjectName extensionMBeanObjectNamePattern = null;
        MBeanNames mbeanNames = mEnvCtx.getMBeanNames();
        
        String objectNamePatternString = JBIJMXObjectNames.JMX_JBI_DOMAIN
                + COLON + JBIJMXObjectNames.TARGET_KEY + EQUAL + targetName
                + COMMA + JBIJMXObjectNames.COMPONENT_ID_KEY + EQUAL
                + componentName + COMMA + JBIJMXObjectNames.SERVICE_TYPE_KEY
                + EQUAL + ComponentServiceType.Extension;
        
        try {
            extensionMBeanObjectNamePattern = new ObjectName(
                    objectNamePatternString);
        } catch (MalformedObjectNameException exception) {
            return null;
        } catch (NullPointerException exception) {
            return null;
        }
        
        MBeanServer mbeanServer = mEnvCtx.getMBeanServer();
        
        Set objectNames = mbeanServer.queryNames(
                extensionMBeanObjectNamePattern, null);
        
        if (objectNames.isEmpty()) 
        {
            throw new VerifierException(
               mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_NO_COMPONENT_EXTENSION_OBJECT,
                    new Object[]{componentName, extensionMBeanObjectNamePattern.toString()}));            
            
        }
        
        if (objectNames.size() > 1) 
        {
            throw new VerifierException(
               mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_MANY_COMPONENT_EXTENSION_OBJECT,
                    new Object[]{componentName, extensionMBeanObjectNamePattern.toString()}));            
                       
        }
        
        ObjectName extensionMBeanObjectName = null;
        extensionMBeanObjectName = (ObjectName) objectNames.iterator().next();
         ToolsLogManager.getRuntimeLogger().finer(
                 "extensionMBeanObjectName Found : " + extensionMBeanObjectName);
        
        return extensionMBeanObjectName;
    }
    
    /**
     * Test whether it is a valid target.
     * 
     * @param objectName
     * @return true when the mbean is registered, false otherwise
     * @throws JBIRemoteException
     */
    boolean isValidTarget(ObjectName objectName) throws JBIRemoteException {
        boolean result = false;
        MBeanServer mbeanServer = mEnvCtx.getMBeanServer();
        try {
            result = mbeanServer.isRegistered(objectName);
        } catch (RuntimeException exception) {
        }
        
        return result;
    }    
    
    /**
     * Invokes the mbean and retrieves the attribute value
     * 
     * @param objectName
     *            object name
     * @param attributeName
     *            attribute name
     * @return the value of the attribute
     * @throws JBIRemoteException
     *             on error
     */
    protected Object getAttributeValue(ObjectName objectName,
            String attributeName) throws JBIRemoteException {
        
        MBeanServer mbeanServer = mEnvCtx.getMBeanServer();
        Object result = null;
        try {
            result = mbeanServer.getAttribute(objectName, attributeName);
        } catch (AttributeNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (InstanceNotFoundException e) {
            throw new JBIRemoteException(e);
        } catch (MBeanException e) {
            throw new JBIRemoteException(e);
        } catch (ReflectionException e) {
            throw new JBIRemoteException(e);
        }
        return result;
    }
    
    /**
     * Check if a target is valid or not
     * 
     * @param objectName
     * @throws JBIRemoteException
     */
    void checkForValidTarget(
            ObjectName objectName, 
            String targetName)
    throws JBIRemoteException 
    {
        boolean isRegistered = this.isValidTarget(objectName);
        if (isRegistered == false) {
            String[] args = { targetName };
            throw new JBIRemoteException(
                    mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_INVALID_MBEAN, 
                    new Object[] {objectName, targetName}));
           
        }
    }    
    
     /**
      * Convert a application configuration TabularData to a Map keyed by the 
      * "configurationName". The value is the application configuration represented as 
      * properties.
      *
      * @return a Map of application configuration properties
      */
     private Map<String, Properties> getApplicationConfigurationsMap(TabularData td)
     {
         Map<String, Properties> configMap = new HashMap();
         Set configKeys = td.keySet();
         
         for ( Object configKey : configKeys )
         {
             List keyList = (List)configKey;
             
             String[] index = new String[keyList.size()];
             index = (String[]) keyList.toArray(index);
             CompositeData cd = td.get(index);
             
             Properties configProps = 
             new ComponentConfigurationHelper().convertCompositeDataToProperties(cd);
             configMap.put(index[0], configProps);
         }
         return configMap;
     }    
     
     /**
      * This method is used to find out if JavaEEVerifierMBean is available.
      * This method queries the MBean server to see if the MBean with a particular
      * object name is available.
      * @return true if present and false otherwise
      */
     private boolean isJavaEEVerifierAvailable()
     {
        try
        {
            if (isValidTarget(getJavaEEVerifierMBeanName()))
            {
                return true;
            }         
            else
            {
                return false;
            }
        }
        catch (MalformedObjectNameException malformedObjEx)
        {
            return false;
        }
        catch (JBIRemoteException jbiRe)
        {
            return false;
        } 
        catch (Throwable th)
        {
            return false;
        }
     }
     
     /**
      * This method is used to get the object name for JavaEEVerifier
      * @return ObjectName JavaEEVerifier MBean object name
      */
     private ObjectName getJavaEEVerifierMBeanName()
     throws MalformedObjectNameException
     {
        return new ObjectName(
            JBIJMXObjectNames.JMX_JBI_DOMAIN + COLON  +
            mBeanNames.SERVICE_NAME_KEY + EQUAL + JAVAEE_VERIFIER + COMMA +
            mBeanNames.COMPONENT_TYPE_KEY + EQUAL + mBeanNames.COMPONENT_TYPE_SYSTEM);
     }
     
     /**
      * This method is used to verify all the JavaEEServiceUnits in the given SA.
      * This method calls the JavaEEVerifier for each of the service units and
      * returns the reponse.
      * @param saPath the path to the zip file
      * @param archive JBIArchive
      * @param target the target 
      * @return TabularData[] array with all responses for all JavaEE serviceunits
      */
     private CompositeData[] verifyJavaEEServiceUnits(String saPath, JBIArchive archive, String target)
     {
        try
        {
            ServiceAssemblyDD saDesc = (ServiceAssemblyDD)archive.getJbiDescriptor();
            List<ServiceAssemblyDD.ServiceUnitDD> suDescs = 
                 saDesc.getServiceUnitDDList();
            ArrayList<CompositeData> verifierReports = new ArrayList<CompositeData>();
            
            for (ServiceAssemblyDD.ServiceUnitDD suDesc: suDescs)
            {
                String targetComponent = suDesc.getTargetName();
                String suName = suDesc.getName();
                if (targetComponent != null && targetComponent.equals(JAVAEE_SERVICE_ENGINE))
                {
                    TabularData verifierReport = verifyJavaEEServiceUnit(
                            saPath,
                            suDesc.getArtifactZipName(),
                            target);
                    CompositeData formattedReport = formatJavaEEVerifierReport(suName, verifierReport);
                    verifierReports.add(formattedReport);
                }
            }    
            return (CompositeData[])verifierReports.toArray(new CompositeData[]{});
        }
        catch (Throwable ex)
        {
            //issues in JavaEEVerification do not affect the entire verifier report
            ToolsLogManager.getRuntimeLogger().log(
                    Level.INFO, 
                    mResourceBundle.getMessage(
                        LocalStringKeys.VERIFIER_JAVAEE_VERIFICATION_FAILED),                                   
                    ex);
            return null;
        }
         
     }
     
     
     /**
      * This method is used to call the JavaEEVerifier MBean for a given service unit
      * and return the response from JavaEEVerifier.
      * @param saPath sa path
      * @param suZipName su zip Name
      * @param target the target
      * @return TabularData response from JavaEEVerifier
      */
     private TabularData verifyJavaEEServiceUnit(String saPath, String suZipName, String target)
     throws Exception
     {
         try
         {
            if (saPath == null || suZipName == null )
            {
                ToolsLogManager.getRuntimeLogger().warning(
                        mResourceBundle.getMessage(LocalStringKeys.VERIFIER_JAVAEE_EAR_FILE_NOT_FOUND));                  
                return null;
            }
                
            ToolsLogManager.getRuntimeLogger().finer("Invoking JavaEEVerifier with : " + saPath + suZipName);
            TabularData verifierReport = (TabularData)invokeMBeanOperation(
                    getJavaEEVerifierMBeanName(),
                    "verifyServiceUnit", 
                    new Object[] { saPath, suZipName, target },
                    new String[] { "java.lang.String",  "java.lang.String", "java.lang.String"});
            return verifierReport;
         }
         catch(Throwable t)
         {
            ToolsLogManager.getRuntimeLogger().log(
                    Level.INFO, 
                    mResourceBundle.getMessage(
                        LocalStringKeys.VERIFIER_JAVAEE_VERIFICATION_FAILED),                                   
                    t);
             return null;
         }
     }
     
     /**
      * This method is used to format the verifier report for JavaEE service units
      * @param suName service unit name
      * @param verifierReport verifier report returned by JavaEE verifier mbean
      * @return CompositeData that includes suName and the verification report
      */
     private CompositeData formatJavaEEVerifierReport(String suName, TabularData verifierReport)
     {
         try
         {
             CompositeData formattedReport = null;
             if (verifierReport != null)
             {
                OpenType[] itemTypes = new OpenType[]
                {
                    SimpleType.STRING,
                    verifierReport.getTabularType()
                };  

                Object[] values = new Object[] {
                    suName,
                    verifierReport
                };

                CompositeType cType = new CompositeType(
                        "JavaEEVerifier Report",
                        "Java EE Verifier Report",
                        JAVAEE_VERIFIER_ITEM_NAMES,
                        JAVAEE_VERIFIER_ITEM_DESCRIPTIONS,
                        itemTypes);
                
                formattedReport = new CompositeDataSupport(
                        cType,
                        JAVAEE_VERIFIER_ITEM_NAMES,
                        values);
             }
             return  formattedReport;
         }
         catch (OpenDataException oe)
         {
             if(oe.getMessage() != null)
             {
                ToolsLogManager.getRuntimeLogger().warning(oe.getMessage());             
             }
             return null;
         }
     }
     
    /**
     * This method is used to export the application variables and application
     * configuration objects used by the given application in the specified
     * target.
     * 
     * @param applicationName the name of the application
     * @param targetName the target whose configuration has to be exported
     * @param configDir the dir to store the configurations
     * @returns String the id for the zip file with exported configurations
     * 
     * @throws ManagementRemoteException
     *             if the application configuration could not be exported
     * 
     * Note: param configDir is used between ant/cli and common client client.
     * The return value is used between common client server and common client
     * client.
     * 
     */
    public String exportApplicationConfiguration(
            String applicationName,
            String targetName,
            String configDir)
    throws JBIRemoteException 
    {
       
        mTarget = targetName;
        if( !isApplicationDeployedOnTarget(applicationName, targetName))
        {
            String message = 
                    mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_EXPORT_CONFIG_APP_NOT_DEPLOYED,
                    new Object[]{applicationName, targetName});
            ToolsLogManager.getRuntimeLogger().warning(message);            
            throw new JBIRemoteException(message);
        }
        
        String zipFile = null;
        try
        {
            String saDir = locateInRepository(applicationName);
            ToolsLogManager.getRuntimeLogger().finer("Located Service Assembly in: " + saDir);                         

            mAppVarTemplatesMap = getApplicationVariablesMap(applicationName, saDir);
            ToolsLogManager.getRuntimeLogger().finer("Created list of application variables " + mAppVarTemplatesMap);                                 

            mAppConfigTemplatesMap = getApplicationConfigsMap(applicationName, saDir);
            ToolsLogManager.getRuntimeLogger().finer("Created list of application configs " + mAppConfigTemplatesMap);                                 

            //here dereference the application config values to get a list of application variables
            //inside
            if (mAppConfigTemplatesMap != null )
            {
                for(String component :  mAppConfigTemplatesMap.keySet())
                {
                    List<String> referredVars = new ArrayList<String>();
                    for (String config : mAppConfigTemplatesMap.get(component))
                    {
                         referredVars.addAll(getAppVarReferencedInAppConfig(component, config));
                    }
                    List<String> tmpAppVarList = mAppVarTemplatesMap.get(component);
                    List<String> appVars = removeComponentConfigurations(tmpAppVarList, component);
                    appVars.addAll(referredVars);
                    mAppVarTemplatesMap.put(component, appVars);
                }
            }
            

            mAppConfigTypeMap = getAppConfigTypeMap(mAppConfigTemplatesMap.keySet());
            ToolsLogManager.getRuntimeLogger().finer("Created list of application config types ");    
            
            mAppVarValuesMap = getAppVarsValueMap(mAppVarTemplatesMap, targetName);
            mAppConfigValuesMap = getAppConfigValuesMap(mAppConfigTemplatesMap, targetName);
            

            TemplateGenerator generator = new TemplateGenerator(
                             mEnvCtx,
                             mResourceBundle);        
            zipFile = generator.generateTemplates(
                             mAppVarTemplatesMap, 
                             mAppConfigTemplatesMap,
                             mAppConfigTypeMap,
                             false, // do not include deploy command
                             mTarget, //target
                             true, // isAnt
                             applicationName, //SA name
                             null, // SA path used only for include deploy
                             true, // fillInValues        
                             mAppVarValuesMap, //app var values
                             mAppConfigValuesMap); //app config values

        ToolsLogManager.getRuntimeLogger().finer("Generated templates in " + zipFile);                                         
        }
        catch (Throwable th)
        {
            String message = 
                    mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_ERROR_EXPORTING_APP_CONFIG);
            ToolsLogManager.getRuntimeLogger().log(Level.WARNING, message, th);            
            throw new JBIRemoteException(message);            
        }
        return zipFile;
    }
    
    
    /**
     * This method is used to get a list of application variables
     * used in the given service assembly
     * @param applicationName application name     
     * @param saDir sa dir
     * @return Map map of component name to list of app vars
     */
    private Map<String, List<String>> getApplicationVariablesMap(String applicationName, String saDir)
    {
        
        Map<String, List<String>> appVarMap = new HashMap<String, List<String>>();        
        try
        {
            //this is a map of SU Names to all WSDLs in that SU
            Map<String, File[]> susWsdlMap = mVerifierUtil.getWSDLs(saDir);
          
            Map<String, String> suCompMap = getServiceUnitComponentMap(applicationName, mTarget);
            for (String serviceUnit : susWsdlMap.keySet() )
            {
                String component = suCompMap.get(serviceUnit);

                if (component != null && !component.equals(JAVAEE_SERVICE_ENGINE))
                {
                    List<String> appVarListForComponent = new ArrayList<String>();
                    File[] wsdls = susWsdlMap.get(serviceUnit);
                    for (int j=0; j<wsdls.length; j++)
                    {
                        try
                        {
                            String[] endpointNames = mVerifierUtil.getEndpointName(wsdls[j]);  
                            for (int iter=0; iter<endpointNames.length; iter++)                        
                            {
                                appVarListForComponent.addAll(
                                        mVerifierUtil.getApplicationVariables(
                                                wsdls[j], 
                                                endpointNames[iter]));
                            }
                        } catch (VerifierException ex)
                        {
                            //the wsdl is not proper, on exception move to next wsdl
                            continue;
                        }
                    }
                    appVarMap.put(component, appVarListForComponent);    
                    ToolsLogManager.getRuntimeLogger().finer(
                            "Application Variables List for Component " + component
                            + " is " + appVarListForComponent);                                
                }
            }
        }
        catch (Exception ex)
        {
            //continue with app config processing  
            String message = 
                    mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_ISSUE_COLLECTING_LIST_OF_APP_VARS,
                    new Object[]{applicationName});
            ToolsLogManager.getRuntimeLogger().log(Level.WARNING, message, ex);
        }
        return appVarMap;
    }
    
    /**
     * This method is used to get a list of application configurations used 
     * in the given SA
     * @param applicationName app name
     * @param saDir sa dir
     * @return Map map of component name to configurations 
     */
    private Map<String, List<String>> getApplicationConfigsMap(
            String applicationName, 
            String saDir)
    {
        Map<String, List<String>> appConfigMap = new HashMap<String, List<String>>();        
        try
        {
            Map<String, String> suCompNameMap = getServiceUnitComponentMap(applicationName, mTarget);
            for (String serviceUnitName : suCompNameMap.keySet())
            {
                try
                {
                    String component = suCompNameMap.get(serviceUnitName);
                    if (component != null && !component.equals(JAVAEE_SERVICE_ENGINE))
                    {
                        String suDir = saDir + File.separator + serviceUnitName + File.separator + component;
                        Map<String, List<String>> endpointConfigMap = mVerifierUtil.getEndpointConfigMap(suDir);          
                        List<String> appConfigListForComponent = new ArrayList<String>();
                        for (String endpointName : endpointConfigMap.keySet())
                        {
                            List<String> configs = endpointConfigMap.get(endpointName);
                            if (configs != null)
                            {
                                appConfigListForComponent.addAll(endpointConfigMap.get(endpointName));
                            }
                        }
                        appConfigMap.put(component, appConfigListForComponent);
                    }
                }
                catch (VerifierException vEx)
                {
                    //move to the next su
                    if (vEx.getMessage() != null)
                    {
                        ToolsLogManager.getRuntimeLogger().warning(vEx.getMessage());     
                    }
                    continue;
                }
            }

        }
        catch (Exception vEx)
        {
            //report error and return the current list
            String message = 
                    mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_ISSUE_COLLECTING_LIST_OF_APP_CONFIGS,
                    new Object[]{applicationName});
            if (vEx.getMessage() != null)
            {
                ToolsLogManager.getRuntimeLogger().log(Level.WARNING, message, vEx);
            }
        }
        return appConfigMap;
    }
    
    /**
     * This method is used to find out if the given application is deployed in 
     * the given target 
     * @param applicationName application name
     * @param target target
     * @return true if the application is deployed
     */
    private boolean isApplicationDeployedOnTarget(String applicationName, String target)
    {

        try
        {
            String[] saList =  (String[])  this.getAttributeValue(
                 getDeploymentServiceFacadeMBeanName(target),
                 "DeployedServiceAssemblies");
             
             if (saList != null)
            {
                for(int i = 0; i < saList.length; i++)
                {
                    if (applicationName.equals(saList[i]))
                    {
                        return true;
                    }
                }
            
            }
            return false;
        }
        catch (Throwable ex)
        {
            //if SA query fails for any reason do not proceed further with export
            if (ex.getMessage() != null)
            {
                ToolsLogManager.getRuntimeLogger().warning(ex.getMessage());
            }
            return false;
        }
        
    }
    
           
    /**
     * This method is used to find out the path to the SA in the repository
     * @param saName application name
     * @return String the location
     * @throws VerifierException if there are issues in locating the sa
     */
    private String locateInRepository(String saName)
    throws VerifierException
    {
        try
        {
            //Note: This method relies upon the SA store dir layout
            //to access the SU files
            File saStore = new File(
                    mEnvCtx.getAppServerInstanceRoot(),
                    REPOSITORY_SA_STORE);
            File saDir = new File(saStore, saName);
            if (saDir.exists())
            {
                return saDir.getAbsolutePath();
            }
            else
            {
                throw new VerifierException(mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_APPLICATION_NOT_LOCATED,
                    new Object[]{saName}));
            }
        }
        catch (Throwable ex)
        {
            throw new VerifierException(ex);
        }
    }
    
     /**
      * This method is used to create a map of service unit names and 
      * target component in the given application.
      * @param applicationName application name
      * @param targetName target name
      * @return Map<String, String> map of suNames to component names 
      */
     private Map<String, String> getServiceUnitComponentMap(String applicationName, String targetName)
     throws VerifierException
     {
         Map<String, String> suCompMap = new HashMap<String, String>();
         try
         {
             ComponentQuery compQuery = mEnvCtx.getComponentQuery(targetName);
             String[] componentList =  (String[])  this.invokeMBeanOperation(
                 getDeploymentServiceFacadeMBeanName(targetName),
                 "getComponentsForDeployedServiceAssembly", 
                 new Object[] { applicationName },
                 new String[] { "java.lang.String"});

             for(int i=0; i<componentList.length; i++)
             {
                ComponentInfo compInfo = compQuery.getComponentInfo(componentList[i]);
                List<ServiceUnitInfo> suInfoList = compInfo.getServiceUnitList();                     
                for (ServiceUnitInfo suInfo :  suInfoList) 
                {
                    if (suInfo.getServiceAssemblyName().equals(applicationName))
                    {
                        suCompMap.put(suInfo.getName(), componentList[i]);
                    }
                }
             }
  
         }
         catch (Throwable ex)
         {
             throw new VerifierException(ex);
         }
         return suCompMap;
     }
     
     
    /**
     * This mtehod is used to get the name of the DeploymentService Facade MBean name
     * for this target
     * @param targetName - target name
     * @return the ObjectName of the DeploymentService facade MBean for a target.
     */
    private ObjectName getDeploymentServiceFacadeMBeanName(String targetName)
    throws VerifierException
    {
        return mBeanNames.getSystemServiceMBeanName(
                ServiceName.DeploymentService, 
                ServiceType.Deployment,
                targetName);       
    }
    
    /**
     * This method is used to create a map of component names to their
     * app config type
     * @param componentList the list of components
     * @return Map<String, CompositeType> map of component names to Config type
     */
    private Map<String, CompositeType> getAppConfigTypeMap(Set<String> componentList)
    {
        Map<String, CompositeType> configTypeMap = new HashMap<String, CompositeType>();
        for(String component:componentList)
        {
            try
            {
                configTypeMap.put(component, getAppConfigType(component, mTarget));
            }
            catch (VerifierException verEx)
            {
                //move on to next component
                if (verEx.getMessage() != null)
                {
                    ToolsLogManager.getRuntimeLogger().warning(verEx.getMessage());                            
                }
                continue;
            }
            catch (JBIRemoteException jbiRe)
            {
                //move on to next component
                String message = 
                        mResourceBundle.getMessage(
                        LocalStringKeys.VERIFIER_ISSUE_COLLECTING_LIST_OF_APP_CONFIG_TYPE);
                ToolsLogManager.getRuntimeLogger().log(Level.WARNING, message, jbiRe);
            }
        }
        return configTypeMap;
    }
    
    
    /**
     * This method is used to create a map of component names mapped to
     * a map of app. var names and corresponding values.
     * @param mAppVarTemplatesMap map of component names and list of app. variables
     * @param targetName target name
     * @return Map<String, Map<String, Properties>> map of component names to map
     * of app. vars and corresponding values.
     *
     */
    private Map<String, Map<String, Object>> getAppVarsValueMap(
            Map<String, List<String>> appVarTemplatesMap, String targetName)
    {
        Map<String,Map<String, Object>> appVarValuesMap = new HashMap<String, Map<String, Object>>();
        try
        {
            for(String component:appVarTemplatesMap.keySet())
            {
                Map<String, Object> valuesNeededForComponent = new HashMap<String, Object>();                
                try
                {
                    Properties appVarProps = new Properties();
        
                    ObjectName configFacadeMBean = this
                        .getComponentConfigurationFacadeMBeanName(component, targetName);

                    if ((configFacadeMBean != null)
                        && (true == this.isValidTarget(configFacadeMBean))) 
                    {
                        TabularData appVarTable = 
                                (TabularData) this.getAttributeValue(
                                    configFacadeMBean, 
                                    "ApplicationVariables");
                
                        appVarProps = new ComponentConfigurationHelper()
                        .convertToApplicationVariablesProperties(appVarTable);
                        
                        for (Object varName : appVarProps.keySet())
                        {
                            if (appVarTemplatesMap.get(component).contains(varName))
                            {
                                valuesNeededForComponent.put(
                                        (String)varName, 
                                         appVarProps.get(varName));
                            }
                        }
                    }
                    appVarValuesMap.put(component, valuesNeededForComponent);
                }
                catch(Throwable th)
                {
                    String message = 
                        mResourceBundle.getMessage(
                        LocalStringKeys.VERIFIER_ISSUE_GETTING_APP_VAR_VALUE,
                            new Object[]{component});
                    ToolsLogManager.getRuntimeLogger().log(Level.WARNING, message, th);                                
                }
            }
        }
        catch(Throwable th)
        {
            //return the empty map
            String message = 
                    mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_ISSUE_GETTING_APP_VAR_VALUE);
            ToolsLogManager.getRuntimeLogger().log(Level.WARNING, message, th);            
        }
        return appVarValuesMap;
        
    }
          
    /**
     * This method is used to create a map of component names mapped to
     * a map of app. config names and corresponding values.
     * @param appConfigTemplatesMap map of component names and list of app. variables
     * @param targetName target name
     * @return Map<String, Map<String, Properties>> map of component names to map
     * of app. vars and corresponding values.
     *
     */
    private Map<String, Map<String, Properties>> getAppConfigValuesMap(
            Map<String, List<String>> appConfigTemplatesMap, String targetName)
    {
        Map<String,Map<String, Properties>> appConfigValuesMap = new HashMap<String, Map<String, Properties>>();
        try
        {
            for(String component:appConfigTemplatesMap.keySet())
            {
                Map<String, Properties> valuesNeededForComponent = new HashMap<String, Properties>();                
                try
                {
                    Map<String, Properties> appConfigMap = 
                        getApplicationConfigurationsAsMap(component, targetName);

                    for (String configName : appConfigMap.keySet())
                    {
                        if (appConfigTemplatesMap.get(component).contains(configName))
                        {
                            valuesNeededForComponent.put(
                                    configName, 
                                    appConfigMap.get(configName));
                        }
                    }
                appConfigValuesMap.put(component, valuesNeededForComponent);
                }
                catch(Throwable th)
                {
                    String message = 
                        mResourceBundle.getMessage(
                        LocalStringKeys.VERIFIER_ISSUE_GETTING_APP_CONFIG_VALUE,
                            new Object[]{component});
                    ToolsLogManager.getRuntimeLogger().warning(message);        
                    ToolsLogManager.getRuntimeLogger().log(Level.WARNING, message, th);                                
                }
            }
        }
        catch(Throwable th)
        {
            //return the empty map
            String message = 
                    mResourceBundle.getMessage(
                    LocalStringKeys.VERIFIER_ISSUE_GETTING_APP_CONFIG_VALUE);
            ToolsLogManager.getRuntimeLogger().log(Level.WARNING, message, th);            
        }
        return appConfigValuesMap;
        
    }
    
    
    /**
     * This method is used to get a list of application configurations 
     * for a component as a Map
     * @param component component
     * @param target target
     */
    private Map<String, Properties> getApplicationConfigurationsAsMap(String component, String target)
    throws VerifierException
    {
        Map<String, Properties> appConfigMap = new HashMap<String, Properties>();                        
        try
        {
            ObjectName configFacadeMBean = this
                    .getComponentConfigurationFacadeMBeanName(component, target);

            if ((configFacadeMBean != null)
                && (true == this.isValidTarget(configFacadeMBean))) 
            {

                TabularData appConfigTable = 
                        (TabularData) this.getAttributeValue(
                            configFacadeMBean, 
                            "ApplicationConfigurations");
                appConfigMap = getApplicationConfigurationsMap(appConfigTable);        
            }
        } 
        catch (JBIRemoteException ex)
        {
            throw new VerifierException(ex);
        }
        return appConfigMap;
    }
         
}

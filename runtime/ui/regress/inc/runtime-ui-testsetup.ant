<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)runtime-ui-testsetup.ant
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<project name="taskdefs">
   <!--
    # We assume that you have passed in the <JBI_DOMAIN_ROOT>/jbi/config/jbienv.properties file:
    #    ant -propertyfile $JBI_DOMAIN_ROOT/jbi/config/jbienv.properties ...
    -->
   <!--
    # DEFINE JMX4ANT typdefs and tasks:
    -->
   <typedef name="mbean"          classname="org.apache.tools.ant.types.optional.MBeanType"/>
   <typedef name="context"        classname="org.apache.tools.ant.types.optional.ContextType"/>
   <taskdef name="configureMBean" classname="org.apache.tools.ant.taskdefs.optional.jmx.ConfigureMBeanTask"/>
   <taskdef name="copyMBean"      classname="org.apache.tools.ant.taskdefs.optional.jmx.CopyMBeanTask"/>
   <taskdef name="removeMBean"    classname="org.apache.tools.ant.taskdefs.optional.jmx.RemoveMBeanTask"/>
   <taskdef name="createMBean"    classname="org.apache.tools.ant.taskdefs.optional.jmx.CreateMBeanTask"/>
   <taskdef name="invokeMBean"    classname="jmx4ant.InvokeMBeanTask"/> 
   <taskdef name="showMBean"      classname="org.apache.tools.ant.taskdefs.optional.jmx.ShowMBeanTask"/>
   <!--
    # Use ant-contrib tasks (includes "foreach" task):
    -->
   <taskdef resource="net/sf/antcontrib/antlib.xml"/>
   <taskdef resource="com/sun/jbi/ui/ant/antlib.xml" />

   <!--
   # Include our XML canonicalizer task
   -->
   <taskdef name="canonicalizeXml" classname="com.sun.jbi.internal.ant.tools.CanonicalizerTask" />

   <target name="runtime_ui_test_init" depends="set-global,set-defaults,set-mbeanrefs">
      <echo message="JBI_HOME is ${env.JBI_HOME}"/>
      <echo message="jmx.provider is ${jmx.provider}"/>
   </target>


   <target name="set-defaults">
      <property name="jbi_jmx_domain" value="com.sun.jbi"/>
      <!-- these come from JBI_DOMAIN_ROOT property file: -->
      <property name="jbi_instance_name" value="${env.JBI_INSTANCE_NAME}"/>
      <property name="jmx.provider" value="${env.AS_JMX_REMOTE_URL}"/>
      <!-- these must be defined in the test environment: -->
      <property name="jmx.user" value="${env.AS_ADMIN_USER}"/>
      <property name="jmx.password" value="${env.AS_ADMIN_PASSWD}"/>
      <!-- build products folder: -->
      <property name="runtime_ui_bld_dir" value="${env.JV_SVC_BLD}"/>
      <property name="runtime_ui_bld_regress" value="${env.JV_SVC_TEST_CLASSES}"/>
      <property name="regress.dist.dir" location="${runtime_ui_bld_regress}/dist"/>
      <property name="testdata.dir" location="${runtime_ui_bld_regress}/testdata"/>
      <property name="test.classes.dir" location="${runtime_ui_bld_regress}/test"/>

   </target>


   <target name="set-global">
      <property environment="env"/>
   </target>

   <target name="set-mbeanrefs">
      <mbean id="JbiReferenceAdminUiService" providerUrl="${jmx.provider}" 
             name="com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System" 
             serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>
      <mbean id="ServerStats" providerUrl="${jmx.provider}" 
             name="com.sun.jbi:JbiName=server,ServiceName=MessageService,ControlType=Statistics,ComponentType=System" 
             serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>
      <mbean id="Instance1Stats" providerUrl="${jmx.provider}" 
             name="com.sun.jbi:JbiName=instance1,ServiceName=MessageService,ControlType=Statistics,ComponentType=System" 
             serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>

   </target>
   
   <target name="process-text-result">
      <echo message="##### Result of ${operation} = ${result_data}"/>
   </target>
      
  
   <target name="verify.application" 
           description="Run a java test class to invoke verifier">
        
        <java classname="java4ant.TestVerifier" 
              outputproperty="TestVerifier.result">
        <sysproperty key="jmx.provider" value="${jmx.provider}"/>
        <sysproperty key="jmx.user" value="${jmx.user}"/>
        <sysproperty key="jmx.password" value="${jmx.password}"/>

        <sysproperty key="application_url" value="${application_url}"/>
        <sysproperty key="target" value="${target}"/>
        <sysproperty key="generate_templates" value="${generate_templates}"/>
        <sysproperty key="template_dir" value="${template_dir}"/>
        <sysproperty key="include_deploy_command" value="${include_deploy_command}"/>
        <arg value="verify"/>
      </java>
      <echo message="${TestVerifier.result}"/>
   </target>

   <target name="verify.application.ant.task" 
           description="Run ant task for verifier">
        <jbi-verify-application-environment
              username="${jmx.username}"
              password="${jmx.password}"
              host="localhost"
              port="${jbi.port}"
              secure="false"      
              failOnError="true"     
              file="${application_url}"
              templatedir="${template_dir}"
              includedeploy="${include_deploy_command}"
              target="${target}"/>

   </target>

   <target name="export.application" 
           description="Run a java test class to invoke verifier">
        
        <java classname="java4ant.TestVerifier" 
              outputproperty="TestVerifier.result">

        <sysproperty key="jmx.provider" value="${jmx.provider}"/>
        <sysproperty key="jmx.user" value="${jmx.user}"/>
        <sysproperty key="jmx.password" value="${jmx.password}"/>
        <sysproperty key="application_name" value="${application_name}"/>
        <sysproperty key="target" value="${target}"/>
        <sysproperty key="config_dir" value="${config_dir}"/>
        <arg value="export"/>
      </java>
   </target>

   <target name="get.framework.stats">
        <java classname="java4ant.TestStats" outputproperty="TestStats.result">
            <sysproperty key="jmx.provider" value="${jmx.provider}"/>
            <sysproperty key="jmx.user" value="${jmx.user}"/>
            <sysproperty key="jmx.password" value="${jmx.password}"/>
            <sysproperty key="target" value="${target}"/>
            <arg value="Framework"/>
        </java>
      <echo message="${TestStats.result}"/>
   </target>

   <target name="get.nmr.stats">
        <java classname="java4ant.TestStats" outputproperty="TestStats.result">
            <sysproperty key="jmx.provider" value="${jmx.provider}"/>
            <sysproperty key="jmx.user" value="${jmx.user}"/>
            <sysproperty key="jmx.password" value="${jmx.password}"/>
            <sysproperty key="target" value="${target}"/>
            <arg value="NMR"/>
        </java>
      <echo message="${TestStats.result}"/>
   </target>

   <target name="get.component.stats">
        <java classname="java4ant.TestStats" outputproperty="TestStats.result">
            <sysproperty key="jmx.provider" value="${jmx.provider}"/>
            <sysproperty key="jmx.user" value="${jmx.user}"/>
            <sysproperty key="jmx.password" value="${jmx.password}"/>
            <sysproperty key="target" value="${target}"/>
            <arg value="Component"/>
            <arg value="${component}"/>
        </java>
      <echo message="${TestStats.result}"/>
   </target>

   <target name="get.endpoint.stats">
        <java classname="java4ant.TestStats" outputproperty="TestStats.result">
            <sysproperty key="jmx.provider" value="${jmx.provider}"/>
            <sysproperty key="jmx.user" value="${jmx.user}"/>
            <sysproperty key="jmx.password" value="${jmx.password}"/>
            <sysproperty key="target" value="${target}"/>
            <arg value="Endpoint"/>
            <arg value="${endpoint}"/>
        </java>
      <echo message="${TestStats.result}"/>
   </target>

   <target name="get.sa.stats">
        <java classname="java4ant.TestStats" outputproperty="TestStats.result">
            <sysproperty key="jmx.provider" value="${jmx.provider}"/>
            <sysproperty key="jmx.user" value="${jmx.user}"/>
            <sysproperty key="jmx.password" value="${jmx.password}"/>
            <sysproperty key="target" value="${target}"/>
            <arg value="SA"/>
            <arg value="${serviceassembly}"/>
        </java>
      <echo message="${TestStats.result}"/>
   </target>

   <target name="get.consuming.endpoints.list">
        <java classname="java4ant.TestStats" outputproperty="TestStats.result">
            <sysproperty key="jmx.provider" value="${jmx.provider}"/>
            <sysproperty key="jmx.user" value="${jmx.user}"/>
            <sysproperty key="jmx.password" value="${jmx.password}"/>
            <sysproperty key="target" value="${target}"/>
            <arg value="listconsumingendpoints"/>
            <arg value="${componentName}"/>
        </java>
      <echo message="${TestStats.result}"/>
   </target>

   <target name="get.providing.endpoints.list">
        <java classname="java4ant.TestStats" outputproperty="TestStats.result">
            <sysproperty key="jmx.provider" value="${jmx.provider}"/>
            <sysproperty key="jmx.user" value="${jmx.user}"/>
            <sysproperty key="jmx.password" value="${jmx.password}"/>
            <sysproperty key="target" value="${target}"/>
            <arg value="listprovidingendpoints"/>
            <arg value="${componentName}"/>
        </java>
      <echo message="${TestStats.result}"/>
   </target> 

    <target name="test.component.logger" description="Test component logger level ops">      
      <java classname="java4ant.TestComponentLogger" outputproperty="TestComponentLogger.result">
            <sysproperty key="jmx.provider" value="${jmx.provider}"/>
            <sysproperty key="jmx.user" value="${jmx.user}"/>
            <sysproperty key="jmx.password" value="${jmx.password}"/>
            <sysproperty key="target" value="${target}"/>
        </java>    
        <echo message="${TestComponentLogger.result}"/> 
   </target>

</project>

#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)runtime-ui-00101.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# Stats Test on server
####

echo "runtime-ui-00150 : Test Stats on cluster."

. ./regress_defs.ksh

echo Stopping instance : instance1
asadmin stop-instance --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords instance1

asadmin create-cluster  --port $ASADMIN_PORT --user $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords testcluster 
asadmin create-instance  --port $ASADMIN_PORT --user $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords --cluster testcluster --nodeagent agent1 TestInstance1
asadmin create-instance  --port $ASADMIN_PORT --user $AS_ADMIN_USER --passwordfile $JV_AS8BASE/passwords --cluster testcluster --nodeagent agent1 TestInstance2

asadmin start-instance --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords TestInstance1
asadmin start-instance --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords TestInstance2

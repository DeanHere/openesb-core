/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminValueConverter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  AdminValueConverter.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 29, 2005, 2:35 PM
 */

package jmx4ant;

import org.apache.tools.ant.taskdefs.optional.jmx.converter.ValueConverter;
import org.apache.tools.ant.taskdefs.optional.jmx.converter.ValueFactory;
import java.util.ArrayList;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class AdminValueConverter
    implements ValueConverter
{
    
    public String[] getSupportedTypes()
    {
        return new String[]{"java.util.List"};
    }
    
    public Object valueOf(String value, String type)
        throws Exception
    {
        if ( isTypeSupported(type) )
        {
            // -- The only supported type is List
            return convertStringToList(value);         
        }
        else
        {
            throw new Exception("Unsupported Type : " + type);
        }
    }

    /**
     * @return a List created from the String value.
     */
    private java.util.List convertStringToList(String value)
    {
        ArrayList list = new ArrayList();
        value = value.trim();

        if ( value.charAt(0) == '[' && value.charAt(value.length() - 1 ) == ']')
        {
            String[] values = ( value.substring (1, value.length() - 1) ).split("\\s");
            for ( int i=0; i<values.length; i++ )
            {
                list.add(values[i]);
            }
        }
        return list;
    }
    /**
     * Check if this value Converter supports the type.
     */
    private boolean isTypeSupported(String type)
    {
        String[] types = getSupportedTypes();
        for ( int i = 0; i < types.length; i++ )
        {
            if ( types[i].equals(type) )
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Set the ValueConverter
     */
    public static void main(String[] args)
    {     
        ValueConverter vc = new AdminValueConverter();
        ValueFactory.getInstance().registerValueConverter(vc);
        
        /**
        try
        {
        java.util.List list = (java.util.List) vc.valueOf("[CAS ESBMember]", "java.util.List");
        System.out.println("Size : " + list.size());
        
        for ( java.util.Iterator itr = list.iterator(); itr.hasNext(); )
        {
            System.out.println("List item : " + (String)itr.next());
        }
        }
        catch (Exception ex)
        {
            
        }
         **/
    }
}

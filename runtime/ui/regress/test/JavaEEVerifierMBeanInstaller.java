/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JavaEEVerifierMBeanInstaller.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package test;

import com.sun.appserv.server.LifecycleEvent;
import com.sun.appserv.server.LifecycleEventContext;
import com.sun.appserv.server.LifecycleListener;
import com.sun.appserv.server.ServerLifecycleException;

import java.util.Properties;
import java.util.logging.Logger;
import javax.management.openmbean.OpenDataException;
import javax.management.ObjectName;
import javax.management.StandardMBean;
import javax.management.MBeanServer;
import java.lang.management.ManagementFactory;

/**
 *
 * This class is used to add the JavaEEVerifierMBean in the MBean Server.
 * This class is used to test the contract between JBI application verifier
 * and JavaEEVerifier.
 *
 * @author Sun Microsystems, Inc.
 */
public class JavaEEVerifierMBeanInstaller
implements LifecycleListener
{
    
    /**
     * Provide the processing for AppServer lifecycle events. For each
     * event, a corresponding method is called to perform the processing
     * for that event.
     * @param anEvent LifecycleEvent from AppServer
     * @throws ServerLifecycleException if any error occurs processing
     * an event.
     */
    public void handleEvent(LifecycleEvent anEvent)
        throws ServerLifecycleException
    {
        // This entire method must be enclosed in a try-catch block to
        // prevent any exception other than ServerLifecycleException from
        // being thrown. Any other exception causes the AppServer startup
        // to fail.

        try
        {
            LifecycleEventContext eventContext =
                anEvent.getLifecycleEventContext();
            
            if ( LifecycleEvent.STARTUP_EVENT == anEvent.getEventType() )
            {
                addJavaEEVerifierMBean(eventContext);
            }
        }
        catch ( Throwable ex )
        {
            throw new ServerLifecycleException(ex);
        }

        return;
    }
    

    /**
     * This method is used to add the JavaEEVerifier MBean
     * @param LifecycleEventContext event context
     */
    public void addJavaEEVerifierMBean(LifecycleEventContext eventContext)
    {
        try
        {
            ObjectName mBeanObjName = new ObjectName(
                "com.sun.jbi:ServiceName=JavaEEVerifier,ComponentType=System");
            StandardMBean mbean = new StandardMBean(
                    new TestJavaEEVerifierMBeanImpl(),
                    test.TestJavaEEVerifierMBean.class);
            ManagementFactory.getPlatformMBeanServer().registerMBean(mbean, mBeanObjName);        
        }
        catch(javax.management.MalformedObjectNameException ex)
        {
            System.out.println("JavaEEVerifierMBean could not be added" + ex.getMessage());
        }
        catch(javax.management.NotCompliantMBeanException ex)
        {
            System.out.println("JavaEEVerifierMBean could not be added" + ex.getMessage());
        }
        catch (javax.management.InstanceAlreadyExistsException ex)
        {
            System.out.println("JavaEEVerifierMBean could not be added" + ex.getMessage());
        }
        catch (javax.management.MBeanRegistrationException ex)
        {
            System.out.println("JavaEEVerifierMBean could not be added" + ex.getMessage());            
        }
    
    }
}


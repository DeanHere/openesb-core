/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBossASJBIFramework.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.jboss;

import com.sun.jbi.framework.JBIFramework;

import java.util.Properties;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


/**
 * This is the JBI Framework class for JBoss.
 *
 * @author Sun Microsystems, Inc.
 */
public class JBossASJBIFramework extends JBIFramework
    implements JBossASJBIFrameworkMBean {
    /**
     * JBI Home property, which is the install root.
     */
    private static final String JBI_HOME = "com.sun.jbi.home";

    /**
     * Jboss naming service.
     */
    private static final String JBOSS_NAMING_SERVICE = "jboss:service=Naming";

    /**
     * key for the install root property *
     */
    public static final String INSTALL_ROOT = "install.root";

    /**
     * key for the instance name property *
     */
    public static final String INSTANCE_NAME = "instance.name";

    /**
     * key for the instance root property.
     */
    public static final String INSTANCE_ROOT = "instance.root";

    /**
     * Configuration defaults.
     */
    private static final String DEFAULT_INSTALL_ROOT = System.getProperty(
            "user.dir");

    /**
     * Default Jboss instance name.
     */
    private static final String DEFAULT_INSTANCE_NAME = "default";

    /**
     * Naming prefix for Jboss
     */
    private static final String JBOSS_NAMING_PREFIX = "";

    /**
     * Determines wheter or not the Framework has been loaded.
     */
    private boolean mLoaded;

    /**
     * The Logger instance for the framework.
     */
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());

    /**
     * Folder where JBoss is installed.
     */
    private String mInstallRoot;

    /**
     * JMX connector server for accepting JMX connections.
     */
    private JMXConnectorServer mConnectorServer;

    /**
     * platform context instance
     */
    private JBossPlatformContext mPlatformContext;

    public JBossASJBIFramework(Properties env) {
        super();
        mInstallRoot = env.getProperty(INSTALL_ROOT, DEFAULT_INSTALL_ROOT);

        mPlatformContext = new JBossPlatformContext(env.getProperty(
                    INSTANCE_NAME, DEFAULT_INSTANCE_NAME), mInstallRoot,
                env.getProperty(INSTANCE_ROOT, DEFAULT_INSTALL_ROOT));
    }

    /**
     * This method is called by the Bootstrap to load the JBI Framework.
     *
     * @throws java.lang.Exception any exception.
     */
    public void load() throws java.lang.Exception {
        try {
            Properties props = new Properties();
            props.put(JBI_HOME, mInstallRoot);

            MBeanServer mbs;
            mbs = mPlatformContext.getMBeanServer();
            startJMXConnectorServer(mbs);
            super.init(mPlatformContext, props);
        } catch (javax.jbi.JBIException je) {
            throw je;
        }

        try {
            InitialContext ctx = new InitialContext();
            Context subctx = null;

            try {
                Object obj = ctx.lookup("jbi");

                // great ther is already one
                subctx = (Context) obj;
            } catch (NamingException ne) {
                subctx = ctx.createSubcontext("jbi");
            }

            mConnectorServer.start();
            super.startup(ctx, JBOSS_NAMING_PREFIX);
            super.prepare();
            mLog.info("JBI Framework - Created");
            super.ready(true);
            mLog.info("JBI Framework - Started");
            // JBI framework has been loaded
            mLoaded = true;
        } catch (javax.jbi.JBIException sje) {
            throw sje;
        }
    }

    /**
     * Starts the JMX connector server. This is a feature that is supported
     * in J2SE 1.5 ( JMX-remoting). When run on 1.4 the jmx-remoting jar should
     * be supplied. This is only a temporary measure as the Jboss AS 4.0.3
     * does not have a JSR 160 implementation. Once the implementation is
     * available that can be used instead of the jms-remoting RI.
     *
     * @param mbs Mbean server to be used.
     */
    private void startJMXConnectorServer(MBeanServer mbs) {
        try {
            ObjectName mbeanName = new ObjectName(JBOSS_NAMING_SERVICE);
            Integer port = (Integer) mbs.getAttribute(mbeanName, "Port");

            String jmxurl = "service:jmx:rmi:///jndi/jnp://localhost:" +
                port.toString() + "/jbiconnector";
            mLog.info("JMX Url: " + jmxurl);

            JMXServiceURL url = new JMXServiceURL(jmxurl);
            mConnectorServer = JMXConnectorServerFactory.newJMXConnectorServer(url,
                    null, mbs);
            mLog.info("RMI Connector server started - JMXServiceURL to access " +
                "   the mbean server would be " + jmxurl);
        } catch (Exception e) {
            mLog.severe("Cannot create Connector " + e.getMessage());
        }
    }

    /**
     * Queries the state of the JBI Framework.
     *
     * @return true if the JBI framework is loaded, false otherwise.
     */
    public boolean isLoaded() {
        return mLoaded;
    }

    /**
     * Unloads the JBI framework.  When this method retuns, all
     * public interfaces, system services, and JMX connector (if configured)
     * have been destroyed.
     *
     * @throws java.lang.Exception failed to unload JBI framework
     */
    public synchronized void unload() throws Exception {
        if (!mLoaded) {
            return;
        }

        shutdown();
        mLog.info("JBI Framework - Stopped");
        mConnectorServer.stop();
        mLog.info("JBI RMI Connector server stopped ");
        terminate();

        mLoaded = false;
    }
}

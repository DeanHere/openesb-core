<?xml version="1.0" encoding="UTF-8"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)pom.xml
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<project>
    <parent>
        <artifactId>build-common</artifactId>
        <groupId>open-esb</groupId>
        <version>1.0</version>
        <relativePath>../../build-common</relativePath>
    </parent>
    <modelVersion>4.0.0</modelVersion>
    <groupId>open-esb</groupId>
    <artifactId>jboss5-framework</artifactId>
    <name>jboss5-framework-services</name>
    
    <version>${openesb.currentVersion}</version>
    <description>JBI Framework Services</description>
    <build>
        <resources>
            <resource>
                <directory>src</directory>
                <includes>
                    <include>**/*.properties</include>
                </includes>
            </resource>
        </resources>
        <plugins>
          
            <plugin>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>${project.artifactId}-ant-junit</id>
                        <phase>test</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <tasks>
                                <patternset id="openesb.junit.test.patternset" >
                                    <include name="**/Test*.java"/>
                                </patternset>

                                <!--
                                 ! This is a method to add elements to the maven test classpath
                                 ! without publishing each test artifact in the local repository.
                                 ! you can also use a dependency with <scope>system, but this adds
                                 ! the test jars to all paths which is undesirable.  RT 7/31/06
                                -->
                                <property name="framework.maventestclasspath" refid="maven.test.classpath"/>
                                <path id="maven.test.classpath">
                                    <pathelement path="${framework.maventestclasspath}"/>
                                    <fileset dir="${project.build.testOutputDirectory}">
                                        <include name="**/*.jar"/>
                                    </fileset>
                                </path>

                                <ant antfile="${project.parent.relativePath}/m2.ant"
                                    dir="${basedir}" target="run_junit" >
                                    <property name="maven.test.skip" value="${maven.test.skip}" />
                                    <property name="maven.repo.local" value="${maven.repo.local}" />
                                    <property name="project.artifactId" value="${project.artifactId}" />
                                    <property name="project.build.directory" value="${project.build.directory}" />
                                    <property name="project.build.testSourceDirectory" value="${project.build.testSourceDirectory}" />

                                    <property name="junit.srcroot" value="${basedir}${file.separator}../.."/>
                                    <property name="junit.as8base" value="${AS8BASE}"/>
                                    <property name="com.sun.jbi.home" value="${JBI_HOME}"/>
                                    <propertyset id="openesb.junit.sysproperties">
                                        <propertyref name="junit.srcroot"/>
                                        <propertyref name="junit.as8base"/>
                                        <propertyref name="com.sun.jbi.home"/>
                                    </propertyset>

                                    <reference refid="maven.test.classpath"/>
                                    <reference refid="openesb.junit.sysproperties"/>
                                    <reference refid="openesb.junit.test.patternset"/>
                                </ant>
                            </tasks>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
        </plugins>
    </build>
    <dependencies>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>base</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>jbi</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>manage</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>esb-manage</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>wsdl2</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>nmr</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>

        <!-- Not supported in Glassfish 9.1
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>security</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        -->
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>ui</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>framework-core</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>
        <dependency>
            <groupId>javax.transaction</groupId>
            <artifactId>jta</artifactId>
            <version>1.1</version>
        </dependency>
        <dependency>
            <groupId>open-esb</groupId>
            <artifactId>lassen-version</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>

        <!-- GLASSFISH DEPENDENCIES: -->
        <dependency>
            <groupId>jbiplatform</groupId>
            <artifactId>jbi_compileconf</artifactId>
            <version>${openesb.currentVersion}</version>
        </dependency>


        <dependency>
            <groupId>org.jboss.jbossas</groupId>
            <artifactId>jboss-as-system</artifactId>
            <version>5.1.0.GA</version>
            <type>jar</type>
        </dependency>
       
        <!--dependency>
            <groupId>quotes</groupId>
            <artifactId>quotes</artifactId>
            <version>none</version>
            <scope>system</scope>
            <systemPath>${SRCROOT}/runtime/framework/regress/classloaderregresstests/lib/quotes.jar</systemPath>
              
        </dependency-->
        <dependency>
            <groupId>org.jboss.jbossas</groupId>
            <artifactId>jboss-as-jmx</artifactId>
            <version>5.1.0.GA</version>
            <type>jar</type>
        </dependency>
        <dependency>
            <groupId>org.jboss.jbossas</groupId>
            <artifactId>jboss-as-console</artifactId>
            <version>5.1.0.GA</version>
            <type>jar</type>
        </dependency>
    </dependencies>
</project>

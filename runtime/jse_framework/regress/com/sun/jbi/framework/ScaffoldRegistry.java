/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ScaffoldRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ScaffoldRegistry.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 8, 2006, 5:17 PM
 */

package com.sun.jbi.framework;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceAssemblyInfo;
import com.sun.jbi.ServiceAssemblyQuery;
import com.sun.jbi.ServiceAssemblyState;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ServiceUnitState;

import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.descriptor.ComponentDescriptor;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.registry.RegistryException;

import java.io.ByteArrayInputStream;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Map;

import org.w3c.dom.Element;

/**
 * Scaffolded registry implementation to allow unit testing of the framework.
 *
 * @author Sun Microsystems, Inc.
 */
 public class ScaffoldRegistry
    extends com.sun.jbi.management.descriptor.ComponentDescriptor
    implements com.sun.jbi.management.registry.Registry,
        com.sun.jbi.management.registry.Updater,
        com.sun.jbi.management.registry.GenericQuery,
        com.sun.jbi.ComponentQuery
{
    /**
     * EnvironmentContext handle.
     */
    EnvironmentContext mContext;

    /**
     * ComponentRegistry handle.
     */
    ComponentRegistry mCompReg;

    /**
     * Pre-constructed XML element passed into the constructor for use by
     * the overridden methods in the ComponentDescriptor class. The tests that
     * require this will provide the XML element.
     */
    Element mXmlElement;

    /**
     * Constructor.
     *
     * @param the instance of the EnvironmentContext for testing.
     */
    ScaffoldRegistry(EnvironmentContext context)
    {
        super();
        mContext = context;
        mCompReg = mContext.getComponentRegistry();
        mXmlElement = null;
    }

    /**
     * Setter for providing a DOM Element that is used by the overridden
     * methods from the ComponentDescriptor class. This can be used to set up
     * test data for unit tests that process jbi.xml extensions.
     */
    public void setXmlElement(Element xmlElement)
    {
        mXmlElement = xmlElement;
    }

///////////////////////////////////////////////////////////////////////////////
//
// Methods from the Registry interface
//
///////////////////////////////////////////////////////////////////////////////
 
    /**
     * Get the Updater instance to be used for updating the Registry.
     *
     * @return an instance of the Updater to be used to manipulate 
     * ( create / delete / modify ) contents of the registry.
     * @throws RegistryException if problems are encountered in creating
     * a Updater.
     */
    public com.sun.jbi.management.registry.Updater getUpdater()
        throws RegistryException
    {
        return this;
    }

    /**
     * Get a Generic Query Instance.
     * @return an instance of GenericQuery.
     * @throws RegistryException on errors.
     */
   public com.sun.jbi.management.registry.GenericQuery getGenericQuery()
        throws RegistryException
    {
        return this;
    }

    /**
     * Get a ComponentQuery instance for a given target.
     *
     * @param targetName the name identifying the target.
     * @return an instance of ComponentQuery.
     * @throws RegistryException on errors.
     */
    public ComponentQuery getComponentQuery(String targetName)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * Get the ComponentQuery specific to the target of the instance.
     * @return an instance of ComponentQuery.
     * @throws RegistryException on errors.
     */
    public ComponentQuery getComponentQuery()
        throws RegistryException
    {
        return this;
    }

    /**
     * Get a ServiceAssemblyQuery instance for a given target
     *
     * @param targetName the name identifying the target.
     * @return an instance of ServiceAssemblyQuery.
     * @throws RegistryException on errors.
     */
    public ServiceAssemblyQuery getServiceAssemblyQuery(String targetName)
        throws RegistryException
    {
        return null;
    }

    /**
     * Get the ServiceAssemblyQuery specific to the target of the instance.
     * @return an instance of ServiceAssemblyQuery.
     * @throws RegistryException on errors.
     */
    public ServiceAssemblyQuery getServiceAssemblyQuery()
        throws RegistryException
    {
        return null;
    }

    /**
     * destroy the single instance.
     */
    public void destroy()
    {
    }

    /**
     * Get the Registry Specification.
     */
    public com.sun.jbi.management.registry.RegistrySpec getRegistrySpec()
    {
        return null;
    }

    /**
     * Get the Repository the registry instance uses.
     */
    public com.sun.jbi.management.repository.Repository getRepository()
    {
        return null;
    }

    /**
     * Get a registry property.
     */
    public String getProperty(String propName)
    {
        return propName;
    }

    /**
     * Commit the changes to the registry.
     */
    public void commit()
        throws RegistryException
    {
    }

    /**
     * Commit the changes to the registry.
     */
    public ByteArrayInputStream snapshot()
        throws RegistryException
    {
	return (null);
    }

///////////////////////////////////////////////////////////////////////////////
//
// Methods from the Updater interface
//
///////////////////////////////////////////////////////////////////////////////
 
    /**
     * Add a component to the runtime target.
     *
     * @param componentInfo the component instance
     * @throws RegistryException on errors
     */
    public void addComponent(ComponentInfo componentInfo) 
        throws RegistryException
    {
    }
    
    /**
     * Add a component to a given target.
     *
     * @param targetName {'domain', 'server', "instance-name", "cluster-name"}
     * @param componentInfo the component instance
     * @throws RegistryException on errors
     */
    public void addComponent(String targetName, ComponentInfo componentInfo) 
        throws RegistryException
    {
    }

    /**
     * Remove a component from the runtime's target.
     *
     * @param componentName the component name
     * @throws RegistryException on errors
     */
    public void removeComponent(String componentName) 
        throws RegistryException
    {
    }

    /**
     * Remove a component from a given target. 
     *
     * @param targetName {'domain', 'server', "instance-name", "cluster-name"}
     * @param componentName the component name
     * @throws RegistryException on errors
     */
    public void removeComponent(String targetName, String componentName) 
        throws RegistryException
    {
    }
 
    /** 
     * Add a shared library to the runtime's target.
     *
     * @param ComponentInfo the shared library instance
     * @throws RegistryException on errors
     */
    public void addSharedLibrary(ComponentInfo sharedLibraryInfo)
        throws RegistryException
    {
    }

    /** 
     * Add a shared library to a given target. If target = DOMAIN, then a simple
     * entry is created with the library name. If the target = SERVER / CLUSTER
     * a shared-library-ref is added.
     *
     * @param targetName {'domain', 'server', "instance-name", "cluster-name"}
     * @param ComponentInfo the shared library instance
     * @throws RegistryException on errors
     */
    public void addSharedLibrary(String targetName, ComponentInfo sharedLibraryInfo)
        throws RegistryException
    {
    }

    /** 
     * Remove a shared library from the runtime's target
     *
     * @param sharedLibraryName the shared library name
     * @throws RegistryException on errors
     */
    public void removeSharedLibrary(String sharedLibraryName)
        throws RegistryException
    {
    }

    /** 
     * Remove a shared library from a given target.
     *
     * @param targetName {'domain', 'server', "instance-name", "cluster-name"}
     * @param sharedLibraryName the shared library name
     * @throws RegistryException on errors
     */
    public void removeSharedLibrary(String targetName, String sharedLibraryName)
        throws RegistryException 
    {   
    }

    /**
     * Remove a service assembly from a given target. 
     *
     * @param serviceAssemblyName the service assembly name
     * @throws RegistryException on errors
     */
    public void removeServiceAssembly(String serviceAssemblyName)
        throws RegistryException
    {
    }

    /**
     * Remove a service assembly from a given target.
     *
     * @param serviceAssemblyName - service assembly name
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @throws RegistryException on errors
     */
    public void removeServiceAssembly(String targetName, String serviceAssemblyName)
            throws RegistryException
    {
    }

    /**
     * Add a server name.
     *
     * @param serverNameRef the server name to be added
     * @throws RegistryException on errors
     */
    public void addServer(String serverNameRef) 
        throws RegistryException
    {
    }

    /**
     * Remove a server name.
     *
     * @param serverNameRef the server name to be removed
     * @throws RegistryException on errors
     */
     public void removeServer(String serverNameRef) 
        throws RegistryException
    {
    }

    /**
     * Add a cluster name.
     *
     * @param clusterNameRef the cluster name to be added
     * @throws RegistryException on errors
     */
    public void addCluster(String clusterNameRef) 
        throws RegistryException
    {
    }

    /**
     * Remove a cluster name.
     *
     * @param clusterNameRef the cluster name to be removed
     * @throws RegistryException on errors
     */
     public void removeCluster(String clusterNameRef) 
        throws RegistryException 
    {
    }  

    /**
     * Add a domain shared library
     * 
     * @param slName the name of the shared library
     * @param fileName the shared library archive file name
     * @throws RegistryException on errors
     */
     public void addSharedLibrary(String slName, String fileName, Calendar timestamp)
        throws RegistryException
    {
    }

    /**
     * Add a domain component
     * 
     * @param componentName the name of the component
     * @param fileName the component archive file name
     * @throws RegistryException on errors
     */
    public void addComponent(String componentName, String fileName, Calendar timestamp)
        throws RegistryException
    {
    }

    /**
     * Add a service assembly to the domain.
     *
     * @param saName the service assembly name
     * @param fileName the service assembly archive name
     * @throws RegistryException on errors
     */
    public void addServiceAssembly(String saName, String fileName, Calendar timestamp)
        throws RegistryException
    {
    }

    /**
     * Add a service assembly to he runtime's target
     *
     * @param saName - service assembly name
     * @throws RegistryException on errors
     */
    public void addServiceAssembly(String saName)
            throws RegistryException
    {
    }
   
    /**
     * Add a service assembly to a given target. If target = DOMAIN, then a simple
     * entry is created with the assembly name. If the target = SERVER / CLUSTER
     * a service-assembly-ref is added.
     *
     * @param targetName - {'domain', 'server', "instance-name", "cluster-name"}
     * @param saName - service assembly name
     * @throws RegistryException on errors
     */
    public void addServiceAssembly(String targetName, String saName)
            throws RegistryException
    { 
    }

    /**
     * Set the state of a component for the target at runtime.
     *
     * @param componentName the component name
     * @param state the state to set. 
     * @throws RegistryException on errors
     */
    public void setComponentState(ComponentState state, String componentName) 
        throws RegistryException
    {
    }

    /**
     * Set the state of a component in a server/cluster.
     *
     * @param targetName either the server-name or cluster-name
     * @param componentName the component name
     * @param state the state to set. 
     * @throws RegistryException on errors
     */
    public void setComponentState(String targetName, ComponentState state,
        String componentName)
        throws RegistryException
    {
    }

    /**
     * Set the properties of a component in a server/cluster.
     *
     * @param targetName either the server-name or cluster-name, this operation
     * is a no-op  when target = domain.
     * @param componentName the component name
     * @param props the properties to set. 
     * @throws RegistryException on errors
     */
    public void setComponentProperties(String targetName, Map<String, String> state,
        String componentName)
        throws RegistryException
    {
    }

    /**
     * Set the state of a ServiceUnit for the runtime target.
     *
     * @param componentName the component name
     * @param suName the service unit name
     * @param state the state to set. 
     * @throws RegistryException on errors
     */
    public void setServiceUnitState(
        ServiceUnitState state, String componentName, String suName) 
        throws RegistryException
    {
    }

    /**
     * Set the state of a ServiceUnit in a server / cluster.
     *
     * @param targetName either the value 'domain', 'server' or the
     * server-name or cluster-name.
     * @param componentName the component name
     * @param suName the service unit name
     * @param state the state to set. 
     * @throws RegistryException on errors
     */
    public void setServiceUnitState(String targetName,
        ServiceUnitState state, String componentName, String suName) 
        throws RegistryException
    {
    }

    /**
     * Set the state of a ServiceAssembly for the runtime target.
     *
     * @param state  - the state to set.
     * @param saName - the service assembly name
     * @throws RegistryException on errors
     */
    public void setServiceAssemblyState(
        ServiceAssemblyState state, String saName)
                throws RegistryException
    {
    }
  
    /**
     * Set the state of a ServiceAssembly in a server / cluster.
     *
     * @param targetName - either the value 'domain', 'server' or the
     * server-name or cluster-name.
     * @param saName - service assembly name
     * @param state  - the state to set.
     * @throws RegistryException on errors
     */
    public void setServiceAssemblyState(String targetName,
        ServiceAssemblyState state, String saName)
                throws RegistryException
    {
    }

    /**
     * Add a ServiceUnit to a Component for the runtime target.
     *
     * @param suInfo the ServiceUnitInfo
     * @param componentName the component name
     * @throws RegistryException on errors
     */
    public void addServiceUnitToComponent(
        String componentName, ServiceUnitInfo suInfo) 
        throws RegistryException
    {
    }

    /**
     * Add a ServiceUnit to a Component.
     *
     * @param suInfo the ServiceUnitInfo
     * @param targetName either the value 'domain', 'server' or the
     * server-name or cluster-name.
     * @param componentName the component name
     * @throws RegistryException on errors
     */
    public void addServiceUnitToComponent(String targetName, String componentName,
        ServiceUnitInfo suInfo) 
        throws RegistryException
    {
    }

    /**
     * Remove a ServiceUnit from a Component for the runtime target.
     *
     * @param suName the service unit name
     * @param componentName the component name
     * @throws RegistryException on errors
     */
    public void removeServiceUnitFromComponent(String componentName, String suName)
        throws RegistryException
    {
    }

    /**
     * Remove a ServiceUnit from a Component.
     *
     * @param suName the service unit name
     * @param targetName the server-name or cluster-name.
     * @param componentName the component name
     * @throws RegistryException on errors
     */
    public void removeServiceUnitFromComponent(String targetName,
        String componentName, String suName)
        throws RegistryException
    {
    }
    
    /**
     * Set the file name for the domain component.
     *
     * @param fileName the name of the component archive
     * @param componentName the component name
     */
    public void setComponentFileName(String fileName, String componentName)
        throws RegistryException
    {
    }

    /**
     * Set the file name for the domain shared library.
     *
     * @param fileName the name of the shared library archive
     * @param slName the shared library  name
     */
    public void setSharedLibraryFileName(String fileName, String slName)
        throws RegistryException
    {
    }

    /**
     * Set the file name for the domain service assembly.
     *
     * @param fileName the name of the service assembly archive
     * @param saName the service assembly name
     */
    public void setServiceAssemblyFileName(String fileName, String saName)
        throws RegistryException
    {
    }
    
    /**
     * This method is used to set the upgrade-number attribute in the domain 
     * level entry for a component
     * @param componentName the component name
     * @param upgradeNumber the update number
     * @throws RegistryException if the update number could not be set
     */
    public void setComponentUpgradeNumber(String componentName, java.math.BigInteger upgradeNumber)
        throws RegistryException
    {
        
    }
    

    /**
     * Set the value of a configuration attribute belonging to the
     * specified category, for the runtime target. 
     *
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setAttribute(ConfigurationCategory type, String name, String value)
        throws RegistryException
    {
    }
    
    /**
     * Get the value of a configuration attribute belonging to the
     * specified category, for the runtime target. 
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setAttribute(String targetName, ConfigurationCategory type, String name, String value)
        throws RegistryException
    {   
    }
    
    /**
     * Delete the named attribute from the category if it exists, for the implicit target
     * The attribute is deleted only if it is overriden for the target, if target is "domain"
     * the attribute is not deleted.
     *
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     */
    public void deleteAttribute(ConfigurationCategory type, String name)
        throws RegistryException
    {
        
    }
    
    
    /**
     * Delete the named attribute from the category if it exists, for the implicit target
     * The attribute is deleted only if it is overriden for the target, if target is "domain"
     * the attribute is not deleted.
     *
     * @param target - target name
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     */
    public void deleteAttribute(String target, ConfigurationCategory type, String name)
        throws RegistryException
    {
        
    }
///////////////////////////////////////////////////////////////////////////////
//
// Methods from the GenericQuery interface
//
///////////////////////////////////////////////////////////////////////////////

    /** 
     * @param componentName - Component name
     * @return a List<String> of all servers installing a component.
     *         The List<String> excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     *
     */
    public List<String> getServersInstallingComponent(String componentName)
        throws RegistryException
    {
        return null;
    }
    
    /** 
     * @param  sharedLibraryName - Shared Library name
     * @return a List<String> of all servers installing a shared library.
     *         The List<String> excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     *
     */
    public List<String> getServersInstallingSharedLibrary(String sharedLibraryName)
        throws RegistryException
    {
        return null;
    }
    
    /** 
     * @param  serviceAssemblyName - Service Assembly name
     * @return a List<String> of all servers deploying a service assembly.
     *         The List<String> excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     *
     */
    public List<String> getServersDeployingServiceAssembly(String serviceAssemblyName)
        throws RegistryException
    {
        return null;
    }
    
    /** 
     * @param componentName - Component name
     * @return a List<String> of all clusters installing a component.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     *
     */    
    public List<String> getClustersInstallingComponent(String componentName)
        throws RegistryException
    {
        return null;
    }
    
    /** 
     * @param  sharedLibraryName - Shared Library name
     * @return a List<String> of all clusters installing a shared library.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     *
     */
    public List<String> getClustersInstallingSharedLibrary(String sharedLibraryName)
        throws RegistryException
    {
        return null;
    }
    
    /** 
     * @param  serviceAssemblyName - Service Assembly name
     * @return a List<String> of all clusters deploying a service assembly.
     *         The List<String> excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     *
     */
    public List<String> getClustersDeployingServiceAssembly(String serviceAssemblyName)
        throws RegistryException
    {
        return null;
    }

    /**
     * @param componentName - component name
     * @return the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public String getComponentInstallationDescriptor(String componentName)
        throws RegistryException
    {
        return null;
    }

    /**
     * @param componentName - component name
     * @return wrapper for the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     */
    public ComponentDescriptor getComponentDescriptor(String componentName)
        throws RegistryException
    {
        return this;
    }

    /**
     * @param sharedLibraryName - shared library name
     * @return the Installation descriptor for the shared library.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     */
    public String getSharedLibraryInstallationDescriptor(String sharedLibraryName)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @param serviceAssemblyName - service assembly name
     * @return the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     */
    public String getServiceAssemblyDeploymentDescriptor(String serviceAssemblyName)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @param serviceAssemblyName - service assembly name
     * @param serviceUnitName - service unit name
     * @return the Installation descriptor for the service unit.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     */
    public String getServiceUnitDeploymentDescriptor(String serviceAssemblyName,
						String serviceUnitName)
        throws RegistryException
    {
        return null;
    }

    /**
     * @param serviceAssemblyName - service assembly name
     * @return the filename for the associated archive
     * @throws RegistryException if a problem exists.
     */
    public String getServiceAssemblyArchive(String serviceAssemblyName)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @param componentName - component name
     * @return the filename for the associated archive
     * @throws RegistryException if a problem exists.
     */
    public String getComponentArchive(String componentName)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @param componentName - component name
     * @return the component type for the associated archive.
     * @throws RegistryException if a problem exists.
     */
    public ComponentType getComponentType(String componentName)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @param sharedLibraryName - sharedLibrary name
     * @return the filename for the associated archive
     * @throws RegistryException if a problem exists.
     */
    public String getSharedLibraryArchive(String sharedLibraryName)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @return true if a component is installed on one or more servers/clusters
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     */
    public boolean isComponentInstalled(String componentName)
        throws RegistryException
    {
        return false;
    }
    
    /**
     * @return true if a shared library is installed on one or more servers/clusters
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     */
    public boolean isSharedLibraryInstalled(String sharedLibraryName)
        throws RegistryException
    {
        return false;
    }
    
    /**
     * @return true if a service assembly is deployed on one or more servers/clusters
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     */
    public boolean isServiceAssemblyDeployed(String serviceAssemblyName)
        throws RegistryException
    {
        return false;
    }
    
    /**
     * @return a list of Servers from the registry
     */
    public List<String> getServers()
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @return  a list of Clusters from the registry
     */
    public List<String> getClusters()
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @return true if the Shared Library is registered in the domain.
     */
    public boolean isSharedLibraryRegistered(String sharedLibraryName)
        throws RegistryException
    {
        return false;
    }
    
    /**
     * @return true if the Service Assembly is registered in the domain.
     */
    public boolean isServiceAssemblyRegistered(String serviceAssemblyName)
        throws RegistryException
    {
        return false;
    }
    
    /**
     * @return true if the component is registered in the domain.
     */
    public boolean isComponentRegistered(String componentName)
        throws RegistryException
    {
        return false;
    }
    
    /**
     * @return a List of Service Assemblies registered in the domain.
     */
    public List<String> getRegisteredServiceAssemblies()
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @return a List of Shared Libraries registered in the domain.
     */
    public List<String> getRegisteredSharedLibraries()
        throws RegistryException
    {
        return null;
    }
    
    /**
     * @return a List of Components registered in the domain.
     */
    public List<String> getRegisteredComponents()
        throws RegistryException
    {
        return null;
    }
 
    /**
     * This method is used to find out if this is a system
     * component.
     * This method returns the value of system-install attribute
     * for this component from the registry
     * A component that has system-install set to true will have
     * its install root under AS_INSTALL/jbi and it should not be
     * deleted on unload of the component's uninstaller.
     * @param componentName the name of the component
     * @return boolean true if system-install is true, false otherwise
     *
     */
    public boolean isSystemComponent(String componentName)
        throws RegistryException
    {
        return false;
    }
    
    /**
     * This method is used to find out if this is a system
     * shared library 
     * This method returns the value of system-install attribute
     * for this shared library from the registry
     * A shared library that has system-install set to true will have
     * its install root under AS_INSTALL/jbi and it should not be
     * deleted on uninstall
     * @param sharedLibraryName the name of the shared library
     * @return boolean true if system-install is true, false otherwise
     *
     */
    public boolean isSystemSharedLibrary(String sharedLibraryName)
        throws RegistryException
    {
        return false;
    }
    
    /**
     * Get the file name for the domain component.
     *
     * @param componentName - component name
     */
    public String getComponentFileName(String componentName)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * Get the file name for the domain shared library.
     *
     * @param slName - shared library  name
     */
    public String getSharedLibraryFileName(String slName)
        throws RegistryException
    {
        return null;
    }
     
    /**
     * Get the file name for the domain service assembly.
     *
     * @param saName - service assembly name
     */
    public String getServiceAssemblyFileName(String saName)
        throws RegistryException
    {
        return null;
    }
  
    /**
     * Get the timestamp for the domain component.
     *
     * @param componentName - component name
     */
    public long getComponentTimestamp(String componentName)
        throws RegistryException
    {
        return 0;
    }
    
    /**
     * Get the timestamp for the domain shared library.
     *
     * @param slName - shared library  name
     */
    public long getSharedLibraryTimestamp(String slName)
        throws RegistryException
    {
        return 0;
    }
     
    /**
     * Get the timestamp for the domain service assembly.
     *
     * @param saName - service assembly name
     */
    public long getServiceAssemblyTimestamp(String saName)
        throws RegistryException
    {
        return 0;
    }
    
    /**
     * This method is used to get the value of the attribute upgrade-number from the 
     * domain level entry for the component in the registry
     * @param componentName the componentName
     * @return BigInteger the upgrade number
     * @throws RegistryException if the upgrade number could not be retrieved
     */
    public BigInteger getComponentUpgradeNumber(String componentName)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * Get the value of a configuration attribute belonging to the
     * specified category, for the runtime target. 
     *
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     * @return the String representation of the attributes value
     */
    public String getAttribute(ConfigurationCategory type, String name)
        throws RegistryException
    {
        return null;
    }
    
    /**
     * Get the value of a configuration attribute belonging to the
     * specified category, for the specified target. 
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     * @return the String representation of the attributes value
     */
    public String getAttribute(String targetName, ConfigurationCategory type, String name)
        throws RegistryException
    {
        return null;
    }
    
    /** 
     * Determine if a configuration attribute is overriden by a target.
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @return true if the attribute is overriden by a target
     */
    public boolean isAttributeOverriden(String targetName, ConfigurationCategory type, 
        String name)
    {
        return false;
    }
    
    /**
     * Determine if the domain configuration is present in the registry
     *
     * @return true if the domain-config is defined in the registry
     */
    public boolean isGlobalConfigurationDefined()
    {
        return false;
    }

///////////////////////////////////////////////////////////////////////////////
//
// Methods from the ComponentQuery interface
//
///////////////////////////////////////////////////////////////////////////////
 
   /**
    * Get a list of component IDs of the specified type.
    * @param type The type of component for which the IDs are requested.
    * Can be one of: ComponentType.BINDING, ComponentType.ENGINE,
    * ComponentType.SHARED_LIBRARY, ComponentType.BINDINGS_AND_ENGINES, or
    * ComponentType.ALL.
    * @return A list of Strings containing the component IDs.
    */
    public List<String> getComponentIds(ComponentType type)
    {
        return new ArrayList();
    }

   /**
    * Get a list of component IDs of the specified type, with the
    * specified status.
    * @param type The type of component for which the IDs are requested.
    * @param status The status for which the IDs are requested. 
    * Can be one of {LOADED, SHUTDOWN, STOPPED, STARTED}
    * @return A list of Strings containing the component IDs. If no components
    * were found, the list is empty.
    */
    public List<String> getComponentIds(ComponentType type, ComponentState status)
    {
        return mCompReg.getComponentIdsFromCache(type, status);
    }
    
   /**
    * Look up a component using its unique name.
    * @param componentName the unique name of the component.
    * @return the ComponentInfo instance containing all runtime information
    * about the component or null if no component was found with the specified
    * name.
    */
    public ComponentInfo getComponentInfo(String componentName)
    {
        return null;
    }

   /**
    * Return a list of all component names dependent upon the specified Shared
    * Library.
    * @param sharedLibraryName - the unique name of the Shared Library.
    * @return A list containing the unique names of all components that
    * depend upon the Shared Library.
    */
    public List<String> getDependentComponentIds(String sharedLibraryName)
    {
       return new ArrayList();
    }

   /**
    * Look up a Shared Library using its unique name.
    * @param sharedLibraryName the unique name of the Shared Library.
    * @return the ComponentInfo instance containing all runtime information
    * about the Shared Library or null if no Shared Library was found with the
    * specified name.
    */
    public ComponentInfo getSharedLibraryInfo(String sharedLibraryName)
    {
        return (ComponentInfo) mCompReg.getSharedLibrary(sharedLibraryName);
    }

   /**
    * Get the current status of a Binding Component or Service Engine.
    * @return the current status of the BC or SE.
    * @throws javax.jbi.JBIException if the component name is not found.
    */
    public ComponentState getStatus(String componentName)
        throws javax.jbi.JBIException
    {
        return mCompReg.getStatus(componentName);
    }
    
        /**
     * Set the value of a configuration attribute for a component installed on a target
     * for the runtime target. 
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @param componentName - identification for the component
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setComponentAttribute(String componentName, String name, String value)
        throws RegistryException
    {
        
    }
    
    /**
     * Set the value of a configuration attribute for a component installed on a target,
     *
     * @param targetName - target instance/cluster name.
     * @param name - identification for the attribute
     * @param value - string representation of the attribute value
     * @param componentName - identification for the component
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setComponentAttribute(String componentName, String targetName, 
        String name, String value)
        throws RegistryException
    {
        
    }
    
        /**
     * Set the value of a configuration attribute for a component installed on a target
     * for the runtime target. 
     * @param props - properties to set
     * @param componentName - identification for the component
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setComponentAttributes(String componentName, Properties props)
        throws RegistryException
    {
        
    }
    
    /**
     * Set the value of a configuration attribute for a component installed on a target,
     *
     * @param targetName - target instance/cluster name.
     * @param props - properties to set
     * @param componentName - identification for the component
     * @exception RegistryException on errors in getting the attribute value
     */
    public void setComponentAttributes(String componentName, String targetName, 
        Properties props)
        throws RegistryException
    {
        
    }
    
       /**
     * Add a set of application variables to the registry for the runtime target.
     *
     * @param appVars - list of application variables to be added to the registry
     * @exception RegistryException on errors in adding the application variables.
     */
    public void addComponentApplicationVariables(String componentName, 
		com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException
    {
        
    }
    
    /**
     * Add a set of application variables to the registry for the specified target.
     *
     * @param appVars - list of application variables to be added to the registry
     * @param targetName - target to be updated
     * @exception RegistryException on errors in adding the application variables.
     */
    public void addComponentApplicationVariables(String componentName, String targetName, 
        com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException
    {
        
    }
    
    /**
     * Update a set of application variables to the registry for the runtime target.
     *
     * @param appVars - list of application variables to be updated
     * @exception RegistryException on errors in updating the application variables.
     */
    public void updateComponentApplicationVariables(String componentName, 
        com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException
    {
        
    }
    
    /**
     * Updated a set of application variables to the registry for the specified target.
     *
     * @param appVars - list of application variables to be updated
     * @param targetName - target to be updated
     * @exception RegistryException on errors in updating the application variables.
     */
    public void updateComponentApplicationVariables(String componentName, String targetName, 
        com.sun.jbi.management.ComponentInfo.Variable[] appVars )
        throws  RegistryException
    {
        
    }
    
    /**
     * Delete a set of application variables from the registry for the runtime target.
     *
     * @param appVars - list of application variables to be deleted from the registry
     * @param names - the names of the variables to be deleted.
     * @exception RegistryException on errors in deleting the application variables.
     */
    public void deleteComponentApplicationVariables(String componentName, String[] names )
        throws  RegistryException
    {
        
    }
    
    /**
     * Add a set of application variables to the registry for the specified target.
     *
     * @param appVars - list of application variables to be deleted from the registry
     * @param targetName - target to be updated
     * @param names - the names of the variables to be deleted.
     * @exception RegistryException on errors in deleting the application variables.
     */
    public void deleteComponentApplicationVariables(String componentName, String targetName, 
        String[] names )
        throws  RegistryException
    {
        
    }
        
    /**
     * Add a named application configuration to the registry for the component for the
     * implicit runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @exception RegistryException on errors in adding the application configuration.
     */
    public void addComponentApplicationConfiguration(String componentName, 
		Properties appConfig )
        throws  RegistryException
    {
        
    }
    
    /**
     * Add a named application configuration to the registry for the component for the
     * specified runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @param targetName 
     *                target name
     * @exception RegistryException on errors in adding the application configuration.
     */
    public void addComponentApplicationConfiguration( String componentName, String targetName,
		Properties appConfig )
        throws  RegistryException
    {
        
    }
    
    /**
     * Update a named application configuration set on a component 
     * for the implicit runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @exception RegistryException on errors in updating the application configuration.
     */
    public void updateComponentApplicationConfiguration(String componentName, 
        Properties appConfig )
        throws  RegistryException
    {
        
    }
    
    
    /**
     * Update a named application configuration set on a component 
     * for the specified runtime target.
     *
     * @param componentName
     *                 component identification
     * @param appConfig 
     *                 named application configuration represented as properties
     * @param targetName 
     *                 target name
     * @exception RegistryException on errors in updating the application configuration.
     */
    public void updateComponentApplicationConfiguration(String componentName, String targetName,
        Properties appConfig )
        throws  RegistryException
    {
        
    }
    
    /**
     * Delete a named application configuration
     *
     * @param appConfigName 
     *          name of the application configuration to delete.
     * @param componentName
     *          component identification
     * @exception RegistryException on errors in deleting the application configuration.
     */
    public void deleteComponentApplicationConfiguration(String componentName, String appConfigName )
        throws  RegistryException
    {
        
    }
    
    
    /**
     * Delete a named application configuration
     *
     * @param componentName
     *          component identification
     * @param targetName
     *          target name
     * @param appConfigName 
     *          the name of the configuration to be deleted.
     * @exception RegistryException on errors in deleting the application configuration.
     */
    public void deleteComponentApplicationConfiguration(String componentName, String targetName,
        String appConfigName )
        throws  RegistryException
    {
        
    }
 
///////////////////////////////////////////////////////////////////////////////
//
// Overrideen methods from the ComponentDescriptor class
//
///////////////////////////////////////////////////////////////////////////////
 
    /**
     * Get the the "Logging" element in the jbi.xml. If the Logging 
     * element is missing in the jbi.xml a null value is returned.
     *
     * @return the logging element.
     */
    public Element getComponentLoggingXml()
    {
        return mXmlElement;
    }

    /**
     * Get the the "Observer" element in the jbi.xml. If the Observer 
     * element is missing in the jbi.xml a null value is returned.
     *
     * @return the observer element.
     */
    public Element getComponentObserverXml()
    {
        return mXmlElement;
    }
}

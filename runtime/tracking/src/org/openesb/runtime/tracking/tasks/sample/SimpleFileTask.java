/*
 * BEGIN_HEADER - DO NOT EDIT

 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.

 * You can obtain a copy of the license at
 * http://opensource.org/licenses/cddl1.php.
 * See the License for the specific language governing
 * permissions and limitations under the License.

 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * http://opensource.org/licenses/cddl1.php.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package org.openesb.runtime.tracking.tasks.sample;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.xml.transform.Source;
import org.openesb.runtime.tracking.MessageTrackingInfo;
import org.openesb.runtime.tracking.tasks.MessageTrackingTaskRunnable;
import org.openesb.runtime.tracking.util.NormalizedMessageUtil;

/**
 *
 * Copyright 2011 Alexander Lomov.
 */
public class SimpleFileTask extends MessageTrackingTaskRunnable {

    private String filePath = null;
    private static final Lock wl = new ReentrantReadWriteLock().writeLock();
    private MessageTrackingInfo trackingInfo;

    public SimpleFileTask(MessageTrackingInfo trackingInfo) {
        filePath = System.getProperty("com.sun.aas.instanceRoot");
        filePath = filePath.concat("/logs/");
        this.trackingInfo = trackingInfo;
    }

    
//    @Override
    public void run() {

        FileWriter fw = null;
        try {
            wl.lock();
            fw = getFileWriter();

            // Write the leading date
            fw.write("\n");
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");
            fw.write(sdf.format(trackingInfo.getDate()));

            //Write the tracking comment
            fw.write("\n");
            fw.write(trackingInfo.getComment());

            //Now write down all the message exchange
            fw.write("\n");
            
            fw.write("In Message is:\n");
            fw.write(NormalizedMessageUtil.toString(trackingInfo.getInMessage()));

            Source outMsg = trackingInfo.getOutMessage();
            if (outMsg != null){
                fw.write("\n");
                fw.write("Out Message is:\n");
                fw.write(NormalizedMessageUtil.toString(outMsg));
            }

            Source faultMsg = trackingInfo.getFaultMessage();
            if (faultMsg != null){
                fw.write("\n");
                fw.write("Fault Message is:\n");
                fw.write(NormalizedMessageUtil.toString(faultMsg));
            }

            final ServiceEndpoint se = trackingInfo.getEndpoint();
            String epName = se.getServiceName().getNamespaceURI().concat(se.getServiceName().getLocalPart());
            epName.concat(se.getEndpointName());

            fw.write("\nEndpoint is:\n");
            fw.write(epName);
            fw.write("\n");

            fw.write("Status is: ");
            fw.write(trackingInfo.getStatus().toString());
            fw.write("\n");

        } /*catch (TransformerConfigurationException ex) {
            Logger.getLogger(SimpleFileTask.class.getName()).log(Level.SEVERE, null, ex);
        }*/ catch (java.io.IOException ioe) {
        } finally{
            if (fw != null)
                try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(SimpleFileTask.class.getName()).log(Level.SEVERE, null, ex);
            }
            wl.unlock();
        }

    }

    private FileWriter getFileWriter() throws java.io.IOException {
        final String fileName = trackingInfo.getMessageTrackingId().concat(".log");
        File f = new File(filePath.concat(fileName));

        if (!f.exists())
            f.createNewFile();

        FileWriter fw = new FileWriter(f, true);
        return fw;
    }

    @Override
    public void track() {
        run();
    }
}

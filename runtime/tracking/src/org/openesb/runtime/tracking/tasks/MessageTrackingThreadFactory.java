/*
 * BEGIN_HEADER - DO NOT EDIT

 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.

 * You can obtain a copy of the license at
 * http://opensource.org/licenses/cddl1.php.
 * See the License for the specific language governing
 * permissions and limitations under the License.

 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * http://opensource.org/licenses/cddl1.php.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package org.openesb.runtime.tracking.tasks;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * Copyright 2011 Alexander Lomov.
 */
public class MessageTrackingThreadFactory implements ThreadFactory {

    private AtomicLong threadNumber = new AtomicLong(0);
    private String name;

    public MessageTrackingThreadFactory(String nameBase) {
        super();
        this.name = nameBase;
    }

    public Thread newThread(Runnable r) {
        Thread daemonThread = new Thread(r, name + getThreadNumber());
        daemonThread.setDaemon(true);
        return daemonThread;
    }

    private long getThreadNumber() {
        return threadNumber.getAndIncrement();
    }
}

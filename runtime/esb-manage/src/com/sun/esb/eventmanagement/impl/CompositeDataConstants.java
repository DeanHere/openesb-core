/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CompositeDataConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.eventmanagement.impl;

public interface CompositeDataConstants {
     static final String DATE ="eventDate";
     static final String ID = "eventID";
     static final String ENVIRONMENT = "eventEnvironment";
     static final String LOGICALHOST = "eventLogicalHost";
     static final String SERVER = "eventServer";         
     static final String COMPONENTPROJECTPATH = "eventComponentProjectPath";
     static final String DEPLOYMENT = "eventDeployment";     
     static final String COMPONENTNAME ="eventComponentName";
     static final String SEVERITY = "eventSeverity";       
     static final String TYPE = "eventType";           
     static final String STATUS = "eventStatus";         
     static final String STATE ="eventState";          
     static final String PHYSICALHOST = "eventPhysicalHost";   
     static final String MESSAGECODE = "eventMessageCode";    
     static final String MESSAGEDETAILS = "eventMessageDetails"; 
     static final String SERVERTYPE = "eventServerType";      
     static final String COMPONENTTYPE = "eventComponentType";  

}

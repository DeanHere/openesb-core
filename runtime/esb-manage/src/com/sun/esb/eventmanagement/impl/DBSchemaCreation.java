
/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DBSchemaCreation.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.eventmanagement.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import com.sun.esb.eventmanagement.api.AlertPersistenceDBType;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.ui.common.I18NBundle;


/**
 * @author Sun Microsystems
 */
public class  DBSchemaCreation {

    private static String CREATE_TABLES_SCRIPT = "create_event_store_tables.sql"; //$NON-NLS-1$
    private static String DROP_TABLES_SCRIPT = "drop_event_store_tables.sql"; //$NON-NLS-1$
    private static String TRUNCATE_TABLES_SCRIPT = "truncate_event_store_tables.sql"; //$NON-NLS-1$  
    
    private static final Logger mLogger;
    private static boolean isDebugEnabled;
    private I18NBundle I18NPACKAGEBUNDLE;
    private String mUniqueTableName;

    static {
        mLogger = Logger.getLogger(EventForwarderMBean.class.getName());
        isDebugEnabled = mLogger.isLoggable(Level.FINEST);
     
    }
 
    private static String DELIMITER = ";"; //$NON-NLS-1$   
    /**
     * Check to see if all the tables required by BPEL-SE for persistence are present. 
     * If not present, create the tables
     * @param prop database properties
     */
    public void checkAndCreateTables(AlertPersistenceDBType dbType,DataSource aDataSource) throws ManagementRemoteException{
        //Check to see if all the tables required by BPEL-SE for persistence are present
        //If not present, create the tables
        if (!checkTablesIntegrity( aDataSource)) {
            createTables(dbType, aDataSource);
        }
    }

    /**
     * Creates the tables required for persistence
     * @param prop database properties
     */
    public void createTables( AlertPersistenceDBType dbType, DataSource aDataSource ) throws ManagementRemoteException{
        StringTokenizer tokens = populateCreateScript(dbType, CREATE_TABLES_SCRIPT);
        executeScript(tokens, aDataSource);
    }
    
    /**
     * Drops the tables created for persistence
     * @param prop database properties
     */
    public void dropTables(AlertPersistenceDBType dbType, DataSource aDataSource) throws ManagementRemoteException{
        StringTokenizer tokens = populateCreateScript(dbType, getDropScriptName());
        executeScript(tokens, aDataSource);
    }
    
    public void truncateTables (AlertPersistenceDBType dbType, DataSource aDataSource) throws ManagementRemoteException{
        StringTokenizer tokens = populateCreateScript(dbType, getTruncateScriptName());
        executeScript(tokens, aDataSource);
    }

    /**
     * Check to see if all the tables required by BPEL-SE for persistence are present
     *
     * @param prop database properties
     * @return boolean true: if valid schema exists in database false: otherwise
     */
    public boolean checkTablesIntegrity( DataSource aDataSource ) throws ManagementRemoteException{
        
        boolean allTablesPresent = false;
        String dbUserName = null;
        ResultSet resultSet = null;
        Connection connection  = null;
        // DEVNOTE: Do not try again if there is a SQLException, since this is a startup method. If the method
        // fails due to DBUnavailability (or anything else for that matter) the BPEL SE will not start.
        try {
            connection = aDataSource.getConnection();
            dbUserName = connection.getMetaData().getUserName();
            
            //Check for all tables present
            int numTablesFound = 0;
            
            String [] objectsToSearchFor = {"TABLE"};
            resultSet = connection.getMetaData().getTables(null, "%", "%", objectsToSearchFor); //$NON-NLS-1$
            while (resultSet.next() && (numTablesFound != getTabels().length)) {
                
                 String rsTableName = resultSet.getString("TABLE_NAME");
                
                //if the table is not in a schema same as the user name, go to next
                String rsTableSchemaName = resultSet.getString("TABLE_SCHEM");
                if(!(dbUserName.equalsIgnoreCase(rsTableSchemaName))){
                    continue;
                }

                for (int i = 0; i < getTabels().length; i++) {
                    String tableItem = getTabels()[i];
                    if(rsTableName.equalsIgnoreCase(tableItem)) { //$NON-NLS-1$
                        //Found a table, increment the counter
                        numTablesFound++;
                        break;
                    }
                }
            }
            
            
            if (numTablesFound == getTabels().length) { // found all tables
                allTablesPresent = true;
                return allTablesPresent;
            } 
            return false;
            
        } catch (Exception e) {
            throw new ManagementRemoteException (getPackageBundle().getMessage("DBSchemaCreation.Table.Integrity.Check.Exception",  
                            new Object [] {dbUserName}), e);
        } finally {
                if (resultSet != null) {
                        try {
                                resultSet.close();
                        } catch (SQLException resCloseExcp) {
                                mLogger.log(Level.SEVERE, getPackageBundle().getMessage("DBSchemaCreation.Resultset.Close.Exception"), resCloseExcp);
                        }
                }
                if (connection != null) {
                try {
                    connection.close(); // this wrapper takes care of setting the initial value of setAutoCommit
                } catch (SQLException ex) {
                        mLogger.log(Level.SEVERE, getPackageBundle().getMessage("DBSchemaCreation.Connection.Close.Exception"), ex);
                }
            }
        }
    }
    
    private StringTokenizer populateCreateScript(AlertPersistenceDBType dbType, String scriptName) throws ManagementRemoteException{
        String scriptsPath = "scripts/"; //$NON-NLS-1$
        
        if (dbType.equals(AlertPersistenceDBType.DB2)) {
            scriptsPath = scriptsPath + "db2/" + scriptName; //$NON-NLS-1$
        } else if (dbType.equals(AlertPersistenceDBType.ORACLE)) {
            scriptsPath = scriptsPath + "oracle/" + scriptName; //$NON-NLS-1$
        } else if (dbType.equals(AlertPersistenceDBType.POINTBASE)) {
            scriptsPath = scriptsPath + "pointbase/" + scriptName; //$NON-NLS-1$
        } else if (dbType.equals(AlertPersistenceDBType.SYBASE)) {
            scriptsPath = scriptsPath + "sybase/" + scriptName; //$NON-NLS-1$
        } else if (dbType.equals(AlertPersistenceDBType.DERBY)) {
            scriptsPath = scriptsPath + "derby/" + scriptName; //$NON-NLS-1$
        }
        
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        InputStream is = null;
        
        try {
            is = DBSchemaCreation.class.getResourceAsStream(scriptsPath);
            
            int c;
            
            while ((c = is.read()) != -1) {
                os.write(c);
            }
        } catch (IOException e) {
            // should never happen as data is read from a resource that exist
            // in the same package as this code.
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
                
                if (os != null) {
                    os.close();
                }
            } catch (IOException ex) {}
        }
        
        String script = new String(os.toByteArray());
        StringTokenizer tokens = new StringTokenizer(script, DELIMITER);
        
        return tokens;
    }

    private void executeScript (
                StringTokenizer tokens, DataSource aDataSource ) throws ManagementRemoteException{
        Statement stmt = null;
        Connection connection  = null;
        
        // DEVNOTE: Do not try again if there is a SQLException, since this is a startup method. If the method
        // fails due to DBUnavailability (or anything else for that matter) the BPEL SE will not start.
        try {
            connection = aDataSource.getConnection();
            connection.setAutoCommit(false);
            stmt = connection.createStatement();

            String element = null;
            for (int i = 0, size = tokens.countTokens(); i < (size - 1); i++) {
                // last token may just be empty.
                element = tokens.nextToken();
                if(element.indexOf(" NOTIFICATION_EVENT") != -1) {
                    element = fixTableName(element);
                }
                stmt.execute(element);
            }

            connection.commit();

        } catch (Exception ex) {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e) {
                    String msg = getPackageBundle().getMessage("DBSchemaCreation.Connection.Rollback.Exception");
                    mLogger.log(Level.SEVERE, msg, e);
                    throw new ManagementRemoteException(msg,e);
                }
            }
            
           throw new ManagementRemoteException(getPackageBundle().getMessage("DBSchemaCreation.Exception.While.Creating.Tables"), ex);
        } finally {
                if (stmt != null) {
                        try {
                                stmt.close();
                        } catch (SQLException e) {
                                mLogger.log(Level.INFO, getPackageBundle().getMessage("DBSchemaCreation.Statement.Close.Exception"), e);                        
                        }
                }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                        mLogger.log(Level.INFO, getPackageBundle().getMessage("DBSchemaCreation.Connection.Close.Exception"), e);
                }
            }
        }
    }
    
    private I18NBundle getPackageBundle() {
        if (I18NPACKAGEBUNDLE == null) {
            I18NPACKAGEBUNDLE = new I18NBundle("com.sun.esb.eventmanagement.impl");
        }
        return I18NPACKAGEBUNDLE;
    }

    private String [] getTabels () {
        String[] tables = {mUniqueTableName};
        return tables;
    }
    
    private  String getCreateScriptName () {
        return CREATE_TABLES_SCRIPT;
    }
    
    private String getDropScriptName() {
        return DROP_TABLES_SCRIPT;
    }
    
    private String getTruncateScriptName() {
        return TRUNCATE_TABLES_SCRIPT;
       
    }

    public void setUniqueTableName(String uniqueTableName) {
        mUniqueTableName = uniqueTableName;
    }

    private String fixTableName(String originalScript) {
        String fixedScript = originalScript.replace("NOTIFICATION_EVENT", 
                mUniqueTableName);
        return fixedScript;
    }




}

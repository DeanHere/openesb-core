package com.sun.esb.eventmanagement.impl;

import com.sun.esb.eventmanagement.api.InstanceIdentifier;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.JBIProvider;
import com.sun.jbi.platform.PlatformContext;

public class InstanceIdentifierFactory {

    public static InstanceIdentifier getInstanceIdentifier(PlatformContext context) {

        JBIProvider provider = context.getProvider();
        
        switch (provider)  {
            case SUNAS:
                return new SunAppServerInstanceIdentifier(context);
        }
        
        return null;
    }
    
}

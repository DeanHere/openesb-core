--Create a tablespace named EVENTDB_USER_DB. Change this value if a different name is desired.
--Specify the name and the location of the file where the data related to the tablespace 
--created above will be stored. The location is by default the location determined by
--the database server/instance on which this script is run
--For example, for Windows c:\MyDatafiles\EVENTDB_USER_DB.dat, for Unix /dev/home1/EVENTDB_USER_DB.dat
--Note that the name of the actual file need not be EVENTDB_USER_DB.dat
--Specify the size constraints

CREATE TABLESPACE "EM_EVENTSTORE_DB"
 DATAFILE
	-- Window and Oracle 9i there is a limitation on file size, it is 2 GB. This by default creats 4GB, add more files if you need more than 4 GB.  
	'EVENTDB_USER_DB.dat' SIZE 2000M --- provide abosolute path if you preference is not default location 'C:\Alaska\EVENTDB_USER_DB.dat' SIZE 2000M,
	,'EVENTDB_USER_DB1.dat' SIZE 2000M --- provide abosolute path if you preference is not default location 'C:\Alaska\EVENTDB_USER_DB1.dat' SIZE 2000M
    ;  -- when TABLESPACE is created with these options performance is degrading gradually as more and more records added to schema  EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO


--Create a new user EVENTDB_USER. Change the name if so desired. Password will be same as 
--the user name by default. This username and password will be used to create the 
--connection pool on the application server. Also specify the tablespace and the quota on 
--the tablespace the user has. Note that if you used a different tablespace name above, 
--you will have to specify that tablespace name here.

CREATE USER EVENTDB_USER IDENTIFIED BY EVENTDB_USER
DEFAULT TABLESPACE EM_EVENTSTORE_DB
QUOTA UNLIMITED ON EM_EVENTSTORE_DB
TEMPORARY TABLESPACE temp
QUOTA 0M ON system;

--Modify the user name if the default user name was changed

GRANT CREATE session to EVENTDB_USER;
GRANT CREATE table to EVENTDB_USER;
GRANT CREATE procedure to EVENTDB_USER;

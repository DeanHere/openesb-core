/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventForwarderMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */


package com.sun.esb.eventmanagement.impl;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.sun.esb.eventmanagement.api.AlertPersistenceDBType;
import com.sun.esb.eventmanagement.api.ConnectionConfiguration;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.EnvironmentContext;

import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.ui.common.I18NBundle;

public final class AlertPersistenceConfiguration implements EventManagementConstants{
    
    
    public final static String ONE_OF_GLASSFISH_DOMAIN_NAMES = "com.sun.appserv"; 
    public final static String DEFAULT_PERSISTENCE_JNDI_NAME = "AlertPersistenceDB"; 
    private I18NBundle I18NPACKAGEBUNDLE;
    private Boolean mPersistedAlertRemovalPolicyExecEnabled;
    private Long mPersistenceAlertAgePolicyValue;
    private Long mPersistenceAlertCountPolicyValue;
    private Long mPersistenceAlertPolicyExecInterval;
    private AlertLevelType mPersistenceAlertLevelPolicyValue;
    private List <AlertRemovalPolicyType> mAlertRemovalPolicyTypeList;
    private AlertPersistenceDBType mAlertPersistenceDBType;
    private String mDataSourceJndiName;
    private Boolean mPersistenceEnabled;
    private Boolean mJournalEnabled;
    private boolean isDBSetup;
    private MBeanServerConnection mMBeanServer;
    private DBEventStore mDBEventStore;
    private static AlertPersistenceConfiguration mAlertPersistenceConfiguration;
    private String mAlertTableName;
    private boolean isAdminServer;
    
    public static synchronized AlertPersistenceConfiguration getInstance(MBeanServerConnection aMBeanServer) {
        if(mAlertPersistenceConfiguration == null) {
            mAlertPersistenceConfiguration =  
                new AlertPersistenceConfiguration(aMBeanServer); 
        }  
        return mAlertPersistenceConfiguration;
        
    }

    private AlertPersistenceConfiguration(MBeanServerConnection aMBeanServer) {
        mAlertRemovalPolicyTypeList =  new ArrayList<AlertRemovalPolicyType>();
        mPersistenceEnabled = new Boolean(false);
        mJournalEnabled = new Boolean(false);
        mPersistedAlertRemovalPolicyExecEnabled = new Boolean(false);
        mMBeanServer = aMBeanServer;
    }

    public Boolean getPersistedAlertRemovalPolicyExecEnabled() {
        return mPersistedAlertRemovalPolicyExecEnabled;
    }
    
    public void setPersistedAlertRemovalPolicyExecEnabled(
            Boolean persistedAlertRemovalPolicyExecEnabled) {
        mPersistedAlertRemovalPolicyExecEnabled = persistedAlertRemovalPolicyExecEnabled;
        if(mDBEventStore != null) {
            mDBEventStore.enablePolicyExecution(persistedAlertRemovalPolicyExecEnabled.booleanValue());
        }
    }
    
    public Long getPersistenceAlertAgePolicyValue() {
        return mPersistenceAlertAgePolicyValue;
    }
    
    public void setPersistenceAlertAgePolicyValue(
            Long persistenceAlertAgePolicyValue) {
        mPersistenceAlertAgePolicyValue = persistenceAlertAgePolicyValue;
    }
    
    public Long getPersistenceAlertCountPolicyValue() {
        return mPersistenceAlertCountPolicyValue;
    }
    
    public void setPersistenceAlertCountPolicyValue(
            Long persistenceAlertCountPolicyValue) {
        mPersistenceAlertCountPolicyValue = persistenceAlertCountPolicyValue;
    }
    
    public Long getPersistenceAlertPolicyExecInterval() {
        return mPersistenceAlertPolicyExecInterval;
    }
    
    public void setPersistenceAlertPolicyExecInterval(
            Long persistenceAlertPolicyExecInterval) {
        mPersistenceAlertPolicyExecInterval = persistenceAlertPolicyExecInterval;
        if(mDBEventStore != null) {
             mDBEventStore.UpdatePolicyExecutionInterval(mPersistenceAlertPolicyExecInterval);
        }
    }
    
    public AlertLevelType getPersistenceAlertLevelPolicyValue() {
        return mPersistenceAlertLevelPolicyValue;
    }
    
    public void setPersistenceAlertLevelPolicyValue(
            AlertLevelType persistenceAlertLevelPolicyValue) {
        mPersistenceAlertLevelPolicyValue = persistenceAlertLevelPolicyValue;
    }

    public List<AlertRemovalPolicyType> getAlertRemovalPolicyTypeList() {
        return mAlertRemovalPolicyTypeList;
    }
    
    public void setAlertRemovalPolicyTypeList(
            List<String> alertRemovalPolicyTypeList) {
        mAlertRemovalPolicyTypeList.clear();
        for (int index = 0; index < alertRemovalPolicyTypeList.size(); index++) {
            mAlertRemovalPolicyTypeList.add(AlertRemovalPolicyType.valueOf(alertRemovalPolicyTypeList.get(index)));
        }
    }

    public void setAlertRemovalPolicyTypeList(
            String alertEnforcedPolicyList) {
        mAlertRemovalPolicyTypeList.clear();
        if(alertEnforcedPolicyList == null || alertEnforcedPolicyList.length() == 0) {
            return;
        }

        StringTokenizer tokenizer = new StringTokenizer(alertEnforcedPolicyList,
                EventManagementConstants.PERSISTENCE_ENFORCED_POLICY_DELIMITER);
        while (tokenizer.hasMoreTokens()) {
            String enforcedPolicy =  tokenizer.nextToken();
            mAlertRemovalPolicyTypeList.add(AlertRemovalPolicyType.valueOf(enforcedPolicy));
        }
    }
    
        
    public String getDataSourceJndiName() {
        return mDataSourceJndiName;
    }

    public void setDataSourceJndiName(String dataSourceJndiName) {
        mDataSourceJndiName = dataSourceJndiName;
    }

    public AlertPersistenceDBType getAlertPersistenceDBType() {
        return mAlertPersistenceDBType;
    }

    public void setAlertPersistenceDBType(
            AlertPersistenceDBType alertPersistenceDBType) {
        mAlertPersistenceDBType = alertPersistenceDBType;
    }

    public Properties getPersistenceProperties() {
        Properties policyProps =  new Properties();
        if(isAdminServer) {
            // the following properties only required by the the controller MBean on the DAS
            policyProps.setProperty(EventManagementConstants.PERSISTENCE_POLICY_EXEC_ENABLED_PROPERTY,
                    mPersistedAlertRemovalPolicyExecEnabled.toString());
            policyProps.setProperty(EventManagementConstants.PERSISTENCE_POLICY_EXEC_INTERVAL_PROPERTY,
                    mPersistenceAlertPolicyExecInterval.toString());
            policyProps.setProperty(EventManagementConstants.PERSISTENCE_POLICY_ALERT_AGE_PROPERTY,
                    mPersistenceAlertAgePolicyValue.toString());
            policyProps.setProperty(EventManagementConstants.PERSISTENCE_POLICY_ALERT_COUNT_PROPERTY,
                    mPersistenceAlertCountPolicyValue.toString());
            policyProps.setProperty(EventManagementConstants.PERSISTENCE_POLICY_ALERT_LEVEL_PROPERTY,
                    mPersistenceAlertLevelPolicyValue.getAlertLevel());
            policyProps.setProperty(EventManagementConstants.JOURNAL_ENABLED_PROPERTY,
                    mJournalEnabled.toString());
            int enforcedPolicySize = mAlertRemovalPolicyTypeList.size();
            if(enforcedPolicySize > 0) {
                StringBuffer enforcedPolicy = new StringBuffer();
               for (int index = 0; index < enforcedPolicySize; index++) {
                   String policyEntry = mAlertRemovalPolicyTypeList.get(index).getPolicyType();
                   enforcedPolicy.append(policyEntry);
                   if(index != enforcedPolicySize -1) {
                       enforcedPolicy.append(EventManagementConstants.PERSISTENCE_ENFORCED_POLICY_DELIMITER); 
                   }
               } 
               policyProps.setProperty(EventManagementConstants.PERSISTENCE_ENFORCED_POLICY_PROPERTY,
                       enforcedPolicy.toString());
            }
        }
        policyProps.setProperty(EventManagementConstants.DATABASE_TYPE_PROPERTY,
                mAlertPersistenceDBType.getDatabasetype());
        policyProps.setProperty(EventManagementConstants.DATABASE_JNDI_NAME_PROPERTY,
                mDataSourceJndiName);
        policyProps.setProperty(EventManagementConstants.PERSISTENCE_ENABLED_PROPERTY,
                mPersistenceEnabled.toString());
        policyProps.setProperty(EventManagementConstants.DATABASE_ALERT_TABLE_NAME_PROPERTY_NAME,
                mAlertTableName);
       
        return policyProps;
    }

    public Boolean getPersistenceEnabled() {
        return mPersistenceEnabled;
    }

    public void setPersistenceEnabled(Boolean persistenceEnabled) throws ManagementRemoteException{
        boolean currentPersistenceEnabledValue = mPersistenceEnabled.booleanValue();
        boolean newPersistenceEnabledValue = persistenceEnabled.booleanValue();
        
        if((currentPersistenceEnabledValue == false) && 
           (newPersistenceEnabledValue == true && isAdminServer) &&
           (isDBSetup == false)) {
            if(createDatabase()) {
                isDBSetup = true;
            }
            
        }
        mPersistenceEnabled = persistenceEnabled;
       if(!mPersistenceEnabled.booleanValue()) {
           // make sure journal attibute is clear if peristence is disabled.
           mJournalEnabled = mPersistenceEnabled;
        }
    }

    public boolean createDatabase() throws ManagementRemoteException {
        // create the pool if needed (in case of derby)
        // otherwise for other database the pool & jdbc resource
        // need to be pre-created.
        boolean done = true;
        ConnectionConfiguration connectionConfig = 
            new ConnectionConfigurationImpl(mDataSourceJndiName,mMBeanServer);
        try {
            if(isGlassfishAppSever() && mAlertPersistenceDBType == AlertPersistenceDBType.DERBY) {
                connectionConfig.createConnectionsPools();
            }
            InitialContext ic = null;
            DataSource aDataSource = null;
            try {
                ic = new InitialContext();
                aDataSource = (DataSource) ic.lookup(mDataSourceJndiName);
            } catch (NamingException e) {
                Exception exp = AlertUtil.createManagementException("caps.management.server.alert.configuration.initialContext.error",
                         null, e);
                 throw new ManagementRemoteException(exp);
            }

            if(aDataSource == null) {
                throw new ManagementRemoteException (getPackageBundle().getMessage("caps.management.server.alert.configuration.invalidDataSource.error",  
                        new Object [] {mDataSourceJndiName}));
            }
            DBSchemaCreation dbschema = new DBSchemaCreation();
            if(isGlassfishAppSever() && mAlertPersistenceDBType == AlertPersistenceDBType.DERBY) {
                dbschema.setUniqueTableName(getAlertTableName());
                dbschema.checkAndCreateTables(mAlertPersistenceDBType, aDataSource);
            }
            if(mDBEventStore != null) {
                mDBEventStore.initializeDataSource();
            }
        } catch (ManagementRemoteException e) {
            // @TODO  report problem as warning and disable persistence
            mPersistenceEnabled = new Boolean(false);
            mJournalEnabled = new Boolean(false);
            done = false;
            throw new ManagementRemoteException(e);
            
        }   
        return done;
    }
    
    private I18NBundle getPackageBundle() {
        if (I18NPACKAGEBUNDLE == null) {
            I18NPACKAGEBUNDLE = new I18NBundle("com.sun.esb.eventmanagement.impl");
        }
        return I18NPACKAGEBUNDLE;
    }

    public void setDBEventStore(DBEventStore eventStore) {
        mDBEventStore = eventStore;
    }

    private boolean isGlassfishAppSever() {
        boolean isGlassFish = false;
        try {
            String[] registeredDomains = mMBeanServer.getDomains();
            for (int index = 0; index < registeredDomains.length; index++) {
                String domain = registeredDomains[index];
                if(domain.indexOf(ONE_OF_GLASSFISH_DOMAIN_NAMES) != -1) {
                    isGlassFish = true;
                    break;
                }
            }
        } catch (IOException e) {
        }
        return isGlassFish;
    }

    public String getAlertTableName() {
        return mAlertTableName;
    }

    public void setAlertTableName(String alertTableName) {
        mAlertTableName = alertTableName;
    }

    public void setAdminServer(boolean isAdminServer) {
        this.isAdminServer = isAdminServer;
    }

    public Boolean isJournalEnabled() {
        return mJournalEnabled;
    }

    public void setJournalEnabled(Boolean journalEnabled) {
        mJournalEnabled = journalEnabled;
    }
    
}


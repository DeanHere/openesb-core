/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallationService.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;

import com.sun.jbi.management.internal.support.DOMUtil;
import com.sun.jbi.management.internal.support.JarFactory;
import com.sun.jbi.management.descriptor.ComponentDescriptor;
import com.sun.jbi.management.registry.data.ComponentInfoImpl;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.repository.RepositoryException;
import com.sun.jbi.management.repository.Archive;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.message.MessageHelper;
import com.sun.jbi.management.system.ManagementException;
import com.sun.jbi.management.util.FileHelper;

import java.io.InputStream;
import java.io.IOException;
import java.io.File;
import java.math.BigInteger;
import java.util.jar.JarFile;
import java.util.List;
import java.util.Set;
import java.util.HashMap;
import java.util.zip.ZipEntry;

import javax.management.ObjectName;
import javax.management.StandardMBean;
import javax.jbi.JBIException;

/**
 * The installation service MBean allows administrative tools to manage
 * component and shared library installations.  The tasks supported are:
 * <ul>
 *   <li>Installing (and uninstalling) a shared library</li>
 *   <li>Creating (loading) and destroying (unloading) a component installer
 *       MBean.</li>
 *   <li>Finding an existing component installer MBean.
 * </ul>
 *
 * Installing and uninstalling components is accomplished using
 * {@link InstallerMBean}s, loaded by this MBean.  An individual installer MBean
 * is needed for each component installation / uninstallation.  This is to support 
 * the more complex installation process that some components require.
 *
 * @author Sun Microsystems, Inc.
 */
public class InstallationService
    extends Facade
    implements com.sun.jbi.management.InstallationServiceMBean
{    
    
    /** the install root directory name **/
    private static final String INSTALL_ROOT = "install_root";
    
    /** the workspace directory name **/
    private static final String WORKSPACE = "workspace";    
    
    /** the delete me marker in repository **/
    private static final String DELETE_ME = ".DELETE_ME";     
    
    /** the delete me marker in repository **/
    private static final String LOGGER_CONFIG = "config";     
        
    /** the upgradeComponent task name **/
    private static final String UPGRADE_COMPONENT = "upgradeComponent";
    
    /** a constant for META-INF **/
    private static final String METAINF = "META-INF";
    
    /** a constant for jbi.xml **/
    private static final String JBIXML = "jbi.xml";
    
    /** a constant for temp dir in instance root **/
    private static final String TMP_DIR = "tmp";
    
    /** a constant for / **/
    private static final String SLASH = "/";
    
    public InstallationService(EnvironmentContext ctx, String target)
        throws ManagementException
    {
        super(ctx, target);        
    }
    
    /**
     * Load the installer for a new component for the given component 
     * installation package.
     *
     * @param installZipURL URL locating a ZIP file containing the
     *        JBI Installation package to be installed; must be non-null, 
     *        non-empty, and a legal URL
     * @return the JMX ObjectName of the InstallerMBean loaded from
     *         installZipURL; must be non-null
     */
    public ObjectName loadNewInstaller(String installZipURL)
    {
        boolean wasPreRegistered = false;
        ObjectName installerObjName = null;
        try
        {
            Archive compArchive = validateArchive(installZipURL, ArchiveType.COMPONENT, 
                "loadNewInstaller");

            ComponentDescriptor descr = new ComponentDescriptor(compArchive.getJbiXml(false));
            ComponentType compType  =  descr.getComponentType();
            String compName = descr.getName();
            
            if ( mTarget.equals(DOMAIN))
            {
                componentRegistrationCheck("loadNewInstaller", compName, false);  
                addComponentToDomain("loadNewInstaller", compName, compArchive);
     
                installerObjName = registerInstallerMBean(compName, compType, "loadNewInstaller");
            }
            else
            {
                /** -- if target anything other than domain
                (a) Component maybe registered
                (b) If already registered make sure archives are identical
                (c) If not registered - register it
                 **/
                
                boolean isRegistered = false;
                try
                {
                    isRegistered = getGenericQuery().isComponentRegistered(compName);
                }
                catch(RegistryException rex)
                {
                    mLog.fine(MessageHelper.getMsgString(rex));
                    throw new RuntimeException(mMsgBuilder.buildExceptionMessage("loadNewInstaller",
                        rex));            
                }
                
                if ( !isRegistered )
                {
                    addComponentToDomain("loadNewInstaller", compName, compArchive);
                }   
                else
                {
                    File newFile = new File(installZipURL);
                    File domainFile = new File(getRegistry().getRepository().findArchive(
                        ArchiveType.COMPONENT, compName));
                    archiveEqualityCheck(newFile, domainFile, ArchiveType.COMPONENT, compName);
                    wasPreRegistered = true;
                    installZipURL = compArchive.getPath();
                }
                
                try
                {
                    installerObjName = loadNewInstallerForTarget(installZipURL, descr);
                }
                catch ( ManagementException mex )
                {
                    if ( !wasPreRegistered )
                    {
                        removeComponentFromDomain("loadNewInstaller", compName);
                    }
                    throw new RuntimeException(mex.getMessage());
                }
            }
            
            return installerObjName;
        }
        catch (ManagementException mex)
        {
            mLog.fine(MessageHelper.getMsgString(mex, mLog.getLevel()));
            throw new RuntimeException(mex.getMessage());
        }
    }

    /**
     * Load the InstallerMBean for a previously installed component.
     * <p>
     * The "component name" refers to the 
     * <code>&lt;identification>&lt;name></code> element value from the 
     * component's installation package (see {@link #loadNewInstaller(String)}).
     * 
     * @param componentName the component name identifying the installer to 
     *        load; must be non-null and non-empty
     * @return the JMX ObjectName of the InstallerMBean loaded from an existing
     *         installation context; <code>null</code> if the installer MBean
     *         doesn't exist
     */
    public ObjectName loadInstaller(String componentName)
    {
        return loadInstaller(componentName, false);
    }

    /**
     * Load the InstallerMBean for a previously installed component.
     * <p>
     * The "component name" refers to the 
     * <code>&lt;identification>&lt;name></code> element value from the 
     * component's installation package (see {@link #loadNewInstaller(String)}).
     * 
     * @param componentName the component name identifying the installer to 
     *        load; must be non-null and non-empty
     * @param force set to <code>true</code> for a forced uninstall.
     * @return the JMX ObjectName of the InstallerMBean loaded from an existing
     *         installation context; <code>null</code> if the installer MBean
     *         doesn't exist
     */
    public ObjectName loadInstaller(String componentName, boolean force)
    {
        mLog.finer("loadInstaller entered with componentName=" + componentName + ", force=" + force);
        try
        {
            componentRegistrationCheck("loadInstaller", componentName, true);  
            ComponentType compType = null;
            ObjectName installerObjName;
            
            try
            {
                compType = getGenericQuery().getComponentType(componentName);
            }
            catch(Exception rex)
            {
                mLog.fine(MessageHelper.getMsgString(rex));
                throw new RuntimeException(
                    mMsgBuilder.buildExceptionMessage("loadInstaller", rex));
            }
            
            
            
            if ( mTarget.equals(DOMAIN))
            {
                installerObjName = registerInstallerMBean(componentName, compType, 
                "loadInstaller");
            }
            else
            {
                installerObjName = loadInstallerForTarget(componentName, compType, force);
            }
            
            return installerObjName;
        }
        catch ( ManagementException mex )
        {
            mLog.fine(MessageHelper.getMsgString(mex, mLog.getLevel()));
            throw new RuntimeException(mex.getMessage());
        }
    }

    /**
     * Unload an InstallerMBean previously loaded for a component.
     *
     * @param componentName the component name identifying the installer to 
     *        unload; must be non-null and non-empty
     * @param isToBeDeleted <code>true</code> if the component is to be deleted 
     *        as well
     * @return true if the operation was successful, otherwise false
     */
    public boolean unloadInstaller(String componentName, boolean isToBeDeleted)
    {
        ObjectName instName = null;
        ObjectName configName;
        
        try
        {
            instName = getRegisteredInstallerMBean(componentName);
            configName = getRegisteredInstallerConfigMBean(componentName);
        }
        catch ( ManagementException mex)
        {
            mLog.fine(MessageHelper.getMsgString(mex, mLog.getLevel()));
            return false;
        }
        
        boolean wasUnloaded = false;
        boolean wasUnloadedFromTarget = true;
        
        if ( mMBeanSvr.isRegistered(instName))
        {
             // -- if target is not Domain unload the installer for the target
            if ( !DOMAIN.equals(mTarget))
            {
                try
                {
                    wasUnloadedFromTarget = unloadInstallerFromTarget(componentName);
                }
                catch(ManagementException mex)
                {
                    throw new RuntimeException(mex.getMessage());
                }
                
            }
            else
            {
                // -- Can unload installer from Domain only if the component is
                // -- not installed on any other target.
                try
                {
                    canUninstallComponentFromDomainCheck(componentName);
                }
                catch (ManagementException ex)
                {
                    throw new RuntimeException(ex.getMessage());
                }    
            }
            
            if ( wasUnloadedFromTarget )
            {
                try
                {
                    mMBeanSvr.unregisterMBean(instName);
                    
                    // Installer config MBeans are optional
                    if (mMBeanSvr.isRegistered(configName))
                    {
                        mMBeanSvr.unregisterMBean(configName);
                    }
                }
                catch (javax.management.JMException jmex)
                {
                    mLog.fine(MessageHelper.getMsgString(jmex));
                    wasUnloaded = false;
                }
                wasUnloaded = true;
            }
            try
            {
                if ( isToBeDeleted )
                {
                    wasUnloaded = removeComponentFromDomain("unloadInstaller", componentName);
                }
            }
            catch(ManagementException mex)
            {
                throw new RuntimeException(mex.getMessage());
            }
        }
        else
        {
            mLog.fine("Installer MBean " + instName + " not registered.");
        }
        return wasUnloaded;
    }

    /**
     * Install a shared library installation package.
     * <p>
     * The return value is the unique name for the shared-library, as found
     * in the the value of the installation descriptor's 
     * <code>&lt;identification>&lt;name></code> element.
     * 
     * @param slZipURL URL locating a zip file containing a shared library
     *        installation package; must be non-null, non-empty, and a legal
     *        URL
     * @return the unique name of the shared library loaded from slZipURL; must
     *         be non-null and non-empty
     */
    public String installSharedLibrary(String slZipURL)
    {
        try
        {
            boolean wasPreRegistered = false;
            
            Archive slArchive = validateArchive(slZipURL, ArchiveType.SHARED_LIBRARY,
                    "installSharedLibrary");
            
            String slName = 
                    slArchive.getJbiXml(false).getSharedLibrary().getIdentification().getName();
            
            if ( mTarget.equals(DOMAIN))
            {
                sharedLibraryRegistrationCheck("installSharedLibrary", slName, false);
                addSharedLibraryToDomain("installSharedLibrary", slName, slArchive );
            }
            else
            {
                /** -- if target anything other than domain
                (a) Shared Library  maybe registered
                (b) If already registered make sure archives are identical
                (c) If not registered - register it
                 **/
                boolean isRegistered = false;
                boolean isSystemLibrary = false;
                try
                {
                    isRegistered = getGenericQuery().isSharedLibraryRegistered(slName);
                }
                catch(RegistryException rex)
                {
                    mLog.fine(MessageHelper.getMsgString(rex));
                    throw new RuntimeException(mMsgBuilder.buildExceptionMessage("installSharedLibrary",
                        rex));            
                }

                if ( !isRegistered )
                {
                    addSharedLibraryToDomain("installSharedLibrary", slName, slArchive );
                }   
                else
                {
                    File newFile = new File(slZipURL);
                    File domainFile = new File(getRegistry().getRepository().findArchive(
                        ArchiveType.SHARED_LIBRARY, slName));
                    archiveEqualityCheck(newFile, domainFile, ArchiveType.SHARED_LIBRARY, slName);
                    wasPreRegistered = true;
                }

                // -- Install to Target
                try
                {
                    String path = getRegistry().getRepository().findArchive(ArchiveType.SHARED_LIBRARY, slName);
                    mLog.fine("Installing shared library " + path + " to target " + mTarget);
                    installSharedLibraryToTarget(slName, path);

                }
                catch ( ManagementException mex )
                {
                    // Complete failure : remove the SA from reg/rep if was added 
                    // with this deploy
                    if ( !wasPreRegistered )
                    {
                        removeSharedLibraryFromDomain("installSharedLibrary", slName);
                    }
                    throw new RuntimeException(mex.getMessage());
                }
            }
            return slName;
        }
        catch(ManagementException mex)
        {
            mLog.fine(MessageHelper.getMsgString(mex, mLog.getLevel()));
            throw new RuntimeException(mex.getMessage());            
        }
    }

    /**
     * Uninstall a previously installed shared library.
     *
     * @param slName the name of the shared name space to uninstall; must be
     *        non-null and non-empty
     * @return true if the uninstall was successful
     */
    public boolean uninstallSharedLibrary(String slName)
    {
        // default keep = false
        return uninstallSharedLibrary(slName, false);
    }
    
    /*---------------------------------------------------------------------------------*\
     *                      Extended InstallationServiceMBean ops                        *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Load the installer for a component from the Repository. The component archive from 
     * the repository is uploaded to the target instance(s), loadNewInstaller() in
     * invoked on the remote instance(s). The object name of the facade Installer MBean 
     * is returned.
     * <br/>
     * If the component is not there in the DAS repository a exception will
     * be thrown. 
     * 
     * @param componentName - name of the registered component.
     * @return the ObjectName of facade Installer MBean for the registered component
     * @exception javax.jbi.JBIException if the component is not registered.
     */
     public ObjectName loadInstallerFromRepository(String componentName)
        throws javax.jbi.JBIException
     {
         ObjectName facadeInstallerObjName = null;
         try
         {
             if (DOMAIN.equals(mTarget))
             {
                 throwNotSupportedManagementException("installComponentFromRepository");
             }
                      
            // Check if component is there in repos - exception otherwise
            componentRegistrationCheck("loadNewInstaller", componentName, true); 

            // Get the comp archive, path, descr
            Archive compArchive = 
                    getRegistry().getRepository().getArchive(
                        ArchiveType.COMPONENT,
                        componentName);
            ComponentDescriptor descr = new ComponentDescriptor(compArchive.getJbiXml(false));
            String installZipURL = compArchive.getPath();
            
            mLog.fine("Installing component from repository " + installZipURL + 
                " to target " + mTarget);
            
             // Load New installer for target ( this intrinsically checks if component is 
             // installed first )
            facadeInstallerObjName = loadNewInstallerForTarget(installZipURL, descr);

         }
        catch (ManagementException mex)
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
         
         return facadeInstallerObjName;
     }
     
    /**
     * Install a shared library from the Repository. The library archive from the 
     * repository is uploaded to the target instance(s) and installSharedLibrary() is 
     * invoked on the remote instance InstallationService with the uploaded file name.
     * <br/>
     * If the shared library is not there in the DAS repository an exception will
     * be thrown. 
     * 
     * @param sharedLibraryName - name of the registered shared library.
     * @return the shared library name
     * @exception javax.jbi.JBIException if the shared library is not registered or
     * uninstall fails.
     */
     public String installSharedLibraryFromRepository(String sharedLibraryName)
        throws javax.jbi.JBIException
     {
        try
        {
             if (DOMAIN.equals(mTarget))
             {
                 throwNotSupportedManagementException("installSharedLibraryFromRepository");
             }
                         
            sharedLibraryRegistrationCheck("installSharedLibraryFromRepository", 
                sharedLibraryName, true);  

            // -- Install to Target
            Archive slArchive = 
                getRegistry().getRepository().getArchive(
                    ArchiveType.SHARED_LIBRARY, 
                    sharedLibraryName);
            String slZipUrl = slArchive.getPath();
            mLog.fine("Installing shared library from repository " + slZipUrl + 
                " to target " + mTarget);
            installSharedLibraryToTarget(sharedLibraryName, slZipUrl);
        }
        catch (ManagementException mex)
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
        return sharedLibraryName;
     }
     
     
    /**
     * Uninstall a previously installed shared library.
     *
     * @param slName the name of the shared name space to uninstall; must be
     *        non-null and non-empty
     * @param keep if true the shared libray is not deleted from the domain. 
     *        If false, the shared library is deleted from the repository 
     *        if after this uninstall it is not installed on any targets.
     * @return true if the uninstall was successful
     */
    public boolean uninstallSharedLibrary(String slName, boolean keep)
    {
        String sccsWithWarning = null;
        try
        {
            sharedLibraryRegistrationCheck("uninstallSharedLibrary", slName, true);
            
            // -- If target other than domain then uninstall first
            if ( !DOMAIN.equals(mTarget) )
            {
                sccsWithWarning = uninstallSharedLibraryFromTarget(slName);
            }
            else
            {
                if (isSharedLibraryInstalled(slName))
                {
                    String[] params = new String[] {slName};
                    mMsgBuilder.throwManagementException("uninstallSharedLibrary",
                        LocalStringKeys.JBI_ADMIN_SHARED_LIBRARY_INSTALLED_ON_TARGETS, params);
                }
            }
            // -- Remove the Shared Library from Reg/Rep
            if ( !keep )
            {
                removeSharedLibraryFromDomain("uninstallSharedLibrary", slName);
            }
            
            // Take care of the case when the shared library is uninstalled
            // successfully but with a warning
            if (sccsWithWarning != null)
            {
                throw new RuntimeException(sccsWithWarning);
            }
            return true;
        }
        catch (ManagementException mex)
        {
            mLog.fine(MessageHelper.getMsgString(mex, mLog.getLevel()));
            throw new RuntimeException(mex.getMessage());
        }
    }
    
   /**
    * Upgrade a component. This is used to perform an upgrade of the runtime
    * files of a component without requiring undeployment of Service Assemblies
    * with Service Units deployed to the component.
    * @param componentName The name of the component.
    * @param installZipURL The URL to the component archive.
    * @return a status management message that contains component upgrade result
    * @throws JBIException if there is a problem with the upgrade.
    */
    public String upgradeComponent(String componentName, String installZipURL)
    throws javax.jbi.JBIException
    {

        mLog.finer("upgradeComponent in facade installation service entered");
        
        if (!mTarget.equals(DOMAIN))
        {
            String message = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_TARGET_ONLY);
            mLog.fine(message);
            throw new JBIException(message);
        }
        try 
        {
            validateComponentRegistration(componentName);
            validateArchive(installZipURL, ArchiveType.COMPONENT, "upgradeComponent");   
            validateComponentName(componentName, installZipURL);
            validateComponentStateInAllTargets(componentName);
        } 
        catch(ManagementException mEx)
        {
            mLog.fine(MessageHelper.getMsgString(mEx));
            throw new JBIException(mEx.getMessage());
        }

        mLog.fine("Preconditions for upgrade are satisfied for component: " + componentName);                
        
        String backupDir = 
                    mEnvCtx.getJbiInstanceRoot() + File.separator + 
                    "tmp" + File.separator + 
                    componentName + "_" + 
                    getRegistry().getGenericQuery().getComponentUpgradeNumber(componentName) 
                    + "_" + FileHelper.getTimestamp();
        
        String existingInstallRoot = getComponentInstallRoot(componentName);
        //existingInstallRoot will be <instanceRoot>/jbi/components/sun-http-binding/install_root
        String componentRoot = new File(existingInstallRoot).getParent();
        //componentRoot will be <instanceRoot>/jbi/components/sun-http-binding
        Registry registry = getRegistry();
        synchronized(registry)
        {
            try 
            {
                updateInstallRootInDomainRepository(backupDir, componentName, installZipURL);            
            }
            catch (ManagementException mEx)
            {
                mLog.fine(MessageHelper.getMsgString(mEx));
                restoreInstallRootInDomainRepository(
                        componentName, 
                        backupDir, 
                        componentRoot);
                throw new JBIException(mEx.getMessage());
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                mLog.fine(MessageHelper.getMsgString(ex));
                throw new JBIException(ex.getMessage());            
            }
        }

        
        /* 
         * Here we are talking to the instance mbeans directly, instead of going through
         * the cluster/instance facade mbeans. This is different from other use cases
         */
        List<String> updateInstances = getAllInstancesInstallingComponent(componentName);

        MessageBuilder.Message result = upgradeComponent(componentName, installZipURL, updateInstances);      
        if ( result.isSuccess() )
        {
            incrementComponentUpgradeNumber(componentName);
            mLog.info(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_COMPONENT_UPGRADE_SUCCESSFUL, 
                    componentName));
            return result.getMessage();
        }
        else
        {
            restoreInstallRootInDomainRepository(componentName, backupDir, componentRoot);   
            mLog.fine(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_COMPONENT_UPGRADE_FAILED, 
                    componentName));            
            throw new JBIException(result.getMessage());
        }
    }

    
    /*----------------------------------------------------------------------------------*\
     *                          Private Helpers                                         *
    \*----------------------------------------------------------------------------------*/
   
    /**
     * Register the InstallerMBean for the Component.
     */
    private ObjectName registerInstallerMBean(String componentName, ComponentType type,
        String taskId)
        throws ManagementException
    {
        return registerInstallerMBean(componentName, type, taskId, null);
    }
    
    /**
     * Register the InstallerMBean for the Component.
     *
     * @param instanceInstMBean - Object Name of the InstallerMBean for the Instance which,
     * this InstallerMBean is a proxy to.
     */
    private ObjectName registerInstallerMBean(String componentName, ComponentType type,
        String taskId, HashMap instanceInstMBeans)
        throws ManagementException
    {
        try
        {
            
            ObjectName instName = null;
            
            if ( ComponentType.BINDING.equals(type) )
            {
                instName = mMBeanNames.getBindingMBeanName(componentName, 
                        MBeanNames.ComponentServiceType.Installer, mTarget);
            }
            else if ( ComponentType.ENGINE.equals(type) )
            {
                instName = mMBeanNames.getEngineMBeanName(componentName, 
                        MBeanNames.ComponentServiceType.Installer, mTarget);
            }

            if ( !mMBeanSvr.isRegistered(instName) )
            {
                
                com.sun.jbi.management.InstallerMBean
                    installer = new Installer(mEnvCtx, mTarget, 
                        componentName, instanceInstMBeans);
                StandardMBean mbean = new StandardMBean(installer,
                    com.sun.jbi.management.InstallerMBean.class); 
                mMBeanSvr.registerMBean(mbean, instName);
            }
            else
            {
                mLog.fine("Installer MBean " + instName +" already registered");
            }
                
            return instName; 
        }
        catch ( Exception ex )
        {
            String [] params = new String[]{componentName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, 
                LocalStringKeys.JBI_ADMIN_FAILED_LOAD_NEW_INSTALLER, params);
            
            // never reached
            return null;
        }
    }
    
    /**
     * If the target is a standalone server instance, first the instances
     * loadNewInstaller() operation is invoked, the object name of the returned Installer
     * MBean is passed to the proxy InstallerMBean which is loaded for the instance.
     * The proxy facade InstallerMBean is then registered in the DAS.
     *
     * If the target is a cluster, for each instance in the cluster the loadNewInstaller() 
     * operation is invoked, the object names of the returned Installer
     * MBeans is passed to the proxy InstallerMBean which is loaded for the cluster.
     * The proxy facade InstallerMBean is then registered in the DAS.
     */
    private ObjectName loadNewInstallerForTarget(String installZipURL, 
        ComponentDescriptor descr)
        throws ManagementException
    {
        // throw exception if component already installed on target
        componentInstallationCheck(descr.getName(), true);
        
        /**
         * The Installation service on each instance calls the frameworks loadBootstrap() 
         * when asked to loadNewInstaller(). The frameworks loadBootstrap() checks if
         * the shared libraries required by the component are installed. This check
         * is being made on the DAS so that we report failure much earlier
         */
        dependentSharedLibraryInstallationCheck(descr.getName(),
            descr.getSharedLibraryIds());
        
        mLog.fine(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_LOADING_NEW_INSTALLER_ON_TARGET,
            descr.getName(), mTarget));
        
        ObjectName remoteInstObjName = null;
        HashMap<String, ObjectName> instInstallerMBeans = null; 
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            remoteInstObjName =  loadNewInstallerForInstance(mTarget, installZipURL);
            instInstallerMBeans = new HashMap<String, ObjectName>();
            instInstallerMBeans.put(mTarget, remoteInstObjName);
        }
        else
        {
            instInstallerMBeans = loadNewInstallerForCluster(mTarget, installZipURL);
        }
        
        return registerInstallerMBean(descr.getName(), descr.getComponentType(), 
            "loadNewInstaller", instInstallerMBeans);
    }
    
    /**
     * If the target is a standalone server instance, first the instances
     * loadInstaller() operation is invoked, the object name of the returned Installer
     * MBean is passed to the proxy InstallerMBean which is loaded for the instance.
     * The proxy facade InstallerMBean is then registered in the DAS.
     *
     * If the target is a cluster, for each instance in the cluster the loadInstaller() 
     * operation is invoked, the object names of the returned Installer
     * MBeans is passed to the proxy InstallerMBean which is loaded for the cluster.
     * The proxy facade InstallerMBean is then registered in the DAS.
     * @param compName the name of the component whose installer is to be loaded.
     * @param compType the type of the component (binding or engine).
     * @param force set to <code>true</code> for a forced uninstall.
     * @return the JMX ObjectName of the proxy InstallerMBean.
     */
    private ObjectName loadInstallerForTarget(String compName, ComponentType compType, boolean force)
        throws ManagementException
    {
        // throw an exception if the component is not installed on the target
        componentInstallationCheck(compName, false);
        
        mLog.fine(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_LOADING_INSTALLER_ON_TARGET,
            compName, mTarget));
                
        ObjectName remoteInstObjName = null;
        HashMap<String, ObjectName> instInstallerMBeans = null; 
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            remoteInstObjName =  loadInstallerForInstance(mTarget, compName, force);
            instInstallerMBeans = new HashMap<String, ObjectName>();
            instInstallerMBeans.put(mTarget, remoteInstObjName);
        }
        else
        {
            instInstallerMBeans = loadInstallerForCluster(mTarget, compName, force);
        }
        
        return registerInstallerMBean(compName, compType, "loadInstaller", instInstallerMBeans);
    }
    
    /**
     * If the target is a standalone server instance then the Installer MBean for the
     * component on the instance is unloaded ( InstallationService.unloadInstaller() )
     *
     * If the target is a cluster then for each instance in the cluster the unloadInstaller() 
     * operation is invoked.
     */
    private boolean unloadInstallerFromTarget(String componentName)
        throws ManagementException
    {
        mLog.fine(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_UNLOADING_INSTALLER_FROM_TARGET,
            componentName, mTarget));
        
        boolean unloaded = false;
        boolean isToBeDeletedFromInstance = true;
        
        /** If component is installed on instance isToBeDeletedFromInstance = false else
            its true.
         */
        if ( isComponentInstalled(componentName) )
        {
            isToBeDeletedFromInstance = false;
        }
        
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            unloaded =  unloadInstallerFromInstance(mTarget, componentName, isToBeDeletedFromInstance);
        }
        else
        {
            unloaded = unloadInstallerFromCluster(mTarget, componentName, isToBeDeletedFromInstance);
        }
        
        return unloaded;
    }
    
    /**
     * @return the ObjectName of a registered InstallerMBean for a component
     */
    private ObjectName getRegisteredInstallerMBean(String componentName)
        throws ManagementException
    {
        ComponentType compType = getComponentType(componentName);
        ObjectName instName = null;
        
        if ( ComponentType.BINDING.equals(compType) )
        {
            instName = mMBeanNames.getBindingMBeanName(componentName, 
                    MBeanNames.ComponentServiceType.Installer, mTarget);
        }
        else if ( ComponentType.ENGINE.equals(compType) )
        {
            instName = mMBeanNames.getEngineMBeanName(componentName, 
                    MBeanNames.ComponentServiceType.Installer, mTarget);
        }
        else
        {
            String[] params = new String[]{componentName};
            mMsgBuilder.throwManagementException("unloadInstaller", LocalStringKeys.JBI_ADMIN_UNKNOWN_COMPONENT, params); 
        }
        return instName;
    }
    
    /**
     * Returns the ObjectName of a component's installer configuration MBean.
     * @return ObjectName of config MBean 
     */
    private ObjectName getRegisteredInstallerConfigMBean(String componentName)
        throws ManagementException
    {
        return mMBeanNames.getComponentMBeanName(componentName, 
                getComponentType(componentName), 
                MBeanNames.ComponentServiceType.InstallerConfiguration, 
                mTarget);
    }
    
    /**
     * Throw a ManagementException based on Component Registration status
     */
    private void componentRegistrationCheck(String taskId, 
        String componentName, boolean throwExWhenNotRegistered)
        throws ManagementException
    {
        boolean registered = false;
        
        try
        {
            registered = getGenericQuery().isComponentRegistered(componentName);
        }
        catch(RegistryException rex)
        {
            mLog.fine(MessageHelper.getMsgString(rex));
            throw new RuntimeException(mMsgBuilder.buildExceptionMessage("componentRegistrationCheck",
                rex));            
        }    
        
        if ( !registered && throwExWhenNotRegistered)
        {
            String[] params = new String[]{componentName};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_UNKNOWN_COMPONENT, params); 
        }
        
        if ( registered && !throwExWhenNotRegistered)
        {
            String[] params = new String[]{componentName};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_KNOWN_COMPONENT, params); 
        }
    }
    
    /**
     * Throw a ManagementException based on Shared Library registration status
     */
    private void sharedLibraryRegistrationCheck(String taskId, 
        String libraryName, boolean throwExWhenNotRegistered)
        throws ManagementException
    {
        boolean registered =  false;
      
        try
        {
            registered = getGenericQuery().isSharedLibraryRegistered(libraryName);
        }
        catch(RegistryException rex)
        {
            mLog.fine(MessageHelper.getMsgString(rex));
            throw new RuntimeException(mMsgBuilder.buildExceptionMessage(
                "sharedLibraryRegistrationCheck",
                rex));            
        }  
        
        if ( !registered && throwExWhenNotRegistered)
        {
            String[] params = new String[]{libraryName};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_UNKNOWN_SHARED_LIBRARY, params); 
        }
        
        if ( registered && !throwExWhenNotRegistered)
        {
            String[] params = new String[]{libraryName};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_KNOWN_SHARED_LIBRARY, params);            
        }
    }
    
    /**
     * Add Shared Library Archive to the repository and register in the domain
     */
    private void addSharedLibraryToDomain(String taskId, String slName, Archive slArchive )
        throws ManagementException
    {
        String archivePath = slArchive.getPath();
        
        try
        {
            mLog.finer("Adding shared library " + slName + " to the domain");
            getRegistry().getRepository().addArchive(slArchive);
            getUpdater().addSharedLibrary(slName, slArchive.getFileName(), slArchive.getUploadTimestamp());
        }
        catch ( RegistryException ex )
        {
            // -- Remove from repos if added (rollback)
            /**
            try
            {
                mRepository.removeArchive(ArchiveType.SHARED_LIBRARY, slName);
            }
            catch(RepositoryException rex)
            {
                mLog.fine(rex.getMessage());
            }
             */
            
            String[] params = new String[]{slName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_FAILED_ADD_SHARED_LIBRARY, params);
        }
        catch ( RepositoryException ex )
        {   
            String[] params = new String[]{slName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_FAILED_ADD_SHARED_LIBRARY, params);
        }
    }
    
    /**
     * Remove the Shared Library Archive from the repository and registry
     */
    private void removeSharedLibraryFromDomain(String taskId, String slName)
        throws ManagementException
    {
        boolean isSystemLibrary = false;
        try
        {
            if ( !getGenericQuery().isSharedLibraryInstalled(slName))
            {
                getUpdater().removeSharedLibrary(DOMAIN, slName);
                getRegistry().getRepository().removeArchive(ArchiveType.SHARED_LIBRARY, slName);
            }
        }
        catch ( RepositoryException ex )
        {   
            String[] params = new String[]{slName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_FAILED_REMOVE_SHARED_LIBRARY, params);
        }
        catch ( RegistryException ex )
        {   
            String[] params = new String[]{slName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_FAILED_REMOVE_SHARED_LIBRARY, params);
        }
    }
    
    /**
     * Add Component Archive to the repository and register in the domain
     */
    private void addComponentToDomain(String taskId, String compName, Archive compArchive )
        throws ManagementException
    {
        try
        {
            mLog.fine("Adding component " + compName + " to the domain");
            getRegistry().getRepository().addArchive(compArchive);
            getUpdater().addComponent(compName, compArchive.getFileName(), compArchive.getUploadTimestamp());
        }
        catch ( RegistryException ex )
        {
            // -- Remove from repos if added (rollback)
            /**
            try
            {
                mRepository.removeArchive(ArchiveType.COMPONENT, compName);
            }
            catch(RepositoryException rex)
            {
                mLog.fine(rex.getMessage());
            }*/
            
            String[] params = new String[]{compName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_FAILED_ADD_COMPONENT, params);
        }
        catch ( RepositoryException ex )
        {   
            String[] params = new String[]{compName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_FAILED_ADD_COMPONENT, params);
        }
    }
    
    /**
     * Add component Archive to the repository and register in the domain
     */
    private boolean removeComponentFromDomain(String taskId, String compName)
        throws ManagementException
    {
        boolean removed = false;
        try
        {
            if ( !getGenericQuery().isComponentInstalled(compName))
            {
                // Unregister the ComponentLifeCycleMBean registered for target=domain ( if registered )
                try
                {
                    unregisterComponentExtensionMBean(DOMAIN, compName);
                    unregisterComponentLifeCycleMBean(DOMAIN, compName);
                }
                catch (Exception ex)
                {
                    mLog.fine(MessageHelper.getMsgString(ex));
                }

              
                getUpdater().removeComponent(DOMAIN, compName);
                getRegistry().getRepository().removeArchive(ArchiveType.COMPONENT, compName);

                removed = true;
            }
        }
        catch ( RepositoryException ex )
        {   
            String[] params = new String[]{compName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_FAILED_REMOVE_COMPONENT, params);
        }
        catch ( RegistryException ex )
        {   
            String[] params = new String[]{compName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, LocalStringKeys.JBI_ADMIN_FAILED_REMOVE_COMPONENT, params);
        }


        return removed;
    }
    
    private ObjectName getRemoteInstallationServiceObjectName(String instanceName)
    {
        MBeanNames mbnNames = mMgtCtx.getMBeanNames(instanceName);
        return  mbnNames.getSystemServiceMBeanName(
            MBeanNames.SERVICE_NAME_INSTALL_SERVICE,
            MBeanNames.CONTROL_TYPE_INSTALL_SERVICE);
    }
    
    /**
     * Install the shared library to the remote target. Target can be a standalone server
     * instance or a cluster, in the cluster case all the instances in the cluster are
     * targeted.
     */ 
    private String installSharedLibraryToTarget(String slName, String slInstallZipPath)
        throws ManagementException
    {
        String response = null;
        mLog.info(mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_INSTALLING_SHARED_LIBRARY_TO_TARGET, slName, mTarget) );
        sharedLibraryInstallationCheck(slName, true);
        
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            response =  installSharedLibraryToInstance(mTarget, slName, slInstallZipPath);
        }
        else
        {
            response = installSharedLibraryToCluster(mTarget, slName, slInstallZipPath);
        }
        
        addSharedLibraryToTarget(slName);  
        return response;
    }
    
    /**
     * Install the Shared Library to a single instance and return the result
     */
    private String installSharedLibraryToInstance(String serverName, String slName, 
        String slInstallZipPath)
        throws ManagementException
    {
        
        if ( isInstanceRunning(serverName) )
        {
            mLog.fine("Installing shared library to instance " + serverName);   
            if ( !serverName.equals(mPlatform.getAdminServerName()) )
            {
                // upload the zip file to the instance
                // slInstallZipPath is changed to the uploaded file path
                slInstallZipPath = uploadFile(serverName, slInstallZipPath);
            }
            else
            {
                // Convert the install zip path to a file URL
                slInstallZipPath = toFileURL(slInstallZipPath);
            }

            String[] sign   = new String[]{"java.lang.String"};
            String[] params = new String[]{slInstallZipPath};

            slName = (String) invokeRemoteOperation(getRemoteInstallationServiceObjectName(serverName),  
                    "installSharedLibrary", params, sign, serverName);
        }       
       return slName;
    }
    
    /**
     * Install shared library to a cluster.
     *
     * @param clusterName - name of the cluster to install to.
     * @param slName - name of the shared library
     * @param slInstallZipPath - path to the shared library
     */
    private String installSharedLibraryToCluster(String clusterName, String slName, 
        String slInstallZipPath) throws ManagementException
    {
        String response = slName;
       
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            for ( String instance : instances )
            {
                try
                {
                    response = installSharedLibraryToInstance(instance, slName, 
                        slInstallZipPath);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            if ( exceptionMap.size() > 0 )
            {
                handleClusteredInstanceFailures("installSharedLibrary", 
                    exceptionMap,     
                    instances.size(), 
                    LocalStringKeys.JBI_ADMIN_FAILED_INSTALL_SHARED_LIBRARY_TO_INSTANCE);
            }
        }
        return response;
    }
    
    /**
     * Uninstall the shared library from the remote target. Target can be a standalone server
     * instance or a cluster, in the cluster case all the instances in the cluster are
     * targeted.
     *
     * @return a string indicating the uninstall status. If the instance throws a SUCCESS
     * with WARNING management exception, then the exception message is returned. If 
     * the operation was a complete success then a value of null is returned. The reason 
     * the return value is not clean is that InstallationService.uninstallSharedLibrary
     * definition/implementation is not clean, it is supposed to return true on success
     * and false on failure, but when a shared library is uninstalled with a installed
     * dependent component in the shutdown state a runtime SUCCESS with WARNING exception 
     * is thrown, this actually is a success case. The warning needs to be propogated 
     * to the user, so we return the success with warning jbi message in this case. 
     *
     * If the uninstall is a complete success the return string is a null string.
     *
     * @param slName - name of the shared library to uninstall.
     * 
     */ 
    private String uninstallSharedLibraryFromTarget(String slName)
        throws ManagementException
    {
        boolean uninstalled = false;
        boolean isSuccess   = false;
        String  result      = null;
        
        sharedLibraryInstallationCheck(slName, false);
        dependentComponentCheck(mTarget, slName);
        
        mLog.fine(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_UNINSTALLING_SHARED_LIBRARY_FROM_TARGET, slName, mTarget) );
        
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                uninstalled =  uninstallSharedLibraryFromInstance(mTarget, slName);
            }
            else
            {
                uninstalled = uninstallSharedLibraryFromCluster(mTarget, slName);
            }
        }
        catch(ManagementException mex )
        {
            if ( mMsgBuilder.isXmlString(mex.getMessage()) )
            {
                com.sun.jbi.management.message.MessageBuilder.Message
                    msg = mMsgBuilder.createMessage(mex.getMessage());
                isSuccess = msg.isSuccess();
            }
            if ( !isSuccess )
            {
                throw mex;
            }
            else
            {
                result = mex.getMessage();
            }
        }
        
        if ( uninstalled || isSuccess )
        {
            removeSharedLibraryFromTarget(slName);
        }
        return result;
    }
    
    /**
     * Install the Shared Library from a single instance and return the result.
     */
    private boolean uninstallSharedLibraryFromInstance(String serverName, String slName)
        throws ManagementException
    {
        boolean success = true;
        
        if ( isInstanceRunning(serverName) )
        {
            mLog.fine("Uninstalling shared library from instance " + serverName);
            

            String[] sign   = new String[]{"java.lang.String"};
            String[] params = new String[]{slName};
            Boolean rslt    = (Boolean) invokeRemoteOperation(
                getRemoteInstallationServiceObjectName(serverName), 
                "uninstallSharedLibrary", params, sign, serverName);
            success = rslt.booleanValue();
        }
        return success;
    }
    
    /**
     * Install shared library to a cluster.
     *
     * @param clusterName - name of the cluster to install to.
     * @param slName - name of the shared library
     * @param slInstallZipPath - path to the shared library
     */
    private boolean uninstallSharedLibraryFromCluster(String clusterName, String slName)
        throws ManagementException
    {
        boolean uninstalled = true;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap();
            for ( String instance : instances )
            {
                try
                {
                    boolean currResult = uninstallSharedLibraryFromInstance(instance, slName);
                    uninstalled = ( uninstalled & currResult );
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            if ( exceptionMap.size() > 0 )
            {
                handleClusteredInstanceFailures("uninstallSharedLibrary", 
                    exceptionMap,     
                    /** This is the going down case. Uninstall is a success if the
                     * library is uninstalled from all instances in the cluster.
                     * Even a single exception = failure
                     */
                    exceptionMap.size(), 
                    LocalStringKeys.JBI_ADMIN_FAILED_UNINSTALL_SHARED_LIBRARY_FROM_INSTANCE);
            }
        }
        else
        {
            uninstalled = true;
        }
        return uninstalled;
    }
    
    /**
     * Invoke loadNewInstaller on the instance
     * and return the MBeans Object Name
     */
    private ObjectName loadNewInstallerForInstance(String serverName, 
        String installZipURL) throws ManagementException
    {
        ObjectName remoteInstaller = null;
        if ( isInstanceRunning(serverName) )
        {
            if ( !serverName.equals(mPlatform.getAdminServerName()) )
            {
                // upload the zip file to the instance
                // installZipURL is changed to the uploaded file path
                installZipURL = uploadFile(serverName, installZipURL);
            }
            else
            {
                // Convert the install zip path to a file URL
                installZipURL = toFileURL(installZipURL);
            }

            String[] sign   = new String[]{"java.lang.String"};
            String[] params = new String[]{installZipURL};

            remoteInstaller = (ObjectName) invokeRemoteOperation(
                getRemoteInstallationServiceObjectName(serverName),  
                    "loadNewInstaller", params, sign, serverName);
        }
        return remoteInstaller;
    }
    
    /**
     * Invoke loadNewInstaller on each instance in the cluster 
     * and return a map of the instance names and the component installer MBean 
     * object name.
     *
     * Partial Success : If loadNewInstaller on any instance in the cluster fails, 
     * a warning is logged for it, and the installation continues for the 
     * remaining instances.  A value of null is added to the HashMap for the object name.
     * </br>
     * Complete failure : All the runtime execptions thrown by the remote instance
     * are composed into a management task message and a management exception is 
     * thrown with the composite message.
     */
    private HashMap<String, ObjectName> loadNewInstallerForCluster(String clusterName, 
        String installZipURL)  throws ManagementException
    {
        HashMap<String, ObjectName> installerMBeans = new HashMap<String, ObjectName>();
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap();
            for ( String instance : instances )
            {
                try
                {
                    ObjectName installerMBean = loadNewInstallerForInstance(instance, 
                        installZipURL);
                    installerMBeans.put(instance, installerMBean);
                    
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);
                    installerMBeans.put(instance, null);
                    continue;
                }
            }

            if ( exceptionMap.size() > 0 )
            {
                handleClusteredInstanceFailures("loadNewInstaller", 
                    exceptionMap,     
                    instances.size(), 
                    LocalStringKeys.JBI_ADMIN_FAILED_LOAD_NEW_INSTALLER_ON_INSTANCE);
            }
        }
        return installerMBeans;
    }
      
 
    /**
     * Invoke loadInstaller on each instance in the cluster 
     * and return a map of the instance names and the component installer MBean 
     * object name.
     *
     * Partial Success : If loadInstaller on any instance in the cluster fails, 
     * a warning is logged for it, and the installation continues for the 
     * remaining instances.  A value of null is added to the HashMap for the object name.
     * </br>
     * Complete failure : All the runtime execptions thrown by the remote instance
     * are composed into a management task message and a management exception is 
     * thrown with the composite message.
     * @param clusterName the name of the cluster.
     * @param compName the name of the component whose installer is to be loaded.
     * @param force set to <code>true</code> for a forced uninstall.
     * @return a map of instance names to component installer MBean object names.
     */
    private HashMap<String, ObjectName> loadInstallerForCluster(String clusterName, String compName, boolean force)
        throws ManagementException
    {
        HashMap<String, ObjectName> installerMBeans = new HashMap<String, ObjectName>();
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap();
            for ( String instance : instances )
            {
                try
                {
                    ObjectName installerMBean = loadInstallerForInstance(instance, 
                        compName, force);
                    installerMBeans.put(instance, installerMBean);
                    
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);
                    installerMBeans.put(instance, null);
                    continue;
                }
            }

            if ( exceptionMap.size() > 0 )
            {
                handleClusteredInstanceFailures("loadInstaller", 
                    exceptionMap,     
                    instances.size(), 
                    LocalStringKeys.JBI_ADMIN_FAILED_LOAD_NEW_INSTALLER_ON_INSTANCE);
            }
        }
        return installerMBeans;
    }
    
    
    /**
     * Invoke loadInstaller on the instance
     * and return the MBeans Object Name
     * @param serverName the name of the instance.
     * @param compName the name of the component whose installer is to be loaded.
     * @param force set to <code>true</code> for a forced uninstall.
     * @return the ObjectName of the installer MBean.
     */
    private ObjectName loadInstallerForInstance(String serverName, 
        String compName, boolean force) throws ManagementException
    {
        
        ObjectName instObjName = null;
        if ( isInstanceRunning(serverName) )
        {
            String[] sign   = new String[]{"java.lang.String", "boolean"};
            Object[] params = new Object[2];
            params[0] = compName;
            params[1] = Boolean.valueOf(force);

            instObjName = (ObjectName) invokeRemoteOperation(
                getRemoteInstallationServiceObjectName(serverName),  
                    "loadInstaller", params, sign, serverName);     
        }
        return instObjName;
    }
    
    
    /**
     * Invoke unloadInstaller on the instance.
     */
    private boolean unloadInstallerFromInstance(String serverName, 
        String componentName, boolean isToBeDeletedFromInstance)
        throws ManagementException
    {
        boolean result = false;
        if ( isInstanceRunning(serverName) )
        {
            String[] sign   = new String[]{"java.lang.String", "boolean"};
            Object[] params = new Object[]{componentName, new Boolean(isToBeDeletedFromInstance)};

            Boolean unloaded = (Boolean) invokeRemoteOperation(
                getRemoteInstallationServiceObjectName(serverName),  
                "unloadInstaller", params, sign, serverName);
            result = unloaded.booleanValue();
        }
        else
        {
            result = true;
        }
        return result;
    }
    
    
    /**
     * Invoke unloadInstaller on each instance in the cluster if the Installer is unloaded
     * from even a single instance, the operation is a success.
     *
     * Partial Success : If unloadInstaller on any instance in the cluster fails, 
     * a warning is logged for it, and the installation continues for the 
     * remaining instances.  A value of null is added to the HashMap for the object name.
     * </br>
     * Complete failure : All the runtime exceptions thrown by the remote instance
     * are composed into a management task message and a management exception is 
     * thrown with the composite message.
     */
    private boolean unloadInstallerFromCluster(String clusterName, String componentName,
        boolean isToBeDeletedFromInstance)
        throws ManagementException
    {
        boolean unloaded = false;
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap();
            for ( String instance : instances )
            {
                boolean currResult = false;
                try
                {
                    currResult = unloadInstallerFromInstance(instance, 
                        componentName, isToBeDeletedFromInstance);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);
                    currResult = false;
                    continue;
                }
                unloaded = ( currResult | unloaded );                
            }

            if ( exceptionMap.size() > 0 )
            {
                handleClusteredInstanceFailures("unloadInstaller", 
                    exceptionMap,     
                    instances.size(), 
                    LocalStringKeys.JBI_ADMIN_FAILED_UNLOAD_INSTALLER_FROM_INSTANCE);
            }
        }
        else
        {
            // -- no target instances - operation should be a success
            unloaded = true;
        }
        return unloaded;
    }
    
    /**
     * @throws a ManagementException if there are active dependent components 
     * for the Shared Library on the target.
     */
    private void dependentComponentCheck(String targetName, String slName)
        throws ManagementException
    {
        try
        {
            ComponentQuery compQuery = getRegistry().getComponentQuery(targetName);
            List<String> depComps = compQuery.getDependentComponentIds(slName);

            List<String> instances = getTargetInstances(targetName);
            
            for ( String component : depComps )
            {
                for ( String instanceName : instances )
                {
                    // -- Get the component state on each instance in the target if active
                    // then throw MgmtExcep - indicating shared library cannot be uninstalled
                    if ( com.sun.jbi.ComponentState.STARTED == getComponentState(component, instanceName) )
                    {
                        String[] params = new String[]{component, "STARTED", instanceName, slName};
                        String errMsg = mTranslator.getString(
                            LocalStringKeys.JBI_ADMIN_DEPENDENT_COMPONENT_ACTIVE,
                            params);
                        String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(
                            "dependentComponentCheck", 
                            MessageBuilder.TaskResult.FAILED, 
                            MessageBuilder.MessageType.ERROR,
                            mMsgBuilder.getMessageString(errMsg),
                            params,
                            mMsgBuilder.getMessageToken(errMsg));
                        throw new ManagementException(jbiTaskMsg);
                    }
                    else if ( com.sun.jbi.ComponentState.STOPPED == getComponentState(component, instanceName) )
                    {
                        String[] params = new String[]{component, "STOPPED", instanceName, slName};
                        String errMsg = mTranslator.getString(
                            LocalStringKeys.JBI_ADMIN_DEPENDENT_COMPONENT_ACTIVE,
                            params);
                        String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(
                            "dependentComponentCheck", 
                            MessageBuilder.TaskResult.FAILED, 
                            MessageBuilder.MessageType.ERROR,
                            mMsgBuilder.getMessageString(errMsg),
                            params,
                            mMsgBuilder.getMessageToken(errMsg));
                        throw new ManagementException(jbiTaskMsg);
                    }
                }
                
            }
        }
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage("uninstalSharedLibrary", rex);
            throw new ManagementException(errMsg);
        }  
    } 
    
    /**
     * Update the DAS registry : add shared library info to the target entry.
     *
     * The DAS jbi-registry is updated when a action is partial/full success or when 
     * the instance is not up.
     *
     * @param slName - the shared library name
     * @param target - target name
     */
    private void  addSharedLibraryToTarget(String slName)
        throws ManagementException
    {
        try
        {
            if ( !mTarget.equals(mPlatform.getAdminServerName()) )
            {
                ComponentQuery compQuery = getRegistry().getComponentQuery("domain");
                ComponentInfoImpl slInfo = (ComponentInfoImpl) 
                    compQuery.getSharedLibraryInfo(slName);

                // SYSTEM_COMP_TODO :
                slInfo.setInstallRoot(getSharedLibraryInstallRoot(slName));
                getUpdater().addSharedLibrary(mTarget, slInfo);
            }
        }
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage("addSharedLibraryToTarget", rex);
            throw new ManagementException(errMsg);
        }  
    }
    
    /**
     * Update the DAS registry : remove a shared library from the target
     *
     * The DAS jbi-registry is updated when a action is partial/full success or when 
     * the instance is not up.
     *
     * @param slName - the shared library name
     * @param target - target name
     */
    private void  removeSharedLibraryFromTarget(String slName)
        throws ManagementException
    {
        try
        {
            if ( !mTarget.equals(mPlatform.getAdminServerName()) )
            {
                getUpdater().removeSharedLibrary(mTarget, slName);
            }
        }
        catch (RegistryException rex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage(
                "removeSharedLibraryFromTarget", rex);
            throw new ManagementException(errMsg);
        }
    }
    
    /**
     * Get the instances in a target 
     */
    private List<String> getTargetInstances(String targetName)
    {
        List<String> instances = new java.util.ArrayList<String>();
        if ( mPlatform.isStandaloneServer(targetName) )
        {
            instances.add(targetName);
        }
        else if ( mPlatform.isCluster(targetName) )
        {
            instances.addAll(mPlatform.getServersInCluster(targetName));
        }
        return instances;
    }
    
    
    /**
     * @return the SharedLibraryInstallRoot for a non-system library
     */
    private String getSharedLibraryInstallRoot(String slName)
    throws RegistryException, ManagementException
    {
        Archive archive = 
            getRegistry().getRepository().getArchive(
                ArchiveType.SHARED_LIBRARY, 
                slName);
        String archivePath = archive.getPath();
        
        return new java.io.File(archivePath).getParent();
    }
    
    /**
     * @throws ManagementException if one or more shared libraries from the list is not 
     * installed on the target
     */
    private void dependentSharedLibraryInstallationCheck(String componentName,
        List<String> requiredSharedLibs)
        throws ManagementException
    {
        if ( !requiredSharedLibs.isEmpty() )
        {
            List<String> uninstalledLibs = new java.util.ArrayList();
            
            for ( String slName : requiredSharedLibs )
            {
                if (!isSharedLibraryInstalled(slName) )
                {
                    uninstalledLibs.add(slName);
                }
            }
            
            if ( !uninstalledLibs.isEmpty() )
            {
                String[] params = {componentName, listToString(uninstalledLibs)};
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_REQUIRED_SHARED_LIBRARIES_NOT_INSTALLLED,
                    params);
                
                String jbiMsg = mMsgBuilder.buildFrameworkMessage(
                            "dependentSharedLibraryInstallationCheck", 
                            MessageBuilder.TaskResult.FAILED, 
                            MessageBuilder.MessageType.ERROR,
                            mMsgBuilder.getMessageString(errMsg),
                            params,
                            mMsgBuilder.getMessageToken(errMsg));
                
                throw new ManagementException(jbiMsg);
            }
        }
    }
    
    /**
     * @throws a ManagementException if the component is installed on non-domain targets
     */
    private void canUninstallComponentFromDomainCheck(String componentName)
        throws ManagementException
    {
        try
        {
            if ( getGenericQuery().isComponentInstalled(componentName) )
            {
                String[] params = new String[]{componentName};
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_COMPONENT_INSTALLED_ON_TARGETS,
                    params);
                String jbiTaskMsg = mMsgBuilder.buildFrameworkMessage(
                    "unloadInstaller", 
                    MessageBuilder.TaskResult.FAILED, 
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params,
                    mMsgBuilder.getMessageToken(errMsg));
                throw new ManagementException(jbiTaskMsg);
            }
        }
        catch (RegistryException rex)
        {
            throw new ManagementException( mMsgBuilder.buildExceptionMessage("canUninstallComponentFromDomainCheck",
                rex));
        }
    }
    
    
    /**
     * This method is used to validate component registration
     * @param componentName the componentName
     * @throws ManagementException if the component is not registered
     */
    private void validateComponentRegistration(String componentName)
    throws ManagementException
    {
        try 
        {
            if (!getGenericQuery().isComponentRegistered(componentName))
            {
                throw new ManagementException(
                    mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_UPGRADE_COMPONENT_NOT_INSTALLED, 
                        componentName));
            }
            mLog.finer("Component " +  componentName + " is registered in the system");
        } catch (RegistryException regEx)
        {
            throw new ManagementException(regEx.getMessage());
        }
    }
    
    /**
     * This method is used to validate if a given component is in Shutdown state
     * in all targets in which it is installed
     * @param componentName the component name
     * @throws ManagementException if the component is not Shutdown in all targets
     */
    private void validateComponentStateInAllTargets(String componentName)
    throws ManagementException
    {
        List<String> targets = getAllInstancesInstallingComponent(componentName);
        for( String target : targets )
        {
            if (!(getComponentState(componentName, target) == ComponentState.SHUTDOWN))
            {
                throw new ManagementException(
                    mTranslator.getString(                            
                    LocalStringKeys.JBI_ADMIN_UPGRADE_COMPONENT_NOT_SHUTDOWN, 
                    componentName, 
                    target));
            }
        }
        mLog.finer("Component " + componentName + " is Shutdown in all targets");
    }
    
    /**
     * This method is used to upgrade a component in given instances 
     * This method invokes upgradeComponent in each of the instances and 
     * composes the composite return message
     * @param componentName the component name
     * @param installZipURL the The URL of the component archive
     * @return String a status management message that contains upgrade result
     * @throws ManagementException if there is a problem with upgrade 
     */   
    private MessageBuilder.Message upgradeComponent(
            String componentName,  
            String installZipURL,
            List<String> instances)
    throws JBIException
    {

        HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
        HashMap<String, String> responseMap = new HashMap<String, String>();
        
        for( String instance : instances )
        {
            try
            {
                String instanceResult = 
                        upgradeComponent(componentName, installZipURL, instance);
                responseMap.put(instance, instanceResult);
            }
            catch(JBIException jbiEx)
            {
                exceptionMap.put(instance, jbiEx);
            }
        }
            
        return 
                new MessageBuilder(mTranslator).buildCompositeMessage(
                    UPGRADE_COMPONENT,
                    responseMap, 
                    exceptionMap, 
                    false);


    }    

    /**
     * This method is used to upgrade a component in an instance
     * This method invokes upgradeComponent on the instance installation service and
     * returns the response  
     * @param componentName the component name
     * @param installZipURL the The URL of the component archive
     * @param instanceName the instance name
     * @return String a status management message that contains upgrade result
     * @throws ManagementException if there is a problem with upgrade 
     */   
    private String upgradeComponent(
            String componentName,  
            String installZipURL,
            String instanceName)
    throws ManagementException
    {
            
        String updateResult ;
        if ( isInstanceRunning(instanceName) )
        {
            String componentInstallZipPath;
            mLog.fine("Uploading " + installZipURL  + " to target " + instanceName);   
            if ( !instanceName.equals(mPlatform.getAdminServerName()) )
            {
                componentInstallZipPath = uploadFile(instanceName, installZipURL);
            }
            else
            {
                componentInstallZipPath = toFileURL(installZipURL);
            }
            String[] sign   = new String[]{"java.lang.String", "java.lang.String"};
            String[] params = new String[]{componentName, componentInstallZipPath};
              
            updateResult =
                   (String) invokeRemoteOperation(
                        getRemoteInstallationServiceObjectName(instanceName),  
                                "upgradeComponent", 
                                params, 
                                sign, 
                                instanceName);
           componentInstallZipPath = null; 
        }
        else 
        {
            mLog.fine(instanceName + " is down. Component upgrade will be performed by synchronization.");   
            updateResult = mMsgBuilder.buildFrameworkMessage(
                    "upgradeComponent",
                    MessageBuilder.TaskResult.SUCCESS);            
        }
        return updateResult;
    }      
    
    /**
     * This method is used to verify if the component name in the archive
     * provided for upgrade is same as the component selected for upgrade
     * @param componentName the component to be upgraded
     * @param installZipURL the path to the archive supplied for upgrade component
     * @throws ManagementException if the names do not match
     */
    private void validateComponentName(
            String componentName, 
            String installZipURL)
    throws ManagementException
            
    {
        String PATH_TO_JBI_XML = METAINF + SLASH + JBIXML;
       
       try 
       {
            //do not use JarFile/JarEntry to read the descriptor as it keep some references 
            //to the archive and will eventually fail 
           
            //get hold of the current descriptor 
            String existingDescPath = 
                    getComponentInstallRoot(componentName) + File.separator
                    +  METAINF  + File.separator + JBIXML;
            File existingDesc = new File(existingDescPath);
            
            //extract the given archive in a temporary place
            String temporaryDirPath =
                    mEnvCtx.getJbiInstanceRoot() + File.separator + 
                    TMP_DIR + File.separator +  
                    componentName + "_validate" + FileHelper.getTimestamp();
            
            File temporaryDir = new File(temporaryDirPath);
            temporaryDir.mkdir();
            JarFactory jarHelper = new JarFactory(temporaryDir.getAbsolutePath());
            jarHelper.unJar(new File(installZipURL));
            
            //get a pointer to the descriptor in the temporary dir
            String newDescPath =
                    temporaryDir + File.separator 
                    + METAINF + File.separator + JBIXML;
            File newDesc = new File(newDescPath);
       
            boolean namesMatch = 
               DOMUtil.areElementsEqual(
                            existingDesc,
                            newDesc,
                            "/jbi:jbi/jbi:component/jbi:identification/jbi:name");
            if (!namesMatch)
            {
                throw new ManagementException(
                        mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_UPGRADE_COMPONENT_NAME_NOT_SAME));
            }
            mLog.finer("Component names are the same");
            FileHelper.cleanDirectory(temporaryDir);
            temporaryDir.delete();
        }
        catch (java.io.IOException ioEx)
        {
            mLog.fine(MessageHelper.getMsgString(ioEx));
            throw new ManagementException(ioEx.getMessage());
        }
    }
            
    /** 
     * This method is used to get a list of all instances in which the given 
     * component is installed
     * @param componentName the component name
     * @return List<String> of instances installing a component including 
     * instances that are part of a cluster
     * @throws ManagementException if the list of instances could not be obtained 
     */
    List<String> getAllInstancesInstallingComponent(String componentName)
    throws ManagementException
    {
        List<String> instances;
        try
        {
            instances = getGenericQuery().getServersInstallingComponent(componentName);
            List<String> clusters = getGenericQuery().getClustersInstallingComponent(componentName);
            if (!clusters.isEmpty())
            {
                for (String cluster : clusters)
                {
                    instances.addAll(mPlatform.getServersInCluster(cluster));
                }
            }
        } 
        catch (RegistryException regEx)
        {
            throw new ManagementException(regEx.toString());
        }
        return instances;                
    }    
    
    /**
     * This method is used to update the install root of a component in 
     * the domain repository. This method takes a backup of the component install
     * root and then adds the new archive to the repository. After that the 
     * component workspace from the backup dir is copied over to the new 
     * workspace
     * @param installRootBkup the dir where the install root should be saved
     * @param componentName the component name
     * @param installZipURL path to the upgrade archive
     * @throws ManagementException if the install root could not be updated
     */
    private void updateInstallRootInDomainRepository(
            String installRootBkup, 
            String componentName, 
            String installZipURL)
    throws ManagementException
    {
        try
        {
            String existingArchiveName = 
                    getRegistry().getGenericQuery().getComponentFileName(componentName);
            
            String installRoot = getComponentInstallRoot(componentName);      
            //we need to save the archive along with the install_root dir
            File installRootDir = new File(installRoot);
            String componentRoot = installRootDir.getParent();            

            FileHelper.copy(componentRoot, installRootBkup); 
            mLog.finer("Install root has been saved in " + installRootBkup);
            installRootDir = null;
            
            
            getRegistry().getRepository().removeArchive(
                    ArchiveType.COMPONENT, 
                    componentName);

            installRootDir = new File(installRoot);
            if (installRootDir.exists())
            {

                String message = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_INSTALL_ROOT_NOT_UPDATED, 
                        componentName);
                throw new ManagementException(message);
            }       
            Archive upgradeArchive = 
                    getRegistry().getRepository().addArchive(ArchiveType.COMPONENT, installZipURL);
            mLog.finer("Successfully added new archive in repository" );   
           
            /*
             * backupDir is <instanceRoot>/tmp/sun-http-binding_backup
             * installRoot is <instanceRoot>/components/sun-http-binding/install_root
             * we need to copy backupDir/intall_root/workspace to
             * components/sun-http-binding/install_root/workspace 
             */            
            String backupWorkspace = 
                    installRootBkup + File.separator + 
                    INSTALL_ROOT + File.separator +
                    WORKSPACE;
                    
            FileHelper.copy(
                backupWorkspace, 
                installRoot + File.separator + WORKSPACE);
            mLog.finer("Successfully recreated component workspace" );            
            
            String backupConfig =
                   installRootBkup + File.separator +
                   LOGGER_CONFIG;
            String config =
                    installRoot + File.separator +
                    LOGGER_CONFIG;
            boolean result = FileHelper.copy(backupConfig, config);
            if (result)
            {
                mLog.finer("Successfully restored logger settings" );                            
            }
            else
            {
                if (new File(backupConfig).exists())
                {
                    mLog.fine(mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_COMPONENT_UPGRADE_LOGGER_SETTINGS_NOT_RESTORED, 
                        componentName));
                }
            }
 
            //if archive names are different modify registry
            modifyArchiveNameInRegistry(
                    componentName,
                    existingArchiveName,
                    upgradeArchive.getFileName());
            
            //the descriptors will be different, remove old one from cache 
            ((com.sun.jbi.management.registry.xml.GenericQueryImpl)
            getRegistry().getGenericQuery()).removeComponentFromCache(componentName);            
            
        }
        catch (RepositoryException repEx)
        {
            throw new ManagementException(
                    mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_INSTALL_ROOT_NOT_UPDATED, 
                        componentName) +
                        repEx.getMessage());
        }
        catch (RegistryException regEx)
        {
            throw new ManagementException(
                    mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_INSTALL_ROOT_NOT_UPDATED, 
                        componentName) +
                        regEx.getMessage());
        }        
        catch (IOException ioEx)
        {
            throw new ManagementException(
                    mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_INSTALL_ROOT_NOT_UPDATED, 
                        componentName) +
                        ioEx.getMessage());            
        }
        
    }
    
    
    /**
     * This method is used to restore the component install root in the domain
     * repository by copying it from the backup dir
     * @param componentName the component name
     * @param backupComponentRoot the backup dir
     * @param componentRoot the install root
     */
    private void restoreInstallRootInDomainRepository(
            String componentName,
            String backupComponentRoot, 
            String componentRoot)
    {
        try
        {
            //restore could be called before the new archive is added
            //or after the archive is added if there are issues in framework
            //update. check the repository before removing the new archive
            if(getRegistry().getRepository().archiveExists(
                     ArchiveType.COMPONENT,
                     componentName))
            {            
                getRegistry().getRepository().removeArchive(
                    ArchiveType.COMPONENT, 
                    componentName);            
            }
            //remove the marker
            File marker = new File(componentRoot, DELETE_ME);
            if (marker != null && marker.exists())
            {
                marker.delete();
            }
            
            /*
             * backupDir is <instanceRoot>/tmp/sun-http-binding_backup
             * componentRoot is <instanceRoot>/components/sun-http-binding
             * we need to copy backupDir to <instanceRoot>/components
             */            
            FileHelper.copy(
                    backupComponentRoot, 
                    componentRoot);
            
            mLog.fine("install root has been restored");
        }
        catch (Exception ex)
        {
            mLog.fine(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_INSTALL_ROOT_NOT_RESTORED) 
                    + ex.getMessage());
        }              
    }
    
    /**
     * This method is used to increment the upgrade number of a component by one
     * @param componentName the component name
     * @throws RegistryException if the upgrade number could not be incremented
     */
    private void incrementComponentUpgradeNumber(String componentName)
    throws RegistryException
    {
        BigInteger currNumber = 
                getRegistry().getGenericQuery().getComponentUpgradeNumber(componentName);
        getRegistry().getUpdater().setComponentUpgradeNumber(
                componentName,
                currNumber.add(BigInteger.ONE));
    }    
    
    /**
     * This method is used to return the install root of a given component
     * @param componentName the component name
     * @return String the install root of the component
     */
    private String getComponentInstallRoot(String componentName)
    {        
        Archive archive = null;
        String archivePath = null;
        String installRoot = null;
         
        archive =  
                getRegistry().getRepository().getArchive(
                    ArchiveType.COMPONENT, 
                    componentName);
        
        if ( archive != null )
        {
            archivePath = archive.getPath();
            installRoot = new File(archivePath).getParent() +
                          File.separator + INSTALL_ROOT;
        }

        mLog.finer("Install Root for component " + componentName + " is " + installRoot);
        return installRoot;
    }
    
    /**
     * This method is used to modify the name of the component archive in 
     * registry. This is needed to address the cases where the name of the
     * archive given for upgrade is different from the current
     * one.
     * @param componentName the component name
     * @param exisitingName the archive name that is in registry
     * @param newName the new archive name given for upgrade
     * @param installZipURL the URL for achive provided for upgrade
     */
    private void modifyArchiveNameInRegistry(
            String componentName,
            String existingName,
            String newName)
    throws RegistryException, ManagementException
    {
        if (!existingName.equals(newName))
        {
            getUpdater().setComponentFileName(componentName, newName);
        }
    }
           
}



/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.management.ComponentInfo.Variable;
import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.message.MessageHelper;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.xml.RegistryDocument;
import com.sun.jbi.management.system.ManagementException;
import com.sun.jbi.util.ComponentConfigurationHelper;
import com.sun.jbi.util.Constants;

import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.lang.reflect.Method;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;


/**
 * The ComponentConfiguration is a dynamic facade MBean for component configuration.
 * This MBean provides a facade for managing component static, variable and
 * named configuration.
 *
 * This ComponentExtension MBean is registered for each installation of the component
 * on a target i.e it is target specific. 
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentConfiguration
    extends Facade
    implements com.sun.jbi.management.ComponentConfiguration, Constants
{
   /** The component name */
    protected String mComponentName;

    /** Component Type : BINDING / ENGINE ? */
    protected ComponentType mComponentType;
    
    /** Component Configuration Helper */
    protected ComponentConfigurationHelper mCfgHlpr;
    
    /** Configuration meta-data */
    protected Document mConfigDoc;
        
   /** The component configuration response for a target has two pieces of information
    *  <ul>
    *    <li> The attributes which were set successfully </li>
    *    <li> A management XML message with details </li>
    * </ul>
    */
    protected class ComponentConfigurationResponse
    {
        /** The message */
        private String mJbiMsg;
        
        /** The attribute list */
        private AttributeList mAttributes;
        
        public ComponentConfigurationResponse(String msg, AttributeList attribList)
        {
            mJbiMsg = msg;
            mAttributes = attribList;
        }
        
        /**
         * @return the message 
         */
        public String getResponseMessage()
        {
            return mJbiMsg;
        }
        
        /**
         * @return the attributes
         */
        public AttributeList getAttributes()
        {
            return mAttributes;
        }
    }
    
    public ComponentConfiguration(com.sun.jbi.EnvironmentContext ctx, String target, 
        String compName, ComponentType compType) 
        throws ManagementException
    {
        super(ctx, target);
        
        mComponentName = compName;
        mComponentType = compType;
        mCfgHlpr = new ComponentConfigurationHelper();
        initPrimitiveTypeMap();
        mConfigDoc = null;
    }
       
    /*----------------------------------------------------------------------------------*\
     *                   javax.management.DynamicMBean Methods                          *
    \*----------------------------------------------------------------------------------*/

    /**
     * Obtain the value of a specific attribute of the Dynamic MBean.
     *
     * @param attribute The name of the attribute to be retrieved
     * @return The value of the attribute retrieved.
     * @exception AttributeNotFoundException
     * @exception MBeanException  Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's getter.
     * @exception ReflectionException  Wraps a <CODE>java.lang.Exception</CODE>
     *            thrown while trying to invoke the getter.
     * @see #setAttribute
     */
    public Object getAttribute(String attribute) 
        throws AttributeNotFoundException, MBeanException, ReflectionException 
    {
        try
        {
            
            if ( "ApplicationVariables".equals(attribute) ) 
            {
                return getApplicationVariables();
            }
            
            if ( "ApplicationConfigurations".equals(attribute) ) 
            {
                return getApplicationConfigurations();
            }
        
            attributeCheck(attribute);
            
            targetUpCheck();
            componentStartedOrStoppedOnTargetCheck();

     
            AttributeList attribList = getAttributes(new String[]{attribute});
        
            if ( !attribList.isEmpty() )
            {
                return ( (Attribute) attribList.get(0)).getValue();
            }
            else
            {
                // should never be the case
                return null;
            }
        }
        catch ( javax.jbi.JBIException jbiex )
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
    }

    /**
     * Set the value of a specific attribute of the Dynamic MBean.
     * 
     * @param attribute The identification of the attribute to
     *        be set and  the value it is to be set to.
     * @exception AttributeNotFoundException
     * @exception InvalidAttributeValueException
     * @exception MBeanException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's setter.
     * @exception ReflectionException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown while trying to invoke the MBean's setter.
     * @see #getAttribute
     */
    public void setAttribute(Attribute attribute) 
        throws AttributeNotFoundException, InvalidAttributeValueException, 
            MBeanException, ReflectionException 
    {   
        try
        {
            setConfigurationAttribute(attribute);
        }
        catch ( javax.jbi.JBIException jex )
        {
            mLog.fine(MessageHelper.getMsgString(jex));
            throw new MBeanException(jex);
        }
    }

    /**
     * Allows an action to be invoked on the Dynamic MBean.
     * 
     * @param actionName The name of the action to be invoked.
     * @param params An array containing the parameters to be set when the 
     *        action is invoked.
     * @param signature An array containing the signature of the action. The 
     *        class objects will be loaded through the same class loader as the
     *        one used for loading the MBean on which the action is invoked.
     * @return The object returned by the action, which represents the result of
     *         invoking the action on the MBean specified.
     * @exception MBeanException  Wraps a <CODE>java.lang.Exception</CODE> thrown
     *            by the MBean's invoked method.
     * @exception ReflectionException  Wraps a <CODE>java.lang.Exception</CODE> 
     *            thrown while trying to invoke the method
     */
    public Object invoke(String actionName, Object[] params, String[] signature) 
        throws MBeanException, ReflectionException 
    {
        try
        {
            if ( actionName.equals("setConfigurationAttribute"))
            {
                return setConfigurationAttribute( (Attribute) params[0]);
            }

            if ( actionName.equals("setConfigurationAttributes"))
            {
                return setConfigurationAttributes((AttributeList) params[0]);
            }
            
            if ( actionName.equals("addApplicationVariable") )
            {
                return addApplicationVariable((String) params[0],
                    (CompositeData) params[1]);
            }
            
            if ( actionName.equals("setApplicationVariable") )
            {
                return setApplicationVariable((String) params[0],
                    (CompositeData) params[1]);
            }
            
            if ( actionName.equals("deleteApplicationVariable") )
            {
                return deleteApplicationVariable((String) params[0]);
            }
            
            if ( actionName.equals("getApplicationVariables") )
            {
                return getApplicationVariables();
            }
            
            if ( actionName.equals("getApplicationConfigurations") )
            {
                return getApplicationConfigurations();
            }
            
            if ( actionName.equals("addApplicationConfiguration") )
            {
                if ( params[1] instanceof Properties )
                {
                    return addApplicationConfiguration((String) params[0],
                        (Properties) params[1]);
                }
                else
                {
                    return addApplicationConfiguration((String) params[0],
                        (CompositeData) params[1]);
                }
            }

            if ( actionName.equals("setApplicationConfiguration") )
            {
                if ( params[1] instanceof Properties )
                {
                    return setApplicationConfiguration((String) params[0],
                        (Properties) params[1]);
                }
                else
                {
                    return setApplicationConfiguration((String) params[0],
                        (CompositeData) params[1]);
                }
            }
            
            if ( actionName.equals("deleteApplicationConfiguration") )
            {
                return deleteApplicationConfiguration((String) params[0]);
            }
            
            if ( actionName.equals("queryApplicationConfigurationType") )
            {
                return queryApplicationConfigurationType();
            }
            
            if ( actionName.equals("retrieveConfigurationDisplaySchema"))
            {
                return retrieveConfigurationDisplaySchema();
            }
            
            if ( actionName.equals("retrieveConfigurationDisplayData"))
            {
                return retrieveConfigurationDisplayData();
            }
            
            if ( actionName.equals("isAppVarsSupported"))
            {
                return isAppVarsSupported();
            }
            
            if ( actionName.equals("isAppConfigSupported"))
            {
                return isAppConfigSupported();
            }   
            
            if ( actionName.equals("isComponentConfigSupported"))
            {
                return isComponentConfigSupported();
            }
            
            
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            throw new MBeanException(jbiex);
        }
        
        throw new UnsupportedOperationException();
    }

    /**
     * Sets the values of several attributes of the Dynamic MBean.
     * 
     * @param attributes A list of attributes: The identification of the
     *        attributes to be set and  the values they are to be set to.
     * @return The list of attributes that were set, with their new values.
     * @see #getAttributes
     */
    public AttributeList setAttributes(AttributeList attributes) 
    {
        try
        {
            setConfigurationAttributes(attributes);
            return attributes;
        }
        catch ( Exception ex )
        {
            mLog.fine(MessageHelper.getMsgString(ex));
            return new AttributeList();
        }
        
    }

    /**
     * Provides the exposed attributes and actions of the Dynamic MBean using 
     * an MBeanInfo object.
     * 
     * @return An instance of <CODE>MBeanInfo</CODE> allowing all attributes 
     *         and actions exposed by this Dynamic MBean to be retrieved.
     */
    public MBeanInfo getMBeanInfo() 
    {
        // Get the MbeanInfo from the actual component configuration MBean and
        // augment it with the facade configuration operations
        MBeanInfo mbeanInfo = getComponentMBeanInfo();
        MBeanInfo augInfo = mbeanInfo;
        
        try
        {
            Method[] methods = Class.forName("com.sun.jbi.management.ComponentConfiguration").getDeclaredMethods();
            MBeanOperationInfo[] opInfos = new MBeanOperationInfo[methods.length];
            int i = 0;
            for ( Method mtd : methods )
            {
                opInfos[i++] = new MBeanOperationInfo(mtd.getName(), mtd);
            }
            
            augInfo = new MBeanInfo(
                mbeanInfo.getClassName(),
                mbeanInfo.getDescription(),
                mbeanInfo.getAttributes(),
                mbeanInfo.getConstructors(),
                merge(mbeanInfo.getOperations(), opInfos),
                mbeanInfo.getNotifications());
        }
        catch (Exception ex)
        {
            mLog.fine(MessageHelper.getMsgString(ex));
        }        
        return augInfo;
    }

    /**
     * Get the values of several attributes of the Dynamic MBean.
     * 
     * @param attributes A list of the attributes to be retrieved.
     * @return The list of attributes retrieved.
     * @see #setAttributes
     */
    public AttributeList getAttributes(String[] attributes) 
    {
        AttributeList list = new AttributeList();
     
        try
        {
            attributeCheck(attributes);
            targetUpCheck();
            componentStartedOrStoppedOnTargetCheck();
        }
        catch ( Exception ex)
        {
            mLog.fine(MessageHelper.getMsgString(ex));
            return list;
        }
        
        
        /**
         * If the framework is ready ( i.e. JAXB initialized ) get the attribute from
         * the JAXB model
         *
         * If framework not ready then get the attribute from the registry using DOM/XPath
         *
         * If the attribute is not defined in the registry ( for target or global domain config )
         * the return the default value of the attribute, the domain setting has not been 
         * updated and is identical to the default
         */

        // -- Always get the component configuration from the live MBean,
        //    for all targets, even cluster. 
        
        /** Fix for Issue 780
        if ( mPlatform.isCluster(mTarget) )
        {
            Properties props = retrieveAttributes();

            if ( !props.isEmpty() )
            {
                list = new AttributeList(props.size());

                for (String key : attributes)
                {
                    MBeanAttributeInfo attribInfo = getAttributeInfo(key);
                    String strValue = props.getProperty(key);
                    if (strValue != null )
                    {
                        if ( attribInfo != null)
                        {
                            try
                            {
                                Object value = (Object) com.sun.jbi.management.util.StringHelper.
                                    convertStringToType( attribInfo.getType(), strValue );
                                list.add(new Attribute( key, value));
                            }
                            catch (Exception ex)
                            {
                                mLog.fine(MessageHelper.getMsgString(ex));
                                list.add( new Attribute(key, strValue));
                            }

                        }
                        else
                        {
                            list.add( new Attribute(key, strValue));
                        }
                    }
                }
                return list;
            }
        }**/
            
        // -- Call getAttributes on the instance MBean
        String instance = getRunningInstance();
        ObjectName configMBeanName = null;
        if ( instance != null )
        {
            try
            {
                configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);

                AttributeList attribs =  (AttributeList) getRemoteAttributes(
                    configMBeanName, attributes, instance);
           
                // persist all the attributes
                //Issue 780 Fix : persistMBeanAttributes(configMBeanName, instance);
                return attribs;
            }
            catch ( ManagementException mex)
            {
                // -- should never occur
                mLog.fine(MessageHelper.getMsgString(mex));
             
            }
        }

        // No attributes in registry and instance MBean unavailable
        return list;
    }
    
    /*---------------------------------------------------------------------------------*\
     *          Operations for Static Configuration Management                         *
    \*---------------------------------------------------------------------------------*/
    /**
     * Set a single configuration attribute on a target.
     *
     * @param attrib - the configuration attribute to be set.
     * @return a management message string with the status of the operation.
     */
    public String setConfigurationAttribute(Attribute attrib)
        throws javax.jbi.JBIException
    { 
        componentStartedOrStoppedOnTargetCheck();
        attributeCheck(attrib);
        
        // If attributes have never been persisted do that first
        /** Issue 780 fix: Properties props = retrieveAttributes();
        if ( props.isEmpty() )
        {
            String instance = getRunningInstance();
            if ( instance != null )
            {
                persistMBeanAttributes(
                    getRemoteInstanceComponentConfigMBeanName(instance), 
                    instance);
            }
        }*/
        
        String result = setConfigurationAttributeOnTarget(attrib);
        
        // Parse the result if success then persist the attribute changes
        MessageBuilder.Message msg = mMsgBuilder.createMessage(result);
        
        if ( msg.isSuccess())
        {
            persistAttribute(attrib);
        } 
        return result;
    }
    
    /**
     *
     */
    public String setConfigurationAttributes(AttributeList attribList)
            throws javax.jbi.JBIException
    {
        componentStartedOrStoppedOnTargetCheck();
        attributeCheck(attribList);
        // If attributes have never been persisted do that first
        /** Fix for Issue 780 : Properties props = retrieveAttributes();
        if ( props.isEmpty() )
        {
             String instance = getRunningInstance();
             if ( instance != null )
             {
                 persistMBeanAttributes(
                        getRemoteInstanceComponentConfigMBeanName(instance), 
                        instance);
             }
        }*/
        
        ComponentConfigurationResponse result = setConfigurationAttributesOnTarget(attribList);
        
        // Parse the result if success then persist the attribute changes
        MessageBuilder.Message msg = mMsgBuilder.createMessage(result.getResponseMessage());
        
        if ( msg.isSuccess())
        {
            // For target=server the instance persists the change
            if ( !mTarget.equals(SERVER) )
            {
                persistAttributes(result.getAttributes());
            }
        }
        return result.getResponseMessage();
    }
    
    /*---------------------------------------------------------------------------------*\
     *          Operations for Application Variables Management                        *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for application variables. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application variable management.
     *
     * @return true if the components configuration MBean implements all the
     *         operations for application variables.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppVarsSupported()  
        throws MBeanException         
     {
        Boolean isAppVarsSupported = false;
       
        String ns = getComponentConfigurationNS(mComponentName);
        
        if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
        {
            /**
             * If it's the new schema this information can be obtained from 
             * the components configuration display data.
             */  
            try
            {
            
                Document configDoc = getConfigDoc();

                isAppVarsSupported = mCfgHlpr.isAppVarsSupported(configDoc);
            }
            catch ( ManagementException ex)
            {
                throw new MBeanException(new javax.jbi.JBIException(ex));
            }

        }
        else
        {
    
            try
            {
                targetUpCheck();
                componentStartedOrStoppedOnTargetCheck();
            }
            catch ( javax.jbi.JBIException jbiex)
            {
                mLog.fine(MessageHelper.getMsgString(jbiex));
                throw new MBeanException(jbiex);
            }

            String instance = getRunningInstance();
            ObjectName configMBeanName = null;
            if ( instance != null )
            {
                configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);
                if ( configMBeanName != null )
                {
                    try
                    {
                       isAppVarsSupported =  (Boolean) invokeRemoteOperation(
                           configMBeanName, 
                           "isAppVarsSupported",
                           new Object[]{},
                           new String[]{},
                           instance);
                    }
                    catch ( ManagementException mex)
                    {
                        mLog.fine(MessageHelper.getMsgString(mex));
                        throw new RuntimeException(mex.getMessage());
                    }
                }
            }
        
        }        
        // If component not started - exception
       return isAppVarsSupported;
     }
     
    /**
     * This operation adds a new application variable. If a variable already exists with 
     * the same name as that specified then the operation fails.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste
     * @throws MBeanException if an error occurs in adding the application variables to the 
     *         component. 
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public String addApplicationVariable(String name, CompositeData appVar) 
        throws MBeanException
    {
        String result;
        mLog.fine("Adding application variable " + name);
        try
        {
            componentStartedOrStoppedOnTargetCheck();
            supportsAppVarsCheck();
            applicationVariableExistsCheck(name, false);
            applicationVariableCompositeIntegrityCheck(name, null, appVar, false);
            result = addApplicationVariableToTarget(name, appVar);

            // Parse the result if success then persist the attribute changes
            MessageBuilder.Message msg = mMsgBuilder.createMessage(result);

            if ( msg.isFailure())
            {
                // if failed, throw an exception with the failure msg
                throw new MBeanException(new javax.jbi.JBIException(result));
            }
            else
            {
                addApplicationVariable(appVar);
            } 
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
        return result;
    }
     
    /**
     * This operation sets an application variable. If a variable does not exist with 
     * the same name, its an error.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste to be updated.
     * @throws MBeanException if one or more application variables cannot be deleted
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public String setApplicationVariable(String name, CompositeData appVar) 
        throws MBeanException
    {
        String result;
        mLog.fine("Setting application variable " + name);
        try
        {
            componentStartedOrStoppedOnTargetCheck();
            supportsAppVarsCheck();
            String type = applicationVariableExistsCheck(name, true);
            applicationVariableCompositeIntegrityCheck(name, type, appVar, true);
            result = setApplicationVariableOnTarget(name, appVar);

            // Parse the result if success then persist the attribute changes
            MessageBuilder.Message msg = mMsgBuilder.createMessage(result);

            if ( msg.isFailure())
            {
                // if failed, throw an exception with the failure msg
                throw new MBeanException(new javax.jbi.JBIException(result));
            }
            else
            {
                updateApplicationVariable(appVar);
            } 
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
        return result;
    }
     
    /**
     * This operation deletes an application variable, if a variable with the specified name does
     * not exist, it's an error.
     *
     * @param name - name of the application variable
     * @throws MBeanException on errors.
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public String deleteApplicationVariable(String name) throws MBeanException
    {

        String result;
        mLog.fine("Deleting application variable " + name);
        try
        {
            clusterUpCheck();
            componentStartedOrStoppedOnTargetCheck();
            supportsAppVarsCheck();
            //applicationVariableExistsCheck(name, true);
            result = deleteApplicationVariableFromTarget(name);

            // Parse the result if success then persist the attribute changes
            MessageBuilder.Message msg = mMsgBuilder.createMessage(result);

            if ( msg.isFailure())
            {
                // if failed, throw an exception with the failure msg
                throw new MBeanException(new javax.jbi.JBIException(result));
            }
            else
            {
               removeApplicationVariable(name);
            } 
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
        return result;
    }
     
     /**
      * Get the Application Variable set for a component.
      *
      * @return  a TabularData which has all the applicationvariables set on the component. 
      */
     public TabularData getApplicationVariables()
     {
         TabularData appVars = null;
         
        /** Issue 780 Fix
         * If framework not ready then get the application variable from the registry 
         * using DOM.
         *
         * If the framework is ready ( i.e. JAXB initialized ) get the application variable 
         *  from the JAXB model
         *
         * If the registry component configuration / application variables is empty get the 
         * actual values from the MBean.
         *

         
        TabularData appVars = retrieveApplicationVariables();
        
        
        if ( appVars.isEmpty() )
        {   
            */
            // -- Call getAttributes on the actual MBean, the application variables might not
            //    have been persisted
            // -- Call getAttributes on the instance MBean
            String instance = getRunningInstance();
            ObjectName configMBeanName = null;
            if ( instance != null )
            {
                configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);

                if ( configMBeanName != null )
                {
                    try
                    {
                        appVars =  (TabularData) getRemoteAttribute(configMBeanName, "ApplicationVariables", instance);
                    }
                    catch ( ManagementException mex)
                    {
                        mLog.fine(MessageHelper.getMsgString(mex));
                        throw new RuntimeException(mex.getMessage());
                    }
                }
            }
            /** Issue 780 Fix  } */
            if ( appVars == null )
            {
                try
                {
                    appVars = createEmptyAppVarTable();
                }
                catch ( javax.management.openmbean.OpenDataException oex )
                {   
                    throw new RuntimeException(oex);
                }
            }
        return appVars;
     }

          /**
      * Get the Application Variable set for a component.
      *
      * @return  a TabularData which has all the applicationvariables set on the component. 
      */
     public TabularData getApplicationVariablesForInstance(String instance)
     {
         TabularData appVars = null;
         
            ObjectName configMBeanName = null;
            if ( instance != null )
            {
                configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);

                if ( configMBeanName != null )
                {
                    try
                    {
                        appVars =  (TabularData) getRemoteAttribute(configMBeanName, "ApplicationVariables", instance);
                    }
                    catch ( ManagementException mex)
                    {
                        mLog.fine(MessageHelper.getMsgString(mex));
                        throw new RuntimeException(mex.getMessage());
                    }
                }
            }
            /** Issue 780 Fix  } */
            if ( appVars == null )
            {
                try
                {
                    appVars = createEmptyAppVarTable();
                }
                catch ( javax.management.openmbean.OpenDataException oex )
                {   
                    throw new RuntimeException(oex);
                }
            }
        return appVars;
     }

    /**
     * Detect the components support for component configuration. This method
     * returns true if the component has a configuration MBean with
     * configuration attributes.
     *
     * @return true if the components configuration MBean has configuration
     *         attributes.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
    public boolean isComponentConfigSupported()  
        throws MBeanException
    {
        Boolean isComponentConfigSupported = false;
        
        String ns = getComponentConfigurationNS(mComponentName);
        
        if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
        {
            /**
             * If it's the new schema this information can be obtained from 
             * the components configuration display data.
             */  
            try
            {
                Document configDoc = getConfigDoc();
                isComponentConfigSupported = mCfgHlpr.isComponentConfigSupported(configDoc);
            }
            catch ( ManagementException ex)
            {
                throw new MBeanException(new javax.jbi.JBIException(ex));
            }

        }
        else
        {
            try
            {
                targetUpCheck();
                componentStartedOrStoppedOnTargetCheck();
            }
            catch ( javax.jbi.JBIException jbiex)
            {
                mLog.fine(MessageHelper.getMsgString(jbiex));
                throw new MBeanException(jbiex);
            }

            String instance = getRunningInstance();
            ObjectName configMBeanName = null;
            if ( instance != null )
            {
                configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);
                if ( configMBeanName != null )
                {
                    try
                    {
                       isComponentConfigSupported =  (Boolean) invokeRemoteOperation(
                           configMBeanName, 
                           "isComponentConfigSupported",
                           new Object[]{},
                           new String[]{},
                           instance);
                    }
                    catch ( ManagementException mex)
                    {
                        mLog.fine(MessageHelper.getMsgString(mex));
                        throw new RuntimeException(mex.getMessage());
                    }
                }
            }
        }

       return isComponentConfigSupported;         
    }
    
    /*---------------------------------------------------------------------------------*\
     *            Operations for Application Configuration Management                  *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for application configuration. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application configuration management.
     *
     * @return true if the components configuration MBean implements all the
     *         operations for application configuration.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
    public boolean isAppConfigSupported()  
        throws MBeanException
    {
        Boolean isAppConfigSupported = false;
        
        String ns = getComponentConfigurationNS(mComponentName);
        
        if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
        {
            /**
             * If it's the new schema this information can be obtained from 
             * the components configuration display data.
             */  
            try
            {
                Document configDoc = getConfigDoc();
                isAppConfigSupported = mCfgHlpr.isAppConfigSupported(configDoc);
            }
            catch ( ManagementException ex)
            {
                throw new MBeanException(new javax.jbi.JBIException(ex));
            }

        }
        else
        {
            try
            {
                targetUpCheck();
                componentStartedOrStoppedOnTargetCheck();
            }
            catch ( javax.jbi.JBIException jbiex)
            {
                mLog.fine(MessageHelper.getMsgString(jbiex));
                throw new MBeanException(jbiex);
            }

            String instance = getRunningInstance();
            ObjectName configMBeanName = null;
            if ( instance != null )
            {
                configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);
                if ( configMBeanName != null )
                {
                    try
                    {
                       isAppConfigSupported =  (Boolean) invokeRemoteOperation(
                           configMBeanName, 
                           "isAppConfigSupported",
                           new Object[]{},
                           new String[]{},
                           instance);
                    }
                    catch ( ManagementException mex)
                    {
                        mLog.fine(MessageHelper.getMsgString(mex));
                        throw new RuntimeException(mex.getMessage());
                    }
                }
            }
        }

       return isAppConfigSupported;         
    }
     
     /**
      * Get the CompositeType definition for the components application configuration 
      *
      * @return the CompositeType for the components application configuration.
      *         If the target is not up a null value is returned.
      */
     public CompositeType queryApplicationConfigurationType()
     {

         CompositeType appConfigType = null;
         String instance = getRunningInstance();
         ObjectName configMBeanName = null;
         if ( instance != null )
         {
             configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);

             if ( configMBeanName != null )
             {
                 try
                 {
                    appConfigType =  (CompositeType) invokeRemoteOperation(
                        configMBeanName, 
                        "queryApplicationConfigurationType",
                        new Object[]{},
                        new String[]{},
                        instance);
                 }
                 catch ( ManagementException mex)
                 {
                     mLog.fine(MessageHelper.getMsgString(mex));
                     throw new RuntimeException(mex.getMessage());
                 }
             }
         }
         return appConfigType;
     }
     

     /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appCfgProps - application configuration properties
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String addApplicationConfiguration(String name, Properties appCfgProps) 
        throws MBeanException
     {
         mLog.fine(" adding application configuration " + appCfgProps.toString() +
            " to component " + mComponentName);
         
         String responseMsg = null;
         try
         {
             componentStartedOrStoppedOnTargetCheck();
             supportsAppConfigCheck();
             applicationConfigurationCompositeIntegrityCheck(name, appCfgProps, false);
             applicationConfigurationExistsCheck(name, false);
             if (isTargetUp(mTarget))
             {
                 CompositeType appCfgType = queryApplicationConfigurationType();

                 if ( appCfgType != null )
                 {
                    // Convert properties to Compoiste Data and invoke the MBean operation
                    CompositeData cd = mCfgHlpr.convertPropertiesToCompositeData(appCfgProps,
                        appCfgType);
                    responseMsg = addApplicationConfigurationComposite(name, cd);
                 }
                 else
                 {
                     String errMsg = mTranslator.getString(
                        LocalStringKeys.JBI_CCFG_APP_CFG_TYPE_NULL,
                        mComponentName);
                     throw new Exception(errMsg);
                 }
             }
             else
             {
                 // -- Cannot determine if the MBean supports application configuration
                 //    If target is standalone server throw exception - component offline
                 //    If target is cluster persist the changes
                 if ( mPlatform.isStandaloneServer(mTarget))
                 {
                    // Component is offline 
                    String[] params = new String[]{mComponentName, mTarget};
                    String errMsg = mTranslator.getString(
                        LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
                    responseMsg = mMsgBuilder.buildFrameworkMessage("addApplicationConfiguration",
                        com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                            MessageBuilder.MessageType.ERROR,
                            mMsgBuilder.getMessageString(errMsg),
                            params, mMsgBuilder.getMessageToken(errMsg));
                 }
                 else
                 {
                    addApplicationConfiguration(appCfgProps);
                    
                    responseMsg = mMsgBuilder.buildFrameworkMessage("addApplicationConfiguration",
                        com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
                 }
             }
         }
         catch ( Exception ex )
         {
             try
             {
                 String errMsg = mMsgBuilder.buildExceptionMessage("addApplicationConfiguration",
                    ex);
                 throw new MBeanException(new javax.jbi.JBIException(errMsg));
             }
             catch(ManagementException mex)
             {
                 mLog.fine(MessageHelper.getMsgString(mex));
             }
         }
         return responseMsg;
     }
     
     /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appConfig - application configuration composite 
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String addApplicationConfiguration(String name, CompositeData appConfig) 
        throws MBeanException
     {
        String result;
        try
        {
            componentStartedOrStoppedOnTargetCheck();
            supportsAppConfigCheck();
            applicationConfigurationCompositeIntegrityCheck(name, appConfig, false);
            applicationConfigurationExistsCheck(name, false);
            result = addApplicationConfigurationComposite(name, appConfig);
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            throw new MBeanException(jbiex);
        }
        return result;
     }
    
     /**
      * Delete an application configuration. 
      *
      * @param name - identification of the application configuration to be deleted
      * @throws MBeanException if the configuration cannot be deleted.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String deleteApplicationConfiguration(String name) throws MBeanException
     {
         
        String result;
        mLog.fine(" deleteing application configuration with name " + name);
        
        try
        {
            clusterUpCheck();
            componentStartedOrStoppedOnTargetCheck();
            supportsAppConfigCheck();
            //applicationConfigurationExistsCheck(name, true);
            result = deleteApplicationConfigurationFromTarget(name);

            // Parse the result if success then persist the attribute changes
            MessageBuilder.Message msg = mMsgBuilder.createMessage(result);

            if ( msg.isFailure())
            {
                // if failed, throw an exception with the failure msg
                throw new MBeanException(new javax.jbi.JBIException(result));
            }
            else
            {
                removeApplicationConfiguration(name);
            } 
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            throw new MBeanException(jbiex);
        }
        return result;
     }
 
     /**
      * Set an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appCfgProps - application configuration properties
      * @throws MBeanException if the application configuration cannot be set.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String setApplicationConfiguration(String name, Properties appCfgProps) 
        throws MBeanException
     {
         mLog.fine(" setting application configuration " + appCfgProps.toString() +
            " on component " + mComponentName);
         
         String responseMsg = null;
         try
         {
             componentStartedOrStoppedOnTargetCheck();
             supportsAppConfigCheck();
             applicationConfigurationCompositeIntegrityCheck(name, appCfgProps, true);
             applicationConfigurationExistsCheck(name, true);
             if (isTargetUp(mTarget))
             {
                 CompositeType appCfgType = queryApplicationConfigurationType();

                 if ( appCfgType != null )
                 {
                    // Convert properties to Compoiste Data and invoke the MBean operation
                    CompositeData cd = mCfgHlpr.convertPropertiesToCompositeData(appCfgProps,
                        appCfgType);
                    responseMsg = setApplicationConfigurationComposite(name, cd);
                 }
                 else
                 {
                    String errMsg = mTranslator.getString(
                        LocalStringKeys.JBI_CCFG_APP_CFG_TYPE_NULL,
                        mComponentName);
                     throw new Exception(errMsg);
                 }
             }
             else
             {
                 // -- Don't know if the MBean supports application configuration
                 //    If target is standalone server throw exception - component offline
                 //    If target is cluster persist the changes
                 if ( mPlatform.isStandaloneServer(mTarget))
                 {
                    // Component is offline 
                    String[] params = new String[]{mComponentName, mTarget};
                    String errMsg = mTranslator.getString(
                        LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
                    responseMsg = mMsgBuilder.buildFrameworkMessage("setApplicationConfiguration",
                        com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                            MessageBuilder.MessageType.ERROR,
                            mMsgBuilder.getMessageString(errMsg),
                            params, mMsgBuilder.getMessageToken(errMsg));
                 }
                 else
                 {
                    updateApplicationConfiguration(appCfgProps);
                    
                    responseMsg = mMsgBuilder.buildFrameworkMessage("updateApplicationConfiguration",
                        com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
                 }
             }
         }
         catch ( Exception ex )
         {
             try
             {
                 String errMsg = mMsgBuilder.buildExceptionMessage("updateApplicationConfiguration",
                    ex);
                 throw new MBeanException(new javax.jbi.JBIException(errMsg));
             }
             catch(ManagementException mex)
             {
                 mLog.fine(MessageHelper.getMsgString(mex));
             }
         }
         return responseMsg;
     }
 
     
     /**
      * Update a application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "configurationName" in the appConfig
      * @param appConfig - application configuration composite
      * @throws MBeanException if there are errors encountered when updating the configuration.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String setApplicationConfiguration(String name, CompositeData appConfig) 
        throws MBeanException
     {
        String result;
        try
        {
            componentStartedOrStoppedOnTargetCheck();
            supportsAppConfigCheck();
            applicationConfigurationCompositeIntegrityCheck(name, appConfig, true);
            applicationConfigurationExistsCheck(name, true);
            result = setApplicationConfigurationComposite(name, appConfig);
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            throw new MBeanException(jbiex);
        }
        return result;
     }
    
    /**
     * Get a Map of all application configurations for the component.
     *
     * @return a TabularData of all the application configurations for a 
     *         component keyed by the configuration name. 
     */
    public TabularData getApplicationConfigurations()
    {
        try
        {
            canListAppConfigCheck();
        }
        catch ( javax.jbi.JBIException jbiex )
        {
            throw new RuntimeException(jbiex.getMessage());
        }
        TabularData appConfigs = null;
        
        /** Issue 780 fix
         * If framework not ready then get the application variable from the registry 
         * using DOM.
         *
         * If the framework is ready ( i.e. JAXB initialized ) get the application variable 
         *  from the JAXB model
         *
         * If the registry component configuration / application variables is empty get the 
         * actual values from the MBean.
         *
        
        try
        {
            
            appConfigs = createEmptyAppConfigTable();
            appConfigs = retrieveApplicationConfiguration();
        }
        catch ( javax.management.openmbean.OpenDataException oex )
        {   
            throw new RuntimeException(oex);
        }
        
        if ( appConfigs.isEmpty() )
        {   */
            // -- Call getAttribute on the instance MBean
            String instance = getRunningInstance();
            ObjectName configMBeanName = null;
            if ( instance != null )
            {
                configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);

                if ( configMBeanName != null )
                {
                    try
                    {
                        appConfigs =  (TabularData) getRemoteAttribute(configMBeanName, 
                            "ApplicationConfigurations", instance);
                    }
                    catch ( ManagementException mex)
                    {
                        mLog.fine(MessageHelper.getMsgString(mex));
                        throw new RuntimeException(mex.getMessage());
                    }
                }
            }
            
            if ( appConfigs == null )
            {
                try
                {
                    appConfigs = createEmptyAppConfigTable();
                }
                catch ( javax.management.openmbean.OpenDataException oex )
                {   
                    throw new RuntimeException(oex);
                }
            }
        /** Issue 780 Fix } */
        return appConfigs;
    }
    
    /**
     * Get a Map of all application configurations for the component on a instance.
     * @param instance
     *         the instance to check
     * @return a TabularData of all the application configurations for a 
     *         component keyed by the configuration name. 
     */
    public TabularData getApplicationConfigurationsForInstance(String instance)
    {
        try
        {
            canListAppConfigCheck();
        }
        catch ( javax.jbi.JBIException jbiex )
        {
            throw new RuntimeException(jbiex.getMessage());
        }
        TabularData appConfigs = null;
        
             ObjectName configMBeanName = null;
            if ( instance != null )
            {
                configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);

                if ( configMBeanName != null )
                {
                    try
                    {
                        appConfigs =  (TabularData) getRemoteAttribute(configMBeanName, 
                            "ApplicationConfigurations", instance);
                    }
                    catch ( ManagementException mex)
                    {
                        mLog.fine(MessageHelper.getMsgString(mex));
                        throw new RuntimeException(mex.getMessage());
                    }
                }
            }
            
            if ( appConfigs == null )
            {
                try
                {
                    appConfigs = createEmptyAppConfigTable();
                }
                catch ( javax.management.openmbean.OpenDataException oex )
                {   
                    throw new RuntimeException(oex);
                }
            }
       return appConfigs;
    }
      
    /*---------------------------------------------------------------------------------*\
     *            Operations Component Configuration meta-data Management              *
    \*---------------------------------------------------------------------------------*/
    
    /** 
     * Retrieves the component specific configuration schema.
     *
     * @return a String containing the configuration schema.
     */
    public String retrieveConfigurationDisplaySchema()
        throws MBeanException
    {
        String response = null;
        
        try
        {        
            String ns = getComponentConfigurationNS(mComponentName);

            if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
            {
                // New configuration schema
                response = getComponentConfigurationSchema(); 

            }  
            else
            {

                componentStartedOrStoppedOnTargetCheck();
                String instance = getRunningInstance();
                ObjectName configMBeanName = null;
                if ( instance != null )
                {
                    configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);

                    if ( configMBeanName != null )
                    {
                        response =  (String) invokeRemoteOperation(
                            configMBeanName, 
                            "retrieveConfigurationDisplaySchema", 
                            new Object[]{}, 
                            new String[]{}, 
                            instance);
                    }
                }
            }
        }
        catch (javax.jbi.JBIException jbiex )
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
        catch (Exception ex)
        {
            try
            {
                String errMsg = mMsgBuilder.buildExceptionMessage(
                            "retrieveConfigurationDisplaySchema", ex);
                Exception jbiEx = new javax.jbi.JBIException(errMsg); 
                mLog.fine(MessageHelper.getMsgString(jbiEx));
                throw new MBeanException(jbiEx);
            }
            catch( Exception exp )
            {
                mLog.fine(MessageHelper.getMsgString(exp));
                throw new MBeanException(ex);
            }
        }
        return response;
    }
    
    /** 
     * Retrieves the component configuration metadata.
     * The XML data conforms to the component 
     * configuration schema.
     *
     * @return a String containing the configuration metadata.
     */
    public String retrieveConfigurationDisplayData()
        throws MBeanException
    {
        String response = null;
        
        String ns = getComponentConfigurationNS(mComponentName);
        
        if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
        {
            // New configuration schema
            response = getComponentConfigurationData(mComponentName); 
            
        }
        else
        {
            try
            {
                componentStartedOrStoppedOnTargetCheck();   
                String instance = getRunningInstance();
                ObjectName configMBeanName = null;
                if ( instance != null )
                {
                    configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);

                    if ( configMBeanName != null )
                    {
                        response =  (String) invokeRemoteOperation(
                            configMBeanName, 
                            "retrieveConfigurationDisplayData", 
                            new Object[]{}, 
                            new String[]{}, 
                            instance);
                    }
                }
            }
            catch (javax.jbi.JBIException jbiex )
            {
                mLog.fine(MessageHelper.getMsgString(jbiex));
                throw new MBeanException(jbiex);
            }
            catch (Exception ex)
            {
                try
                {
                    String errMsg = mMsgBuilder.buildExceptionMessage(
                                "retrieveConfigurationDisplayData", ex);
                    Exception jbiEx = new javax.jbi.JBIException(errMsg); 
                    mLog.fine(MessageHelper.getMsgString(jbiEx));
                    throw new MBeanException(jbiEx);
                }
                catch( Exception exp )
                {
                    mLog.fine(MessageHelper.getMsgString(exp));
                    throw new MBeanException(ex);
                }
            }
        }
        return response;
    }
    
    /*---------------------------------------------------------------------------------*\
     *                                Private Helpers                                  *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Get the MBeanInfo from the actual component configuration MBean. If the component
     * is off-line, this information is not available and a blank MBeanInfo is returned.
     * If the component does not have a configuration MBean then a empty MBeanInfo is 
     * returned.
     */
    protected MBeanInfo getComponentMBeanInfo()
    {
         String instanceName = getRunningInstance();
         MBeanInfo mbeanInfo = null;
         
         if ( instanceName != null )
         {
             try
             {
                mbeanInfo = getComponentConfigurationMBeanInfo(instanceName);
             }
             catch ( Exception ex )
             {
                 mLog.fine(MessageHelper.getMsgString(ex));
             }
         }
         
         if ( mbeanInfo == null )
         {
             // -- all instance(s) in target down or component does not have a
             //    registered config MBean, return empty MBeanInfo
             mbeanInfo = new MBeanInfo(this.getClass().getName(), 
                "Dynaic Component Configuration facade MBean",
                new MBeanAttributeInfo[]{},
                new MBeanConstructorInfo[]{},
                new MBeanOperationInfo[]{},
                new MBeanNotificationInfo[]{});
         }
         return mbeanInfo;
    }
    
    /**
     * Get the name of the instance Configuration MBean. This is the MBean registered
     * by the ComponentFramework on behalf of the Component. This MBean is the action
     * MBean for configuration on the instance. This MBean propagates the configuration
     * calls to the actual configuration MBean registered by the component.
     *
     * @param instanceName - instance name
     * @return the JMX ObjectName of the instance's Component Configuration MBean
     */
    protected ObjectName getRemoteInstanceComponentConfigMBeanName(String instanceName)
    {
        com.sun.jbi.management.MBeanNames mbeanNames =
            new com.sun.jbi.management.system.ManagementContext(mEnvCtx).getMBeanNames(instanceName);
        
        ObjectName cfgMBeanName = null;
        if ( mComponentType == ComponentType.BINDING )
        {
            cfgMBeanName = mbeanNames.getBindingMBeanName(
                            mComponentName, 
                            com.sun.jbi.management.MBeanNames.CONTROL_TYPE_CONFIGURATION);
        }
        else
        {
            cfgMBeanName = mbeanNames.getEngineMBeanName(
                            mComponentName, 
                            com.sun.jbi.management.MBeanNames.CONTROL_TYPE_CONFIGURATION);
        }
        return cfgMBeanName;
    }
    
    /**
     * Set the configuration attribute on the target. If the target is a cluster then the
     * attribute is set on each and every instance in the cluster.
     *
     * @param Attribute - the attribute being set
     * @return a management message string which gives the status of the operation.
     */
    private String setConfigurationAttributeOnTarget(Attribute attrib)
        throws javax.jbi.JBIException
    {
        String result = null;
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                result = setConfigurationAttributeOnInstance(attrib, mTarget);
            }
            else
            {
                result = setConfigurationAttributeOnCluster(attrib, mTarget);
            }
        }
        catch ( ManagementException mex )
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
        return result;
    }
    
    /**
     * If the instance is running, set the attribute on the component configuration MBean.
     * 
     * @param attrib - management attribute to set
     * @param instanceName - instance name
     */
     protected String setConfigurationAttributeOnInstance(Attribute attrib, String instanceName)
        throws ManagementException
    {
        String response = null;
        
        if ( isInstanceRunning(instanceName) )
        {        
           setRemoteAttribute( getRemoteInstanceComponentConfigMBeanName(instanceName), 
                attrib,  instanceName);
           String[] params = new String[]{attrib.getName(), mComponentName};
           String responseMsg = mTranslator.getString(
           LocalStringKeys.JBI_CCFG_SET_ATTRIBUTE_SUCCESS, params);
           response = mMsgBuilder.buildFrameworkMessage("setAttribute",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS,
                MessageBuilder.MessageType.INFO,
           mMsgBuilder.getMessageString(responseMsg),
                params, mMsgBuilder.getMessageToken(responseMsg));
        }        
        else
        {
            // Add info that the instance is down, but component started so changes
            //        made on DAS and instance will pick up the changes.
            //        return a success w/WARNING  management message
            String[] params = new String[]{mComponentName, instanceName};
            String responseMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            response = mMsgBuilder.buildFrameworkMessage("setAttribute",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS,
                    MessageBuilder.MessageType.WARNING,
                    mMsgBuilder.getMessageString(responseMsg),
                    params, mMsgBuilder.getMessageToken(responseMsg));
        }
        return response;
    }
     
    /**
     * Set the attribute on each instance in the cluster. If a cluster 
     * has zero instances, the operation is assumed a success and the cluster-ref entry 
     * for the cluster is updated with the attribute changes
     */
     private String setConfigurationAttributeOnCluster(Attribute attrib, String clusterName)
        throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    response = setConfigurationAttributeOnInstance(attrib, instance);
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage("setAttribute",
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = mMsgBuilder.buildFrameworkMessage("setAttribute",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
        }
        return response;
    }

         
    /**
     * Set the configuration attributes on the target. If the target is a cluster then the
     * attributes are set on each and every instance in the cluster. The management message
     * for the cluster target has the instance level details on which atrributes from the
     * list were set successfully.
     *
     * @param attribList - the list of attributes being set
     * @return a ComponentConfigurationResponse which gives the status of the operation.
     */
    private ComponentConfigurationResponse setConfigurationAttributesOnTarget(AttributeList attribList)
        throws javax.jbi.JBIException
    {
        ComponentConfigurationResponse result = null;
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                result = setConfigurationAttributesOnInstance(attribList, mTarget);
            }
            else
            {
                result = setConfigurationAttributesOnCluster(attribList, mTarget);
            }
        }
        catch ( ManagementException mex )
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
        return result;
    }
    
    /**
     * If the instance is running, set the attributes on the instance
     * component configuration action MBean.
     * 
     * @param attribList - management attribute to set
     * @param instanceName - instance name
     */
     protected ComponentConfigurationResponse 
             setConfigurationAttributesOnInstance(AttributeList attribList, String instanceName)
        throws ManagementException
    {
        ComponentConfigurationResponse response = null;
        
        if ( isInstanceRunning(instanceName) )
        {        
            AttributeList setAttributes =  (AttributeList) setRemoteAttributes(
                getRemoteInstanceComponentConfigMBeanName(instanceName), attribList, instanceName);
            
            if ( !attribList.isEmpty() && setAttributes.isEmpty() )
            {
                String[] params = new String[]{attribListToString(attribList), mComponentName, instanceName};
                String responseMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_SET_ATTRIBUTES_FAILURE, params);
                String jbiMsg = mMsgBuilder.buildFrameworkMessage("setAttributes",
                    com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                        MessageBuilder.MessageType.ERROR,
                        mMsgBuilder.getMessageString(responseMsg),
                        params, mMsgBuilder.getMessageToken(responseMsg));
                throw new ManagementException(jbiMsg);
            }
            else
            {
            
                // -- Build a management message response, the task status message
                //          includes the names of all the attributes set
                String[] params = new String[]{attribListToString(setAttributes), mComponentName};
                String responseMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_SET_ATTRIBUTES_SUCCESS, params);
                String jbiMsg = mMsgBuilder.buildFrameworkMessage("setAttributes",
                    com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS,
                    ( ( setAttributes.size() < attribList.size() ) ? 
                        MessageBuilder.MessageType.WARNING :  MessageBuilder.MessageType.INFO ),
                        mMsgBuilder.getMessageString(responseMsg),
                        params, mMsgBuilder.getMessageToken(responseMsg));
                
                response = new ComponentConfigurationResponse(jbiMsg, setAttributes);
            }
        }        
        else
        {
            // Add info that the instance is down, but component started so changes
            //        made on DAS and instance will pick up the changes.
            //        return a success w/WARNING  management message
            
            String[] params = new String[]{mComponentName, instanceName};
            String responseMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            String jbiMsg = mMsgBuilder.buildFrameworkMessage("setAttributes",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS,
                    MessageBuilder.MessageType.WARNING,
                    mMsgBuilder.getMessageString(responseMsg),
                    params, mMsgBuilder.getMessageToken(responseMsg));
            response = new ComponentConfigurationResponse(jbiMsg, attribList);
        }
        return response;
    }
     
    /**
     * Set the attributes on each instance in the cluster. If a cluster 
     * has zero instances, the operation is assumed a success and the cluster-ref entry 
     * for the cluster is updated with the attribute changes
     *
     * @param attribList - attribute list
     * @param clusterName - name of the cluster
     */
     private ComponentConfigurationResponse
             setConfigurationAttributesOnCluster(AttributeList attribList, 
        String clusterName)  throws ManagementException
     {
        ComponentConfigurationResponse response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        AttributeList setAttributes = new AttributeList();
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    ComponentConfigurationResponse instResponse = 
                            setConfigurationAttributesOnInstance(attribList, instance);
                    responseMap.put(instance, instResponse.getResponseMessage());
                    for ( Object attrib : instResponse.getAttributes() )
                    {
                        if ( !setAttributes.contains(attrib) )
                        {
                            setAttributes.add(attrib); 
                        }
                    }
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);
                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage("setAttributes",
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                
                response = new ComponentConfigurationResponse(
                        msg.getMessage(), setAttributes);
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            // Empty cluster - always success
            String jbiMsg = mMsgBuilder.buildFrameworkMessage("setAttributes",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
            response = new ComponentConfigurationResponse(
                        jbiMsg, attribList);
        }
        return response;
    }
     
    /**
     * @return the name of a instance which is running in the target. If target is a 
     *         standalone instance and it's up, then the name of the instance is returned
     *         if it's down a null value is returned. If target is a cluster then the 
     *         name of a running instance in the cluster is returned. If none of the
     *         instances in the cluster are up then a null is returned.
     */
     private String getRunningInstance()
     {
        String runningInstance = null;
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                if ( isInstanceRunning(mTarget) )
                {
                    runningInstance = mTarget;
                }
            }
            else if ( mPlatform.isCluster(mTarget) )
            {
                Set<String> instances = mPlatform.getServersInCluster(mTarget);

                for ( String instance : instances )
                {
                    if ( isInstanceRunning(instance) )
                    {
                        runningInstance = instance;
                        break;
                    }
                }
            }
        }
        catch ( ManagementException mex )
        {
            mLog.fine(MessageHelper.getMsgString(mex));
        }
        return runningInstance;
     }
     
    /** 
     * Get the ObjectName of the actual Component Configuration MBean. 
     * @param instanceName the target instance name
     * @return the ObjectName of the actual Component Configuration MBean, if 
     *         a configuration MBean is not registered by the component a null value
     *         is returned.
     */
    private ObjectName getComponentConfigurationMBeanName(String instanceName)
        throws javax.jbi.JBIException
    {
        ObjectName configMBeanName = null;
        MBeanInfo compCfgMBeanInfo = null;
        javax.management.MBeanServerConnection mbns = null;
        try
        {
            mbns = mPlatform.getMBeanServerConnection(instanceName);
        
            com.sun.jbi.management.MBeanNames mbeanNames =
            new com.sun.jbi.management.system.ManagementContext(mEnvCtx).getMBeanNames(instanceName);
            /**
             * First check if there is a match for a custom component MBean which uses the
             * following ObjectName pattern : 
             *    com.sun.jbi:ControlType=Configuration,ComponentName=<component-name>
             */
            ObjectName standardPattern = mbeanNames.
                getCustomComponentMBeanNameFilter(
                    instanceName, 
                    // CustomControlName="Configuration" 
                    "Configuration", 
                    mComponentName);

            Set<ObjectName> names = mbns.queryNames(standardPattern, null);

            if ( !names.isEmpty() )
            {
                if ( names.size() > 1 )
                {
                    mLog.finest("More than one MBean matches ObjectName pattern "
                        + standardPattern + ".  " + convertToString(names));
                }

                configMBeanName = (ObjectName) names.iterator().next();
            }
            else
            {
                /**
                 * If a custom component configuration MBean does not follow the standard 
                 * naming convention ( which the open-jbi components don't right now ) then
                 * look for the specific MBean for the component :
                 *   com.sun.ebi:ServiceType=Configuration,IdentificationName=<component-name>,* 
                 */
                ObjectName ebiPattern = null;
                try
                {
                    ebiPattern = new ObjectName(
                        "com.sun.ebi:ServiceType=Configuration,IdentificationName="+ mComponentName + ",*");
                }
                catch ( Exception ex)
                {
                    mLog.fine(MessageHelper.getMsgString(ex));
                }

                Set<ObjectName> ebiNames = mbns.queryNames(ebiPattern, null);

                if ( !ebiNames.isEmpty() )
                {
                    if ( ebiNames.size() > 1 )
                    {
                        mLog.finest("More than one MBean matches ObjectName pattern "
                            + standardPattern + ".  " + convertToString(ebiNames));
                    }

                    configMBeanName = (ObjectName) ebiNames.iterator().next();
                }

            }
        }
        catch ( Exception ex )
        {
            String errMsg = mMsgBuilder.buildExceptionMessage(
                "getComponentConfigurationMBeanName", ex);
            throw new javax.jbi.JBIException(errMsg);
        }
        return configMBeanName;
    }
            
     /** 
     * Get the MBeanInfo of the actual Component Configuration MBean. 
     * @param instanceName the target instance name
     * @return the MBeanInfo of the actual Component Configuration MBean, if 
     *         a configuration MBean is not registered by the component a null value
     *         is returned.
     */
    private MBeanInfo getComponentConfigurationMBeanInfo(String instanceName)
        throws javax.jbi.JBIException
    {           
        MBeanInfo compCfgMBeanInfo = null;
        ObjectName configMBeanName = getComponentConfigurationMBeanName(instanceName);

        if ( configMBeanName != null )
        {
            try
            {
                javax.management.MBeanServerConnection mbns =
                    mPlatform.getMBeanServerConnection(instanceName);
                compCfgMBeanInfo = mbns.getMBeanInfo(configMBeanName);
            }
            catch ( Exception ex)
            {
                String errMsg = mMsgBuilder.buildExceptionMessage(
                    "getComponentConfigurationMBeanInfo", ex);
                throw new javax.jbi.JBIException(errMsg);
            }
        }
  
        return compCfgMBeanInfo;
    }
    
    /**
     * Merge two MBeanOperationInfo arrays.
     *
     * @param array1 - 
     * @param array2
     * @return an array containing the OperationInfos from both the arrays.
     */
    protected MBeanOperationInfo[] merge(MBeanOperationInfo[] a1, MBeanOperationInfo[] a2)
    {   	
		MBeanOperationInfo[] array = new MBeanOperationInfo[a1.length + a2.length];
        
        // Add the operation infos from the first set
	for (int x=0; x < a1.length; x++) 
        {
	    array[x] = a1[x];
	}
        // Add the operation infos from the second set
	for (int x=0; x < a2.length; x++) 
        {
	    array[x + a1.length] = a2[x];
	}
        
        return array;
    }
    
    /**
     * Check if the component is started or stopped on the target, if it is not then a exception is
     * thrown. The component state persisted in the registry is used to determine if the
     * component is started.
     *
     * @throws javax.jbi.JBIException if the component is not started or stopped on the target.
     */
    protected void componentStartedOrStoppedOnTargetCheck()
        throws javax.jbi.JBIException
    {
        ComponentState state = getComponentQuery().getStatus(mComponentName);

        if (!(( ComponentState.STARTED == state ) || ( ComponentState.STOPPED == state )))
        {
            String[] params = new String[]{mComponentName, state.toString(), mTarget};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_NOT_STARTED_NOT_STOPPED_CFG,
                params);


            String jbiMsg = mMsgBuilder.buildFrameworkMessage("componentStartedOrStoppedOnTargetCheck",
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg),
                params, mMsgBuilder.getMessageToken(errMsg));

            throw new javax.jbi.JBIException(jbiMsg);
        }
    }

    /**
     * Check if the target is Up, throw an exception otherwise
     *
     * @throws javax.jbi.JBIException if the target is not up.
     */
    private void targetUpCheck()
        throws javax.jbi.JBIException
    {
        String instance = getRunningInstance();
        if (  instance == null )
        {
            String[] params = new String[]{mTarget};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JB_ADMIN_TARGET_NOT_UP,
                params);
            

            String jbiMsg = mMsgBuilder.buildFrameworkMessage("targetUpCheck", 
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                mMsgBuilder.getMessageString(errMsg),
                params, mMsgBuilder.getMessageToken(errMsg));

            throw new javax.jbi.JBIException(jbiMsg);
        }
    }
    
    /**
     * Check if the cluster is completely Up, throw an exception otherwise
     *
     * @throws javax.jbi.JBIException if the target is not up.
     */
    private void clusterUpCheck()
        throws javax.jbi.JBIException
    {
         if ( mPlatform.isCluster(mTarget) )
         {
            if ( !areAllInstancesUp(mTarget))
            {
                String[] params = new String[]{mTarget};
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_CLUSTER_NOT_COMPLETELY_UP,
                        params);
            

                String jbiMsg = mMsgBuilder.buildFrameworkMessage("clusterUpCheck", 
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));

                throw new javax.jbi.JBIException(jbiMsg);
            }
         }
    }
    
    /** 
     * Check if the component is started on the target, if it is not then a exception is
     * thrown. The component state persisted in the registry is used to determine if the
     * component is started.
     *
     * @throws javax.jbi.JBIException if the component is not started on the target, 
     *         indicating one cannot list application configuration.
     */
    protected void canListAppConfigCheck()
        throws javax.jbi.JBIException
    {
        ComponentState state = getComponentQuery().getStatus(mComponentName);
        
        if (!(( ComponentState.STARTED == state ) || ( ComponentState.STOPPED == state )))
        {
            String[] params = new String[]{mComponentName, state.toString(), mTarget};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_NOT_STARTED_NOT_STOPPED_LST,
                params);
            

            String jbiMsg = mMsgBuilder.buildFrameworkMessage("canListAppConfigCheck", 
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                mMsgBuilder.getMessageString(errMsg),
                params, mMsgBuilder.getMessageToken(errMsg));

            throw new javax.jbi.JBIException(jbiMsg);
        }
    }
    
    /**
     * Check if all the attributes in the attribute list are valid i.e. the MBeanInfo
     * of the ComponentConfigurationMBean has these attributes and the values are of the
     * same type
     *
     * @param attributeList - list of attributes
     * @throws javax.jbi.JBIException if a attribute value is not of the type
     *         expected or if the provided attributeList has an attribute
     *         which is not present in the AttributeInfo
     */
    protected void attributeCheck(AttributeList attributeList)
        throws javax.jbi.JBIException
    {
        for ( Object attribute : attributeList )
        {
            attributeCheck((Attribute) attribute);
        }
    }
    
    
    /**
     * Check if the attribute is valid i.e. the MBeanInfo of the 
     * ComponentConfigurationMBean has these attributes and the values are of the
     * same type.
     *
     * @param attribute - the attribute to be checked
     * @throws javax.jbi.JBIException if the attribute value is not of the type
     *         expected or if the provided attribute
     *         is not present in the AttributeInfo
     */
    protected void attributeCheck(Attribute attribute)
        throws javax.jbi.JBIException
    {
        boolean attribFound = false;
        boolean valueValid = false;
        
        String instance = getRunningInstance();
        if (  instance != null )
        {
            MBeanAttributeInfo[] attribInfos = getMBeanInfo().getAttributes();
                    
            if ( attribInfos.length == 0 )
            {   
                /** At least one instance is up, but no attribute infos means
                 *  no configuration MBean
                 */
                String errMsg = null;
                // ComponentState state = getComponentQuery().getStatus(mComponentName);
                ObjectName ccfgMBeanName = getComponentConfigurationMBeanName(instance);
                
                if (ccfgMBeanName == null)
                {
                    /** This is the case that component is started or stopped, 
                     * but the no registered config mbean is found.
                     */
                    errMsg = mTranslator.getString(
                                LocalStringKeys.JBI_CCFG_CONFIG_MBEAN_NOT_FOUND,
                                mComponentName);
                }
                else
                {
                    /** This is the case that component is started, but the registered 
                     *  config mbean did not expose any attributes
                     */
                    errMsg = mTranslator.getString(
                                    LocalStringKeys.JBI_CCFG_NO_CONFIG_ATTRIBUTES,
                                    mComponentName);
                }
                
                String jbiMsg = mMsgBuilder.buildFrameworkMessage("attributeCheck", 
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                    mMsgBuilder.getMessageString(errMsg),
                    new String[]{mComponentName}, mMsgBuilder.getMessageToken(errMsg));
                    
                throw new javax.jbi.JBIException(jbiMsg);
            }
            
            for ( MBeanAttributeInfo attribInfo : attribInfos )
            {
                if ( attribInfo.getName().equals(attribute.getName()))
                {
                    String type = attribInfo.getType();

                    if ( sTypeMap.containsKey(type) )
                    {
                        type = (String) sTypeMap.get(type);
                    }   
                    if ( !( type.equals(attribute.getValue().getClass().getName()) ) )
                    {
                        String errMsg = mTranslator.getString(
                            LocalStringKeys.JBI_CCFG_ATTRIBUTE_VALUE_INVALID, 
                                attribute.getName(), 
                                mComponentName, type);

                        throw new javax.jbi.JBIException(errMsg);
                    }

                    // Found an attribute with matching name and type
                    return;
                }
            }
            
            String errMsg = mTranslator.getString(
                            LocalStringKeys.JBI_CCFG_ATTRIBUTE_NOT_FOUND, 
                            attribute.getName(), 
                            mComponentName);
                            
            throw new javax.jbi.JBIException(errMsg);
        }   
    }
    
    /**
     * Check if all the attributes in the list are valid i.e. the MBeanInfo
     * of the ComponentConfigurationMBean has these attributes 
     *
     * @param attributeList - list of attribute names
     * @throws javax.jbi.JBIException if the provided attributeList has an attribute
     *         which is not present in the AttributeInfo
     */
    protected void attributeCheck(String[] attributeList)
        throws javax.jbi.JBIException
    {
        for ( String attribute : attributeList )
        {
            attributeCheck( (String) attribute);
        }
    }
    
    
    /**
     * Check if the attribute is valid i.e. the MBeanInfo of the 
     * ComponentConfigurationMBean has this attribute.
     *
     * @param attribute - the attribute to be checked
     * @throws javax.jbi.JBIException if the provided attribute
     *         is not present in the AttributeInfo
     */
    protected void attributeCheck(String attributeName)
        throws javax.jbi.JBIException
    {
        boolean attribFound = false;
        boolean valueValid = false;
        
        String instance = getRunningInstance();
        if (  instance != null )
        {
            MBeanAttributeInfo[] attribInfos = getMBeanInfo().getAttributes();
                   
            if ( attribInfos.length == 0 )
            {
                // At least one instance is up, but no attribute infos means
                // no configuration MBean
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_NO_CONFIG_ATTRIBUTES, mComponentName);
                    
                String jbiMsg = mMsgBuilder.buildFrameworkMessage("attributeCheck", 
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                    mMsgBuilder.getMessageString(errMsg),
                    new String[]{mComponentName}, mMsgBuilder.getMessageToken(errMsg));
                    
                throw new javax.jbi.JBIException(jbiMsg);
            }
            
            for ( MBeanAttributeInfo attribInfo : attribInfos )
            {
                if ( attribInfo.getName().equals(attributeName))
                {
                    // Found an attribute with natching name
                    return;
                 
                }
            }
            
            String errMsg = mTranslator.getString(
                            LocalStringKeys.JBI_CCFG_ATTRIBUTE_NOT_FOUND, 
                            attributeName, 
                            mComponentName);
                            
            throw new javax.jbi.JBIException(errMsg);
        }   
    }
    
    
    /**
     * Persist component configuration static attribute to the registry
     *
     * @param attrib - the attribute to persist
     */
    private void persistAttribute(Attribute attrib)
    {
        try
        {
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            
            String attributeValueToPersist = encryptCompConfigPasswordField(
                        attrib.getName(), attrib.getValue().toString());
            updater.setComponentAttribute(mComponentName, mTarget, attrib.getName(), 
                attributeValueToPersist);
        }
        catch(Exception rex)
        {
            String msg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_PERSIST_ATTRIBUTE_FAILURE,
                mComponentName, attrib.getName(), rex.getMessage());
            mLog.fine(msg);
        }
    }
    
    /**
     * Persist component configuration static attributes to the registry
     *
     * @param attribs - list of attributes to persist
     */
    private void persistAttributes(AttributeList attribs)
    {
        try
        {
            com.sun.jbi.management.registry.Updater
                updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
            
            Properties props = new Properties();
            for ( Object attrib : attribs )
            {
                Attribute attribute = (Attribute) attrib;
                String value = ( attribute.getValue() == null ? "" : attribute.getValue().toString());
                
                String attributeValueToPersist = encryptCompConfigPasswordField(
                        attribute.getName(), value);
                props.put(attribute.getName(), attributeValueToPersist);
            }
            
            updater.setComponentAttributes(mComponentName, mTarget, props);
        }
        catch(Exception rex)
        {
            StringBuffer strBuf = new StringBuffer();
            strBuf.append("[ ");
            int i = 0;
            for ( Object attrib : attribs )
            {
                i++;
                strBuf.append( ( (Attribute) attrib).getName() );
                if ( i != attribs.size() )
                {
                    strBuf.append( ", ");
                }
            }
            strBuf.append(" ]");
            String msg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_PERSIST_ATTRIBUTE_FAILURE,
                mComponentName, strBuf.toString(), rex.getMessage());
            mLog.fine(msg);
        }
    }
    
    /**
     * @return all the properties set on the component and persisted
     */
    /** Issue 780 Fix : private Properties retrieveAttributes()
    {
            Properties props = new Properties();
            
            try
            {
            
                if ( mEnvCtx.isFrameworkReady(false))
                {
                    // Initialization complete can use registry
                    com.sun.jbi.management.ComponentInfo compInfo
                        = (com.sun.jbi.management.ComponentInfo) 
                            getComponentQuery().getComponentInfo(mComponentName);

                    props = compInfo.getConfiguration();
                }
                else
                {
                    // Get the attribute from the registry using DOM
                    com.sun.jbi.management.registry.xml.RegistryDocument regDoc = 
                        new com.sun.jbi.management.registry.xml.RegistryDocument(
                            mEnvCtx.getReadOnlyRegistry());

                    com.sun.jbi.platform.PlatformContext ctx = mEnvCtx.getPlatformContext();
                    props = regDoc.getComponentConfigurationAttributes( mTarget, 
                        ctx.isStandaloneServer(mTarget), mComponentName);
                }
                props = maskCompConfigPasswordFields(props);
            }
            catch ( Exception ex )
            {
                mLog.fine(MessageHelper.getMsgString(ex));
            }
            
            return props;
     }
     **/
            
            

    /**
     * @param attribName - identification of the attribute
     * @return the MBeanAttributeInfo for the specified attribute
     */
    protected MBeanAttributeInfo getAttributeInfo(String attribute)
    {
        MBeanInfo  mbeanInfo =  this.getMBeanInfo();
        MBeanAttributeInfo[] attribInfos = null;
        MBeanAttributeInfo aInfo = null;
        if ( mbeanInfo != null )
        {
            attribInfos = mbeanInfo.getAttributes();
            for(MBeanAttributeInfo attribInfo : attribInfos )
            {
                if ( attribInfo.getName().equals(attribute))
                {
                    aInfo = attribInfo;
                }
            }
        }
        return aInfo;
    }           
    
    /**
     * Persist the attributes of the component configuration MBean. Skip the 
     * "EnvironmentVariables" attribute
     */
    /** Issue 780 Fix : private AttributeList persistMBeanAttributes(ObjectName configMBeanName, String instance)
        throws ManagementException
    {
        MBeanAttributeInfo[] attribInfos = getMBeanInfo().getAttributes();
        Vector<String> attributes = new Vector();
        AttributeList attribs = new AttributeList();
        
        for ( MBeanAttributeInfo attribInfo : attribInfos )
        {
            if ( !attribInfo.getName().equals("ApplicationVariables") &&
                 !attribInfo.getName().equals("ApplicationConfigurations")   )
            {
                attributes.add(attribInfo.getName());
            }
        }
        
        javax.management.MBeanServerConnection 
                instanceMBnSvr = getMBeanServerConnection(instance);
        try
        {
            if ( instanceMBnSvr.isRegistered(configMBeanName))
            {
                attribs =  (AttributeList) getRemoteAttributes(
                    configMBeanName, attributes.toArray(new String[attribs.size()]), instance);
                persistAttributes(attribs);
            } 
        } 
        catch ( java.io.IOException iex)
        {
            mLog.fine(MessageHelper.getMsgString(iex));
        }
        return attribs;
    } */
    
    /**
     * Check for the existance of an application variable.
     *
     * @name application variable name
     * @name shouldExist - if true, an exception is thrown if the application
     *                     variable does not exist, if false, an exception is
     *                     thrown if the variable exists.
     */
    protected String applicationVariableExistsCheck(String name, boolean shouldExist)
        throws javax.jbi.JBIException
    {
        TabularData appVars = getApplicationVariables();
        Object[] key = new Object[]{name};
        boolean exists = false;
        String type = mCfgHlpr.DEFAULT_APP_VAR_TYPE;
        if ( appVars.containsKey(key))
        {
            exists = true;
            CompositeData appVar = appVars.get(key);
            type = (String) appVar.get(mCfgHlpr.getApplicationVariableItemNames()[2]);
        }
        
        String[] params = new String[]{name};
        String errMsg   = null;
        if ( exists && !shouldExist)
        {
            // throw a exception with a message that app variable already exists
            errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_APP_VAR_EXISTS, params);
        }
        else if ( !exists && shouldExist)
        {
            // throw a exception with a message that app variable does not exist
            errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_APP_VAR_DOES_NOT_EXIST, params);
        }
        
        if ( errMsg != null)
        {
            String jbiMsg = mMsgBuilder.buildFrameworkMessage(
                "applicationVariableExistsCheck",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg),
                params, mMsgBuilder.getMessageToken(errMsg));
             throw new javax.jbi.JBIException(jbiMsg);
        }
        return type;
    }

        /**
     * Check for the existance of an application variable.
     *
     * @name application variable name
     * @name instance - instance name to check
     */
    protected boolean applicationVariableExistsInstanceCheck(String name, String instance)
   {
        TabularData appVars = getApplicationVariablesForInstance(instance);
        Object[] key = new Object[]{name};
        boolean exists = false;
         if ( appVars.containsKey(key))
        {
            exists = true;
        }
        return (exists);
    }

    /**
     * Check if the name string matches the application variable name
     *
     * @param name - application variable should have the same name
     * @param type - existing application variable type, if app var exists,
     *               null other wise.
     * @param appVar - application variable compoiste to check
     */
     protected void applicationVariableCompositeIntegrityCheck(String name, 
             String type, CompositeData appVar, boolean isUpdate)
        throws javax.jbi.JBIException
     {
         if ( type == null )
         {
             type = mCfgHlpr.DEFAULT_APP_VAR_TYPE;
         }
         
         String errMsg = null;
         String[] params = new String[]{};
         String newType =  (String) appVar.get(mCfgHlpr.getApplicationVariableItemNames()[2]);
         String newValue = (String) appVar.get(mCfgHlpr.getApplicationVariableItemNames()[1]);
         if ( !appVar.get("name").equals(name))
         {
             params = new String[]{ name, (String)appVar.get("name")};
             errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_INVALID_APP_VAR, params);
         }
         
         if (newType == null || newValue == null)
         {
             errMsg = mTranslator.getString(LocalStringKeys.JBI_CCFG_INCOMPLETE_APP_VAR);
         }
         
         if ( isUpdate )
         {
             if ( !type.equalsIgnoreCase(newType) )
             {
                 params = new String[]{ name, type, newType };
                 errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_CANNOT_UPDATE_TYPE, params);
             }
         }
         
         checkAppVarValue(name, newType, newValue);
         
         if ( errMsg != null )
         {
         
             String jbiMsg = mMsgBuilder.buildFrameworkMessage(
                "applicationVariableCompositeIntegrityCheck",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg),
                params, mMsgBuilder.getMessageToken(errMsg));
             throw new javax.jbi.JBIException(jbiMsg);
         }
     }
     
    /**
     * Add an application variable to a target. If the target is a cluster then the
     * variable is added to each and every instance in the cluster.
     *
     * @param name - name of the application variable
     * @param appvar - the application variable being set
     * @return a management message string which gives the status of the operation.
     */
    private String addApplicationVariableToTarget(String name, CompositeData appVar)
        throws javax.jbi.JBIException
    {
        String result = null; 
        
        // --If the MBean application variables have not been peristsed, do that first
        /** Issue 780 Change: When target=cluster persist only the app. variable 
         *  being added.
         * 
         */
        //persistApplicationVariables();
        
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                result = addApplicationVariableToInstance(name, appVar, mTarget);
            }
            else
            {
                result = addApplicationVariableToCluster(name, appVar, mTarget);
            }
        }
        catch ( ManagementException mex )
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
        return result;
    }
    
    /**
     * If the instance is running, set the attribute on the component configuration MBean.
     * 
     * @param instanceName - server name
     * @param name - name of the application variable
     * @param appvar - the application variable being set     
     */
     protected String addApplicationVariableToInstance(String name, CompositeData appVar, String instanceName)
        throws ManagementException
    {
        String responseMsg = null;
        
        if ( isInstanceRunning(instanceName) )
        {        
           responseMsg = (String) invokeRemoteOperation(
                getRemoteInstanceComponentConfigMBeanName(instanceName),
                "addApplicationVariable", 
                new Object[]{name, appVar}, 
                new String[]{"java.lang.String","javax.management.openmbean.CompositeData"}, 
                instanceName);
        }        
        else
        {
            // Component is offline 
            String[] params = new String[]{mComponentName, instanceName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            responseMsg = mMsgBuilder.buildFrameworkMessage("addApplicationVariable",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));
        }
        return responseMsg;
    }
     
    /**
     * Add the variable to each instance in the cluster. If a cluster 
     * has zero instances, the operation is assumed a success and the cluster-ref entry 
     * for the cluster is updated with the attribute changes
     *
     * @param clusterName - cluster name
     * @param name - name of the application variable
     * @param appvar - the application variable being set     
     */
     private String addApplicationVariableToCluster(String name, CompositeData appVar, 
        String clusterName) throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    // invoke the operation on each instance 
                    if ( isInstanceRunning(instance) )
                    {
                        response = addApplicationVariableToInstance(name, appVar, instance);
                    }
                    else
                    {
                        // assume success and update registry later
                        response = mMsgBuilder.buildFrameworkMessage("addApplicationVariable",
                            com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
                    }
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage("addApplicationVariable",
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = mMsgBuilder.buildFrameworkMessage("addApplicationVariable",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
        }
        return response;
    }
     
     /**
      * Add the application variable to the registry.
      *
      * @param appVar - the application variable to be added to the registry.
      */
     private void addApplicationVariable(CompositeData appVar)
        throws com.sun.jbi.management.registry.RegistryException
     {
         if ( !mTarget.equals(SERVER) )
         {
            com.sun.jbi.management.ComponentInfo.Variable[] vars =
                    new com.sun.jbi.management.ComponentInfo.Variable[1];

            try
            {
                String appVarValue = (String) appVar.get("value");
                if ( PASSWORD.equals((String) appVar.get("type")) )
                {
                    appVarValue = mPlatform.getKeyStoreUtil().
                            encrypt(appVarValue);
                }
                vars[0] = new com.sun.jbi.management.ComponentInfo.Variable(
                            (String) appVar.get("name"),
                            appVarValue,
                            (String) appVar.get("type"));
                com.sun.jbi.management.registry.Updater
                    updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                updater.addComponentApplicationVariables(mComponentName, mTarget, vars);
            }
            catch(Exception rex)
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_FAILED_ADD_VAR_REG,
                    mComponentName, vars[0].getName(), rex.getMessage());
                mLog.fine(msg);
            }
         }
     }
     
     
    /**
     * Set an application variable on a target. If the target is a cluster then the
     * variable is added to each and every instance in the cluster.
     *
     * @param name - name of the application variable
     * @param appvar - the application variable being set
     * @return a management message string which gives the status of the operation.
     */
    private String setApplicationVariableOnTarget(String name, CompositeData appVar)
        throws javax.jbi.JBIException
    {
        String result = null;
        
        /** Issue 780 Fix - don't persist the changes after querying, all queries are live
        // --If the MBean application variables have not been peristsed, do that first
        persistApplicationVariables();
         */
     
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                result = setApplicationVariableOnInstance(name, appVar, mTarget);
            }
            else
            {
                result = setApplicationVariableOnCluster(name, appVar, mTarget);
            }
        }
        catch ( ManagementException mex )
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
        return result;
    }
    
    /**
     * If the instance is running, set the application variable component configuration MBean.
     * 
     * @param instanceName - server name
     * @param name - name of the application variable
     * @param appvar - the application variable being set     
     */
     protected String setApplicationVariableOnInstance(String name, CompositeData appVar, 
        String instanceName)
        throws ManagementException
    {
        String responseMsg = null;
        
        if ( isInstanceRunning(instanceName) )
        {        
           responseMsg =  (String) invokeRemoteOperation(
                getRemoteInstanceComponentConfigMBeanName(instanceName),
                "setApplicationVariable", 
                new Object[]{name, appVar}, 
                new String[]{"java.lang.String","javax.management.openmbean.CompositeData"}, 
                instanceName);
        }        
        else
        {
            // Component is offline 
            String[] params = new String[]{mComponentName, instanceName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            responseMsg = mMsgBuilder.buildFrameworkMessage("setApplicationVariable",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));
        }
        return responseMsg;
    }
     
    /**
     * Set the variable on each instance in the cluster. If a cluster 
     * has zero instances, the operation is assumed a success and the cluster-ref entry 
     * for the cluster is updated with the attribute changes
     *
     * @param clusterName - cluster name
     * @param name - name of the application variable
     * @param appvar - the application variable being set     
     */
     private String setApplicationVariableOnCluster(String name, CompositeData appVar, 
        String clusterName) throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    // invoke the operation on each instance 
                    if ( isInstanceRunning(instance) )
                    {
                        response = setApplicationVariableOnInstance(name, appVar, instance);
                    }
                    else
                    {
                        // assume success and update registry later
                        response = mMsgBuilder.buildFrameworkMessage("setApplicationVariable",
                            com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
                    }
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage("setApplicationVariable",
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = mMsgBuilder.buildFrameworkMessage("setApplicationVariable",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
        }
        return response;
    }
     
     /**
      * Update the application variable in the registry.
      *
      * @param appVar - the application variable to be updated in the registry.
      */
     private void updateApplicationVariable(CompositeData appVar)
     {
         if ( !mTarget.equals(SERVER) )
         {
            com.sun.jbi.management.ComponentInfo.Variable[] vars =
                    new com.sun.jbi.management.ComponentInfo.Variable[1];

            try
            {
                String appVarValue = (String) appVar.get("value");
                if ( PASSWORD.equals((String) appVar.get("type")) )
                {
                    appVarValue = mPlatform.getKeyStoreUtil().
                            encrypt(appVarValue);
                }
                vars[0] = new com.sun.jbi.management.ComponentInfo.Variable(
                            (String) appVar.get("name"),
                            appVarValue,
                            (String) appVar.get("type"));
                com.sun.jbi.management.registry.Updater
                    updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                updater.updateComponentApplicationVariables(mComponentName, mTarget, vars);
            }
            catch(Exception rex)
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_FAILED_UPDATE_VAR_REG,
                    vars[0].getName(), mComponentName, rex.getMessage());
                mLog.fine(msg);
            }   
         }
     }

    /**
     * Delete an application variable from a target. If the target is a cluster then the
     * variable is deleted from each and every instance in the cluster.
     *
     * @param name - name of the application variable
     * @return a management message string which gives the status of the operation.
     */
    private String deleteApplicationVariableFromTarget(String name)
        throws javax.jbi.JBIException
    {
        String result = null;
        
        /** Issue 780 Fix
        // --If the MBean application variables have not been peristsed, do that first
        persistApplicationVariables(); */
        
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                applicationVariableExistsCheck(name, true);
                result = deleteApplicationVariableFromInstance(name, mTarget);
            }
            else
            {
                result = deleteApplicationVariableFromCluster(name, mTarget);
            }
        }
        catch ( ManagementException mex )
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
        return result;
    }
    
    /**
     * If the instance is running, delete the application variable from the
     * component configuration MBean.
     * 
     * @param instanceName - server name
     * @param name - name of the application variable
     * @param appvar - the application variable being set     
     */
     protected String deleteApplicationVariableFromInstance(String name, String instanceName)
        throws ManagementException
    {
        String responseMsg = null;
        
        if ( isInstanceRunning(instanceName) )
        {        
           responseMsg =  (String) invokeRemoteOperation(
                getRemoteInstanceComponentConfigMBeanName(instanceName),
                "deleteApplicationVariable", 
                new Object[]{name}, 
                new String[]{"java.lang.String"}, 
                instanceName);
        }        
        else
        {
            // Component is offline 
            String[] params = new String[]{mComponentName, instanceName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            responseMsg = mMsgBuilder.buildFrameworkMessage("deleteApplicationVariable",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));
        }
        return responseMsg;
    }
     
    /**
     * Delete the variable from each instance in the cluster. If a cluster 
     * has zero instances, the operation is assumed a success and the cluster-ref entry 
     * for the cluster is updated with the attribute changes
     *
     * @param clusterName - cluster name
     * @param name - name of the application variable
     * @param appvar - the application variable being set     
     */
     private String deleteApplicationVariableFromCluster(String name, String clusterName) 
        throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    // invoke the operation on each instance 
                    if ( isInstanceRunning(instance) && applicationVariableExistsInstanceCheck(name, instance))
                    {
                        response = deleteApplicationVariableFromInstance(name, instance);
                    }
                    else
                    {
                        // assume success and update registry later
                        response = mMsgBuilder.buildFrameworkMessage("deleteApplicationVariable",
                            com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
                    }
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage("deleteApplicationVariable",
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = mMsgBuilder.buildFrameworkMessage("deleteApplicationVariable",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
        }
        return response;
    }

     
     /**
      * Delete the application variable in the registry.
      *
      * @param name - the name of the application variable to be deleted from the registry.
      */
     private void removeApplicationVariable(String name)
     {
         
         if ( !mTarget.equals(SERVER))
         {
            try
            {
                com.sun.jbi.management.registry.Updater
                    updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                updater.deleteComponentApplicationVariables(mComponentName, mTarget, 
                    new String[]{name});
            }
            catch(com.sun.jbi.management.registry.RegistryException rex)
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_FAILED_DELETE_VAR_REG,
                    name, mComponentName, rex.getMessage());
                mLog.fine(msg);
            }   
         }
     }
     
    /** Issue 780 Fix
     * @return all the application variables set on the component and persisted
     *
    private TabularData retrieveApplicationVariables()
    {
        TabularData td = new TabularDataSupport(mCfgHlpr.getApplicationVariableTabularType());
        Variable[] vars = new Variable[0];
        try
        {

            if ( mEnvCtx.isFrameworkReady(false))
            {
                // Initialization complete can use registry
                com.sun.jbi.management.ComponentInfo compInfo
                    = (com.sun.jbi.management.ComponentInfo) 
                        getComponentQuery().getComponentInfo(mComponentName);

                vars = compInfo.getVariables();
            }
            else
            {
                // Get the attribute from the registry using DOM
                RegistryDocument regDoc = new RegistryDocument(
                    mEnvCtx.getReadOnlyRegistry());

                vars = regDoc.getComponentApplicationVariables( mTarget, 
                    mPlatform.isStandaloneServer(mTarget), mComponentName);
            }

            for ( int i=0; i < vars.length; i++ )
            {
                String[] itemNames = mCfgHlpr.getApplicationVariableItemNames();
                String appVarValue = vars[i].getValue();
                if ( PASSWORD.equalsIgnoreCase(vars[i].getType()) ) 
                {
                    appVarValue = MASKED_FIELD_VALUE;
                }
                CompositeData cd = new CompositeDataSupport(
                    mCfgHlpr.getApplicationVariablesType(),
                    itemNames,
                    new String[]{vars[i].getName(), appVarValue, vars[i].getType()});

                td.put(cd);
            }

        }
        catch ( Exception ex )
        {
            mLog.fine(MessageHelper.getMsgString(ex));
        }
        // return the retrived values 
        return td;
     } */
    
    /** Issue 780 Fix
     * If the application variables element in the registry is empty
     * get the application variables from the actual MBean and persist them.
     *
     *
    private void persistApplicationVariables()
    {
        
        TabularData appVars = null;
        com.sun.jbi.management.ComponentInfo compInfo = null;
        try
        {
            // Get the application variables persisted in the registry
            compInfo
                = (com.sun.jbi.management.ComponentInfo) 
                    getComponentQuery().getComponentInfo(mComponentName);
        }
        catch ( com.sun.jbi.management.system.ManagementException mex)
        {
            mLog.fine(MessageHelper.getMsgString(mex));
        }
        
        Variable[] vars = compInfo.getVariables();
                    
        if ( vars.length == 0 )
        {
            String instance = getRunningInstance();
            ObjectName configMBeanName = null;
            if ( instance != null )
            {
                configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);
                try
                {
                    appVars =  (TabularData) getRemoteAttribute(configMBeanName, "ApplicationVariables", instance);
                }
                catch ( ManagementException mex)
                {
                    mLog.fine(MessageHelper.getMsgString(mex));
                    throw new RuntimeException(mex.getMessage());
                }

                vars = new Variable[appVars.size()];
                java.util.Collection cds = appVars.values();
                int i=0;
                try
                {
                    for ( java.util.Iterator itr = cds.iterator(); itr.hasNext(); )
                    {
                        CompositeData cd = (CompositeData) itr.next();

                        String appVarValue = (String) cd.get("value");
                        if ( PASSWORD.equals((String) cd.get("type")) )
                        {
                            appVarValue = mPlatform.getKeyStoreUtil().
                                    encrypt(appVarValue);
                        }

                        vars[i++] = new Variable(
                                (String) cd.get("name"), 
                                         appVarValue, 
                                (String) cd.get("type"));
                    }

                    // persist them
                    com.sun.jbi.management.registry.Updater
                    updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                    updater.addComponentApplicationVariables(mComponentName, mTarget, vars);
                }
                catch(Exception rex)
                {
                    String msg = mTranslator.getString(
                        LocalStringKeys.JBI_CCFG_FAILED_ADD_VAR_REG,
                        vars[0].getName(), mComponentName, rex.getMessage());
                    mLog.fine(msg);
                }
            }       
        }
    } */
    

    /** Issue 780 Fix
     * If the application configuration element in the registry is empty
     * get the application configuration from the actual MBean and persist them.
     *
    private void persistApplicationConfiguration()
    {
        TabularData appConfigs = null;
        com.sun.jbi.management.ComponentInfo compInfo = null;
        String[] configNames = new String[0];
        try
        {
            compInfo
                = (com.sun.jbi.management.ComponentInfo) 
                    getComponentQuery().getComponentInfo(mComponentName);
            configNames = compInfo.getApplicationConfigurationNames();
        }
        catch ( ManagementException mex)
        {
            mLog.fine(MessageHelper.getMsgString(mex));
        }
                    
        if ( configNames.length == 0 )
        {
            String instance = getRunningInstance();
            // for the "server" target the framework conmfiguration MBean perists the data  
            if ( instance != null  && !SERVER.equals(instance))
            {
                ObjectName configMBeanName = getRemoteInstanceComponentConfigMBeanName(instance);
                try
                {
                    Object result = getRemoteAttribute(configMBeanName, "ApplicationConfigurations", instance);
                    if ( result != null )
                    {
                        appConfigs =  (TabularData) result;
                    }
                }
                catch ( ManagementException mex)
                {
                    mLog.fine(MessageHelper.getMsgString(mex));
                    throw new RuntimeException(mex.getMessage());
                }
                    
                if ( appConfigs != null )
                {
                    java.util.Collection cds = appConfigs.values();
                    int i=0;
                    for ( java.util.Iterator itr = cds.iterator(); itr.hasNext(); )
                    {
                        CompositeData cd = (CompositeData) itr.next();
                        Properties props = mCfgHlpr.convertCompositeDataToProperties(cd);

                        // persist them
                        try
                        {
                            props = encryptAppConfigPasswordFields(props);
                            com.sun.jbi.management.registry.Updater
                            updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                            updater.addComponentApplicationConfiguration(mComponentName, mTarget, props);
                        }
                        catch(Exception rex)
                        {
                            String msg = mTranslator.getString(
                                LocalStringKeys.JBI_CCFG_FAILED_ADD_CFG_REG,
                                cd.get(CONFIG_NAME_KEY), mComponentName, rex.getMessage());
                            mLog.fine(msg);
                        }
                    }          
                }
            }
        }
    }*/
    
    /**
     * Check if the configurationName key exists and matches the name passed in.
     *
     * @param name - application configuration should have the same name
     * @param appConfig - application configuration compoiste to check
     * @param isUpdate - true when this method is invoked when a config is 
     *                   being updated.
     */
     protected void applicationConfigurationCompositeIntegrityCheck(String name, 
        CompositeData appConfig, boolean isUpdate)  throws javax.jbi.JBIException
     {
         String errMsg = null;
         String[] params = new String[]{};
         
         if ( name == null )
         {
             params = new String[]{ name };
             errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_INVALID_APP_CFG_NAME, params);
         }
         else if ( "".equals(name.trim()))
         {
            params = new String[]{ name };
            errMsg = mTranslator.getString( 
                LocalStringKeys.JBI_CCFG_INVALID_APP_CFG_NAME, params);   
         }
         else if (appConfig.get(CONFIG_NAME_KEY) == null )
         {
             // if the CONFIG_NAME_KEY doesn't exist insert it
             CompositeType ct = appConfig.getCompositeType();
             
             Set<String> keySet = ct.keySet();
             
             Vector<String> keys = new Vector();
             Vector<Object> values = new Vector();
             
             for ( String key : keySet )
             {
                 if ( CONFIG_NAME_KEY.equals(key) )
                 {
                     keys.add(key);
                     values.add(name);
                 }
                 else
                 {
                     try
                     {
                        values.add(appConfig.get(key));
                        keys.add(key); 
                     }
                     catch ( javax.management.openmbean.InvalidKeyException iex ){}
                     catch ( IllegalArgumentException iax ) {}
                 }
             }
            
             try
             {
                appConfig = new CompositeDataSupport(ct, 
                         keys.toArray(new String[keys.size()]),
                         values.toArray(new String[values.size()]));
             }
             catch ( javax.management.openmbean.OpenDataException oex )
             {
                 errMsg = oex.getMessage();
             }
         }
         else if ( "".equals( ((String) appConfig.get(CONFIG_NAME_KEY)).trim()) )
         {
             // if the CONFIG_NAME_KEY exists it cannot be an empty string
             params = new String[]{ name };
             errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_INVALID_APP_CFG_DATA, params);   
         }
         else if ( !name.equals(appConfig.get(CONFIG_NAME_KEY)) )
         {
             // If both name and CONFIG_NAME_KEY are provided, they should match
             params = new String[]{ name, (String)appConfig.get(CONFIG_NAME_KEY)};
             errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_INVALID_APP_CFG, params);
         }
         
         if ( errMsg == null && !isUpdate)
         {
             // -- Verify all required fields are present when creating a new config
             CompositeType ct = appConfig.getCompositeType();

             Set<String> keySet = ct.keySet();

             Vector<String> keys = new Vector();

             for ( String key : keySet )
             {
                 boolean isInvalidValue = false;
                 
                 try
                 {
                    if ( appConfig.get(key) == null)
                    {
                        isInvalidValue = true;
                    }
                 }
                 catch ( javax.management.openmbean.InvalidKeyException iex )
                 {
                     isInvalidValue = true;
                 }
                 catch ( IllegalArgumentException iax ) {}
                 
                 try
                 {
                     if ( isInvalidValue && mCfgHlpr.isRequired(key, getConfigDoc()) )
                     {
                         params = new String[]{ name, key };
                         errMsg = mTranslator.getString(
                            LocalStringKeys.JBI_CCFG_APP_CFG_MISSING_FIELD, params);
                     }
                 }
                 catch(Exception ex)
                 {
                     errMsg = mMsgBuilder.buildExceptionMessage(
                                "applicationConfigurationCompositeIntegrityCheck", ex);
                 }
             }
         }
         
         if( errMsg != null )
         {
         
             String jbiMsg = mMsgBuilder.buildFrameworkMessage(
                "applicationConfigurationCompositeIntegrityCheck",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg),
                params, mMsgBuilder.getMessageToken(errMsg));
             throw new javax.jbi.JBIException(jbiMsg);
         }
     }
    
     
    /**
     * Check if the configurationName key exists and matches the name passed in.
     *
     * @param name - application configuration should have the same name
     * @param appCfgProps - application configuration properties to check
     * @param isUpdate - true when this method is invoked when a config is 
     *                   being updated.
     */
     protected void applicationConfigurationCompositeIntegrityCheck(String name, 
        Properties appCfgProps, boolean isUpdate)  throws javax.jbi.JBIException
     {
         String errMsg = null;
         String[] params = new String[]{};
         
         if ( name == null )
         {
             params = new String[]{ name };
             errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_INVALID_APP_CFG_NAME, params);
         } 
         else if ( "".equals(name.trim()))
         {
            params = new String[]{ name };
            errMsg = mTranslator.getString( 
                LocalStringKeys.JBI_CCFG_INVALID_APP_CFG_NAME, params);   
         }
         if ( appCfgProps.get(CONFIG_NAME_KEY) == null )
         {
             appCfgProps.setProperty(CONFIG_NAME_KEY, name);
         }
         else if ( "".equals( ((String) appCfgProps.get(CONFIG_NAME_KEY)).trim()))
         {
             // if the CONFIG_NAME_KEY exists it cannot be an empty string             
            params = new String[]{ name };
            errMsg = mTranslator.getString( 
                LocalStringKeys.JBI_CCFG_INVALID_APP_CFG_DATA, params);   
         }
         else if ( !name.equals(appCfgProps.get(CONFIG_NAME_KEY)) )
         {
             // If both name and CONFIG_NAME_KEY are provided, they should match             
             params = new String[]{ name, (String)appCfgProps.get(CONFIG_NAME_KEY)};
             errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_INVALID_APP_CFG, params);
         }
         
         // -- Verify all required fields are present
         if ( errMsg == null  && !isUpdate)
         {
             // -- Verify all required fields are present when a new config is created
             //    can do this only if the app config composite is available
             CompositeType appCfgType = queryApplicationConfigurationType();
             
             if ( appCfgType != null )
             {
                 Set<String> keySet = appCfgType.keySet();

                 for ( String key : keySet )
                 {
                     boolean isInvalidValue = false;

                     if ( appCfgProps.getProperty(key) == null)
                     {
                        isInvalidValue = true;
                     }

                     try
                     {
                         if ( isInvalidValue && mCfgHlpr.isRequired(key, getConfigDoc()) )
                         {
                             params = new String[]{key, mComponentName, name };
                             errMsg = mTranslator.getString(
                                LocalStringKeys.JBI_CCFG_APP_CFG_MISSING_FIELD, params);
                         }
                     }
                     catch(Exception ex)
                     {
                         errMsg = mMsgBuilder.buildExceptionMessage(
                                    "applicationConfigurationCompositeIntegrityCheck", ex);
                     }
                 }
             }
         }
         
         if ( errMsg != null )
         {
         
             String jbiMsg = mMsgBuilder.buildFrameworkMessage(
                "applicationConfigurationCompositeIntegrityCheck",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg),
                params, mMsgBuilder.getMessageToken(errMsg));
             throw new javax.jbi.JBIException(jbiMsg);
         }
     }
     
    /**
     * Add the application configuration compoiste.
     */
     private String addApplicationConfigurationComposite(String name, CompositeData appConfig )
        throws javax.jbi.JBIException
     {
         String result = addApplicationConfigurationToTarget(name, appConfig);

        // Parse the result if success then persist the attribute changes
        MessageBuilder.Message msg = mMsgBuilder.createMessage(result);

        if ( msg.isFailure())
        {
            // if failed, throw an exception with the failure msg
            throw new javax.jbi.JBIException(result);
        }
        else
        {
            addApplicationConfiguration(appConfig);
        }
        return result;
     }
    /**
     * Add an application configuration to a target. If the target is a cluster then the
     * configuration is added to each and every instance in the cluster.
     *
     * @param name - name of the application configuration
     * @param appCfg - the application configuration being set
     * @return a management message string which gives the status of the operation.
     */
    private String addApplicationConfigurationToTarget(String name, CompositeData appCfg)
        throws javax.jbi.JBIException
    {
        String result = null; 
        
        /** Issue 780 Fix
        // --If the MBean application configuration have not been peristsed, do that first
        persistApplicationConfiguration(); */
        
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                result = addApplicationConfigurationToInstance(name, appCfg, mTarget);
            }
            else
            {
                result = addApplicationConfigurationToCluster(name, appCfg, mTarget);
            }
        }
        catch ( ManagementException mex )
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
        return result;
    }
    
    /**
     * If the instance is running, add the application config
     * 
     * @param instanceName - server name
     * @param name - name of the application configuration
     * @param appCfg - the application config being set     
     */
     protected String addApplicationConfigurationToInstance(String name, CompositeData appCfg, String instanceName)
        throws ManagementException
    {
        String responseMsg = null;
        
        if ( isInstanceRunning(instanceName) )
        {        
           responseMsg = (String) invokeRemoteOperation(
                getRemoteInstanceComponentConfigMBeanName(instanceName),
                "addApplicationConfiguration", 
                new Object[]{name, appCfg}, 
                new String[]{"java.lang.String","javax.management.openmbean.CompositeData"}, 
                instanceName);
        }        
        else
        {
            // Component is offline 
            String[] params = new String[]{mComponentName, instanceName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            responseMsg = mMsgBuilder.buildFrameworkMessage("addApplicationConfiguration",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));
        }
        return responseMsg;
    }
     
    /**
     * Add the configuration to each instance in the cluster. If a cluster 
     * has zero instances, the operation is assumed a success and the cluster-ref entry 
     * for the cluster is updated with the attribute changes
     *
     * @param clusterName - cluster name
     * @param name - name of the application configuration
     * @param appCfg - the application configuration being set     
     */
     private String addApplicationConfigurationToCluster(String name, CompositeData appCfg, 
        String clusterName) throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    // invoke the operation on each instance 
                    if ( isInstanceRunning(instance) )
                    {
                        response = addApplicationConfigurationToInstance(name, appCfg, instance);
                    }
                    else
                    {
                        // assume success and update registry later
                        response = mMsgBuilder.buildFrameworkMessage("addApplicationConfiguration",
                            com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
                    }
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage("addApplicationConfiguration",
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = mMsgBuilder.buildFrameworkMessage("addApplicationConfiguration",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
        }
        return response;
    }
     
    /**
     * Set application configuration composite
     */
    private String setApplicationConfigurationComposite(String name, CompositeData appConfig)
        throws javax.jbi.JBIException
    {
        String result = setApplicationConfigurationOnTarget(name, appConfig);

        // Parse the result if success then persist the attribute changes
        MessageBuilder.Message msg = mMsgBuilder.createMessage(result);
        
        if ( msg.isFailure())
        {
            // if failed, throw an exception with the failure msg
            throw new javax.jbi.JBIException(result);
        }
        else
        {
            updateApplicationConfiguration(appConfig);
        } 
        
        return result;
    }
     
    /**
     * Set an application configuration to a target. If the target is a cluster then the
     * configuration is set on each and every instance in the cluster.
     *
     * @param name - name of the application configuration
     * @param appCfg - the application configuration being set
     * @return a management message string which gives the status of the operation.
     */
    private String setApplicationConfigurationOnTarget(String name, CompositeData appCfg)
        throws javax.jbi.JBIException
    {
        String result = null; 
        
        /**Issue 780 Fix
        // --If the MBean application configuration have not been peristsed, do that first
        persistApplicationConfiguration();*/
        
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                result = setApplicationConfigurationOnInstance(name, appCfg, mTarget);
            }
            else
            {
                result = setApplicationConfigurationOnCluster(name, appCfg, mTarget);
            }
        }
        catch ( ManagementException mex )
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
        return result;
    }
    
    /**
     * If the instance is running, set the application config
     * 
     * @param instanceName - server name
     * @param name - name of the application configuration
     * @param appCfg - the application config being set     
     */
     protected String setApplicationConfigurationOnInstance(String name, CompositeData appCfg, String instanceName)
        throws ManagementException
    {
        String responseMsg = null;
        
        if ( isInstanceRunning(instanceName) )
        {        
           responseMsg = (String) invokeRemoteOperation(
                getRemoteInstanceComponentConfigMBeanName(instanceName),
                "setApplicationConfiguration", 
                new Object[]{name, appCfg}, 
                new String[]{"java.lang.String","javax.management.openmbean.CompositeData"}, 
                instanceName);
        }        
        else
        {
            // Component is offline 
            String[] params = new String[]{mComponentName, instanceName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            responseMsg = mMsgBuilder.buildFrameworkMessage("setApplicationConfiguration",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));
        }
        return responseMsg;
    }
     
    /**
     * Set the configuration to each instance in the cluster. If a cluster 
     * has zero instances, the operation is assumed a success and the cluster-ref entry 
     * for the cluster is updated with the attribute changes
     *
     * @param clusterName - cluster name
     * @param name - name of the application configuration
     * @param appCfg - the application configuration being set     
     */
     private String setApplicationConfigurationOnCluster(String name, CompositeData appCfg, 
        String clusterName) throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    // invoke the operation on each instance 
                    if ( isInstanceRunning(instance) )
                    {
                        response = setApplicationConfigurationOnInstance(name, appCfg, instance);
                    }
                    else
                    {
                        // assume success and update registry later
                        response = mMsgBuilder.buildFrameworkMessage("setApplicationConfiguration",
                            com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
                    }
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage("setApplicationConfiguration",
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = mMsgBuilder.buildFrameworkMessage("setApplicationConfiguration",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
        }
        return response;
    } 

    /**
     * Delete a application configuration from a target. If the target is a cluster then the
     * configuration is deleted from each and every instance in the cluster.
     *
     * @param name - name of the application configuration
     * @return a management message string which gives the status of the operation.
     */
    private String deleteApplicationConfigurationFromTarget(String name)
        throws javax.jbi.JBIException
    {
        String result = null; 
        
        /** Issue 780 Fix
        // --If the MBean application configuration have not been peristsed, do that first
        //persistApplicationConfiguration();
         */
        
        try
        {
            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                applicationConfigurationExistsCheck(name, true);
                result = deleteApplicationConfigurationFromInstance(name, mTarget);
            }
            else
            {
                result = deleteApplicationConfigurationFromCluster(name, mTarget);
            }
        }
        catch ( ManagementException mex )
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
        return result;
    }
    
    /**
     * If the instance is running, delete the application config
     * 
     * @param instanceName - server name
     * @param name - name of the application configuration
     * @param appCfg - the application config being set     
     */
     protected String deleteApplicationConfigurationFromInstance(String name, String instanceName)
        throws ManagementException
    {
        String responseMsg = null;
        
        if ( isInstanceRunning(instanceName) )
        {        
           responseMsg = (String) invokeRemoteOperation(
                getRemoteInstanceComponentConfigMBeanName(instanceName),
                "deleteApplicationConfiguration", 
                new Object[]{name}, 
                new String[]{"java.lang.String"}, 
                instanceName);
        }        
        else
        {
            // Component is offline 
            String[] params = new String[]{mComponentName, instanceName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            responseMsg = mMsgBuilder.buildFrameworkMessage("deleteApplicationConfiguration",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));
        }
        return responseMsg;
    }
     
    /**
     * Delete the configuration from each instance in the cluster. If a cluster 
     * has zero instances, the operation is assumed a success and the cluster-ref entry 
     * for the cluster is updated with the attribute changes
     *
     * @param clusterName - cluster name
     * @param name - name of the application configuration
     */
     private String deleteApplicationConfigurationFromCluster(String name,
        String clusterName) throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    // invoke the operation on each instance 
                    if ( isInstanceRunning(instance) && applicationConfigurationExistsInstanceCheck(name, instance) )
                    {
                        response = deleteApplicationConfigurationFromInstance(name, instance);
                    }
                    else
                    {
                        // assume success and update registry later
                        response = mMsgBuilder.buildFrameworkMessage("deleteApplicationConfiguration",
                            com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
                    }
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage("deleteApplicationConfiguration",
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = mMsgBuilder.buildFrameworkMessage("deleteApplicationConfiguration",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.SUCCESS);
        }
        return response;
    } 

     /**
      * Add the application configuration to the registry.
      *
      * @param cfgProps - the application configuration properties
      */
     private void addApplicationConfiguration(Properties cfgProps)
        throws com.sun.jbi.management.registry.RegistryException
     {
         if ( !mTarget.equals(SERVER) )
         {
            try
            {
                cfgProps = encryptAppConfigPasswordFields(cfgProps);
                com.sun.jbi.management.registry.Updater
                    updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                updater.addComponentApplicationConfiguration(mComponentName, mTarget, cfgProps);
            }
            catch(Exception rex)
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_FAILED_ADD_CFG_REG,
                    cfgProps.get(CONFIG_NAME_KEY), mComponentName, rex.getMessage());
                mLog.fine(msg);
            }
        }
     }

     
     /**
      * Add the application configuration to the registry.
      *
      * @param appConfig - the application configuration to be added to the registry.
      */
     private void addApplicationConfiguration(CompositeData appConfig)
        throws com.sun.jbi.management.registry.RegistryException
     {
         Properties cfgProps = mCfgHlpr.convertCompositeDataToProperties(appConfig);
         addApplicationConfiguration(cfgProps);
     }
     
     /**
      * Update the application configuration in the registry.
      *
      * @param appVar - the application configuration to be updated in the registry.
      */
     private void updateApplicationConfiguration(CompositeData appConfig)
        throws com.sun.jbi.management.registry.RegistryException
     {       
         Properties cfgProps = mCfgHlpr.convertCompositeDataToProperties(appConfig);
         updateApplicationConfiguration(cfgProps);
     }
     
     
     /**
      * Update the application configuration in the registry.
      *
      * @param cfgProps - the application configuration properties
      */
     private void updateApplicationConfiguration(Properties cfgProps)
        throws com.sun.jbi.management.registry.RegistryException
     {
         if ( !mTarget.equals(SERVER) )
         {
            
            try
            {
                cfgProps = encryptAppConfigPasswordFields(cfgProps);
                com.sun.jbi.management.registry.Updater
                    updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                updater.updateComponentApplicationConfiguration(mComponentName, mTarget, cfgProps);
            }
            catch(Exception rex)
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_FAILED_UPDATE_CFG_REG,
                    cfgProps.get(CONFIG_NAME_KEY), mComponentName, rex.getMessage());
                mLog.fine(msg);
            }
        }
     }
     
     /**
      * Delete the application configuration from the registry.
      *
      * @param name - identification for the application configuration to be deleted.
      */
     private void removeApplicationConfiguration(String name)
        throws com.sun.jbi.management.registry.RegistryException
     {

         if ( !mTarget.equals(SERVER) )
         {
            try
            {
                com.sun.jbi.management.registry.Updater
                    updater = ((Registry) mEnvCtx.getRegistry()).getUpdater();
                updater.deleteComponentApplicationConfiguration(mComponentName, mTarget,  name);
            }
            catch(com.sun.jbi.management.registry.RegistryException rex)
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_FAILED_DELETE_CFG_REG,
                    name, mComponentName, rex.getMessage());
                mLog.fine(msg);
            }
         }
     }     
     
     
    /** Issue 780 Fix: TBD
     * If the component is offline this returns a null value, since the components
     * application configuration composite type is not known.
     *
     * @return all the application configurations set on the component and persisted
     *
    private TabularData retrieveApplicationConfiguration()
        throws javax.management.openmbean.OpenDataException
    {
        CompositeType appConfigCompositeType = queryApplicationConfigurationType();

        TabularData td = null;

        if ( appConfigCompositeType != null )
        {
            TabularType tt = new TabularType(
                "Applcation Configuration",
                "Table of Application configurations",
                appConfigCompositeType,
                new String[]{CONFIG_NAME_KEY});
            td = new TabularDataSupport(tt);
            try
            {
                Map<String, Properties> appConfigMap = new java.util.HashMap();
                if ( mEnvCtx.isFrameworkReady(false))
                {
                    // Initialization complete can use registry
                    com.sun.jbi.management.ComponentInfo compInfo
                        = (com.sun.jbi.management.ComponentInfo) 
                            getComponentQuery().getComponentInfo(mComponentName);

                    String[] configNames = compInfo.getApplicationConfigurationNames();
                    for ( String configName : configNames )
                    {
                        Properties appCfgProps = compInfo.getApplicationConfiguration(configName);
                        appCfgProps = maskAppConfigPasswordFields(appCfgProps);
                        appConfigMap.put(configName, appCfgProps);
                    }
                }
                else
                {
                    // Get the attribute from the registry using DOM
                    RegistryDocument regDoc = new RegistryDocument(
                        mEnvCtx.getReadOnlyRegistry());

                    appConfigMap = regDoc.getComponentApplicationConfiguration( mTarget, 
                        mPlatform.isStandaloneServer(mTarget), mComponentName);
                }

                Set<String> configNames = appConfigMap.keySet();
                for ( String configName : configNames )
                {
                    Properties appCfgProps = appConfigMap.get(configName);
                    appCfgProps = maskAppConfigPasswordFields(appCfgProps);
                    CompositeData cd = mCfgHlpr.convertPropertiesToCompositeData(
                        appCfgProps, appConfigCompositeType);

                    td.put(cd);
                }
            }
            catch ( Exception ex )
            {
                mLog.fine(MessageHelper.getMsgString(ex));
            }
        }
        // return the retrieved values 
        return td;
     } */
    
    /**
     * Check if the application configuration by the name is already defined.
     * If the "throwIfMissing" flag is set to true and exception is thrown if the
     * configuration is not defined, if this is false and exception is thrown if the
     * configuration is defined.
     *
     * @param configName 
     *             application configuration name
     * @param throwIfMissing 
     *             boolean flag if set to true anexception is thrown if the
     *             configuration is not defined, if this is false an exception is 
     *             thrown if the configuration is defined.
     * @throw Exception based on the throwIfMissing flag state and presence of the 
     *         named configuration. 
     */
    protected void applicationConfigurationExistsCheck(String configName, boolean throwIfMissing )
        throws javax.jbi.JBIException
    {
        TabularData td = getApplicationConfigurations();
        boolean isDefined = false;
        if ( td != null )
        {
            isDefined = ( (td.get(new String[]{configName}) == null) ? false : true );
            String[] params = new String[]{configName, mComponentName};
            String errMsg = null;
            if ( !isDefined && throwIfMissing )
            {
                errMsg =     mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_APP_CFG_DOES_NOT_EXIST,
                    params);
            }
            else if ( isDefined && !throwIfMissing )
            {
                errMsg =     mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_APP_CFG_EXISTS,
                    params);
            }
            if ( errMsg != null )
            {
                String jbiMsg = mMsgBuilder.buildFrameworkMessage(
                        "applicationConfigurationExistsCheck", 
                        MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                        mMsgBuilder.getMessageString(errMsg),
                        params, 
                        mMsgBuilder.getMessageToken(errMsg));
                throw new javax.jbi.JBIException(jbiMsg);
            }
        }
    }
    /**
     * Check if the application configuration by the name is already defined.
     * If the "throwIfMissing" flag is set to true and exception is thrown if the
     * configuration is not defined, if this is false and exception is thrown if the
     * configuration is defined.
     *
     * @param configName 
     *             application configuration name
     * @parm instance
     *             instance name to check
     * @throw Exception based on the throwIfMissing flag state and presence of the 
     *         named configuration. 
     * 
     */
    protected boolean applicationConfigurationExistsInstanceCheck(String configName,
            String instance)
     {
        TabularData td = getApplicationConfigurationsForInstance(instance);
        boolean isDefined = false;
        if ( td != null )
        {
            isDefined = ( (td.get(new String[]{configName}) == null) ? false : true );
        }
        return (isDefined);
    }
    
    /**
     * Check if the component supports application variables. If it does not
     * then an exception is thrown indicating that the component does not support
     * application variables.
     *
     * @throws javax.jbi.JBIException if the component does not support
     *         application variables
     */
    protected void supportsAppVarsCheck()
        throws javax.jbi.JBIException, MBeanException
    {
        if ( !isAppVarsSupported() )
        {
            String[] params = new String[]{mComponentName};
            String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_APP_VARS_NOT_SUPPORTED,
                    params);
            
            String jbiMsg = mMsgBuilder.buildFrameworkMessage(
                    "supportsAppVarsCheck", 
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                    mMsgBuilder.getMessageString(errMsg),
                    params, 
                    mMsgBuilder.getMessageToken(errMsg));
            throw new javax.jbi.JBIException(jbiMsg);
        }
    }

    /**
     * Check if the component supports application configuration. If it does not
     * then an exception is thrown indicating that the component does not support
     * application configuration.
     *
     * @throws javax.jbi.JBIException if the component does not support
     *         application configuration
     */
    protected void supportsAppConfigCheck()
        throws javax.jbi.JBIException, MBeanException
    {
        if ( !isAppConfigSupported() )
        {
            
            String[] params = new String[]{mComponentName};
            
            String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_APP_CFG_NOT_SUPPORTED,
                    params);
            
            String jbiMsg = mMsgBuilder.buildFrameworkMessage(
                    "supportsAppVarsCheck", 
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                    mMsgBuilder.getMessageString(errMsg),
                    params, 
                    mMsgBuilder.getMessageToken(errMsg));
            throw new javax.jbi.JBIException(jbiMsg);
        }
    }
    
    
    protected TabularData createEmptyAppConfigTable()
        throws javax.management.openmbean.OpenDataException
    {
        CompositeType appConfigCompositeType = queryApplicationConfigurationType();

        TabularData td = null;

        if ( appConfigCompositeType != null )
        {
            TabularType tt = new TabularType(
                "Applcation Configuration",
                "Table of Application configurations",
                appConfigCompositeType,
                new String[]{CONFIG_NAME_KEY});
            td = new TabularDataSupport(tt);
        }
        return td;
    }
    
       
    protected TabularData createEmptyAppVarTable()
        throws javax.management.openmbean.OpenDataException
    {
        return new TabularDataSupport(mCfgHlpr.getApplicationVariableTabularType());
    }
    
    private Properties encryptAppConfigPasswordFields(Properties cfgProps)
        throws Exception
    {
        Document configDoc = getConfigDoc();
        
        Set cfgNames = cfgProps.keySet();
        
        for ( Object cfgName : cfgNames )
        {
            if ( mCfgHlpr.isPassword((String)cfgName, configDoc) )
            {
                String clearTxt = cfgProps.getProperty((String)cfgName);
                String encrTxt = mPlatform.getKeyStoreUtil().encrypt(clearTxt);
                cfgProps.setProperty((String)cfgName, encrTxt);
            }
        }
        
        return cfgProps;
    }
    
    
    /**
     * Encrypt component configuration attribute value if it is a password field
     *
     * @param cfgName  - component configuration attribute name
     * @param cfgValue - component configuration attribute value
     */
    private String encryptCompConfigPasswordField(String cfgName, String cfgValue)
        throws Exception
    {
        Document configDoc = getConfigDoc();

        if ( mCfgHlpr.isPassword(cfgName, configDoc) )
        {
            String encrTxt = mPlatform.getKeyStoreUtil().encrypt(cfgValue);
            return encrTxt;
        }
        return cfgValue;
    }
    
    
    /**
     * If a application configuration data item is a password field, mask it 
     * i.e. replace value with "*****".
     *
     * @param cfgProps - properties representation of the composite data
     */
    private Properties maskAppConfigPasswordFields(Properties cfgProps)
        throws Exception
    {
        Document configDoc = getConfigDoc();
        
        Set cfgNames = cfgProps.keySet();
        
        for ( Object cfgName : cfgNames )
        {
            if ( mCfgHlpr.isPassword((String)cfgName, configDoc) )
            {
                cfgProps.setProperty((String)cfgName, MASKED_FIELD_VALUE);
            }
        }
        
        return cfgProps;
    }
    
    /**
     * If a component configuration attribute is a password field, mask it 
     * i.e. replace value with "*****".
     *
     * @param cfgProps - component configuration attributes
     */
    private Properties maskCompConfigPasswordFields(Properties cfgProps)
        throws Exception
    {
        Document configDoc = getConfigDoc();
        
        Set cfgNames = cfgProps.keySet();
        
        for ( Object cfgName : cfgNames )
        {
            if ( mCfgHlpr.isPassword((String)cfgName, configDoc) )
            {
                cfgProps.setProperty((String)cfgName, MASKED_FIELD_VALUE);
            }
        }
        
        return cfgProps;
    }    
    
    
    protected Document getConfigDoc()
        throws ManagementException
    {
        if ( mConfigDoc == null )
        {
            try
            {
                String xmlConfigData = retrieveConfigurationDisplayData();

                if (xmlConfigData != null)
                {
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    dbf.setNamespaceAware(true);
                    DocumentBuilder db  = dbf.newDocumentBuilder();
                    //InputStream ios     = new StringBufferInputStream(xmlConfigData);
                    java.io.StringReader reader = new java.io.StringReader(xmlConfigData);
                    org.xml.sax.InputSource ipsrc = new org.xml.sax.InputSource(reader); 
                   
                    mConfigDoc = db.parse(ipsrc);
                    //ios.close();
                }
            }
            catch ( Throwable exception )
            {
                 try
                 {
                     String errMsg = mMsgBuilder.buildExceptionMessage("getConfigDoc",
                        exception);
                     throw new ManagementException(errMsg);
                 }
                 catch(ManagementException mex)
                 {
                     throw new ManagementException(exception);
                 }
            }
        }
        return mConfigDoc;
    }
    
    /**
     * Convert attribute list to a String value
     *
     * @param attribList the AttributeList to be converted
     * @return the list as string [ attribName, ..]
     */
    protected String attribListToString(AttributeList attribList)
    {
        StringBuffer strBuf = new StringBuffer();
        strBuf.append("[ ");
        int i = 0;
        for ( Object attrib : attribList )
        {
            i++;
            strBuf.append( ( (Attribute) attrib).getName() );
            if ( i != attribList.size() )
            {
                strBuf.append( ", ");
            }
        }
        strBuf.append(" ]");

        return strBuf.toString();
    }
    
    /**
     * Ensures that values of BOOLEAN application variable types are
     * either "true" or "false", and value of NUMBER types is a number
     *
     * @param type 
     * @param value
     */
    protected void checkAppVarValue(String name, String type, String value)
        throws javax.jbi.JBIException
    {
        if ( type == null || value == null )
        {
            return;
        }

        boolean isInvalidValue = false;

        if ( "BOOLEAN".equalsIgnoreCase(type) )
        {
            if ( !isBoolean(value))
            {
                isInvalidValue = true;
            }
        }
        else if ("NUMBER".equalsIgnoreCase(type) )
        {
            if ( !isNumber(value))
            {
                isInvalidValue = true;

            }
        }
        if ( isInvalidValue )
        {

            String[] params = new String[]{ name, value, type };
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_INVALID_APP_VAR_VALUE, params);

            String jbiMsg = mMsgBuilder.buildFrameworkMessage(
                "checkAppVarValue",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg),
                params, mMsgBuilder.getMessageToken(errMsg));
            throw new javax.jbi.JBIException(jbiMsg);
        }
    }

    /**
     * @return true if the value passed in is a number ( ex. 99, 99.99, +99, -99, 99.0 )
     *         false otherwise
     */
    protected boolean isNumber(String str)
    {
        String pStr = "[+|-]*\\d*[.]*[\\d]*";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(pStr);

        return p.matcher(str).matches();
    }
    
    /**
     * @return false if the passed in string is not a boolean string
     */
    protected boolean isBoolean(String str)
    {
        boolean isBoolean = true;
        if ( !(Boolean.FALSE.toString().equalsIgnoreCase(str)) &&
                !(Boolean.TRUE.toString().equalsIgnoreCase(str)))
        {
            isBoolean = false;
        }
        return isBoolean;
    }
}

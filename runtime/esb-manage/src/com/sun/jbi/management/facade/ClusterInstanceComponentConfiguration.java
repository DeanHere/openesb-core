/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.management.ComponentInfo.Variable;
import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.message.MessageHelper;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.xml.RegistryDocument;
import com.sun.jbi.management.system.ManagementException;
import com.sun.jbi.util.ComponentConfigurationHelper;
import java.io.InputStream;
import java.io.StringBufferInputStream;

import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.lang.reflect.Method;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanConstructorInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;


/**
 * The ComponentConfiguration is a dynamic facade MBean for component configuration.
 * This MBean provides a facade for managing component static, variable and
 * named configuration.
 *
 * This ComponentExtension MBean is registered for each installation of the component
 * on a target i.e it is target specific. 
 *
 * @author Sun Microsystems, Inc.
 */
public class ClusterInstanceComponentConfiguration
    extends ComponentConfiguration
    implements com.sun.jbi.management.ComponentConfiguration
{
    
    /** Clustered Instance Name */
    private String mInstanceName;
    
    /**
     * Create an instance of this class.
     * @param ctx - Environment Context
     * @param instanceName - clustered instance name
     * @param clusterName - parent cluster name
     * @param compName - component name
     * @param compType - component type
     * @throws com.sun.jbi.management.system.ManagementException 
     */
    public ClusterInstanceComponentConfiguration(
                com.sun.jbi.EnvironmentContext ctx, 
                String instanceName,
                String clusterName,
                String compName, 
                ComponentType compType) 
        throws ManagementException
    {       
        super(ctx, clusterName, compName, compType);
        
        if ( !mPlatform.isClusteredServer(instanceName) )
        {
            // -- target is not a clustered instance -- throw an exception
            String[] params = new String[]{instanceName};
            String responseMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_INVALID_CLUSTER_INST_NAME, params);
            String jbiMgtMsg = mMsgBuilder.buildFrameworkMessage(
                "ClusterInstanceComponentConfiguration[ctor]",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(responseMsg),
                params, mMsgBuilder.getMessageToken(responseMsg));
           throw new ManagementException(jbiMgtMsg);
            
        }
        
        mInstanceName = instanceName;
        
        mComponentName = compName;
        mComponentType = compType;
        mCfgHlpr = new ComponentConfigurationHelper();
        initPrimitiveTypeMap();
    }
       
    /*----------------------------------------------------------------------------------*\
     *                   javax.management.DynamicMBean Methods                          *
    \*----------------------------------------------------------------------------------*/

    /**
     * Obtain the value of a specific attribute of the Dynamic MBean.
     * @param attribute The name of the attribute to be retrieved
     * @return The value of the attribute retrieved.
     * @exception AttributeNotFoundException if an attribute is not found
     * 
     * @exception MBeanException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's getter.
     * @exception ReflectionException Wraps a <CODE>java.lang.Exception</CODE>
     *            thrown while trying to invoke the getter.
     * @see #setAttribute
     */
    public Object getAttribute(String attribute) 
        throws AttributeNotFoundException, MBeanException, ReflectionException 
    {
        try
        {
            
            if ( "ApplicationVariables".equals(attribute) ) 
            {
                return getApplicationVariables();
            }
            
            if ( "ApplicationConfigurations".equals(attribute) ) 
            {
                return getApplicationConfigurations();
            }
        
            attributeCheck(attribute);
     
            AttributeList attribList = getAttributes(new String[]{attribute});
        
            if ( !attribList.isEmpty() )
            {
                return ( (Attribute) attribList.get(0)).getValue();
            }
            else
            {
                // should never be the case
                return null;
            }
        }
        catch ( javax.jbi.JBIException jbiex )
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
    }

    /**
     * Set the value of a specific attribute of the Dynamic MBean.
     * @param attribute The identification of the attribute to
     *        be set and  the value it is to be set to.
     * @exception AttributeNotFoundException 
     * @exception InvalidAttributeValueException 
     * @exception MBeanException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's setter.
     * @exception ReflectionException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown while trying to invoke the MBean's setter.
     * @see #getAttribute
     */
    public void setAttribute(Attribute attribute) 
        throws AttributeNotFoundException, InvalidAttributeValueException, 
            MBeanException, ReflectionException 
    {   
        try
        {
            setConfigurationAttribute(attribute);
        }
        catch ( javax.jbi.JBIException jex )
        {
            mLog.fine(MessageHelper.getMsgString(jex));
            throw new MBeanException(jex);
        }
    }

    /**
     * Allows an action to be invoked on the Dynamic MBean.
     * 
     * @param actionName The name of the action to be invoked.
     * @param params An array containing the parameters to be set when the 
     *        action is invoked.
     * @param signature An array containing the signature of the action. The 
     *        class objects will be loaded through the same class loader as the
     *        one used for loading the MBean on which the action is invoked.
     * @return The object returned by the action, which represents the result of
     *         invoking the action on the MBean specified.
     * @exception MBeanException  Wraps a <CODE>java.lang.Exception</CODE> thrown
     *            by the MBean's invoked method.
     * @exception ReflectionException  Wraps a <CODE>java.lang.Exception</CODE> 
     *            thrown while trying to invoke the method
     */
    public Object invoke(String actionName, Object[] params, String[] signature) 
        throws MBeanException, ReflectionException 
    {
        try
        {
            if ( actionName.equals("setConfigurationAttribute"))
            {
                return setConfigurationAttribute( (Attribute) params[0]);
            }

            if ( actionName.equals("setConfigurationAttributes"))
            {
                return setConfigurationAttributes((AttributeList) params[0]);
            }
            
            if ( actionName.equals("addApplicationVariable") )
            {
                return addApplicationVariable((String) params[0],
                    (CompositeData) params[1]);
            }
            
            if ( actionName.equals("setApplicationVariable") )
            {
                return setApplicationVariable((String) params[0],
                    (CompositeData) params[1]);
            }
            
            if ( actionName.equals("deleteApplicationVariable") )
            {
                return deleteApplicationVariable((String) params[0]);
            }
            
            if ( actionName.equals("getApplicationVariables") )
            {
                return getApplicationVariables();
            }
            
            if ( actionName.equals("getApplicationConfigurations") )
            {
                return getApplicationConfigurations();
            }
            
            if ( actionName.equals("addApplicationConfiguration") )
            {
                if ( params[1] instanceof Properties )
                {
                    return addApplicationConfiguration((String) params[0],
                        (Properties) params[1]);
                }
                else
                {
                    return addApplicationConfiguration((String) params[0],
                        (CompositeData) params[1]);
                }
            }

            if ( actionName.equals("setApplicationConfiguration") )
            {
                if ( params[1] instanceof Properties )
                {
                    return setApplicationConfiguration((String) params[0],
                        (Properties) params[1]);
                }
                else
                {
                    return setApplicationConfiguration((String) params[0],
                        (CompositeData) params[1]);
                }
            }
            
            if ( actionName.equals("deleteApplicationConfiguration") )
            {
                return deleteApplicationConfiguration((String) params[0]);
            }
            
            if ( actionName.equals("queryApplicationConfigurationType") )
            {
                return queryApplicationConfigurationType();
            }
            
            if ( actionName.equals("retrieveConfigurationDisplaySchema"))
            {
                return retrieveConfigurationDisplaySchema();
            }
            
            if ( actionName.equals("retrieveConfigurationDisplayData"))
            {
                return retrieveConfigurationDisplayData();
            }
            
            if ( actionName.equals("isAppVarsSupported"))
            {
                return isAppVarsSupported();
            }
            
            if ( actionName.equals("isAppConfigSupported"))
            {
                return isAppConfigSupported();
            } 
            
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            throw new MBeanException(jbiex);
        }
        
        throw new UnsupportedOperationException();
    }

    /**
     * Sets the values of several attributes of the Dynamic MBean.
     * 
     * @param attributes A list of attributes: The identification of the
     *        attributes to be set and  the values they are to be set to.
     * @return The list of attributes that were set, with their new values.
     * @see #getAttributes
     */
    public AttributeList setAttributes(AttributeList attributes) 
    {
        try
        {
            setConfigurationAttributes(attributes);
            return attributes;
        }
        catch ( Exception ex )
        {
            mLog.fine(MessageHelper.getMsgString(ex));
            return new AttributeList();
        }
        
    }

    /**
     * Provides the exposed attributes and actions of the Dynamic MBean using 
     * an MBeanInfo object.
     * 
     * @return An instance of <CODE>MBeanInfo</CODE> allowing all attributes 
     *         and actions exposed by this Dynamic MBean to be retrieved.
     */
    public MBeanInfo getMBeanInfo() 
    {
        // Get the MbeanInfo from the actual component configuration MBean and
        // augment it with the facade configuration operations
        MBeanInfo mbeanInfo = getComponentMBeanInfo();
        MBeanInfo augInfo = mbeanInfo;
        
        try
        {
            Method[] methods = Class.forName("com.sun.jbi.management.ComponentConfiguration").getDeclaredMethods();
            MBeanOperationInfo[] opInfos = new MBeanOperationInfo[methods.length];
            int i = 0;
            for ( Method mtd : methods )
            {
                opInfos[i++] = new MBeanOperationInfo(mtd.getName(), mtd);
            }
            
            augInfo = new MBeanInfo(
                mbeanInfo.getClassName(),
                mbeanInfo.getDescription(),
                mbeanInfo.getAttributes(),
                mbeanInfo.getConstructors(),
                merge(mbeanInfo.getOperations(), opInfos),
                mbeanInfo.getNotifications());
        }
        catch (Exception ex)
        {
            mLog.fine(MessageHelper.getMsgString(ex));
        }        
        return augInfo;
    }

    /**
     * Get the values of several attributes of the Dynamic MBean.
     * 
     * @param attributes A list of the attributes to be retrieved.
     * @return The list of attributes retrieved.
     * @see #setAttributes
     */
    public AttributeList getAttributes(String[] attributes) 
    {
        AttributeList list = new AttributeList();
     
        try
        {
            instanceRunningCheck();
        }
        catch ( javax.jbi.JBIException jbiex )
        {
            throw new RuntimeException(jbiex);
        }
        
        try
        {
            attributeCheck(attributes);
        }
        catch ( Exception ex)
        {
            mLog.fine(MessageHelper.getMsgString(ex));
            return list;
        }
            
        // -- Call getAttributes on the instance MBean
        ObjectName configMBeanName = null;

        try
        {
            configMBeanName = getRemoteInstanceComponentConfigMBeanName(mInstanceName);

            AttributeList attribs =  (AttributeList) getRemoteAttributes(
                configMBeanName, attributes, mInstanceName);

            return attribs;
        }
        catch ( ManagementException mex)
        {
            // -- should never occur
            mLog.fine(MessageHelper.getMsgString(mex));

        }

        // No attributes in registry and instance MBean unavailable
        return list;
    }
    
    /*---------------------------------------------------------------------------------*\
     *          Operations for Static Configuration Management                         *
    \*---------------------------------------------------------------------------------*/
    /**
     * Set a single configuration attribute on a target.
     * @return a management message string with the status of the operation.
     * @param attrib - the configuration attribute to be set.
     * @throws javax.jbi.JBIException on errors
     */
    public String setConfigurationAttribute(Attribute attrib)
        throws javax.jbi.JBIException
    { 
        instanceRunningCheck();
        componentStartedOrStoppedOnTargetCheck();
        attributeCheck(attrib);
        String result = setConfigurationAttributeOnInstance(attrib, mInstanceName);
       
        return result;
    }
    
    /**
     * 
     * @param attribList List of configuration attributes to set
     * 
     * @return updated attribute list
     * @throws javax.jbi.JBIException on errors
     */
    public String setConfigurationAttributes(AttributeList attribList)
            throws javax.jbi.JBIException
    {
        instanceRunningCheck();
        componentStartedOrStoppedOnTargetCheck();
        attributeCheck(attribList);
        ComponentConfigurationResponse result = setConfigurationAttributesOnInstance(attribList, mInstanceName);
        return result.getResponseMessage();
    }
    
    /*---------------------------------------------------------------------------------*\
     *          Operations for Application Variables Management                        *
    \*---------------------------------------------------------------------------------*/
    /**
     * Detect the components support for application variables. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application variable management.
     *
     * @return true if the components configuration MBean implements all the
     *         operations for application variables.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppVarsSupported()  
        throws MBeanException
             
     {
        Boolean result = false;
        
        String ns = getComponentConfigurationNS(mComponentName);
        
        if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
        {
            /**
             * If it's the new schema this information can be obtained from 
             * the components configuration display data.
             */  
            try
            {
            
                Document configDoc = getConfigDoc();

                result = mCfgHlpr.isAppVarsSupported(configDoc);
            }
            catch ( ManagementException ex)
            {
                throw new MBeanException(new javax.jbi.JBIException(ex));
            }

        }
        else
        {        
            try
            {
                instanceRunningCheck();
                componentStartedOrStoppedOnTargetCheck();
            }
            catch ( javax.jbi.JBIException jbiex)
            {
                throw new MBeanException(jbiex);
            }
             
            MBeanInfo mbnInfo = getComponentMBeanInfo();
            
            if ( mbnInfo != null )
            {
        
                MBeanOperationInfo[] opInfos = mbnInfo.getOperations();

                int numOpsSupported = 0;
                for ( MBeanOperationInfo opInfo : opInfos )
                {
                    if ( opInfo.getName().equals("addApplicationVariable"))
                    {
                        MBeanParameterInfo[] params = opInfo.getSignature();

                        if ( params.length == 2 )
                        {
                            if ( params[0].getType().equals("java.lang.String") &&
                                 params[1].getType().equals("javax.management.openmbean.CompositeData") )
                            {
                                numOpsSupported++;
                            }
                        }
                    }
                    else if ( opInfo.getName().equals("setApplicationVariable"))
                    {
                        MBeanParameterInfo[] params = opInfo.getSignature();

                        if ( params.length == 2 )
                        {
                            if ( params[0].getType().equals("java.lang.String") &&
                                 params[1].getType().equals("javax.management.openmbean.CompositeData") )
                            {
                                numOpsSupported++;
                            }
                        }
                    }
                    else if ( opInfo.getName().equals("deleteApplicationVariable"))
                    {
                        MBeanParameterInfo[] params = opInfo.getSignature();

                        if ( params.length == 1 )
                        {
                            if ( params[0].getType().equals("java.lang.String") )
                            {
                                numOpsSupported++;
                            }
                        }
                    }
                }

                boolean isAppVarsAttribSupported = false;
                MBeanAttributeInfo[] attrInfos = mbnInfo.getAttributes();

                for ( MBeanAttributeInfo attrInfo : attrInfos )
                {
                    // -- Check if "ApplicationConfigurations" attribute is present
                    if ( attrInfo.getName().equals("ApplicationVariables"))
                    {
                        isAppVarsAttribSupported = true;
                    }
                } 

                result = ( numOpsSupported == 3 && isAppVarsAttribSupported); 
            }
        }
        return result;    
     }
     
    /**
     * This operation adds a new application variable. If a variable already exists with 
     * the same name as that specified then the operation fails.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste
     * @throws MBeanException if an error occurs in adding the application variables to the 
     *         component. 
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public String addApplicationVariable(String name, CompositeData appVar) 
        throws MBeanException
    {
        String result;
        mLog.fine("Adding application variable " + name);
        try
        {
            instanceRunningCheck();
            componentStartedOrStoppedOnTargetCheck();
            supportsAppVarsCheck();
            applicationVariableExistsCheck(name, false);
            applicationVariableCompositeIntegrityCheck(name, null, appVar, false);
            result = addApplicationVariableToInstance(name, appVar, mInstanceName); 
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
        return result;
    }
     
    /**
     * This operation sets an application variable. If a variable does not exist with 
     * the same name, its an error.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste to be updated.
     * @throws MBeanException if one or more application variables cannot be deleted
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public String setApplicationVariable(String name, CompositeData appVar) 
        throws MBeanException
    {
        String result;
        mLog.fine("Setting application variable " + name);
        try
        {
            instanceRunningCheck();
            componentStartedOrStoppedOnTargetCheck();
            supportsAppVarsCheck();
            String type = applicationVariableExistsCheck(name, true);
            applicationVariableCompositeIntegrityCheck(name, type, appVar, true);
            result = setApplicationVariableOnInstance(name, appVar, mInstanceName);
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
        return result;
    }
     
    /**
     * This operation deletes an application variable, if a variable with the specified name does
     * not exist, it's an error.
     *
     * @param name - name of the application variable
     * @throws MBeanException on errors.
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public String deleteApplicationVariable(String name) throws MBeanException
    {

        String result;
        mLog.fine("Deleting application variable " + name);
        try
        {
            instanceRunningCheck();
            componentStartedOrStoppedOnTargetCheck();
            supportsAppVarsCheck();
            applicationVariableExistsCheck(name, true);
            result = deleteApplicationVariableFromInstance(name, mInstanceName);
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
        return result;
    }
     
     /**
      * Get the Application Variable set for a component.
      *
      * @return  a TabularData which has all the applicationvariables set on the component. 
      */
     public TabularData getApplicationVariables()
     {        

        try
        {
            instanceRunningCheck();
            componentStartedOrStoppedOnTargetCheck();
        }
        catch ( javax.jbi.JBIException jbiex )
        {
            throw new RuntimeException(jbiex);
        }
        
        TabularData appVars = null;

        // -- Call getAttributes on the actual MBean, the application variables might not
        //    have been persisted
        // -- Call getAttributes on the instance MBean

        ObjectName configMBeanName = null;
        configMBeanName = getRemoteInstanceComponentConfigMBeanName(mInstanceName);

        if ( configMBeanName != null )
        {
            try
            {
                appVars =  (TabularData) getRemoteAttribute(configMBeanName, 
                            "ApplicationVariables", mInstanceName);
            }
            catch ( ManagementException mex)
            {
                mLog.fine(MessageHelper.getMsgString(mex));
                throw new RuntimeException(mex.getMessage());
            }
        }
        return appVars;
     }
     
    /**
     * Detect the components support for component configuration. This method
     * returns true if the component has a configuration MBean with configurable
     * attributes.
     *
     * @return true if the components configuration MBean has configurable 
     *         attributes.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isComponentConfigSupported()  
        throws MBeanException
     {
        Boolean result = false;
        
        String ns = getComponentConfigurationNS(mComponentName);
        
        if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
        {
            /**
             * If it's the new schema this information can be obtained from 
             * the components configuration display data.
             */  
            try
            {
                Document configDoc = getConfigDoc();
                result = mCfgHlpr.isComponentConfigSupported(configDoc);
            }
            catch ( ManagementException ex)
            {
                throw new MBeanException(new javax.jbi.JBIException(ex));
            }

        }
        else
        {
            try
            {
                instanceRunningCheck();
                componentStartedOrStoppedOnTargetCheck();
            }
            catch ( javax.jbi.JBIException jbiex)
            {
                throw new MBeanException(jbiex);
            }
            
            MBeanInfo mbnInfo = getComponentMBeanInfo();
            
            if ( mbnInfo != null )
            {
        
                MBeanAttributeInfo[] attrInfos = mbnInfo.getAttributes();

                int realAttrCount = 0;

                for ( MBeanAttributeInfo attrInfo : attrInfos )
                {
                    if ( !"ApplicationVariables".equals(attrInfo.getName()) &&
                         !"ApplicationConfigurations".equals(attrInfo.getName())  &&
                         attrInfo.isReadable() && attrInfo.isWritable() )
                    {
                        realAttrCount++;
                    }
                }

                result = ( realAttrCount > 0 );    
            }
        }
        return result;         
     }

    /*---------------------------------------------------------------------------------*\
     *            Operations for Application Configuration Management                  *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for application configuration. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application configuration management.
     *
     * @return true if the components configuration MBean implements all the
     *         operations for application configuration.
     * @throws MBeanException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppConfigSupported()  
        throws MBeanException
     {
        Boolean result = false;
        
        String ns = getComponentConfigurationNS(mComponentName);
        
        if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
        {
            /**
             * If it's the new schema this information can be obtained from 
             * the components configuration display data.
             */  
            try
            {
                Document configDoc = getConfigDoc();
                result = mCfgHlpr.isAppConfigSupported(configDoc);
            }
            catch ( ManagementException ex)
            {
                throw new MBeanException(new javax.jbi.JBIException(ex));
            }

        }
        else
        {
            try
            {
                instanceRunningCheck();
                componentStartedOrStoppedOnTargetCheck();
            }
            catch ( javax.jbi.JBIException jbiex)
            {
                throw new MBeanException(jbiex);
            }
            
            MBeanInfo mbnInfo = getComponentMBeanInfo();
            
            if ( mbnInfo != null )
            {

                MBeanOperationInfo[] opInfos = mbnInfo.getOperations();

                int numOpsSupported = 0;
                for ( MBeanOperationInfo opInfo : opInfos )
                {
                    if ( opInfo.getName().equals("addApplicationConfiguration"))
                    {
                        MBeanParameterInfo[] params = opInfo.getSignature();

                        if ( params.length == 2 )
                        {
                            if ( params[0].getType().equals("java.lang.String") &&
                                 params[1].getType().equals("javax.management.openmbean.CompositeData") )
                            {
                                numOpsSupported++;
                            }
                        }
                    }
                    else if ( opInfo.getName().equals("setApplicationConfiguration"))
                    {
                        MBeanParameterInfo[] params = opInfo.getSignature();

                        if ( params.length == 2 )
                        {
                            if ( params[0].getType().equals("java.lang.String") &&
                                 params[1].getType().equals("javax.management.openmbean.CompositeData") )
                            {
                                numOpsSupported++;
                            }
                        }
                    }
                    else if ( opInfo.getName().equals("deleteApplicationConfiguration"))
                    {
                        MBeanParameterInfo[] params = opInfo.getSignature();

                        if ( params.length == 1 )
                        {
                            if ( params[0].getType().equals("java.lang.String") )
                            {
                                numOpsSupported++;
                            }
                        }
                    }
                    else if ( opInfo.getName().equals("queryApplicationConfigurationType"))
                    {
                        MBeanParameterInfo[] params = opInfo.getSignature();

                        if ( params.length == 0 )
                        {
                            String returnType = opInfo.getReturnType();

                            if ( returnType.equals("javax.management.openmbean.CompositeType"))
                            {
                                numOpsSupported++;
                            }
                        } 
                    }
                    
                }

                boolean isAppConfigAttribSupported = false;
                MBeanAttributeInfo[] attrInfos = mbnInfo.getAttributes();

                for ( MBeanAttributeInfo attrInfo : attrInfos )
                {
                    // -- Check if "ApplicationConfigurations" attribute is present
                    if ( attrInfo.getName().equals("ApplicationConfigurations"))
                    {
                        isAppConfigAttribSupported = true;
                    }
                }

                result = ( numOpsSupported == 4 && isAppConfigAttribSupported); 
            }
        }
        return result;         
     }
     
     /**
      * Get the CompositeType definition for the components application configuration 
      *
      * @return the CompositeType for the components application configuration.
      */
     public CompositeType queryApplicationConfigurationType()
     {
        try
        {
            instanceRunningCheck();
        }
        catch ( javax.jbi.JBIException jbiex )
        {
            throw new RuntimeException(jbiex);
        }
         CompositeType appConfigType = null;
         
         ObjectName configMBeanName = null;

         configMBeanName = getRemoteInstanceComponentConfigMBeanName(mInstanceName);

         if ( configMBeanName != null )
         {
             try
             {
                appConfigType =  (CompositeType) invokeRemoteOperation(
                    configMBeanName, 
                    "queryApplicationConfigurationType",
                    new Object[]{},
                    new String[]{},
                    mInstanceName);
             }
             catch ( ManagementException mex)
             {
                 mLog.fine(MessageHelper.getMsgString(mex));
                 throw new RuntimeException(mex.getMessage());
             }
         }
         return appConfigType;
     }
     

     /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appCfgProps - application configuration properties
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String addApplicationConfiguration(String name, Properties appCfgProps) 
        throws MBeanException
     {
         mLog.fine(" adding application configuration " + appCfgProps.toString() +
            " to component " + mComponentName);
         
         String responseMsg = null;
         try
         {
             instanceRunningCheck();
             applicationConfigurationCompositeIntegrityCheck(name, appCfgProps, false);
             componentStartedOrStoppedOnTargetCheck();
             applicationConfigurationExistsCheck(name, false);

             CompositeType appCfgType = queryApplicationConfigurationType();

             if ( appCfgType != null )
             {
                // Convert properties to Compoiste Data and invoke the MBean operation
                CompositeData cd = mCfgHlpr.convertPropertiesToCompositeData(appCfgProps,
                    appCfgType);
                responseMsg = addApplicationConfigurationComposite(name, cd);
             }
             else
             {
                 String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_APP_CFG_TYPE_NULL,
                    mComponentName);
                 throw new Exception(errMsg);
             }
         }
         catch (javax.jbi.JBIException jbiex)
         {
             throw new MBeanException(jbiex);
         }
         catch ( Exception ex )
         {
             try
             {
                 String errMsg = mMsgBuilder.buildExceptionMessage("addApplicationConfiguration",
                    ex);
                 throw new MBeanException(new javax.jbi.JBIException(errMsg));
             }
             catch(ManagementException mex)
             {
                 mLog.fine(MessageHelper.getMsgString(mex));
             }
         }
         return responseMsg;
     }
     
     /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appConfig - application configuration composite 
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String addApplicationConfiguration(String name, CompositeData appConfig) 
        throws MBeanException
     {
        String result;
        try
        {
            instanceRunningCheck();
            applicationConfigurationCompositeIntegrityCheck(name, appConfig, false);
            componentStartedOrStoppedOnTargetCheck();
            applicationConfigurationExistsCheck(name, false);
            result = addApplicationConfigurationComposite(name, appConfig);
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            throw new MBeanException(jbiex);
        }
        return result;
     }
    
     /**
      * Delete an application configuration. 
      *
      * @param name - identification of the application configuration to be deleted
      * @throws MBeanException if the configuration cannot be deleted.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String deleteApplicationConfiguration(String name) throws MBeanException
     {
         
        String result;
        mLog.fine(" deleteing application configuration with name " + name);
        
        try
        {
            instanceRunningCheck();
            componentStartedOrStoppedOnTargetCheck();
            applicationConfigurationExistsCheck(name, true);
            result = deleteApplicationConfigurationFromInstance(name);
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            throw new MBeanException(jbiex);
        }
        return result;
     }
 
     /**
      * Set an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appCfgProps - application configuration properties
      * @throws MBeanException if the application configuration cannot be set.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String setApplicationConfiguration(String name, Properties appCfgProps) 
        throws MBeanException
     {
         mLog.fine(" setting application configuration " + appCfgProps.toString() +
            " on component " + mComponentName);
         
         String responseMsg = null;
         try
         {
             instanceRunningCheck();
             applicationConfigurationCompositeIntegrityCheck(name, appCfgProps, true);
             componentStartedOrStoppedOnTargetCheck();
             applicationConfigurationExistsCheck(name, true);

             CompositeType appCfgType = queryApplicationConfigurationType();

             if ( appCfgType != null )
             {
                // Convert properties to Compoiste Data and invoke the MBean operation
                CompositeData cd = mCfgHlpr.convertPropertiesToCompositeData(appCfgProps,
                    appCfgType);
                responseMsg = setApplicationConfigurationComposite(name, cd);
             }
             else
             {
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_APP_CFG_TYPE_NULL,
                    mComponentName);
                 throw new Exception(errMsg);
             }
         }
         catch ( Exception ex )
         {
             try
             {
                 String errMsg = mMsgBuilder.buildExceptionMessage("updateApplicationConfiguration",
                    ex);
                 throw new MBeanException(new javax.jbi.JBIException(errMsg));
             }
             catch(ManagementException mex)
             {
                 mLog.fine(MessageHelper.getMsgString(mex));
             }
         }
         return responseMsg;
     }
 
     
     /**
      * Update a application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "configurationName" in the appConfig
      * @param appConfig - application configuration composite
      * @throws MBeanException if there are errors encountered when updating the configuration.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public String setApplicationConfiguration(String name, CompositeData appConfig) 
        throws MBeanException
     {
        String result;
        try
        {
            instanceRunningCheck();
            applicationConfigurationCompositeIntegrityCheck(name, appConfig, true);
            componentStartedOrStoppedOnTargetCheck();
            applicationConfigurationExistsCheck(name, true);
            result = setApplicationConfigurationComposite(name, appConfig);
        }
        catch ( javax.jbi.JBIException jbiex)
        {
            throw new MBeanException(jbiex);
        }
        return result;
     }
    
    /**
     * Get a Map of all application configurations for the component.
     *
     * @return a TabularData of all the application configurations for a 
     *         component keyed by the configuration name. 
     */
    public TabularData getApplicationConfigurations()
    {
        try
        {
            instanceRunningCheck();
            canListAppConfigCheck();
        }
        catch ( javax.jbi.JBIException jbiex )
        {
            throw new RuntimeException(jbiex.getMessage());
        }

        TabularData appConfigs = null;
 
        // -- Call getAttribute on the instance MBean
       
        ObjectName configMBeanName = getRemoteInstanceComponentConfigMBeanName(mInstanceName);

        if ( configMBeanName != null )
        {
            try
            {
                appConfigs =  (TabularData) getRemoteAttribute(configMBeanName, 
                    "ApplicationConfigurations", mInstanceName);
            }
            catch ( ManagementException mex)
            {
                mLog.fine(MessageHelper.getMsgString(mex));
                throw new RuntimeException(mex.getMessage());
            }
        }
        return appConfigs;
    }
    
      
    /*---------------------------------------------------------------------------------*\
     *            Operations Component Configuration meta-data Management              *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * 
     * Retrieves the component specific configuration schema.
     * @return a String containing the configuration schema.
     * @throws javax.management.MBeanException on errors
     */
    public String retrieveConfigurationDisplaySchema()
        throws MBeanException
    {
        String response = null;
        
        try
        {
            String ns = getComponentConfigurationNS(mComponentName);

            if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
            {
                // New configuration schema
                response = getComponentConfigurationSchema(); 

            }  
            else
            {

                instanceRunningCheck();
                componentStartedOrStoppedOnTargetCheck();

                ObjectName configMBeanName = getRemoteInstanceComponentConfigMBeanName(mInstanceName);

                if ( configMBeanName != null )
                {
                    response =  (String) invokeRemoteOperation(
                        configMBeanName, 
                        "retrieveConfigurationDisplaySchema", 
                        new Object[]{}, 
                        new String[]{}, 
                        mInstanceName);
                }
            }
        }
        catch (javax.jbi.JBIException jbiex )
        {
            mLog.fine(MessageHelper.getMsgString(jbiex));
            throw new MBeanException(jbiex);
        }
        catch (Exception ex)
        {
            try
            {
                String errMsg = mMsgBuilder.buildExceptionMessage(
                            "retrieveConfigurationDisplaySchema", ex);
                Exception jbiEx = new javax.jbi.JBIException(errMsg); 
                mLog.fine(MessageHelper.getMsgString(jbiEx));
                throw new MBeanException(jbiEx);
            }
            catch( Exception exp )
            {
                mLog.fine(MessageHelper.getMsgString(exp));
                throw new MBeanException(ex);
            }
        }
        return response;
    }
    
    /**
     * 
     * Retrieves the component configuration metadata.
     * The XML data conforms to the component 
     * configuration schema.
     * @return a String containing the configuration metadata.
     * @throws javax.management.MBeanException on errors
     */
    public String retrieveConfigurationDisplayData()
        throws MBeanException
    {
        String response = null;
        
        String ns = getComponentConfigurationNS(mComponentName);
        
        if ( ComponentConfigurationHelper.CONFIGURATION_NS.equals(ns) )
        {
            // New configuration schema
            response = getComponentConfigurationData(mComponentName); 
            
        }
        else
        {
                    
            try
            {
                instanceRunningCheck();
                componentStartedOrStoppedOnTargetCheck();   
                ObjectName configMBeanName = getRemoteInstanceComponentConfigMBeanName(mInstanceName);

                if ( configMBeanName != null )
                {
                    response =  (String) invokeRemoteOperation(
                        configMBeanName, 
                        "retrieveConfigurationDisplayData", 
                        new Object[]{}, 
                        new String[]{}, 
                        mInstanceName);
                }
            }
            catch (javax.jbi.JBIException jbiex )
            {
                mLog.fine(MessageHelper.getMsgString(jbiex));
                throw new MBeanException(jbiex);
            }
            catch (Exception ex)
            {
                try
                {
                    String errMsg = mMsgBuilder.buildExceptionMessage(
                                "retrieveConfigurationDisplayData", ex);
                    Exception jbiEx = new javax.jbi.JBIException(errMsg); 
                    mLog.fine(MessageHelper.getMsgString(jbiEx));
                    throw new MBeanException(jbiEx);
                }
                catch( Exception exp )
                {
                    mLog.fine(MessageHelper.getMsgString(exp));
                    throw new MBeanException(ex);
                }
            }
        }
        return response;
    }
    
    /*---------------------------------------------------------------------------------*\
     *                                Private Helpers                                  *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Get the MBeanInfo from the actual component configuration MBean. If the component
     * is off-line, this information is not available and a blank MBeanInfo is returned.
     * If the component does not have a configuration MBean then a empty MBeanInfo is 
     * returned.
     */
    protected MBeanInfo getComponentMBeanInfo()
    {
         MBeanInfo mbeanInfo = null;
         boolean isInstanceUp = false;
         
         try
         {
             isInstanceUp = isInstanceRunning(mInstanceName);
         }
         catch ( ManagementException mex )
         {
             mLog.fine( MessageHelper.getMsgString(mex));
         }
         
         if ( isInstanceUp )
         {
             try
             {
                ObjectName cfgMBeanName = getComponentConfigurationMBeanName();
                if ( cfgMBeanName != null )
                {
                    mbeanInfo = getMBeanServerConnection(mInstanceName).
                                getMBeanInfo(cfgMBeanName);
                }
             }
             catch ( Exception ex )
             {
                 mLog.fine(MessageHelper.getMsgString(ex));
             }
         }
         
         if ( mbeanInfo == null )
         {
             // -- clustered instance is down or component does not have a
             //    registered config MBean, return empty MBeanInfo
             mbeanInfo = new MBeanInfo(this.getClass().getName(), 
                "Dynaic Component Configuration facade MBean",
                new MBeanAttributeInfo[]{},
                new MBeanConstructorInfo[]{},
                new MBeanOperationInfo[]{},
                new MBeanNotificationInfo[]{});
         }
         return mbeanInfo;
    }
    
    
    /**
     * Check if the attribute is valid i.e. the MBeanInfo of the 
     * ComponentConfigurationMBean has this attribute.
     *
     * @param attribute - the attribute to be checked
     * @throws javax.jbi.JBIException if the provided attribute
     *         is not present in the AttributeInfo
     */
    protected void attributeCheck(String attributeName)
        throws javax.jbi.JBIException
    {
        boolean attribFound = false;
        boolean valueValid = false;

        if (  isInstanceRunning(mInstanceName) )
        {
            MBeanAttributeInfo[] attribInfos = getMBeanInfo().getAttributes();
                   
            if ( attribInfos.length == 0 )
            {
                // At least one instance is up, but no attribute infos means
                // no configuration MBean
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_CCFG_NO_CONFIG_ATTRIBUTES, mComponentName);
                    
                String jbiMsg = mMsgBuilder.buildFrameworkMessage("attributeCheck", 
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                    mMsgBuilder.getMessageString(errMsg),
                    new String[]{mComponentName}, mMsgBuilder.getMessageToken(errMsg));
                    
                throw new javax.jbi.JBIException(jbiMsg);
            }
            
            for ( MBeanAttributeInfo attribInfo : attribInfos )
            {
                if ( attribInfo.getName().equals(attributeName))
                {
                    // Found an attribute with natching name
                    return;
                 
                }
            }
            
            String errMsg = mTranslator.getString(
                            LocalStringKeys.JBI_CCFG_ATTRIBUTE_NOT_FOUND, 
                            attributeName, 
                            mComponentName);
                            
            throw new javax.jbi.JBIException(errMsg);
        }   
    }      

     
    /**
     * Add the application configuration compoiste.
     */
     private String addApplicationConfigurationComposite(String name, CompositeData appConfig )
        throws javax.jbi.JBIException
     {
         String result = addApplicationConfigurationToInstance(name, appConfig);
         return result;
     }
     
    
    /**
     * If the instance is running, add the application config
     * 
     * @param name - name of the application configuration
     * @param appCfg - the application config being set     
     */
     private String addApplicationConfigurationToInstance(String name, 
             CompositeData appCfg)
        throws ManagementException
    {
        String responseMsg = null;
        
        if ( isInstanceRunning(mInstanceName) )
        {        
           invokeRemoteOperation(
                getRemoteInstanceComponentConfigMBeanName(mInstanceName),
                "addApplicationConfiguration", 
                new Object[]{name, appCfg}, 
                new String[]{"java.lang.String","javax.management.openmbean.CompositeData"}, 
                mInstanceName);
           
           responseMsg =  mMsgBuilder.buildFrameworkMessage("addApplicationConfiguration",
                MessageBuilder.TaskResult.SUCCESS);
        }        
        else
        {
            // Component is offline 
            String[] params = new String[]{mComponentName, mInstanceName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            responseMsg = mMsgBuilder.buildFrameworkMessage("addApplicationConfiguration",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));
        }
        return responseMsg;
    }
     

     
    /**
     * Set application configuration composite
     */
    private String setApplicationConfigurationComposite(String name, CompositeData appConfig)
        throws javax.jbi.JBIException
    {
        String result = setApplicationConfigurationOnInstance(name, appConfig);
        
        return result;
    }
    
    
    /**
     * If the instance is running, set the application config
     * 
     * @param name - name of the application configuration
     * @param appCfg - the application config being set     
     */
     private String setApplicationConfigurationOnInstance(String name, CompositeData appCfg)
        throws ManagementException
    {
        String responseMsg = null;
        
        if ( isInstanceRunning(mInstanceName) )
        {        
           invokeRemoteOperation(
                getRemoteInstanceComponentConfigMBeanName(mInstanceName),
                "setApplicationConfiguration", 
                new Object[]{name, appCfg}, 
                new String[]{"java.lang.String","javax.management.openmbean.CompositeData"}, 
                mInstanceName);
           
           responseMsg =  mMsgBuilder.buildFrameworkMessage("setApplicationConfiguration",
                MessageBuilder.TaskResult.SUCCESS);
        }        
        else
        {
            // Component is offline 
            String[] params = new String[]{mComponentName, mInstanceName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            responseMsg = mMsgBuilder.buildFrameworkMessage("setApplicationConfiguration",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));
        }
        return responseMsg;
    }
    
    /**
     * If the instance is running, delete the application config
     * 
     * @param name - name of the application configuration
     * @param appCfg - the application config being set     
     */
     private String deleteApplicationConfigurationFromInstance(String name)
        throws ManagementException
    {
        String responseMsg = null;
        
        if ( isInstanceRunning(mInstanceName) )
        {        
           invokeRemoteOperation(
                getRemoteInstanceComponentConfigMBeanName(mInstanceName),
                "deleteApplicationConfiguration", 
                new Object[]{name}, 
                new String[]{"java.lang.String"}, 
                mInstanceName);
           
           responseMsg =  mMsgBuilder.buildFrameworkMessage("deleteApplicationConfiguration",
                MessageBuilder.TaskResult.SUCCESS);
        }        
        else
        {
            // Component is offline 
            String[] params = new String[]{mComponentName, mInstanceName};
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_CCFG_COMPONENT_INSTANCE_DOWN, params);
            responseMsg = mMsgBuilder.buildFrameworkMessage("deleteApplicationConfiguration",
                com.sun.jbi.management.message.MessageBuilder.TaskResult.FAILED,
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg),
                    params, mMsgBuilder.getMessageToken(errMsg));
        }
        return responseMsg;
    }
    
    /** 
     * Get the JMX ObjectName of the actual Component Configuration MBean. 
     * 
     * @return the object name of the configuration MBean for the component.
     *         Look for the cascaded MBean with the standard name first, 
     *         if one is not found then look for the com.sun.ebi MBean 
     */
    private ObjectName getComponentConfigurationMBeanName()
    {
        ObjectName configMBeanName = null;
        
        try
        {
            /**
             * First check if there is a match for a custom component MBean which uses the
             * following ObjectName pattern : 
             *    com.sun.jbi:ControlType=Configuration,ComponentName=<component-name>
             */
            ObjectName standardPattern = mEnvCtx.getMBeanNames().
                getCustomComponentMBeanNameFilter(
                    mInstanceName, 
                    // CustomControlName="Configuration" 
                    "Configuration", 
                    mComponentName);

            Set<ObjectName> names = getMBeanServerConnection(mInstanceName).queryNames(standardPattern, null);

            if ( !names.isEmpty() )
            {
                if ( names.size() > 1 )
                {
                    mLog.finest("More than one MBean matches ObjectName pattern "
                        + standardPattern + ".  " + convertToString(names));
                }

                configMBeanName = (ObjectName) names.iterator().next();
            }
            else
            {
                /**
                 * If a custom component configuration MBean does not follow the standard 
                 * naming convention ( which the open-jbi components don't right now ) then
                 * look for the specific MBean for the component :
                 *   com.sun.ebi:ServiceType=Configuration,IdentificationName=<component-name>,* 
                 */
                ObjectName ebiPattern = null;

                    ebiPattern = new ObjectName(
                        "com.sun.ebi:ServiceType=Configuration,IdentificationName="+ mComponentName + ",*");
                    Set<ObjectName> ebiNames = getMBeanServerConnection(mInstanceName).queryNames(ebiPattern, null);

                    if ( !ebiNames.isEmpty() )
                    {
                        if ( ebiNames.size() > 1 )
                        {
                            mLog.finest("More than one MBean matches ObjectName pattern "
                                + standardPattern + ".  " + convertToString(ebiNames));
                        }

                        configMBeanName = (ObjectName) ebiNames.iterator().next();
                    }
            }

        }
        catch ( Exception ex)
        {
            mLog.fine(MessageHelper.getMsgString(ex));
        }
        return configMBeanName;
    }
    
    
    /**
     * @return true if the MBean Name is that of a standard jbi MBean.
     */
    boolean isStandardCascadedConfigMBean(ObjectName mbnName)
    {
        return ( "com.sun.jbi".equals(mbnName.getDomain()) );
    }
    
    /**
     * @throws javax.jbi.JBIException if the instance is not running.
     */
    private void instanceRunningCheck()
        throws javax.jbi.JBIException
    {
        super.instanceRunningCheck(mInstanceName);
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ServiceAssemblyState;
import com.sun.jbi.ServiceAssemblyQuery;
import com.sun.jbi.ServiceAssemblyInfo;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ServiceUnitState;

import com.sun.jbi.management.ComponentMessageHolder;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.ee.ApplicationInterceptor;
import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.repository.Archive;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.repository.RepositoryException;
import com.sun.jbi.management.message.MessageHelper;
import com.sun.jbi.management.system.ManagementException;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.jbi.JBIException;
import javax.jbi.management.DeploymentServiceMBean;
import javax.management.ObjectName;

/**
 * The deployment service MBean allows administrative tools to manage
 * service assembly deployments. The tasks supported are:
 * <ul>
 *   <li>Deploying a service assembly.</li>
 *   <li>Undeploying a previously deployed service assembly.</li>
 *   <li>Querying deployed service assemblies:
 *     <ul>
 *       <li>For all components in the system.</li>
 *       <li>For a particular component.</li>
 *     </ul>
 *   </li>
 *   <li>Control the state of deployed service assemblies:
 *     <ul>
 *       <li>Start the service units that contained in the SA.</li>
 *       <li>Stop the service units that contained in the SA. </li>
 *       <li>Shut down the service units that contained in the SA.</li>
 *     </ul>
 *   </li>
 *   <li>Query the service units deployed to a particular component.</li>
 *   <li>Check if a service unit is deployed to a particular component.</li>
 *   <li>Query the deployment descriptor for a particular service assembly.</li>
 *   <li>Query the deployment unit for a particular service unit.</li>
 * </ul>
 * 
 * @author Sun Microsystems, Inc.
 */
public class DeploymentService 
    extends Facade
    implements com.sun.jbi.management.DeploymentServiceMBean
{
    /** Component name for the Java EE Service Engine. */
    private static final String JAVA_EE_SE = "sun-javaee-engine";
    
    /** Constants for operation names */
    private static final String DEPLOY          = "deploy";
    private static final String UNDEPLOY        = "undeploy";
    private static final String START           = "start";
    private static final String STOP            = "stop";
    private static final String SHUT_DOWN       = "shutDown";
    private static final String GET_SA_DESCRIPTOR   = "ServiceAssemblyDescriptor";
    private static final String GET_SU_DESCRIPTOR   = "ServiceUnitDescriptor";
    
    /** Constants for attribute names */
    private static final String CURRENT_STATE   = "CurrentState";
    
    /** Reference to platform JavaEE Deployer */
    private ApplicationInterceptor mInterceptor;
    
    
    public DeploymentService(EnvironmentContext ctx, String target)
        throws ManagementException
    {
        super(ctx, target);
        
        // We should get the ApplicationInterceptor reference from PlatformContext 
        // instead of instantiating it directly here.
        mInterceptor = new com.sun.jbi.management.ee.JavaEEApplicationInterceptor(ctx);
    }
    


    /**
     * Deploys the given Service Assembly to the JBI environment.
     * <p>
     * Note that the implementation must not automatically start the service 
     * assembly after deployment; it must wait for the {@link #start(String)}
     * method to be invoked by the administrative tool.
     *
     * </br>
     * For the domain target, deploy only validates the service assembly and then
     * adds it to the Registry / Repository.
     * 
     * @param serviceAssemblyZipUrl String containing the location URL of the
     *        Service Assembly ZIP file; must be non-null, non-empty, and a
     *        legal URL
     * @return Result/Status of the current deployment; must conform to 
     *         JBI management result/status XML schema; must be non-null and
     *         non-empty
     * @exception Exception if complete deployment fails      
     */
    public String deploy(String serviceAssemblyZipUrl) 
        throws Exception
    {
        try
        {
            return deployServiceAssembly(serviceAssemblyZipUrl);
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }
    
    
    private String deployServiceAssembly(String serviceAssemblyZipUrl)
        throws ManagementException
    {
        boolean wasPreRegistered = false;
        
        Archive saArchive = validateArchive(serviceAssemblyZipUrl, 
                ArchiveType.SERVICE_ASSEMBLY, DEPLOY);

        String saName = 
                saArchive.getJbiXml(false).getServiceAssembly().getIdentification().getName();
            
            
        if ( mTarget.equals(DOMAIN))
        {
            serviceAssemblyRegistrationCheck(DEPLOY, saName, false);  
            addServiceAssemblyToDomain(DEPLOY, saName, saArchive);

            performJavaEEProcessing(saName, DEPLOY);
            
            return mMsgBuilder.buildFrameworkMessage(DEPLOY,
                MessageBuilder.TaskResult.SUCCESS);
        }
        else
        {
            /** -- if target anything other than domain
            (a) Service Assembly  maybe registered
            (b) If already registered make sure archives are identical
            (c) If not registered - register it
             **/
            boolean isRegistered = false;
            try
            {
                isRegistered = getGenericQuery().isServiceAssemblyRegistered(saName);
            }
            catch (RegistryException rex)
            {
                mLog.fine(MessageHelper.getMsgString(rex));
                String errMsg = mMsgBuilder.buildExceptionMessage("deployServiceAssembly",
                    rex);
                throw new ManagementException(errMsg);
            }
            if ( !isRegistered )
            {
                addServiceAssemblyToDomain(DEPLOY, saName, saArchive);
            }   
            else
            {
                File newFile = new File(serviceAssemblyZipUrl);
                File domainFile = new File(getRegistry().getRepository().findArchive(
                    ArchiveType.SERVICE_ASSEMBLY, saName));
                archiveEqualityCheck(newFile, domainFile, ArchiveType.SERVICE_ASSEMBLY, saName);
                wasPreRegistered = true;
            }

            // -- Deploy to Target
            try
            {
                return deployServiceAssemblyToTarget(saName, serviceAssemblyZipUrl);
            }
            catch ( ManagementException mex )
            {
                // Complete failure : remove the SA from reg/rep if was added 
                // with this deploy
                if ( !wasPreRegistered )
                {
                    removeServiceAssemblyFromDomain(DEPLOY, saName);
                }
                throw mex;
            }
        }
    }

    /**
     * Undeploys the given Service Assembly from the JBI environment.
     *
     * </br>
     * For the domain target, undeploy removes the service assembly from the 
     * registry / repository only if the service assembly is not deployed on
     * any servers / clusters.
     * 
     * </br>
     * For target other than the domain, the Service Assembly is undeployed 
     * first. If the service assembly is not deployed on any server / cluster 
     * after this action, then the service assembly is removed.
     *
     * </br>
     * 
     * @param serviceAssemblyName name of the Service Assembly that is to be 
     *        undeployed; must be non-null and non-empty
     * @return Result/Status of the current undeployment; must conform to 
     *         JBI management result/status XML schema; must be non-null and
     *         non-empty
     * @exception Exception if compelete undeployment fails      
     */
    public String undeploy(String serviceAssemblyName) throws Exception
    {
        // default : force = false, keep = false
        return undeploy(serviceAssemblyName, false, false);
    }

    /*----------------------------------------------------------------------------------*\
     *                             SA LifeCycle Operations                              *
    \*----------------------------------------------------------------------------------*/
    
     /**
     * Start the service assembly. This puts the assembly into the {@link 
     * #STARTED} state. 
     * 
     * @param serviceAssemblyName name of the assembly to be started; must be
     *        non-null and non-empty
     * @return result / status string giving the results of starting (and 
     *         possibly initializing) each service unit in the assembly; must
     *         be non-null and non-empty
     * @exception Exception if there is no such assembly
     * @exception Exception if the assembly fails to start
     *         
     */
    public String start(String serviceAssemblyName) throws Exception
    {
        try
        {   
            serviceAssemblyName = serviceAssemblyName.trim();
            
            String response = null;
            if ( DOMAIN.equals(mTarget))
            {
                throwNotSupportedManagementException(START);
            }
            else
            {
                
                // Check : Service Assembly should be deployed on target
                serviceAssemblyDeploymentCheck(serviceAssemblyName);
        
                // give ApplicationInterceptor a chance to process start command
                performJavaEEProcessing(serviceAssemblyName, START);
                response = startServiceAssemblyOnTarget(serviceAssemblyName);
            }
            return response;
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }

    /**
     * Stop the service assembly. This puts the assembly into the {@link 
     * #STOPPED} state. 
     * 
     * @param serviceAssemblyName name of the assembly to be stopped; must be 
     *        non-null and non-empty
     * @return result / status string giving the results of stopping each 
     *         service unit in the assembly; must be non-null and non-empty
     * @exception Exception if there is no such assembly
     * @exception Exception if the assembly fails to stop
     */
    public String stop(String serviceAssemblyName) throws Exception
    {
        try
        {
            String response = null;
            if ( DOMAIN.equals(mTarget))
            {
                throwNotSupportedManagementException(STOP);
            }
            else
            {                
                // Check : Service Assembly should be deployed on target
                serviceAssemblyDeploymentCheck(serviceAssemblyName);

                // give ApplicationInterceptor a chance to process stop command
                performJavaEEProcessing(serviceAssemblyName, STOP);
                response = stopServiceAssemblyOnTarget(serviceAssemblyName);
            }
            return response;
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }        

    /**
     * Shut down the service assembly. This puts the assembly back into the
     * {@link #SHUTDOWN} state.
     * 
     * @param serviceAssemblyName name of the assembly to be shut down; must be
     *        non-null and non-empty
     * @return result / status string giving the results of shutting down  
     *         each service unit in the assembly; must be non-null and non-empty
     * @exception Exception if there is no such assembly
     * @exception Exception if the assembly fails to shut down
     */
    public String shutDown(String serviceAssemblyName) throws Exception
    {
        // default force = false
        return shutDown(serviceAssemblyName, false);
    }        

    
    /*----------------------------------------------------------------------------------*\
     *                                 Query Operations                                 *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Returns <code>true</code> if the the given component accepts the 
     * deployment of service units. This is used by admin tools to 
     * determine which components can be named in service assembly
     * deployment descriptors.
     * 
     * @param componentName name of the component; must be non-null and 
     *        non-empty
     * @return <code>true</code> if the named component accepts deployments;
     *         <code>false</code> if the named component does not accept
     *         deployments or it does not exist
     *
     */
    public boolean canDeployToComponent(String componentName)
    {
        boolean canDeploy = false;
        
        try
        {
            if ( DOMAIN.equals(mTarget))
            {
                canDeploy = false;
            }
            else if (isComponentInstalled(componentName) && 
                    mEnvCtx.isStartOnDeployEnabled())
            {
                //components are started automatically if start-on-deploy is true
                canDeploy = true;
            }
            else 
            {
                if ( mPlatform.isStandaloneServer(mTarget) )
                {
                    canDeploy =  ComponentState.STARTED.equals(
                        getComponentState(componentName, mTarget));
                }
                else
                {
                    ComponentType compType = getComponentType(componentName);

                    if ( compType != null )
                    {
                        ObjectName facadeComponentLCMBean = null;                      
                        
                        facadeComponentLCMBean = mMBeanNames.getComponentMBeanName(
                                componentName, compType,
                                MBeanNames.ComponentServiceType.ComponentLifeCycle, mTarget);

                        try
                        {
                            String state = (String) mMBeanSvr.getAttribute(
                                    facadeComponentLCMBean, CURRENT_STATE);

                            ComponentState effState = ComponentState.valueOfLifeCycleState(state);

                            if ( effState == ComponentState.STARTED )
                            {
                                canDeploy = true;
                            }
                        }
                        catch ( Exception ex )
                        {
                            mLog.fine(MessageHelper.getMsgString(ex));
                            canDeploy = false;
                        }
                    }
                }
            }
        }
        catch ( ManagementException mex )
        {
            mLog.fine(MessageHelper.getMsgString(mex, mLog.getLevel()));
            canDeploy = false;
        }
        return canDeploy;
    }


    
    /**
     * Returns an array of service unit names that are currently deployed to 
     * the named component.
     * 
     * @param componentName the name of the component to query; must be 
     *        non-null and non-empty
     * @return array of service unit names deployed in the named component;
     *         must be non-null; may be empty
     * @exception Exception if a processing error occurs
     */
    public String[] getDeployedServiceUnitList(String componentName) throws Exception
    {
        ComponentInfo compInfo = getComponentQuery().getComponentInfo(componentName);

        List<String> sus = new java.util.ArrayList();

        if ( compInfo != null )
        {
            List<ServiceUnitInfo> suList = compInfo.getServiceUnitList();

            for ( ServiceUnitInfo suInfo : suList)
            {
                sus.add(suInfo.getName());
            }
        }

        return sus.toArray(new String[sus.size()]);            
    }

    /**
     * Returns a list of Service Assemblies deployed to the JBI environment. 
     * 
     * @return list of Service Assembly names; must be non-null; may be
     *         empty
     * @exception Exception if a processing error occurs
     */
    public String[] getDeployedServiceAssemblies() throws Exception
    {
            java.util.List<String> saList = getServiceAssemblyQuery().getServiceAssemblies();

            return saList.toArray(new String[saList.size()]);
    }

    /**
     * Returns the deployment descriptor of the Service Assembly that was 
     * deployed to the JBI enviroment, serialized to a <code>String</code>.
     * 
     * @param serviceAssemblyName name of the service assembly to be queried;
     *        must be non-null and non-empty
     * @return descriptor of the Assembly Unit; must be non-null
     * @exception Exception if a processing error occurs
     */
    public String getServiceAssemblyDescriptor(String serviceAssemblyName) throws Exception
    {
        try
        {
            serviceAssemblyRegistrationCheck(GET_SA_DESCRIPTOR, 
                serviceAssemblyName, true);
            String jbiXml = null;
            try
            {
                jbiXml = getGenericQuery().getServiceAssemblyDeploymentDescriptor(serviceAssemblyName);
            }
            catch ( com.sun.jbi.management.registry.RegistryException rex )
            {
                String response = mMsgBuilder.buildExceptionMessage(
                    GET_SA_DESCRIPTOR, rex);
                throw new ManagementException(response);
            }

            if ( jbiXml == null )
            {
               String[] params = new String[]{serviceAssemblyName};
               mMsgBuilder.throwManagementException(GET_SA_DESCRIPTOR, 
                   LocalStringKeys.JBI_ADMIN_FAILED_GET_SERVICE_ASSEMBLY_DESCRIPTOR, params);
            }

            return jbiXml;
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }

    /**
     * Returns the deployment descriptor of the Service Unit that was 
     * deployed to the JBI enviroment, serialized to a <code>String</code>.
     * 
     * @param serviceAssemblyName name of the service assembly to be queried;
     *        must be non-null and non-empty
     * @param serviceUnitName name of the service unit to be queried;
     *        must be non-null and non-empty
     * @return descriptor of the Service Unit; must be non-null
     * @exception Exception if a processing error occurs
     */
    public String getServiceUnitDescriptor(String serviceAssemblyName, String serviceUnitName)
		throws Exception
    {
        try
        {
            serviceAssemblyRegistrationCheck(GET_SA_DESCRIPTOR, 
                serviceAssemblyName, true);
            String jbiXml = null;
            try
            {
                jbiXml = getGenericQuery().getServiceUnitDeploymentDescriptor(serviceAssemblyName, serviceUnitName);
            }
            catch ( com.sun.jbi.management.registry.RegistryException rex )
            {
                String response = mMsgBuilder.buildExceptionMessage(
                    GET_SU_DESCRIPTOR, rex);
                throw new ManagementException(response);
            }

            if ( jbiXml == null )
            {
               String[] params = new String[]{serviceAssemblyName, serviceAssemblyName};
               mMsgBuilder.throwManagementException(GET_SU_DESCRIPTOR, 
                   LocalStringKeys.JBI_ADMIN_FAILED_GET_SERVICE_UNIT_DESCRIPTOR, params);
            }

            return jbiXml;
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }

    /**
     * Returns an array of Service Assembly names, each service assembly in the
     * returned list has a service unit deployed to the component
     * 
     * @param componentName name of the component to query; must be non-null
     *        and non-empty
     * @return array of of Service Assembly names, where each assembly contains
     *         a Service Unit for the named component; must be non-null; may
     *         be empty
     * @exception Exception if a processing error occurs
     */
    public String[] getDeployedServiceAssembliesForComponent(String componentName) 
        throws Exception
    {
        if ( DOMAIN.equals(mTarget) )
        {
            List<String> allTargets = getAllTargets();
            List<String> allSAs = new ArrayList();
            for ( String target : allTargets )
            {
                String[] currentSAs = getDeployedServiceAssembliesForComponent(target, componentName);
                
                for ( String sa : currentSAs )
                {
                    if ( !allSAs.contains(sa))
                    {
                        allSAs.add(sa);
                    }
                } 
            }
            
            
            return allSAs.toArray(new String[allSAs.size()]);
        }
        else
        {
            return getDeployedServiceAssembliesForComponent(mTarget, componentName);
        }
    }
    
    /**
     * Get the Service Assembly name for a specific service unit deployed to a 
     * specific component.
     * 
     * @param serviceUnitName - service unit identifier
     * @param componentName - component identifier
     * @return the parent service assembly name for the specified service unit
     * deployed to the specific component. If the service unit is not
     * deployed a null value is returned.
     */
    public String getServiceAssemblyName(String serviceUnitName, String componentName) 
            throws Exception{
        
        String serviceAssemblyName = null;
        if ( DOMAIN.equals(mTarget) )
        {
            // This cannot be supported for target=domain, since we are
            // looking for depployed service unit information
            throwNotSupportedManagementException("getServiceAssemblyName");
        }
        else
        {
            ComponentInfo compInfo = getComponentQuery().getComponentInfo(componentName);

            if ( compInfo != null )
            {
                List<ServiceUnitInfo> suList = compInfo.getServiceUnitList();

                for ( ServiceUnitInfo suInfo : suList)
                {
                    if ( suInfo.getName().equals(serviceUnitName) )
                    {
                        serviceAssemblyName = suInfo.getServiceAssemblyName();
                    }
                }
            }
        }
        return serviceAssemblyName;
    }
    
    /**
     * Returns an array of component names, where for each the given assembly 
     * contains a service unit for the component. 
     * 
     * @param serviceAssemblyName the service assembly to be queried; must be
     *        non-null and non-empty
     * @return array of component names, where for each name the given assembly
     *         contains a service unit from the given service assembly; must
     *         be non-null; may be empty
     * @exception Exception if a processing error occurs
     */
    public String[] getComponentsForDeployedServiceAssembly(String serviceAssemblyName) 
        throws Exception
    {
        List<String> 
            allComponents = getComponentQuery().getComponentIds(ComponentType.BINDINGS_AND_ENGINES);

        List<String> comps = new java.util.ArrayList();

        for (String compName : allComponents )
        {
            ComponentInfo compInfo = getComponentQuery().getComponentInfo(compName);

            if ( compInfo != null )
            {
                List<ServiceUnitInfo> suList = compInfo.getServiceUnitList();

                for ( ServiceUnitInfo suInfo : suList)
                {
                    if ( suInfo.getServiceAssemblyName().equals(serviceAssemblyName) )
                    {
                        comps.add(compName);
                    }
                }
            }
        }
        return comps.toArray(new String[comps.size()]);
    }

    /**
     * Queries if the named Service Unit is currently deployed to the named
     * component.
     * 
     * @param componentName name of the component to query; must be non-null
     *        and non-empty
     * @param serviceUnitName name of the subject service unit; must be non-null
     *        and non-empty
     * @return <code>true</code> if the named service unit is currently deployed
     *         to the named component
     */
    public boolean isDeployedServiceUnit(String componentName, String serviceUnitName) 
        throws Exception
    {
        boolean isDeployed = false;

        ComponentInfo compInfo = getComponentQuery().getComponentInfo(componentName);

        if ( compInfo != null )
        {
            List<ServiceUnitInfo> suList = compInfo.getServiceUnitList();

            for ( ServiceUnitInfo suInfo : suList)
            {
                if ( suInfo.getName().equals(serviceUnitName) )
                {
                    isDeployed = true;
                }
            }
        }

        return isDeployed;
    }

   
    /**
     * Get the running state of a service assembly. The possible result values
     * of this query are enumerated by the following set of constants:
     * {@link #SHUTDOWN}, {@link #STOPPED}, {@link #STARTED}.
     * 
     * @param serviceAssemblyName name of the assembly to query; must be 
     *        non-null and non-empty
     * @return the state of the service assembly, as a string value; must be one
     *         of the enumerated string values provided by this interface:
     *         {@link #SHUTDOWN}, {@link #STOPPED}, or {@link #STARTED}
     * @exception Exception if there is no such assembly
     */
    public String getState(String serviceAssemblyName) throws Exception
    {
        try
        {

            if ( DOMAIN.equals(mTarget))
            {
                return getStateFromAllTargets(serviceAssemblyName);
            }
            else
            {
                return getStateFromTarget(serviceAssemblyName);
            }
            
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }
    
    /*---------------------------------------------------------------------------------*\
     *                      Extended DeploymentServiceMBean ops                        *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Get the state of a service unit deployed to a component. 
     * </br>
     * If the service unit is deployed to the component then one of the states :
     * "Started", "Stopped" or "Shutdown" is returned. 
     *
     * @return the actual state of the deployed service unit or "Unknown". 
     * @param componentName - the component name
     * @param serviceUnitName - name of the service unit
     * @exception javax.jbi.JBIException if the component is not installed or if the 
     * service unit is not deployed to the component.
     * @see javax.jbi.management.DeploymentServiceMBean
     */
    public String getServiceUnitState(String componentName, String serviceUnitName) 
        throws javax.jbi.JBIException
    {
        try
        {
            if ( DOMAIN.equals(mTarget))
            {
                return getServiceUnitStateFromAllTargets(componentName, serviceUnitName);
            }
            else
            {
                return getServiceUnitStateFromTarget(componentName, serviceUnitName);
            }
            
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }
    
    
    /**
     * Deploy a Service Assembly from the Repository. 
     * <br/>
     * If the service assembly is not there in the DAS repository a exception will
     * be thrown. 
     * 
     * @param serviceAssemblyName - name of the registered service assembly.
     * @return a status management message with details on the service assembly deployment.
     * @exception javax.jbi.JBIException if the service assembly is not registered or
     * deploy fails.
     */
     public String deployFromRepository(String serviceAssemblyName)
        throws javax.jbi.JBIException
     { 
        try
        {
            if ( DOMAIN.equals(mTarget))
            {
                throwNotSupportedManagementException("deployFromRepository()");
            }
                    
            serviceAssemblyRegistrationCheck(DEPLOY, serviceAssemblyName, true); 

            Archive saArchive = getRegistry().getRepository().getArchive(ArchiveType.SERVICE_ASSEMBLY, 
                serviceAssemblyName);
            String serviceAssemblyZipUrl = saArchive.getPath();
            mLog.fine("Deploying service assembly from repository "
                + serviceAssemblyZipUrl + " to target " + mTarget);  

            return deployServiceAssemblyToTarget(serviceAssemblyName, serviceAssemblyZipUrl);
        }
        catch ( ManagementException mex)
        {
            throw new javax.jbi.JBIException(mex.getMessage());
        }
       
     }
     
    /**
     * Undeploys the given Service Assembly from the target. If the service assembly is 
     * not deployed on any targets after undeploying from this target the service assembly
     * is removed from the domain.
     * 
     * @param serviceAssemblyName name of the Service Assembly that is to be 
     *        undeployed; must be non-null and non-empty
     * @param force if this flag is set to true any undeployment errors thrown 
     * from the component will be ignored as the service assembly is forcefully
     * undeployed.
     * @return Result/Status of the current undeployment; must conform to 
     *         JBI management result/status XML schema; must be non-null and
     *         non-empty
     * @exception Exception if compelete undeployment fails      
     */
    public String undeploy(String serviceAssemblyName, boolean force) throws Exception
    {
        // keep flag : false by default
        return undeploy(serviceAssemblyName, force, false);
    }
    
    
    /**
     * Undeploys the given Service Assembly from the JBI environment.
     * 
     * @param serviceAssemblyName name of the Service Assembly that is to be 
     *        undeployed; must be non-null and non-empty
     * @param force if this flag is set to true any undeployment errors thrown 
     *        from the component will be ignored as the service assembly is forcefully
     *        undeployed.
     * @param keep if true the service assembly archive should not be removed
     *        from the domain. If false the archive is removed if the service 
     *        assembly is not deployed on any instances.
     * @return Result/Status of the current undeployment; must conform to 
     *         JBI management result/status XML schema; must be non-null and
     *         non-empty
     * @exception Exception if compelete undeployment fails      
     */
    public String undeploy(String serviceAssemblyName, boolean force, boolean keep) 
        throws Exception
    {
        try
        {
            serviceAssemblyRegistrationCheck(UNDEPLOY, serviceAssemblyName, true);

            if ( mTarget.equals(DOMAIN))
            {
                if (getGenericQuery().isServiceAssemblyDeployed(serviceAssemblyName))
                {
                        // -- Return an error only if the target is the domain, since 
                        // -- with the domain target, the SA was meant to be removed
                        String[] params = new String[]{serviceAssemblyName};
                        mMsgBuilder.throwManagementException(UNDEPLOY, 
                            LocalStringKeys.JBI_ADMIN_SERVICE_ASSEMBLY_IS_DEPLOYED, params);

                }

                // give ApplicationInterceptor a chance to process undeploy
                performJavaEEProcessing(serviceAssemblyName, UNDEPLOY);

                // -- Can remove the service assembly     
                removeServiceAssemblyFromDomain(UNDEPLOY, serviceAssemblyName);

                return mMsgBuilder.buildFrameworkMessage(UNDEPLOY,
                    MessageBuilder.TaskResult.SUCCESS);
            }
            else
            {
                String response = undeployServiceAssemblyFromTarget(serviceAssemblyName, force);
                
                // -- If the service assembly is not deployed anywhere else remove it from
                // -- the domain too
                if (!getGenericQuery().isServiceAssemblyDeployed(serviceAssemblyName) && !keep)
                {
                    removeServiceAssemblyFromDomain(UNDEPLOY, serviceAssemblyName);
                }
                return response;
            }
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }
    
    /**
     * Shut down the service assembly. 
     * 
     * @param serviceAssemblyName name of the assembly to be shut down; must be
     *        non-null and non-empty
     * @param force if this flag is true, the service assembly is shutdown forcefully.
     * Any exceptions thrown by the component for service unit shutdown are ignored.
     * @return result / status string giving the results of shutting down  
     *         each service unit in the assembly; must be non-null and non-empty
     * @exception Exception if there is no such assembly
     * @exception Exception if the assembly fails to shut down
     */
    public String shutDown(String serviceAssemblyName, boolean force) throws Exception
    {
        try
        {
            String response = null;
            if ( DOMAIN.equals(mTarget))
            {
                throwNotSupportedManagementException(SHUT_DOWN);
            }
            else
            {
                // Check : Service Assembly should be deployed on target
                serviceAssemblyDeploymentCheck(serviceAssemblyName);
        
                // give ApplicationInterceptor a chance to process shutDown command
                performJavaEEProcessing(serviceAssemblyName, SHUT_DOWN);
                response = shutDownServiceAssemblyOnTarget(serviceAssemblyName, force);
            }
            return response;
        }
        catch ( ManagementException mex )
        {
            throw new JBIException(mex.getMessage());
        }
    }
    
    /*----------------------------------------------------------------------------------*\
     *                          Private Helpers                                         *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Throw a ManagementException if the Service Assembly is not registered in the
     * domain.
     */
    private void serviceAssemblyRegistrationCheck(String taskId, 
        String serviceAssemblyName, boolean throwExWhenNotRegistered)
        throws ManagementException
    {
        boolean registered = false;
        try
        {
            registered = getGenericQuery().isServiceAssemblyRegistered(serviceAssemblyName);
        }
        catch ( RegistryException rex )
        {
            throw new ManagementException(mMsgBuilder.buildExceptionMessage(
                "serviceAssemblyRegistrationCheck", rex));
        }
        
        if ( !registered && throwExWhenNotRegistered)
        {
            String[] params = new String[]{serviceAssemblyName};
            mMsgBuilder.throwManagementException( taskId, 
                LocalStringKeys.JBI_ADMIN_UNKNOWN_SERVICE_ASSEMBLY,  params);
        }
        
        if ( registered && !throwExWhenNotRegistered)
        {
            String[] params = new String[]{serviceAssemblyName};
            mMsgBuilder.throwManagementException( taskId, 
                LocalStringKeys.JBI_ADMIN_KNOWN_SERVICE_ASSEMBLY,  params);
        }
    }    
    
    /**
     * Add Service Assembly Archive to the repository and register in the domain
     */
    private void addServiceAssemblyToDomain(String taskId, String saName, Archive saArchive )
        throws ManagementException
    {
        try
        {
            getRegistry().getRepository().addArchive(saArchive);
            getUpdater().addServiceAssembly(saName, saArchive.getFileName(), saArchive.getUploadTimestamp());
        }
        catch ( RegistryException ex )
        {
            String[] params = new String[]{saName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, 
                LocalStringKeys.JBI_ADMIN_FAILED_ADD_SERVICE_ASSEMBLY, params);
        }
        catch ( RepositoryException ex )
        {   
            String[] params = new String[]{saName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, 
                LocalStringKeys.JBI_ADMIN_FAILED_ADD_SERVICE_ASSEMBLY, params);
        }
    }
    
    
    /**
     * Remove the named service assembly from the domain 
     */
    private void removeServiceAssemblyFromDomain(String taskId, String saName)
        throws ManagementException
    {
        try
        {
            getUpdater().removeServiceAssembly(DOMAIN, saName);
            getRegistry().getRepository().removeArchive(ArchiveType.SERVICE_ASSEMBLY, saName);
        }
        catch ( RegistryException ex )
        {
            String[] params = new String[]{saName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, 
                LocalStringKeys.JBI_ADMIN_FAILED_REMOVE_SERVICE_ASSEMBLY, params);
        }
        catch ( RepositoryException ex )
        {   
            String[] params = new String[]{saName, ex.getMessage()};
            mMsgBuilder.throwManagementException(taskId, 
                LocalStringKeys.JBI_ADMIN_FAILED_REMOVE_SERVICE_ASSEMBLY, params);
        }
    }
    
    /**
     * Deploy the Service Assembly to the target and return the response string.
     *
     * In case of a standalone instance, the result string is the one obtained from the
     * instance.
     *
     * In case of a cluster target the result string is a composite management message
     * based on the result from each of the member instances.
     *
     * @throws ManagementException if the deployment fails completely
     *
     * @return a jbi management message contating the result of a successful or partially
     * successful deployment
     */
    private String deployServiceAssemblyToTarget(String saName, String saZipUrl)
        throws ManagementException
    {
        String response = null;
        mLog.info(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_DEPLOYING_SERVICE_ASSEMBLY_TO_TARGET, 
                saName, mTarget));
        
        // -- Check if all components targeted by the service assembly are 
        // -- installed on the target
        targetComponentsInstallationCheck(saName, true);
        
        /** Perform Java EE processing, if necessary. */
        performJavaEEProcessing(saName, DEPLOY);                
        List<ServiceUnitInfo> suList = new ArrayList();
        
        try
        {
            // -- Auto start all targeted components, if start on deploy is true
            autoStartComponents(saName);

            if ( mPlatform.isStandaloneServer(mTarget) )
            {
                response = deployServiceAssemblyToInstance(saName, saZipUrl, mTarget);       
            }
            else
            {
                response = deployServiceAssemblyToCluster(saName, saZipUrl, mTarget);
            }
            // parse the response and get the successful su deploys and update registry
            suList = mMsgBuilder.getSuccessfulServiceUnits(response,
                getServiceUnitList(saName, "domain"));
            
            addServiceUnitsToTarget(suList);

            MessageBuilder.Message msg = mMsgBuilder.createMessage(response);
            if ( msg.isSuccess() )
            {
               addServiceAssemblyToTarget(saName); 
            }

            return response;
        
        }
        catch ( ManagementException mex )
        {
            mLog.fine(MessageHelper.getMsgString(mex));
            throw mex;
        }
        finally
        {
            /**
             * Issue 73 Fix : If su deployment on target fails, rollback those
             *                su deployments from JavaEE SE.
             */
            if ( isComponentInstalled(JAVA_EE_SE) )
            {
                List<ServiceUnitInfo> suRollbackList = getJavaEEFailedServiceUnits(
                    getServiceUnitList(saName, "domain"), suList);

                undoJavaEEProcessing(DEPLOY, saName, suRollbackList);
            }
        }
        
    }
    
    
    /**
     * Undeploy the Service Assembly from the target and return the response string.
     *
     * In case of a standalone instance, the result string is the one obtained from the
     * instance.
     *
     * In case of a cluster target the result string is a composite management message
     * based on the result from each of the member instances.
     *
     * @param saName - service assembly name
     * @param force - if true the service assembly is forcefully undeployed i.e. any
     * exceptions from the component are ignored.
     * @throws ManagementException if the undeployment fails completely
     *
     * @return a jbi management message containing the result of a successful or partially
     * successful undeployment
     */
    private String undeployServiceAssemblyFromTarget(String saName, boolean force)
        throws ManagementException
    { 
        
        // Check : Service Assembly should be deployed on target
        serviceAssemblyDeploymentCheck(saName);
        
        mLog.info(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_UNDEPLOYING_SERVICE_ASSEMBLY_FROM_TARGET, 
                saName, mTarget));        
        
        // give ApplicationInterceptor a chance to process undeploy
        performJavaEEProcessing(saName, UNDEPLOY);
        
        String response = null;
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            response = undeployServiceAssemblyFromInstance(saName, mTarget, force);
        }
        else
        {
            response = undeployServiceAssemblyFromCluster(saName, mTarget, force);
        }
        // parse the response and get the successful su undeploys and update registry
        List<ServiceUnitInfo> suList = mMsgBuilder.getSuccessfulServiceUnits(response,
            getServiceUnitList(saName, mTarget));
        removeServiceUnitsFromTarget(suList);
      
        MessageBuilder.Message msg = mMsgBuilder.createMessage(response);
        if ( msg.isSuccess() )
        {
            removeServiceAssemblyFromTarget(saName);
        } 
        return response;
    }
    
    /**
     *
     */
    private List<ServiceUnitInfo> getJavaEEFailedServiceUnits(
             List<ServiceUnitInfo> allSus, List<ServiceUnitInfo> sccsSus)
    {
        List<ServiceUnitInfo> rollbackSus = new ArrayList();
        
        for ( ServiceUnitInfo suInfo : allSus )
        {
            if ( suInfo.getTargetComponent().equals(JAVA_EE_SE) )
            {
                rollbackSus.add(suInfo);
                
                // if not in the sccss list then in failure list
                boolean succeeded = false;
                for ( ServiceUnitInfo sccsInfo : sccsSus )
                {
                    if ( sccsInfo.getName().equals(suInfo.getName()) )
                    {
                        System.out.append("Removing service unit " +
                                 suInfo.getName() + " from rollback list");
                        rollbackSus.remove(suInfo);
                        break;
                    }
                }
            }
        }
        
        return rollbackSus;
    }
    /**
     *  @return true if the Service Unit is deployed to the component on the target.
     */
    private boolean isServiceUnitDeployedToComponentOnTarget(String componentName,
        String serviceUnitName)  throws ManagementException
    {
        boolean isServiceUnitDeployed = false;
        ComponentInfo compInfo = getComponentQuery().getComponentInfo(componentName);

        if ( compInfo != null )
        {
            List<ServiceUnitInfo> suList = compInfo.getServiceUnitList();
            for (ServiceUnitInfo suInfo : suList )
            {
                if ( suInfo.getName().equals(serviceUnitName) )
                {
                    isServiceUnitDeployed = true;
                    break;
                }
            }
        } 
        return isServiceUnitDeployed;
    }
    
    /**
     * Check if the Service Unit  is deployed on component on the target.
     */
    private void serviceUnitDeploymentCheck(String componentName, 
        String serviceUnitName)
        throws ManagementException
    {
        if (!isServiceUnitDeployedToComponentOnTarget(componentName, serviceUnitName) )
        {
            String[] params = new String[]{serviceUnitName, componentName, mTarget};
            String msg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_SERVICE_UNIT_NOT_DEPLOYED_ON_TARGET,
                params);
            String jbiMsg = mMsgBuilder.buildFrameworkMessage("serviceUnitDeploymentCheck", 
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                mMsgBuilder.getMessageString(msg), params, mMsgBuilder.getMessageToken(msg));
            throw new ManagementException(jbiMsg);
        }
    }
    
    /**
     *  @return true if the ServiceAssembly is deployed on the target.
     */
    private boolean isServiceAssemblyDeployedOnTarget(String saName)    
        throws ManagementException
    {
        try
        {
            java.util.List<String> servers = getGenericQuery().getServersDeployingServiceAssembly(saName);
            if ( servers.contains(mTarget))
            {
                return true;
            }
            java.util.List<String> clusters = getGenericQuery().getClustersDeployingServiceAssembly(saName);
            if ( clusters.contains(mTarget))
            {
                return true;
            }
            return false;
        }
        catch(RegistryException rex)
        {
            throw new ManagementException( mMsgBuilder.buildExceptionMessage(
                "isServiceAssemblyDeployedOnTarget", rex));
        }
    }
    
    /**
     * Check if the Service Assembly is deployed on the target first.
     */
    private void serviceAssemblyDeploymentCheck(String saName)
        throws ManagementException
    {
        if (!isServiceAssemblyDeployedOnTarget(saName) )
        {
            String[] params = new String[]{saName, mTarget};
            String msg = mTranslator.getString(LocalStringKeys.JBI_ADMIN_SERVICE_ASSEMBLY_NOT_DEPLOYED_ON_TARGET,
                params);
            String jbiMsg = mMsgBuilder.buildFrameworkMessage("serviceAssemblyDeploymentCheck", 
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR, 
                mMsgBuilder.getMessageString(msg), params, mMsgBuilder.getMessageToken(msg));
            throw new ManagementException(jbiMsg);
        }
    }
    
    
    /**
     * Checks if the Service Assembly is shutdown, this is a required check for undeploy.
     *
     * @param saName - service assembly name
     * @param instanceName - instance name
     * @param force - if true, service assembly state of Unknown ( i.e. undeployed )
     * does not cause an exception to be thrown.
     * @throws a ManagementException if ServiceAssembly is not shutdown
     */
    private void  serviceAssemblyShutdownCheck(String saName, String instanceName, boolean force)
        throws ManagementException
    {
        String saState = DeploymentServiceMBean.SHUTDOWN;
        
        try
        {
            saState = getStateFromInstance(saName, instanceName);
        }
        catch ( ManagementException mex )
        {
            // Most likely the service assembly is not deployed on the instance
            // and there is a orphan service-assembly-ref 
            if ( force )
            {
                mLog.fine(MessageHelper.getMsgString(mex, mLog.getLevel()));
                return;
            }
            else
            {
                throw mex;
            }
        }

        if ( !DeploymentServiceMBean.SHUTDOWN.equals(saState) )
        {
            String[] params = new String[]{saName, instanceName};
            
            String errMsg = mTranslator.getString(
                LocalStringKeys.JBI_ADMIN_SERVICE_ASSEMBLY_NOT_SHUTDOWN, params);

            String jbiMsg = mMsgBuilder.buildFrameworkMessage("serviceAssemblyShutdownCheck",
                MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                mMsgBuilder.getMessageString(errMsg), params, 
                mMsgBuilder.getMessageToken(errMsg) );
            
            throw new ManagementException(jbiMsg);
        }
    }
    
    /**
     * @return the object name of the Deployment Service
     */
    private ObjectName getRemotedeploymentServiceObjectName(String instanceName)
    {
        MBeanNames mbnNames = mMgtCtx.getMBeanNames(instanceName);
        return mbnNames.getSystemServiceMBeanName(
            MBeanNames.SERVICE_NAME_DEPLOY_SERVICE,
            MBeanNames.CONTROL_TYPE_DEPLOY_SERVICE);
    }

    
    /** 
     * Throws a management exception with an appropriate message when a SA's 
     * target component is not installed.
     *
     * @param saName - service assembly identification
     * @param isDeploy - true if this operation is being called in the context of deploy
     *                   false in case of undeploy
     * @exception ManagementException
     */
    private void targetComponentsInstallationCheck(String saName, boolean isDeploy)
        throws ManagementException
    {
        List<String> comps = null;
        // -- Get all the components targeted by the assembly use "domain"
        if ( isDeploy )
        {
            comps = getTargetComponents(DOMAIN, saName);
        }
        else
        {
            comps = getTargetComponents(mTarget, saName);
        }
        
        for ( String compName : comps )
        {
            if ( !isComponentInstalled(compName) )
            {
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_SERVICE_ASSEMBLY_TARGET_NOT_INSTALLED,
                    compName, mTarget);

                String mmsg = mMsgBuilder.buildFrameworkMessage(
                    "targetComponentsInstallationCheck",
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg), new String[]{compName, mTarget}, 
                    mMsgBuilder.getMessageToken(errMsg) );

                throw new ManagementException(mmsg);
            }
        }
    }
    
    /** 
     * Throws a management exception with an appropriate message when a SA's 
     * target component is not started
     */
    private void targetComponentsRunningCheck(String saName, String instanceName)
        throws ManagementException
    {
        // -- Get all the components targeted by the assembly use "domain"
        List<String> comps = getTargetComponents(DOMAIN, saName);
        
        for ( String compName : comps )
        {
            ComponentState state = getComponentState(compName, instanceName);
            if ( !(ComponentState.STARTED == state) )
            {
                String errMsg = null;

                if ( ComponentState.UNKNOWN == state )
                {
                    errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_COMPONENT_NOT_INSTALLED_ON_INSTANCE,
                    compName, instanceName);
                }
                else
                {    
                    errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_SERVICE_ASSEMBLY_TARGET_NOT_STARTED,
                    compName, instanceName);
                }

                String mmsg = mMsgBuilder.buildFrameworkMessage(
                    "targetComponentsRunningCheck",
                    MessageBuilder.TaskResult.FAILED, MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg), 
                    new String[]{compName, instanceName}, 
                    mMsgBuilder.getMessageToken(errMsg) );

                throw new ManagementException(mmsg);
            }
        }
    }
    
    /** 
     * To undeploy the Service Assembly successfully, the target components should
     * be in the STOPPED or STARTED state. If the force flag is set to 
     * true the target component can be in the SHUTDOWN state also.
     *
     * @param force if this flag is true the service assembly SHUTDOWN state is a good state 
     * to undeploy the service assembly.
     * @throws a management exception with an appropriate message when a SA's 
     * target component is not installed or is not shutdown
     */
    private void targetComponentsDownCheck(String saName, 
        String instanceName, boolean force)
        throws ManagementException
    {
        // -- Get only those components to which the service assembly is deployed
        List<String> comps = getTargetComponents(mTarget, saName);
        
        for ( String compName : comps )
        {
            ComponentState state = getComponentState(compName, instanceName);
            if ( (ComponentState.SHUTDOWN == state && !force) ||
                 (ComponentState.UNKNOWN  == state ) )
            {
                String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_SERVICE_ASSEMBLY_TARGET_INCORRECT_STATE,
                    compName);

                String mmsg = mMsgBuilder.buildFrameworkMessage(
                    "targetComponentsDownCheck",
                    MessageBuilder.TaskResult.FAILED, 
                    MessageBuilder.MessageType.ERROR,
                    mMsgBuilder.getMessageString(errMsg), 
                    new String[]{compName, state.toString()}, 
                    mMsgBuilder.getMessageToken(errMsg) );

                throw new ManagementException(mmsg);
            }
        }
    }
    
    /**
     * @return a List of component names targeted by the Service Assembly
     */
    private List<String> getTargetComponents(String target, String saName)
        throws ManagementException
    {
        ServiceAssemblyQuery  saQuery = null;
        
        try
        {
            saQuery = getRegistry().getServiceAssemblyQuery(target)  ;
        }
        catch(RegistryException rex)
        {
            throw new ManagementException( mMsgBuilder.buildExceptionMessage(
                "getTargetComponents", rex));
        }
        List<String> components = new ArrayList();
        ServiceAssemblyInfo saInfo = saQuery.getServiceAssemblyInfo(saName);
        if ( saInfo != null )
        {
            List<ServiceUnitInfo> suList = saInfo.getServiceUnitList();
            
            for ( ServiceUnitInfo suInfo : suList )
            {
                components.add(suInfo.getTargetComponent());
            }
        }
        return components;
    }
    
    /**
     * If the target is a server instance then the "start" operation of it's
     * DeploymentService is invoked. If the server instance is any instance
     * other than the DAS server instance then the desired state of the SU's
     * in the jbi-registry needs to be updated.
     *
     * If the target is a cluster, then "start" is called on each member
     * server instances Deployment Service. The DAS registry needs to be
     * updated with the desired state.
     */
    private String startServiceAssemblyOnTarget(String serviceAssemblyName)
        throws ManagementException
    {
        mLog.info(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_STARTING_SERVICE_ASSEMBLY_ON_TARGET, 
                serviceAssemblyName, mTarget));
        
        String response = null;
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            response = startServiceAssemblyOnInstance(serviceAssemblyName, mTarget);
        }
        else
        {
            response = startServiceAssemblyOnCluster(serviceAssemblyName, mTarget);
        }
        List<ServiceUnitInfo> suList = mMsgBuilder.getSuccessfulServiceUnits(response,
            getServiceUnitList(serviceAssemblyName, mTarget));
        setServiceUnitsStateOnTarget(ServiceUnitState.STARTED, suList);
        return response;
    }
    
    /**
     * If the target is a server instance then the "stop" operation of it's
     * DeploymentService is invoked. If the server instance is any instance
     * other than the DAS server instance then the desired state of the SU's
     * in the jbi-registry needs to be updated.
     *
     * If the target is a cluster, then "stop" is called on each member
     * server instances Deployment Service. The DAS registry needs to be
     * updated with the desired state.
     */
    private String stopServiceAssemblyOnTarget(String serviceAssemblyName)
        throws ManagementException
    {   
        mLog.info(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_STOPPING_SERVICE_ASSEMBLY_ON_TARGET, 
                serviceAssemblyName, mTarget));
        
        String response = null;
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            response = stopServiceAssemblyOnInstance(serviceAssemblyName, mTarget);
        }
        else
        {
            response = stopServiceAssemblyOnCluster(serviceAssemblyName, mTarget);
        }
        
        List<ServiceUnitInfo> suList = mMsgBuilder.getSuccessfulServiceUnits(response,
            getServiceUnitList(serviceAssemblyName, mTarget));
        setServiceUnitsStateOnTarget(ServiceUnitState.STOPPED, suList);
        return response;
    }
    
    /**
     * If the target is a server instance then the "shutDown" operation of it's
     * DeploymentService is invoked. If the server instance is any instance
     * other than the DAS server instance then the desired state of the SU's
     * in the jbi-registry needs to be updated.
     *
     * If the target is a cluster, then "shutDown" is called on each member
     * server instances Deployment Service. The DAS registry needs to be
     * updated with the desired state.
     *
     * @param serviceAssemblyName - name of the service assembly to be undeployed
     * @param force - if true the service assembly is to be shutdown forcefully.
     */
    private String shutDownServiceAssemblyOnTarget(String serviceAssemblyName, 
        boolean force)
        throws ManagementException
    {   
        mLog.info(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_SHUTTING_DOWN_SERVICE_ASSEMBLY_ON_TARGET, 
                serviceAssemblyName, mTarget));
        
        String response = null;
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            response = shutDownServiceAssemblyOnInstance(serviceAssemblyName, mTarget, force);
        }
        else
        {
           response = shutDownServiceAssemblyOnCluster(serviceAssemblyName, mTarget, force);
        }
        
        List<ServiceUnitInfo> suList = mMsgBuilder.getSuccessfulServiceUnits(response,
            getServiceUnitList(serviceAssemblyName, mTarget));
        setServiceUnitsStateOnTarget(ServiceUnitState.SHUTDOWN, suList);
        return response;
    }
    
    /**
     * Get the runtime state information from the target directly and not the DAS registry.
     *
     * @return the service assembly state.
     */
    private String getStateFromTarget(String serviceAssemblyName)
        throws ManagementException
    {
        // Service assembly should be deployed on the target
        serviceAssemblyDeploymentCheck(serviceAssemblyName);
        
        String state = null;
        
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            state = getStateFromInstance(serviceAssemblyName, mTarget);
        }
        else
        {
            state = getStateFromCluster(serviceAssemblyName, mTarget);
        }
        return state;
    }
    
    /**
     *
     */
     private String deployServiceAssemblyToInstance(String saName, String saZipUrl, 
        String instanceName)
        throws ManagementException
    {
        String response = null;
        
        if ( isInstanceRunning(instanceName) )
        {
            
            // Check ::  Target Components need to be in  started state 
            targetComponentsRunningCheck(saName, instanceName);
        
            if ( !mPlatform.getAdminServerName().equals(mTarget) )
            {
                saZipUrl = uploadFile(instanceName, saZipUrl);
            }
            else
            {
                // Convert the zip path to a file URL
                saZipUrl = toFileURL(saZipUrl);
            }
            
            Object[] params = new Object[]{saZipUrl};
            String[] sign   = new String[]{"java.lang.String"};
            response =  (String) invokeRemoteOperation(
                getRemotedeploymentServiceObjectName(instanceName), DEPLOY, 
                params, sign, instanceName);
        }        
        else
        {
           return buildSuccessMessage(saName, DEPLOY, "domain");
        }
        return response;
    }
     
    /**
     * Deploy the service assembly to each instance in the cluster. If a cluster 
     * has zero instances, the operation is assumed a success and the cluster-ref entry 
     * for the cluster is updated with the service assembly information ( i.e. 
     * all the target components for this assembly are updated with new service unit 
     * entries )
     */
     private String deployServiceAssemblyToCluster(String saName, String saZipUrl, 
        String clusterName)
        throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    response = deployServiceAssemblyToInstance(saName, saZipUrl, 
                        instance);
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage(DEPLOY,
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = buildSuccessMessage(saName, DEPLOY, "domain");
        }
        return response;
    }
     
     /**
      * Udeploy service assembly from the instance
      */
     private String undeployServiceAssemblyFromInstance(String saName, String instanceName,
        boolean force)
        throws ManagementException
     {
        String response = null;
        if ( isInstanceRunning(instanceName)  )
        {
            /*
             * this is to handle cluster scenarios where one or more instances had successful
             * SA deployment and other instances failed in deployment. The DAS registry
             * will show the SA as deployed for the cluster target. Check again to verify 
             * if the instance has deployment. return SUCCESS with INFO
             */
            if (!isServiceAssemblyDeployedInInstance(saName, instanceName))
            {

                String msg = mTranslator.getString(
                        LocalStringKeys.JBI_ADMIN_SA_NOT_DEPLOYED_IN_INSTANCE, 
                        saName,
                        instanceName);           
                String[] parameters = new String[]{
                        saName, 
                        instanceName};
            
                mLog.fine(msg);
                
                return mMsgBuilder.buildFrameworkMessage(
                        "undeploy", 
                        MessageBuilder.TaskResult.SUCCESS, 
                        MessageBuilder.MessageType.INFO, 
                        mMsgBuilder.getMessageString(msg), 
                        parameters, 
                        mMsgBuilder.getMessageToken(msg));  

            }
            
            // Check : Service Assembly should be shutdown.
            serviceAssemblyShutdownCheck(saName, instanceName, force);

            // Check : all target components should be installed and either in the
            // stopped or started state for undeploy to succeed
            targetComponentsInstallationCheck(saName, false);
            targetComponentsDownCheck(saName, instanceName, force);
        
            Object[] params = new Object[]{saName, Boolean.valueOf(force)};
            String[] sign   = new String[]{"java.lang.String", "boolean"};
            response = (String) invokeRemoteOperation(
                getRemotedeploymentServiceObjectName(instanceName), UNDEPLOY, 
                params, sign, instanceName);
        }
        else
        {
            return buildSuccessMessage(saName, UNDEPLOY, 
                mPlatform.getTargetName(instanceName));
        }
        return response;
     }
     
     
    /**
     * Undeploy the Service Assembly from each instance in the cluster
     *
     * @param saName - name of the service assembly to be undeployed
     * @param clusterName - target cluster name
     * @param force - if set to true the service assembly is forcefully undeployed from 
     * the cluster.
     */
     private String undeployServiceAssemblyFromCluster(String saName, String clusterName,
        boolean force)
        throws ManagementException
    {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    response = undeployServiceAssemblyFromInstance(saName, instance, force);
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage(UNDEPLOY,
                responseMap, exceptionMap, true);

            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            return buildSuccessMessage(saName, UNDEPLOY, clusterName); 
        }
     }
    
    /**
     * Stop Service Assembly on Instance
     */
    private String stopServiceAssemblyOnInstance(String serviceAssemblyName,
        String instanceName)
        throws ManagementException
    {
        String response = null;
        if ( isInstanceRunning(instanceName) )
        {
            Object[] params = new Object[]{serviceAssemblyName};
            String[] sign   = new String[]{"java.lang.String"};
            response = (String) invokeRemoteOperation(
                getRemotedeploymentServiceObjectName(instanceName), STOP, params,
                sign, instanceName);
        }
        else
        {
             return buildSuccessMessage(serviceAssemblyName, STOP, 
                mPlatform.getTargetName(instanceName));
        }
        return response;
    }
    
    /**
     * Stop the service assembly on each instance in the cluster. If the cluster
     * has zero instances the service assembly start is assumed to be a success 
     * and the DAS registry is updated.
     */
     private String stopServiceAssemblyOnCluster(String saName, String clusterName)
        throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    response = stopServiceAssemblyOnInstance(saName, instance);
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage(STOP,
                responseMap, exceptionMap, true);
            
            
            if ( msg.isSuccess()  && 
                getServiceAssemblyState(saName) == ServiceAssemblyState.STOPPED )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = buildSuccessMessage(saName, STOP, clusterName);
        }
        return response;
    }
    
    /**
     * Shutdown Service Assembly on Instance
     *
     * @param serviceAssemblyName - name of the service assembly to be shutdown
     * @param instanceName - targeted instance name
     * @param force - if true the service assembly is forcefully shutdown
     */
    private String shutDownServiceAssemblyOnInstance(String serviceAssemblyName,
        String instanceName, boolean force)
        throws ManagementException
    {
        String response = null;
        if ( isInstanceRunning(instanceName) )
        {
            Object[] params = new Object[]{serviceAssemblyName, Boolean.valueOf(force)};
            String[] sign   = new String[]{"java.lang.String", "boolean"};
            response =  (String) invokeRemoteOperation(
                getRemotedeploymentServiceObjectName(instanceName), SHUT_DOWN, params, 
                sign, instanceName);
        }
        else
        {
             return buildSuccessMessage(serviceAssemblyName, SHUT_DOWN, 
                mPlatform.getTargetName(instanceName));
        }
        return response;
    }
    
    /**
     * Shutdown the service assembly on each instance in the cluster. If the cluster
     * has zero instances the service assembly start is assumed to be a success 
     * and the DAS registry is updated.
     *
     * @param serviceAssemblyName - name of the service assembly to be shutdown
     * @param clusterName - target cluster name
     * @param force - if true the service assembly is forcefully shutdown in the cluster
     */
     private String shutDownServiceAssemblyOnCluster(String saName, String clusterName,
        boolean force)
        throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    response = shutDownServiceAssemblyOnInstance(saName, instance, force);
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage(SHUT_DOWN,
                responseMap, exceptionMap, true);
            
            if ( msg.isSuccess() && 
                getServiceAssemblyState(saName) == ServiceAssemblyState.SHUTDOWN )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = buildSuccessMessage(saName, SHUT_DOWN, clusterName);
        }
        return response;
    }
    
    /**
     * Start Service Assembly on Instance
     */
    private String startServiceAssemblyOnInstance(String serviceAssemblyName, 
        String instanceName)
        throws ManagementException
    {
     
        String response = null;
        if ( isInstanceRunning(instanceName) )
        {   
            Object[] params = new Object[]{serviceAssemblyName};
            String[] sign   = new String[]{"java.lang.String"};
            response =  (String) invokeRemoteOperation(
                getRemotedeploymentServiceObjectName(instanceName), START, 
                params, sign, instanceName);
        }
        else
        {
             return buildSuccessMessage(serviceAssemblyName, START, 
                mPlatform.getTargetName(instanceName));
        }
        return response;
    }
    
    
    /**
     * Start the service assembly on each instance in the cluster. If the cluster
     * has zero instances the service assembly start is assumed to be a success 
     * and the DAS registry is updated.
     */
     private String startServiceAssemblyOnCluster(String saName, String clusterName)
        throws ManagementException
     {
        String response = null;
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
            HashMap<String, String> responseMap = new HashMap<String, String>();
            
            for ( String instance : instances )
            {
                try
                {
                    response = startServiceAssemblyOnInstance(saName, instance);
                    responseMap.put(instance, response);
                }
                catch(ManagementException mex)
                {
                    exceptionMap.put(instance, mex);

                    continue;
                }
            }

            MessageBuilder.Message msg = mMsgBuilder.buildCompositeMessage(START,
                responseMap, exceptionMap, false);
            
            if ( msg.isSuccess() )
            {
                return msg.getMessage();
            }
            else
            {
                throw new ManagementException(msg.getMessage());
            }
        }
        else
        {
            response = buildSuccessMessage(saName, START, clusterName);
        }
        return response;
    }
         
    /**
     * Get the Service Assembly state from the instance
     */
    private String getStateFromInstance(String serviceAssemblyName,  String instanceName)
        throws ManagementException
    {
        String state = null;
        if ( isInstanceRunning(instanceName) )
        {
            Object[] params = new Object[]{serviceAssemblyName};
            String[] sign   = new String[]{"java.lang.String"};
            state = (String) invokeRemoteOperation(
                getRemotedeploymentServiceObjectName(instanceName), "getState", params, 
                sign, instanceName);
        }
        else
        {
            state = ServiceAssemblyState.convertState(ServiceAssemblyState.SHUTDOWN);
        }
        mLog.finest("State of service assembly " + serviceAssemblyName + " on instance "
            + instanceName + " is " + state);
        return state;
    }
    
    /**
     * Get the state of the service assembly from each instance and then compute the
     * effective state of the service assembly in the cluster
     */
    private String getStateFromCluster(String saName,  String clusterName)
        throws ManagementException
    {
        String state = ServiceAssemblyState.convertState(ServiceAssemblyState.SHUTDOWN);
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            List<ServiceAssemblyState> 
                statesInCluster = new ArrayList<ServiceAssemblyState>();
            for ( String instance : instances )
            {
                try
                {
                    if ( isInstanceRunning(instance) )
                    {
                        String instState = getStateFromInstance(saName, instance);
                        statesInCluster.add(
                            ServiceAssemblyState.valueOfDeploymentServiceState(instState));
                    }
                    else
                    {
                        statesInCluster.add(ServiceAssemblyState.SHUTDOWN);
                    }
                }
                catch(ManagementException mex)
                {
                    mLog.fine(MessageHelper.getMsgString(mex, mLog.getLevel()));
                    statesInCluster.add(ServiceAssemblyState.UNKNOWN);
                    continue;
                }
            }

            mLog.fine(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_STATE_OF_SERVICE_ASSEMBLY_IN_CLUSTER, saName, 
                clusterName,  statesInCluster.toString()));
           
           ServiceAssemblyState 
            effState = ServiceAssemblyState.getEffectiveServiceAssemblyState(statesInCluster);
            
           state = ServiceAssemblyState.convertState(effState);
        }
        return state;
    }
    
    
    /**
     * Get the runtime state of the service unit on the target
     *
     * @param componentName   - the componentName
     * @param serviceUnitName - the service unit name
     * @return the service assembly state.
     */
    private String getServiceUnitStateFromTarget(String componentName, 
        String serviceUnitName)
        throws ManagementException
    {
        // Component should be installed on target
        componentInstallationCheck(componentName, false);
        
        // Service Unit should be deployed to the component
        serviceUnitDeploymentCheck(componentName, serviceUnitName);
        
        String state = null;
        
        if ( mPlatform.isStandaloneServer(mTarget) )
        {
            state = getServiceUnitStateFromInstance(componentName, serviceUnitName, mTarget);
        }
        else
        {
            state = getServiceUnitStateFromCluster(componentName, serviceUnitName, mTarget);
        }
        return state;
    }
    
    /**
     * Get the Service Unit state from the instance
     *
     * @param componentName   - the componentName
     * @param serviceUnitName - the service unit name
     * @param instanceName    - target instance name
     */
    private String getServiceUnitStateFromInstance(String componentName, 
        String serviceUnitName, String instanceName)
        throws ManagementException
    {
        String state = null;
        if ( isInstanceRunning(instanceName) )
        {
            Object[] params = new Object[]{componentName, serviceUnitName};
            String[] sign   = new String[]{"java.lang.String", "java.lang.String"};
            state = (String) invokeRemoteOperation(
                getRemotedeploymentServiceObjectName(instanceName), "getServiceUnitState"
                    , params, sign, instanceName);
        }
        else
        {
            state = ServiceUnitState.convertState(ServiceUnitState.SHUTDOWN);
        }
        mLog.finest("State of service unit " + serviceUnitName + " on instance "
            + instanceName + " is " + state);
        return state;
    }
    
    /**
     * Get the state of the service unit from each instance and then compute the
     * effective state of the service unit in the cluster
     */
    private String getServiceUnitStateFromCluster(String componentName,  
        String serviceUnitName, String clusterName)
        throws ManagementException
    {
        String state = ServiceUnitState.convertState(ServiceUnitState.SHUTDOWN);
        
        Set<String> instances = mPlatform.getServersInCluster(clusterName);
        
        if ( !instances.isEmpty() )
        {
            List<ServiceUnitState> 
                statesInCluster = new ArrayList<ServiceUnitState>();
            for ( String instance : instances )
            {
                try
                {
                    if ( isInstanceRunning(instance) )
                    {
                        String instState = getServiceUnitStateFromInstance(
                            componentName, serviceUnitName, instance);
                        statesInCluster.add(
                            ServiceUnitState.valueOfDeploymentServiceState(instState));
                    }
                    else
                    {
                        statesInCluster.add(ServiceUnitState.SHUTDOWN);
                    }
                }
                catch(ManagementException mex)
                {
                    mLog.fine(MessageHelper.getMsgString(mex, mLog.getLevel()));
                    statesInCluster.add(ServiceUnitState.UNKNOWN);
                    continue;
                }
            }

            String[] params = new String[]{serviceUnitName, componentName, clusterName,  
                statesInCluster.toString()};
            mLog.fine(mTranslator.getString(
            LocalStringKeys.JBI_ADMIN_STATE_OF_SERVICE_UNIT_IN_CLUSTER, params));
           
           ServiceUnitState 
            effState = ServiceUnitState.getEffectiveServiceAssemblyState(statesInCluster);
            
           state = ServiceUnitState.convertState(effState);
        }
        return state;
    }
    
    /**
     * Add the service units to the corresponding target-component on the target.
     */
    private void addServiceUnitsToTarget(List<ServiceUnitInfo> suList)
        throws ManagementException
    {
        if ( !mPlatform.getAdminServerName().equals(mTarget) )
        {
            try
            {
                for ( ServiceUnitInfo suInfo : suList )
                {
                    // change : the second param in redundant
                    //redeployment support - check before adding SU ref
                    if (isDeployedServiceUnit(suInfo.getTargetComponent(), suInfo.getName()))
                    {
                        mLog.fine("Service unit " + suInfo.getName() + " is already registered" );
                    }
                    else
                    {
                        getUpdater().addServiceUnitToComponent(mTarget, suInfo.getTargetComponent(), 
                        suInfo);
                    }
                }
            }
            catch(Exception rex)
            {
                String msg = mMsgBuilder.buildExceptionMessage("addServiceUnitsToTarget", 
                    rex);
                throw new ManagementException(msg);
            }
        }
    }
    
    /**
     * Update DAS registry : Remove the service units from the corresponding 
     * target-component on the target.
     */
    private void removeServiceUnitsFromTarget(List<ServiceUnitInfo> suList)
        throws ManagementException
    {
        if ( !mPlatform.getAdminServerName().equals(mTarget) )
        {
            try
            {
                for ( ServiceUnitInfo suInfo : suList )
                {
                    // change : the first param in redundant
                    getUpdater().removeServiceUnitFromComponent(mTarget, 
                        suInfo.getTargetComponent(), 
                        suInfo.getName());
                }
            }
            catch(RegistryException rex)
            {
                String msg = mMsgBuilder.buildExceptionMessage("removeServiceUnitsFromTarget", 
                    rex);
                throw new ManagementException(msg);
            }
        }
    }
    
    /**
     * Update the DAS registry : set the state of each service unit to the specified 
     * desired state
     *
     * @param state - the ServiceUnit State
     * @param suList - list of service unit infos to set the state for
     */
    private void setServiceUnitsStateOnTarget(ServiceUnitState state,
        List<ServiceUnitInfo> suList) throws ManagementException
    {
        if ( !mPlatform.getAdminServerName().equals(mTarget) )
        {
            try
            {
                for ( ServiceUnitInfo suInfo : suList )
                {
                    getUpdater().setServiceUnitState(mTarget, state,
                        suInfo.getTargetComponent(), 
                        suInfo.getName());
                }
            }
            catch(RegistryException rex)
            {
                String msg = mMsgBuilder.buildExceptionMessage("setServiceUnitsStateOnTarget", 
                    rex);
                throw new ManagementException(msg);
            }   
        }
    }

    /**
     * Add the service assembly to the target.
     */
    private void addServiceAssemblyToTarget(String saName)
        throws ManagementException
    {
        if ( !mTarget.equals(mPlatform.getAdminServerName()) )
        {
            try
            {
                    // If it is a reddeploy case then service assembly already added 
                    if ( !getRegistry().getServiceAssemblyQuery(mTarget).getServiceAssemblies().contains(saName) )
                    { 
                        getUpdater().addServiceAssembly( mTarget, saName );
                    }
            }
            catch(RegistryException rex)
            {
                String msg = mMsgBuilder.buildExceptionMessage("addServiceAssemblyToTarget",
                    rex);
                throw new ManagementException(msg);
            }
        }
    }
 
    /**
     * Remove the service assembly from the target.
     */
    private void removeServiceAssemblyFromTarget(String saName)
        throws ManagementException
    {
        if ( !mTarget.equals(mPlatform.getAdminServerName()) )
        {
            try
            {
                getUpdater().removeServiceAssembly( mTarget, saName );
            }
            catch(RegistryException rex)
            {
                String msg = mMsgBuilder.buildExceptionMessage("removeServiceAssemblyFromTarget",
                    rex);
                throw new ManagementException(msg);
            }
        }
    }
    /**
     * @return the ServiceUnitList for the specified Service Assembly
     */
    List<ServiceUnitInfo> getServiceUnitList(String saName, String target)
    {
            List<ServiceUnitInfo> suList = new ArrayList<ServiceUnitInfo>();
            com.sun.jbi.ServiceAssemblyQuery  saQuery;
            try
            {
                    saQuery = getRegistry().getServiceAssemblyQuery(target);
            }
            catch (RegistryException rex)
            {
                mLog.fine(MessageHelper.getMsgString(rex));
                return suList;
            }
            ServiceAssemblyInfo saInfo = saQuery.getServiceAssemblyInfo(saName);   
            if ( saInfo != null )
            {
               return saInfo.getServiceUnitList();
            }
            else
            {
                return suList;
            }
    }
    
    /**
     * This method is called to build a successful result for sa operations 
     * which cannot be completed because the instance is not running or when there are no
     * instances in a cluster, success is assumed.
     */
    private String buildSuccessMessage(String saName, String taskId, String target)
        throws ManagementException
    {
        
        List<ComponentMessageHolder> compMsgs = new ArrayList<ComponentMessageHolder>();
        List<ServiceUnitInfo> suList = getServiceUnitList(saName, target);
        
        for (ServiceUnitInfo suInfo : suList )
        {
            ComponentMessageHolder 
            compMsg = new ComponentMessageHolder(MessageBuilder.MessageType.INFO.toString());
            compMsg.setComponentName(suInfo.getTargetComponent());
            compMsg.setTaskName(taskId);
            compMsg.setTaskResult(MessageBuilder.TaskResult.SUCCESS.toString());
            
            compMsgs.add(compMsg);
        }
        
        return mMsgBuilder.buildFrameworkMessage(
            taskId, MessageBuilder.TaskResult.SUCCESS, compMsgs);
        
    }
    
    /**
     * Get the service assembly state on this target
     */
    private ServiceAssemblyState getServiceAssemblyState(String saName)
    {
        ServiceAssemblyState saState = ServiceAssemblyState.UNKNOWN;
        try
        {
            String state = getState(saName);
            saState = ServiceAssemblyState.valueOfDeploymentServiceState(state);
        }
        catch ( Exception ex )
        {
            mLog.fine(MessageHelper.getMsgString(ex));
        }
        return saState;
    }
    
    /** This method performs Java EE pre-processing for a service assembly, 
     *  if required.  At the moment, we assume that preprocessing is 
     *  only necessary if the service assembly targets a component named 
     *  sun-javaee-engine.  Moving forward, we may need to make the component 
     *  name configurable as other Java EE capable components become available.
     */
    private void performJavaEEProcessing(String saName, String operation)
        throws ManagementException
    {
        
        // If the Java EE service engine is installed on the target, then proceed
        if ( isComponentInstalled(JAVA_EE_SE) )
        {

            ArrayList<ServiceUnitInfo> suList     = new ArrayList<ServiceUnitInfo>();
            ArrayList<ServiceUnitInfo> suSccssList = new ArrayList<ServiceUnitInfo>();
            ServiceAssemblyInfo saInfo;

            try
            {
                saInfo = getRegistry().getServiceAssemblyQuery(DOMAIN).
                        getServiceAssemblyInfo(saName);
            }
            catch (com.sun.jbi.management.registry.RegistryException regEx)
            {
                mLog.fine(MessageHelper.getMsgString(regEx));
                return;
            }

            // Build a list of Java EE SUs
            for (ServiceUnitInfo suInfo : saInfo.getServiceUnitList())
            {
                if (JAVA_EE_SE.equals(suInfo.getTargetComponent()))
                {
                    suList.add(suInfo);
                }
            }

            // If we have at least one Java EE SU to process, we need to make 
            // sure that the Java EE SE is started on the DAS.
            if (suList.size() > 0)
            {
                autoStartComponent(JAVA_EE_SE, mPlatform.getAdminServerName());
            }

            // Iterate through the list and process each SU
            for (ServiceUnitInfo suInfo : suList)
            {
                String result;
                String suPath = getRegistry().getRepository().findArchive(
                        ArchiveType.SERVICE_UNIT, saName + "/" + suInfo.getName());

                try
                {
                    result = mInterceptor.performAction(
                        operation, saName, suInfo.getName(), suPath, mTarget);
                    mLog.finer("ApplicationInterceptor response for service assembly " +
                            saName + " : " + result);
                    suSccssList.add(suInfo);
                }
                catch (Exception ex)
                {
                    // We want to fail fast with deployment exceptions, since there
                    // is no point in proceeding with the service unit deployments
                    // when we know there is something wrong with the app.
                    if (operation.equals("deploy") || operation.equals("start"))
                    {
                        undoJavaEEProcessing(operation, saName, suSccssList);
                        throw new ManagementException(mMsgBuilder.buildFrameworkMessage(
                                operation, MessageBuilder.TaskResult.FAILED, 
                                MessageBuilder.MessageType.ERROR, ex.getMessage(),
                                new String[] {}, null));
                    }                

                    mLog.fine(mTranslator.getString(
                            LocalStringKeys.JBI_ADMIN_APPLICATION_INTERCEPTOR_ERROR, 
                            operation, saName, suInfo.getName(), ex.toString()));
                }
            }
        }
    }
    
    /**
     * This method is invoked when the JavaEE processing step fails. The service
     * units chich succeeded the operation need to be rolled back. Currently we roll
     * back only "deploy" operations. 
     *
     * @param operation - task id
     * @param saName    - service assembly name
     * @param suSccssList - list of ServiceUnitInfo's which have to be rolled back
     */
    private void undoJavaEEProcessing(String operation, String saName, List<ServiceUnitInfo> suSccssList)
    {
        /**
         * When this operation is called the check to see if the JavaEE Service Engine
         * is deployed has passed and the engine is started. So this assumes that the
         * Java EE SE is installed and started.
         */
        if ( operation.equals("deploy"))
        {
            String rollBackOperation = "undeploy";
            
            // Iterate through the list of service units which need to be rolled back
            for (ServiceUnitInfo suInfo : suSccssList)
            {
                String result;
                String suPath = getRegistry().getRepository().findArchive(
                        ArchiveType.SERVICE_UNIT, saName + "/" + suInfo.getName());

                if ( suPath != null )
                {
                    try
                    {
                        result = mInterceptor.performAction(
                            rollBackOperation, saName, suInfo.getName(), suPath, mTarget);
                    }
                    catch (Exception ex)
                    {                
                        mLog.fine(mTranslator.getString(
                                LocalStringKeys.JBI_ADMIN_APPLICATION_INTERCEPTOR_ROLLBACK_ERROR, 
                                operation, saName, suInfo.getName(), ex.toString()));
                        continue;
                    }
                }
            }
        }
    }
    /**
     * This operation gets the state of the service assembly on all targets in the domain
     * and returns the most advanced state of the service assembly.
     * 
     * @return the most advanced DeploymentServiceMBean state string
     */
    private String getStateFromAllTargets(String saName)
        throws ManagementException
    {
       List<String> targets = getAllTargets();
       List<ServiceAssemblyState> states = new ArrayList<ServiceAssemblyState>();
       for( String target : targets )
       {
           try
           {
               ObjectName facadeDeploymentSvcMBean = 
                   mMBeanNames.getSystemServiceMBeanName(
                       MBeanNames.ServiceName.DeploymentService, 
                       MBeanNames.ServiceType.Deployment, target);
               
               String state = (String) mMBeanSvr.invoke(facadeDeploymentSvcMBean, "getState",
                   new Object[]{saName}, new String[]{"java.lang.String"});
                   states.add(ServiceAssemblyState.valueOfDeploymentServiceState(state));
               mLog.finer("The state of service assembly  " + saName + " on target " 
                + target + " is " + state);
           }
           catch ( Exception ex )
           {
               states.add(ServiceAssemblyState.UNKNOWN);
               continue;
           }
       }
       mLog.finer("The state of service assembly  " + saName + " on targets " 
                + targets.toString() + " is " + states);
       ServiceAssemblyState effState = 
           ServiceAssemblyState.getEffectiveServiceAssemblyState(states);
       return ServiceAssemblyState.convertState(effState);
    }
    
    
    /**
     * This operation gets the state of the service unit on all targets in the domain
     * and returns the most advanced state of the service unit.
     * 
     * @return the most advanced DeploymentServiceMBean state string
     * @param componentName - the componentName
     * @param serviceUnitName - the service unit name
     */
    private String getServiceUnitStateFromAllTargets(String componentName, 
        String serviceUnitName) throws ManagementException
    {
       List<String> targets = getAllTargets();
       List<ServiceUnitState> states = new ArrayList<ServiceUnitState>();
       for( String target : targets )
       {
           try
           {
               ObjectName facadeDeploymentSvcMBean = 
                   mMBeanNames.getSystemServiceMBeanName(
                       MBeanNames.ServiceName.DeploymentService, 
                       MBeanNames.ServiceType.Deployment, target);
               
               String state = (String) mMBeanSvr.invoke(facadeDeploymentSvcMBean, "getServiceUnitState",
                   new Object[]{componentName, serviceUnitName}, 
                   new String[]{"java.lang.String", "java.lang.String"});
               states.add(ServiceUnitState.valueOfDeploymentServiceState(state));
               mLog.finer("The state of service unit  " + serviceUnitName 
                + " deployed to component " + componentName + " on target " 
                + target + " is " + state);
           }
           catch ( Exception ex )
           {
               states.add(ServiceUnitState.UNKNOWN);
               continue;
           }
       }
       mLog.finer("The state of service unit  " + serviceUnitName + " deployed to component"
        + componentName + " on targets " + targets.toString() + " is " + states);
       ServiceUnitState effState = 
           ServiceUnitState.getEffectiveServiceAssemblyState(states);
       return ServiceUnitState.convertState(effState);
    }
    
    /**
     * This method is used to automatically start components that have
     * start-on-deploy true
     * @param componentName the component to be started
     * @param targetName the target in which the component has to be started
     */
    private void autoStartComponent(String componentName, String targetName)
        throws ManagementException
    {
        
        ComponentType compType = getComponentType(componentName);

        if ( compType != null )
        {
            ObjectName facadeComponentLCMBean = null;
            
            facadeComponentLCMBean = mMBeanNames.getComponentMBeanName(
                    componentName, compType,
                    MBeanNames.ComponentServiceType.ComponentLifeCycle, targetName);
            
            try 
            {
                // Determine the current state of the component on the specified target
                String state = (String) mMBeanSvr.getAttribute(
                        facadeComponentLCMBean, CURRENT_STATE);
                
                if (ComponentState.valueOfLifeCycleState(state).equals(ComponentState.STARTED))
                {
                    // Component is already started, so we don't have to do anything
                    mLog.finer("Component " + componentName + " is already started.");
                    return;
                }

                mLog.finer(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_CHANGING_COMPONENT_STATE_ON_INSTANCE,
                    "start", componentName, targetName));

                mLog.info(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_AUTO_START_COMPONENT, 
                        componentName, targetName));
       
                mMBeanSvr.invoke(facadeComponentLCMBean, 
                        "start", new Object[]{}, new String[]{});
            }
            catch (Exception ex)
            {
                mLog.fine(MessageHelper.getMsgString(ex));
                return;
            }
        }            
    }   
    
    /**
     * This method is used to find out if a  SA is deployed in the 
     * given instance.
     * @param saName - the SA name
     * @param instaneName - the instance name 
     * @return boolean -  true if the given SA is deployed
     * @throws Exception
     */
    public boolean isServiceAssemblyDeployedInInstance(String saName, String instanceName)
    throws ManagementException
    {
        try
        {
            String[] saNames = (String[])invokeRemoteOperation(
                 getRemotedeploymentServiceObjectName(instanceName), 
                 "getDeployedServiceAssemblies", 
                 new Object[]{}, 
                 new String[]{},
                 instanceName);
            for(int index =0; index < saNames.length; index++)
            {
                if (saNames[index].equals(saName))
                {
                    return true;
                }
            }
        }
        catch(Exception ex)
        {
            mLog.fine(MessageHelper.getMsgString(ex));
            throw new ManagementException(ex.getMessage());            
        }

        return false;
    }
    
    /**
     * Automatically start all the components in the service assembly if auto start is
     * enabled
     *
     * @param saName - service assembly name
     * @exception ManagementException on errors
     */
    private void autoStartComponents(String saName)
        throws ManagementException
    {
        // Get *all* the components targeted by the assembly, use "domain" target
        List<String> comps = getTargetComponents(DOMAIN, saName);
        for ( String compName : comps )
        {
            // auto-start components with start-on-deploy set to true
            if (mEnvCtx.isStartOnDeployEnabled()) 
            {
                autoStartComponent(compName, mTarget);    
            }
        }        
    }
    
    /**
     * 
     * @param componentName - component of interest
     * @param target - target of interest
     * @return a atring array containing the names of all service assemblies
     *         deployed to the component installed on the target. If the
     *         component is not installed on the target an empty array is returned.
     */
    private String[] getDeployedServiceAssembliesForComponent(String target, String componentName)
        throws Exception
    {
        List<String> sas = new java.util.ArrayList();

        ComponentInfo compInfo = getComponentQuery(target).getComponentInfo(componentName);

        if ( compInfo != null )
        {
            List<ServiceUnitInfo> suList = compInfo.getServiceUnitList();

            for ( ServiceUnitInfo suInfo : suList)
            {
               sas.add(suInfo.getServiceAssemblyName());
            }
        }
        return sas.toArray(new String[sas.size()]);
    }
}
        
        

<?xml version="1.0" encoding="UTF-8"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)esb-result.xsd
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<xs:schema targetNamespace="http://java.sun.com/xml/ns/jbi/esb" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://java.sun.com/xml/ns/jbi/esb" xmlns:jbi="http://java.sun.com/xml/ns/jbi/management-message" elementFormDefault="qualified">
   <xs:include schemaLocation="esb-core.xsd"/>
   <!-- Used for task results -->
   <xs:import namespace="http://java.sun.com/xml/ns/jbi/management-message" schemaLocation="managementMessage.xsd"/>
   <!--
   Top-level element which contains a task result *or* a query result.
-->
   <xs:element name="esb-result">
      <xs:complexType>
         <xs:choice>
            <xs:element ref="esb-task-result"/>
            <xs:element ref="esb-info-result"/>
         </xs:choice>
         <xs:attribute name="version" type="xs:decimal" use="required"/>
      </xs:complexType>
   </xs:element>
   <!--
     ESB task result uses the managementMessage structure for task results.  The task-result-details
     definition was extended to include ESB-specific details in appropriate places.
   -->
   <xs:element name="esb-task-result">
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="jbi:task-result-details">
               <xs:sequence>
                  <xs:element ref="component-info" minOccurs="0"/>
                  <xs:element ref="shared-library-info" minOccurs="0"/>
                  <xs:element ref="service-assembly-info" minOccurs="0"/>
                  <xs:element ref="instance-details"/>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <!--
       This element contains a reference to all element definitions used as return types
       for MBean query operations
    -->
   <xs:element name="esb-info-result">
      <xs:complexType>
         <xs:choice>
            <xs:element ref="component-info"/>
            <xs:element ref="all-components-info"/>
            <xs:element ref="installed-components-info"/>
            <xs:element ref="shared-library-info"/>
            <xs:element ref="all-shared-libraries-info"/>
            <xs:element ref="installed-shared-libraries-info"/>
            <xs:element ref="dependent-shared-libraries-list"/>
            <xs:element ref="dependent-components-list"/>
            <xs:element ref="instances-info"/>
            <xs:element ref="instance-info"/>
            <xs:element ref="all-service-assemblies-info"/>
            <xs:element ref="deployed-service-assemblies-info"/>
            <xs:element ref="service-assembly-info"/>
            <xs:element ref="deployed-service-assemblies-list"/>
         </xs:choice>
      </xs:complexType>
   </xs:element>
   <!-- Encapsulates individual instance results -->
   <xs:element name="instance-details">
      <xs:complexType>
         <xs:sequence>
            <xs:element ref="instance-result-detail" minOccurs="0" maxOccurs="unbounded"/>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <!-- Detailed result information from an instance -->
   <xs:element name="instance-result-detail">
      <xs:complexType>
         <xs:sequence>
            <xs:element ref="name"/>
            <xs:element ref="jbi:jbi-task-result"/>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <!-- MBean Query operations -->
   <xs:element name="all-components-info" type="ComponentListType"/>
   <xs:element name="instances-info" type="InstanceListType"/>
   <xs:element name="instance-info" type="InstanceType"/>
   <xs:element name="installed-components-info" type="ComponentListType"/>
   <xs:element name="component-info" type="ComponentInfoType"/>
   <xs:element name="shared-library-info" type="SharedLibraryType"/>
   <xs:element name="all-shared-libraries-info" type="SharedLibraryListType"/>
   <xs:element name="installed-shared-libraries-info" type="SharedLibraryListType"/>
   <xs:element name="dependent-shared-libraries-list" type="NameListType"/>
   <xs:element name="dependent-components-list" type="NameListType"/>
   <xs:element name="service-assembly-info" type="ServiceAssemblyType"/>
   <xs:element name="all-service-assemblies-info" type="ServiceAssemblyListType"/>
   <xs:element name="deployed-service-assemblies-info" type="ServiceAssemblyListType"/>
   <xs:element name="deployed-service-assemblies-list" type="NameListType"/>
   <!--
        Reusable type for listing component details; eliminates the need to repeatedly define this
        sequence for every operation element that contains a list of components.
       -->
   <xs:complexType name="ComponentListType">
      <xs:sequence>
         <xs:element ref="component-info" minOccurs="0" maxOccurs="unbounded"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="InstanceListType">
      <xs:sequence>
         <xs:element ref="instance-info" minOccurs="0" maxOccurs="unbounded"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="SharedLibraryListType">
      <xs:sequence>
         <xs:element ref="shared-library-info" minOccurs="0" maxOccurs="unbounded"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="ServiceAssemblyListType">
      <xs:sequence>
         <xs:element ref="service-assembly-info" minOccurs="0" maxOccurs="unbounded"/>
      </xs:sequence>
   </xs:complexType>
   <xs:complexType name="ComponentInfoType">
      <xs:complexContent>
         <xs:extension base="ComponentType">
            <xs:sequence>
               <xs:element ref="dependent-shared-libraries-list"/>
               <xs:element ref="deployed-service-assemblies-list" minOccurs="0"/>
            </xs:sequence>
         </xs:extension>
      </xs:complexContent>
   </xs:complexType>
</xs:schema>

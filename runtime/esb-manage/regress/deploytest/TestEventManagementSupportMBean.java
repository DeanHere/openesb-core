package deploytest;

import java.util.Collection;

import javax.management.openmbean.CompositeData;

public interface TestEventManagementSupportMBean {

	
	public void forwardEvent(CompositeData data);
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtensionBindingComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package deploytest;

import java.util.logging.Logger;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;

/**
 * Dummy binding component used to test deployment.
 *
 * @author Sun Microsystems, Inc.
 */
public class ExtensionBindingComponent 
    extends BindingComponent
    implements Component, ComponentLifeCycle, ServiceUnitManager, Bootstrap

{

    public void init(javax.jbi.component.InstallationContext installationContext)
        throws javax.jbi.JBIException
    {
        org.w3c.dom.DocumentFragment docFrag = 
            installationContext.getInstallationDescriptorExtension();
        
        if ( docFrag == null )
        {
            throw new javax.jbi.JBIException("Missing extension data in installation context.");
        }
        
        org.w3c.dom.NodeList extData = docFrag.getChildNodes();
        
        int numNodes = extData.getLength();
        System.out.println(numNodes);
       
        if ( numNodes != 2 )
        {
            throw new javax.jbi.JBIException("Extension data not set correctly. Expected " +
                "two elements");
        }
        
        for ( int i=0; i < numNodes; i++ )
        {
            org.w3c.dom.Node currNode = extData.item(i);
            
               
            if ( currNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE )
            {
                org.w3c.dom.Element el = (org.w3c.dom.Element) currNode;
                System.out.println("### Extension Data ### " + el.getNodeName());
                if ( el.getFirstChild() != null )
                {
                    System.out.println(" Details " + 
                        el.getFirstChild().getNodeName() + " " + el.getFirstChild().getNodeValue());
                }
            }
        }
    }

   
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BadInstallBindingComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package deploytest;

import java.util.logging.Logger;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;

/**
 * Dummy binding component used to test bad install.
 *
 * @author Sun Microsystems, Inc.
 */
public class BadInstallBindingComponent 
    extends BindingComponent
    implements Component, ComponentLifeCycle, ServiceUnitManager, Bootstrap

{
    /**
     * This operation always fails will always throw an exception
     * and will cause install to fail
     */
    public void onInstall() 
        throws javax.jbi.JBIException
    {
       mLog.info("Component " + mComponentName + " throwing exception on install");
       throw new javax.jbi.JBIException(
               createErrorResult("onInstall", "Component install failed"));
     
    }
}

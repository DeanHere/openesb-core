/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CustomConfig.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package deploytest;

import javax.management.MBeanException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.ArrayType;

public class MissingConfig
    implements MissingConfigMBean
{
    private String mInfo;
    
    private int mPort;
    private String mHostName;
    private String mProxyPwd;
    
    /** The application variables TabularData */
    private TabularDataSupport mAppVars;    
    
    /** Application Variable Composite Type */
    private static CompositeType sAppVarComposite;
    
    /** Application Variable Tabular Type */
    private static TabularType   sAppVarTable;
    
    /** Application Variable Composite Item Names */
    private static String[] sAppVarItemNames;
    
    /** The index of the Application Variables **/
    private static String[] sappVarRowIndex;
    
    
   /** The application configuration TabularData */
    private TabularDataSupport mAppConfigs;
    
    
    /** Application Configuration Composite Type */
    private static CompositeType sAppConfigComposite;
    
    /** Application Configuration Tabular Type */
    private static TabularType   sAppConfigTable;
    
    /** Application Configuration Composite Item Names */
    private static String[] sAppConfigItemNames;
    
    /** The index of the Application Configurations **/
    private static String[] sAppConfigRowIndex;
    
    
    private static final String CONFIG_XML =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<componentConfig:Configuration name=\"sun-http-binding\" " +
                               "xmlns:componentConfig=\"http://com.sun.jbi.component/schema/configuration\"> " +
                        "<componentConfig:OutboundThreads displayName=\"Number of outbound threads\" " +
                                     "displayDescription=\"Number of threads to concurrently process outbound HTTP/SOAP requests\" " +
                                     "isPasswordField=\"false\">10</componentConfig:OutboundThreads> " +
                                     
                        "<componentConfig:HttpDefaultPort displayName=\"Default HTTP port number\" " +
                                     "displayDescription=\"Default HTTP port number for inbound HTTP/SOAP requests\" " +
                                     "isPasswordField=\"false\">1111</componentConfig:HttpDefaultPort> " +
                                     
                        "<componentConfig:HttpsDefaultPort displayName=\"Default HTTPS port number\" " +
                                    "displayDescription=\"Default HTTPS port number for inbound HTTP/SOAP requests\" " +
                                     "isPasswordField=\"false\">2222</componentConfig:HttpsDefaultPort> " +
                                     
                        "<componentConfig:UseJVMProxySettings displayName=\"Use default JVM proxy settings\" " +
                                     "displayDescription=\"Determines whether or not to use the default JVM system properties for proxy settings\" " +
                                     "isPasswordField=\"false\">true</componentConfig:UseJVMProxySettings> " +
                                     
                        "<componentConfig:ProxyType displayName=\"Proxy type\" " +
                                     "displayDescription=\"Specifies a valid proxy type\" " +
                                     "isPasswordField=\"false\">SOCKS</componentConfig:ProxyType>" +
                                     
                        "<componentConfig:ProxyHost displayName=\"Proxy host\" " +
                                     "displayDescription=\"Specifies a valid proxy host name\" " +
                                     "isPasswordField=\"false\">dummy</componentConfig:ProxyHost>" +
                                     
                        "<componentConfig:ProxyPort displayName=\"Proxy port\" " +
                                     "displayDescription=\"Specifies a valid proxy port\" " +
                                     "isPasswordField=\"false\">3333</componentConfig:ProxyPort>" +
                                     
                        "<componentConfig:NonProxyHosts displayName=\"Non proxy hosts\" " +
                                     "displayDescription=\"Specifies a list of server hosts whose connections do not require a proxy server.\" " +
                                     "isPasswordField=\"false\">dummy</componentConfig:NonProxyHosts>" +
                                     
                        "<componentConfig:ProxyUserName displayName=\"Proxy user name\" " +
                                     "displayDescription=\"Specifies a valid proxy user name\" " +
                                     "isPasswordField=\"false\">dummy</componentConfig:ProxyUserName>" +
                                     
                        "<componentConfig:ProxyPassword displayName=\"Proxy user password\" " +
                                     "displayDescription=\"Specifies a valid proxy user password.\" " +
                                     "isPasswordField=\"true\">dummy</componentConfig:ProxyPassword>" +
            
                        "<componentConfig:ApplicationConfiguration>" +

                            "<componentConfig:configurationName displayName=\"Application Configuration Name\" " + 
                                                 "displayDescription=\"Specifies the name of the application configuration.\" " +
                                                 "isPasswordField=\"false\" isRequiredField=\"true\">dummy</componentConfig:configurationName >" +

                            "<componentConfig:connectionURL displayName=\"HTTP URL Location\" " + 
                                                 "displayDescription=\"Specifies the HTTP URL location.\" " +
                                                 "isPasswordField=\"false\" isRequiredField=\"true\">http://</componentConfig:connectionURL>" +

                            "<componentConfig:securityPrincipal displayName=\"HTTP URL Location\" " + 
                                                 "displayDescription=\"Specifies the HTTP URL location.\" " +
                                                 "isPasswordField=\"false\" isRequiredField=\"true\">http://</componentConfig:securityPrincipal>" +
            
            
                            "<componentConfig:securityCredential displayName=\"User Password\" " + 
                                                 "displayDescription=\"Specifies the user password.\" " +
                                                 "isPasswordField=\"true\" isRequiredField=\"true\">secret</componentConfig:securityCredential>" +

                            "<componentConfig:jndienv displayName=\"HTTP URL Location\" " + 
                                                 "displayDescription=\"Specifies the HTTP URL location.\" " +
                                                 "isPasswordField=\"false\" isRequiredField=\"true\">http://</componentConfig:jndienv>" +
            
                        "</componentConfig:ApplicationConfiguration>" +
                        "</componentConfig:Configuration>";
    
    
    private void initOpenTypes()
        throws javax.management.openmbean.OpenDataException
    {
            // Define the Application Variables CompositeType
            sAppVarItemNames = new String[] {"name", 
                                "value", 
                                "type" };

            String[] appVarItemDesc = { "Application variable name", 
                                        "Application variable value", 
                                        "Application variable type" };

            OpenType[] appVarRowAttrTypes = { 
                                        SimpleType.STRING, 
                                        SimpleType.STRING, 
                                        SimpleType.STRING };

            sappVarRowIndex = new String[]{ "name" };


            sAppVarComposite = new CompositeType("ApplicationVariableComposite",
                                                  "Application variable name and value pair",
                                                   sAppVarItemNames,
                                                   appVarItemDesc,
                                                   appVarRowAttrTypes);

            sAppVarTable = new TabularType("ApplicationVariableList",
                                           "List of application variables",
                                                    sAppVarComposite,
                                                    sappVarRowIndex);
            
            // Define the Application Configuration OpenType
            sAppConfigItemNames = new String[] {"configurationName", 
                                "connectionURL", 
                                "securityPrincipal",
                                "securityCredential",
                                "jndienv"};

            String[] appConfigItemDesc = { "Application configuration name", 
                                        "Connection URL", 
                                        "Security Principal",
                                        "Security Credential",
                                        "JNDI Properties"};

                                        
            ArrayType jndiEnvArrayType = new ArrayType(1, SimpleType.STRING);                            
            OpenType[] appConfigRowAttrTypes = { 
                                        SimpleType.STRING, 
                                        SimpleType.STRING, 
                                        SimpleType.STRING,
                                        SimpleType.STRING, 
                                        jndiEnvArrayType};

            sAppConfigRowIndex = new String[]{ "configurationName" };


            sAppConfigComposite = new CompositeType("ApplicationConfigurationComposite",
                                                  "Application Configuration Composite",
                                                   sAppConfigItemNames,
                                                   appConfigItemDesc,
                                                   appConfigRowAttrTypes);

            sAppConfigTable = new TabularType("ApplicationConfigurationList",
                                           "List of application configurations",
                                                    sAppConfigComposite,
                                                    sAppConfigRowIndex);
    }
    
    
    /**
     * HostName gettter/setter
     */
    public void setHostName(String hostName)
    {
        mHostName = hostName;
    }
    
    public String getHostName()
    {
        return mHostName;
    }
    
    /**
     * Port get/set
     */
    public void setPort(int port)
    {
        mPort = port;
    }
    public int getPort()
    {
        return mPort;
    }
    
    /**
     * ProxyPassword - secure field
     */
    public void setProxyPassword(String proxyPassword)
    {
        mProxyPwd = proxyPassword;    
    }
    
    public String getProxyPassword()
    {
        return mProxyPwd;
    }
    
    public MissingConfig(String info)
    {
        mInfo = info;
        try
        {
            initOpenTypes();
            mAppVars = new TabularDataSupport(sAppVarTable);
            mAppConfigs = new TabularDataSupport(sAppConfigTable);
            
            // Add a default app var
            CompositeDataSupport cd = new CompositeDataSupport(sAppVarComposite,
                sAppVarItemNames, new String[] {"default", "defValue", "STRING"});
                mAppVars.put(cd);
                
           // Add a default app config
           CompositeDataSupport appCfgCD = new CompositeDataSupport(sAppConfigComposite,
                sAppConfigItemNames, new Object[] {"defaultConfig", "http://www.sun.com", 
                    "administrator", "abc", new String[]{"env1=value1", "env2=value2"}});
           mAppConfigs.put(appCfgCD);     
        }
        catch ( Exception ex )
        {
            System.out.println("Error in populating the Application Variables");
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * Test method.
     */
    public String getComponentCustomInfo()
    {
        return mInfo;
    }
        
    
     /**
      * Get the CompositeType definition for the components application configuration 
      *
      * @return the CompositeType for the components application configuration.
      */
     public CompositeType queryApplicationConfigurationType()
     {
         return sAppConfigComposite;
     }
     
      /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appConfig - application configuration composite 
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public void addApplicationConfiguration(String name, CompositeData appConfig) throws MBeanException
     {
         mAppConfigs.put(appConfig);
     }
    
     /**
      * Delete an application configuration. 
      *
      * @param name - identification of the application configuration to be deleted
      * @throws MBeanException if the configuration cannot be deleted.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public void deleteApplicationConfiguration(String name) throws MBeanException
     {
        CompositeData cd = (CompositeData) mAppConfigs.get(new String[]{name});
        
        if ( cd != null )
        {
            mAppConfigs.remove(new String[]{name});
        }
     }
     
    
    /**
     * Get a Map of all application configurations for the component.
     *
     * @return a TabularData of all the application configurations for a 
     *         component keyed by the configuration name. 
     */
    public TabularData getApplicationConfigurations()
    {
        return mAppConfigs;
    }
    
     
    /**
     * This operation sets an application variable. If a variable does not exist with 
     * the same name, its an error.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste to be updated.
     * @throws MBeanException if one or more application variables cannot be deleted
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public void setApplicationVariable(String name, CompositeData appVar) 
        throws MBeanException
    {
        CompositeData cd = (CompositeData) mAppVars.get(new String[]{name});
        
        if ( cd != null )
        {
            mAppVars.remove(new String[]{name});
            mAppVars.put(appVar);
        }
    }
     
    /**
     * This operation deletes an application variable, if a variable with the specified name does
     * not exist, it's an error.
     *
     * @param name - name of the application variable
     * @throws MBeanException on errors.
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public void deleteApplicationVariable(String name) throws MBeanException
    {
        mAppVars.remove(new String[]{name});
    }
     
     /**
      * Get the Application Variable set for a component.
      *
      * @return  a TabularData which has all the applicationvariables set on the component. 
      */
     public TabularData getApplicationVariables()
     {
        return mAppVars;
     }
     
     /** 
     * Retrieves the component specific configuration schema.
     *
     * @return a String containing the configuration schema.
     */
    public String retrieveConfigurationDisplaySchema()
    {
        return CONFIG_XML;
    }
    
    /** 
     * Retrieves the component configuration metadata.
     * The XML data conforms to the component 
     * configuration schema.
     *
     * @return a String containing the configuration metadata.
     */
    public String retrieveConfigurationDisplayData()
    {
        return CONFIG_XML;
    }
}

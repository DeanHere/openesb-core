#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01255.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
#### NOTE: depends on jbiadmin01200.ksh 
####

####
#jbiadmin01255 - Test fix for 6536737 - Sync. in clustered instance
#this test covers the use case of adding a new instance to a cluster with existing deployments
#start/stop operations are performed after the new instance is added and started and verified that 
#the service units were synchronized to the new instance.
####

echo testname is jbiadmin01255
. ./regress_defs.ksh

asadmin create-cluster  --port $ASADMIN_PORT --user $AS_ADMIN_USER $ASADMIN_PW_OPTS testcluster1 1>&2
createClusterDelay

asadmin create-instance --port $ASADMIN_PORT --user $AS_ADMIN_USER $ASADMIN_PW_OPTS --cluster testcluster1 --nodeagent agent1 testins1 1>&2
createInstanceDelay
asadmin start-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testins1 1>&2
startInstanceDelay

asadmin install-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --target=testcluster1 --port $ASADMIN_PORT $JV_JBI_HOME/components/sun-http-binding/httpbc.jar 2>&1
installComponentDelay

asadmin deploy-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=testcluster1 $JV_SVC_TEST_CLASSES/dist/system-comps-sa.jar 2>&1
deploySaDelay
asadmin start-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=testcluster1 system-comps-sa 2>&1
startSaDelay
asadmin stop-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=testcluster1 system-comps-sa 2>&1
stopSaDelay

asadmin create-instance --port $ASADMIN_PORT --user $AS_ADMIN_USER $ASADMIN_PW_OPTS --cluster testcluster1 --nodeagent agent1 testins2 1>&2
createInstanceDelay
asadmin start-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testins2 1>&2
startInstanceDelay

asadmin start-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=testcluster1 system-comps-sa 2>&1
startSaDelay
asadmin stop-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=testcluster1 system-comps-sa 2>&1
stopSaDelay

asadmin stop-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testins1 1>&2
stopInstanceDelay
asadmin stop-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testins2 1>&2
stopInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testins1 1>&2
deleteInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testins2 1>&2
deleteInstanceDelay
asadmin delete-cluster --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testcluster1 1>&2
deleteClusterDelay

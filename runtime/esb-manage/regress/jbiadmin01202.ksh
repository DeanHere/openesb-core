#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01202.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin01202 - System Components Test in cluster

echo testname is jbiadmin01202
. ./regress_defs.ksh

#create sa needed for deployment test
ant -q  -lib "$REGRESS_CLASSPATH" -f jbiadmin01200.xml 1>&2

echo list, start, stop test for cluster
asadmin  list-jbi-shared-libraries --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 2>&1
asadmin  list-jbi-binding-components  --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 2>&1
asadmin  list-jbi-service-engines --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 2>&1

#asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 sun-javaee-engine 2>&1
#asadmin stop-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 sun-javaee-engine 2>&1

asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 $JV_JBI_HOME/components/sun-javaee-engine/appserv-jbise.jar 2>&1
installComponentDelay

echo deploy test on cluster
asadmin  list-jbi-service-engines  --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --lifecyclestate=started --target=CAS-cluster1 2>&1

asadmin deploy-jbi-service-assembly --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 $JV_SVC_TEST_CLASSES/dist/system-comps-javaee-su-sa.jar 2>&1
deploySaDelay

asadmin  list-jbi-service-engines  --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --lifecyclestate=started --target=CAS-cluster1 2>&1

echo undeploy, uninstall test on cluster
asadmin undeploy-jbi-service-assembly --target=CAS-cluster1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT system-comps-javaee-su-sa 2>&1
undeploySaDelay

asadmin stop-jbi-component --target=CAS-cluster1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 2>&1
stopComponentDelay

asadmin shut-down-jbi-component --target=CAS-cluster1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 2>&1
stopComponentDelay

asadmin uninstall-jbi-component --target=CAS-cluster1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-javaee-engine 2>&1
uninstallComponentDelay

#test install from domain for cluster target
echo testing install from domain
$JBI_ANT_NEG -Djbi.target=CAS-cluster1 -Djbi.component.name=sun-javaee-engine install-component
stopComponentDelay

asadmin shut-down-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 2>&1
stopComponentDelay

asadmin uninstall-jbi-component --target=instance1 --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-javaee-engine 2>&1
uninstallComponentDelay

asadmin uninstall-jbi-component --target=sync --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-javaee-engine 2>&1
uninstallComponentDelay

#This is needed because sun-javaee-engine is started in DAS when it is targeted for deployment in any target
asadmin stop-jbi-component --target=server --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 2>&1
stopComponentDelay
asadmin shut-down-jbi-component --target=server --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 2>&1
stopComponentDelay
asadmin uninstall-jbi-component --target=server --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS  --port $ASADMIN_PORT sun-javaee-engine 2>&1
uninstallComponentDelay

echo reinstall test on cluster
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_JBI_HOME/components/sun-javaee-engine/appserv-jbise.jar 2>&1
installComponentDelay

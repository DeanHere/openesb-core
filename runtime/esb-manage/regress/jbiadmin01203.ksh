#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01203.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin01203

echo testname is jbiadmin01203
. ./regress_defs.ksh

echo test install form domain for shared libraries in cluster target after --keeparchive=true
asadmin  list-jbi-shared-libraries  --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 2>&1
asadmin uninstall-jbi-shared-library --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=CAS-cluster1 --keeparchive=true sun-wsdl-library 2>&1
$JBI_ANT_NEG -Djbi.target=CAS-cluster1 -Djbi.shared.library.name=sun-wsdl-library install-shared-library 2>&1

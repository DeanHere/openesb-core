/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEventForwarderMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.ParserAdapter;
import org.xml.sax.helpers.XMLReaderFactory;

import com.sun.enterprise.deployment.backend.DeploymentStatus;



public class TestEventForwarder  implements NotificationListener{


	private static final String EVENT_TYPE_ALERT = "Alert";
	private static final String SERVER_TYPE_INTEGRATION = "INTEGRATION";
    private static final String PHOST_COMPDATA_ELEMENT ="EVENTPHYSICALHOST";
    private static final String SERVERTYPE_COMPDATA_ELEMENT = "EVENTSERVERTYPE";
    private static final String COMPONENTTYPE_TEST_VALUE = "EventManagement";
    private static final String COMPONENTTYPE_COMPDATA_ELEMENT = "EVENTCOMPONENTTYPE";
    private static final String TYPE_COMPDATA_ELEMENT ="EVENTTYPE";
    private static final String SEVERITY_COMPDATA_ELEMENT ="EVENTSEVERITY";
    private static final int SEVERITY_TEST_VALUE = 1;
    private static final String MSGCODE_TEST_VALUE = "CUSTOM-00001";
    private static final String MSGCODE_COMPDATA_ELEMENT ="EVENTMESSAGECODE";
    private static final String MSGDETAILS_TEST_VALUE = "This is single event test";
    private static final String MSGDETAILS_COMPDATA_ELEMENT ="EVENTMESSAGEDETAILS";

    private static final String DOMAIN_XML_CONFIGS_ELEMENT ="configs";
    private static final String DOMAIN_XML_CONFIG_ELEMENT ="config";
    private static final String DOMAIN_XML_SYSTEM_PROPERTY_ELEMENT ="system-property";
    private static final String DOMAIN_XML_NAME_ATTRIBUTE ="name";
    private static final String DOMAIN_XML_VALUE_ATTRIBUTE ="value";
    private static final String DOMAIN_XML_INSTANCE1_CONFIG ="instance1-config";
    private static final String DOMAIN_XML_JMX_PORT_NAME ="JMX_SYSTEM_CONNECTOR_PORT";

    private enum CURRENT_DOMAIN_XML_ELEMENT { NONE,CONFIGS_ELEMENT,
        CONFIG_ELEMENT,SYSTEM_PROPERTY_ELEMENT};

    private static final String EVENTMANAGEMENT_CONTROL_MBEAN_NAME = "EventManagement:name=EventManagementControllerMBean";
    private static final String FORWARDER_MBEAN_NAME = "EventManagement:name=EventForwarderMBean";
    private static final String EVENTMANAGEMENT_CHANNEL_MBEAN_NAME = "EventManagement:name=EventManagementChannelSupportMBean";
    private static final String GF_CONFIG_MBEAN_NAME = "com.sun.appserv:type=applications,category=config";
    private static final String JBI_INSTALL_ROOT_ATTRIBUTE_NAME = "JBIInstallRoot";

    private static final String DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME = "alertTableName";
    private static final String EVENT_TEST_OPERATION = "testSendEvent";
    private static final String DATABASE_TYPE_ATTRIBUTE_NAME = "DBType";

    private static final String AVAILABILITY_ENABLED         = "availabilityenabled";
    private static final String DEFAULT_ENABLE               = "true";
    private static final String ARCHIVE_NAME                 = "archiveName";
    private static final String NAME                         = "name";
    private static final String TYPE                         = "type";
    private static final String APP_NAME                 = "TestEventManagement";
    private static final String APP_TYPE                 = "web";


    private static final String DATABASE_TYPE_TEST_VALUE = "ORACLE";
    private static final String DATABASE_TABLE_TEST_VALUE = "TestTable";

    private MBeanServerConnection mCASMBeanServerConnection;
    private MBeanServerConnection mInstanceMBeanServerConnection;
    private static final String RESULT_PREFIX = "##### Result of ";

    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String TARGET   = "target";
    private static final String SUPPORT_APP_LOC = "test.support.app";
    private static final String INSTANCE_NAME = "instance.target.name";
    private static final String INSTANCE_JMX_PORT = "instance.jmx.port";

    private ObjectName mForwarderObjectName;
    private ObjectName mForwarderSupportObjectName;
    private ObjectName mForwarderControllerObjectName;
    private ObjectName mConfigObjectName;
    private CompositeData  event;
    private int failureCount;
    private ExecutorService mExecutorService;
    private NotificationListener mNotificationListener;
    private boolean exit= false;
    private String mDasConfigFile;
    private CURRENT_DOMAIN_XML_ELEMENT mCurrentElement = CURRENT_DOMAIN_XML_ELEMENT.NONE;
    private String mInstanceJmxPort;


    public TestEventForwarder (String aJmxURL,String aSupportAppLocation)
    throws Exception
    {
        System.setProperty(USER, "admin");
        System.setProperty(PASSWORD, "adminadmin");
        System.setProperty(PROVIDER, aJmxURL);
        System.setProperty(TARGET, "server");
//        System.setProperty(INSTANCE_NAME, "instance1");
//        System.setProperty(INSTANCE_JMX_PORT, "8690");
        System.setProperty(SUPPORT_APP_LOC, aSupportAppLocation);


        getObjectNames();
    	getMBeanServerConnections();
    	mNotificationListener = new EventHandler();
        mExecutorService = Executors.newFixedThreadPool(2);

    }

    public TestEventForwarder () throws Exception
    {
        getObjectNames();
    	getMBeanServerConnections();
    	mNotificationListener = new EventHandler();
        mExecutorService = Executors.newFixedThreadPool(2);

     }

    private void getMBeanServerConnections()  throws Exception{

    	java.util.Map<String, String[]> env = new java.util.HashMap<String, String[]>();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);

        String jmxProvider = System.getProperty(PROVIDER);
        JMXServiceURL casJmxServiceUrl = new JMXServiceURL(jmxProvider);
        JMXConnector connector = JMXConnectorFactory.connect(casJmxServiceUrl, env);
    	mCASMBeanServerConnection = connector.getMBeanServerConnection();
    	boolean isInstanceTest = System.getProperty(INSTANCE_NAME)!= null ? true : false;

    	if(isInstanceTest) {
                // get Instance jmx port from domain.xml
    	        getInstance1PortFromdomainDotXMLFile();
        	// get connection to instance1
    		JMXServiceURL instnaceJmxURL = new JMXServiceURL(constructInstanceJmxURL(jmxProvider));

        	JMXConnector instanceConnector = JMXConnectorFactory.connect(instnaceJmxURL, env);
        	mInstanceMBeanServerConnection = instanceConnector.getMBeanServerConnection();
                System.out.println("get mbean connection to  " + System.getProperty(INSTANCE_NAME));
    	}
    }

    private String constructInstanceJmxURL(String jmxProvider){
    	String urlPostFix = "/management/rmi-jmx-connector";
    	String subURL = jmxProvider.substring(0, jmxProvider.indexOf(urlPostFix));
    	int portNumberSepIndex = subURL.lastIndexOf(":");
    	String portNumber = subURL.substring(portNumberSepIndex+1);
    	String instanceJMXUrl = jmxProvider.replace(portNumber, mInstanceJmxPort);
    	return instanceJMXUrl;

    }

    private void getObjectNames()
	{
	    try {
			mForwarderObjectName = new ObjectName(FORWARDER_MBEAN_NAME);
		} catch (MalformedObjectNameException e) {
			System.out.println(FORWARDER_MBEAN_NAME + "is invalid object name");
		} catch (NullPointerException e) {
		}
	    try {
			mForwarderSupportObjectName = new ObjectName(EVENTMANAGEMENT_CHANNEL_MBEAN_NAME);
		} catch (MalformedObjectNameException e) {
			System.out.println(EVENTMANAGEMENT_CHANNEL_MBEAN_NAME + "is invalid object name");
	                failureCount++;
		} catch (NullPointerException e) {
	                failureCount++;
		}


                try {
			mConfigObjectName = new ObjectName(GF_CONFIG_MBEAN_NAME);
                } catch (MalformedObjectNameException e) {
			System.out.println(GF_CONFIG_MBEAN_NAME + "is invalid object name");
                } catch (NullPointerException e) {
                }

            try {
                    mForwarderControllerObjectName = new ObjectName(this.EVENTMANAGEMENT_CONTROL_MBEAN_NAME);
            } catch (MalformedObjectNameException e) {
                    System.out.println(FORWARDER_MBEAN_NAME + "is invalid object name");
            } catch (NullPointerException e) {
            }
        }


    private void disablePersistence() {
        try {
        	Attribute persistenceAttribute = new Attribute("enableTargetPersistence",false);
			mCASMBeanServerConnection.setAttribute(mForwarderObjectName, persistenceAttribute);
		} catch (InstanceNotFoundException e) {
			System.out.println("Mbean with object name '"+ FORWARDER_MBEAN_NAME + "' instance not found : \n" + e );
	                failureCount++;
		} catch (MBeanException e) {
			System.out.println("Mbean with object name '"+ FORWARDER_MBEAN_NAME + "' " + EVENT_TEST_OPERATION
						+" mathod throw exception: \n" + e);
	                failureCount++;
		} catch (ReflectionException e) {
			System.out.println("Mbean with object name '"+ FORWARDER_MBEAN_NAME + "' reflection exception: \n" + e);
	                failureCount++;
		} catch (IOException e) {
			System.out.println("communication to Mbean with object name '" + FORWARDER_MBEAN_NAME + "' failed\n" + e);
	                failureCount++;
		} catch (AttributeNotFoundException e) {
			System.out.println("Mbean with object name '"+ FORWARDER_MBEAN_NAME + "' does not have attribute named 'enableTargetPersistence'");
	                failureCount++;
		} catch (InvalidAttributeValueException e) {
			System.out.println("Mbean with object name '"+ FORWARDER_MBEAN_NAME + "' attribute named 'enableTargetPersistence' is not boolean type");
	                failureCount++;
		}

    }

    private boolean addEventListener() {
    	boolean added = false;
    	// add the inner classs as a listener
    	try {
    		mCASMBeanServerConnection.addNotificationListener(mForwarderSupportObjectName, mNotificationListener, null, new Object());
			System.out.println("Added notification listener ");
			return true;
		} catch (InstanceNotFoundException e) {
			System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CHANNEL_MBEAN_NAME + "' instance not found : \n" + e );
    		failureCount++;
    		return added;
		} catch (IOException e) {
			System.out.println("communication to Mbean with object name '" + EVENTMANAGEMENT_CHANNEL_MBEAN_NAME + "' failed\n" + e);
    		failureCount++;
    		return added;
		}


    }

    private void testSendEvent()

    {
    	MBeanServerConnection aMBeanServerConnection = null;
    	String serverName = null;
    	if(System.getProperty(INSTANCE_NAME) == null) {
    		serverName =  System.getProperty(TARGET);
    		aMBeanServerConnection = mCASMBeanServerConnection;
    	} else {
    		serverName =  System.getProperty(INSTANCE_NAME);
    		aMBeanServerConnection = this.mInstanceMBeanServerConnection;
    	}

    	Object[] params = {MSGDETAILS_TEST_VALUE,SEVERITY_TEST_VALUE};
        String[] signature ={"java.lang.String","java.lang.Integer"};
        try {
        	aMBeanServerConnection.invoke(mForwarderObjectName, EVENT_TEST_OPERATION,params,signature);
			System.out.println("Invoked test send event");
		} catch (InstanceNotFoundException e) {
			System.out.println(serverName + " - Mbean with object name '"+ FORWARDER_MBEAN_NAME + "' instance not found : \n" + e );
    		failureCount++;
    		return;
		} catch (MBeanException e) {
			System.out.println(serverName + " - Mbean with object name '"+ FORWARDER_MBEAN_NAME + "' " + EVENT_TEST_OPERATION
						+" mathod throw exception: \n" + e);
    		failureCount++;
    		return;
		} catch (ReflectionException e) {
			System.out.println(serverName + " - Mbean with object name '"+ FORWARDER_MBEAN_NAME + "' reflection exception: \n" + e);
    		failureCount++;
    		return;
		} catch (IOException e) {
			System.out.println(serverName + " - communication to Mbean with object name '" + FORWARDER_MBEAN_NAME + "' failed\n" + e);
    		failureCount++;
    		return;
		}
		int count =0;
        try {
        	while (!exit && count <100) {
			 Thread.currentThread().sleep(1000);
        	}
		} catch (InterruptedException e) {
			System.out.println(" sleep was interrupted\n" + e);
		}

		if(event == null ) {
	    	exit= true;
			System.out.println(" ERROR: no event received");
			removeNotifoicationListener();
    		failureCount++;
			return;
		}

		verifyEvent( System.getProperty(INSTANCE_NAME) == null);
    }

    private void verifyEvent(boolean isDAS) {
                System.out.println("verifying received event data");
                String serverName = isDAS == true ? System.getProperty(TARGET) : System.getProperty(INSTANCE_NAME);
		checkEventElement("Server Type",SERVER_TYPE_INTEGRATION,
				(String)event.get(SERVERTYPE_COMPDATA_ELEMENT));
		checkEventElement("Component Type",COMPONENTTYPE_TEST_VALUE,
				(String)event.get(COMPONENTTYPE_COMPDATA_ELEMENT));
		checkEventElement("event Type", EVENT_TYPE_ALERT,
				(String)event.get(TYPE_COMPDATA_ELEMENT));
		checkEventElement("Severity value",(SEVERITY_TEST_VALUE+""),
				(String)event.get(SEVERITY_COMPDATA_ELEMENT));
		checkEventElement("Message Code",MSGCODE_TEST_VALUE,
				(String)event.get(MSGCODE_COMPDATA_ELEMENT));
		checkEventElement("Message Details", MSGDETAILS_TEST_VALUE,
				(String)event.get(MSGDETAILS_COMPDATA_ELEMENT));

    }

    private void checkEventElement(String elementName,String expectedValue,String returnValue) {
    	if(expectedValue.trim().equals(returnValue.trim())) {
    		System.out.println(" element " + elementName + " value : equal to " + expectedValue + " (OK)");
    	} else {
    		System.out.println(" element " + elementName + " expected value = " + expectedValue
    				+ " found " + returnValue);
    		failureCount++;

    	}
    }

	public void handleNotification(Notification notification, Object handback) {
		  event = (CompositeData)notification.getUserData();
  		  exit = true;
	}

	private void removeNotifoicationListener() {
    	// add this class as a listener
    	try {
			mCASMBeanServerConnection.removeNotificationListener(mForwarderSupportObjectName, mNotificationListener);
			System.out.println("Removed notification listener ");
		} catch (InstanceNotFoundException e) {
			System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CHANNEL_MBEAN_NAME + "' instance not found : \n" + e );
    		failureCount++;
    		return;
		} catch (IOException e) {
			System.out.println("communication to Mbean with object name '" + EVENTMANAGEMENT_CHANNEL_MBEAN_NAME + "' failed\n" + e);
    		failureCount++;
    		return;
		} catch (ListenerNotFoundException e) {
			System.out.println("should not get here as this class is the listener");
    		failureCount++;
    		return;
		}

	}

	private boolean  deploySupportApp() {
		String webappLocation = System.getProperty(SUPPORT_APP_LOC);
		String[] targets = new String[] {System.getProperty(TARGET)};
        Properties deploymentProperties = new Properties();
        deploymentProperties.setProperty(ARCHIVE_NAME, webappLocation);
        deploymentProperties.setProperty(AVAILABILITY_ENABLED, DEFAULT_ENABLE);

        Object[] params = new Object[2];
        params[0] = deploymentProperties;
        params[1] = targets;

        String[] signatures = new String[2];
        signatures[0] = "java.util.Properties";
        // [Ljava.lang.String;
        signatures[1] = targets.getClass().getName();

        try {
	        DeploymentStatus deploymentStatus = (DeploymentStatus) mCASMBeanServerConnection.invoke(
	        		mConfigObjectName, "deploy", params, signatures);
	        if (deploymentStatus.getStatus() == DeploymentStatus.FAILURE) {
				System.out.println("test support application failed to deploy\n"+
						prepareDeploymentResult(deploymentStatus));
	    		failureCount++;
	    		return false;
	        }
	        if (deploymentStatus.getStatus() == DeploymentStatus.NOTINITIALIZED) {
				System.out.println("deployment of test support application not initialized\n" +
						prepareDeploymentResult(deploymentStatus));
	    		failureCount++;
	    		return false;
	        }
	        if (deploymentStatus.getStatus() == DeploymentStatus.SUCCESS) {
				System.out.println("deployment of test support application successful");
	        }
	        if (deploymentStatus.getStatus() == DeploymentStatus.WARNING) {
				System.out.println("deployment of test support application successful with warning" +
						prepareDeploymentResult(deploymentStatus));
	        }
		} catch (InstanceNotFoundException e) {
			System.out.println("Mbean with object name '"+ GF_CONFIG_MBEAN_NAME + "' instance not found : \n" + e );
    		failureCount++;
    		return false;
		} catch (MBeanException e) {
			System.out.println("Mbean with object name '"+ GF_CONFIG_MBEAN_NAME + "' " + EVENT_TEST_OPERATION
						+" mathod throw exception: \n" + e);
    		failureCount++;
    		return false;
		} catch (ReflectionException e) {
			System.out.println("Mbean with object name '"+ GF_CONFIG_MBEAN_NAME + "' reflection exception: \n" + e);
    		failureCount++;
    		return false;
		} catch (IOException e) {
			System.out.println("communication to Mbean with object name '" + GF_CONFIG_MBEAN_NAME + "' failed\n" + e);
    		failureCount++;
    		return false;
		}
        return true;
	}

	private boolean  undeploySupportApp() {
		String[] targets = new String[] {System.getProperty(TARGET)};
        Properties deploymentProperties = new Properties();
        deploymentProperties.setProperty(NAME, APP_NAME);
        deploymentProperties.setProperty(TYPE, APP_TYPE);

        Object[] params = new Object[2];
        params[0] = deploymentProperties;
        params[1] = targets;

        String[] signatures = new String[2];
        signatures[0] = "java.util.Properties";
        // [Ljava.lang.String;
        signatures[1] = targets.getClass().getName();

        try {
	        DeploymentStatus deploymentStatus = (DeploymentStatus) mCASMBeanServerConnection.invoke(
	        		mConfigObjectName, "undeploy", params, signatures);
	        if (deploymentStatus.getStatus() == DeploymentStatus.FAILURE) {
				System.out.println("test support application failed to undeploy\n" +
						prepareDeploymentResult(deploymentStatus));
	    		failureCount++;
	    		return false;
	        }
	        if (deploymentStatus.getStatus() == DeploymentStatus.NOTINITIALIZED) {
				System.out.println("undeployment of test support application not initialized\n" +
						prepareDeploymentResult(deploymentStatus));
	    		failureCount++;
	    		return false;
	        }
	        if (deploymentStatus.getStatus() == DeploymentStatus.SUCCESS) {
				System.out.println("undeployment of test support application successful");
	        }
	        if (deploymentStatus.getStatus() == DeploymentStatus.WARNING) {
				System.out.println("undeployment of test support application successful with warning\n" +
						prepareDeploymentResult(deploymentStatus));
	        }
		} catch (InstanceNotFoundException e) {
			System.out.println("Mbean with object name '"+ GF_CONFIG_MBEAN_NAME + "' instance not found : \n" + e );
    		failureCount++;
    		return false;
		} catch (MBeanException e) {
			System.out.println("Mbean with object name '"+ GF_CONFIG_MBEAN_NAME + "' " + EVENT_TEST_OPERATION
						+" mathod throw exception: \n" + e);
    		failureCount++;
    		return false;
		} catch (ReflectionException e) {
			System.out.println("Mbean with object name '"+ GF_CONFIG_MBEAN_NAME + "' reflection exception: \n" + e);
    		failureCount++;
    		return false;
		} catch (IOException e) {
			System.out.println("communication to Mbean with object name '" + GF_CONFIG_MBEAN_NAME + "' failed\n" + e);
    		failureCount++;
    		return false;
		}
        return true;
	}

    private String prepareDeploymentResult(DeploymentStatus deploymentStatus) {
        List<String> statusMessages = new ArrayList<String>();

        String stageStatusMessage = deploymentStatus.getStageStatusMessage();
        if ((stageStatusMessage != null)
                && (stageStatusMessage.trim().length() > 0)) {
             statusMessages.add("Main (Un)Deployment Stage Status is: "
                    + stageStatusMessage);
        }

        DeploymentStatus statusElement = null;
        Iterator<DeploymentStatus> iterator = deploymentStatus.getSubStages();
        int index = 0;
        while ((iterator != null) && (iterator.hasNext())) {
            statusElement = (DeploymentStatus) iterator.next();
            if (statusElement != null) {
                if (statusElement.getStageIdentifier() != null) {
                    //logger.finest("Stage "+(++index)+" :"+statusElement.getStageIdentifier());
                }
                if (statusElement.getStageStatusMessage() != null) {
                    String statusMessage = statusElement
                            .getStageStatusMessage();
                    String message = "Stage " + (++index) + " Status is: "
                            + statusMessage;
                    //logger.finest(message);
                    if ((statusMessage != null)
                            && (statusMessage.trim().length() > 0)) {
                        statusMessages.add(message);
                    }
                }
            }
        }
        StringBuffer result = new StringBuffer();
        for (String statusMessage : statusMessages) {
        	result.append(statusMessage + "\n");
		}
        return result.toString();
    }


	private void tearDown() {
		removeNotifoicationListener();
		undeploySupportApp();
	}

    private class EventHandler implements Runnable,NotificationListener {
        public void run() {
            try {
            	while(!exit) {
            		Thread.currentThread().wait(100);
            	}
    		} catch (InterruptedException e) {
    		}
      }

      public void handleNotification(Notification notification, Object handback) {
    		event = (CompositeData)notification.getUserData();
     		  exit = true;
     	}

     }
    private void getInstance1PortFromdomainDotXMLFile() {

        try {
            String jbiRoot =  (String)mCASMBeanServerConnection.getAttribute(mForwarderControllerObjectName,
                    JBI_INSTALL_ROOT_ATTRIBUTE_NAME);
            mDasConfigFile = jbiRoot + File.separator + "../../config/domain.xml";
        } catch (InstanceNotFoundException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' instance not found : \n" + e );
                failureCount++;
                return;
        } catch (MBeanException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' getAttribute"
                                        +" mathod throw exception: \n" + e);
                failureCount++;
                return;
        } catch (ReflectionException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' reflection exception: \n" + e);
                failureCount++;
                return;
        } catch (IOException e) {
                System.out.println("communication to Mbean with object name '" + EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' failed\n" + e);
                failureCount++;
                return;
        } catch (AttributeNotFoundException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' does not have attribute named 'enableTargetPersistence'");
                failureCount++;
                return;
        }

        try {
            FileInputStream fis = new FileInputStream(mDasConfigFile);
            InputSource is = new InputSource(fis);
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(new XMLParsingHandler());
            xmlReader.parse(is);

        } catch (IOException e) {
                System.out.println("communication to Mbean with object name '" + EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' failed\n" + e);
                failureCount++;
        }  catch (SAXException e) {
            System.out.println("SAX Exception " + e.getMessage());
            failureCount++;
        }


    }
	private void runTest() {
		disablePersistence();
		deploySupportApp();
		if(addEventListener()) {
			testSendEvent();
		}
		if(failureCount == 0) {
	        System.out.println(RESULT_PREFIX + " test event forwarding passed" );
		} else {
	        System.out.println(RESULT_PREFIX + " test event forwarding failed" );

		}
		tearDown();
	}


        private class XMLParsingHandler implements ContentHandler {

            public void characters(char[] ch, int start, int length) throws SAXException {
            }

            public void endDocument() throws SAXException {
            }

            public void endElement(String uri, String localName, String qName) throws SAXException {
            }

            public void endPrefixMapping(String prefix) throws SAXException {
            }

            public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
            }

            public void processingInstruction(String target, String data) throws SAXException {
            }

            public void setDocumentLocator(Locator locator) {
            }

            public void skippedEntity(String name) throws SAXException {
            }

            public void startDocument() throws SAXException {
            }

            public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
                 switch (mCurrentElement) {
                     case NONE:
                       if(qName.equals(DOMAIN_XML_CONFIGS_ELEMENT)) {
                           mCurrentElement = CURRENT_DOMAIN_XML_ELEMENT.CONFIGS_ELEMENT;
                       }
                      break;
                     case CONFIGS_ELEMENT:
                         if(qName.equals(DOMAIN_XML_CONFIG_ELEMENT)) {
                             String value = atts.getValue(DOMAIN_XML_NAME_ATTRIBUTE);
                             // are we parsing instance1 config ?
                             if(value.equals(DOMAIN_XML_INSTANCE1_CONFIG)) {
                                 mCurrentElement = CURRENT_DOMAIN_XML_ELEMENT.CONFIG_ELEMENT;
                             }
                         }

                         break;
                     case CONFIG_ELEMENT:
                         // are we parsing system property in instance1 config ?
                         if(qName.equals(DOMAIN_XML_SYSTEM_PROPERTY_ELEMENT)) {
                             String name = atts.getValue(DOMAIN_XML_NAME_ATTRIBUTE);
                             String value = atts.getValue(DOMAIN_XML_VALUE_ATTRIBUTE);
                             if(name.equals(DOMAIN_XML_JMX_PORT_NAME)) {
                                 mInstanceJmxPort = value;
                                 mCurrentElement = CURRENT_DOMAIN_XML_ELEMENT.NONE;
//                                 System.out.println(" instnace1 jmx port = "+ mInstanceJmxPort);
                             }
                         }

                     break;

                default:
                    break;
                }
            }

            public void startPrefixMapping(String prefix, String uri) throws SAXException {
            }

        }


		public static void main(String[] args) {
			TestEventForwarder test;
			try {
				// use the following for standalone java test program
//                                test = new TestEventForwarder("service:jmx:rmi:///jndi/rmi://RDamir-tecra.stc.com:8689/management/rmi-jmx-connector",
//				"C:/cygwin/bld/open-esb/runtime/esb-manage/bld/test-classes/dist/TestEventManagement.war");
				test = new TestEventForwarder();
				test.runTest();
			} catch (Exception e) {
	    		System.out.println("unable to initailze test - " + e.getMessage());
			}


		}


}

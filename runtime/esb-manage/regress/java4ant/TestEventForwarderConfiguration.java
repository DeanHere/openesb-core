/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEventForwarderMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;





public class TestEventForwarderConfiguration {

   
    private static final String PERSISTENCE_ENABLED_PROP = "PersistenceEnabled";
    private static final String PERSISTENCE_ENABLED_DEFUALT_VAL = "false";
    
    private static final String JOURNAL_ENABLED_PROP = "journalEnabled";
    private static final String JOURNAL_ENABLED_DEFUALT_VAL = "false";
    
    private static final String POLICY_EXEC_ENABLED_PROP = "PersistencePolicyExecEnabled";
    private static final String POLICY_EXEC_ENABLED_DEFUALT_VAL = "false";
    private static final String POLICY_EXEC_ENABLED_TEST_VAL = "true";
    
    private static final String ALERT_COUNT_PROP ="PersistenceAlertCount";
    private static final String ALERT_COUNT_DEFUALT_VAL ="-1";
    private static final String ALERT_COUNT_TEST_VAL ="100";
    
    private static final String ALERT_AGE_PROP ="PersistenceAlertAge";
    private static final String ALERT_AGE_DEFUALT_VAL ="-1";
    private static final String ALERT_AGE_TEST_VAL ="100000";
    
    private static final String ALERT_LEVEL_PROP = "PersistenceAlertLevel";
    private static final String ALERT_LEVEL_DEFUALT_VAL = "NONE";
    private static final String ALERT_LEVEL_TEST_VAL = "FATAL";
    
    private static final String POLICY_EXEC_INTERVAL_PROP = "PersistencePolicyExecInterval";
    private static final String POLICY_EXEC_INTERVAL_DEFUALT_VAL = "600000";
    private static final String POLICY_EXEC_INTERVAL_TEST_VAL = "6000000";
    
    private static final String DATABASE_TYPE_PROP = "DatabaseType";
    private static final String DATABASE_TYPE_DEFUALT_VAL = "DERBY";
    private static final String DATABASE_TYPE_TEST_VAL = "ORACLE";

    private static final String DB_JNDI_NAME_PROP ="DBJndiName";
    private static final String DB_JNDI_NAME_DEFUALT_VAL ="AlertPersistenceDB";
    private static final String DB_JNDI_NAME_TEST_VAL ="TestAlertPersistenceDB";
    

    private static final String ALERT_TABLE_PROP ="AlertTableName";
    private static final String ALERT_TABLE_TEST_VAL = "AlertTest";


    private static final String PERSISTENCE_ENABLED_ATTRIBUTE_NAME = "alertsPersistenceEnabled";
    private static final String JOURNAL_ENABLED_ATTRIBUTE_NAME = "alertsJournalEnabled";
    private static final String ENABLE_PERSISTENCE_ATTRIBUTE_NAME = "enableTargetPersistence";
    private static final String ENABLE_POLICY_EXECUTION_ATTRIBUTE_NAME = "enablePolicyExcution";
    private static final String PERSISTED_ALERT_MAX_AGE_ATTRIBUTE_NAME = "persistedAlertMaxAge";
    private static final String PERSISTED_ALERT_MAX_COUNT_ATTRIBUTE_NAME = "persistedAlertMaxCount";
    private static final String PERSISTED_ALERT_LEVEL_ATTRIBUTE_NAME = "persistedAlertLevel";
    private static final String PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_ATTRIBUTE_NAME = "persistedAlertsRemovalPolicyExecInterval";
    private static final String MAX_CACHESIZE_ATTRIBUTE_NAME = "maxInMemoryEventCacheSize";
    private static final String DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME = "alertTableName";
    private static final String CURRENT_PERSISTED_EVENT_COUNT_ATTRIBUTE_NAME =
        "CurrentPersistedEventsCount";
    private static final String DATABASE_JNDI_NAME_ATTRIBUTE_NAME = "DBJndiName";
    private static final String DATABASE_TYPE_ATTRIBUTE_NAME = "DBType";
    private static final String TARGET_CHECK_INTERVAL_ATTRIBUTE_NAME ="tergetCheckInterval";
    
    private static final String EVENTMANAGEMENT_CONTROL_MBEAN_NAME = "EventManagement:name=EventManagementControllerMBean"; 
    private static final String DISABLE_PERSISTENCE_OPERATION = "disableAlertsPersistence"; 
    


    
 
    private MBeanServerConnection mCASMBeanServerConnection;
    private static final String RESULT_PREFIX = "##### Result of ";
    private static final String JBI_INSTALL_ROOT_ATTRIBUTE_NAME = "JBIInstallRoot";    
    
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String TARGET   = "target";
    private static final String INSTANCE_NAME = "instance.target.name";
    private static final String INSTANCE_JMX_PORT = "instance.jmx.port";
    

//    private ObjectName mForwarderObjectName;
    private ObjectName mForwarderControllerObjectName;
    private int failureCount;
    private String mDasEMConfigFile;
    private String mDasInstanceConfigFile;
    
   public TestEventForwarderConfiguration (String aJmxURL)
    throws Exception
    {
        System.setProperty(USER, "admin");
        System.setProperty(PASSWORD, "adminadmin");
        System.setProperty(PROVIDER, aJmxURL);
        System.setProperty(TARGET, "server");

        
    	getMBeanServerConnections();	
    	getObjectNames();
    	getJBIRootIntsall();
    }
   
    public TestEventForwarderConfiguration () throws Exception
    {
    	getMBeanServerConnections();	
    	getObjectNames();
        getJBIRootIntsall();
        
     }

    private void getMBeanServerConnections()  throws Exception{
        
    	java.util.Map<String, String[]> env = new java.util.HashMap<String, String[]>();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        JMXServiceURL casJmxServiceUrl = new JMXServiceURL(jmxProvider);
        JMXConnector connector = JMXConnectorFactory.connect(casJmxServiceUrl, env);
    	mCASMBeanServerConnection = connector.getMBeanServerConnection();
    	

    }
    
    private void getJBIRootIntsall() {
        
        try {
            String jbiRoot =  (String)mCASMBeanServerConnection.getAttribute(mForwarderControllerObjectName,
                    JBI_INSTALL_ROOT_ATTRIBUTE_NAME);
            mDasEMConfigFile = jbiRoot + File.separator + "eventmanagement.properties";
            mDasInstanceConfigFile = jbiRoot + File.separator + "../../../../nodeagents/agent1/instance1/jbi/config/eventmanagement.properties";
        } catch (InstanceNotFoundException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' instance not found : \n" + e );
                failureCount++;
        } catch (MBeanException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' getAttribute" 
                                        +" mathod throw exception: \n" + e);
                failureCount++;
        } catch (ReflectionException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' reflection exception: \n" + e);
                failureCount++;
        } catch (IOException e) {
                System.out.println("communication to Mbean with object name '" + EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' failed\n" + e);
                failureCount++;
        } catch (AttributeNotFoundException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' does not have attribute named 'enableTargetPersistence'");
                failureCount++;
        }
    }

    private void getObjectNames()
	{
	    try {
			mForwarderControllerObjectName = new ObjectName(this.EVENTMANAGEMENT_CONTROL_MBEAN_NAME);
		} catch (MalformedObjectNameException e) {
			System.out.println(EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "is invalid object name");
	                failureCount++;
		} catch (NullPointerException e) {
	                failureCount++;
		}
	   
	}


    private void disablePersistence() {
        try {
            mCASMBeanServerConnection.invoke(mForwarderControllerObjectName, 
                    DISABLE_PERSISTENCE_OPERATION,null,null);
         
	} catch (InstanceNotFoundException e) {
		System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' instance not found : \n" + e );
                failureCount++;
	} catch (MBeanException e) {
		System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' " + DISABLE_PERSISTENCE_OPERATION
					+" mathod throw exception: \n" + e);
                failureCount++;
	} catch (ReflectionException e) {
		System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' reflection exception: \n" + e);
                failureCount++;
	} catch (IOException e) {
		System.out.println("communication to Mbean with object name '" + EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' failed\n" + e);
                failureCount++;
	}    	
    }

     
     
    private void verifyEventConfiguration(Properties aTestProps,boolean isDAS,boolean aTestData) {
                
                if(isDAS && !aTestData) {
                    verifyDASDefaultEventConfiguration(aTestProps);
                } else if(isDAS && aTestData) {
                    verifyDASTestEventConfiguration(aTestProps);
                } else if(!isDAS && !aTestData) {
                    verifyInstanceDefaultEventConfiguration(aTestProps);
                } else if(isDAS && !aTestData) {
                    verifyInstanceTestEventConfiguration(aTestProps);
                } 
    }

    private void verifyDASDefaultEventConfiguration(Properties aTestProps) {
        System.out.println("verifying 'server' default configuration data");
        checkEventConfigurationElement(PERSISTENCE_ENABLED_PROP,PERSISTENCE_ENABLED_DEFUALT_VAL,
                (String)aTestProps.get(PERSISTENCE_ENABLED_PROP));

        checkEventConfigurationElement(JOURNAL_ENABLED_PROP,JOURNAL_ENABLED_DEFUALT_VAL,
                (String)aTestProps.get(JOURNAL_ENABLED_PROP));

        checkEventConfigurationElement(POLICY_EXEC_ENABLED_PROP,POLICY_EXEC_ENABLED_DEFUALT_VAL,
                (String)aTestProps.get(POLICY_EXEC_ENABLED_PROP));
    
        checkEventConfigurationElement(ALERT_COUNT_PROP,ALERT_COUNT_DEFUALT_VAL,
                (String)aTestProps.get(ALERT_COUNT_PROP));

        checkEventConfigurationElement(ALERT_AGE_PROP,ALERT_AGE_DEFUALT_VAL,
                (String)aTestProps.get(ALERT_AGE_PROP));

        checkEventConfigurationElement(ALERT_LEVEL_PROP,ALERT_LEVEL_DEFUALT_VAL,
                (String)aTestProps.get(ALERT_LEVEL_PROP));
    
        checkEventConfigurationElement(POLICY_EXEC_INTERVAL_PROP,POLICY_EXEC_INTERVAL_DEFUALT_VAL,
                (String)aTestProps.get(POLICY_EXEC_INTERVAL_PROP));
    
        checkEventConfigurationElement(DATABASE_TYPE_PROP,DATABASE_TYPE_DEFUALT_VAL,
                (String)aTestProps.get(DATABASE_TYPE_PROP));

        checkEventConfigurationElement(DB_JNDI_NAME_PROP,DB_JNDI_NAME_DEFUALT_VAL,
                (String)aTestProps.get(DB_JNDI_NAME_PROP));

        checkEventConfigurationElement(ALERT_LEVEL_PROP,ALERT_LEVEL_DEFUALT_VAL,
                (String)aTestProps.get(ALERT_LEVEL_PROP));
    
        
    }

    private void verifyDASTestEventConfiguration(Properties aTestProps) {
        System.out.println("verifying 'server' test configuration data");

        checkEventConfigurationElement(POLICY_EXEC_ENABLED_PROP,POLICY_EXEC_ENABLED_TEST_VAL,
                (String)aTestProps.get(POLICY_EXEC_ENABLED_PROP));
    
        checkEventConfigurationElement(ALERT_COUNT_PROP,ALERT_COUNT_TEST_VAL,
                (String)aTestProps.get(ALERT_COUNT_PROP));

        checkEventConfigurationElement(ALERT_AGE_PROP,ALERT_AGE_TEST_VAL,
                (String)aTestProps.get(ALERT_AGE_PROP));

        checkEventConfigurationElement(ALERT_LEVEL_PROP,ALERT_LEVEL_TEST_VAL,
                (String)aTestProps.get(ALERT_LEVEL_PROP));
    
        checkEventConfigurationElement(POLICY_EXEC_INTERVAL_PROP,POLICY_EXEC_INTERVAL_TEST_VAL,
                (String)aTestProps.get(POLICY_EXEC_INTERVAL_PROP));
    
        checkEventConfigurationElement(DATABASE_TYPE_PROP,DATABASE_TYPE_TEST_VAL,
                (String)aTestProps.get(DATABASE_TYPE_PROP));

        checkEventConfigurationElement(DB_JNDI_NAME_PROP,DB_JNDI_NAME_TEST_VAL,
                (String)aTestProps.get(DB_JNDI_NAME_PROP));

        checkEventConfigurationElement(ALERT_LEVEL_PROP,ALERT_LEVEL_TEST_VAL,
                (String)aTestProps.get(ALERT_LEVEL_PROP));
    

        checkEventConfigurationElement(ALERT_TABLE_PROP,ALERT_TABLE_TEST_VAL,
                (String)aTestProps.get(ALERT_TABLE_PROP));
       
    }
    
    private void verifyInstanceDefaultEventConfiguration(Properties aTestProps) {
        System.out.println("verifying 'instance1' default configuration data");
        checkEventConfigurationElement(PERSISTENCE_ENABLED_PROP,PERSISTENCE_ENABLED_DEFUALT_VAL,
                (String)aTestProps.get(PERSISTENCE_ENABLED_PROP));

        checkEventConfigurationElement(DATABASE_TYPE_PROP,DATABASE_TYPE_DEFUALT_VAL,
                (String)aTestProps.get(DATABASE_TYPE_PROP));

        checkEventConfigurationElement(DB_JNDI_NAME_PROP,DB_JNDI_NAME_DEFUALT_VAL,
                (String)aTestProps.get(DB_JNDI_NAME_PROP));

        
    }
    
    private void verifyInstanceTestEventConfiguration(Properties aTestProps) {
        System.out.println("verifying 'instance1' test configuration data");

        checkEventConfigurationElement(PERSISTENCE_ENABLED_PROP,PERSISTENCE_ENABLED_DEFUALT_VAL,
                (String)aTestProps.get(PERSISTENCE_ENABLED_PROP));

        checkEventConfigurationElement(DATABASE_TYPE_PROP,DATABASE_TYPE_TEST_VAL,
                (String)aTestProps.get(DATABASE_TYPE_PROP));

        checkEventConfigurationElement(DB_JNDI_NAME_PROP,DB_JNDI_NAME_TEST_VAL,
                (String)aTestProps.get(DB_JNDI_NAME_PROP));

    
        
        checkEventConfigurationElement(ALERT_TABLE_PROP,ALERT_TABLE_TEST_VAL,
                (String)aTestProps.get(ALERT_TABLE_PROP));

    }

    
    private void checkEventConfigurationElement(String elementName,String expectedValue,String returnValue) {
    	if(expectedValue.trim().equals(returnValue.trim())) {
    		System.out.println(" element " + elementName + " value : equal to " + expectedValue + " (OK)");
    	} else {
    		System.out.println(" element " + elementName + " expected value = " + expectedValue
    				+ " found " + returnValue);
    		failureCount++;

    	}
    }

    
    private Properties getConfigurationProperties(String aConfigFilePath) {
        File fIn = new File(aConfigFilePath);
        System.out.println(fIn.getAbsolutePath());
        Properties lprops = new Properties();
        if(fIn.exists() == false) {
            return lprops;
        }
        try {
            FileInputStream fis = new FileInputStream(fIn);
            lprops.load(fis);
        } catch (Exception e) {
            System.out.println(" fail to get configuration file from  " + fIn.getAbsolutePath()  + "\n with error " + e.getMessage());
            e.printStackTrace();
            failureCount++;
        } 
        return lprops;
        
    }
	
    private void updateConfigurationProperties() {
        
        setConfigurationAttribute(PERSISTED_ALERT_MAX_AGE_ATTRIBUTE_NAME, 
                new Long(ALERT_AGE_TEST_VAL));
        
        setConfigurationAttribute(PERSISTED_ALERT_MAX_COUNT_ATTRIBUTE_NAME, 
                new Long(ALERT_COUNT_TEST_VAL));

        setConfigurationAttribute(PERSISTED_ALERT_LEVEL_ATTRIBUTE_NAME, 
                ALERT_LEVEL_TEST_VAL);
        
        setConfigurationAttribute(PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_ATTRIBUTE_NAME, 
                new Long(POLICY_EXEC_INTERVAL_TEST_VAL));

        setConfigurationAttribute(DATABASE_JNDI_NAME_ATTRIBUTE_NAME, 
                DB_JNDI_NAME_TEST_VAL);

        setConfigurationAttribute(DATABASE_TYPE_ATTRIBUTE_NAME, 
                DATABASE_TYPE_TEST_VAL);
        
        setConfigurationAttribute(DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME, 
                ALERT_TABLE_TEST_VAL);

        setConfigurationAttribute(ENABLE_POLICY_EXECUTION_ATTRIBUTE_NAME, 
                new Boolean(POLICY_EXEC_ENABLED_TEST_VAL));

    }

    private void setConfigurationAttribute(String aAttributeName,Object aAttributeValue) {
        Attribute lAttribute = new Attribute(aAttributeName,aAttributeValue);
        try {
            mCASMBeanServerConnection.setAttribute(mForwarderControllerObjectName, lAttribute);
        } catch (InstanceNotFoundException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' instance not found : \n" + e );
                failureCount++;
        } catch (MBeanException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' setAttribute" 
                                        +" mathod throw exception: \n" + e);
                failureCount++;
        } catch (ReflectionException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' reflection exception: \n" + e);
                failureCount++;
        } catch (IOException e) {
                System.out.println("communication to Mbean with object name '" + EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' failed\n" + e);
                failureCount++;
        } catch (AttributeNotFoundException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' does not have attribute named 'enableTargetPersistence'");
                failureCount++;
        } catch (InvalidAttributeValueException e) {
                System.out.println("Mbean with object name '"+ EVENTMANAGEMENT_CONTROL_MBEAN_NAME + "' attribute named 'enableTargetPersistence' is not boolean type");
                failureCount++;
        }
        
    }
        
    private void  restoreDefaultConfigurationProperties(Properties aDefaultServerProp) {

        setConfigurationAttribute(ENABLE_POLICY_EXECUTION_ATTRIBUTE_NAME, 
                new Boolean(POLICY_EXEC_ENABLED_DEFUALT_VAL));

        setConfigurationAttribute(PERSISTED_ALERT_MAX_AGE_ATTRIBUTE_NAME, 
                new Long(ALERT_AGE_DEFUALT_VAL));
        
        setConfigurationAttribute(PERSISTED_ALERT_MAX_COUNT_ATTRIBUTE_NAME, 
                new Long(ALERT_COUNT_DEFUALT_VAL));

        setConfigurationAttribute(PERSISTED_ALERT_LEVEL_ATTRIBUTE_NAME, 
                ALERT_LEVEL_DEFUALT_VAL);
        
        setConfigurationAttribute(PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_ATTRIBUTE_NAME, 
                new Long(POLICY_EXEC_INTERVAL_DEFUALT_VAL));

        setConfigurationAttribute(DATABASE_JNDI_NAME_ATTRIBUTE_NAME, 
                DB_JNDI_NAME_DEFUALT_VAL);

        setConfigurationAttribute(DATABASE_TYPE_ATTRIBUTE_NAME, 
                DATABASE_TYPE_DEFUALT_VAL); 
        
        setConfigurationAttribute(DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME, 
                aDefaultServerProp.getProperty(ALERT_TABLE_PROP));

        
    }

    
    private void runTest() {
	disablePersistence();
        System.out.println("DAS default event configuration properties");
        Properties lDefaultServerProp = getConfigurationProperties(mDasEMConfigFile);
        verifyDASDefaultEventConfiguration(lDefaultServerProp);
        System.out.println("Instance default event configuration properties");
        Properties lDefaultInstanceProp = getConfigurationProperties(mDasInstanceConfigFile);
        verifyInstanceDefaultEventConfiguration(lDefaultInstanceProp);
        System.out.println("Update event configuration test properties");
        updateConfigurationProperties();
        System.out.println("DAS test event configuration properties");
        Properties lTestServerProp = getConfigurationProperties(mDasEMConfigFile);
        verifyDASTestEventConfiguration(lTestServerProp);
        System.out.println("Instance test event configuration properties");
        Properties lTestInstanceProp = getConfigurationProperties(mDasInstanceConfigFile);
        verifyInstanceTestEventConfiguration(lTestInstanceProp);
        restoreDefaultConfigurationProperties(lDefaultServerProp);

        
        
        
        if(failureCount == 0) {
	    System.out.println(RESULT_PREFIX + " test event forwarding configuration passed" );
	} else {
	    System.out.println(RESULT_PREFIX + " test event forwarding configuration failed" );
	}
    }

    public static void main(String[] args) {
        TestEventForwarderConfiguration test;
	try {
	    // use the following for standalone java test program
//	        test = new TestEventForwarderConfiguration("service:jmx:rmi:///jndi/rmi://RDamir-gx280xp.stc.com:8689/management/rmi-jmx-connector");
		test = new TestEventForwarderConfiguration(); 
		test.runTest();
	    } catch (Exception e) {
	    	System.out.println("unable to initailze test - " + e.getMessage());
	    }
    }


}

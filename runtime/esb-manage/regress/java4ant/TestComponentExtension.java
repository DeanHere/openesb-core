/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentExtension.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.util.Map;
import java.util.Set;
import javax.management.remote.*;
import javax.management.*;

/**
 */
public class TestComponentExtension
{
     
    private MBeanServerConnection mbns;
    private static final String RESULT_PREFIX = "##### Result of ";
    
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String TARGET   = "target";
    private static final String COMPONENT_NAME = "component.name";
    private static final String CUSTOM_CONTROL_NAME = "custom.control.name";
    private static final String LOGGER_NAME = "logger.name";
    
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
    
    public ObjectName getComponentExtensionName()
        throws Exception
    {
        String info = "com.sun.jbi:Target="        + System.getProperty(TARGET, "domain") + 
                                  ",ComponentName=" + System.getProperty(COMPONENT_NAME)   +
                                  ",ServiceType=Extension";
        return new ObjectName(info);
    }
    
    
    public void getCustomMBeanNames(String customName)
        throws Exception
    {
        Map<String, ObjectName[]> objNameMap = (Map) mbns.invoke(getComponentExtensionName(), "getCustomMBeanNames", 
            new Object[] { customName }, new String[] { "java.lang.String" } );
        
        System.out.println(RESULT_PREFIX + " getCustomMBeanNames =" );
        Set<String> instances = objNameMap.keySet();
        for ( String instance : instances )
        {
            System.out.println(" Custom MBeans on instance " + instance + " : ");
            ObjectName[] objNames = objNameMap.get(instance);
            for ( ObjectName mbnName : objNames )
            {
                System.out.println(mbnName.toString());
            }
        }
    }
    
    
    public void getComponentConfigurationFacadeMBeanName()
        throws Exception
    {
        ObjectName cfgMBean = (ObjectName) mbns.invoke(getComponentExtensionName(), "getComponentConfigurationFacadeMBeanName", 
            new Object[] { System.getProperty(TARGET) }, new String[] { "java.lang.String" } );
        
        System.out.println(RESULT_PREFIX + " getComponentConfigurationFacadeMBeanName = " + cfgMBean);
    }
    
    public void getLoggerMBeanNames(String logName)
        throws Exception
    {
        Map<String, ObjectName[]> objNameMap = (Map) mbns.invoke(getComponentExtensionName(), "getLoggerMBeanNames", 
            new Object[] { logName }, new String[] { "java.lang.String" } );
        
        System.out.println(RESULT_PREFIX + " getLoggerMBeanNames =" );
        Set<String> instances = objNameMap.keySet();
        for ( String instance : instances )
        {
            System.out.println(" Logger MBeans on instance " + instance + " : ");
            ObjectName[] objNames = objNameMap.get(instance);
            for ( ObjectName mbnName : objNames )
            {
                System.out.println(mbnName.toString());
            }
        }
    }
    
    public static void main (String[] params)
        throws Exception 
    {
        TestComponentExtension test = new TestComponentExtension();
        
        test.initMBeanServerConnection();
        
        String logger = System.getProperty(LOGGER_NAME, "$");
        if ( logger.startsWith("$") )
        {
            test.getCustomMBeanNames(System.getProperty(CUSTOM_CONTROL_NAME));
        }
        else
        {
            test.getLoggerMBeanNames(logger);
        }
        test.getComponentConfigurationFacadeMBeanName();
    }
}

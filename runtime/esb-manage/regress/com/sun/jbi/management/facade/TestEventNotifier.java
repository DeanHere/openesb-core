/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEventNotifier.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.management.facade;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.platform.PlatformContext;
import com.sun.jbi.management.EventNotifierMBean;
import com.sun.jbi.management.system.ScaffoldedEnvironmentContext;
import com.sun.jbi.management.system.ScaffoldPlatformContext;

import javax.management.MBeanNotificationInfo;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.ObjectName;
import javax.management.StandardMBean;

/**
 * Tests for EventNotifier.
 *
 * @author Mark S White
 */
public class TestEventNotifier
    extends junit.framework.TestCase
{
    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * Local handle to the EnvironmentContext
     */
    private EnvironmentContext mContext;

    /**
     * Local handle to the PlatformContext
     */
    private PlatformContext mPlatform;

    /**
     * Instance of EventNotifier.
     */
    private EventNotifier mEventNotifier;

    /**
     * MBean object name for EventNotifier
     */
    private ObjectName mMBeanName;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestEventNotifier(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the EventNotifier instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);

        // Set up scaffolded Environment and Platform contexts

        mContext = new ScaffoldedEnvironmentContext();
        mPlatform = mContext.getPlatformContext();

        // Set up EventNotifier instance and register it as an MBean

        mEventNotifier = new EventNotifier(mContext, "domain");
        StandardMBean mbean = null;
        try
        {
            mbean = new StandardMBean(mEventNotifier, EventNotifierMBean.class);
        }
        catch ( javax.management.NotCompliantMBeanException ex )
        {
            fail("Unable to create EventNotifier MBean: " + ex);
        }
        try
        {
            mMBeanName = new ObjectName("com.sun.jbi:Type=Notification");
        }
        catch ( javax.management.MalformedObjectNameException ex )
        {
            fail("Unable to create ObjectName for EventNotifier MBean: " + ex);
        }
        MBeanServer mbs = mContext.getMBeanServer();
        try
        {
            mbs.registerMBean(mbean, mMBeanName);
        }
        catch ( Throwable ex )
        {
            fail("Unable to register EventNotifier MBean: " + ex);
        }
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        MBeanServer mbs = mContext.getMBeanServer();
        try
        {
            mbs.unregisterMBean(mMBeanName);
        }
        catch ( javax.management.InstanceNotFoundException infEx )
        {
            ; // Just ignore this error
        }
        catch ( javax.management.MBeanRegistrationException mbrEx )
        {
            fail("Unable to unregister EventNotifier MBean: " + mbrEx);
        }
        System.err.println("***** END of test " + mTestName);
    }

// =============================  test methods ================================

    /**
     * Tests getNotificationInfo.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetNotificationInfo()
    {
        // Get the notification info for this MBean

        MBeanNotificationInfo[] info = mEventNotifier.getNotificationInfo();
        String[] types = info[0].getNotifTypes();
        String name = info[0].getName();
        String desc = info[0].getDescription();

        // Verify that the notification info is correct

        assertEquals("Got more than one MBeanNotificationInfo, expected one: ",
            1, info.length);
        assertEquals("Got wrong number of notification types: ",
            mEventNotifier.NOTIFICATION_TYPES.length, types.length);
        assertEquals("Got wrong class name: ",
            mEventNotifier.NOTIFICATION_CLASS_NAME, name);
        assertEquals("Got wrong description: ",
            mEventNotifier.NOTIFICATION_DESCRIPTION, desc);
    }

    /**
     * Tests enableNotifications/disableNotifications.
     * @throws Exception if an unexpected error occurs.
     */
    public void testEnableDisableNotifications()
    {
        // Notifications default to enabled. Attempt to enable them again,
        // this should return a false result as nothing changed.

        assertFalse("Error, notifications should already be enabled",
            mEventNotifier.enableNotifications());

        // Now disable notifications, this should return true.

        assertTrue("Error, notifications should have been disabled",
            mEventNotifier.disableNotifications());

        // Now disable notifications again, this should return false.

        assertFalse("Error, notifications should already be disabled",
            mEventNotifier.disableNotifications());

        // Now enable notifications again, this should return true.

        assertTrue("Error, notifications should have been enabled",
            mEventNotifier.enableNotifications());
    }

    /**
     * Tests instanceStarted / instanceStopped.
     * @throws Exception if an unexpected error occurs.
     */
    public void testInstanceStartedStopped()
    {
        String instance = "server";

        // Attempt instanceStopped with no instance active. This should
        // return false.

        assertFalse("Error, instance should not be defined",
            mEventNotifier.instanceStopped(instance, mMBeanName));

        // Attempt instanceStarted with no instance active. This should
        // return true.

        assertTrue("Error, instance should have been added",
            mEventNotifier.instanceStarted(instance, mMBeanName));

        // Attempt instanceStarted with the instance already active. This should
        // return false.

        assertFalse("Error, instance should already be active",
            mEventNotifier.instanceStarted(instance, mMBeanName));

        // Attempt instanceStopped with the instance active. This should
        // return true.

        assertTrue("Error, instance should have been removed",
            mEventNotifier.instanceStopped(instance, mMBeanName));
    }

    /**
     * Tests handleNotification.
     * @throws Exception if an unexpected error occurs.
     */
    public void testHandleNotification()
    {
        String type = "com.sun.jbi.event";
        long seq = 12345;
        String msg = "This is a notification";
        Notification notif = new Notification(type, mMBeanName, seq, msg);
        String handBack = "server";

        // Call the handler. There's really no way to check for a failure
        // other than the fact that no exception occurs.
        mEventNotifier.handleNotification(notif, handBack);
    }

}

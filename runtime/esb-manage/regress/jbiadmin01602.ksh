#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01602.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin01602 : Negative tests for application configuration operations for target=server"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.test.component

TARGET=server
COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-with-custom-mbean.jar
COMPONENT_NAME=manage-binding-1

NEW_APP_CONFIG_FILE=$JV_SVC_REGRESS/deploytest/new-app-config.properties
NEW_APP_CONFIG_EXTRA_FILE=$JV_SVC_REGRESS/deploytest/new-app-config-extra.properties
INCORRECT_NEW_APP_CONFIG_FILE=$JV_SVC_REGRESS/deploytest/incorrect-new-app-config.properties
UPDATED_APP_CONFIG_FILE=$JV_SVC_REGRESS/deploytest/updated-app-config.properties
PARTIALLY_UPDATED_APP_CONFIG_FILE=$JV_SVC_REGRESS/deploytest/partially-updated-app-config.properties
INCORRECT_APP_CONFIG_FILE=$JV_SVC_REGRESS/deploytest/non-existant-app-config.properties

JBI_ANT_CMD="$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET"

# Test : Test the application configuration operations for a component which is not installed - should get back an error.
$JBI_ANT_CMD                                                                                    list-application-configurations
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$NEW_APP_CONFIG_FILE      create-application-configuration
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$UPDATED_APP_CONFIG_FILE  update-application-configuration
$JBI_ANT_CMD -Djbi.app.config.name=testConfig                                                    delete-application-configuration

# Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="server" install-component

# Test : Test the application configuration for a component which is not started - should get back an error.
$JBI_ANT_CMD                                                                                    list-application-configurations
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$NEW_APP_CONFIG_FILE      create-application-configuration
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$UPDATED_APP_CONFIG_FILE  update-application-configuration
$JBI_ANT_CMD -Djbi.app.config.name=testConfig                                                    delete-application-configuration

# Start the component
$JBI_ANT_CMD start-component

# Test adding a duplicate application configuration
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$NEW_APP_CONFIG_FILE  create-application-configuration
$JBI_ANT_CMD                                                                                list-application-configurations
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$NEW_APP_CONFIG_FILE  create-application-configuration
$JBI_ANT_CMD -Djbi.app.config.name=testConfig                                                delete-application-configuration

# Test setting a non-existing application configuration
$JBI_ANT_CMD -Djbi.app.config.name=someConfig -Djbi.app.config.params.file=$INCORRECT_APP_CONFIG_FILE  update-application-configuration

# Test deleting a non-existing application configuration
$JBI_ANT_CMD -Djbi.app.config.name=aConfig                                                  delete-application-configuration

# Test adding a application configuration with extra invalid fields should fail. ( fix for issue # 193)
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$NEW_APP_CONFIG_EXTRA_FILE  create-application-configuration

# Test adding a application configuration with not all fields present ( fix for issue # 149 )
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$INCORRECT_NEW_APP_CONFIG_FILE  create-application-configuration
$JBI_ANT_CMD                                                                                list-application-configurations

# Test updating a application configuration with only a subset of the fields updated
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$NEW_APP_CONFIG_FILE  create-application-configuration
$JBI_ANT_CMD                                                                                     list-application-configurations
$JBI_ANT_CMD -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$PARTIALLY_UPDATED_APP_CONFIG_FILE  update-application-configuration
$JBI_ANT_CMD                                                                                     list-application-configurations

# Test fix for Issue 277 : List an application configuration which does not exist
asadmin show-jbi-application-configuration --component=$COMPONENT_NAME -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT  fred

# Cleanup
$JBI_ANT_CMD  shut-down-component
$JBI_ANT_CMD  uninstall-component



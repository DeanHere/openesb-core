#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01600.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#                     Tests the facade Component Configuration MBean for target=cluster     #
#                                                                                           #             
#                                                                                           #
# The test component used in this test registers a configuration MBean. The configuration   #
# information is represented in the jbi.xml as per the new configuration schema.            #
#                                                                                           #
# The test :                                                                                #
# (a) Package, install and start the test component                                         #
# (b) Get/Set component configuration                                                       #
#                                                                                           #
#############################################################################################

echo "jbiadmin01607 : Tests the facade Component Configuration MBean for target=cluster with the new configuration schema"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.new.test.component

COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-with-new-config-mbean.jar
COMPONENT_NAME=manage-binding-1

# Cluster and Member Instance Setup
asadmin create-cluster  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster
createClusterDelay
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster ccfg-cluster --nodeagent agent1 ccfg-cluster-instance1
createInstanceDelay
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster ccfg-cluster --nodeagent agent1 ccfg-cluster-instance2
createInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance1
startInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance2
startInstanceDelay

# Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="ccfg-cluster" install-component
installComponentDelay
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" start-component
startComponentDelay

# Test (b) :
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="ccfg-cluster" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml get.component.configuration

# Cleanup
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" stop-component
stopComponentDelay
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" shut-down-component
stopComponentDelay
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" uninstall-component
uninstallComponentDelay

# Cluster and Member Instance Cleanup
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT ccfg-cluster-instance1
stopInstanceDelay
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT ccfg-cluster-instance2
stopInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance1
deleteInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance2
deleteInstanceDelay
asadmin delete-cluster $ASADMIN_PW_OPTS --port $ASADMIN_PORT ccfg-cluster
deleteClusterDelay

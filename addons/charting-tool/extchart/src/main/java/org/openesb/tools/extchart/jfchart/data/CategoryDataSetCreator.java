/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CategoryDataSetCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.jfchart.data;


import org.openesb.tools.extchart.exception.ChartException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Logger;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.Dataset;

/**
 *
 * @author rdwivedi
 */
public class CategoryDataSetCreator extends DataSetCreator{
    
    
    private static Logger mLogger = Logger.getLogger(CategoryDataSetCreator.class.getName());
    private DefaultCategoryDataset  dataset = null;
    
    /** Creates a new instance of CategoryDataSetCreator */
    public CategoryDataSetCreator() {
        
    }
    
    public Dataset getDefaultDataSet() {
        return dataset;
    }
    public  void processResultSet(ResultSet resultSet ) throws ChartException {
       
        dataset = new DefaultCategoryDataset();
        try {
        ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();
            if (columnCount < 2) {
                throw new ChartException(
                    "There must be more then 1 columns.");
            }
            // Remove any previous old data
            int i = dataset.getRowCount();
            for (; i > 0; --i) {
                dataset.removeRow(i);
            }
        while (resultSet.next()) {
                // first column contains the row key...
                Comparable categoryKey = resultSet.getString(1);
                String ser = resultSet.getString(2);
                if(columnCount > 3) {
                for (int column = 3; column <= columnCount -1 ; column++) {
                   // all other column makes up the secondary series sepereated by -
                    ser += "-" + resultSet.getString(column);
                }
                }
                // now for value part it needs to be numeric....
                     
                    int columnType = metaData.getColumnType(columnCount);

                    switch (columnType) {
                        case Types.TINYINT:
                        case Types.SMALLINT:
                        case Types.INTEGER:
                        case Types.BIGINT:
                        case Types.FLOAT:
                        case Types.DOUBLE:
                        case Types.DECIMAL:
                        case Types.NUMERIC:
                        case Types.REAL: {
                            Number value = (Number) resultSet.getObject(columnCount);
                                dataset.setValue(value, categoryKey, ser);
                            break;
                        }
                        case Types.DATE:
                        case Types.TIME:
                        case Types.TIMESTAMP:{
                            mLogger.info("Found Data type for value , which is not allowed" + resultSet.getObject(columnCount));
                            break;
                        }
                        case Types.CHAR:
                        case Types.VARCHAR:
                        case Types.LONGVARCHAR: {
                            String string 
                                = (String) resultSet.getObject(columnCount);
                            try {
                                Number value = Double.valueOf(string);
                                    dataset.setValue(value, categoryKey, ser);
                            }
                            catch (NumberFormatException e) {
                               mLogger.info("Found a non number value at value column" + string);
                            }
                            break;
                        }
                        default:
                            mLogger.info("Not a valid type for value column" + metaData.getColumnName(columnCount));
                            break;
                    }
                }
            
        } catch(SQLException e) {
            throw new ChartException (e);
        }
         
    }
    
    
    public String getQuery() throws ChartException {
        return null;
    }
    
    
    
}

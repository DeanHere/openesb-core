/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ChartCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.jfchart;

import org.openesb.tools.extchart.jfchart.data.DataAccess;
import org.openesb.tools.extpropertysheet.IExtPropertyGroupsBean;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rdwivedi
 */
public class ChartCreator {
    
    private static Logger mLogger = Logger.getLogger("com.open.extchart.jfchart.ChartCreator");
    /** Creates a new instance of ChartCreator */
    public ChartCreator() {
        
    }
        
    public void writeBufferedChartToOutputStream(BufferedImage image , HttpServletResponse response) throws IOException {
        OutputStream out = response.getOutputStream();
        response.setContentType("image/png");
        ChartUtilities.writeBufferedImageAsPNG(out,image);
    }
    
    public JFreeChart createJFreeChart(IExtPropertyGroupsBean pg, DataAccess da) {
        JFreeChart jChart = null;
        try {
        Chart chart = null;
        
        chart = Chart.createChartObject(pg,da);
        if(chart != null)  {
            jChart = chart.createChart();
        } else {
            mLogger.info("Created chart is null.");
        }
    } catch(Exception e) {
        mLogger.log(Level.SEVERE,"Can not create the chart.",e);
        //mLogger.severe("Can not create the chart." + e.toString());
         
       // mLogger.exception("Failed ",e);
        //e.printStackTrace();
    }
        return jChart;
    }
    
   
    
}

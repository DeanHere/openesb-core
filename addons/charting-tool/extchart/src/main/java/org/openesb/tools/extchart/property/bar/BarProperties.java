/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BarProperties.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property.bar;



import org.openesb.tools.extchart.property.JFChartConstants;
import org.openesb.tools.extpropertysheet.impl.ExtPropertyGroup;
import org.openesb.tools.extpropertysheet.impl.ExtPropertyOptions;
import java.awt.Color;


/**
 * BarProperties.java
 *
 * @author Chris Johnston
 * @version :$Revision: 1.4 $
 */
public class BarProperties extends ExtPropertyGroup {

    /** the RCS id */
    static final String RCS_ID =
        "$Id: BarProperties.java,v 1.4 2010/07/18 16:18:09 ksorokin Exp $";

    /** IS_STACKED_KEY is a default key for stacked flag default for bar charts. */
    public static final String IS_STACKED_KEY = "bd_stacked";

    /** BAR_CATEGORY_LABEL_TYPE_KEY is a default key. */
    public static final String BAR_CATEGORY_LABEL_TYPE_KEY = "bd_barcategorytype";

    /** BAR_SERIES_LABEL_TYPE_KEY is a default key. */
    public static final String BAR_SERIES_LABEL_TYPE_KEY = "bd_barseriestype";

    

    /** BAR_DISPLAY_EMPTY_CATEGORIES_KEY is a default key. */
    public static final String BAR_DISPLAY_EMPTY_CATEGORIES_KEY = "bd_bardisplayemptycategories";

    /** BAR_DISPLAY_EMPTY_SERIES_KEY is a default key. */
    public static final String BAR_DISPLAY_EMPTY_SERIES_KEY = "bd_bardisplayemptyseries";

    /** BAR_CATEGORY_GAP_PERCENT_KEY is a default key. */
    public static final String BAR_CATEGORY_GAP_PERCENT_KEY = "bd_barcategorygappercent";

    /** BAR_SERIES_GAP_PERCENT_KEY is a default key. */
    public static final String BAR_SERIES_GAP_PERCENT_KEY = "bd_barseriesgappercent";

    /** BAR_DISPLAY_EMPTY_LEGEND_ITEMS_KEY is a default key. */
    public static final String BAR_DISPLAY_EMPTY_LEGEND_ITEMS_KEY = "bd_bardisplayemptylegenditems";

    /** DEFAULT_BAR_CHART_TITLE is used as a hard default. */
    public static final String DEFAULT_BAR_CHART_TITLE = "Bar Chart";

    /** DEFAULT_STACKED is used as a hard default. */
    public static final boolean DEFAULT_STACKED = false;

    /** BAR_CATEGORY_NO_LABELS_STRING is used for XML persistency. */
    public static final String BAR_CATEGORY_NO_LABELS_STRING = "None";

    /** BAR_CATEGORY_NAME_LABELS_STRING is used for XML persistency. */
    public static final String BAR_CATEGORY_NAME_LABELS_STRING = "Name";

    /** BAR_CATEGORY_VALUE_LABELS_STRING is used for XML persistency. */
    public static final String BAR_CATEGORY_VALUE_LABELS_STRING = "Value";

    /** BAR_SERIES_NO_LABELS_STRING is used for XML persistency. */
    public static final String BAR_SERIES_NO_LABELS_STRING = "None";

    /** BAR_SERIES_NAME_LABELS_STRING is used for XML persistency. */
    public static final String BAR_SERIES_NAME_LABELS_STRING = "Name";

    /** BAR_SERIES_VALUE_LABELS_STRING is used for XML persistency. */
    public static final String BAR_SERIES_VALUE_LABELS_STRING = "Value";

    /** DEFAULT_BAR_CATEGORY_LABEL_TYPE is set to zero for no labels */
    public static final int DEFAULT_BAR_CATEGORY_LABEL_TYPE = 0;

    /** DEFAULT_BAR_SERIES_LABEL_TYPE is set to zero for no labels */
    public static final int DEFAULT_BAR_SERIES_LABEL_TYPE   = 0;

    /** DEFAULT_BAR_DISPLAY_EMPTY_CATEGORIES is used to display empty categories */
    public static final boolean DEFAULT_BAR_DISPLAY_EMPTY_CATEGORIES = true;
    /** DEFAULT_BAR_DISPLAY_EMPTY_SERIES is used to display empty series */
    public static final boolean DEFAULT_BAR_DISPLAY_EMPTY_SERIES = true;

    /** DEFAULT_BAR_CATEGORY_GAP_PERCENT is a default */
    public static final int DEFAULT_BAR_CATEGORY_GAP_PERCENT = 10;
    /** DEFAULT_BAR_SERIES_GAP_PERCENT is a default */
    public static final int DEFAULT_BAR_SERIES_GAP_PERCENT   = 10;
    /** DEFAULT_BAR_CATEGORY_OUTLINE_COLOR is a default */
    public static final Color DEFAULT_BAR_CATEGORY_OUTLINE_COLOR = Color.white;
    /** DEFAULT_DISPLAY_EMPTY_LEGEND_ITEMS is a default */
    public static final boolean DEFAULT_BAR_DISPLAY_EMPTY_LEGEND_ITEMS = true;

    /** BAR_CATEGORY_SHADOWBOX_OUTLINE_STYLES is the list of outline styles */
    /*
    public static final int[] BAR_CATEGORY_SHADOWBOX_OUTLINE_STYLES = {
        CategoryAxis.NO_OUTLINE_STYLE,
        CategoryAxis.TOP_STYLE,
        CategoryAxis.BOTTOM_STYLE,
        CategoryAxis.LEFT_STYLE,
        CategoryAxis.RIGHT_STYLE,
        CategoryAxis.BOX_STYLE
    };
    */
    /** DEFAULT_BAR_CATEGORY_SHADOWBOX_OUTLINE_STYLE is a default */

   // public static final int DEFAULT_BAR_CATEGORY_SHADOWBOX_OUTLINE_STYLE = CategoryAxis.NO_OUTLINE_STYLE;

    Object[] defaultObjects = {
        Boolean.valueOf(DEFAULT_STACKED),
        new Integer(DEFAULT_BAR_CATEGORY_LABEL_TYPE),
        new Integer(DEFAULT_BAR_SERIES_LABEL_TYPE),
        Boolean.valueOf(DEFAULT_BAR_DISPLAY_EMPTY_CATEGORIES),
        Boolean.valueOf(DEFAULT_BAR_DISPLAY_EMPTY_SERIES),
        Boolean.valueOf(DEFAULT_BAR_DISPLAY_EMPTY_LEGEND_ITEMS),
        new Integer(DEFAULT_BAR_CATEGORY_GAP_PERCENT),
        new Integer(DEFAULT_BAR_SERIES_GAP_PERCENT),
    };


    /** MY_DEFAULT_KEYS is the list of keys for this object. */
    protected static final String[] MY_DEFAULT_KEYS = {
        IS_STACKED_KEY,
        BAR_CATEGORY_LABEL_TYPE_KEY,
        BAR_SERIES_LABEL_TYPE_KEY,
        BAR_DISPLAY_EMPTY_CATEGORIES_KEY,
        BAR_DISPLAY_EMPTY_SERIES_KEY,
        BAR_DISPLAY_EMPTY_LEGEND_ITEMS_KEY,
        BAR_CATEGORY_GAP_PERCENT_KEY,
        BAR_SERIES_GAP_PERCENT_KEY
    };

    /** BAR_CATEGORY_LABEL_TYPES is used automate the bar category label processing. */
    public static final String[] BAR_CATEGORY_LABEL_TYPES = {
        BAR_CATEGORY_NO_LABELS_STRING,
        BAR_CATEGORY_NAME_LABELS_STRING,
        BAR_CATEGORY_VALUE_LABELS_STRING,
    };
    
    
    
    private static final ExtPropertyOptions BAR_CATEGORY_LABEL_TYPES_OPTIONS = new ExtPropertyOptions(BAR_CATEGORY_LABEL_TYPES,true);

    /** BAR_SERIES_LABEL_TYPES is used automate the bar category label processing. */
    public static final String[] BAR_SERIES_LABEL_TYPES = {
        BAR_SERIES_NO_LABELS_STRING,
        BAR_SERIES_NAME_LABELS_STRING,
        BAR_SERIES_VALUE_LABELS_STRING,
    };
    
   private static final ExtPropertyOptions BAR_SERIES_LABEL_TYPES_OPTIONS = new ExtPropertyOptions(BAR_SERIES_LABEL_TYPES,true); 

    static final String[] BAR_CHART_KEYS;
    static {
        String[] superKeys = XYProperties.MY_DEFAULT_KEYS;
        String[] myKeys    = new String[superKeys.length + MY_DEFAULT_KEYS.length];
        for (int n = 0; n < myKeys.length; n++) {
            if (n < superKeys.length) {
                myKeys[n] = superKeys[n];
            } else {
                myKeys[n] = MY_DEFAULT_KEYS[n - superKeys.length];
            }
        }
        BAR_CHART_KEYS = myKeys;
    }

    /**
     * @see com.stc.ebam.server.chart.engine.view.ViewDefaults.getKeys()
     */
    public String[] getKeys() {
        return BAR_CHART_KEYS;
    }

    /**
     * The BarChartDefaultsImpl constructor creates a new class instance.
     */
    public BarProperties( ) {
        super( );
       
        initDefaults();
        this.setGroupName(JFChartConstants.CHART_BAR_PROPERTIES);
        
       
    }

     

    
    /**
     * Method isDisplayEmptyCategories is used to determine if empty categories are to be
     * drawn (as jfree does by default).
     * @return boolean true if empty categories are to be rendered
     */
    public boolean isDisplayEmptyCategories() {
        Boolean val = (Boolean) this.getPropertyValue(BAR_DISPLAY_EMPTY_CATEGORIES_KEY);
        boolean returnValue = DEFAULT_BAR_DISPLAY_EMPTY_CATEGORIES;
        if (val != null) {
            returnValue = val.booleanValue();
        }
        return returnValue;
    }
    /**
     * Method setDisplayEmptyCategories is used to set empty categories displayability
     * @param bDisplayEmptyBars is true if empty bars are to be rendered
     */
    public void setDisplayEmptyCategories(boolean bDisplayEmptyCatgories) {
        setProperty(BAR_DISPLAY_EMPTY_CATEGORIES_KEY, Boolean.valueOf(bDisplayEmptyCatgories));
    }
    /**
     * Method isDisplayEmptySeries is used to determine if empty series are to be
     * drawn (as jfree does by default).
     * @return boolean true if empty series are to be rendered
     */
    public boolean isDisplayEmptySeries() {
        Boolean val = (Boolean) this.getPropertyValue(BAR_DISPLAY_EMPTY_SERIES_KEY);
        boolean returnValue = DEFAULT_BAR_DISPLAY_EMPTY_SERIES;
        if (val != null) {
            returnValue = val.booleanValue();
        }
        return returnValue;
    }
    /**
     * Method setDisplayEmptyBars is used to set empty bars displayability
     * @param bDisplayEmptyBars is true if empty bars are to be rendered
     */
    public void setDisplayEmptySeries(boolean bDisplayEmptySeries) {
        setProperty(BAR_DISPLAY_EMPTY_SERIES_KEY, Boolean.valueOf(bDisplayEmptySeries));
    }
    /**
     * Method getCategoryGapPercent gets the percentage of gap between categories
     * as a factor of the category drawing area
     * @return int Percent as a percentage from 0 to 100
     */
    public int getCategoryGapPercent() {
        Integer val = (Integer) this.getPropertyValue(BAR_CATEGORY_GAP_PERCENT_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_BAR_CATEGORY_GAP_PERCENT;
    }
    /**
     * Method setCategoryGapPercent sets the percentage of gap between categories
     * as a factor of the category drawing area
     * @param nPercent is a percentage from 0 to 100
     */
    public void setCategoryGapPercent(int nPercent) {
       
            setProperty(BAR_CATEGORY_GAP_PERCENT_KEY, new Integer(nPercent));
       
    }
    /**
     * Method getSeriesGapPercent gets the percentage of gap between series
     * as a factor of the series drawing area
     * @return int Percent as a percentage from 0 to 100
     */
    public int getSeriesGapPercent() {
        Integer val = (Integer) this.getPropertyValue(BAR_SERIES_GAP_PERCENT_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_BAR_SERIES_GAP_PERCENT;
    }
    /**
     * Method setSeriesGapPercent sets the percentage of gap between series
     * as a factor of the series drawing area
     * @param nPercent is a percentage from 0 to 100
     */
    public void setSeriesGapPercent(int nPercent) {
        
            setProperty(BAR_SERIES_GAP_PERCENT_KEY, new Integer(nPercent));
        
    }
    /**
     * Method setBarCategoryLabelType is sets the category label type
     * @param nBarCategoryLabelType is the new type
     */
    public void setBarCategoryLabelType(int nBarCategoryLabelType) {
        if ((nBarCategoryLabelType >= 0) && (nBarCategoryLabelType < BAR_CATEGORY_LABEL_TYPES.length)) {
            setProperty(BAR_CATEGORY_LABEL_TYPE_KEY, new Integer(nBarCategoryLabelType),BAR_CATEGORY_LABEL_TYPES_OPTIONS);
        }
    }

    /**
     * Method getBarCategoryLabelType retrieves the bar category label type
     * @return int of identifier for the label type
     */
    public int getBarCategoryLabelType() {
        Integer val = (Integer) this.getPropertyValue(BAR_CATEGORY_LABEL_TYPE_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_BAR_CATEGORY_LABEL_TYPE;
    }

    /**
     * Method setBarSeriesLabelType is used to set the bar series label type
     * @param nBarSeriesLabelType is the new type
     */
    public void setBarSeriesLabelType(int nBarSeriesLabelType) {
        if ((nBarSeriesLabelType >= 0) && (nBarSeriesLabelType < BAR_SERIES_LABEL_TYPES.length)) {
            setProperty(BAR_SERIES_LABEL_TYPE_KEY, new Integer(nBarSeriesLabelType),BAR_SERIES_LABEL_TYPES_OPTIONS);
        }
    }

    /**
     * Method getBarSeriesLabelType is used to retrieve the series label type
     * @return int identifier for the label type.
     */
    public int getBarSeriesLabelType() {
        Integer val = (Integer) this.getPropertyValue(BAR_SERIES_LABEL_TYPE_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_BAR_SERIES_LABEL_TYPE;
    }

   

    private void initDefaults()  {
        setStacked(DEFAULT_STACKED);
        setBarCategoryLabelType(DEFAULT_BAR_CATEGORY_LABEL_TYPE);
        setBarSeriesLabelType(DEFAULT_BAR_SERIES_LABEL_TYPE);
        setDisplayEmptyCategories(DEFAULT_BAR_DISPLAY_EMPTY_CATEGORIES);
        setDisplayEmptySeries(DEFAULT_BAR_DISPLAY_EMPTY_SERIES);
        setDisplayEmptyLegendItems(DEFAULT_BAR_DISPLAY_EMPTY_LEGEND_ITEMS);
        setCategoryGapPercent(DEFAULT_BAR_CATEGORY_GAP_PERCENT);
        setSeriesGapPercent(DEFAULT_BAR_SERIES_GAP_PERCENT);
    }

    /**
     * @see com.stc.ebam.server.chart.engine.view.chart.xy.simple.BarChartDefaults.isStacked()
     */
    public boolean isStacked() {
        Boolean val = (Boolean) this.getPropertyValue(IS_STACKED_KEY);
        if (val != null) {
            return val.booleanValue();
        }
        return DEFAULT_STACKED;
    }
    /**
     * Sets stacked
     * @param bStacked - if stacked
     */
    public void setStacked(boolean bStacked) {
        setProperty(IS_STACKED_KEY, Boolean.valueOf(bStacked));
    }

    /**
     * @see com.stc.ebam.server.chart.engine.view.chart.xy.simple.BarChartDefaults.isStacked()
     */
    public boolean isDisplayEmptyLegendItems() {
        Boolean val = (Boolean) this.getPropertyValue(BAR_DISPLAY_EMPTY_LEGEND_ITEMS_KEY);
        if (val != null) {
            return val.booleanValue();
        }
        return DEFAULT_BAR_DISPLAY_EMPTY_LEGEND_ITEMS;
    }
    /**
     * Sets stacked
     * @param bStacked - if stacked
     */
    public void setDisplayEmptyLegendItems(boolean bDisplay) {
        setProperty(BAR_DISPLAY_EMPTY_LEGEND_ITEMS_KEY, Boolean.valueOf(bDisplay));
    }

   

   
    /**
     * toString
     * @return - string
     */
    public String toString() {
        return "Bar Chart";
    }
    /**
     * Returns property template name
     * @return - template name
     */
    public String getPropertyTemplateName() {
        return "BarChart";
    }
    
}

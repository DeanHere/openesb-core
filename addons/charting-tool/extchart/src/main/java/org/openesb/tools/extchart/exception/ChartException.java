/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ChartException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.exception;

/**
 *
 * @author rdwivedi
 */

        
        public class ChartException extends Exception {

    

    private Throwable mRootCause;

    /**
     * Constructor to create a new instance for this class.  This method is 
     * included here for the purpose of loading only.
     */
    public ChartException() {
    }

    /**
     * Constructor creates a new instance of EBAMServerException
     *
     * @param message is the message of this exception
     */
    public ChartException(String message) {
        super(message);
    }

    /**
     * Constructs a new instance of EBAMServerException
     * @param logCategory signifies exceptions originating class of generation
     * @param message contains the message of the exception
     */
    public ChartException(String logCategory, String message) {
        super(logCategory + "-" + message);
    }

    /**
     * Constructs a new instance of EBAMServerException
     * @param message is the message of this exception.
     * @param cause is the cause of this exception.
     *
     */
    public ChartException(String message, Throwable cause) {
        super(message);
        mRootCause = cause;
    }

    /**
     * Constructor creates a new instance of this class
     *
     * @param cause represents the cause of this exception.
     */
    public ChartException(Throwable cause) {
        super(cause.toString());
        mRootCause = cause;
    }

    /**
     * Method toString returns the description of this exception.
     *
     * @return String description of the exception
     */
    public String toString() {

        String string = this.getMessage();

        if (null != mRootCause) {
            string += " - Root Cause: " + mRootCause.toString();
        }
        return string;
    }
    
}
        

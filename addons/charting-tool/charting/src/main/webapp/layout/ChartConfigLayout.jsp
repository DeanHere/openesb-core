<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" 
xmlns:webuijsf="http://www.sun.com/web/ui">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page frame="true">
            <webuijsf:html>
                <webuijsf:head title="Chart Config.">
                     <webuijsf:link  id="ChartConfigLayoutLink" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:frameSet cols="70%,30%" id="configFrame">
                    <webuijsf:frame frameBorder="false" name="chartPreviewFrame" noResize="false" url="../chart/ChartPanel.jsp"/>
                    <webuijsf:frame frameBorder="false" name="propertyFrame" noResize="true" scrolling="true" toolTip="Properties" url="../chart/PropertyPane.jsp"/>
                </webuijsf:frameSet>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>

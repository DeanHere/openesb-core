/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ChartDataPersistence.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.charting.persist;

import java.util.List;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.logging.Logger;
import org.openesb.tools.charting.exception.ChartingException;

/**
 *
 * @author rdwivedi
 */
public class ChartDataPersistence {
    
    /** Creates a new instance of ChartDataPersistence */
    String mDefaultDBJndi = "jdbc/__default";
    private static Logger mLogger = Logger.getLogger(ChartDataPersistence.class.getName());
    private static boolean INIT = false;
    public ChartDataPersistence() {
        if(!INIT){
            try {
            setChartSystemDB();
            INIT = true;
} catch(Exception e) {
            mLogger.severe("Error while Initializing "+ e.getMessage());
}
            }
    }

    List getAllAvailableChartIDS() throws ChartingException {
        String queryString = "Select CHART_ID,CHART_D_NAME,DIS_NAME,D_A_ID,C_TYPE,GRP_ID from CHCHARTS,CHGROUPS where " +
                "CHCHARTS.GRP_ID = CHGROUPS.ID";
       ArrayList list = new ArrayList();
       Connection con = null;
       Statement stmt = null;
       ResultSet rset = null;
       try {
        con = getConnectionJNDI();
        stmt = con.createStatement ();
        rset = stmt.executeQuery(queryString);
        
        while(rset.next()) {
            ChartBean cBean = new ChartBean();
            cBean.setChartID(rset.getString(1));
            cBean.setChartName(rset.getString(2));
           
            cBean.setChartParentName(rset.getString(3));
            cBean.setChartDataBeanID(rset.getString(4));
            cBean.setChartPropertiesForDataset(null,rset.getString(5));
             cBean.setParentID(rset.getString(6));
            list.add(cBean);
        }
        rset.close();
        stmt.close();
        
        } catch (SQLException e) {
                throw new ChartingException(e);
        
        } finally {
        if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    // do nothing.
                }
            }

        }
       return list;
    }
    List getAllAvailableDataAccess() throws ChartingException {
        String queryString = "Select CHDATAACC.ID,GRP_ID,D_NAME,DIS_NAME,D_TYPE,D_JNDI_N,DATA from CHDATAACC,CHGROUPS where " +
                " CHDATAACC.GRP_ID = CHGROUPS.ID";
       ArrayList list = new ArrayList();
       Connection con = null;
       Statement stmt = null;
       ResultSet rset = null;
       try {
        con = getConnectionJNDI();
        stmt = con.createStatement ();
        rset = stmt.executeQuery(queryString);
        
        while(rset.next()) {
            DataBean cBean = new DataBean(rset.getString(3),rset.getString(4));
            cBean.setID(rset.getString(1));
            cBean.setDataSetType(rset.getString(5));
            cBean.setJNDIName(rset.getString(6));
            cBean.setQuery(rset.getString(7));
            list.add(cBean);
        }
        rset.close();
        stmt.close();
        
        } catch (SQLException e) {
            mLogger.severe("Error while executing " + queryString);
                throw new ChartingException(e);
        
        } finally {
        if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    // do nothing.
                }
            }

        }
       return list;
    }

    public void upsertGroup(String id, String disname) throws ChartingException {
       
        /**
                "Create table CHGROUPS (ID varchar(100) NOT NULL , DIS_NAME varchar(20)," +
               "CONSTRAINT GRPID_PK PRIMARY KEY(ID))";
        **/
       Connection con = null;
      String queryString = "Select ID from CHGROUPS where ID=?";
       try {
            con = getConnectionJNDI();
       PreparedStatement qStmt = con.prepareStatement(queryString);
       PreparedStatement pstmt = con.prepareStatement("UPDATE CHGROUPS SET DIS_NAME = ?  WHERE ID = ?");
       PreparedStatement istmt1 = con.prepareStatement("Insert into CHGROUPS values(?,?)");
       qStmt.setString(1,id);
       qStmt.execute();
        if(qStmt.execute() && qStmt.getResultSet().next()) {
            pstmt.setString(1,disname);
            pstmt.setString(2, id);
            pstmt.executeUpdate();
        } else {
            istmt1.setString(1,id);
            istmt1.setString(2,disname);
            
            //istmt1.executeUpdate();
            istmt1.executeUpdate();
            
        }
        con.commit();
        pstmt.close();
        istmt1.close();
        } catch (SQLException e) {
            try {
                     con.rollback();
                } catch (SQLException e1) {
                    mLogger.log(Level.SEVERE,"Roll back failed.");
                }
           
            e.printStackTrace();
            mLogger.log(Level.SEVERE, "Unable to save chart data ", e);
               throw new ChartingException("Unable to save chart data ",e);
               
        
        } finally {
        if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    mLogger.log(Level.SEVERE,"Failed to close the connection.");
                }
            }
        
        }
    }
    
    public void persistDataAccessBean(DataBean bean) throws ChartingException {
        Connection con = null;
      
       String queryString = "Select ID from CHDATAACC where ID=?";
       mLogger.info("Saving DA object" + bean.getDisplayName());
       // CHDATAACC ("ID  varchar(200), GRP_ID integer," +
       //        "D_NAME  varchar(200), D_TYPE varchar(40), D_JNDI_N varchar(40)" +
       //        "DATA varchar(20000)

       try {
           con = getConnectionJNDI();
       PreparedStatement qStmt = con.prepareStatement(queryString);
       PreparedStatement pstmt = con.prepareStatement("UPDATE CHDATAACC SET D_NAME = ?," +
               "D_TYPE = ?,D_JNDI_N=?,DATA=?  WHERE ID = ?");
       PreparedStatement istmt1 = con.prepareStatement("Insert into CHGROUPS values(?,?)");
       PreparedStatement istmt2 = con.prepareStatement("Insert into CHDATAACC values(?,?,?,?,?,?)");
       
       qStmt.setString(1,bean.getID());
       qStmt.execute();
       
        if(qStmt.execute() && qStmt.getResultSet().next()) {
            mLogger.info("update Called. " + bean.getDisplayName());
            pstmt.setString(1,bean.getDisplayName());
            pstmt.setString(2, bean.getDataSetType());
            pstmt.setString(3, bean.getJNDIName());
            pstmt.setString(4, bean.getQuery());
            pstmt.setString(5, bean.getID());
            pstmt.executeUpdate();
        } else {
            mLogger.info("insert Called. " + bean.getDisplayName());
            // this needs to be done some where else.    
            //istmt1.setString(1, bean.getGroupID());
            //istmt1.setString(2, bean.getParentDisplayName());
            
            istmt2.setString(1, bean.getID());
            istmt2.setString(2,bean.getGroupID());
            istmt2.setString(3,bean.getDisplayName());
            istmt2.setString(4,bean.getDataSetType());
            istmt2.setString(5,bean.getJNDIName());
            istmt2.setString(6,bean.getQuery());
            
            //istmt1.executeUpdate();
            istmt2.executeUpdate();
            
        }
        con.commit();
        pstmt.close();
        istmt1.close();
        istmt2.close();
         mLogger.info("Save Data complete. " + bean.getDisplayName());
        } catch (SQLException e) {
            try {
                     con.rollback();
                } catch (SQLException e1) {
                    mLogger.log(Level.SEVERE,"Roll back failed.");
                }
           
            e.printStackTrace();
            mLogger.log(Level.SEVERE, "Unable to save chart data ", e);
               throw new ChartingException("Unable to save chart data ",e);
               
        
        } finally {
        if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    mLogger.log(Level.SEVERE,"Failed to close the connection.");
                }
            }
        
        }
    }
    
    public void persistChartBean(ChartBean bean) throws ChartingException {
        Connection con = null;
      
       String queryString = "Select CHART_ID from CHCHARTS where CHART_ID=?";
       
      // Statement stmt = null;
      // ResultSet rset = null;
      /** CHCHARTS (CHART_ID  varchar(200), GRP_ID integer," +
               " D_A_ID integer , CHART_D_NAME  varchar(200), C_TYPE varchar(40)" +
               "CHART_DATA varchar(20000)," +
               "CONSTRAINT FK_GRPID " +
               " FOREIGN KEY (GRP_ID) " +
               " REFERENCES CHGROUPS(ID)," +
               "CONSTRAINT FK_DAGRPID " +
               " FOREIGN KEY (D_A_ID) " +
               " REFERENCES CHGROUPS(ID))";
       * 
       * **/
       try {
       con = getConnectionJNDI();
       PreparedStatement qStmt = con.prepareStatement(queryString);
       PreparedStatement pstmt = con.prepareStatement("UPDATE CHCHARTS SET D_A_ID = ?," +
               "CHART_D_NAME = ?,C_TYPE=?,CHART_DATA=?  WHERE CHART_ID = ?");
       PreparedStatement istmt1 = con.prepareStatement("Insert into CHGROUPS values(?,?)");
       PreparedStatement istmt2 = con.prepareStatement("Insert into CHCHARTS values(?,?,?,?,?,?)");
       
       qStmt.setString(1,bean.getChartID());
        
        //stmt = con.createStatement ();
        //rset = stmt.executeQuery(queryString);
       if(qStmt.execute() && qStmt.getResultSet().next()) {
            pstmt.setString(1,bean.getChartDataBeanID());
            pstmt.setString(2, bean.getChartName());
            pstmt.setString(3, bean.getChartType());
            StringBuffer buffer = new StringBuffer();
            bean.getChartProperties().toXML(buffer);
            pstmt.setString(4,buffer.toString());
            pstmt.setString(5, bean.getChartID());
            pstmt.executeUpdate();
        } else {
            
            //istmt1.setString(1, bean.getParentID());
            //istmt1.setString(2, bean.getChartParentName());
            
            
            istmt2.setString(1, bean.getChartID());
            istmt2.setString(2,bean.getParentID());
            istmt2.setString(3,bean.getChartDataBeanID());
            istmt2.setString(4,bean.getChartName());
            istmt2.setString(5,bean.getChartType());
            StringBuffer buffer = new StringBuffer();
            bean.getChartProperties().toXML(buffer);
            istmt2.setString(6,buffer.toString());
            
            //istmt1.executeUpdate();
            istmt2.executeUpdate();
            
        }
        //rset.close();
        //stmt.close();
        pstmt.close();
        istmt1.close();
        istmt2.close();
        } catch (SQLException e) {
               mLogger.log(Level.SEVERE, "Unable to save chart data ", e);
               throw new ChartingException("Unable to save chart data ",e);
        
        } finally {
        if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    // do nothing.
                }
            }
        
        }
    }
    
    private Connection getConnectionJNDI() throws SQLException {
       try {
        InitialContext ctx = new InitialContext();
        DataSource ds = (DataSource)
        ctx.lookup(mDefaultDBJndi);
        Connection con = ds.getConnection();
        return con;
       } catch(NamingException e){
           mLogger.severe("Naming Exception occured " + e.getMessage());
       }
       return null;
   }
    
    public void setChartSystemDB() throws ChartingException {
       String checkQuery = "Select CHART_ID from CHCHARTS";
       Connection con = null;
       Statement stmt = null;
       ResultSet rset = null;
       try {
        con = getConnectionJNDI();
        stmt = con.createStatement ();
        rset = stmt.executeQuery(checkQuery);
        rset.close();
        stmt.close();
        
        } catch (SQLException e) {
               tryCreateSystemTables();
        
        } finally {
        if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    // do nothing.
                }
            }

        }
   }
   
   private void tryCreateSystemTables()  throws ChartingException {
       
       
       String q = "Create table CHCHARTS (CHART_ID  varchar(100), GRP_ID varchar(100)," +
               " D_A_ID varchar(100) , CHART_D_NAME  varchar(200), C_TYPE varchar(40)," +
               "CHART_DATA varchar(20000)," +
               "CONSTRAINT FK_GRPID " +
               " FOREIGN KEY (GRP_ID) " +
               " REFERENCES CHGROUPS(ID)," +
               "CONSTRAINT FK_DAGRPID_1 " +
               " FOREIGN KEY (D_A_ID) " +
               " REFERENCES CHDATAACC(ID))";
       String q2 = "Create table CHGROUPS (ID varchar(100) NOT NULL , DIS_NAME varchar(20)," +
               "CONSTRAINT GRPID_PK PRIMARY KEY(ID))";
       
       
       String q3 = "Create table CHDATAACC (ID  varchar(100) NOT NULL, GRP_ID varchar(100)," +
               "D_NAME  varchar(200), D_TYPE varchar(40), D_JNDI_N varchar(40)," +
               "DATA varchar(20000)," +
               " CONSTRAINT DA_PK PRIMARY KEY(ID)," +
               " CONSTRAINT FK_GRPID_2 " +
               " FOREIGN KEY (GRP_ID) " +
               " REFERENCES CHGROUPS(ID))";
       Connection con = null;
       Statement stmt = null;
      
       try {
        con = getConnectionJNDI();
        stmt = con.createStatement ();
        stmt.addBatch(q2);
        
        stmt.addBatch(q3);
        stmt.addBatch(q);
        stmt.executeBatch();
        stmt.close();
        
        } catch (SQLException e) {
                e.printStackTrace();
                while((e = e.getNextException())!=null){
                    e.printStackTrace();
                } 
               throw new ChartingException("Unable to create system tables",e);
        
        } finally {
        if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                     e.printStackTrace();
                }
            }

        }
   }
   
    
}

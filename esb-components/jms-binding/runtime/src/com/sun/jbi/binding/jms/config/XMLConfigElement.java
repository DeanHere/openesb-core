/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XMLConfigElement.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.config;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.JMSBindingContext;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * Represents a XML element node which contain one or more element nodes in it.
 * All XML element nodes which only text node are represented as properties of
 * the enclosing node.  For e.g : The following XML fragment
 * <pre><code>
 *      &lt;elem&gt;
 *          &lt;child&gt;
 *              Harry
 *          &lt;/child&gt;
 *      &lt;elem&gt;
 * </code></pre>
 * would be represented by a ConfigElement named "elem" having a property
 * "child" whose value is "Harry".
 *
 * @author Sun Microsystems Inc.
 */
public class XMLConfigElement
    implements ConfigElement
{
    /**
     * The xml Element for the Config Element.
     */
    private Element mElement;

    /**
     * Internal data structure to store the name value pairs.
     */
    private Map mProperties;

    /**
     * Internal handle to the element name.
     */
    private String mElementName;

    /**
     * Internal handle to the string translator.
     */
    private StringTranslator mStringTranslator;

    /**
     * Creates a new instance of XMLConfigElement.
     *
     * @param element element Node representing this element.
     */
    public XMLConfigElement(Element element)
    {
        mProperties = new HashMap();
        init(element);
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Gets the config element associated with property name.
     *
     * @param propertyName element's property name.
     *
     * @return associated property value as a ConfigElement instance.
     *
     * @throws IllegalArgumentException - if the property does not exist.
     */
    public ConfigElement getElement(String propertyName)
    {
        Object propertyValue = mProperties.get(propertyName);

        if (propertyValue == null)
        {
            return null;
        }
        else if (propertyValue instanceof ConfigElement)
        {
            return (ConfigElement) propertyValue;
        }
        else
        {
            throw new IllegalArgumentException(mStringTranslator.getString(
                    "JMS_PROPERTY_NOT_ELEMENT"));
        }
    }

    /**
     * Returns the element for this config element.
     *
     * @return the Element for this config element.
     */
    public Element getElement()
    {
        return mElement;
    }

    /**
     * Gets the element list associated with property name. This method is not
     * currently supported.
     *
     * @param propertyName element's property name.
     *
     * @return associated ConfigElement instance list.
     */
    public ConfigElement [] getElementList(String propertyName)
    {
        // Not currently supported. Will be supported later.
        // a workaround exists 
        return null;
    }

    /**
     * Gets the configuration element name.
     *
     * @return the element name as a String.
     */
    public String getElementName()
    {
        return mElementName;
    }

    /**
     * Get the iterator containing the element's property names.
     *
     * @return an property name interator.
     */
    public Iterator getKeys()
    {
        return mProperties.keySet().iterator();
    }

    /**
     * Gets the associated property value.
     *
     * @param propertyName elements' property name.
     *
     * @return associated property value as a String.
     *
     * @throws IllegalArgumentException - if the property does not exist.
     */
    public String getProperty(String propertyName)
    {
        Object propertyValue = mProperties.get(propertyName);

        if (propertyValue == null)
        {
            return null;
        }
        else if (propertyValue instanceof java.lang.String)
        {
            return (String) propertyValue;
        }
        else
        {
            throw new IllegalArgumentException(mStringTranslator.getString(
                    "JMS_PROPERTY_NOT_STRING"));
        }
    }

    /**
     * Gets the associated property value list. This method is not currently
     * supported.
     *
     * @param propertyName element's property name.
     *
     * @return associated property value list.
     */
    public String [] getPropertyList(String propertyName)
    {
        // not currently supported. Will be supported later
        // A workaround exists
        return null;
    }

    /**
     * Initialize the configuration element.
     *
     * @param element element node which represents this configuration element.
     */
    private void init(Element element)
    {
        mElement = element;
        mElementName = element.getTagName();

        NodeList childNodes = element.getChildNodes();

        for (int i = 0; i < childNodes.getLength(); i++)
        {
            Object nodeObj = childNodes.item(i);

            if (nodeObj instanceof Element)
            {
                Element nodeElement = (Element) nodeObj;
                processChildNode(nodeElement);
            }
        }
    }

    /**
     * processes the child node.
     *
     * @param childNode child node.
     */
    private void processChildNode(Element childNode)
    {
        String childName = childNode.getTagName();
        int size = childNode.getChildNodes().getLength();

        if (size == 1)
        {
            Text textNode = (Text) childNode.getFirstChild();
            mProperties.put(childName, textNode.getData());
        }
        else
        {
            mProperties.put(childName, (new XMLConfigElement(childNode)));
        }
    }
}

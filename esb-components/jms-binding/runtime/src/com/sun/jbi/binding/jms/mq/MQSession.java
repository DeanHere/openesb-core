/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MQSession.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.mq;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;

import com.sun.jbi.binding.jms.handler.MessageHandler;

import com.sun.jbi.binding.jms.util.UtilBase;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import java.util.logging.Logger;

import javax.jms.InvalidDestinationException;
import javax.jms.InvalidSelectorException;
import javax.jms.JMSException;

import javax.jms.Message;

import javax.jms.Session;
/**
 * Wraps a JMS session.
 *
 * @author Sun Microsystems Inc.
 */
public final class MQSession
    extends UtilBase
    implements Runnable, JMSBindingResources
{
    /**
     *  Message processor.
     */
    private JMSMessageProcessor mProcessor;
    /**
     *    Logger.
     */
    private Logger mLogger;
    /**
     *    Connection.
     */
    private MQConnection mConnection;
    /**
     *    Destination.
     */
    private MQDestination mDestination;
    /**
     *   Consumer.
     */
    private javax.jms.MessageConsumer mConsumer;
    /**
     *  Monitor.
     */
    private Object mMonitor;
    /**
     *  JMS Sesion.
     */
    private javax.jms.Session mSession;
    /**
     *  Durability.
     */
    private String mDurabilty;
    /**
     *  Message Selector.
     */
    private String mMessageSelector = "";
    /**
     *  Unique name.
     */
    private String mUniqueName;
    /**
     *  Initialised.
     */
    private boolean mInitialised = false;
    /**
     *  Receiver.
     */
    private boolean mReceiver;
    /**
     *  Transacted.
     */
    private boolean mTransacted = false;
    /**
     *  Ackonwledgement mode.
     */
    private int mAckMode = Session.CLIENT_ACKNOWLEDGE;       
    /**
     *  Toic or Queue.
     */
    private int mStyle;
    
    /**
     * i18n.
     */
    private StringTranslator mTranslator;

    /**
     * Creates a new JMS Session.
     */
    public MQSession()
    {
        mTransacted = false;
        mProcessor = new JMSMessageProcessor();
        mDurabilty = ConfigConstants.NON_DURABLE;
        mLogger = JMSBindingContext.getInstance().getLogger();
        mTranslator = JMSBindingContext.getInstance().getStringTranslator();
        mMonitor = new Object();
    }

    /**
     * Gets the type of ackowledgement mode for this session.
     *
     * @return type of ACK mode.
     */
    public int getAckMode()
    {
        return mAckMode;
    }

    /**
     * Creates a new MQSession object. This constructor will be used by manager
     * for creating message receivers.
     *
     * @param bean endpooint bean.
     */
    public void setBean(EndpointBean bean)
    {
        setConnection(bean.getConnection());
        setStyle(bean.getStyle());
        setName(bean.getUniqueName());
        setDestination(bean.getDestination());
        setMessageSelector((String) bean.getValue(
                ConfigConstants.MESSAGE_SELECTOR));
        setDurabilty((String) bean.getValue(ConfigConstants.DURABILITY));
    }

    /**
     * Sets the connection object.
     *
     * @param con Wrapper to JMS connection.
     */
    public void setConnection(MQConnection con)
    {
        mConnection = con;
    }

    /**
     * Gets the MQ connection object.
     *
     * @return wrapper to MQ connection.
     */
    public MQConnection getConnection()
    {
        return mConnection;
    }

    /**
     * Sets the destination.
     *
     * @param dest wrapper to JMS destination.
     */
    public void setDestination(MQDestination dest)
    {
        mDestination = dest;
    }

    /**
     * Sets the durability.
     *
     * @param durable durability.
     */
    public void setDurabilty(String durable)
    {
        mDurabilty = durable;
    }

    /**
     * Sets the message selector.
     *
     * @param sel selector.
     */
    public void setMessageSelector(String sel)
    {
        mMessageSelector = sel;
    }

    /**
     * Sets the name.
     *
     * @param name name.
     */
    public void setName(String name)
    {
        mUniqueName = name;
    }

    /**
     * Sets a receiver.
     *
     * @return true if the receiver is set.
     */
    public boolean setReceiver()
    {
        clear();

        if (!mInitialised)
        {
            mLogger.severe(mTranslator.getString(JMS_SESSION_NOT_INITIALIZED));
            setError(mTranslator.getString(JMS_SESSION_NOT_INITIALIZED));

            return false;
        }

        try
        {
            mLogger.fine(mTranslator.getString(JMS_MESSAGE_SELECTOR_FINE,
                    mMessageSelector));

            if (mDurabilty.equals(ConfigConstants.DURABLE))
            {
                mConsumer =
                    mSession.createDurableSubscriber((javax.jms.Topic) mDestination
                        .getDestination(), mUniqueName, mMessageSelector, true);
            }
            else
            {
                mConsumer =
                    mSession.createConsumer(mDestination.getDestination(),
                        mMessageSelector);
            }
        }
        catch (JMSException je)
        {
            setError(mTranslator.getString(JMS_MQ_CANNOT_CREATE_CONSUMER, mDestination));
            setError(je.getMessage());

            return false;
        }

        mReceiver = true;

        return true;
    }

    /**
     * Sets the style.
     *
     * @param style topic or queue.
     */
    public void setStyle(int style)
    {
        mStyle = style;
    }

    /**
     * Gets the style.
     *
     * @return style.
     */
    public int getStyle()
    {
        return mStyle;
    }

    /**
     * Set transacted.
     */
    public void setTransacted()
    {
        mTransacted = true;
    }

    /**
     * Gets the tarnasction status.
     *
     * @return transaction status.
     */
    public boolean getTransacted()
    {
        return mTransacted;
    }

    /**
     * Close.
     */
    public void close()
    {
        try
        {
            mSession.close();
        }
        catch (JMSException je)
        {
            setError(mTranslator.getString(JMS_CANNOT_CLOSE_SESSION, mSession));
            setError(je.getMessage());
        }
    }

    /**
     * Creates a JMS message.
     *
     * @param type type of JMS message.
     *
     * @return JMS message.
     */
    public Message createJMSMessage(String type)
    {
        Message msg = null;

        try
        {
            if (type.equals("TextMessage"))
            {
                msg = mSession.createTextMessage();
            }
            else if (type.equals("StreamMessage"))
            {
                msg = mSession.createStreamMessage();
            }
            else if (type.equals("MapMessage"))
            {
                msg = mSession.createMapMessage();
            }
            else if (type.equals("ByteMessage"))
            {
                msg = mSession.createBytesMessage();
            }
            else if (type.equals("ObjectMessage"))
            {
                msg = mSession.createObjectMessage();
            }
        }
        catch (Exception e)
        {
            setError(mTranslator.getString(JMS_CANNOT_CREATE_MESSAGE, 
                    type));
            setError(e.getMessage());
        }

        return msg;
    }

    /**
     * Initializes the session.
     *
     * @throws JMSException
     */
    public void init()
        throws JMSException
    {
        clear();

        if (mInitialised)
        {
            return;
        }

        try
        {
            if (mStyle == ConfigConstants.QUEUE)
            {
                mSession =
                    ((javax.jms.QueueConnection) (mConnection.getConnection()))
                    .createQueueSession(mTransacted, mAckMode);
            }
            else
            {
                mSession =
                    ((javax.jms.TopicConnection) (mConnection.getConnection()))
                    .createTopicSession(mTransacted, mAckMode);
            }

            mInitialised = true;
            mReceiver = false;
        }
        catch (JMSException je)
        {
            mLogger.severe(mTranslator.getString(JMS_CANNOT_CREATE_SESSION,
                je.getMessage()));
            setError(je.getMessage());
            throw je;
        }
    }

    /**
     * Runs the thread.
     */
    public void run()
    {
        mLogger.fine("Starting Receiver for Destination " + 
                mDestination.getDestination().toString());
        
        if (!mInitialised)
        {
            mLogger.severe(mTranslator.getString(JMS_SESSION_NOT_INITIALIZED));
            setError(mTranslator.getString(JMS_SESSION_NOT_INITIALIZED));

            return;
        }

        JMSBindingContext ctx = JMSBindingContext.getInstance();

        try
        {
            Message msg = null;

            while (mMonitor != null)
            {
                synchronized (this)
                {
                    msg = mConsumer.receive(50);
                    if (msg != null)
                    {
                        msg.acknowledge();
                    }
                }

                MessageHandler hndl = null;

                if (msg != null)
                {
                    hndl = mProcessor.process(mUniqueName, msg);
                }

                if (hndl == null)
                {
                    // decide where to acknowledge or continue
                    continue;
                }

                ctx.getJMSWorkManager().processCommand(hndl);
                mLogger.fine(msg.toString());
            }
        }
        catch (InvalidDestinationException ie)
        {
            ie.printStackTrace();
        }
        catch (InvalidSelectorException is)
        {
            is.printStackTrace();
        }
        catch (JMSException je)
        {
            if (je.getErrorCode().equals(MQCodes.GOODBYE))
            {
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (mConsumer != null)
            {
                try
                {
                    mConsumer.close();
                }
                catch (JMSException je)
                {
                    ;
                }
            }
        }

        mReceiver = false;
    }

    /**
     * Sends the message.
     *
     * @param msg JMS message to be sent.
     * @param dest destination on which it has to be sent.
     *
     * @return true if successfuly sent.
     */
    public synchronized boolean sendMessage(
        Message msg,
        MQDestination dest)
    {
        javax.jms.MessageProducer producer = null;
        clear();

        try
        {
            producer = mSession.createProducer(dest.getDestination());
            msg.setJMSDestination(dest.getDestination());
            producer.send(msg);
            mLogger.fine("Sent message " + msg.toString());
            producer.close();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            setError(t.getMessage());

            return false;

            // dunno what to do wil decide
        }
        finally
        {
            if (producer != null)
            {
                try
                {
                    producer.close();
                }
                catch (JMSException je)
                {
                    ;
                }
            }
        }

        return true;
    }

    /**
     * Stops receiving on this session.
     */
    public void stopReceiving()
    {
        mMonitor = null;
        try        
        {
            mConsumer.close();            
        }
        catch (JMSException je)
        {
            ;
        }
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageAdaptorImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;

import com.sun.jbi.binding.jms.util.UtilBase;

import java.util.HashMap;

import java.util.logging.Logger;

import javax.jbi.messaging.MessageExchange;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.DeliveryMode;

/**
 * Implements a Message Adaptor Interface.
 *
 * @author Sun Microsystems Inc.
 */
class MessageAdaptorImpl
    extends UtilBase
    implements MessageAdaptor, JMSBindingResources
{
    /**
     * Logger object.
     */
    protected Logger mLogger;
    /**
     * Translator.
     */
    protected StringTranslator mStringTranslator;
    /**
     * Reply to.
     */
    private Destination mReplyTo;
    /**
     *  Exception.
     */
    private Exception mException;
    /**
     * Properties.
     */
    private HashMap mJMSProperties;
    /**
     * Correlation ID.
     */
    private String mCorrelationId;
    /**
     * Endpoint  name.
     */
    private String mEndpointName;
    /**
     * Type.
     */
    private String mJMSType;
    /**
     * Message Id.
     */
    private String mMessageId;
    /**
     * Operation.
     */
    private String mOperation;
    /**
     * Operation namespace.
     */
    private String mOperationNamespace;
    /**
     *  Service name.
     */
    private String mServiceName;
    /**
     * Error.
     */
    private StringBuffer mError;
    /**
     * Delivery mode.
     */
    private int mDeliveryMode;
    /**
     * Priority.
     */
    private int mJMSPriority;
    
    /**
     * Epilogue processor
     */
    private Object mEpilogueProcessor;

    /**
     * Creates a new MessageAdaptorImpl object.
     */
    public MessageAdaptorImpl()
    {
        mJMSProperties = new HashMap();
        mError = new StringBuffer();
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Sets error.
     *
     * @param err error.
     */
    public void setError(String err)
    {
        super.setError(err);
    }

    /**
     * Returns the error.
     *
     * @return error.
     */
    public String getError()
    {
        return super.getError();
    }

    /**
     * Sets exception.
     *
     * @param ex exception.
     */
    public void setException(Exception ex)
    {
        super.setException(ex);
    }

    /**
     * Returns exception.
     *
     * @return exception.
     */
    public Exception getException()
    {
        return super.getException();
    }

    /**
     * Setter for property mCorrelationId.
     *
     * @param msg New value of property mCorrelationId.
     */
    public void setJMSCorrelationId(javax.jms.Message msg)
    {
        try
        {
            mCorrelationId = msg.getJMSCorrelationID();
        }
        catch (JMSException je)
        {
            ;
        }
    }

    /**
     * Getter for property mCorrelationId.
     *
     * @return Value of property mCorrelationId.
     */
    public java.lang.String getJMSCorrelationId()
    {
        return mCorrelationId;
    }

    /**
     * Setter for property mDeliveryMode.
     *
     * @param msg New value of property mDeliveryMode.
     */
    public void setJMSDeliveryMode(javax.jms.Message msg)
    {
        try
        {
            mDeliveryMode = msg.getJMSDeliveryMode();
        }
        catch (JMSException je)
        {
            ;
        }
    }

    /**
     * Getter for property mDeliveryMode.
     *
     * @return Value of property mDeliveryMode.
     */
    public int getJMSDeliveryMode()
    {
        return mDeliveryMode;
    }

    /**
     * Sets the JMS headers.
     *
     * @param msg JMS message.
     */
    public void setJMSHeaders(Message msg)
    {
        try
        {
            msg.setJMSCorrelationID(getJMSCorrelationId());
            msg.setJMSMessageID(getJMSMessageId());
            msg.setJMSReplyTo(getJMSReplyTo());
        }
        catch (JMSException je)
        {
            ;
        }
    }

    /**
     * Setter for property mMessageId.
     *
     * @param msg New value of property mMessageId.
     */
    public void setJMSMessageId(javax.jms.Message msg)
    {
        try
        {
            mMessageId = msg.getJMSMessageID();
        }
        catch (JMSException je)
        {
            ;
        }
    }

    /**
     * Getter for property mMessageId.
     *
     * @return Value of property mMessageId.
     */
    public java.lang.String getJMSMessageId()
    {
        return mMessageId;
    }

    /**
     * Setter for property mJMSPriority.
     *
     * @param msg New value of property mJMSPriority.
     */
    public void setJMSPriority(javax.jms.Message msg)
    {
        try
        {
            mJMSPriority = msg.getJMSPriority();
        }
        catch (JMSException je)
        {
            ;
        }
    }

    /**
     * Getter for property mJMSPriority.
     *
     * @return Value of property mJMSPriority.
     */
    public int getJMSPriority()
    {
        return mJMSPriority;
    }

    /**
     * Sets the JMS properties.
     *
     * @param msg JMS message.
     */
    public void setJMSProperties(Message msg)
    {
        try
        {
            msg.setStringProperty(MessageProperties.JBI_SERVICE_NAME,
                getNMSServiceName());

            msg.setStringProperty(MessageProperties.JBI_ENDPOINT_NAME,
                getNMSEndpointName());

            msg.setStringProperty(MessageProperties.JBI_OPERATION_NAME,
                getNMSOperation());

            msg.setStringProperty(MessageProperties.JBI_OPERATION_NAMESPACE,
                getNMSOperationNamespace());
        }
        catch (JMSException je)
        {
            ;
        }
    }

    /**
     * Setter for property mReplyTo.
     *
     * @param msg New value of property mReplyTo.
     */
    public void setJMSReplyTo(javax.jms.Message msg)
    {
        try
        {
            mReplyTo = msg.getJMSReplyTo();
        }
        catch (JMSException je)
        {
            ;
        }
    }

    /**
     * Set JMS reply to.
     *
     * @param reply reply to destination.
     */
    public void setJMSReplyTo(javax.jms.Destination reply)
    {
        mReplyTo = reply;
    }

    /**
     * Getter for property mReplyTo.
     *
     * @return Value of property mReplyTo.
     */
    public javax.jms.Destination getJMSReplyTo()
    {
        return mReplyTo;
    }

    /**
     * Returns JMS status.
     *
     * @param msg JMS message.
     *
     * @return satatus string.
     */
    public String getJMSStatus(Message msg)
    {
        String status = null;

        try
        {
            status = msg.getStringProperty(MessageProperties.JBI_STATUS);
        }
        catch (JMSException je)
        {
            status = new String(MessageProperties.SUCCESS);
        }

        if ((status == null) || (status.trim().equals("")))
        {
            status = new String(MessageProperties.SUCCESS);
        }

        return status;
    }

    /**
     * Setter for property mJMSType.
     *
     * @param msg New value of property mJMSType.
     */
    public void setJMSType(javax.jms.Message msg)
    {
        try
        {
            mJMSType = msg.getJMSType();
        }
        catch (JMSException je)
        {
            ;
        }
    }

    /**
     * Getter for property mJMSType.
     *
     * @return Value of property mJMSType.
     */
    public java.lang.String getJMSType()
    {
        return mJMSType;
    }

    /**
     * Returns name.
     *
     * @return name string.
     */
    public String getName()
    {
        return "";
    }

    /**
     * Gets the reply to destination.
     *
     * @return reply destination.
     */
    public String getReplyToDestinationName()
    {
        if (mReplyTo == null)
        {
            return null;
        }

        String name = null;

        try
        {
            if (mReplyTo instanceof javax.jms.Queue)
            {
                name = ((javax.jms.Queue) (mReplyTo)).getQueueName();
            }
            else if (mReplyTo instanceof javax.jms.Topic)
            {
                name = ((javax.jms.Topic) (mReplyTo)).getTopicName();
            }
        }
        catch (Exception e)
        {
            name = null;
        }

        return name;
    }

    /**
     * Sets the status.
     *
     * @param msg JMS message.
     * @param status status string.
     */
    public void setStatus(
        Message msg,
        String status)
    {
        try
        {
            if (status != null)
            {
                msg.setStringProperty(MessageProperties.JBI_STATUS, status);
            }
        }
        catch (JMSException je)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_CANNOT_SET_JMS_STATUS));
        }
    }

    /**
     * Check validity.
     *
     * @return true if valid.
     */
    public boolean isValid()
    {
        return super.isValid();
    }

    /**
     * Clears all error strings.
     */
    public void clear()
    {
        super.clear();
    }

    /**
     * Converts a JMS message to NMR message.
     *
     * @param msg JMS message.
     * @param exch NMR message.
     */
    public void convertJMStoNMSMessage(
        Message msg,
        MessageExchange exch)
    {
        if (msg != null)
        {
            processJMSHeaders(msg);
            processJMSProperties(msg);
        }

        if (exch != null)
        {
            setNMSProperties(exch);
        }
    }

    /**
     * Convert NMR to JMS messages.
     *
     * @param exch NMR message.
     * @param msg JMS message.
     */
    public void convertNMStoJMSMessage(
        MessageExchange exch,
        Message msg)
    {
        if (exch != null)
        {
            processNMSHeaders(exch);
            processNMSProperties(exch);
        }

        if (msg != null)
        {
            setJMSHeaders(msg);
            setJMSProperties(msg);
        }
    }

    /**
     * Process JMS headers.
     *
     * @param msg JMS message.
     */
    public void processJMSHeaders(Message msg)
    {
        if (msg != null)
        {
            setJMSCorrelationId(msg);
            setJMSDeliveryMode(msg);
            setJMSMessageId(msg);
            setJMSReplyTo(msg);
            setJMSType(msg);
            setJMSPriority(msg);
        }
    }

    /**
     * Process NMR message.
     *
     * @param ex NMR message.
     */
    public void processNMSHeaders(MessageExchange ex)
    {
        if (ex != null)
        {
            setNMSServiceName(ex);
            setNMSEndpointName(ex);
            setNMSOperation(ex);
        }
    }

    /**
     * Process NMR properties.
     *
     * @param ex NMR exchange.
     */
    public void processNMSProperties(MessageExchange ex)
    {
        if (ex != null)
        {
            String cor =
                (String) ex.getProperty(MessageProperties.JMS_CORRELATION_ID);

            if (cor != null)
            {
                setNMSCorrelationId((String) ex.getProperty(
                        MessageProperties.JMS_CORRELATION_ID));
            }
            else
            {
                setNMSCorrelationId(ex.getExchangeId());
            }

            setNMSDeliveryMode((String) ex.getProperty(
                    MessageProperties.DELIVERY_MODE));
            setNMSMessageId(ex.getExchangeId());
        }
    }

    /**
     * Updates the error message.
     *
     * @param msg JMS message.
     * @param obj object.
     */
    public void updateJMSErrorMessage(
        Message msg,
        Object obj)
    {
        ;
    }

    /**
     * Updates the NMR out message.
     *
     * @param msg JMS message.
     * @param exch NMR message.
     */
    public void updateNMSOutMessage(
        Message msg,
        MessageExchange exch)
    {
        ;
    }

    /**
     * Gets the JMS properties.
     *
     * @param msg JMS message.
     * @param property properties.
     *
     * @return string representing properties.
     */
    private String getJMSProperty(
        Message msg,
        String property)
    {
        String value = null;

        try
        {
            value = msg.getStringProperty(property);
        }
        catch (JMSException je)
        {
            ;
        }

        return value;
    }

    /**
     * Sets the NMR correlation Id.
     *
     * @param s correlation id string.
     */
    private void setNMSCorrelationId(String s)
    {
        mCorrelationId = s;
    }

    /**
     * Sets the NMR delivery mode.
     *
     * @param mode delivery mode.
     */
    private void setNMSDeliveryMode(String mode)
    {
        if ((mode != null)
                && (mode.trim().equals(MessageProperties.PERSISTENT_MODE)))
        {
            mDeliveryMode = DeliveryMode.PERSISTENT;
        }
        else
        {
            mDeliveryMode = DeliveryMode.NON_PERSISTENT;
        }
    }

    /**
     * Sets the endpoint name.
     *
     * @param ex NMR message.
     */
    private void setNMSEndpointName(MessageExchange ex)
    {
        mEndpointName = ex.getEndpoint().getEndpointName();
    }

    /**
     * Gets the endpoint name.
     *
     * @return endpoint name.
     */
    private String getNMSEndpointName()
    {
        return mEndpointName;
    }

    /**
     * Sets the NMR message Id.
     *
     * @param id message id.
     */
    private void setNMSMessageId(String id)
    {
        mMessageId = id;
    }

    /**
     * Sets the NMR operation.
     *
     * @param ex NMR exchange.
     */
    private void setNMSOperation(MessageExchange ex)
    {
        mOperation = ex.getOperation().getLocalPart();
        mOperationNamespace = ex.getOperation().getNamespaceURI();
    }

    /**
     * Gets the NMR operation.
     *
     * @return operation name.
     */
    private String getNMSOperation()
    {
        return mOperation;
    }

    /**
     * Gets NMR operation namespace.
     *
     * @return namespace.
     */
    private String getNMSOperationNamespace()
    {
        return mOperationNamespace;
    }

    /**
     * Sets the NMR properties.
     *
     * @param exch NMR message.
     */
    private void setNMSProperties(MessageExchange exch)
    {
        if (getJMSCorrelationId() != null)
        {
            exch.setProperty(MessageProperties.JMS_CORRELATION_ID,
                getJMSCorrelationId());
        }

        if (getJMSDeliveryMode() == DeliveryMode.PERSISTENT)
        {
            exch.setProperty(MessageProperties.DELIVERY_MODE,
                MessageProperties.PERSISTENT_MODE);
        }
        else
        {
            exch.setProperty(MessageProperties.DELIVERY_MODE,
                MessageProperties.NON_PERSISTENT_MODE);
        }

        if (getJMSMessageId() != null)
        {
            exch.setProperty(MessageProperties.JMS_MESSAGE_ID, getJMSMessageId());
        }

        if (getReplyToDestinationName() != null)
        {
            exch.setProperty(MessageProperties.REPLY_TO,
                getReplyToDestinationName());
        }
    }

    /**
     * Sets the NMR service name.
     *
     * @param ex NMR message.
     */
    private void setNMSServiceName(MessageExchange ex)
    {
        mServiceName = ex.getEndpoint().getServiceName().toString();
    }

    /**
     * Gets the NMR service name.
     *
     * @return service name.
     */
    private String getNMSServiceName()
    {
        return mServiceName;
    }

    /**
     * Process JMS properties.
     *
     * @param msg JMS message.
     */
    private void processJMSProperties(Message msg)
    {
        mJMSProperties.put(MessageProperties.JMS_OPERATION,
            getJMSProperty(msg, MessageProperties.JMS_OPERATION));

        mJMSProperties.put(MessageProperties.JBI_STATUS,
            getJMSProperty(msg, MessageProperties.JBI_STATUS));
    }
    
    /**
     * Getter for property mEpilogueProcessor.
     * @return Value of property mEpilogueProcessor.
     */
    public java.lang.Object getEpilogueProcessor() 
    {
        return mEpilogueProcessor;
    }
    
    /**
     * Setter for property mEpilogueProcessor.
     * @param mEpilogueProcessor New value of property mEpilogueProcessor.
     */
    public void setEpilogueProcessor(java.lang.Object mEpilogueProcessor)
    {
        this.mEpilogueProcessor = mEpilogueProcessor;
    }
    
}

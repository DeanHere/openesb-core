/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MQDestination.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.mq;

import com.sun.jbi.binding.jms.util.UtilBase;

import javax.jms.Destination;
import javax.jms.Queue;
import javax.jms.Topic;

/**
 * MQ destination wrapper.
 *
 * @author Sun Microsystems Inc.
 */
public final class MQDestination
    extends UtilBase
{
    /**
     *  JMS destination.
     */
    private javax.jms.Destination mDestination;

    /**
     *  Lookup name.
     */
    private String mLookupName;

    /**
     * Style.
     */
    private int mStyle;
    
    /**
     * Destination name 
     */
    private String mDestinationName;
    

    /**
     * Creates a new MQDestination object.
     *
     * @param dest JMS destination.
     */
    public MQDestination(javax.jms.Destination dest)
    {
        mDestination = dest;
        setStyle();
        setDestinationName();
    }

    /**
     * Creates a new MQDestination object.
     *
     * @param dest jms destination.
     * @param style q or topic.
     * @param lookupname destination lookup name.
     */
    public MQDestination(
        javax.jms.Destination dest,
        int style,
        String lookupname)
    {
        mDestination = dest;
        mStyle = style;
        mLookupName = lookupname;
    }

    /**
     * Gets the destination.
     *
     * @return JMS destination.
     */
    public javax.jms.Destination getDestination()
    {
        return mDestination;
    }
    
    /**
     * Gets the lookup name.
     *
     * @return lookup name.
     */
    public String getDestinationName()
    {
        if (mDestinationName != null)
        {
            return mDestinationName;
        }
        else
        {
            return mLookupName;
        }
    }
    
    
    /**
     * Gets the lookup name.
     *
     * @return lookup name.
     */
    public String getLookupName()
    {
        if (mLookupName != null)
        {
            return mLookupName;
        }
        else
        {
            return mDestinationName;
        }
    }

    /**
     * Gets the style.
     *
     * @return topic or queue.
     */
    public int getStyle()
    {
        return mStyle;
    }

    /**
     * Sets the lookupname.
     */
    private void setDestinationName()
    {
        try
        {
            if (mDestination instanceof javax.jms.Queue)
            {
                mDestinationName = ((javax.jms.Queue) mDestination).getQueueName();
            }
            else if (mDestination instanceof javax.jms.Topic)
            {
                mDestinationName = ((javax.jms.Topic) mDestination).getTopicName();
            }
        }
        catch (Throwable e)
        {
            setError("Destination is not a Queue or topic ");
            setError(e.getMessage());
        }
    }

    /**
     * Sets the style.
     */
    private void setStyle()
    {
        if (mDestination instanceof javax.jms.Queue)
        {
            mStyle = 0;
        }
        else if (mDestination instanceof javax.jms.Topic)
        {
            mStyle = 1;
        }
    }
}

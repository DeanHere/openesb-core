/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageExchangeHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.RobustInOnly;


/**
 * Helper class for processing message exchange.
 *
 * @author Sun Microsystems Inc.
 */
public class MessageExchangeHelper
{
    /**
     * Logger.
     */
    private static Logger sLog = Logger.getLogger("com.sun.jbi.binding.jms");

    /**
     * Creates a new instance of MessageExchangeHelper.
     */
    public MessageExchangeHelper()
    {
    }

    /**
     * Gets the destination name.
     *
     * @param dest destination JMS.
     * @param eb endpoint bean.
     *
     * @return destination name.
     */
    public static Object getDestinationName(
        javax.jms.Destination dest,
        EndpointBean eb)
    {
        if (dest == null)
        {
            return eb.getValue(ConfigConstants.REPLY_TO);
        }
        
        return dest;
    
        /*
        String replyto = null;

        try
        {
            if (dest instanceof javax.jms.Queue)
            {
                replyto = ((javax.jms.Queue) (dest)).getQueueName();
            }
            else if (dest instanceof javax.jms.Topic)
            {
                replyto = ((javax.jms.Topic) dest).getTopicName();
            }
        }
        catch (Exception e)
        {
            ;
        }
         */

    }

    /**
     * Gets the error.
     *
     * @param msg NMR message.
     *
     * @return exception.
     */
    public static Exception getError(MessageExchange msg)
    {
        if (msg == null)
        {
            return null;
        }

        if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_OUT))
        {
            return (((InOut) (msg)).getError());
        }
        else if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_ONLY))
        {
            return null;
        }
        else if (msg.getPattern().toString().trim().equals
                (ConfigConstants.ROBUST_IN_ONLY))
        {
            return (((RobustInOnly) (msg)).getError());
        }
        else
        {
            return null;
        }
    }

    /**
     * Gets fault from NMR msg.
     * 
     * @param msg NMR msg.
     *
     * @return Fault.
     */
    public static Fault getFault(MessageExchange msg)
    {
        if (msg == null)
        {
            return null;
        }

        if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_OUT))
        {
            return (((InOut) (msg)).getFault());
        }
        else if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_ONLY))
        {
            return null;
        }
        else if (msg.getPattern().toString().trim().equals
                (ConfigConstants.ROBUST_IN_ONLY))
        {
            return (((RobustInOnly) (msg)).getFault());
        }
        else
        {
            return null;
        }
    }

    /**
     * Sets the input message.
     *
     * @param msg NMR message.
     * @param nm normalized message.
     */
    public static void setInMessage(
        MessageExchange msg,
        NormalizedMessage nm)
    {
        if (msg == null)
        {
            return;
        }

        try
        {
            if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_OUT))
            {
                ((InOut) (msg)).setInMessage(nm);
            }
            else if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_ONLY))
            {
                ((InOnly) (msg)).setInMessage(nm);
            }
            else if (msg.getPattern().toString().trim().equals
                    (ConfigConstants.ROBUST_IN_ONLY))
            {
                ((RobustInOnly) (msg)).setInMessage(nm);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Gets the normalized message from NMR msg.
     *
     * @param msg NMR msg.
     *
     * @return normalized msg. 
     */
    public static NormalizedMessage getInMessage(MessageExchange msg)
    {
        if (msg == null)
        {
            sLog.severe("Returnign nulll in getInMessage()");

            return null;
        }

        if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_OUT))
        {
            return (((InOut) (msg)).getInMessage());
        }
        else if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_ONLY))
        {
            return (((InOnly) (msg)).getInMessage());
        }
        else if (msg.getPattern().toString().trim().equals
                (ConfigConstants.ROBUST_IN_ONLY))
        {
            return (((RobustInOnly) (msg)).getInMessage());
        }
        else
        {
            return null;
        }
    }

    /**
     * Gets the outbound message.
     * 
     * @param msg NMR msg.
     *
     * @return normalized message.
     */
    public static NormalizedMessage getOutMessage(MessageExchange msg)
    {
        if (msg == null)
        {
            return null;
        }

        if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_OUT))
        {
            return (((InOut) (msg)).getOutMessage());
        }
        else if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_ONLY))
        {
            return null;
        }
        else if (msg.getPattern().toString().trim().equals
                (ConfigConstants.ROBUST_IN_ONLY))
        {
            return null;
        }
        else
        {
            return null;
        }
    }

    /**
     * Creates a new exchange based on pattern. This method can be replaced
     * by the MessageExchangeFactory class.
     *
     * @param pattern mep.
     *
     * @return me.
     */
    public static MessageExchange createExchange(String pattern)
    {
        MessageExchange tmpExchng = null;

        try
        {
            DeliveryChannel chnl = JMSBindingContext.getInstance().getChannel();
            MessageExchangeFactory factory = chnl.createExchangeFactory();

            if (pattern.trim().equals(ConfigConstants.IN_OUT))
            {
                tmpExchng = factory.createInOutExchange();
            }
            else if (pattern.trim().equals(ConfigConstants.IN_ONLY))
            {
                tmpExchng = factory.createInOnlyExchange();
            }
            else if (pattern.toString().trim().equals(ConfigConstants.ROBUST_IN_ONLY))
            {
                tmpExchng = factory.createRobustInOnlyExchange();
            }

            return tmpExchng;
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Updates the message exchange.
     *
     * @param msg message exchange.
     * @param in in message.
     *
     * @return true / false depending if update was successful or not
     */
    public static boolean updateInMessage(
        MessageExchange msg,
        NormalizedMessage in)
    {
        if (msg == null)
        {
            return false;
        }

        if (in != null)
        {
            return createIn(msg, in);
        }

        return true;
    }

    /**
     * Updates the Me with an outbound message.
     *
     * @param msg ME.
     * @param out NM msg.
     * @param error Error.
     * @param fault  Fault.
     *
     * @return true if udpated.
     */
    public static boolean updateOutMessage(
        MessageExchange msg,
        NormalizedMessage out,
        Exception error,
        Fault fault)
    {
        if (msg == null)
        {
            return false;
        }

        if (out != null)
        {
            return createOut(msg, out);
        }

        if (fault != null)
        {
            return createFault(msg, fault);
        }

        if (error != null)
        {
            return createError(msg, error);
        }

        try
        {
            msg.setStatus(ExchangeStatus.DONE);
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }

        return true;
    }

    /**
     * Creates error in ME.
     *
     * @param msg ME.
     * @param error exception.
     *
     * @return true if updated.
     */
    private static boolean createError(
        MessageExchange msg,
        Exception error)
    {
        try
        {
            if (error == null)
            {
                msg.setError(new Exception("Unknown Exception"));

                return true;
            }
            msg.setError(error);

            /* Should do onlt setError or setStatus , should not do both
             */

            //       msg.setStatus(ExchangeStatus.ERROR);     
        }
        catch (Exception me)
        {
            me.printStackTrace();

            return false;
        }

        return true;
    }

    /**
     * Creates fault message in ME.
     *
     * @param msg ME.
     * @param flt fault message.
     *
     * @return true if udpated.
     */
    private static boolean createFault(
        MessageExchange msg,
        Fault flt)
    {
        if (flt == null)
        {
            return false;
        }

        try
        {
            if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_ONLY))
            {
                msg.setStatus(ExchangeStatus.ERROR);
            }
            else if (msg.getPattern().toString().trim().equals(ConfigConstants.OUT_ONLY))
            {
                msg.setStatus(ExchangeStatus.ERROR);
            }

            msg.setFault(flt);
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }

        return true;
    }

    /**
     * Creates inbound messagge in ME.
     *
     * @param msg ME.
     * @param in inbound message.
     *
     * @return true if updated.
     */
    private static boolean createIn(
        MessageExchange msg,
        NormalizedMessage in)
    {
        try
        {
            if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_OUT))
            {
                ((InOut) (msg)).setInMessage(in);
            }
            else if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_ONLY))
            {
                ((InOnly) (msg)).setInMessage(in);
            }
            else if (msg.getPattern().toString().trim().equals
                    (ConfigConstants.ROBUST_IN_ONLY))
            {
                ((RobustInOnly) (msg)).setInMessage(in);
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }

        return true;
    }

    /**
     * Creates the out message.
     *
     * @param msg ME
     * @param out out message
     *
     * @return true /false.
     */
    private static boolean createOut(
        MessageExchange msg,
        NormalizedMessage out)
    {
        try
        {
            if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_OUT))
            {
                ((InOut) (msg)).setOutMessage(out);
            }
            else if (msg.getPattern().toString().trim().equals(ConfigConstants.IN_ONLY))
            {
                msg.setStatus(ExchangeStatus.DONE);
            }
            else if (msg.getPattern().toString().trim().equals
                    (ConfigConstants.ROBUST_IN_ONLY))
            {
                msg.setStatus(ExchangeStatus.DONE);
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }

        return true;
    }
}

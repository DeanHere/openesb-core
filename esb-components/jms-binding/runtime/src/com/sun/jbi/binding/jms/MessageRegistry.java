/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * This is a registry which maintains the exchnage Id and the servicelist
 * runtime  object so that responses can be correlated.
 *
 * @author Sun Microsystems Inc.
 */
public final class MessageRegistry
    implements JMSBindingResources
{
    /**
     * Singleton reference.
     */
    private static MessageRegistry sMe;

    /**
     * List of all registered exchanges.
     */
    private HashMap mExchanges;
    /**
     * Private constructor.
     */
    public MessageRegistry()
    {
        mExchanges = new HashMap();
    }

    /**
     * Chesks if there any active message exchanges.
     *
     * @return true if no exchanges are active.
     */
    public boolean isEmpty()
    {
        return mExchanges.isEmpty();
    }

    /**
     * Retrieves the servicelist runtime object corresponding to each exchange
     * ID.
     *
     * @param exchangeId message xchnage.
     *
     * @return servicelist bean object.
     */
    public Object getObject(String exchangeId)
    {
        Object l;
        l = mExchanges.get(exchangeId);

        return l;
    }

    /**
     * Removes all the entries from the registry.
     */
    public void clearRegistry()
    {
        if (mExchanges == null)
        {
            return;
        }

        mExchanges = new HashMap();
    }

    /**
     * Removes an exchange from the registry.
     *
     * @param exchangeId exchange id.
     */
    public synchronized void deregisterExchange(String exchangeId)
    {
        Object l = mExchanges.remove(exchangeId);
    }

    /**
     * List all the active exchanges.
     *
     * @return iterator iterator.
     */
    public Iterator listActiveExchanges()
    {
        if (mExchanges == null)
        {
            return null;
        }

        Set tmp = mExchanges.keySet();

        if (tmp == null)
        {
            return null;
        }

        return tmp.iterator();
    }

    /**
     * Registers a message Exchange.
     *
     * @param exchnageId exchnage id.
     * @param artifact servicelist.
     */
    public synchronized void registerExchange(
        String exchnageId,
        Object artifact)
    {
        mExchanges.put(exchnageId, artifact);
    }
}

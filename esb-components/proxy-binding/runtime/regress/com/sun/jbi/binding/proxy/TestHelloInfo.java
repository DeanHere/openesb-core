/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestHelloInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/**
 * Tests for the HelloInfo class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestHelloInfo extends junit.framework.TestCase
{    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestHelloInfo(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
   }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * testBasics
     * @throws Exception if an unexpected error occurs
     */
    public void testBasics()
           throws Exception
    {
        HelloInfo       hi;
        HelloInfo       hi2;
        EventImpl           e = new EventImpl();
        
        hi = new HelloInfo("a", 1);
        assertEquals(hi.getInstanceId(), "a");
        assertEquals(hi.getBirthtime(), 1);
        assertEquals(hi.getEventName(), HelloInfo.EVENTNAME);
        hi.encodeEvent(e);
        hi2 = new HelloInfo(e);
        assertEquals(hi.getInstanceId(), hi2.getInstanceId());
        assertEquals(hi.getBirthtime(), hi2.getBirthtime());
    }
}

#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)regress_defs.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for regression tests.

#set this prop to the test domain. default is JBITest
#my_test_domain=domain1

#use global regress setup:
. $SRCROOT/antbld/regress/common_defs.ksh

#override the variable or define other common scripts here
# the following variables are setup by regress scritps
#JBI_DOMAIN_NAME=JBITest
#JBI_ADMIN_HOST=localhost
#JBI_ADMIN_PORT=8687
#JBI_ADMIN_XML=$SRCROOT/ui/src/scripts/jbi_admin.xml
#JBI_DOMAIN_ROOT=$AS8BASE/domains/$JBI_DOMAIN_NAME

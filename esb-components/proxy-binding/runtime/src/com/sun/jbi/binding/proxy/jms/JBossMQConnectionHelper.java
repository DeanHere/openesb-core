/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBossMQConnectionHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.jms;

import com.sun.jbi.binding.proxy.connection.ConnectionException;
import com.sun.jbi.binding.proxy.connection.ConnectionHelper;

import java.util.Hashtable;
import java.util.logging.Logger;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.jms.Topic;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import javax.naming.Context;
import javax.naming.InitialContext;


/**
 * Helper for ConnectionManagers
 *
 * @author Sun Microsystems, Inc.
 */
public class JBossMQConnectionHelper
    implements ConnectionHelper
{
    /**
     *
     */

    /**
     *    
     */
    String mHost;

    /**
     *
     */

    /**
     *    
     */
    String mPort;

    /**
     *
     */

    /**
     *    
     */
    ConnectionFactory mFactory;

    /**
     *
     */

    /**
     *    
     */
    private static final String DEFAULT_HOST = "localhost";

    /**
     *
     */

    /**
     *    
     */
    private static final String DEFAULT_PORT = "1100";

    /**
     *
     */

    /**
     *    
     */
    private static final String PROTOCOL = "jnp://";

    /**
     *
     */

    /**
     *    
     */
    private Context mContext = null;

    /**
     *
     */

    /**
     *    
     */
    private Logger mLog;

    /**
     * Creates a new JBossMQConnectionHelper object.
     *
     * @param host
     * @param port
     */
    public JBossMQConnectionHelper(
        String host,
        String port)
    {
        mHost = host;
        mPort = port;
        mLog = Logger.getLogger("com.sun.jbi.binding.proxy.jms");
        prepare();
    }
   
    /**
     *
     */
    public void prepare()
    {
        Hashtable env = new Hashtable();

        try
        {
            String ctxfac =
                System.getProperty("com.sun.jbi.binding.proxy.connection.factory",
                    "org.jnp.interfaces.NamingContextFactory");
            String urlprefix =
                System.getProperty("com.sun.jbi.binding.proxy.connection.urlprefix",
                    "jboss.naming:org.jnp.interfaces");
            String user =                    
                System.getProperty("com.sun.jbi.binding.proxy.connection.user",
                    "admin");
            String password =                
                System.getProperty("com.sun.jbi.binding.proxy.connection.password",
                    "admin");
            env.put(Context.INITIAL_CONTEXT_FACTORY, ctxfac);
            env.put(Context.URL_PKG_PREFIXES, urlprefix);
            if ((user != null) || (!user.trim().equals("")))
            {
                env.put(Context.SECURITY_PRINCIPAL, user);
            }            
            if (password != null)
            {
                env.put(Context.SECURITY_CREDENTIALS, password);
            }

            if ((mHost == null) || (mHost.equals("")))
            {
                mHost = DEFAULT_HOST;
            }

            if ((mPort == null) || (mPort.equals("")))
            {
                mPort = DEFAULT_PORT;
            }

            String providerurl = "localhost" + ":" + mPort;

            env.put(Context.PROVIDER_URL, providerurl);
            mLog.info("Going to get the context");
            mContext = new InitialContext(env);

            mLog.info("Got the context");
            mLog.info(env.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            mLog.severe("Cannot get Initial context using " + env.toString());
            mContext = null;
        }
    }

    /**
     * Return a JMS ConnectionFactory.
     *
     * @return ConnectionFactory
     *
     * @throws ConnectionException
     */
    public synchronized ConnectionFactory getConnectionFactory()
        throws ConnectionException
    {
        if (mContext == null)
        {
            throw new ConnectionException("No context");
        }

        ConnectionFactory confac = null;

        try
        {
            confac = (ConnectionFactory) mContext.lookup("XAConnectionFactory");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            mLog.severe("Cannot lookup factory ConnectionFactory");
        }

        return confac;
    }

    /**
     * Return a JMS Queue.
     *
     * @param name
     *
     * @return Queue.
     *
     * @throws ConnectionException
     */
    public Queue getQueue(String name)
        throws ConnectionException
    {
        try
        {
            return ((Queue) mContext.lookup("queue/" + name));
        }
        catch (javax.naming.NamingException nEx)
        {
            // means the queue does not exist;
            mLog.info("Destination " + name + " not found, Creating ....");

            return createQueue(name);
        }
    }

    /**
     *
     *
     * @param qname  
     *
     * @return  
     *
     * @throws ConnectionException    
     */
    private Queue createQueue(String qname)
        throws ConnectionException
    {
        if (mContext == null)
        {
            throw new ConnectionException("No context available");
        }

        try
        {
            
            MBeanServerConnection mbs =
                (MBeanServerConnection) mContext.lookup(
                    "jmx/invoker/SingletonRMIAdaptor");
            ObjectName objname =
                new ObjectName("jboss.mq:service=DestinationManager");
            Object [] params = new String[1];
            params[0] = qname;

            String [] signature = new String[1];
            signature[0] = "java.lang.String";
            mbs.invoke(objname, "createQueue", params, signature);
            mLog.info("Created Destination " + qname);

            return (Queue) mContext.lookup("queue/" + qname);
        }
        catch (Exception ce)
        {
            throw new ConnectionException(ce);
        }
    }

    /**
     *
     *
     * @param tname  
     *
     * @return  
     *
     * @throws ConnectionException    
     */
    private Topic createTopic(String tname)
        throws ConnectionException
    {
        if (mContext == null)
        {
            throw new ConnectionException("No context available");
        }

        try
        {
            MBeanServerConnection mbs =
                (MBeanServerConnection) mContext.lookup(
                    "jmx/invoker/SingletonRMIAdaptor");
            ObjectName objname =
                new ObjectName("jboss.mq:service=DestinationManager");
            Object [] params = new String[1];
            params[0] = tname;

            String [] signature = new String[1];
            signature[0] = "java.lang.String";

            mbs.invoke(objname, "createTopic", params, signature);
            mLog.info("Created Destination " + tname);

            return (Topic) mContext.lookup("topic/" + tname);
        }
        catch (Exception ce)
        {
            throw new ConnectionException(ce);
        }
    }

    /**
     * Return a JMS Topic.
     *
     * @param name
     *
     * @return Topic.
     *
     * @throws ConnectionException
     */
    public Topic getTopic(String name)
        throws ConnectionException
    {
        try
        {
            return ((Topic) mContext.lookup("topic/" + name));
        }
        catch (javax.naming.NamingException nEx)
        {
            mLog.info("Destination " + name + " not found, Creating ....");

            return createTopic(name);
        }
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ProxyBindingLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;


import java.io.File;
import java.util.Date;

import com.sun.jbi.binding.proxy.util.Translator;
import com.sun.jbi.util.jmx.MBeanUtils;
import com.sun.jbi.component.ComponentContext;

import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.support.MBeanNamesImpl;
import com.sun.jbi.management.support.JbiNameInfo;

import com.sun.jbi.messaging.DeliveryChannel;

import java.util.logging.Logger;
import java.util.Random;

import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.management.MBeanNotificationInfo;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.ObjectName;
import javax.management.StandardMBean;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;


/**
 * This is the Lifecycle class of the Proxy Binding.  The init and the start()
 * methods of this class  are invoked by  the framework when the component  is
 * started
 *
 * @author Sun Microsystems, Inc.
 */
public class ProxyBindingLifeCycle
    extends com.sun.jbi.management.binding.ModelBindingComponent
    implements javax.jbi.component.ComponentLifeCycle, 
        javax.jbi.component.Component
{
    /**
     * Binding context.
     */
    private ComponentContext    mContext = null;

    /**
     * ProxyBinding instance
     */
    private ProxyBinding        mProxyBinding;

    /**
     * Logger Object
     */
    private Logger              mLog;

    /**
     * Our immutable name
     */
    JbiNameInfo                 mJbiNameInfo;
    
    /**
     * Our JbiInstanceName.
     */
    String                      mInstanceName;
    
    /**
     * Flag to hold the result of init
     */
    private boolean             mInitSuccess = false;

    int                         mNotificationSequence;
    

//
// ---------- Methods defined in javax.jbi.component.ComponentLifeCycle ---------
//
           
    /**
     * Get the ObjectName for any MBean that is provided for managing this
     * binding. In this case, there is none, so a null is returned.
     * @return ObjectName is always null.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Initialize the Proxy BC. This performs initialization required by the
     * Proxy BC but does not make it ready to process messages.  This method is
     * called immediately after installation of the Proxy BC.  It is also
     * called when the JBI framework is starting up, and any time  the BC is
     * being restarted after previously being shut down through  a call to
     * shutdown().
     * @param jbiContext the JBI environment context
     * @throws javax.jbi.JBIException if the BC is unable to initialize.
     */
    public void init(javax.jbi.component.ComponentContext jbiContext)
        throws javax.jbi.JBIException
    {
        //initialize our immutable name:
        mJbiNameInfo = new JbiNameInfo( jbiContext.getComponentName(), true );

        //we are a model component - intialize the model:
        super.initModelBindingComponent( jbiContext, mJbiNameInfo);

        super.bootstrap();
        
        mInstanceName = System.getProperty("com.sun.aas.domainName") + "." + super.mMBeanNames.getJbiInstanceName();
        mContext = (ComponentContext)jbiContext;
        mLog = mContext.getLogger("lifecycle", null);
        Translator.setStringTranslator(
            mContext.getStringTranslator("com.sun.jbi.binding.proxy"));
        mLog.info(Translator.translate(LocalStringKeys.INIT_START));

	
        /* This a guard variable to check if init is a success
         * Nothing should be done in start in case init is a failure
         */
        mInitSuccess = true;
    }
    
    /**
     * Shutdown the BC. This performs cleanup before the BC is terminated.
     * Once this has been called, init() must be called before the BC can be
     * started again with a call to start().
     * @throws javax.jbi.JBIException if the BC is unable to shut down.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mLog.info(Translator.translate(LocalStringKeys.SHUTDOWN_BEGIN));
        mLog.info(Translator.translate(LocalStringKeys.SHUTDOWN_END));
    }

    /**
     * Start the Proxy BC. This makes the Proxy BC ready to process messages.
     * This method is called after init() completes when the JBI framework is
     * starting up, and when the BC is being restarted after a previous call
     * to shutdown(). If stop() was called previously but shutdown() was not,
     * start() can be called without a call to init().
     * @throws javax.jbi.JBIException if the BC is unable to start.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLog.fine(Translator.translate(LocalStringKeys.START_BEGIN, mInstanceName));

        /* check if init is success */
        if (!mInitSuccess)
        {
            mLog.severe(Translator.translate(LocalStringKeys.INIT_FAILED));
            return;
        }

        try
        {
            mProxyBinding = new ProxyBinding(this, mInstanceName);
            mProxyBinding.start();
            //mChannel.setResolver(mResolver);
        }
        catch (MessagingException me)
        {
            mLog.severe(Translator.translate(LocalStringKeys.DELIVERYCHANNEL_FAILED)
                + me.getMessage());
            return;
        }

        mLog.fine(Translator.translate(LocalStringKeys.START_END));
    }

    /**
     * Stop the BC. This makes the BC stop accepting messages for processing.
     * After a call to this method, start() can be called again without first
     * calling init().
     * @throws javax.jbi.JBIException if the BC is unable to stop.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mLog.fine(Translator.translate(LocalStringKeys.STOP_BEGIN, mInstanceName));

        try
        {
            if (mProxyBinding != null)
            {
                mProxyBinding.stop();
                mProxyBinding = null;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        mLog.fine(Translator.translate(LocalStringKeys.STOP_END));
    }
  
//
// ---------- Methods defined in javax.jbi.component.Component ---------
//
           
    /**
     * Get the Lifecycle for this entity.
     * @return the LifeCycle.
     */
    public javax.jbi.component.ComponentLifeCycle getLifeCycle() 
    {
        return this;
    }
    
    /**
     * Get the ServiceUnitManager. None is this instance.
     * @return the ServiceUnitManager.
     */
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager() 
    {
        return null;
    }
  
    /**
     * Retrieves a DOM representation containing metadata which describes the 
     * service provided by this component, through the given endpoint. The 
     * result can use WSDL 1.1 or WSDL 2.0.
     *
     * @param endpoint the service endpoint.
     * @return the description for the specified service endpoint.
     */
    public Document getServiceDescription(ServiceEndpoint endpoint)
    {
        return (mProxyBinding.getServiceDescription(endpoint));
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually 
     * perform the operation desired. 
     *
     * @param endpoint the endpoint to be used by the consumer; must be
     *        non-null.
     * @param exchange the proposed message exchange to be performed; must be
     *        non-null.
     * @return <code>true</code> if this provider component can perform the
     *         given exchange with the described consumer.
     */
    public boolean isExchangeWithConsumerOkay(
        ServiceEndpoint endpoint,
        MessageExchange exchange)
    {
        //
        //  The ProxyBinding needs to forward this to the remote PB that is hosting
        //  the endpoint.
        //
        return (mProxyBinding.isExchangeWithConsumerOkay(endpoint, exchange));
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually 
     * interact with the provider properly. The provider is described by the
     * given endpoint and the service description supplied by that endpoint.
     *
     * @param endpoint the endpoint to be used by the provider; must be
     *        non-null.
     * @param exchange the proposed message exchange to be performed; must be
     *        non-null.
     * @return <code>true</code> if this consumer component can interact with
     *         the described provider to perform the given exchange.
     */
    public boolean isExchangeWithProviderOkay(
        ServiceEndpoint endpoint,
        MessageExchange exchange)
    {
        //
        //  The ProxyBinding is never a consumer. So it should be safe to return false.
        //
       return true;
    }

    /**
     * Resolve the given endpoint reference. This is called by JBI when it is 
     * attempting to resolve the given EPR on behalf of a component.
     * <p>
     * If this component returns a non-null result, it must conform to the
     * following:
     * <ul>
     *   <li>This component implements the {@link ServiceEndpoint} returned.
     *   </li>
     *   <li>The result must not be registered or activated with the JBI
     *       implementation.</li>
     * </ul>
     * 
     * Dynamically resolved endpoints are distinct from static ones; they must
     * not be activated (see {@link ComponentContext#activateEndpoint(QName, 
     * String)}), nor registered (see {@link ComponentContext}) by components. 
     * They can only be used to address message exchanges; the JBI 
     * implementation must deliver such exchanges to the component that resolved
     * the endpoint reference (see {@link 
     * ComponentContext#resolveEndpointReference(DocumentFragment)}).
     * 
     * @param epr the endpoint reference, in some XML dialect understood by
     *        the appropriate component (usually a binding); must be non-null.
     * @return the service endpoint for the EPR; <code>null</code> if the
     *         EPR cannot be resolved by this component.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return (mProxyBinding.resolveEndpointReference(epr));
    }

//
// ---------- Methods defined in javax.management.NotificationEmitter ---------
//

    /**
     * <p>Returns an array indicating, for each notification this
     * MBean may send, the name of the Java class of the notification
     * and the notification type.</p>
     *
     * <p>It is not illegal for the MBean to send notifications not
     * described in this array.  However, some clients of the MBean
     * server may depend on the array being complete for their correct
     * functioning.</p>
     *
     * @return the array of possible notifications.
     */
    public MBeanNotificationInfo[] getNotificationInfo()
    {
        return new MBeanNotificationInfo[] {
            new MBeanNotificationInfo(
                new String[] { ESB_MEMBER_JOIN }, String.class.getName(),
                    "This notification is emitted when an ESB instance member joins." ), 
            new MBeanNotificationInfo(
                new String[] { ESB_MEMBER_LEAVE }, String.class.getName(),
                    "This notification is emitted when an ESB instance member leaves." )};
    }
    
    public static final String      ESB_MEMBER_JOIN = "ESB-Member-Join";
    public static final String      ESB_MEMBER_LEAVE = "ESB-Member-Leave";
 

   
//
//  ---------- Methods locally defined ------------------------------------------
//
    public ComponentContext getComponentContext()
    {
        return mContext;
    }


    public void postNotification(String type, String value)
    {
        Notification        note;
        
        note = new Notification(type, this, ++mNotificationSequence);
        note.setUserData(value);
        getNotificationBroadcaster().sendNotification(note);
    }

}

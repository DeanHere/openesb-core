/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSConnectionManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.jms;

import com.sun.jbi.binding.proxy.ProxyBinding;

import com.sun.jbi.binding.proxy.LocalStringKeys;

import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.binding.proxy.connection.ConnectionHelper;
import com.sun.jbi.binding.proxy.connection.ConnectionManager;
import com.sun.jbi.binding.proxy.connection.ClientConnection;
import com.sun.jbi.binding.proxy.connection.EventConnection;
import com.sun.jbi.binding.proxy.connection.EventInfo;
import com.sun.jbi.binding.proxy.connection.ServerConnection;

import java.util.logging.Logger;
import java.util.HashMap;

import javax.jms.ConnectionFactory;
import javax.jms.Connection;
import javax.jms.BytesMessage;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;

import javax.naming.InitialContext;

/**
 * Implements a JMS instance of the ConnectionManager contract.
 * @author Sun Microsystems, Inc
 */
public class JMSConnectionManager
        implements ConnectionManager
{
    private Logger mLog = Logger.getLogger("com.sun.jbi.binding.proxy.jms");
    
    private ConnectionFactory       mConnFac;
    private Connection              mTopicConn;
    private Connection              mServerQueueConn;
    private Connection              mClientQueueConn;
    private ConnectionHelper        mHelper;
    private Topic                   mTopic;
    private MessageProducer         mProducer;
    private Session                 mSharedQueueSession;
    private HashMap                 mClientConnections;
    private JMSServerConnection     mSC;
    private JMSEventConnection      mEC;
    private String                  mId;
    private String                  mExternalId;

    /** JMS topic name which coordinates registration activity. */
    private static final String TOPIC_NAME = "SunJbiProxyBindingTopic";
   
    /**
     * Creates a new connection manager.
     * Four connections to the JMS resource are used:
     *      The SendEvent connection is used to send events (Many synchronized).
     *      The ReceiveEvent connection is used to receive events(EventProcessor)
     *      The Server connection is used to receive messages (RemoteProcessor).
     *      The Client connection is used to send messages (NMRProcessor).
     * Each connection is used by a single thread (see-above).
     * Three connections are used due to a restriction on connection when using the JMS RA.
     *
     * @param id with unique instance id
     * @param host of our JMS resource
     * @param port on the host of our JMS resource
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public JMSConnectionManager(String id, ConnectionHelper helper) 
    {
        mExternalId = id;
        mId = sanitizeName(id);
        mHelper = helper;
    }
    
    public void start(ProxyBinding pb)
        throws com.sun.jbi.binding.proxy.connection.ConnectionException 
    {
        mLog.fine("Initializing JMS connection manager: ExternalId(" + mExternalId + ") InternalId(" + mId + ")");
        
        try
        {
            mClientConnections = new HashMap();
            mConnFac = mHelper.getConnectionFactory();

            mLog.fine("Creating topic and queue connections");

            mServerQueueConn = mConnFac.createConnection();
            mClientQueueConn = mConnFac.createConnection();
            mTopicConn = mConnFac.createConnection();

            mServerQueueConn.start();
            mClientQueueConn.start();
            mTopicConn.start();
            
            //
            //  Create the JMSServerConnection and session used to handle inbound messages.
            //
            mSC = new JMSServerConnection(this, 
                mServerQueueConn.createSession(false, Session.AUTO_ACKNOWLEDGE),
                    mHelper.getQueue(mId));
            
            //
            //  Create the session used to handle all outbound messages.
            //
            mSharedQueueSession = mClientQueueConn.createSession(false, 
                        Session.AUTO_ACKNOWLEDGE);

            //
            //  Get the topic used for event send/receive.
            //
            mTopic = mHelper.getTopic(TOPIC_NAME);
            
            //
            //  Create a local producer for out-of-band (read: different thread) event sending.
            //
            mProducer  = mSharedQueueSession.createProducer(mTopic);

            //
            //  Create the event connection for all synchronous event traffic.
            //
            mEC = new JMSEventConnection(mTopicConn, mTopic);
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void stop()
    {
    }
    
    public void prepare()
        throws com.sun.jbi.binding.proxy.connection.ConnectionException 
    {
        mHelper.prepare();
    }
    
    /**
     * Create a JMS ServerConnection.
     * @return ServerConnection
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public synchronized ServerConnection getServerConnection()
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        return (mSC);
    }
    
    /**
     * Get a ClientConnection. This may be a recycled connection or a new connection.
     * @param id for this connection
     * @return ClientConnection
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public ClientConnection getClientConnection(String id)
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        ClientConnection        cc;
        Queue                   q;
        
        cc = (ClientConnection)mClientConnections.get(id);
        if (cc == null)
        {
            cc = new JMSClientConnection(mSC, mSharedQueueSession, mHelper.getQueue(id));
            mClientConnections.put(id, cc);
        }
        return (cc);      
    }

    /**
     * Get a ClientConnection based on JMS BytesMessage. Used to capture the JMSReplyTo as the
     * instanceId.
     * @param message used to capture instance id.
     * @return ClientConnection
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    ClientConnection getClientConnection(BytesMessage message)
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        JMSClientConnection     cc;
        String                  id = null;
        
        try
        {
            id = ((Queue)message.getJMSReplyTo()).getQueueName();

            cc = (JMSClientConnection)mClientConnections.get(id);
            if (cc == null)
            {
                cc = new JMSClientConnection(mSC, mSharedQueueSession, mHelper.getQueue(id));
                mClientConnections.put(id, cc);
            }
            cc.setMessage(message);
            return (cc);    
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.ConnectionException(
                Translator.translate(LocalStringKeys.CANT_CREATE_JMS_QUEUESESSION, id), jEx);
        }
    }
 
    /**
     * Create and send an Event. This is used when the event is being generated in a context
     * different from the normall event processing loop.
     */
    public void postEvent(EventInfo info)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        JMSEvent       e;
        Message        m;
        try
        {
            e = new JMSEvent(mSharedQueueSession.createStreamMessage(), info.getEventName());
            info.encodeEvent(e);
            m = e.getMessage();
            m.setJMSType(mId);
            mProducer.send(e.getMessage());
        }
        catch (javax.jms.JMSException jmsEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jmsEx);
        }
    }

   /**
     * Get an EventConnection.
     * @return EventConnection
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    public synchronized EventConnection getEventConnection()
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        return (mEC);      
    }

    /**
     * Close the EventConnection.
     * @throws javax.jbi.JBIException if there is a problem.
     */
    public void closeServerConnection()
    {
        try
        {
            mServerQueueConn.close();
        }
        catch (javax.jms.JMSException jEx)
        {
            mLog.info(Translator.translate(LocalStringKeys.CLOSE_SERVERCONN, jEx));
        }
    }
    
    /**
     * Close the EventConnection.
     */
    public void closeClientConnection()
    {
        try
        {
            mClientQueueConn.close();
        }
        catch (javax.jms.JMSException jEx)
        {
            mLog.info(Translator.translate(LocalStringKeys.CLOSE_CLIENTCONN, jEx));
        }
        
    }
    
    /**
     * Close the EventConnection.
     */
    public void closeEventConnection()
    {
        try
        {
            mTopicConn.close();
        }
        catch (javax.jms.JMSException jEx)
        {
            mLog.info(Translator.translate(LocalStringKeys.CLOSE_EVENTCONN, jEx));
        }
        
    }
            
    /**
     * Gets the instance id of this connection.,
     * @return String containing instance id.
     */
    public String getInstanceId()
    {
        return (mId);
    }
    
    /**
     * Gets the instance id of this connection.,
     * @return String containing instance id.
     */
    public String getExternalInstanceId()
    {
        return (mExternalId);
    }
    
    /**
     * Takes an external name and converts it into a viable JMS Queue name.
     * @param is to be sanitized
     * @return String containing the sanitized id.
     */
    String sanitizeName(String id)
    {
        StringBuffer        sb = new StringBuffer();
        
        for (int i = 0; i < id.length(); i++)
        {
            char    ch = id.charAt(i);
            
            if (ch == '@')
            {
                sb.append("_at_");
            }
            else if (ch == '_')
            {
                sb.append("__");
            }
            else if (ch == '.')
            {
                sb.append("_dot_");
            }
            else if (ch == ':')
            {
                sb.append("_colon_");
            }
            else if (ch == '-')
            {
                sb.append("_minus_");
            }
            else
            {
                sb.append(ch);
            }
        }
        
        return (sb.toString());
    }
}

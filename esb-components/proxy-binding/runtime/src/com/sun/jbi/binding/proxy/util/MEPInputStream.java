/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MEPInputStream.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.util;


import com.sun.jbi.binding.proxy.ExchangeEntry;
import com.sun.jbi.binding.proxy.LocalStringKeys;
import com.sun.jbi.binding.proxy.ProxyBinding;

import com.sun.jbi.binding.proxy.connection.ClientConnection;

import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.messaging.MessageExchange;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.URI;

import java.util.HashMap;
import java.util.logging.Logger;

import javax.activation.DataHandler;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.dom.DOMResult;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


/** Implementation of an input stream that can deserialize a MEP.
 * @author Sun Microsystems, Inc
 */
public class MEPInputStream 
        extends java.io.InputStream
{
        private InputStream             mIS;
        private MEPObjectInputStream    mOIS;
        private int                     mCount;
        private int                     mOffset;
        private int                     mStream;
        private int                     mNextStream;
        private int                     mMsgType;
        private byte[]                  mBytes;
        private String                  mExchangeId;
        private Exception               mException;
        private Transformer             mTransform;
        private ProxyBinding            mPB;
        private ExchangeEntry           mExchangeEntry;
        private boolean                mMEPOK;
        
        static final int                BUFFER_SIZE = 1024;
        public static final int      TYPE_MEP = 1;
        public static final int      TYPE_EXCEPTION = 2;
        public static final int      TYPE_ISMEPOK = 3;
        public static final int      TYPE_MEPOK = 4;
        
        static private Logger          mLog;
        
        static final int                VERSION_1 = 1;
        static final int                VERSION_CURRRENT = VERSION_1;
        
        public MEPInputStream(ProxyBinding proxyBinding)
            throws javax.jbi.messaging.MessagingException
        {
            mPB = proxyBinding;
            mBytes = new byte[BUFFER_SIZE];
            if (mLog == null)
            {
                mLog = mPB.getLogger("input");
            }
            
            try
            {
                // initialize transformer details
                mTransform = TransformerFactory.newInstance().newTransformer();            
            }
            catch (javax.xml.transform.TransformerFactoryConfigurationError tfcEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPINPUT_TRANSFORMER_ERROR), tfcEx);
            }
            catch (javax.xml.transform.TransformerConfigurationException cfgEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPINPUT_TRANSFORMER_ERROR), cfgEx);
            }
        }
               
       public int readMessage(ClientConnection cc)
            throws javax.jbi.messaging.MessagingException
       {
            MessageExchange         me = null;
            ExchangeEntry           ee = null;
            byte[]                  bytes = cc.receive();
            
            try
            {
                //
                //  Prepare for reading.
                //
                mCount = 0;
                mOffset = 0;
                mStream = 0;
                mNextStream = 0;
                mIS = new ByteArrayInputStream(bytes);
                mOIS = new MEPObjectInputStream(this);
                mOIS.setClassLoader(((com.sun.jbi.messaging.DeliveryChannel)mPB.getDeliveryChannel()).getClassLoader());
                
                //
                //  Get message type.
                //
                mMsgType = mOIS.readByte();
                
                //
                //  Read contents of message type.
                //
                if (mMsgType == TYPE_EXCEPTION)
                {
                    readException();
                }
                else if (mMsgType == TYPE_MEP)
                {
                    mExchangeEntry = ee = readMEP();
                    ee.setClientConnection(cc);
                    ee.receivedBytes(bytes.length);
                }
                else if (mMsgType == TYPE_ISMEPOK)
                {
                    mExchangeEntry = ee = readISMEPOK();
                    ee.setClientConnection(cc);
                    ee.receivedBytes(bytes.length);
                }
                else if (mMsgType == TYPE_MEPOK)
                {
                    mMEPOK = readMEPOK();
                }
                else
                {
                    
                }
                
                //
                // This may reference a large byte[], so kill the reference ASAP.
                //
                mIS = null;
            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(LocalStringKeys.MEPINPUT_IO_ERROR), ioEx);
            }
            return (mMsgType);
       }
    
       void readException()
            throws javax.jbi.messaging.MessagingException,
                java.io.IOException
       {
            if (mOIS.readByte() == VERSION_1)
            {
                //
                //  Read the exchangeId.
                //
                mExchangeId = mOIS.readUTF();

                //
                //  Read the exception.
                //
                try
                {
                    mException = (Exception)mOIS.readObject();
                }
                catch (java.lang.ClassNotFoundException cnfEx)
                {
                    throw new javax.jbi.messaging.MessagingException(
                            Translator.translate(LocalStringKeys.MEPINPUT_IO_ERROR), cnfEx);
                }

            }

       }
       
       boolean readMEPOK()
            throws javax.jbi.messaging.MessagingException,
                java.io.IOException
       {
            if (mOIS.readByte() == VERSION_1)
            {
                //
                //  Read the exchangeId.
                //
                mExchangeId = mOIS.readUTF();

                //
                //  Read the exception.
                //
                return (mOIS.readBoolean());
            }
            return (false);
       }
       
       ExchangeEntry readISMEPOK()
            throws javax.jbi.messaging.MessagingException,
                java.io.IOException
       {
            ExchangeEntry           ee = null;

            //
            //  Check version #.
            //
            if (mOIS.readByte() == VERSION_1)
            {
                //
                //  Read the exchangeId.
                //
                mExchangeId = mOIS.readUTF();

                //
                //  Read the proposed exchange.
                //
                ee = readExchange();
            }
            else
            {
                
            }

            return (ee);
           
       }
       
       ExchangeEntry readMEP()
             throws javax.jbi.messaging.MessagingException,
                java.io.IOException
      {
            ExchangeEntry           ee = null;

            //
            //  Check version #.
            //
            if (mOIS.readByte() == VERSION_1)
            {
                //
                //  Read the exchange contents.
                //
                ee = readExchange();
            }
            else
            {
                
            }

            return (ee);
       }
       
       ExchangeEntry    readExchange()
            throws javax.jbi.messaging.MessagingException,
                java.io.IOException
       {
            ExchangeEntry           ee = null;
            MessageExchange         me;
            String                  endpoint;
            QName                   qname;
            URI                     pattern;
            NormalizedMessage       nm;
              
            try
            {
                //
                //  Read the exchangeId.
                //
                mExchangeId = mOIS.readUTF();

                //
                //  Only read the addressing information when sent (typically just the first time.)
                //
                if (mOIS.readBoolean())
                {
                    ServiceEndpoint     se;

                    pattern = new URI(mOIS.readUTF());
                    me = (com.sun.jbi.messaging.MessageExchange)
                        ((com.sun.jbi.messaging.DeliveryChannel)mPB.getDeliveryChannel()).createExchange(pattern, mExchangeId);
                    qname = readQName();
                    endpoint = mOIS.readUTF();
                    se = ((com.sun.jbi.messaging.DeliveryChannel)mPB.getDeliveryChannel()).createEndpoint(qname, endpoint);
                    if (se == null)
                    {
                        throw new javax.jbi.messaging.MessagingException(
                            Translator.translate(LocalStringKeys.MEPINPUT_SERVICEENDPOINT_NOTFOUND, qname, endpoint));
                    }
                    me.setEndpoint(se);
                    me.setOperation(readQName());
                    ee = mPB.trackExchange(mExchangeId, me, false);
                    ee.setStatus(ExchangeEntry.STATUS_FIRSTSENT);
                    ee.setService(qname);
                }
                else
                {
                    ee = mPB.getExchangeEntry(mExchangeId); 
                    if (ee != null)
                    {
                        me = ee.getMessageExchange();
                    }
                    else
                    {
                        throw new javax.jbi.messaging.MessagingException(
                                Translator.translate(LocalStringKeys.MEPINPUT_UNKNOWN_EXCHANGE, mExchangeId));

                    }
                }

                if (mOIS.readBoolean())
                {
                    me.setStatus(ExchangeStatus.valueOf(mOIS.readUTF()));
                    ee.setStatus(ExchangeEntry.STATUS_STATUSSENT);
                }
                if (mOIS.readBoolean())
                {
                    me.setError((Exception)mOIS.readObject());
                    ee.setStatus(ExchangeEntry.STATUS_ERRORSENT);
                }
                if (mOIS.readBoolean())
                {
                    nm = readMessage(me);
                    if (nm != null)
                    {
                        me.setFault((Fault)nm);
                    }
                    ee.setStatus(ExchangeEntry.STATUS_FAULTSENT);
                }
                if (mOIS.readBoolean())
                {
                    nm = readMessage(me);
                    if (nm != null)
                    {
                        me.setMessage(nm, "in");
                    }
                    ee.setStatus(ExchangeEntry.STATUS_INSENT);
                }
                if (mOIS.readBoolean())
                {
                    nm = readMessage(me);
                    if (nm != null)
                    {
                        me.setMessage(nm, "out");
                    }
                    ee.setStatus(ExchangeEntry.STATUS_OUTSENT);

                }
                if (mOIS.readBoolean())
                {
                    readMEProperties(me);        
                    me.mergeProperties();
                }
            }
            catch (java.lang.ClassNotFoundException cnfEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(LocalStringKeys.MEPINPUT_IO_ERROR), cnfEx);
            }
            catch (java.net.URISyntaxException usEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(LocalStringKeys.MEPINPUT_IO_ERROR), usEx);
            }

            return (ee);
       }
       
       void readMEProperties(MessageExchange me)
            throws  javax.jbi.messaging.MessagingException
        {
            int     size;
            String  name = null;
            Object  value;
            
            try
            {
                for (size = mOIS.readInt(); size > 0; size--)
                {
                    name = mOIS.readUTF();
                    value = mOIS.readObject();
                    me.setProperty(name, value);
                }
            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPINPUT_ME_PROP_ERROR, name), ioEx);
            }
            catch (java.lang.ClassNotFoundException cnfEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPINPUT_ME_PROP_ERROR, name), cnfEx);
            }
        }

        NormalizedMessage readMessage(MessageExchange me)
            throws java.io.IOException,
                   javax.jbi.messaging.MessagingException
        {
            NormalizedMessage         m = null;
            
            if (mOIS.readBoolean())
            {
                DOMResult       dr;
                
                if (mOIS.readBoolean())
                {
                    m = me.createMessage();
                }
                else
                {
                    m = me.createFault();
                }
                readNMProperties(m);
                try
                {
                    if (mOIS.readBoolean())
                    {
                        Source  source;
                        String  systemId = null;
                        
                        if (mOIS.readBoolean())
                        {
                            systemId = mOIS.readUTF();
                        }

                        startStream();
                        source = new StreamSource(this);
                        mTransform.transform(source, dr = new DOMResult());
                        endStream();
                        m.setContent(source = new DOMSource(dr.getNode()));
                        if (systemId != null)
                        {
                            source.setSystemId(systemId);
                        }
                    }
                }
                catch (javax.xml.transform.TransformerException tEx)
                {
                    throw new javax.jbi.messaging.MessagingException(tEx);
                }
                readAttachments(m);
            }

            return (m);
        }
        
        void readAttachments(NormalizedMessage nm)
            throws javax.jbi.messaging.MessagingException
        {
            int             size;
            String          name = null;
            Object          value;
            String          contentName;
            String          content;
            DataHandler     attach = null;
            
            try
            {
                for (size = mOIS.readInt(); size > 0; size--)
                {
                    name = mOIS.readUTF();
                    contentName = mOIS.readUTF();
                    content = mOIS.readUTF();
                    attach = new DataHandler(new StreamDataSource(contentName, content));
                    startStream();
                    writeTo(attach.getOutputStream());
                    endStream();
                    nm.addAttachment(name, attach);
                }
            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPINPUT_ME_ATTACH_ERROR, name), ioEx);
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPINPUT_ME_ATTACH_ERROR, name), mEx);
            }
       }
 
        void readNMProperties(NormalizedMessage nm)
            throws javax.jbi.messaging.MessagingException
        {
            int     size;
            String  name = null;
            Object  value;
            
            try
            {
                for (size = mOIS.readInt(); size > 0; size--)
                {
                    name = mOIS.readUTF();
                    value = mOIS.readObject();
                    nm.setProperty(name, value);
                }
            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPINPUT_NM_PROP_ERROR, name), ioEx);
            }
            catch (java.lang.ClassNotFoundException cnfEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPINPUT_NM_PROP_ERROR, name), cnfEx);                
            }
        }

        QName readQName()
            throws java.io.IOException
        {
            String      namespace;
            String      localPart;
            String      prefix;
            
            namespace = mOIS.readUTF();
            localPart = mOIS.readUTF();
            prefix = mOIS.readUTF();
            return (new QName(namespace, localPart, prefix));
        }
//
// ------------------- Methods to query about message infomation  ------------------
//
        
        public String getExchangeId()
        {
           return (mExchangeId);
        }

        public Exception getException()
        {
           return (mException);
        }
        
        public ExchangeEntry getExchangeEntry()
        {
            return (mExchangeEntry);
        }
       
        public boolean getMEPOk()
        {
            return (true);
        }
        
//
// ------------------- Methods that implement InputStream  ------------------
//
        
        public int available()
            throws java.io.IOException
        {
            return (mCount);
        }
        
        public void close()
            throws java.io.IOException
        {
            
        }
        
        public void mark(int limit)
        {
            
        }
        
        public boolean markSupported()
        {
            return (false);
        }
        
        public int read()
            throws java.io.IOException
        {
             if (mCount == 0)
            {
                if (fill() == 0)
                    return (-1);
            }

            mCount--;
            return ((int)(mBytes[mOffset++]) & 0xff);
        }
        
        public int read(byte[] b)
            throws java.io.IOException
        {
            int         len = b.length;
            
            return (read(b, 0, len));
        }
        
        public int read(byte[] b, int off, int len)
            throws java.io.IOException
        {
            int             count = 0;
            
            if (mNextStream != mStream)
            {
                return (-1);
            }
            while (len > 0)
            {
                if (mCount == 0)
                {
                    if (fill() == 0)
                    {
                        if (count == 0)
                        {
                            return (-1);
                        }
                        return (count);
                    }
                }
                if (len < mCount)
                {
                    System.arraycopy(mBytes, mOffset, b, off, len);
                    mCount -= len;
                    mOffset += len;
                    count += len;
                    return (count);
                }
                System.arraycopy(mBytes, mOffset, b, off, mCount);
                off += mCount;
                count += mCount;
                len -= mCount;
                mOffset -= mCount;
                mCount = 0;
             }
            
            return (count);
        }
        
        public void writeTo(OutputStream os)
            throws java.io.IOException
        {
            if (mNextStream == mStream)
            {
                byte[]  buffer = new byte[1024];
                int len;
                    
                while ((len = read(buffer)) > 0)
                {
                    os.write(buffer, 0, len);
                }
                os.close();
                return;
            }
            throw new java.io.IOException(
                Translator.translate(LocalStringKeys.MEPINPUT_STREAM_SYNC_ERROR,
                    new Integer(mStream), new Integer(mNextStream)));
        }
        
        public void reset()
            throws java.io.IOException
        {
            
        }
        
//
// ------------------- Internal implementation of InputStream  ------------------
//
        
        private void startStream()
            throws java.io.IOException
        {
            int     s = 0;
            
            if (mCount == 0)
            {
                mStream++;
                if ((s = read()) == mStream)
                {
                    return;
                }
            }
            throw new java.io.IOException(
                Translator.translate(LocalStringKeys.MEPINPUT_START_STREAM_ERROR,
                    new Integer(s), new Integer(mStream)));
        }
        
        private void endStream()
            throws java.io.IOException
        {
            if (mStream != mNextStream)
            {
                mStream--;
                return;
            }
            throw new java.io.IOException(
                Translator.translate(LocalStringKeys.MEPINPUT_END_STREAM_ERROR,
                    new Integer(mStream)));
        }
        
        private int fill()
            throws java.io.IOException
        {
            byte        stream, len1, len2;
            
            mNextStream = (byte)mIS.read();
            len1 = (byte)mIS.read();
            len2 = (byte)mIS.read();
            mCount = (len1 & 0xff) << 8 | (len2 & 0xff);
            mOffset = 0;
            if (mCount == 0)
            {
                return (0);
            }
            if (mIS.read(mBytes, 0, mCount) != mCount)
            {
                throw new java.io.IOException(
                    Translator.translate(LocalStringKeys.MEPINPUT_FILL_STREAM_ERROR,
                        new Integer(mCount)));
            }
            if (mLog.isLoggable(java.util.logging.Level.FINE))
            {
                dumpBuffer("MEPInputStream(" + mNextStream + ") Len (" + mCount + ")", mBytes, mCount);
            }
            if (mNextStream != mStream)
            {
               return (0);
            }
            return (mCount);
        }
        
//
// ------------------- Debugging stuff  ------------------
//

        static void dumpBuffer(String prefix, byte[] buffer, int count)
        {
            StringBuffer    sb = new StringBuffer();
            sb.append(prefix);
            sb.append("\n    ");
            for (int i = 0;; i++)
            {
                int c;

                if (i < count)
                {                        
                    c = (char)buffer[i];
                    sb.append("0123456789ABCDEF".charAt((c >> 4) & 0xf));
                    sb.append("0123456789ABCDEF".charAt(c & 0xf));
                    sb.append(" ");
                }
                else
                {
                    sb.append("   ");
                }
                if (i != 0 && (i % 16) == 15)
                {
                    sb.append("|");
                    for (int j = i - 15; j <= i; j++)
                    {
                        if (j < count)
                        {
                            char cc = (char)buffer[j];
                            if (cc >= ' ' && cc <= '~')
                            {
                                sb.append(cc);
                            }
                            else
                            {
                                sb.append(".");
                            }
                        }
                        else
                        {
                            sb.append(" ");
                        }
                    }
                    sb.append("|\n");

                    if (i + 1 >= count)
                    {
                        break;
                    }
                    sb.append("    ");       
                }
            }
            mLog.fine(sb.toString());

        }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RemoteProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import com.sun.jbi.binding.proxy.connection.ConnectionManager;
import com.sun.jbi.binding.proxy.connection.ConnectionManagerFactory;
import com.sun.jbi.binding.proxy.connection.ClientConnection;
import com.sun.jbi.binding.proxy.connection.EventConnection;
import com.sun.jbi.binding.proxy.connection.ServerConnection;
import com.sun.jbi.binding.proxy.connection.EventInfo;
import com.sun.jbi.binding.proxy.connection.EventInfoFactory;

import com.sun.jbi.binding.proxy.util.MEPInputStream;
import com.sun.jbi.binding.proxy.util.MEPOutputStream;
import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.messaging.DeliveryChannel;
import com.sun.jbi.messaging.EndpointListener;
import com.sun.jbi.messaging.ExchangeFactory;
import com.sun.jbi.messaging.MessageExchange;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Logger;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/**
 * Runnable class that implements the Remote receiver. All traffic from any remote instance is
 * first handled here. The remote traffic is either directed at this ProxyBinding instance or at 
 * an endpoint for a local component.
 * The basic flow is:
 *      Decide if message is for us or for local compoent.
 *      If the exchange is for us, process it.
 *      Else, lookup the local compoent that hosts the service.
 *      Send the to the NMR for local routing
 *      Remember the exchange if new, forget the exchange is this is the last send.
 *
 * @author Sun Microsystems, Inc
 */
class RemoteProcessor
        implements  java.lang.Runnable                    
{
    private Logger                      mLog;
    private ProxyBinding                mPB;
    private ConnectionManager           mCM;
    private boolean                     mRunning;
    private DeliveryChannel             mChannel;
    private MEPInputStream              mMIS;
    private MessageExchangeFactory      mFactory;
    
    RemoteProcessor(ProxyBinding proxyBinding)
    {
         mPB = proxyBinding;
         mLog = mPB.getLogger("remote");        
         mChannel = mPB.getDeliveryChannel();
         mCM = mPB.getConnectionManager();
         mRunning = true;
         mFactory = mPB.getDeliveryChannel().createExchangeFactory();
    }
    
    void stop()
    {
        mRunning = false;
    }
    
    public void run() 
    {
        ServerConnection        sc;
        
        //
        // Set up JMS Queue based on our instance-id for inbound messages.
        //
        try
        {
            sc = mCM.getServerConnection();
            mLog.fine("PB:RemoteProcessor starting as (" + sc.getInstanceId() + ")");
            mMIS = new MEPInputStream(mPB);
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            mLog.warning(Translator.translate(LocalStringKeys.RP_MESSAGING_EXCEPTION,mEx));
            return;
        }
        catch (Exception ex)
        {
            mLog.warning(Translator.translate(LocalStringKeys.RP_INIT_EXCEPTION,ex));
            return;                               
        }


        //
        // Basic processing loop.
        //
        for (;mRunning;)
        {                
            try
            {
                ClientConnection        cc = null;
                int                     msgType;
            
                //
                // Accept a new client connection.
                //
                cc = sc.accept();
                if (cc == null)
                {   
                    continue;
                }

                //
                //  Read the message from the connection.
                //
                msgType = mMIS.readMessage(cc);
                
                //
                //  Process based on type of message.
                //
                if (msgType == MEPInputStream.TYPE_MEP)
                {
                    doMEP();
                }
                else if (msgType == MEPInputStream.TYPE_EXCEPTION)
                {
                    doException();
                }
                else if (msgType == MEPInputStream.TYPE_ISMEPOK)
                {
                    doISMEPOk();
                }
                else if (msgType == MEPInputStream.TYPE_MEPOK)
                {
                    doMEPOK();                 
                }
                else
                {
                }
                
            }
            catch (javax.jbi.messaging.MessagingException mEx)
            {
                if (mRunning)
                {
                    logExceptionInfo(Translator.translate(LocalStringKeys.RP_EXCEPTION), mEx);
                    handleError(mEx);
                }
            }
            catch (Exception ex)
            {
                logExceptionInfo(Translator.translate(LocalStringKeys.RP_EXCEPTION), ex);

                //
                //  Fail fast in the face of unanticipated problems.
                //
                mPB.stop();
            }
         }
    } 
    
    void doException()
    {
        mLog.fine("PB:RemoteProcessor Exception Id(" + mMIS.getExchangeId() + ")");
        handleError(mMIS.getException());
    }
    
    void doISMEPOk()
    {
        ExchangeEntry      ee = mMIS.getExchangeEntry();
        MessageExchange    me = ee.getMessageExchange();
        InOnly             io;
        
        mLog.fine("PB:RemoteProcessor IsMEPOk Id(" + me.getExchangeId() + ") From (" 
            + ee.getClientConnection().getInstanceId() +") Bytes(" + ee.getBytesReceived() + ")");
        try
        {
            io = mFactory.createInOnlyExchange();
            io.setService(mPB.getService().getServiceName());
            io.setOperation(new QName(NMRProcessor.ISEXCHANGEOKAY2));
            io.setProperty(NMRProcessor.EXCHANGE, me);
            mChannel.send(io);
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            mLog.info("PB:RemoteProcessor doISMEPOk MessagingException:" + mEx);
        }
    }
    
    void doMEPOK()
    {
        String             id = mMIS.getExchangeId();
        ExchangeEntry      ee = mPB.getExchangeEntry(id);
        MessageExchange    me = ee.getRelatedExchange();
        boolean            ok = mMIS.getMEPOk();
        
        //
        //  Forward MEP OK status to requestor.
        //
        mLog.fine("PB:RemoteProcessor MEPOk Id(" + id + ") From (" 
            + ee.getClientConnection().getInstanceId() +") Bytes(" + ee.getBytesReceived() + ")");
        try
        {
            me.setProperty(NMRProcessor.EXCHANGEOKAY, Boolean.valueOf(ok));
            me.setStatus(ExchangeStatus.DONE);
            mChannel.send(me);
        }
        catch (javax.jbi.messaging.MessagingException mEx)
        {
            mLog.fine("PB:RemoteProcessor doMEPOk MessagingException:" + mEx);            
        }
        
        //
        //  If the exchange is going to fail, we need to cleanup allocated resources.
        //
        if (!ok)
        {
            mPB.purgeExchange(id);
        }
    }
    
    void doMEP()
        throws javax.jbi.messaging.MessagingException
    {
        ExchangeEntry      ee = mMIS.getExchangeEntry();
        MessageExchange    me = ee.getMessageExchange();
        ClientConnection   cc = ee.getClientConnection();
        String             id = me.getExchangeId();
        long              byteCount = ee.getBytesReceived();

        //
        // Perform any first time processing on the exchange.
        //
        if (ee.checkState(ExchangeEntry.STATE_FIRST))
        {
            mLog.fine("PB:RemoteProcessor exchange Id(" + id + ") From (" 
                + cc.getInstanceId() +") Bytes(" + byteCount + ")");
            if (me.getEndpoint() == null)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.NO_ENDPOINT_AVAILABLE, ee.getService()));
            }
            ee.setState(ExchangeEntry.STATE_ONGOING);
        }
        else
        {
             mLog.fine("PB:RemoteProcessor message Id(" + id + ") From (" 
                + cc.getInstanceId() +") Bytes(" + byteCount + ")");                  
        }                

        //
        // Send it on its merry way.
        //
        mLog.fine("PB:RemoteProcessor send Id(" + id + ")");
        if (mChannel.sendLast(me))
        {
            mLog.fine("PB:RemoteProcessor forget Id(" + id + ")");
            mPB.purgeExchange(id);
        }
    }
    
    void handleError(Exception e)
    {
        ExchangeEntry       ee;
        ClientConnection    cc;
        String              id;
        
        id = mMIS.getExchangeId();
        ee = mPB.getExchangeEntry(id);
        
        //
        //  Ignore this message if we don't know anything about it. Could be a stale message from
        //  the most recent restart.
        //
        if (ee != null)
        {
            cc = ee.getClientConnection();
            if (cc != null)
            {
                //
                //  Send error indication back to the remote NMR based component.
                //
                try
                {
                    mLog.warning("PB:RemoteProcessor forward exception Id(" + id + ") Reason(" + e.toString() + ")");
                    cc.send(new MEPOutputStream(mPB).writeException(id, e));
                }
                catch (javax.jbi.JBIException jEx)
                {
                    //
                    //  Ignore problems sending.
                    //
                }
            }
        
            //
            //  Send error indication back to local NMR based component.
            //  This is only possible if we have an ongoing conversation with this ME.
            //
            try
            {
                MessageExchange     me = ee.getMessageExchange();
                
                mLog.warning("PB:RemoteProcessor set exception Id(" + id + ") Reason(" + e.toString() + ")");
                if (me != null)
                {
                    me.setError(e);
                    mChannel.send(me);
                }
            }
            catch (javax.jbi.JBIException jEx)
            {
                //
                //  Ignore problems sending.
                //
                mLog.warning("PB:RemoteProcessor ignore exception Id(" + id + ") Reason(" + jEx.toString() + ")");
            }
            catch (java.lang.IllegalStateException isEx)
            {
                //
                //  Ignore problems sending.
                //
                mLog.warning("PB:RemoteProcessor ignore exception Id(" + id + ") Reason(" + isEx.toString() + ")");
            }
        }
        mLog.warning("PB:RemoteProcessor dropping exchange Id(" + id + ") Reason(" + e.toString() + ")");
        mPB.purgeExchange(id);
    }
    
    void logExceptionInfo(String reason, Throwable t)
    {
        StringBuffer                    sb = new StringBuffer();
        java.io.ByteArrayOutputStream   b;
        Throwable                       nextT = t;
        
        sb.append(reason);
        while (t != null)
        {
            if (nextT != null)
            {
                java.io.PrintStream ps = new java.io.PrintStream(b = new java.io.ByteArrayOutputStream());
                t.printStackTrace(ps);
                sb.append(" Exception (");
                sb.append(b.toString());
                sb.append(") ");    
            }
            nextT = t.getCause();
            if (nextT != null)
            {
                sb.append("Caused by: ");
            }
            t = nextT;
        }
        
        mLog.warning(sb.toString());        
    }

}
 

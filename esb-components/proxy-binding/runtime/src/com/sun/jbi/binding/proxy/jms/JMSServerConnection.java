/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSServerConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.jms;

import com.sun.jbi.binding.proxy.LocalStringKeys;

import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.binding.proxy.connection.ServerConnection;
import com.sun.jbi.binding.proxy.connection.ClientConnection;

import java.util.logging.Logger;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

/**
 * Connection as a JMS Server.
 * @author Sun Microsystems, Inc
 */
public class JMSServerConnection 
        implements ServerConnection
{
    private Logger                  mLog = Logger.getLogger("com.sun.jbi.binding.proxy.jms");
    private JMSConnectionManager    mCM;
    private Queue                   mQueue;
    private Session                 mSession;
    private MessageConsumer         mConsumer;
    private String                  mId;
    
    /** 
     * Creates a new instance of JMSServerConnection
     * @param cm that owns this server connection
     * @param session that should be used to create any JMS resources
     * @param instance id for this server connection.
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problems.
     */
    JMSServerConnection(JMSConnectionManager cm, Session session, Queue queue) 
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        mCM = cm;
        mSession = session;
        mQueue = queue;
        try
        {
            mId = mQueue.getQueueName();
            mConsumer = mSession.createConsumer(mQueue);
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.ConnectionException(
                Translator.translate(LocalStringKeys.CANT_CREATE_SERVERCONNECTION) + ":" +  jEx);
        }
    }
    
    /**
     * Accept a new client connection.
     * @return The ClientConnection.
     * @throws com.sun.jbi.binding.proxy.connection.ConnectionException for any JMS problem.
     */
    public ClientConnection accept()
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        ClientConnection    cc= null;
        
        try
        {
            BytesMessage    bytes = (BytesMessage)mConsumer.receive();
        
            if (bytes != null)
            {
                cc = mCM.getClientConnection(bytes);
            }
            return (cc);
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.ConnectionException(
                Translator.translate(LocalStringKeys.CANT_ACCEPT_SERVERCONNECTION) + ":" + jEx.toString());
        }
    }
    
    
    /**
     * Return the instance of the client.
     * @return The instanceId of the client.
     */
    public String getInstanceId()
    {
        return (mId);
    }
    
    /**
     * Return reply address.
     * @return the JMS reply queue to use.
     */
    Queue getReplyQueue()
    {
        return (mQueue);
    }
}

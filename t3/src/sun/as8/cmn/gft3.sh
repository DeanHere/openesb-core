#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)gft3.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#this script creates all t3 bundles for glassfish
#by installing the gf bundle, running unhardcode,
#and then setup-cluster.xml

forte_to_installer_port()
#this maps the forte port name to platform property we pass to the setup.xml ant script.
#it is a hack that allows us to install all the unix ports locally.
{
#    echo forte_to_installer_port arg is $1 1>&2

    if [ "$1" = "solsparc" ]; then
        echo solaris
    elif [ "$1" = "solx86" ]; then
        echo solaris
    elif [ "$1" = "nt" ]; then
        echo windows
    elif [ "$1" = "cygwin" ]; then
        echo windows
    elif [ "$1" = "linux" ]; then
        echo linux
    elif [ "$1" = "macosx" ]; then
        echo darwin
    else
        echo "NULL"
    fi
}

forte_to_download_port()
#this maps the forte port name to download directory name on java.net
{
#    echo forte_to_installer_port arg is $1 1>&2

    if [ "$1" = "solsparc" ]; then
        echo SunOS
    elif [ "$1" = "solx86" ]; then
        echo SunOS_X86
    elif [ "$1" = "nt" ]; then
        echo WINNT
    elif [ "$1" = "cygwin" ]; then
        echo WINNT
    elif [ "$1" = "linux" ]; then
        echo Linux
    elif [ "$1" = "macosx" ]; then
        echo Darwin
    else
        echo "NULL"
    fi
}

##################################### MAIN #####################################

p=`basename $0`

#we need to add arg parser, but for now we infer that you are building tools
#if you have the proper definitions:
DOMAKEDRV=0
[ "$AUX_TOOLS_PJLIST" != "" -a -d $SRCROOT/t3/src/sun/as8/cmn ] && DOMAKEDRV=1

#if DOSCP is set, we will attempt to scp the finished product to a master tools build host.
#for this to work, you need to have ssh setup and do an "ssh-add" for the shell to avoid
#a password prompt.
DOSCP=0
[ "$T3_HOST" != "" -a  "$T3_MASTER_SRCROOT" != "" ] && DOSCP=1

if [ "$FORTE_PORT" = "cygwin" ]; then
    TOOLS_FORTE_PORT=nt
else
    TOOLS_FORTE_PORT=$FORTE_PORT
fi

export GF_PLATFORMS GFT3_PORTS GF_VERSION GF_VERSIONS
[ "$GFT3_PORTS" = ""   ] && GFT3_PORTS="$TOOLS_FORTE_PORT"
    # NOTE:  nt has to be installed natively, but we do the fetch.
[ "$GF_PLATFORMS" = "" ] && GF_PLATFORMS=`forte_to_download_port $TOOLS_FORTE_PORT`
[ "$GF_VERSION" = ""   ] && GF_VERSION="v2-b25"
[ "$GF_VERSIONS" = ""  ] && GF_VERSIONS="$GF_VERSION"

[ "$GF_BUNDLE_OUT" = ""  ] && GF_BUNDLE_OUT=gfpe.tgz

bldmsg -p $p  "Will download  platforms:   $GF_PLATFORMS"
bldmsg -p $p  "Will download  versions:    $GF_VERSIONS"
bldmsg -p $p  "Will install version $GF_VERSION $GF_BUNDLE_OUT bundles for: $GFT3_PORTS"

#this forces installer to prompt:
unset DISPLAY

#show config:
cat << EOF
$p - configuration:
    GFT3_PORTS=$GFT3_PORTS
    GF_PLATFORMS=$GF_PLATFORMS
    GF_VERSION=$GF_VERSION
    GF_VERSIONS=$GF_VERSIONS
EOF

mkdir -p "$SRCROOT/$GF_VERSION"
cd "$SRCROOT/$GF_VERSION"

JVM_OPTS="-Xmx256m -Djava.awt.headless=true"
GF_OPTS="-console"
basedir=`pwd`

#download the bundles if they have not yet been created:
getgf
if [ $? -ne 0 ]; then
    bldmsg -error -p $p FAILED to download bundles - HALT.
    exit 1
fi

exit_status=0
for forte_port in $GFT3_PORTS
do
    bldmsg -markbeg create of $forte_port t3 bundle
    echo "================================== $forte_port =================================="
    echo " "
    cd $basedir
    path_port_tmp=`forte_to_download_port $forte_port`
    path_port=`echo $path_port_tmp | tr A-Z a-z`
    installer=glassfish-installer-${GF_VERSION}-${path_port}.jar

    rm -rf $forte_port
    mkdir $forte_port
    cd $forte_port
    echo Working in `pwd`

    echo jar xf ../downloads/$forte_port/$installer
    jar xf ../downloads/$forte_port/$installer
    if [ $? -ne 0 ]; then
        bldmsg -p $p -error unjar of glassfish installer FAILED
        exit_status=1
        continue
    fi

    ####
    #run the installer and accept the licence:
    ####
    echo java $JVM_OPTS glassfish $GF_OPTS
    java $JVM_OPTS glassfish $GF_OPTS << EOF
a
EOF
    if [ $? -ne 0 ]; then
        bldmsg -p $p -error glassfish installer FAILED
        exit_status=1
        continue
    fi

    cd glassfish
    gfportprop=`forte_to_installer_port $forte_port`
    echo ant -D${gfportprop}=1 -f setup-cluster.xml
    ant -D${gfportprop}=1 -f setup-cluster.xml
    if [ $? -ne 0 ]; then
        bldmsg -p $p -error ant setup-cluster.xml FAILED
        exit_status=1
        continue
    fi
    cd ..

    #this allows unhardcode scripts to install in fake t3 service:
    mkdir -p $SRCROOT/t3/src/sun/as8/$forte_port

    relbundle="t3/src/sun/as8/$forte_port/$GF_BUNDLE_OUT"
    chmod +w "$SRCROOT/$relbundle"

    if [ "$forte_port" = "nt" ]; then
        unhardcodent glassfish
    else
        unhardcode -port $forte_port glassfish
    fi

    if [ $DOSCP -eq 1 ]; then
        ssh $T3_HOST chmod +w $T3_MASTER_SRCROOT/$relbundle
        echo scp $SRCROOT/$relbundle ${T3_HOST}:$T3_MASTER_SRCROOT/$relbundle
        scp $SRCROOT/$relbundle ${T3_HOST}:$T3_MASTER_SRCROOT/$relbundle
    fi

    rm -f $HOME/.asadminpass

    #optionally run makedrv each time, so we can test while others are installing:
    if [ $DOMAKEDRV -eq 1 ]; then
        makedrv -c mmf -s t3
    fi

    bldmsg -markend create of $forte_port t3 bundle
done

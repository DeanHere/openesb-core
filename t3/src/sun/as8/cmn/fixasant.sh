#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)fixasant.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


#
#fixasant.sh - reduce length or asant classpath by hiding standard libs in $ANT_HOME/lib,
#              and creating a classpath only jar to re-include them. this script will work on any
#              platform, but we only use it on windows.
#

p=`basename $0`

if [ -z "$AS8BASE" ]; then
    echo ${p}: ERROR: \$AS8BASE must be defined 1>&2
    exit 1
fi

if [ ! -r "$AS8BASE/bin/asant.bat" ]; then
    echo ${p}: INFO: asant.bat not found, installation unmodified.  1>&2
    exit 0
fi

ASANT_HOME="$AS8BASE/lib/ant"

if [ ! -d "$ASANT_HOME" ]; then
    #this is not necessarily an error on older appserver installations.
    echo ${p}: WARNING: $ASANT_HOME is not the asant installation - abort.  1>&2
    exit 0
fi

cd "$ASANT_HOME/lib"
rm -f ant_classpath.jar
#these are implanted by make_jbiroot:
rm -f jbi-ant-tasks.jar jbi-util.jar shasta-ant-tasks.jar esb-admin-client.jar

#if we have already created the "hid" directory...
if [ ! -d hid ]; then
    mkdir hid
    mv *.jar hid
    #we need to keep these jars in lib:
    mv hid/ant.jar hid/ant-launcher.jar hid/ant-nodeps.jar .
fi

list="`walkdir -f hid`"

TMPA="_${p}_A.$$"

cat << EOF > "$TMPA"
<project name="aproject" default="main">
<target name="main" >
    <delete file="ant_classpath.jar" />
    <jar destfile="ant_classpath.jar">
        <manifest>
            <attribute name="Class-Path" value= "
$list
            " />
        </manifest>
    </jar>
</target>
</project>
EOF

ant -f $TMPA
jar_status=$?

if [ $jar_status -ne 0 ]; then
    echo ${p}: ERROR: FAILED to create ant_class.jar, script source follows.  1>&2
    cat $TMPA 1>&2
    rm -f $TMPA
    exit 1
fi

echo ${p}: Modified asant installation sucessfully.  1>&2

rm -f $TMPA
exit 0

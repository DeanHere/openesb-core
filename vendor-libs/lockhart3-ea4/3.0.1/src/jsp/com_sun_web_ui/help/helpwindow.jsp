<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@taglib uri="http://www.sun.com/web/ui" prefix="ui" %>

<%
  // set certain browser dependent frame style attributes
  String outerFramesetRows = "100";
  String middleFramesetSpacing = "";
  String frameBorderTrue = "yes";
  String frameBorderFalse = "no";
  String middleFramesetBorderColor = "";
  String navFrameBorder = "yes";
  String navFrameScrolling = "auto";
  String innerFramesetSpacing = "";
  String innerFramesetBorderColor = "";
  String buttonFrameBorder = "yes";
  
  if (request.getHeader("USER-AGENT").indexOf("MSIE") >= 0) {
      // set the style attrs for IE
      outerFramesetRows = "104";
      middleFramesetSpacing = "\n framespacing=\"2\"";
      innerFramesetSpacing = "\n framespacing=\"1\"";
      frameBorderTrue = "1";
      frameBorderFalse = "0";
      middleFramesetBorderColor = "\n bordercolor=\"#CCCCCC\"";
      navFrameBorder = "0";
      navFrameScrolling = "yes";
      innerFramesetSpacing = "\nf ramespacing=\"1\"";
      innerFramesetBorderColor = "\n bordercolor=\"#939CA3\"";
      buttonFrameBorder = "0";
  }
  
  // get the query params from the helpwindow link
  String windowTitle = request.getParameter("windowTitle") != null ?
      request.getParameter("windowTitle") : "";
  String helpSetPath = request.getParameter("helpSetPath") != null ?
      request.getParameter("helpSetPath") : "";
  String helpFile = request.getParameter("helpFile") != null ?
      request.getParameter("helpFile") : "";
  String pageTitle = request.getParameter("pageTitle") != null ?
      request.getParameter("pageTitle") : "";
  String mastheadUrl = request.getParameter("mastheadUrl") != null ?
      request.getParameter("mastheadUrl") : "";
  String mastheadHeight = request.getParameter("mastheadHeight") != null ?
      request.getParameter("mastheadHeight") : "";
  String mastheadWidth = request.getParameter("mastheadWidth") != null ?
      request.getParameter("mastheadWidth") : "";
  String mastheadDescription = request.getParameter("mastheadDescription") != null ?
      request.getParameter("mastheadDescription") : "";  
  String closeButton = request.getParameter("closeButton") != null ?
      request.getParameter("closeButton") : "true";  
  String jspPath = request.getParameter("jspPath") != null ?
      request.getParameter("jspPath") : ""; 
  String firstLoad = request.getParameter("firstLoad") != null ? 
      request.getParameter("firstLoad") : ""; 
%>

<f:view>
  <HTML>
    <HEAD><TITLE><%=windowTitle%></TITLE></HEAD>
    
<!-- Frameset for Masthead frame -->
<frameset rows="<%=outerFramesetRows%>,*"
 frameborder="<%=frameBorderFalse%>"
 border="0"
 framespacing="0">

<!-- Masthead frame -->
<frame src="<%=jspPath%>masthead.jsp?mastheadUrl=<%=mastheadUrl%>&mastheadHeight=<%=mastheadHeight%>&mastheadWidth=<%=mastheadWidth%>&mastheadDescription=<%=mastheadDescription%>&pageTitle=<%=pageTitle%>&closeButton=<%=closeButton%>"
 name="mastheadFrame"
 scrolling="no"
 id="mastheadFrame"
 title="Frame Containing Masthead and Page Title" />

<!-- Frameset for Nav, ButtonNav, and Content frames -->
<frameset cols="33%,67%"
 frameborder="<%=frameBorderTrue%>"
 border="2" <%=middleFramesetSpacing%> <%=middleFramesetBorderColor%>>

<!-- Nav Frame -->
<frame src="<%=jspPath%>navigator.jsp?tipsUrl=<%=jspPath%>/tips.jsp&helpSetPath=<%=helpSetPath%>"
 name="navFrame"
 frameBorder="<%=navFrameBorder%>"
 scrolling="<%=navFrameScrolling%>"
 id="navFrame"
 title="Frame Containing Table of Contents, Index, and Search" />

<!-- Frameset for ButtonNav and Content Frames -->
<frameset rows="31,*"
 frameborder="<%=frameBorderTrue%>"
 border="1" <%=innerFramesetSpacing%> <%=innerFramesetBorderColor%>>

<!-- ButtonNav Frame -->
<frame src="<%=jspPath%>buttonnav.jsp?helpSetPath=<%=helpSetPath%>"
 name="buttonNavFrame"
 frameBorder="<%=buttonFrameBorder%>"
 scrolling="no"
 id="buttonNavFrame"
 title="Frame Containing Navigation Buttons" />

<!-- Content Frame -->
<frame src="<h:outputText value="#{JavaHelpBean.localizedHelpPath}" /><%=helpFile %>"
 name="contentFrame"
 frameBorder="<%=frameBorderFalse%>"
 scrolling="auto"
 id="contentFrame"
 title="Frame Containing Online Help Text" />

</frameset>
</frameset>
</frameset>

<noframes>
<body>
<cc:text name="NoFramesText" bundleID="help2Bundle"
 defaultValue="help.noframes" />
</body>
</noframes>

  </HTML>
</f:view>

<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)xref.ant
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->

<!--
#   create consolidated javadoc and xref'd source
-->

<project name="jbi" default="doc">

<!--================================= init ==================================-->

<target name="init" unless="init_complete"
 depends="check-anthome,set-global,set-release-env"
 description="- main initialization target"
>
    <property name="antext" value="${ant.home}/lib" />
    <property name="vlib" value="${srcroot}/vendor-libs" />
    <property name="doc_archive" value="${srcroot}/xref_doc_stable" />
    <property name="jardest" value="${MAVEN_REPOSITORY}/open-esb" />

    <!--
    # use a local server for J2SE api defs
    # (instead of http://java.sun.com/j2se/1.5/docs/api):
    -->
    <property name="javadoc_server" value="http://iis.sfbay.sun.com"/>

    <property name="ant.url" value="${javadoc_server}/ant161/manual/api"/>
    <property name="j2ee.url" value="${javadoc_server}/j2ee14/api"/>
    <property name="j2se.url" value="${javadoc_server}/j2se15/api"/>
    <property name="jmx.url" value="${javadoc_server}/jmx121/api"/>
    <property name="jmxremote.url" value="${javadoc_server}/jmxremote10/api/spec-jmxremote"/>

    <!--
    # local cache of the javadoc "package-list" files that we use.
    # this is so you can build offline.  WARNING: this is the directory
    # where "package-lists" lives, not the full file name.
    -->
    <property name="ant.package.list.dir" value="./ext/ant161/manual/api"/>
    <property name="j2ee.package.list.dir" value="./ext/j2ee14/api"/>
    <property name="j2se.package.list.dir" value="./ext/j2se15/api"/>
    <property name="jmx.package.list.dir" value="./ext/jmx1.2.1/api"/>
    <property name="jmxremote.package.list.dir" value="./ext/jmxremote1.0/api/spec-jmxremote"/>

    <!-- see "reorg_java_source" target for an example of how to use this: -->

    <property name="common_sources_expr"
        value="jbi/jbi1/src/api|jbi/jbi1/src/spi|runtime/base|runtime/framework|runtime/manage|runtime/nmr|runtime/ui|runtime/wsdl"
    />

    <property name="shasta_only_sources_expr"
        value="ri-clients/jbi-admin-common|ri-clients/jbi-ant-tasks|ri-clients/jbi-admin-cli|ri-clients/jbi-admin-gui|runtime/esb-security|runtime/esb-manage|ri-components/transformation-engine/runtime|ri-components/transformation-engine/boot|ri-components/sequencing-engine/runtime|ri-components/sequencing-engine/boot|ri-components/soap-binding/runtime|ri-components/soap-binding/boot|ri-components/soap-binding/servlet|ri-components/soap-binding/bridge|esb-components/proxy-binding/runtime|esb-components/proxy-binding/boot|esb-components/jms-binding/runtime|esb-components/jms-binding/boot|ri-components/file-binding/runtime|ri-components/file-binding/boot|ri-components/common"
    />
    <!-- note that runtime/security is (intentionally?) left out, as is esb-util -->
    <!-- note that esb-clients has been removed -->

    <property name="lassen_only_sources_expr"
        value="lassen"
    />

    <!-- this is a list of all directories that have shasta java package source: -->
    <property name="shasta_sources_expr"
        value="${common_sources_expr}|${shasta_only_sources_expr}"
    />

    <!-- this is a list of all directories that have lassen java package source: -->
    <property name="lassen_sources_expr"
        value="${common_sources_expr}|${lassen_only_sources_expr}"
    />

    <!--
        value="base/api|base/ipi|base/spi|framework|manage|nms|registry|repos|wsdl"
    -->

    <!-- copy the java path separator property into PS: -->
    <property name="PS" value="${path.separator}" />

    <mkdir dir="./bld" />

    <!-- INITIALIZATION COMPLETE -->
    <property name="init_complete" value="true" />
</target>

<target name="set-global" description="- set global date format in internal DSTAMP variable">
    <property environment="env"/>
    <tstamp>
        <format property="YYMMDD_HHMM" pattern="yyMMdd_HHmm" />
    </tstamp> 
</target>

<target name="set-release-env"
        depends="set-srcroot,set-toolroot,set-pathref,set-product,set-branch,set-bldnum,set-forte_port,set-host_name,set-have_xref,set-logdir,set-jre-jar"
        description="- setup release variables"
    >
    <!-- these are no-ops if they are set in env. some may be bad guesses  -->
    <property name="srcroot" value="${basedir}/.." />
    <property name="toolroot" value="${srcroot}/tools" />
    <property name="pathref" value="${srcroot}" />
    <property name="PRODUCT" value="jbi" />
    <property name="BRANCH" value="trunk" />
    <property name="BLDNUM" value="${YYMMDD_HHMM}" />
    <property name="FORTE_PORT" value="${os.name}-${os.arch}" />
    <property name="HOST_NAME" value="unknown" />
</target>

<target name="set-pathref" if="env.PATHREF" description="- set ant internal pathref from env PATHREF">
    <property name="pathref" value="${env.PATHREF}" />
</target>

<target name="set-have_xref">
    <property name="have_xref" value="true" />
<!--
    <condition property="have_xref" value="true">
        <or>
            <equals arg1="${os.arch}" arg2="sparc"/>
            <equals arg1="${os.name}" arg2="Mac OS X"/>
        </or>
    </condition>
-->
</target>

<target name="set-jre-jar" depends="set-osx-jre" >
    <!-- default on most systems is here:  -->
    <!-- note that the java.home includes "jre". -->
    <property name="jre.jar" value="${java.home}/lib/rt.jar" />
</target>

<target name="set-osx-jre" >
    <condition property="jre.jar" value="${java.home}/../Classes/classes.jar" >
        <equals arg1="${os.name}" arg2="Mac OS X"/>
    </condition>
</target>

<target name="set-logdir" if="env.LOGDIR" >
    <property name="LOGDIR" value="${env.LOGDIR}" />
</target>

<target name="set-srcroot" if="env.JV_SRCROOT" description="- set ant internal srcroot from env SRCROOT">
    <property name="srcroot" value="${env.JV_SRCROOT}" />
</target>

<target name="set-toolroot" if="env.JV_TOOLROOT" description="- set ant internal toolroot from env TOOLROOT">
    <property name="toolroot" value="${env.JV_TOOLROOT}" />
</target>

<target name="set-product" if="env.PRODUCT" description="- set ant internal PRODUCT from env PRODUCT">
    <property name="PRODUCT" value="${env.PRODUCT}" />
</target>

<target name="set-branch" if="env.CVS_BRANCH_NAME" description="- set ant internal BRANCH from env CVS_BRANCH_NAME">
    <property name="BRANCH" value="${env.CVS_BRANCH_NAME}" />
</target>

<target name="set-bldnum" if="env.BUILD_DATE" description="- set ant internal BLDNUM from env BUILD_DATE">
    <property name="BLDNUM" value="${env.BUILD_DATE}" />
</target>

<target name="set-forte_port" if="env.FORTE_PORT" description="- set ant internal FORTE_PORT from env FORTE_PORT">
    <property name="FORTE_PORT" value="${env.FORTE_PORT}" />
</target>

<target name="set-host_name" if="env.HOST_NAME" description="- set ant internal HOST_NAME from env HOST_NAME">
    <property name="HOST_NAME" value="${env.HOST_NAME}" />
</target>

<!--======================= check-anthome definitions =======================-->

<target name="check-anthome" unless="ant.home" description="- check ant.home defined, set if env.ANT_HOME defined otherwise">
    <antcall target="check-anthome-env"/>
    <param name="ant.home" value="${env.ANT_HOME}" />
</target>

<target name="check-anthome-env" unless="env.ANT_HOME" description="- check env.ANT_HOME defined, else fail" >
<fail message="">
    ${ant.home} is undefined.  This means you are not using a standard
    ant wrapper script.  Please use the wrapper script or set the
    environment variable $$ANT_HOME to the place where ant can find its
    primary jar files and any ant extension jar files you are using.

    NOTE:  in this project, $$ANT_HOME should be set to:
        $$TOOLROOT/java/ant
</fail>
</target>


<!--============================== archive_doc ==============================-->

<target name="archive_doc" depends="init" description="- archive javadoc and xref build">

    <antcall target="bldmsg">
        <param name="args" value="-markbeg archive of javadoc"/>
    </antcall>

    <!-- clean up from previous build: -->
    <delete dir="${doc_archive}.old"/>
    <delete dir="${doc_archive}.new"/>
    <mkdir dir="${doc_archive}"/>

    <!--
    # note: the ant move task moves each file individually, and so is not
    # much faster than a copy.  I.e, there is no way to do a "mv" in the unix sense.
    # hence the dependency on the external mv command.
    # RT 10/25/03
    -->

    <copy todir="${doc_archive}.new" preservelastmodified="true" >
        <fileset dir="./bld/javadoc"/>
    </copy>

    <echo message="mv ${doc_archive} ${doc_archive}.old"/>
    <exec executable="mv">
        <arg line="${doc_archive} ${doc_archive}.old"/>
     </exec>

    <echo message="mv ${doc_archive}.new ${doc_archive}"/>
    <exec executable="mv">
        <arg line="${doc_archive}.new ${doc_archive}"/>
     </exec>

    <!--
    # now we can remove the old directory:
    -->
    <delete dir="${doc_archive}.old"/>

    <antcall target="bldmsg">
        <param name="args" value="-markend archive of javadoc"/>
    </antcall>
</target>

<!--================================== doc ==================================-->

<target name="doc" depends="init,reorg_java_source,shasta_rt_javadoc,tests_javadoc" description="- master doc build target">
    <echo message="Combined Javadoc Complete" />
</target>

<target name="shasta_rt_javadoc" depends="init" description="- generate run time java docs">
    <echo message="Generating Runtime API javadoc..." />

    <antcall target="do_javadoc" inheritAll="true">
        <param name="whatbuild" value="shasta"/>
        <param name="whatdoc" value="rt"/>
        <param name="whatsrc" value="rt.javadoc.path"/>
        <param name="whatclasses" value="rt.javadoc.classpath"/>
        <param name="browserTitle" value="Runtime Javadoc ${PRODUCT}/shasta"/>
        <param name="titleStr" value="Runtime API for ${PRODUCT}{${BRANCH}}/shasta ${BLDNUM}"/>
    </antcall>
</target>

<target name="tests_javadoc" description="- generate test java docs">
    <echo message="Generating Runtime + Regress javadoc..." />

    <!-- THIS IS A WORKAROUND  - javadoc is not seeing junit classes. RT 5/2/02 -->
    <delete dir="./bld/classes" />
    <unjar src="${antext}/junit.jar" dest="./bld/classes" />

    <antcall target="do_javadoc" inheritAll="true">
        <param name="whatdoc" value="tests"/>
        <param name="whatbuild" value="shasta"/>
        <param name="whatsrc" value="tests.javadoc.path"/>
        <param name="whatclasses" value="tests.javadoc.classpath"/>
        <param name="browserTitle" value="Full Javadoc for ${PRODUCT}/shasta"/>
        <param name="titleStr" value="Regress + runtime API for ${PRODUCT}{${BRANCH}}/shasta ${BLDNUM}"/>
    </antcall>
</target>

<!-- internal target to generate javadoc -->
<target name="do_javadoc" description="- internal java doc target / used by doc targets">

    <!--
    # these paths must be defined local to this target, as path-references
    # are apparently not inherited.
     -->
    <path id="rt.javadoc.path">
        <pathelement location="./bld/all_java/${whatbuild}/rt/"/>
    </path>
    <path id="tests.javadoc.path">
        <pathelement location="./bld/all_java/${whatbuild}/tests/"/>
    </path>
    <path id="examples.javadoc.path">
        <pathelement location="./bld/all_java/${whatbuild}/examples/"/>
    </path>

    <path id="rt.javadoc.classpath">
        <fileset dir="${vlib}">
            <include name="**/*.jar"/>
        </fileset>
        <fileset dir="${toolroot}/java/ant/lib">
            <include name="ojdbc14.jar"/>
        </fileset>
    </path>

    <path id="tests.javadoc.classpath">
        <path refid="rt.javadoc.classpath" />
        <!-- THIS IS A WORKAROUND  - javadoc is not seeing junit classes. RT 5/2/02 -->
        <pathelement location="./bld/classes/" />
    </path>

    <path id="examples.javadoc.classpath">
    </path>

    <property name="javadoc.classpath" refid="${whatclasses}" />
<!--
    <echo message="javadoc.classpath is ${javadoc.classpath}"/>
-->
    <echo message="whatdoc is ${whatdoc}"/>
    <echo message="whatsrc is ${whatsrc}"/>

    <delete dir="./bld/javadoc/${whatdoc}" />

    <!--
    # run the javadoc command.
    #
    # NOTES:
    #        the -linksource command creates the html-src directory,
    #            which is decorated with cross-references by the xref task.
    #        the link elements will generate href's to external
    #            javadoc classes for references that appear
    #            in method signatures, etc. (for example, if a method has a
    #            String argument, the String class will get an href).
    #            For this to work, we supply a "package-list" file containing
    #            the standard J2SE packages.
    -->
    <javadoc packagenames="com.*,javax.*"
        additionalparam="-J-Xmx200m -source 1.5 -linksource"
        verbose="no"
        sourcepathref="${whatsrc}"
        classpathref="${whatclasses}"
        destdir="./bld/javadoc/${whatbuild}/${whatdoc}"
        failonerror="false"
        author="true"
        public="true"
        private="false"
        version="true"
        use="true"
        Windowtitle="${browserTitle}"
        doctitle="&lt;h1&gt;${titleStr}&lt;/h1&gt;"
        bottom="&lt;i&gt;Copyright &#169; 2002 SUN Microsystems, Inc. All Rights Reserved.&lt;/i&gt;"
    >

        <link href="${j2se.url}" offline="true" packagelistLoc="${j2se.package.list.dir}" />
        <link href="${j2se.url}" offline="true" packagelistLoc="${j2se.package.list.dir}" />
        <link href="${ant.url}" offline="true" packagelistLoc="${ant.package.list.dir}" />
        <link href="${j2ee.url}" offline="true" packagelistLoc="${j2ee.package.list.dir}" />
        <link href="${jmx.url}" offline="true" packagelistLoc="${jmx.package.list.dir}" />
        <link href="${jmxremote.url}" offline="true" packagelistLoc="${jmxremote.package.list.dir}" />

    </javadoc>
    
</target>

<target name="reorg_java_source" depends="init" description="- internal target / used to generate java source tree">
    <delete dir="./bld/all_java" />

    <!-- collect test+runtime java sources -->
    <copy todir="./bld/all_java/shasta/tests" preservelastmodified="true" >
        <mapper type="regexp"
            from="^(${shasta_sources_expr})[/\\](src|regress|tst)[/\\](.*)$$"
            to="\3"
        />
        <fileset dir="..">
            <include name="**/src/com/**/*.java"/>
            <include name="**/src/javax/**/*.java"/>
            <include name="**/regress/com/**/*.java"/>
            <include name="**/tst/com/**/*.java"/>
        </fileset>
    </copy>

    <!-- collect runtime java sources -->

    <copy todir="./bld/all_java/shasta/rt" preservelastmodified="true" >
        <mapper type="regexp"
            from="^(${shasta_sources_expr})[/\\](src|regress|tst)[/\\](.*)$$"
            to="\3"
        />
        <fileset dir="..">
            <include name="**/src/com/**/*.java"/>
            <include name="**/src/javax/**/*.java"/>
        </fileset>
    </copy>

</target>

<!--================================= xref ==================================-->

<target name="xref" if="have_xref" depends="init,doc,xref_only"
    description="- generate cross-referenced source listings">

    <echo message="Finished building cross-referenced sources."/>
</target>

<target name="shasta_rt_xref" depends="init" description="- generate runtime cross-referenced source tree">
    <antcall target="do_xref" inheritAll="true">
        <param name="whatbuild" value="shasta"/>
        <param name="whatsrc" value="rt"/>
        <param name="whatclasses" value="rt.xref.classpath"/>
    </antcall>
</target>

<target name="shasta_tests_xref" depends="init" description="- generate tests+runtime cross-referenced source tree">
    <antcall target="do_xref" inheritAll="true">
        <param name="whatbuild" value="shasta"/>
        <param name="whatsrc" value="tests"/>
        <param name="whatclasses" value="tests.xref.classpath"/>
    </antcall>
</target>

<target name="xref_only" depends="init" if="have_xref"
    description="- internal target to generate xref'd sources">

    <antcall target="bldmsg">
        <param name="args" value="-mark Start xref."/>
    </antcall>

    <antcall target="shasta_rt_xref" inheritAll="true"/>
    <antcall target="shasta_tests_xref" inheritAll="true"/>

    <antcall target="bldmsg">
        <param name="args" value="-mark Finished xref."/>
    </antcall>
</target>

<!-- internal target to generate xref'd sources -->
<target name="do_xref" if="have_xref" description="- generate cross-referenced source listings">
    <property name="whichdir" value="${srcroot}/antbld/bld/all_java/${whatbuild}/${whatsrc}" />
    <property name="whichhtml" value="${srcroot}/antbld/bld/javadoc/${whatbuild}/${whatsrc}/src-html" />

    <!--
    # these paths must be defined local to this target, as path-references
    # are apparently not inherited.
    # NOTE - cactus lib is not used anymore.  RT 10/15/02
     -->
    <path id="rt.xref.classpath">
        <fileset dir="${jardest}">
            <include name="**/*.jar"/>
        </fileset>
        <fileset dir="${vlib}">
            <include name="**/*.jar"/>
        </fileset>
        <fileset dir="${antext}">
            <include name="junit.jar"/>
        </fileset>
    </path>

    <path id="tests.xref.classpath">
        <path refid="rt.xref.classpath" />
    </path>

    <property name="xref.classpath" refid="${whatclasses}" />

<!--
#the xref command dumps the classpath, so we don't have to:
<echo message="xref.classpath is ${xref.classpath}"/>
<fail message="TESTING xref.classpath"/>
-->

    <antcall target="bldmsg">
        <param name="args" value="-markbeg add cross-refs to ${whatsrc}/src-html ..."/>
    </antcall>

    <exec executable="xref" dir="${whichdir}" >
        <arg line="-no_stdop"/>
        <arg line="-errors"/>
        <arg line="-mf8"/>

        <arg line="-html"/>
        <arg line="-refalphahash"/>
        <arg line="-nocxfile"/>

        <arg line="-java1.4"/>
        <arg line="-java2html"/>
        <arg line="-javafilesonly"/>

        <arg line="-classpath ${xref.classpath}"/>
        <arg line="-sourcepath ."/>

        <arg line="-javadocurl=${j2se.url}"/>
        <arg line="-htmlgenjavadoclinks"/>

        <arg line="-jdkclasspath ${jre.jar}" />

        <arg line="-htmlgxlist"/>
        <arg line="-htmllxlist"/>
        <arg line="-htmldirectx"/>

        <arg line="-htmlnounderline"/>
        <arg line="-htmllinenums"/>
        <arg line="-htmllinenumlabel=line."/>

        <arg line="-htmlcutpath=${whichdir}"/>
        <arg line="-htmlcutsuffix"/>
        <arg line="-htmlcutsourcepaths"/>
        <arg line="-htmlcutcwd"/>

        <!-- this has to be an absolute path: -->
        <arg line="-d ${whichhtml}"/>
        <arg line="com javax"/>

    </exec>

    <antcall target="bldmsg">
        <param name="args" value="-markend add cross-refs to ${whatsrc}/src-html ..."/>
    </antcall>
</target>

<target name="do_xref2" if="have_xref" description="- generate cross-referenced source listings">
    <property name="whichdir" value="${srcroot}/antbld/bld/all_java/${whatsrc}" />
    <property name="whichrefs" value="${whichdir}/Xrefs" />
    <property name="whichhtml" value="${srcroot}/antbld/bld/javadoc/${whatsrc}/src-html" />

    <echo message="MARK: DELETING ${whichrefs}"/>
    <delete dir="${whichrefs}" />

    <!-- STEP ONE - generate the xref's: -->
    <antcall target="bldmsg">
        <param name="args" value="-markbeg Generating xref data in ${whichrefs} ..."/>
    </antcall>

    <exec executable="xref" dir="${whichdir}" >
        <arg line="-errors"/>
        <arg line="-nobrief"/>
        <arg line="-java1.4"/>
        <arg line="-no_stdop"/>

        <arg line="-refs ${whichrefs}"/>
        <arg line="-refalphahash"/>
        <arg line="-javafilesonly"/>

<!--
        <arg line="-no_local"/>
-->

        <!-- note that the java.home includes "jre". -->
        <arg line="-jdkclasspath ${java.home}/lib/rt.jar"/>
        <arg line="-classpath ${whatclasses}"/>
        <arg line="-sourcepath ."/>

        <arg line="com"/>
    </exec>

    <!-- SAVE the Xref tree: -->
    <antcall target="bldmsg">
        <param name="args" value="-mark Copy ${whichrefs} to ${whichrefs}.save"/>
    </antcall>
    <delete dir="${whichrefs}.save" />
    <delete file="${whichrefs}.save" />
    <copy todir="${whichrefs}.save" preservelastmodified="true" >
        <fileset dir="${whichrefs}"/>
    </copy>

    <antcall target="bldmsg">
        <param name="args" value="-markend Generating xref data in ${whichrefs} ..."/>
    </antcall>

    <!-- STEP TWO - decorate the html-src tree: -->
    <antcall target="bldmsg">
        <param name="args" value="-markbeg add cross-refs to ${whichhtml} ..."/>
    </antcall>

    <exec executable="xref" dir="${whichdir}" >
        <arg line="-no_stdop"/>
        <arg line="-nobrief"/>
        <arg line="-errors"/>
        <arg line="-fastupdate"/>

        <arg line="-html"/>
        <arg line="-refs ${whichrefs}"/>
        <arg line="-refalphahash"/>

        <arg line="-java1.4"/>
        <arg line="-java2html"/>
        <arg line="-javafilesonly"/>

        <!-- note that the java.home includes "jre". -->
        <arg line="-jdkclasspath ${java.home}/lib/rt.jar"/>

        <arg line="-classpath ${whatclasses}"/>
        <arg line="-sourcepath ."/>

        <arg line="-javadocurl=http://java.sun.com/j2se/1.5/docs/api"/>
        <arg line="-htmlgenjavadoclinks"/>

        <arg line="-htmlgxlist"/>
        <arg line="-htmllxlist"/>
        <arg line="-htmldirectx"/>

        <arg line="-htmlnounderline"/>
        <arg line="-htmllinenums"/>
        <arg line="-htmllinenumlabel=line."/>

        <arg line="-htmlcutpath=${whichdir}"/>
        <arg line="-htmlcutsuffix"/>
        <arg line="-htmlcutsourcepaths"/>
        <arg line="-htmlcutcwd"/>

        <!-- this has to be an absolute path: -->
        <arg line="-d ${whichhtml}"/>
        <arg line="com"/>

    </exec>

    <antcall target="bldmsg">
        <param name="args" value="-markend add cross-refs to ${whichhtml} ..."/>
    </antcall>
</target>

<!--================================= bldmsg ================================-->

<target name="bldmsg">
    <exec executable="sh">
        <arg line="-c 'bldmsg ${args}'"/>
    </exec>
</target>

<!--================================= info ==================================-->

<target name="info" depends="init" description="- display informational message">
    <echo> <![CDATA[
Master build properties:
    srcroot:         ${srcroot}
    pathref          ${pathref}
    ant.home:        ${ant.home}
    antext:          ${antext}
    vlib:            ${vlib}
    have_xref:       ${have_xref}

    j2se.url               ${j2se.url}
    j2se.package.list.dir  ${j2se.package.list.dir}

    PRODUCT:         ${PRODUCT}
    BRANCH:          ${BRANCH}
    DSTAMP:          ${DSTAMP}
    TSTAMP:          ${TSTAMP}
    BLDNUM:          ${BLDNUM}

    java.home        ${java.home}
    jre.jar:         ${jre.jar}
    user.name        ${user.name}
    os.name          ${os.name}
    os.arch          ${os.arch}
    os.version       ${os.version}
    java.vm.version  ${java.vm.version}
    path.separator   ${path.separator}
    classpath        ${java.class.path}
     ]]> </echo>
</target>

<!--================================== help =================================-->

<target name="help" description="- list main command line callable ant targets">
    <echo> <![CDATA[
Info about master build targets

     help            show this help message
     info            show master build properties

     doc             create combined javadoc for project
     archive_doc     archive the javadoc and xref tree in:
                         ${doc_archive}

     xref            build javadoc and annotate it with
                     html cross-referenced source code listings.
     xref_only       annotate existing javadoc tree.

     ]]> </echo>
</target>

</project>

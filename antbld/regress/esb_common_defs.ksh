#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)esb_common_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
# Common definitions for esb admin/ui regression tests.

# The domains CAS and ESBMember need to be started prior to this
# Export the CAS Environment

my_test_domain=CAS
. $SRCROOT/antbld/regress/common_defs.ksh

export CAS_ADMIN_PORT
CAS_ADMIN_PORT="$JBI_ADMIN_PORT"

export CAS_ADMIN_HOST
CAS_ADMIN_HOST="$JBI_ADMIN_HOST"

export CAS_ADMIN_USER
CAS_ADMIN_USER="$AS_ADMIN_USER"

export CAS_ADMIN_PASSWORD
CAS_ADMIN_PASSWORD="$AS_ADMIN_PASSWD"

export CAS_DOMAIN_NAME
CAS_DOMAIN_NAME="$JBI_DOMAIN_NAME"

export CAS_DOMAIN_ROOT JV_CAS_DOMAIN_ROOT
CAS_DOMAIN_ROOT="$JBI_DOMAIN_ROOT"
JV_CAS_DOMAIN_ROOT="$JV_JBI_DOMAIN_ROOT"

export CAS_DOMAIN_PROPS
CAS_DOMAIN_PROPS="$JV_JBI_DOMAIN_PROPS"

export CAS_JMX_REMOTE_URL
CAS_JMX_REMOTE_URL="AS_JMX_REMOTE_URL"

## kcbabo: separate domain no longer necessary for ESB tests
# Export the ESBMember environment
#my_test_domain=ESBMember
#. $SRCROOT/antbld/regress/common_defs.ksh

#export ESBMEMBER_ADMIN_PORT
#ESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT"

#export ESBMEMBER_ADMIN_HOST
#ESBMEMBER_ADMIN_HOST="$JBI_ADMIN_HOST"

#export ESBMEMBER_ADMIN_USER
#ESBMEMBER_ADMIN_USER="$AS_ADMIN_USER"

#export ESBMEMBER_ADMIN_PASSWORD
#ESBMEMBER_ADMIN_PASSWORD="$AS_ADMIN_PASSWD"

#export ESBMEMBER_DOMAIN_ROOT JV_ESBMEMBER_DOMAIN_ROOT
#ESBMEMBER_DOMAIN_ROOT="$JBI_DOMAIN_ROOT"
#JV_ESBMEMBER_DOMAIN_ROOT="$JV_JBI_DOMAIN_ROOT"

#export ESBMEMBER_DOMAIN_PROPS
#ESBMEMBER_DOMAIN_PROPS="$JV_JBI_DOMAIN_PROPS"

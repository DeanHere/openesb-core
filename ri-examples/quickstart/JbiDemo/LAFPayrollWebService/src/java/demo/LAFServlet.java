/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LAFServlet.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author chikkala
 * @version
 */
public class LAFServlet extends HttpServlet
{
    private static final String OUT_DIR = "payroll";
    private static final String OUT_FILE_PREFIX = "LAFPayroll_";
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        PrintWriter out = null;
        PrintWriter outFileWriter = null;
        BufferedReader reader = null;
        
        try
        {
            response.setContentType("text/xml;charset=UTF-8");
            out = response.getWriter();
            
            reader = request.getReader();
            // create new file to save the message in the request
            String outFilePath = getNewOutputFilePath();
//            System.out.println("LAFServlet:outFile:" + outFilePath );
            outFileWriter = new PrintWriter(outFilePath);
            
            for ( String line = null;  (line = reader.readLine()) != null ; )
            {
                // write back the data as response
                out.println(line);
                // write to file
                outFileWriter.println(line);
                System.out.println("LAFServerlet:" + line);
            }
        }
        finally
        {
            
            if ( out != null )
            {
                out.close();
            }
            if ( outFileWriter != null )
            {
                outFileWriter.close();
            }
            try
            {
                if ( reader != null )
                {
                    reader.close();
                }
            }
            catch (Exception ex)
            {
                // ignore
            }
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo()
    {
        return "LAF Servlet";
    }
    // </editor-fold>
    
    private String getDate()
    {
        GregorianCalendar gregoriancalendar = new GregorianCalendar();
        Date date = gregoriancalendar.getTime();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyMMddHHmmssSSS");
        String s = simpledateformat.format(date);
        return s;
    }
    
    private String getNewOutputFilePath()
    {
        String contextPath = getServletContext().getRealPath("/");
        if ( contextPath == null )
        {
            contextPath = System.getProperty("java.io.tmpdir", "/tmp");
        }
        String outputDir = contextPath + File.separator + OUT_DIR;
        String outputFilePath = outputDir + File.separator + OUT_FILE_PREFIX + getDate() + ".xml";
        
        File outputDirFile = new File(outputDir);
        outputDirFile.mkdirs();
        return outputFilePath;
    }
    
}

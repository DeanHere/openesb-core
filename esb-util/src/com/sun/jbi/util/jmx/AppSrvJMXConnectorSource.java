/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AppSrvJMXConnectorSource.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  AppSrvJMXConnectorSource.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on June 13, 2005, 12:02 PM
 */

package com.sun.jbi.util.jmx;

import java.io.File;
import java.util.Map;

/**
 * Note that this is a Sun One Application Server specific class. For connecting to a 
 * Standard JMX Connector Server use the JMXCOnnectorSourceImpl class.
 */
import com.sun.appserv.management.client.AppserverConnectionSource;
import com.sun.appserv.management.client.TrustStoreTrustManager;
import com.sun.appserv.management.client.TLSParams;
import com.sun.appserv.management.client.HandshakeCompletedListenerImpl;

import com.sun.jbi.util.StringTranslator;

import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.X509TrustManager;
/**
 * This class is to be used by jbi components / services to get a JMX Client Connector.
 * This is a wrapper around the JMXConnectorFactory and allows one to connect to 
 * a Connector Server, which may have proprietary extensions for Security. The 
 * corresponding client Connector Source would be made available through this interface.
 *
 * @author Sun Microsystems, Inc.
 */
public class AppSrvJMXConnectorSource
    implements JMXConnectorSource
{
    
    /** Username. */
    private String mUser;
    
    /** Password. */
    private String mPwd;
    
    /** JMXServiceURL. */
    private JMXServiceURL mJmxServiceUrl;
    
    /** JMXCOnnector. */
    private JMXConnector mJmxConnector;
    
    /** SSL Flag. */
    private boolean mIsSecure;
    
    /** TLS Params. */
    private TLSParams mTlsParams;
    
    /** Host. */
    private String mHost;
    
    /** Port. */
    private int mPort;
    
    /** Null Obj. */
    private static final String NULL_OBJECT = "NULL_OBJECT";
    
    /** Default Store Type. */
    private static final String DEFAULT_STORE_TYPE = "JKS";
    
    
    /** 
     * Ctor. 
     */
    public AppSrvJMXConnectorSource()
    {
        mJmxConnector = null;
        mJmxServiceUrl = null;
        mPwd = null;
        mUser = null;
        mIsSecure = false;
        mHost = null;
        mPort = 0;
    }
    
    
    /**
     * @param username is the username to be used for the Connection.
     * @param password is the user password.
     */
    public void setCredentials(String username, String password)
    {
        mUser = username;
        mPwd = password;
    }
    
    /**
     * This operation is not supported by this implementation of the Connector source and
     * is a no-op.
     *
     * @param secureFlag indicates whether the connection is to be secured (ex. use SSL)
     */
    public void setIsSecure(boolean secureFlag)
    {
        mIsSecure = secureFlag;
    }
    
    /**
     * This operation is not supported by this implementation of the Connector source
     * and is a no-op.
     *
     * @param truststore path to the JKS truststore file.
     * @param type is the type of the Keystore ( JKS, JCEKS etc)
     * @param passwd - the truststore password
     */
    public void setTrustStore(File truststore, String type, char[] passwd)
    {
        if ( type == null )
        {
            type = DEFAULT_STORE_TYPE;
        }
        TrustStoreTrustManager trustMgr = new TrustStoreTrustManager( 
            truststore, type, passwd);
        trustMgr.setPrompt( true );

        HandshakeCompletedListener listener = new HandshakeCompletedListenerImpl();
        mTlsParams = new TLSParams( 
            new X509TrustManager[] {trustMgr}, listener );
    }
    
    /**
     * @param jmxServiceURL - JMX Service URL
     */
    public void setJMXServiceURL(JMXServiceURL jmxServiceURL)
    {
        mJmxServiceUrl = jmxServiceURL;
        
        if ( mJmxServiceUrl != null )
        {
            if ( mJmxServiceUrl.getHost() != null && mJmxServiceUrl.getPort() != 0 )
            {
                mHost = mJmxServiceUrl.getHost();
                mPort = mJmxServiceUrl.getPort();
            }
        }
    }
    
    /**
     * If the JMX Connector Server's host and port cannot be identified from the 
     * JMX Service URL or if the JMXServiceURL is not set, then this is the process
     * of indicating this information. This is a no-op in this implementation.
     *
     * @param host - hostname
     * @param port - port
     */
    public void setHostAndPort(String host, int port)
    {
        mHost = host;
        mPort = port;
    }
    
    /**
     * If the connection has already been created, return the existing 
     * JMXConnector unless 'forceNew' is true or the connection is currently null.
     *
     * @param forceNew - create a new connection
     * @param environment - a set of attributes to determine how the connection is made. 
     * This parameter can be null. Keys in this map must be Strings. The appropriate type 
     * of each associated value depends on the attribute. The contents of environment are 
     * not changed by this call
     * @return the JMX Connector.
     * @throws java.io.IOException if a connection cannot be established.
     */
    public javax.management.remote.JMXConnector getJMXConnector(boolean forceNew, 
        Map environment)
        throws java.io.IOException
    {

        StringTranslator translator = new StringTranslator("com.sun.jbi.util", null);
        if ( mHost == null || mPort == 0 )
        {
            throw new java.io.IOException(
                translator.getString(NULL_OBJECT, "JMX Connector Server Host/Port"));
        }          

        if (( mUser == null ) || ( mPwd == null ))
        {
            throw new java.io.IOException(
                translator.getString(NULL_OBJECT, "Username/Password"));
        }
        
        if (( mJmxConnector == null ) || ( forceNew ))
        {
            // -- Create a new connection.
            AppserverConnectionSource src = null;
            if ( mIsSecure )
            {
                System.out.println("Connecting using TLS Params.");
                src = new AppserverConnectionSource( 
                    AppserverConnectionSource.PROTOCOL_RMI,
                    mHost, mPort, 
                    mUser, mPwd,
                    mTlsParams,
                    environment );
            }
            else
            {
                src = new AppserverConnectionSource( 
                    AppserverConnectionSource.PROTOCOL_RMI,
                    mHost, mPort, 
                    mUser, mPwd,
                    null,
                    environment );
                
            }
             
            mJmxConnector = src.getJMXConnector(forceNew);
        }

        
        return mJmxConnector;

    }
    
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ManagementMessageHolder.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.common.management;

/**
 * This provides necessary methods for various management  services to pass the task
 * result data to ManagemntMessageBuilder. ManagementMessageBuilder constructs an XML
 * document based on this.
 *
 * @author Sun Microsystems, Inc.
 */
public class ManagementMessageHolder extends ComponentMessageHolder
{
    /**
     * Framework locale.
     */
    private String mFrameworkLocale = null;

    /**
     * Exception cause.
     */
    private boolean mIsCauseFramework = true;

    /**
     * Creates a new instance of MessageContentHolder with a message type.
     *
     * @param msgType String describing the message type to build.
     */
    public ManagementMessageHolder(String msgType)
    {
        super(msgType);
    }

    /**
     * Set the JBI Framework locale.
     *
     * @param locale JBI Framework Locale
     */
    public void setFrameworkLocale(String locale)
    {
        mFrameworkLocale = locale;
    }

    /**
     * Set the value indicating if JBI Framework is cause of the exception.
     *
     * @param isCauseFramework set value indicating cause
     */
    public void setCauseFramework(boolean isCauseFramework)
    {
        mIsCauseFramework = isCauseFramework;
    }

    /**
     * Get the type(status, exception) of message to be built for this component.
     *
     * @return Task Result
     */
    public String getManagementMessageType()
    {
        return super.getComponentMessageType();
    }

    /**
     * Get the JBI Framework Locales.
     *
     * @return JBI Framework locale
     */
    public String getFrameworkLocale()
    {
        return mFrameworkLocale;
    }

    /**
     * Return the value indicating if the cause of this exception is JBI Framework.
     *
     * @return value indicating exception cause
     */
    public String isCauseFramework()
    {
        return Boolean.toString(mIsCauseFramework);
    }
}

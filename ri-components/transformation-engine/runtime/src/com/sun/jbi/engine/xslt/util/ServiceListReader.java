/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceListReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.engine.xslt.TEResources;
import com.sun.jbi.engine.xslt.TransformationEngineContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class ServiceListReader
    implements TEResources
{
    /**
     * DOCUMENT ME!
     */
    private ComponentContext mContext = null;

    /**
     * DOCUMENT ME!
     */
    private File mServiceListFile;

    /**
     *    
     */
    private Logger mLogger = null;

    /**
     * DOCUMENT ME!
     */
    private String mDeployId = null;

    /**
     * DOCUMENT ME!
     */
    private String mError;

    /**
     *    
     */
    private String mListFile;

    /**
     * DOCUMENT ME!
     */
    private String mSchemaFile;

    /**
     * DOCUMENT ME!
     */
    private String mServiceListName;

    /**
     *    
     */
    private StringTranslator mTranslator = null;

    /**
     * DOCUMENT ME!
     */
    private ConfigBean [] mListofService = null;

    /**
     * DOCUMENT ME!
     */
    private int mServiceCount = 0;

    /**
     * Creates a new ServiceListReader object.
     *
     * @param suPath Service Unit path!
     * @param jbiContext Component context
     */
    public ServiceListReader(
        String suPath,
        ComponentContext jbiContext)
    {
        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
        mDeployId = suPath;
        mContext = jbiContext;

        mListFile =
            suPath + File.separatorChar + ConfigData.CONFIG_FILE_NAME;
        mSchemaFile =
            jbiContext.getInstallRoot() + File.separatorChar
            + ConfigData.SCHEMA_FILE_NAME;
        
        mLogger = TransformationEngineContext.getInstance().getLogger("");
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getError()
    {
        return mError;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getListName()
    {
        return mServiceListName;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getServiceCount()
    {
        return mServiceCount;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ConfigBean [] getServices()
    {
        return mListofService;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean check()
    {
        if ((mDeployId == null) || (mDeployId.equals("")))
        {
            setError(mTranslator.getString(TEResources.INVALID_DEPLOY_ID)
                + mDeployId);

            return false;
        }

        String root = mContext.getInstallRoot();

        mServiceListFile = new File(mListFile);

        if (!mServiceListFile.exists())
        {
            setError(mTranslator.getString(TEResources.INVALID_SERVICE_LOCATION)
                + mServiceListFile.getAbsolutePath());

            return false;
        }

        return true;
    }

    /**
     * DOCUMENT ME!
     */
    public void loadServices()
    {
        if (!check())
        {
            setError(mTranslator.getString(TEResources.CHECK_FAILED)
                + getError());

            return;
        }

        ServiceListValidator slv =
            new ServiceListValidator(mSchemaFile,
                mServiceListFile.getAbsolutePath());

        // slv.validate();
        Document listDoc = slv.parse();

        // code for extracting namespace
        //Element elem = listDoc.getDocumentElement();
        //String namespace = elem.getAttribute(ConfigData.SERVICE_NAMESPACE);

        if (listDoc == null)
        {
            setError(slv.getError());

            return;
        }

        setListName(listDoc);

        NodeList list = listDoc.getElementsByTagName(ConfigData.SERVICE);
        mServiceCount = list.getLength();
        mLogger.info(mTranslator.getString(TEResources.TOTAL_SERVICE)
            + mServiceCount);
        mListofService = new ConfigBean[mServiceCount];

        try
        {
            for (int i = 0; i < mServiceCount; i++)
            {
                Node node = list.item(i);
                ConfigBean sb = new ConfigBean();

                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    /*if (namespace != null)
                    {
                        sb.setValue(ConfigData.SERVICE_NAMESPACE, namespace);
                    }*/

                    //For each <Service> tag, there should be only one 
                    //<service-name> tag
                    Element servEle = (Element) node;
                    NodeList servNodeList =
                        servEle.getElementsByTagName(ConfigData.SERVICE_NAME);

                    //There should be one and only one <service-name> element 
                    //under each <service> tag.
                    Node sNode = servNodeList.item(0);

                    if (sNode != null)
                    {
                        setByTagName(sNode, sb, ConfigData.SERVICE_NAMESPACE);
                        setByTagName(sNode, sb, ConfigData.SERVICENAME_LOCALPART);
                    }
                    else
                    {
                        mLogger.severe(
                            "Some problem getting service-name element. Check.");
                    }

                    //setByTagName(node, sb, ConfigData.NAME);
                    //setByTagName(node, sb, ConfigData.SERVICE_NAMESPACE);
                    setByTagName(node, sb, ConfigData.ENDPOINT);
                    setByTagName(node, sb, ConfigData.SERVICE_DESCRIPTION);
                    setByTagName(node, sb, ConfigData.CREATE_CACHE);
                    setByTagName(node, sb, ConfigData.SERVICEID);
                    setByTagName(node, sb, ConfigData.COLUMN_HEADERS);
                    setByTagName(node, sb, ConfigData.COLUMN_HEADER);
                    setByTagName(node, sb, ConfigData.COLUMN_SEPARATOR);
                    setByTagName(node, sb, ConfigData.FIRST_ROW_COL_HEADERS);
                }

                mListofService[i] = sb;
                mLogger.finer(mTranslator.getString(TEResources.BEAN_UPLOADED)
                    + mListofService[i]);
            }
        }
        catch (Exception ee)
        {
            setError(ee.getMessage());
            ee.printStackTrace();
        }
    }

    /**
     * Utility method for setting the Bean object from the XML Nodes.
     *
     * @param node The node which needs to be
     * @param sb The end point bean object which has to be updated
     * @param tagName the tag name which needs to be read.
     */
    private void setByTagName(
        Node node,
        ConfigBean sb,
        String tagName)
    {
        Element ele = (Element) node;
        NodeList namelist = ele.getElementsByTagName(tagName);

        if (namelist != null)
        {
            for (int i = 0; i < namelist.getLength(); i++)
            {
                Element name = (Element) namelist.item(i);
                String sValue = null;

                try
                {
                    sValue =
                        ((Node) (name.getChildNodes().item(0))).getNodeValue()
                         .trim();
                }
                catch (NullPointerException ne)
                {
                    //ne.printStackTrace();
                    return;
                }

                mLogger.finer("Setting " + tagName + " to value = " + sValue
                    + " *********");
                sb.setValue(tagName, sValue);
            }
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param error DOCUMENT ME!
     */
    private void setError(String error)
    {
        mError = error;
    }

    /**
     * DOCUMENT ME!
     *
     * @param doc NOT YET DOCUMENTED
     */
    private void setListName(Document doc)
    {
        NodeList nl = doc.getElementsByTagName(ConfigData.SERVICE);
        Node serviceListName = nl.item(0);

        try
        {
            mServiceListName = serviceListName.getFirstChild().getNodeValue();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            mServiceListName = "";
        }
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDL11FileReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;

import javax.wsdl.Definition;
import javax.wsdl.Service;
import javax.wsdl.Port;
import javax.wsdl.Binding;
import javax.wsdl.PortType;
import javax.wsdl.Operation;


import java.net.URI;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.StringTokenizer;

import javax.xml.namespace.QName;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

import com.sun.jbi.engine.xslt.TransformationEngineContext;

/** 
 * This WSDL artifact handler implementation is used to extract deployment information
 * from WSDL 1.1 artifacts. It does not validate the artifact file.
 *
 * @author Sun Microsystems, Inc.
 */
public class WSDL11FileReader extends UtilBase
{
    /** 
     * Entry point to WSDL API. 
     */
    private WSDLFactory mFactory;
    
    /** 
     * Reads WSDL definitions. 
     */
    private WSDLReader mReader;
    
    /** 
     * WSDL definition object from WSDL1.1 deployment. 
     */
    private Definition mDefinition;
        
    /**
     * Contains the error message.
     */
    private String mErrorMessage;

    /**
     * Contains the warning message
     */
    private String mWarningMessage;

    /**
     * Contains the exception object.
     */
    private Exception mException;

    /**
     * Internal handle to the logger instance.
     */
    private Logger mLogger;

    /**
     * Internal handle to the String Translator instance.
     */
    private StringTranslator mStringTranslator;

    /**
     * Contains the list of endpoints described in the WSDL File.
     */
    private ConfigBean[] mEndpointList;

    /**
     * Internal handle to the deployment file name.
     */
    private String mDeployFileName;

    /**
     * Creates a new WSDL11FileReader object.
     *
     * @param deployFileName Name of the deployment artifact file
     */
    public WSDL11FileReader(String deployFileName)
    {
        mLogger = TransformationEngineContext.getInstance().getLogger("");
        mStringTranslator = new StringTranslator("com.sun.jbi.engine.xslt",
                                        this.getClass().getClassLoader());
        mDeployFileName = deployFileName;
        
        try
        {
            mFactory = WSDLFactory.newInstance();
            mReader  = mFactory.newWSDLReader();
            
            // parser the deployment file - mDeployFileName (WSDL 1.1) and create 
            // definitions

            mDefinition = mReader.readWSDL(null, mDeployFileName);
        }
        catch (Exception ex)
        {
            mLogger.severe("Exception while Creating WSDLFactory.");
            ex.printStackTrace();
            setValid(false);
            setError(ex.getMessage());
        }
    }


    public boolean isServiceDefined(QName serviceName)
    {
        boolean bRetVal = false;
        
        Service service = mDefinition.getService(serviceName);
        if(service != null)
        {
            bRetVal = true;
        }
        
        return bRetVal;
    }
    
    public ConfigBean[] extractEndpoints(QName servName)
    {
        java.util.Map servPorts = null;
        java.util.Iterator portsIter = null;
        ConfigBean[] mServiceList = null;
        java.util.List cbList = new LinkedList();

        
        Service service = mDefinition.getService(servName);
        if(service != null)
        {
            mLogger.fine("Found Service "+servName.toString());
            servPorts = service.getPorts();
            portsIter = servPorts.keySet().iterator();
            Port aPort = null;
            Binding pBinding = null;
            PortType bPType = null;
            while(portsIter.hasNext())
            {
                aPort = service.getPort((String)portsIter.next());
                if(aPort != null)
                {
                    mLogger.fine("Found a port: "+aPort.getName());
                }
                pBinding = aPort.getBinding();
                if(pBinding != null)
                {
                    mLogger.fine("Found a Binding: "+pBinding.getQName());
                }
                bPType = pBinding.getPortType();
                if(bPType != null)
                {
                    mLogger.fine("Port Type for Binding: "+pBinding.getQName()+ " is :"+bPType.getQName());
                }
                
                ConfigBean cb = new ConfigBean();
                cb.setValue(ConfigData.SERVICE_NAMESPACE, service.getQName().getNamespaceURI());
                cb.setValue(ConfigData.TARGET_NAMESPACE, service.getQName().getNamespaceURI());
                cb.setValue(ConfigData.SERVICENAME_LOCALPART, service.getQName().getLocalPart());
                cb.setValue(ConfigData.ENDPOINT, aPort.getName());
                
                cbList.add(cb);
                
                //Register the Dcoument so that getServiceDescription will return it
                //Need to check the type
                TransformationEngineContext.getInstance().addEndpointDoc(service.getQName().toString()+ aPort.getName(), mDeployFileName);
                TransformationEngineContext.getInstance().addEndpointDef(service.getQName().toString()+ aPort.getName(), mDefinition);
            }
            
            mServiceList = new ConfigBean[cbList.size()];
            for (int x = 0; x < cbList.size(); x++)
            {
                mServiceList[x] = (ConfigBean) cbList.get(x);
            }
        }
        else
        {
            mLogger.fine("Service Not found in wsdl. "+servName.toString());
            return null;
        }
        
        return mServiceList;
    }
    
/*    public static void main(String[] args)
    {
        System.out.println("WSDL11ArtifactsHandler running....");
        Document wsdlDocument = null;
        Document sujbiDocument = null;
        QName serviceQName = null;
        Definition wsdlDefs = null;
        try
        {
            System.out.println("Reading WSDL 1.1 SU Deployment file.");
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            builderFactory.setNamespaceAware(true);
            DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
            sujbiDocument = documentBuilder.parse( new File(args[0]));
            if(sujbiDocument != null)
            {
                System.out.println("Read the WSDL1.1 Deployment file.");
		System.out.println("");
		printXMLfromDOM(sujbiDocument.getDocumentElement());
		System.out.println("");
                
                NodeList artifactNodes = sujbiDocument.getElementsByTagNameNS("*", "TEServiceExtns");
                if ( artifactNodes.getLength() != 1 )
                {
                    System.out.println("Check the su jbi.xml file for artifacts element");
                }
                Node artifactNode = artifactNodes.item(0);
                NamedNodeMap map = artifactNode.getAttributes();

                String deployFileName = getAttributeValue( map, "file-name");
                String deploymentType = getAttributeValue(map,"type");

                Node serviceNode = null;
                NodeList serviceNodes = null;

                serviceNodes = sujbiDocument.getElementsByTagName("provides");
                if(serviceNodes.getLength() != 1)
                {
                    System.out.println("No provides element found in su jbi.xml");
                }
                else
                {
                    serviceNode = serviceNodes.item(0);
                }

                NamedNodeMap jbiNodeMap = serviceNode.getAttributes();
                QName interfaceQName = getAttributeQName( sujbiDocument, jbiNodeMap, "interface-name");
                if ( interfaceQName == null )
                {
                    System.out.println("Empty Interface name in su jbi");
                }
                serviceQName = getAttributeQName (sujbiDocument, jbiNodeMap, "service-name");
                String endpointName = getAttributeValue (jbiNodeMap, "endpoint-name");

                System.out.println("Service Name: "+serviceQName.toString());
                System.out.println("Endpoint Name: "+endpointName);
                System.out.println("Interface Name: "+interfaceQName.toString());
                
                wsdlDefs = mReader.readWSDL(null, args[1]);
                if(wsdlDefs != null)
                {
                    System.out.println("Read WSDL Artifact document.");
                    extractEndpoints(wsdlDefs, serviceQName);
                }
            }
                 
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private static String getStyleFor(javax.wsdl.BindingOperation bo)
    {
        String sty = null;
        
        java.util.List extList = bo.getExtensibilityElements();
        if(extList != null)
        {
            for(int i=0; i<extList.size(); i++)
            {
                System.out.println("");
                System.out.println("BO Ext["+i+"] = "+extList.get(i));
                System.out.println("");
                //System.out.println(""+ extList.get(i).getClass().getName());
                //System.out.println("");
                
                if( extList.get(i) instanceof SOAPOperation)
                {
                    System.out.println("Found a Soap Operation Extension Element. !!!");

                    SOAPOperation sa = (SOAPOperation) extList.get(i);

                    System.out.println("Operation Style : " + sa.getStyle());
                    System.out.println("SOAP Action URI: " + sa.getSoapActionURI());
                    sty = sa.getStyle();
                    //break;
                }
            }
        }
        return sty;
    }
    
    private static String getStyleFor(javax.wsdl.Binding binding)
    {
        String sty = null;
        
        java.util.List extList = binding.getExtensibilityElements();
        if(extList != null)
        {
            for(int i=0; i<extList.size(); i++)
            {
                System.out.println("");
                System.out.println("Ext["+i+"] = "+extList.get(i));
                //System.out.println(""+ extList.get(i).getClass().getName());
                //System.out.println("");
                
                if( extList.get(i) instanceof SOAPBinding)
                {
                    SOAPBinding sb = (SOAPBinding) extList.get(i);
                    sty = sb.getStyle();
                    break;
                }
            }
        }
        return sty;
    }
    
    private static boolean isSOAPBinding(javax.wsdl.Binding aBinding)
    {
        boolean bFlag = false;
        
        java.util.List extList = aBinding.getExtensibilityElements();
        if(extList != null)
        {
            for(int i=0; i<extList.size(); i++)
            {
                System.out.println("List["+i+"] = "+extList.get(i));
                System.out.println("");
                System.out.println(""+ extList.get(i).getClass().getName());

                if( extList.get(i) instanceof SOAPBinding)
                {
                    System.out.println("Found a Soap Binding Extension Element. !!!");

                    SOAPBinding sb = (SOAPBinding) extList.get(i);

                    System.out.println("SOAP Binding Style: " + sb.getStyle());
                    System.out.println("SOAP Binding Transport: " + sb.getTransportURI());
                    bFlag = true;
                    break;
                }
            }
        }
                
        return bFlag;

    }
    */
    private static String getAttributeValue(NamedNodeMap map, String attributeName)
    {
        Node  attributeNode = map.getNamedItem(attributeName);
        String attributeValue = null;
        if ( attributeNode != null )
        {
            attributeValue = attributeNode.getNodeValue();
        }
        return attributeValue;
    }
    
    private static QName getAttributeQName(Document document, NamedNodeMap map, String attributeName)
     {
         Node  attributeNode = map.getNamedItem(attributeName);
         String attributeValue = null;
         if ( attributeNode != null )
         {
            attributeValue = attributeNode.getNodeValue();
         }
         else
         {
             return null;
         }             
         String namespacePrefix = null;
         String localName = null;
         String namespace = null;
         StringTokenizer attributeValueTokenizer = new StringTokenizer(attributeValue, ":");
         if ( attributeValueTokenizer.countTokens() == 1 )
         {
             localName = attributeValue;
         }
         else
         {
             namespacePrefix = attributeValueTokenizer.nextToken();
             localName = attributeValueTokenizer.nextToken();
         }
         // Find the namespace associate with this namespace prefix if namespace is
         // not null
         if ( namespacePrefix != null )
         {
            NamedNodeMap namespaceMap = document.getDocumentElement().getAttributes();
            for (int j = 0; j < namespaceMap.getLength(); j++)
            {
                Node namespaceNode = namespaceMap.item(j);                
                if (namespaceNode.getLocalName().trim().equals(namespacePrefix.trim()))
                {
                    namespace =  namespaceNode.getNodeValue();
                    break;
                }
            }

            if ( namespace == null )
            {
                System.out.println("Namespace null. !!!!");
            }

         }

         if ( namespace != null )
         {
             return new QName(namespace, localName);
         }
         else
         {
             return new QName(localName);
         }
     }
    
    private static void printXMLfromDOM(Element element) 
    {
         int k;
         NamedNodeMap attributes;
         NodeList children = element.getChildNodes();
         // Start from this element
         System.out.print("<" + element.getNodeName());
         // print attibutes inside the element start tag
         attributes = element.getAttributes();
         if (attributes != null) 
         {
            for (k = 0; k < attributes.getLength(); k++) 
            {
                System.out.print(" " + attributes.item(k).getNodeName());
                System.out.print("=" + attributes.item(k).getNodeValue());
            }
         }
         
         // check for element value or sub-elements
         if (element.hasChildNodes()) 
         { 
            System.out.print(">");
            // print all child elements in the DOM tree 

             for (k = 0; k < children.getLength(); k++) 
             {
                if (children.item(k).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) 
                {
                    printXMLfromDOM((Element) children.item(k));
                }
                else if (children.item(k).getNodeType() == org.w3c.dom.Node.TEXT_NODE) 
                {
                    System.out.print(children.item(k).getNodeValue());
                }
             }  // for loop ends here
             // print end tag
             System.out.print("</" + element.getNodeName() + ">");

         }
         else 
         {  
          // element seems to be empty
          System.out.print(" />");
         }// else ends here
        
    }// printXMLfromDOM ends here

}

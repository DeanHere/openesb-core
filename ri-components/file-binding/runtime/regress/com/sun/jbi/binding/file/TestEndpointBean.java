/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEndpointBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestEndpointBean extends TestCase
{
    /**
     * Creates a new TestEndpointBean object.
     */
    private EndpointBean mEndpointBean;

    /**
     * Creates a new TestEndpointBean object.
     *
     * @param testName DOCUMENT ME!
     */
    public TestEndpointBean(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * sets up test.
     */
    public void setUp()
    {
        mEndpointBean = new EndpointBean();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestEndpointBean.class);

        return suite;
    }

    /**
     * Test of getDeploymentId method, of class
     * com.sun.jbi.binding.file.EndpointBean.
     */
    public void testGetDeploymentId()
    {
        System.out.println("testGetDeploymentId");
        mEndpointBean.setDeploymentId("123456");
        assertEquals("Could not set/get deployment Id ", "123456",
            mEndpointBean.getDeploymentId());
    }

    /**
     * Test of getUniqueName method, of class
     * com.sun.jbi.binding.file.EndpointBean.
     */
    public void testGetUniqueName()
    {
        System.out.println("testGetUniqueName");
        mEndpointBean.setValue("service-local-name", new String("service1"));        
        mEndpointBean.setValue("service-namespace", new String("http://sun.com"));
        mEndpointBean.setValue("endpoint-name", new String("endpoint1"));
        assertEquals("Could not set/get unique name", "{http://sun.com}service1endpoint1",
            mEndpointBean.getUniqueName());
    }

    /**
     * Test of getValue method, of class com.sun.jbi.binding.file.EndpointBean.
     */
    public void testGetValue()
    {
        System.out.println("testGetValue");
        mEndpointBean.setValue("MYVALUE", new String("myvalue"));
        assertEquals("Could not set/get Value ", "myvalue",
            mEndpointBean.getValue("MYVALUE"));
    }
}

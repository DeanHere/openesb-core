/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileThreads.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.framework.WorkManager;
import com.sun.jbi.binding.file.util.ConfigData;
import com.sun.jbi.binding.file.util.FileBindingUtil;
import com.sun.jbi.binding.file.util.FileListing;
import com.sun.jbi.binding.file.util.InputFileFilter;
import com.sun.jbi.binding.file.util.StringTranslator;
import com.sun.jbi.binding.file.util.UtilBase;

import java.io.File;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * The File Threads class processes the requests by polling at  the directory
 * specified in deployment artifact file.  For each file to be processed, it
 * spawns a thread and processes it.
 *
 * @author Sun Microsystems, Inc.
 */
public class FileThreads
    extends UtilBase
    implements Runnable, FileBindingResources
{
    /**
     * Default min threads.
     */
    private static final int DEFAULT_MAX_THREADS = 10;

    /**
     * Default max threads.
     */
    private static final int DEFAULT_MIN_THREADS = 5;

    /**
     * Wait time for polling the read directory.
     */
    private static final int WAIT_TIME = 50;

    /**
     * Component Context.
     */
    private ComponentContext mContext;

    /**
     * Endpoint attributes object.
     */
    private EndpointBean mBean;

    /**
     * Folder name.
     */
    private File mDirName;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     * Monitor Object
     */
    private Object mMonitor;

    /**
     * Helper for i18n.
     */
    private StringTranslator mTranslator;

    /**
     * The ThreadGroup for the Endpoint.
     */
    private WorkManager mWorkManager = null;

    /**
     * Flag to hold the state of this thread.
     */
    private boolean mRunning;

    /**
     * Last bacth size
     */
    private int mLastBatchSize;

    /**
     * Constructor which sets the Normlized message object, Required to create
     * messages every time we send a message.
     *
     * @param endpointInfo end point bean object
     */
    public FileThreads(EndpointBean endpointInfo)
    {
        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        mBean = endpointInfo;
        mContext = FileBindingContext.getInstance().getContext();

        String endptname = endpointInfo.getUniqueName();
        mWorkManager = WorkManager.getWorkManager(endptname);
        mWorkManager.setMinThreads(DEFAULT_MIN_THREADS);
        mWorkManager.setMaxThreads(DEFAULT_MAX_THREADS);
        mMonitor = new Object();
    }

    /**
     * Returns the EndpointBean.
     *
     * @return endpoitn bean object.
     */
    public EndpointBean getBean()
    {
        return mBean;
    }

    /**
     * MEhtod to check if the thread is still executing.
     *
     * @return true if running false otherwise.
     */
    public boolean isRunning()
    {
        return mRunning;
    }

    /**
     * Implemented as a thread because it has to poll a folder continuosly.
     */
    public void run()
    {
        mRunning = true;
        mLastBatchSize = 0;

        if (!mRunning)
        {
            return;
        }

        HashMap listFiles = null;
        InputFileFilter filefilter = new InputFileFilter();
        filefilter.setFilterexpression(mBean.getValue(ConfigData.INPUTPATTERN));
        mWorkManager.start();

        String nam = (String) mBean.getValue(ConfigData.INPUTDIR);

        if (nam != null)
        {
            mDirName = new File(nam);
        }
        else
        {
            mRunning = false;
            mLog.info(mTranslator.getString(FBC_STOP_POLLING,
                    mBean.getUniqueName()));

            return;
        }

        while (mMonitor != null)
        {
            try
            {
                listFiles = getFiles(filefilter);

                if (listFiles == null)
                {
                    mWorkManager.cease();
                    mLog.info(mTranslator.getString(FBC_STOP_POLLING,
                            mBean.getUniqueName()));

                    return;
                }

                if (listFiles.isEmpty())
                {
                    gotoSleep();

                    continue;
                }

                Iterator listIterator = listFiles.keySet().iterator();
                mLastBatchSize = listFiles.size();

                while (listIterator.hasNext())
                {
                    File f = (File) listIterator.next();
                    
                    if ((f.canWrite()) && (!f.isHidden()))
                    {
                        QName oper = (QName) listFiles.get(f);
                        String trk = FileBindingUtil.getTrackingId();
                        FileBindingUtil.addEntry(trk, f.getAbsolutePath());
                        mWorkManager.processCommand(new FileCommand(f, mBean,
                                oper, trk));
                    }
                    else
                    {
                        mLog.severe(mTranslator.getString(
                                FBC_CANNOT_PROCESS_FILE, f.getAbsolutePath()));
                    }
                }

                mWorkManager.finishWork();
                listFiles.clear();
            }
            catch (Exception we)
            {
                we.printStackTrace();

                continue;
            }
        }

        /*
         *Destroy, the thread pool, when needed to stop the FBC
         */
        mWorkManager.cease();
        mLog.info(mTranslator.getString(FBC_STOP_POLLING,
                mBean.getUniqueName()));
        mRunning = false;
    }

    /**
     * Stops the File thread.
     */
    public void stopAll()
    {
        mLog.info(mTranslator.getString(FBC_STOP_POLLING, mBean.getUniqueName()));
        mMonitor = null;
    }

    /**
     * Gets a list of files from all the folders that need to be checked.
     *
     * @param filter filter pattern for reading files.
     *
     * @return List of file operation combinations.
     */
    private HashMap getFiles(InputFileFilter filter)
    {
        HashMap filesmap = new HashMap();

        try
        {
            addFiles(filesmap, mDirName, mBean.getDefaultOperation(), filter);

            for (int i = 0; i < mBean.getOperationsCount(); i++)
            {
                File newdir =
                    new File(mDirName.getAbsolutePath() + File.separatorChar
                        + mBean.getOperationQName(i).getLocalPart());

                addFiles(filesmap, newdir, mBean.getOperationQName(i), filter);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return filesmap;
    }

    /**
     * Util method for adding files into list.
     *
     * @param map List into which files have to be added
     * @param newdir folder to check.
     * @param oper operatio name.
     * @param fil filter.
     */
    private void addFiles(
        HashMap map,
        File newdir,
        QName oper,
        InputFileFilter fil)
    {
        List l = FileListing.getFileListing(newdir, false, fil);

        try
        {
            if ((l != null) && (l.size() > 0))
            {
                for (int j = 0; j < l.size(); j++)
                {
                    File f = (File) l.get(j);
                    map.put(f, oper);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Sleep logic.
     */
    private void gotoSleep()
    {
        try
        {
            /**
             * If we find less files in the last iteration then we sleep more,
             * else we sleep less 0 files mean max sleep time of 500ms
             * BATCH_SIZE files mean least sleep time of 0 sec
             */
            Thread.sleep((ConfigData.BATCH_SIZE - mLastBatchSize) * WAIT_TIME);
        }
        catch (Exception ee)
        {
            //ee.printStackTrace();
        }
    }
}

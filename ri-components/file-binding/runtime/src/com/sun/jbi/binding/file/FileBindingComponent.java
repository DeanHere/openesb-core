/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileBindingComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import org.w3c.dom.DocumentFragment;

import javax.jbi.component.Component;
import javax.jbi.servicedesc.ServiceEndpoint;


/**
 * This class implements the Component contract with JBI.
 *
 * @author Sun Microsystems, Inc.
 */
public class FileBindingComponent
    implements javax.jbi.component.Component
{
    /**
     *  Lifecycle object.
     */
    private FileBindingLifeCycle mLifeCycle;

    /**
     *  Resolver object.
     */
    private FileBindingResolver mResolver;

    /**
     * Service unit manager.
     */
    private FileBindingSUManager mSUManager;

    /**
     * Creates a new instance of FileBindingComponent
     */
    public FileBindingComponent()
    {
        mLifeCycle = new FileBindingLifeCycle();
        mSUManager = new FileBindingSUManager();
        mResolver = new FileBindingResolver();
        mLifeCycle.setSUManager(mSUManager);
        mLifeCycle.setResolver(mResolver);
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually
     * perform the operation desired.
     *
     * @param endpoint
     * @param exchange
     *
     * @return  true if its OK.
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        /* Should not always return true
         * The capabilities of this component has to be checked first.
         * Not implemented right now
         */
        return true;
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually
     * interact with the the provider completely.
     *
     * @param endpoint
     * @param exchange
     *
     * @return  true if OK.
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        /* Should not always return true
         * The capabilities of this component has to be checked first.
         * Not implemented right now
         */
        return true;
    }

    /**
     * Returns the lifecycle object for this component.
     *
     * @return component life cycle object for file binding.
     */
    public javax.jbi.component.ComponentLifeCycle getLifeCycle()
    {
        return mLifeCycle;
    }

    /**
     * Returns a resolver for meta-data.
     *
     * @param ServiceEndpoint endpoint reference object.
     *
     * @return Descriptor for file binding.
     */
    public org.w3c.dom.Document getServiceDescription(
        javax.jbi.servicedesc.ServiceEndpoint ServiceEndpoint)
    {
        org.w3c.dom.Document desc = null;

        try
        {
            desc = mResolver.getServiceDescription(ServiceEndpoint);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return desc;
    }

    /**
     * Returns the SU manager which is in charge of handling deployment. Acc to the
     * spec the object should be the same everytime this method is called.
     *
     * @return SU manager.
     */
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager()
    {
        return mSUManager;
    }

    /**
     * Resolve the endpoint reference using the given capabilities of the
     * consumer. This is called by JBI when its trying to resove the given EPR
     * on behalf of the component.
     *
     * @param epr endpoint reference.
     *
     * @return  Service enpoint.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SequencingEngineUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.util;

/**
 * Sequencing binding util class.
 *
 * @author Sun Microsystems, Inc.
 */
public class SequencingEngineUtil
{
    /**
     * Tracking id.
     */
    private static long sTrackingId = 0;

    /**
     * padding length.
     */
    private static final int PAD_LENGTH = 20;

    /**
     * Method to generate tracking id.
     *
     * @return tracking id.
     */
    public static synchronized String getTrackingId()
    {
        sTrackingId++;

        if (sTrackingId >= Long.MAX_VALUE)
        {
            sTrackingId = 0;
        }

        return padString(Long.toString(sTrackingId));
    }

    /**
     * Util method for padding with zeros.
     *
     * @param s string to be padded.
     *
     * @return padded string.
     */
    private static String padString(String s)
    {
        StringBuffer sb = new StringBuffer(s);
        int len = sb.length();

        for (int i = 0; i < (PAD_LENGTH - len); i++)
        {
            sb.insert(0, '0');
        }

        return sb.toString();
    }
}

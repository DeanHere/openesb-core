/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SequencingEngineContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.DeliveryChannel;

import javax.transaction.TransactionManager;


/**
 * Context object to store sequencing engine specific informaiton.
 *
 * @author Sun Microsystems, Inc.
 */
public final class SequencingEngineContext
{
    /**
     * This object
     */
    private static SequencingEngineContext sSeqContext;

    /**
     * Engine context
     */
    private ComponentContext mContext;

    /**
     * Logger object for this component.
     */
    private Logger mLogger;

    /**
     * Creates a new SequencingEngineContext object.
     */
    private SequencingEngineContext()
    {
    }

    /**
     * Gets the engine channel.
     *
     * @return engine channel
     */
    public DeliveryChannel getChannel()
    {
        DeliveryChannel chnl = null;

        if (mContext != null)
        {
            try
            {
                chnl = mContext.getDeliveryChannel();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return chnl;
    }

    /**
     * Sets the engine context.
     *
     * @param ctx engine context.
     */
    public void setContext(ComponentContext ctx)
    {
        mContext = ctx;
    }

    /**
     * Returns the jbi context.
     *
     * @return engine context
     */
    public ComponentContext getContext()
    {
        return mContext;
    }

    /**
     * Used to grab a reference of this object.
     *
     * @return an initialized SequencingEngineContext reference
     */
    public static synchronized SequencingEngineContext getInstance()
    {
        if (sSeqContext == null)
        {
            sSeqContext = new SequencingEngineContext();
        }

        return sSeqContext;
    }

    /**
     * Sets the Logger.
     *
     * @param logger logger for this component.
     */
    public void setLogger(Logger logger)
    {
        mLogger = logger;
    }

    /**
     * Returns the logger.
     *
     * @return logger
     */
    public Logger getLogger()
    {
        if (mLogger == null)
        {
            mLogger = Logger.getLogger("com.sun.jbi.engine.sequencing");
        }

        return mLogger;
    }

    /**
     * Returns the transaction manager
     *
     * @return Transaction manager
     */
    public TransactionManager getTransactionManager()
    {
        return (javax.transaction.TransactionManager) mContext
        .getTransactionManager();
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageExchangeHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.util;

import com.sun.jbi.engine.sequencing.PatternRegistry;
import com.sun.jbi.engine.sequencing.SequencingEngineContext;

import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.RobustInOnly;


/**
 * Helper class to process message exchanges.
 *
 * @author Sun Microsystems, Inc.
 */
public class MessageExchangeHelper
{
    /**
     * Logger.
     */
    private static Logger sLog =
        Logger.getLogger("com.sun.jbi.engine.seqeuncing");

    /**
     * Creates a new instance of MessageExchangeHelper.
     */
    public MessageExchangeHelper()
    {
    }

    /**
     * Returns error.
     *
     * @param msg ME
     *
     * @return exception exception
     */
    public static Exception getError(MessageExchange msg)
    {
        if (msg == null)
        {
            return null;
        }

        if (msg instanceof InOut)
        {
            return (((InOut) (msg)).getError());
        }
        else if (msg instanceof InOnly)
        {
            return null;
        }
        else if (msg instanceof RobustInOnly)
        {
            return (((RobustInOnly) (msg)).getError());
        }
        else
        {
            return null;
        }
    }

    /**
     * Returns fault.
     *
     * @param msg message exchange from which fault has to be extracted.
     *
     * @return Fault fault
     */
    public static Fault getFault(MessageExchange msg)
    {
        if (msg == null)
        {
            return null;
        }

        if (msg instanceof InOut)
        {
            return (((InOut) (msg)).getFault());
        }
        else if (msg instanceof InOnly)
        {
            return null;
        }
        else if (msg instanceof RobustInOnly)
        {
            return (((RobustInOnly) (msg)).getFault());
        }
        else
        {
            return null;
        }
    }

    /**
     * Returns the in message.
     *
     * @param msg message exchange
     *
     * @return normalized message
     */
    public static NormalizedMessage getInMessage(MessageExchange msg)
    {
        if (msg == null)
        {
            sLog.severe("Returnign nulll in getInMessage()");

            return null;
        }

        if (msg instanceof InOut)
        {
            return (((InOut) (msg)).getInMessage());
        }
        else if (msg instanceof InOnly)
        {
            return (((InOnly) (msg)).getInMessage());
        }
        else if (msg instanceof RobustInOnly)
        {
            return (((RobustInOnly) (msg)).getInMessage());
        }
        else
        {
            return null;
        }
    }

    /**
     * Returns the out message.
     *
     * @param msg message exchange
     *
     * @return normalized message
     */
    public static NormalizedMessage getOutMessage(MessageExchange msg)
    {
        if (msg == null)
        {
            return null;
        }

        if (msg instanceof InOut)
        {
            return (((InOut) (msg)).getOutMessage());
        }
        else if (msg instanceof InOnly)
        {
            return null;
        }
        else if (msg instanceof RobustInOnly)
        {
            return null;
        }
        else
        {
            return null;
        }
    }

    /**
     * Creates an exchange corresponding to pattern.
     *
     * @param pattern pattern
     *
     * @return message exchange
     */
    public static MessageExchange createExchange(String pattern)
    {
        sLog.fine("Creating exchange for pattern " + pattern);

        MessageExchange tmpExchng = null;

        try
        {
            DeliveryChannel chnl =
                SequencingEngineContext.getInstance().getChannel();

            MessageExchangeFactory factory = chnl.createExchangeFactory();

            tmpExchng = factory.createExchange(new java.net.URI(pattern));

            return tmpExchng;
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Updates the message exchange.
     *
     * @param msg message exchange.
     * @param in in message
     *
     * @return true / false depending if update was successful or not
     */
    public static boolean updateInMessage(
        MessageExchange msg,
        NormalizedMessage in)
    {
        if (msg == null)
        {
            return false;
        }

        if (in != null)
        {
            return createIn(msg, in);
        }

        return true;
    }

    /**
     * Updates the message accding to the arguments provided.
     *
     * @param msg ME
     * @param out Out message
     * @param error errror message
     * @param fault fault
     *
     * @return true if update is successfull.
     */
    public static boolean updateMessage(
        MessageExchange msg,
        NormalizedMessage out,
        Exception error,
        Fault fault)
    {
        if (msg == null)
        {
            return false;
        }

        if (out != null)
        {
            return createOut(msg, out);
        }

        if (fault != null)
        {
            return createFault(msg, fault);
        }

        if (error != null)
        {
            return createError(msg, error);
        }

        try
        {
            msg.setStatus(ExchangeStatus.DONE);

            if (msg.getProperty(ConfigData.RESPONSE_SEQ_ID) == null)
            {
                String id = SequencingEngineUtil.getTrackingId();
                msg.setProperty(ConfigData.RESPONSE_SEQ_ID, id);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }

        return true;
    }

    /**
     * Creates an error message.
     *
     * @param msg ME
     * @param error exception
     *
     * @return true/ false.
     */
    private static boolean createError(
        MessageExchange msg,
        Exception error)
    {
        try
        {
            if (error == null)
            {
                msg.setError(new Exception("Unknown Exception"));

                return true;
            }

            msg.setError(error);

            if (msg.getProperty(ConfigData.RESPONSE_SEQ_ID) == null)
            {
                String id = SequencingEngineUtil.getTrackingId();
                msg.setProperty(ConfigData.RESPONSE_SEQ_ID, id);
            }
        }
        catch (Exception me)
        {
            me.printStackTrace();

            return false;
        }

        return true;
    }

    /**
     * Creates a fault message.
     *
     * @param msg ME
     * @param flt fault
     *
     * @return true / false
     */
    private static boolean createFault(
        MessageExchange msg,
        Fault flt)
    {
        if (flt == null)
        {
            return false;
        }

        try
        {
            if (msg instanceof InOnly)
            {
                msg.setStatus(ExchangeStatus.ERROR);
            }

            msg.setFault(flt);
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }

        return true;
    }

    /**
     * Creates an In Message.
     *
     * @param msg ME
     * @param in in message
     *
     * @return true / false.
     */
    private static boolean createIn(
        MessageExchange msg,
        NormalizedMessage in)
    {
        String id = SequencingEngineUtil.getTrackingId();

        try
        {
            if (msg instanceof InOut)
            {
                ((InOut) (msg)).setInMessage(in);
                msg.setProperty(ConfigData.REQUEST_SEQ_ID, id);
            }
            else if (msg instanceof InOnly)
            {
                ((InOnly) (msg)).setInMessage(in);
                msg.setProperty(ConfigData.REQUEST_SEQ_ID, id);
            }
            else if (msg instanceof RobustInOnly)
            {
                ((RobustInOnly) (msg)).setInMessage(in);
                msg.setProperty(ConfigData.REQUEST_SEQ_ID, id);
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }

        return true;
    }

    /**
     * Creates the out message.
     *
     * @param msg ME
     * @param out out message
     *
     * @return true /false.
     */
    private static boolean createOut(
        MessageExchange msg,
        NormalizedMessage out)
    {
        String id = SequencingEngineUtil.getTrackingId();

        try
        {
            if (msg instanceof InOut)
            {
                ((InOut) (msg)).setOutMessage(out);
                msg.setProperty(ConfigData.RESPONSE_SEQ_ID, id);
            }
            else if (msg instanceof InOnly)
            {
                msg.setStatus(ExchangeStatus.DONE);
                msg.setProperty(ConfigData.RESPONSE_SEQ_ID, id);
            }
            else if (msg instanceof RobustInOnly)
            {
                msg.setStatus(ExchangeStatus.DONE);
                msg.setProperty(ConfigData.RESPONSE_SEQ_ID, id);
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();

            return false;
        }

        return true;
    }
}

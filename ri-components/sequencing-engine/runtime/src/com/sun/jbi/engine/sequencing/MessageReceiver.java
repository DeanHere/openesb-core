/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageReceiver.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.SequencingEngineContext;
import com.sun.jbi.engine.sequencing.framework.Servicelist;
import com.sun.jbi.engine.sequencing.framework.threads.WorkManager;
import com.sun.jbi.engine.sequencing.util.StringTranslator;

import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchange;


/**
 * MessageReceiver
 *
 * @author Sun Microsystems, Inc.
 */
class MessageReceiver
    implements Runnable, SequencingEngineResources
{
    /**
     * Time out for receive
     */
    private static final long TIME_OUT = 500;

    /**
     * EngineChannel object
     */
    private DeliveryChannel mChannel;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     * Normalized Message
     */
    private MessageExchange mExchange;

    /**
     * Message processor
     */
    private MessageProcessor mMessageProcessor;

    /**
     * a Monitor Object to stop the thread.
     */
    private Object mMonitor;

    /**
     * Translator object for internationalization.
     */
    private StringTranslator mTranslator;

    /**
     * Work Manager, which maintians thread pool.
     */
    private WorkManager mWorkManager;

    /**
     * Creates the MessageReceiver Thread.
     *
     * @param bc delivery channel
     */
    public MessageReceiver(DeliveryChannel bc)
    {
        mLog = SequencingEngineContext.getInstance().getLogger();
        mChannel = bc;
        mWorkManager = WorkManager.getWorkManager("SEQ_RECEIVER");
        mMessageProcessor = new MessageProcessor(mChannel);
        mMonitor = new Object();
        mTranslator = new StringTranslator();
    }

    /**
     * Blocking call on the service channel to receive the message.
     */
    public void run()
    {
        mLog.info(mTranslator.getString(SEQ_RECEIVER_START));
        mWorkManager.start();

        while (mMonitor != null)
        {
            try
            {
                mExchange = mChannel.accept(TIME_OUT);

                if (mExchange != null)
                {
                    Servicelist list = null;

                    mMessageProcessor.setExchange(mExchange);
                    list = mMessageProcessor.process();

                    if ((list == null) || (!mMessageProcessor.valid()))
                    {
                        mLog.severe(mTranslator.getString(SEQ_INVALID_MESSAGE,
                                mExchange.getExchangeId()));

                        continue;
                    }

                    if (!mWorkManager.processCommand(list))
                    {
                        mLog.info(mTranslator.getString(SEQ_NO_FREE_THREAD));
                    }
                }
            }
            catch (Exception e)
            {
                mLog.severe(mTranslator.getString(SEQ_RECEIVER_ERROR));
                e.printStackTrace();
                mWorkManager.cease();

                return;
            }
        }

        mWorkManager.cease();
        mLog.info(mTranslator.getString(SEQ_RECEIVER_STOPPED));
    }

    /**
     * Stops new requests for this engine.
     */
    public void stopNewRequests()
    {
        mMessageProcessor.stopNewRequests();
    }

    /**
     * Stops the receiving thread.
     */
    public void stopReceiving()
    {
        mLog.info(mTranslator.getString(SEQ_RECEIVER_STOP));

        // Close the Receiver now.
        mMonitor = null;
    }
}

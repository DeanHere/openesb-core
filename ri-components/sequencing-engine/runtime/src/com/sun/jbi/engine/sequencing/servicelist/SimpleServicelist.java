/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SimpleServicelist.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import com.sun.jbi.engine.sequencing.MessageRegistry;
import com.sun.jbi.engine.sequencing.SequencingEngineContext;
import com.sun.jbi.engine.sequencing.SequencingEngineResources;
import com.sun.jbi.engine.sequencing.framework.Servicelist;
import com.sun.jbi.engine.sequencing.util.ConfigData;
import com.sun.jbi.engine.sequencing.util.MessageExchangeHelper;
import com.sun.jbi.engine.sequencing.util.SequencingEngineUtil;
import com.sun.jbi.engine.sequencing.util.StringTranslator;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.transaction.Transaction;

import javax.xml.namespace.QName;


/**
 * Class Servicelist. This class has the logic for exetiing the service list
 * This is a simple implementation which executes the services in a sequence.
 *
 * @author Sun Microsystems, Inc.
 */
public class SimpleServicelist
    implements Servicelist, SequencingEngineResources
{
    /**
     * Engine Channel.
     */
    private DeliveryChannel mChannel;

    /**
     * Object to store the last occurred error.
     */
    private Exception mLastError;

    /**
     * Object to store the last fault message.
     */
    private Fault mLastFaultMessage;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     * Object to keep track of current exchange.
     */
    private MessageExchange mCurrentExchange;

    /**
     * Keeps track of the inbound exchange.
     */
    private MessageExchange mInboundExchange;

    /**
     * Message registry object.
     */
    private MessageRegistry mMessageRegistry;

    /**
     * Last input message.
     */
    private NormalizedMessage mLastInputMessage;

    /**
     * Final output message.
     */
    private NormalizedMessage mLastOutputMessage;

    /**
     * Current service being executed.
     */
    private ServiceBean mCurrentService;

    /**
     * Service list information.
     */
    private ServicelistBean mListBean;

    /**
     * Sequence id for every list.
     */
    private String mSequenceId;

    /**
     * Translator object for internationalization.
     */
    private StringTranslator mTranslator;

    /**
     * Timer object for tracking time-out.
     */
    private Timer mTimer;

    /**
     * Current transaction.
     */
    private Transaction mCurrentTransaction;

    /**
     * True if a response is expected false otherwise
     */
    private boolean mResponseExpected;

    /**
     * Indicates if an exchange has timed out.
     */
    private boolean mTimedOut = false;

    /**
     * The service index number.
     */
    private int mServiceIndex = 0;

    /**
     * State.
     */
    private int mState;

    /**
     *
     */

    /**
     *    
     */
    private int mType;

    /**
     * Creates a new Servicelist object.
     */
    public SimpleServicelist()
    {
        mTimer = new Timer();
        mLog = SequencingEngineContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        mMessageRegistry = MessageRegistry.getInstance();
        mChannel = SequencingEngineContext.getInstance().getChannel();
        mSequenceId = SequencingEngineUtil.getTrackingId();
        setState(ServicelistState.READY);
    }

    /**
     * Returns the current exchange object that is being processed.
     *
     * @return message exchange
     */
    public MessageExchange getCurrentExchange()
    {
        return mCurrentExchange;
    }

    /**
     * Returns the current service object, thats being executed.
     *
     * @return service bean object.
     */
    public ServiceBean getCurrentService()
    {
        return mCurrentService;
    }

    /**
     * Returns the deployment ID associated with this servicelist.
     *
     * @return deployment id
     */
    public String getDeploymentId()
    {
        if (mListBean != null)
        {
            return mListBean.getDeploymentId();
        }
        else
        {
            return null;
        }
    }

    /**
     * Setter for property mInboundExchange.
     *
     * @param mInboundExchange New value of property mInboundExchange.
     */
    public void setInboundExchange(
        javax.jbi.messaging.MessageExchange mInboundExchange)
    {
        this.mInboundExchange = mInboundExchange;
    }

    /**
     * Getter for property mInboundExchange.
     *
     * @return Value of property mInboundExchange.
     */
    public javax.jbi.messaging.MessageExchange getInboundExchange()
    {
        return mInboundExchange;
    }

    /**
     * returnd the list name that is being executed in this object.
     *
     * @return string list name
     */
    public String getListName()
    {
        if (mListBean != null)
        {
            return mListBean.getServicename();
        }
        else
        {
            return null;
        }
    }

    /**
     * sets the message exchange to be processed in this list.
     *
     * @param msgex msg exchange
     */
    public void setMessageExchange(MessageExchange msgex)
    {
        mCurrentExchange = msgex;
    }

    /**
     * sets the servicelist bean.
     *
     * @param listbean service list bean.
     */
    public void setServicelistBean(ServicelistBean listbean)
    {
        mListBean = listbean;
    }

    /**
     * Returns the servicelist bean.
     *
     * @return list bean
     */
    public ServicelistBean getServicelistBean()
    {
        return mListBean;
    }

    /**
     * Sets state.
     *
     * @param state state.
     */
    public void setState(int state)
    {
        mState = state;
    }

    /**
     * Returns the current state od the service list.
     *
     * @return state
     */
    public int getState()
    {
        return 0;
    }

    /**
     * Sets the transaction context.
     *
     * @param tran transaction.
     */
    public void setTransactionContext(Transaction tran)
    {
        mCurrentTransaction = tran;
    }

    /**
     * sets the type.
     *
     * @param type request or response
     */
    public void setType(int type)
    {
        mType = type;
    }

    /**
     * Cancels the message exchange.
     */
    public void cancelExchange()
    {
        mLog.info(mTranslator.getString(SEQ_SERVICE_TIMED_OUT,
                mCurrentExchange.getExchangeId(),
                mCurrentExchange.getEndpoint().getServiceName()));        
        mMessageRegistry.deregisterExchange(mCurrentExchange
            .getExchangeId() + mSequenceId);
        mMessageRegistry.registerTimedOutExchange(mCurrentExchange
            .getExchangeId() + mSequenceId);

        mLastError =
            new Exception(mTranslator.getString(SEQ_SERVICE_TIMED_OUT,
                    mCurrentExchange.getExchangeId(),
                    mCurrentExchange.getEndpoint().getServiceName()));
        mTimer.cancel();
        sendListError();
        mListBean.updateState(mSequenceId, mState);
    }

    /**
     * This is the command pattern implementation method which is invoked when
     * this object is run in  a thread pool.
     */
    public void execute()
    {
        /* This method will be executed in a separate thread.
         * Every new request / response for the service list will
         * result in invocation of this method.
         */
        mLog.fine(this + "Executing message "
            + mCurrentExchange.getExchangeId());
        
        if (mChannel == null)
        {
            mLog.severe(mTranslator.getString(SEQ_EXCHANGE_NOT_SET));

            return;
        }

        if (mCurrentExchange == null)
        {
            mLog.severe(mTranslator.getString(SEQ_CHANNEL_NOT_SET));

            return;
        }

        if (mListBean == null)
        {
            mLog.severe(mTranslator.getString(SEQ_BEAN_NOT_SET));

            return;
        }

        if (hasTimedOut())
        {
            mLog.info(mTranslator.getString(SEQ_SERVICE_TIMED_OUT,
                    mCurrentExchange.getExchangeId(),
                    mCurrentExchange.getEndpoint().getServiceName()));
            setState(ServicelistState.TIMED_OUT);
            try
            {
                if (mCurrentExchange.getStatus().equals(ExchangeStatus.ACTIVE))
                {
                    mCurrentExchange.setStatus(ExchangeStatus.DONE);
                }
                mChannel.send(mCurrentExchange);
            }
            catch (Exception tee)
            {
                mLog.severe("Unable to respond back to timed out exchange");
                //dont do anythign, we tried our best to end this 
                // in a sane way.
            }
            
            mMessageRegistry.deregisterTimedOutExchange(mCurrentExchange
            .getExchangeId() + mSequenceId);
            /**
             *
             * The person who sent this message might ne waiting for
             * a response back , so send it 
             */
            return;
        }

        /* this is the possible states and the MEPs that can get here
         * and their interpretation
         */
        /*
         * ---------------------------------------------------------------
         * MEP - >      In-Out              In-Only         Robust-In-Only
         * State
         * ---------------------------------------------------------------
         *
         *  ACTIVE      New Request /       New Request         New Request
         *              Response
         *
         *  ERROR       Response Failure       Status           Reponse Failed
         *              completed (stage 2)/   response failed   Completed (stg 2)/
         *             Status failed                            Fault status failed
         *              (stage 3 of MEP)                               (stg 3)
         *
         *  DONE        Status success      Status success      Status success completed
         *              completed
         */
        setState(ServicelistState.RUNNING);
        resetTimer();

        if (mType == ConfigData.REQUEST_TYPE)
        {
            processRequest();
        }
        else if (mType == ConfigData.RESPONSE_TYPE)
        {
            processResponse();
        }
        else
        {
            mLog.severe(mTranslator.getString(SEQ_UNSUPPORTED_MEP));
            setState(ServicelistState.ERROR);
        }

        mListBean.updateState(mSequenceId, mState);
        mLog.info(this + mTranslator.getString(SEQ_FINE_EXIT_EXECUTE));
    }

    /**
     * Executes the service with given index in the list.
     *
     * @param index service index
     */
    public void executeService(int index)
    {
        mLog.info(this + mTranslator.getString(SEQ_FINE_EXECUTING_SERVICE)
            + index);

        if (index > (mListBean.getServiceCount() - 1))
        {
            mLog.info(mTranslator.getString(SEQ_FINE_SERVICE_EXECUTION_DONE));
            sendListResponse();

            return;
        }

        try
        {
            mCurrentService = mListBean.getService(index);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(
                    SEQ_SEVERE_CANNOT_GET_SERVICE_INFO));
            e.printStackTrace();
            mLastError =
                new Exception(mTranslator.getString(
                        SEQ_SEVERE_CANNOT_GET_SERVICE_INFO));
            sendListError();

            return;
        }

        MessageExchange req =
            MessageExchangeHelper.createExchange(mCurrentService.getMep());

        try
        {
            if (!MessageExchangeHelper.updateInMessage(req, mLastInputMessage))
            {
                mLog.severe(mTranslator.getString(SEQ_UPDATE_NM_FAILED,
                        mCurrentService.getName()));
                mLastError =
                    new Exception(mTranslator.getString(SEQ_UPDATE_NM_FAILED,
                            mCurrentService.getName()));
                sendListError();

                return;
            }

            req.setOperation(new QName(
                    mCurrentService.getOperationNamespace(),
                    mCurrentService.getOperation()));
            req.setService(new QName(mCurrentService.getNamespace(),
                    mCurrentService.getName()));

            /**
             * The following is the logic an intelligent component adopts to
             * pick an endpoint. First use the service name to identify the
             * endpoint list. Then try  to see if any endpoint name matches
             * the one configured. IF not lucky then try the same process with
             * the interface name. Still not lucky, then our message does not
             * have an address, so cannot send it
             */
            ServiceEndpoint ref =
                getServiceEndpoint(mCurrentService.getNamespace(),
                    mCurrentService.getName(), mCurrentService.getEndpointName());

            if (ref == null)
            {
                ref = getInterfaceEndpoint(mCurrentService
                        .getInterfaceNamespace(),
                        mCurrentService.getInterfaceName(),
                        mCurrentService.getEndpointName());
            }

            /*
             * if ref is null, it means that either the service is
             * not activated
             */
            if (ref == null)
            {
                mLog.info(mTranslator.getString(SEQ_CANNOT_GET_ENDPOINT,
                        mCurrentService.getNamespace()
                        + mCurrentService.getName(),
                        mCurrentService.getEndpointName()));
                mLastError =
                    new Exception(mTranslator.getString(
                            SEQ_CANNOT_GET_ENDPOINT, mCurrentService.getName()));
                sendListError();

                return;
            }

            req.setEndpoint(ref);

            if (!send(req, mCurrentService.getTimeout()))
            {
                mLog.severe(mTranslator.getString(SEQ_SEND_FAILED));
                sendListError();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
         
            /*mLog.severe(mTranslator.getString(SEQ_SEND_FAILED) + " "
                + mCurrentService.getServiceReference().getServiceName()
                                 .toString());
             */
            mLastError = e;

            sendListError();
        }
    }

    /**
     * returns the value of mTimedOut.
     *
     * @return true if the service has timed out
     */
    public boolean hasTimedOut()
    {
        return mTimedOut;
    }

    /**
     * Processes the response.
     */
    public void processResponse()
    {
        mLog.info(this + mTranslator.getString(SEQ_PROCESSING_RESPONSE));

        if (mCurrentExchange.getStatus() == ExchangeStatus.DONE)
        {
            /* terminal condition for service in-out and in-only
             */
            mLog.info(mCurrentExchange.getEndpoint().getServiceName()
                + " DONE ");
            mServiceIndex++;
            executeService(mServiceIndex);

            return;
        }

        if (mCurrentExchange.getStatus() == ExchangeStatus.ERROR)
        {
            mLog.info(mCurrentExchange.getEndpoint().getServiceName()
                + " ERROR ");
            mLastError = mCurrentExchange.getError();
            mLastFaultMessage = mCurrentExchange.getFault();
            sendListError();

            return;
        }

        mLastOutputMessage =
            MessageExchangeHelper.getOutMessage(mCurrentExchange);
        mLastFaultMessage = mCurrentExchange.getFault();

        if (mLastOutputMessage == null)
        {
            if (mLastFaultMessage != null)
            {
                sendListResponse();
            }
            else
            {
                sendListError();
            }
        }
        else
        {
            mLastInputMessage = mLastOutputMessage;
        }

        try
        {
            mCurrentExchange.setStatus(ExchangeStatus.DONE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if (send(mCurrentExchange, mCurrentService.getTimeout()))
        {
            mServiceIndex++;
            executeService(mServiceIndex);
        }
        else
        {
            sendListError();
        }

        mLog.fine(this + "Process response completed");
    }

    /**
     *
     */
    public void resumeTX()
    {
        if (mCurrentTransaction != null)
        {
            try
            {
                mLog.fine(mTranslator.getString(SEQ_RESUME_TX,
                        mCurrentExchange.getExchangeId()));
                SequencingEngineContext.getInstance().getTransactionManager()
                                       .resume(mCurrentTransaction);
            }
            catch (javax.transaction.SystemException se)
            {
                mLog.severe(mTranslator.getString(SEQ_RESUME_TX_FAILED,
                        mCurrentExchange.getExchangeId()));
                se.printStackTrace();
            }
            catch (javax.transaction.InvalidTransactionException ite)
            {
                mLog.severe(mTranslator.getString(SEQ_RESUME_TX_FAILED,
                        mCurrentExchange.getExchangeId()));
                ite.printStackTrace();
            }
        }
    }

    /**
     * sends an error message for list execution.
     */
    public void sendListError()
    {
        mLog.severe(this + "Sending list error");

        if ((mLastError == null) && (mLastFaultMessage == null))
        {
            mLastError = new Exception("Unknown error");
        }

        MessageExchangeHelper.updateMessage(mInboundExchange, null, mLastError,
            mLastFaultMessage);
        send(mInboundExchange, -1);
        setState(ServicelistState.ERROR);
    }

    /**
     * sends the response for the list execution.
     */
    public void sendListResponse()
    {
        mLog.info(this + mTranslator.getString(SEQ_SEND_LIST_RESPONSE));
        mLastError = null;

        if ((mLastInputMessage == null) && (mLastFaultMessage == null))
        {
            mLastError =
                new Exception(mTranslator.getString(SEQ_NULL_OUT_MESSAGE));
        }

        MessageExchangeHelper.updateMessage(mInboundExchange,
            mLastOutputMessage, mLastError, mLastFaultMessage);
        send(mInboundExchange, -1);
        setState(ServicelistState.COMPLETED);
    }

    /**
     * starts the timer.
     *
     * @param timeout timeout , if -1 then timer is not started
     */
    public void startTimer(long timeout)
    {
        mTimer.cancel();
        mTimer = new Timer();
        mTimedOut = false;

        if (timeout != 0)
        {
            mTimer.schedule(new ReminderTask(), timeout);
        }
    }

    /**
     * helper to get the interface endpoint for a service.
     *
     * @param namespace service namespace
     * @param name service name
     * @param epname endpoint name
     *
     * @return ServiceEndpoint
     */
    private ServiceEndpoint getInterfaceEndpoint(
        String namespace,
        String name,
        String epname)
    {
        ServiceEndpoint ep = null;

        try
        {
            ServiceEndpoint [] ref =
                SequencingEngineContext.getInstance().getContext().getEndpoints(new QName(
                        namespace, name));

            if ((ref == null) || (ref.length == 0))
            {
                return null;
            }
            else
            {
                ep = ref[0];

                for (int k = 0; k < ref.length; k++)
                {
                    if (ref[k].getEndpointName().trim().equals(epname.trim()))
                    {
                        ep = ref[k];
                    }
                }
            }
        }
        catch (Exception e)
        {
            mLog.info(mTranslator.getString(SEQ_CANNOT_GET_ENDPOINT, namespace,
                    name));
            e.printStackTrace();

            return null;
        }

        return ep;
    }

    /**
     * helper to get the local endpoint for a service.
     *
     * @param namespace service namespace
     * @param name service name
     * @param epname endpoint name
     *
     * @return ServiceEndpoint
     */
    private ServiceEndpoint getServiceEndpoint(
        String namespace,
        String name,
        String epname)
    {
        ServiceEndpoint ep = null;

        try
        {
            ServiceEndpoint [] ref =
                SequencingEngineContext.getInstance().getContext()
                                       .getEndpointsForService(new QName(
                        namespace, name));

            if ((ref == null) || (ref.length == 0))
            {
                return null;
            }
            else
            {
                ep = ref[0];

                for (int k = 0; k < ref.length; k++)
                {
                    if (ref[k].getEndpointName().trim().equals(epname.trim()))
                    {
                        ep = ref[k];
                    }
                }
            }
        }
        catch (Exception e)
        {
            mLog.info(mTranslator.getString(SEQ_CANNOT_GET_ENDPOINT, namespace,
                    name));
            e.printStackTrace();

            return null;
        }

        return ep;
    }

    /**
     * Processed any request exchanges.
     */
    private void processRequest()
    {
        if (mCurrentExchange.getStatus() == ExchangeStatus.DONE)
        {
            mLog.info(this.toString()
                + mTranslator.getString(SEQ_LIST_COMPLETED,
                    this.mListBean.getServicename()));
            mState = ServicelistState.COMPLETED;
            return;
        }

        if (mCurrentExchange.getStatus() == ExchangeStatus.ERROR)
        {
            mLog.severe(mTranslator.getString(SEQ_LIST_COMPLETED_ERROR,
                    this.mListBean.getServicename()));
            mState = ServicelistState.ERROR;
            return;
        }

        mInboundExchange = mCurrentExchange;
        mLastInputMessage =
            MessageExchangeHelper.getInMessage(mCurrentExchange);

        if (mLastInputMessage == null)
        {
            mLog.severe(mTranslator.getString(SEQ_NO_INPUT_MESSAGE,
                    this.mListBean.getServicename()));
            MessageExchangeHelper.updateMessage(mCurrentExchange, null,
                new Exception(mTranslator.getString(SEQ_NO_INPUT_MESSAGE,
                        this.mListBean.getServicename())), null);
            send(mCurrentExchange, -1);
            mState = ServicelistState.ERROR;

            return;
        }

        executeService(mServiceIndex);
    }

    /**
     * Registers the message exchange.
     *
     * @param msg ME
     */
    private void registerExchange(MessageExchange msg)
    {
        String reqid = (String) msg.getProperty(ConfigData.REQUEST_SEQ_ID);
        String respid = (String) msg.getProperty(ConfigData.RESPONSE_SEQ_ID);

        if (respid != null)
        {
            mMessageRegistry.registerExchange(msg.getExchangeId() + respid, this);
        }
        else if (reqid != null)
        {
            mMessageRegistry.registerExchange(msg.getExchangeId() + reqid, this);
        }
        else
        {
            mLog.severe("Registration info missing");
        }
    }

    /**
     * DeRegisters the message exchange.
     *
     * @param msg ME
     */
    private void deregisterExchange(MessageExchange msg)
    {
        String reqid = (String) msg.getProperty(ConfigData.REQUEST_SEQ_ID);
        String respid = (String) msg.getProperty(ConfigData.RESPONSE_SEQ_ID);

        if (respid != null)
        {
            mMessageRegistry.deregisterExchange(msg.getExchangeId() + respid);
        }
        else if (reqid != null)
        {
            mMessageRegistry.deregisterExchange(msg.getExchangeId() + reqid);
        }
        else
        {
            mLog.severe("Registration info missing");
        }
    }
    /**
     * Resets the timer.
     */
    private void resetTimer()
    {
        if (mTimer != null)
        {
            mTimer.cancel();
        }
    }

    /**
     * Method that sends any message exchnage on the channel.
     *
     * @param msg exchange to be sent
     * @param timeout timeout for that exchange
     *
     * @return true if success else false
     */
    private boolean send(
        MessageExchange msg,
        long timeout)
    {
        /*
         * Send message mSendMessage
         * register in message registry
         * start timer -1 indicates no timer
         */
        if (msg == null)
        {
            mLog.severe(SEQ_NULL_EXCHANGE);

            return false;
        }

        try
        {
            mLog.info(this.toString()
                + mTranslator.getString(SEQ_FINE_SEND_MESSAGE,
                    msg.getEndpoint().getServiceName()));

            if ((msg.getStatus() == ExchangeStatus.DONE)
                    || (msg.getStatus() == ExchangeStatus.ERROR))
            {
                mLog.info(this.toString()
                    + mTranslator.getString(SEQ_FINE_SEND_ACTIVE,
                        msg.getStatus().toString()));
                mResponseExpected = false;
            }
            else
            {
                registerExchange(msg);
                setState(ServicelistState.WAITING);
                mResponseExpected = true;
            }

            mCurrentExchange = msg;
            msg.setProperty(MessageExchange.JTA_TRANSACTION_PROPERTY_NAME,
                mCurrentTransaction);
            resumeTX();
            mChannel.send(msg);
           // mMessageRegistry.dump();            
        }
        catch (MessagingException me)
        {
            mLog.severe(mTranslator.getString(SEQ_SEND_FAILED));
            mLog.severe(me.getMessage());
            mLastError = me;
            me.printStackTrace();
            mResponseExpected = false;
            setState(ServicelistState.ERROR);
            // if you cannot send the message for some reason
            // deregister it             
            deregisterExchange(msg);
            return false;
        }

        if ((mResponseExpected) && (timeout != -1))
        {
            startTimer(timeout);
        }

        return true;
    }

    /**
     * This is a reminder task for the timer.
     *
     * @author Sun Microsystems, Inc.
     */
    public class ReminderTask
        extends TimerTask
    {
        /**
         * run method for the thread.
         */
        public void run()
        {
            /* call some method
             * that will deregister the message from
             * message registry and return an error response
             */
            mTimedOut = true;
            cancelExchange();
        }
    }
}

#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)regress_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for regression tests.

#set this prop to the test domain. default is JBITest
#my_test_domain=domain1

#use global regress setup:
. $SRCROOT/antbld/regress/common_defs.ksh

#override the variable or define other common scripts here
# the following variables are setup by regress scritps
#JBI_DOMAIN_NAME=JBITest
#JBI_ADMIN_HOST=localhost
#JBI_ADMIN_PORT=8687
#JBI_ADMIN_XML=$SRCROOT/ui/src/scripts/jbi_admin.xml
#JBI_DOMAIN_ROOT=$AS8BASE/domains/$JBI_DOMAIN_NAME

#common definitions for sequencing engine  regression tests:

export DEPLOYROOT SEQUENCINGENGINE_BLD_DIR SEQUENCINGENGINE_REGRESS_DIR SEQUENCINGENGINE_SRC_DIR

#this is our current test domain:
DOMAIN_LOG="$JBI_DOMAIN_ROOT/logs/server.log"

if [ -d $SRCROOT/ri-components/file-binding ]; then
    #this is used to pick up the file bindings only.
    DEPLOYROOT=$JV_SRCROOT/ri-components/file-binding/deployments
else
    #this is used to pick up the file bindings only.
    DEPLOYROOT=$JV_SRCROOT/binding/file/deployment
fi
SEQUENCINGENGINE_BLD_DIR=$JV_SVC_TEST_CLASSES
SEQUENCINGENGINE_REGRESS_DIR=$JV_SVC_REGRESS
SEQUENCINGENGINE_SRC_DIR=$JV_SVC_ROOT/src

#this is a local environment variable we use to name the property init file.

export IGNORED_EXCEPTIONS
IGNORED_EXCEPTIONS='java:comp/env|hello|WARNING\|j2ee-appserver1.4'

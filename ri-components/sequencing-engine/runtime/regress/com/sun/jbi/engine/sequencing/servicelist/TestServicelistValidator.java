/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServicelistValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import junit.framework.*;

import org.w3c.dom.Document;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;

import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServicelistValidator extends TestCase
{
    /**
     *    
     */
    private ServicelistValidator mValidator;

    /**
     * Creates a new TestServicelistValidator object.
     *
     * @param testName
     */
    public TestServicelistValidator(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestServicelistValidator.class);

        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
        String srcroot = System.getProperty("junit.srcroot");
        mValidator =
            new ServicelistValidator(srcroot
                + "/engine/sequencing/schema/servicelist.xsd",
                srcroot + "/engine/sequencing/deploy/artifact/servicelist.xml");
    }

    /**
     * Test of error method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistValidator.
     */
    public void testError()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of fatalError method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistValidator.
     */
    public void testFatalError()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getDocument method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistValidator.
     */
    public void testGetDocument()
    {
        // Add your test code below by replacing the default call to fail.
        System.out.println("testGetDocument");
        mValidator.validate();
        assertTrue("Failed validating XML config file", mValidator.isValid());
        assertNotNull("Document object is null", mValidator.getDocument());
    }

    /**
     * Test of getError method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistValidator.
     */
    public void testGetError()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of isValid method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistValidator.
     */
    public void testIsValid()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setValidating method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistValidator.
     */
    public void testSetValidating()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of validate method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistValidator.
     */
    public void testValidate()
    {
        // Add your test code below by replacing the default call to fail.
        System.out.println("testValidate");
        mValidator.validate();
        assertTrue("Failed validating XML config file", mValidator.isValid());
    }

    /**
     * Test of warning method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServicelistValidator.
     */
    public void testWarning()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}

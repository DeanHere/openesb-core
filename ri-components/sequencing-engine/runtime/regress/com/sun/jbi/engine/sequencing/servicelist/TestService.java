/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import com.sun.jbi.engine.sequencing.servicelist.ServiceBean;

import junit.framework.*;

import java.util.HashMap;

import javax.jbi.messaging.NormalizedMessage;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestService extends TestCase
{
    /**
     * Creates a new TestService object.
     *
     * @param testName
     */
    public TestService(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestService.class);

        return suite;
    }

    /**
     * Test of execute method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testExecute()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getAttribute method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testGetAttribute()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getOutputMessage method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testGetOutputMessage()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testGetService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServiceName method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testGetServiceName()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServiceType method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testGetServiceType()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getState method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testGetState()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setAttribute method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testSetAttribute()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setInputMessage method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testSetInputMessage()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setService method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testSetService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setServiceType method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testSetServiceType()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of terminate method, of class
     * com.sun.jbi.engine.sequencing.servicelist.Service.
     */
    public void testTerminate()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}

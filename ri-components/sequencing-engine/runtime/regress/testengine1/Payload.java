/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Payload.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package testengine1;

import org.w3c.dom.Document;

import java.io.StringReader;

import javax.jbi.messaging.NormalizedMessage;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;


/**
 * Simple utility class used to load a dummy DOM document.
 *
 * @author Sun Microsystems, Inc.
 */
public class Payload
{
    /**
     *    
     */
    public static String mDefaultPath;

    /**
     *    
     */
    private static Document mDefaultDoc;

    /**
     *    
     */
    private static DocumentBuilder mBuilder;

    /**
     *
     *
     * @param msg  NOT YET DOCUMENTED
     *
     * @throws Exception    
     */
    public static synchronized void setBadPayload(NormalizedMessage msg)
        throws Exception
    {
        try
        {
            StringReader reader =
                new StringReader("<xmlstart>aim not a good xml <ddd>");
            msg.setContent(new StreamSource(reader));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     *
     *
     * @param msg  NOT YET DOCUMENTED
     *
     * @throws Exception    
     */
    public static synchronized void setPayload(NormalizedMessage msg)
        throws Exception
    {
        if (mBuilder == null)
        {
            init();
        }

        msg.setContent(new DOMSource(mDefaultDoc));
    }

    /**
     *
     *
     * @throws Exception    
     */
    private static void init() throws Exception
    {
        DocumentBuilderFactory factory;

        factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        mBuilder = factory.newDocumentBuilder();
        mDefaultDoc = mBuilder.parse(mDefaultPath);
    }
}

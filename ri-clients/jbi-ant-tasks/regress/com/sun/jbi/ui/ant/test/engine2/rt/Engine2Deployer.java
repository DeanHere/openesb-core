/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Engine2Deployer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant.test.engine2.rt;

import com.sun.jbi.management.ComponentMessageHolder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import javax.jbi.component.ServiceUnitManager;
import javax.jbi.management.DeploymentException;
/**
 * fix it
 * @author Sun Microsystems, Inc.
 */
public class Engine2Deployer implements ServiceUnitManager
{
    /**
     * fix it
     */    
    private Engine2Runtime mContext;
    /**
     * fix it
     */    
    private Map mDeployMap;
    
    /**
     * Creates a new instance of Binding1Deployer
     * @param ctx fix it
     */
    public Engine2Deployer(Engine2Runtime ctx)
    {
        this.mDeployMap = new HashMap();
        
        this.mContext = ctx;
        
        
    }
    
    /**
     * fix it
     * @param asaId fix it
     * @param asaJarPath fix it
     * @throws DeploymentException fix it
     * @return fix it
     */    
    public String deploy(String asaId, String asaJarPath) throws DeploymentException
    {
        // this.mContext.getLogger().log(Level.INFO, "");
        System.out.println("ANT_ENGINE2: Deploying " + asaId);
        
        String asaInfo = (String) this.mDeployMap.get(asaId);
        if ( asaInfo != null  )
        {
            // throw already exists
            String exMsg = createExceptionMessage(
            this.mContext.getId(), "deploy", "FAILED",
            "ANT_E1_0001",
            asaId,
            "ASA " + asaId + " Already Exists",
            null);
            throw new DeploymentException(exMsg);
        } else
        {
            this.mDeployMap.put(asaId, asaJarPath);
            // return "Deployed " + asaId;
            try
            {
                ComponentMessageHolder compMsgHolder = new ComponentMessageHolder("STATUS_MSG");
                compMsgHolder.setComponentName (this.mContext.getId());
                compMsgHolder.setTaskName ("deploy");
                compMsgHolder.setTaskResult ("SUCCESS");
            
                 return this.mContext.getMgmtMsgBuilder().buildComponentMessage(compMsgHolder);
                 
                /*
                return this.mContext.getMgmtMsgBuilder().buildComponentTaskStatusMessage(
                this.mContext.getId(),
                "deploy","SUCCESS", null
                );
                 */
            } catch (Exception ex)
            {
                ex.printStackTrace();
                return null;
            }
            
        }
    }
    
    /**
     * fix it
     * @param asaId fix it
     * @return fix it
     */    
    public String getDeploymentInfo(String asaId)
    {
        // Should throw exception if asaId not exists
        return (String) this.mDeployMap.get(asaId);
    }
    
    /**
     * fix it
     * @return fix it
     */    
    public String[] getDeployments()
    {
        return
        (String[]) this.mDeployMap.keySet().toArray(new String[0]);
    }
    
    /**
     * fix it
     * @param asaId fix it
     * @return fix it
     */    
    public boolean isDeployed(String asaId)
    {
        return (this.mDeployMap.get(asaId) != null);
    }
    
    /**
     * fix it
     * @param asaId fix it
     * @throws DeploymentException fix it
     * @return fix it
     */    
    public String undeploy(String asaId, String asaJarPath) throws DeploymentException
    {
        String asaInfo = (String) this.mDeployMap.remove(asaId);
        if ( asaInfo == null )
        {
            // throw exception
            String exMsg = createExceptionMessage(
            this.mContext.getId(), "undeploy", "FAILED",
            "ANT_E1_0002",
            asaId,
            "ASA with " + asaId + " Not Found",
            null);
            throw new DeploymentException(exMsg);
        } else
        {
            // return "Undeployed " + asaId;
            try
            {
                ComponentMessageHolder compMsgHolder = new ComponentMessageHolder("STATUS_MSG");
                compMsgHolder.setComponentName (this.mContext.getId());
                compMsgHolder.setTaskName ("undeploy");
                compMsgHolder.setTaskResult ("SUCCESS");
            
                return this.mContext.getMgmtMsgBuilder().buildComponentMessage(compMsgHolder);
                
                /*
                return this.mContext.getMgmtMsgBuilder().buildComponentTaskStatusMessage(
                this.mContext.getId(),
                "undeploy","SUCCESS", null
                );
                 */
            } catch (Exception ex)
            {
                ex.printStackTrace();
                return null;
            }
            
        }
    }
    
    /**
     * Initialize the deployment. This is the first phase of a two-phase
     * start, where the component must prepare to receive service requests
     * related to the deployment (if any).
     * @param serviceUnitName the name of the Service Unit being initialized.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not deployed, or is in an incorrect state.
     */
    public void init(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
    }

    /**
     * Shut down the deployment. This causes the deployment to return to the
     * state it was in after <code>deploy()</code> and before <code>init()</code>.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void shutDown(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
    }

    /**
     * Start the deployment. This is the second phase of a two-phase start,
     * where the component can now initiate service requests related to the
     * deployment.
     * @param serviceUnitName the name of the Service Unit being started.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void start(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
    }

    /**
     * Stop the deployment. This causes the component to cease generating
     * service requests related to the deployment. This returns the deployment
     * to a state equivalent to after <code>init()</code> was called.
     * @param serviceUnitName the name of the Service Unit being stopped.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void stop(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
    }

    
    /**
     * helper method to create XML exception string.
     *
     * @param compid Component id.
     * @param oper operation like deploy or undeploy
     * @param status success r failure
     * @param loctoken some failure string token like SEQ_300001
     * @param locparam parameters for error message.
     * @param locmessage error message.
     * @param stckTrElem stack trace of exception.
     *
     * @return XML string.
     */
    private String createExceptionMessage(String compid, String oper, String status,
    String loctoken, String locparam, String locmessage,
    Throwable exObj)
    {
        
        String[] locParams = new String[1];
        locParams[0] = locparam;
        
        ComponentMessageHolder msgMap = new ComponentMessageHolder("EXCEPTION_MSG");
        
        msgMap.setComponentName (compid);
        msgMap.setTaskName (oper);
        msgMap.setTaskResult (status);
        msgMap.setLocToken (1, loctoken);
        msgMap.setLocParam (1, locParams);
        msgMap.setLocMessage (1, locmessage);
        msgMap.setExceptionObject (exObj);
        
        /*
        msgMap.put("COMPONENTID", compid);
        msgMap.put("TASKIDENTIFIER", oper);
        msgMap.put("TASKRSLTSTATUS", status);
        msgMap.put("COMPONENTEXLOCTOKEN", loctoken);
        msgMap.put("COMPONENTEXLOCPARAM", locparam);
        msgMap.put("COMPONENTEXLOCMESSAGE", locmessage);
        msgMap.put("COMPONENTEXSTACKTRACE", sb.toString());
        */
         
        String retMsg = null;
        
        try
        {
            retMsg = this.mContext.getMgmtMsgBuilder()
            .buildComponentMessage(msgMap);
        }
        catch (Exception e)
        {
            this.mContext.getLogger().log(Level.INFO, "Failed to create Mgmt Msg", e);
        }
        
        return retMsg;
    }
    
}

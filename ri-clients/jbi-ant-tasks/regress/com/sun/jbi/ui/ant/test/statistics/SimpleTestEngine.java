/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SimpleTestEngine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant.test.statistics;

import java.io.InputStream;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;
import java.util.Date;
import java.util.Properties;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;
import javax.xml.namespace.QName;
import javax.management.StandardMBean;
import javax.management.ObjectName;
import javax.jbi.servicedesc.ServiceEndpoint;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.MessagingException;  
import javax.jbi.messaging.DeliveryChannel;  
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.NormalizedMessage;
import javax.xml.transform.Source;
import java.io.StringReader;
import org.xml.sax.InputSource;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.jbi.messaging.InOut;
import javax.xml.transform.dom.DOMSource;


/**
 * Simple Component used to test Statistics.
 */
public class SimpleTestEngine implements 
    Component, ComponentLifeCycle, ServiceUnitManager, Bootstrap
{
 
    
   /**
     * Local copy of the component name.
     */
    protected String mComponentName;

    /**
     * Type of component.
     */
    protected String mComponentType = "Engine";

    /**
     * Local handle to the ComponentContext.
     */
    protected ComponentContext mContext;

    /**
     * Logger instance.
     */
    protected Logger mLog = Logger.getLogger("com.sun.jbi.management");
    
    /**
     * Component configuration
     */
    private Properties mConfig = new Properties();
    
    /**
     * delivery channel
     */
    private DeliveryChannel mChannel;
    
    /**
     * message exchange factory
     */
    private MessageExchangeFactory mFactory;    
    
    /**
     *    mReq
     */
    private MessageExchange mReq;    
    

    /**
     * flag that controls this engine's lifecycle
     */
    private boolean mRunFlag = true;    
    
 
    
    //
    // Component methods
    //

    /**
     * Get the ComponentLifeCycle implementation instance for this Binding
     * Component.
     * @return the life cycle implementation instance.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        mLog.info(mComponentType + " getLifeCycle called");
        return this;
    }
 
    /**
     * Get the ServiceUnitManager implementation instance for this Binding
     * Component.
     * @return the Service Unit manager implementation instance.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        mLog.info(mComponentType + " " + mComponentName +
            " getServiceUnitManager called");
        return this;
    }

    /**
     * Resolve descriptor details for the specified reference, which is for a
     * service provided by this component.
     * @param ref the endpoint reference to be resolved.
     * @return the description for the specified reference.
     */
    public org.w3c.dom.Document getServiceDescription(
        javax.jbi.servicedesc.ServiceEndpoint ref)
    {
        mLog.info(mComponentType + " " + mComponentName +
            " getServiceDescription called");
        return null;
    }
    
    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually 
     * perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        mLog.info(mComponentType + " " + mComponentName +
            " isExchangeWithConsumerOkay called");
        return true;
    }
    
    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually 
     * interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        mLog.info(mComponentType + " " + mComponentName +
            " isExchangeWithProviderOkay called");
        return true;
    }

    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public javax.jbi.servicedesc.ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment epr)
    {
        mLog.info(mComponentType + " " + mComponentName +
            " resolveEndpointReference called");
        return null;
    }

    //
    // ComponentLifeCycle methods
    //

    /**
     * Initialize the Binding Component.
     * @param context the JBI component context created by the JBI framework.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void init(ComponentContext context)
        throws javax.jbi.JBIException
    {
        mComponentName = context.getComponentName();
        mContext = context;
    }

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. This
     * implementation always returns null.
     * @return javax.management.ObjectName is always null.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        mLog.info(mComponentType + " " +
            " getExtensionMBeanName called");
        return null;
    }

    /**
     * Start the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void start()
        throws javax.jbi.JBIException
    {
        mLog.info(mComponentType + " " + mComponentName + " start called");
        
        /** send some test messages */
        mLog.info("Test Engine message processing begins....");
        try
        {
            
            String inputString =
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                    "<jbi:message version=\"1.0\" " +
                    "xmlns:jbi=\"http://java.sun.com/xml/ns/jbi/wsdl-11-wrapper\"  " +
                    "xmlns:ping=\"http://ping\" " +  
                    "name=\"pingIn\" " +
                    "type=\"ping:ping-message\"> " +
                    "<jbi:part> " +
                    "<ping:pinginput><ping:test-string>Hello World</ping:test-string></ping:pinginput>" +
                    "</jbi:part> " +
                    "</jbi:message>";


            
            mChannel = mContext.getDeliveryChannel();
            mFactory = mChannel.createExchangeFactory();
    
            String serviceName = "PingService";
            String serviceNamespace = "http://ping";
            String endpointName = "ConsumingPingPort";

            QName service =
                new QName(serviceNamespace, serviceName);
            
            ServiceEndpoint serviceEndpoint = 
                mContext.getEndpoint(service, endpointName);
                  
            QName operation = 
                    new QName("http://ping", "ping");
            
            

            DeliveryChannel channel = mChannel;
            MessageExchangeFactory factory =  channel.createExchangeFactory(serviceEndpoint);
            InOut inOutME = factory.createInOutExchange();
            inOutME.setOperation(operation);
            NormalizedMessage inMsg = inOutME.createMessage();
            inOutME.setInMessage(inMsg);

  
            // set the content of the IN normalized message ( Normalize the message )
            StringReader xmlReader = new StringReader(inputString);
            DocumentBuilderFactory docBuilderFactory =
                DocumentBuilderFactory.newInstance();
            docBuilderFactory.setValidating(false);
            DocumentBuilder docBuilder =
                docBuilderFactory.newDocumentBuilder();
            InputSource is = new InputSource(xmlReader);
            Source inMsgSource = new DOMSource(docBuilder.parse(is));
            
            inMsg.setContent(inMsgSource);            
            

            // send the message exchange and wait for response
            boolean isSent = channel.sendSync(inOutME, 60000);
            if ( !isSent ) {
                throw new Exception("SimpleTestEngine:Timeout occurred ");
            }
            mLog.info("Sent message successfully");
           
        }            
        catch (Exception me)
        {
            me.printStackTrace();
        }
       
    }

    /**
     * Stop the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        mRunFlag = false;
        mLog.info(mComponentType + " " + mComponentName + " stop called");
    }

    /**
     * Shut down the Binding Component.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mLog.info(mComponentType + " " + mComponentName + " shutDown called");
    }

    //
    // ServiceUnitManager methods
    //
 
    /**
     * Deploy a Service Unit.
     * @param serviceUnitName the name of the Service Unit being deployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return a deployment status message.
     * @throws javax.jbi.management.DeploymentException if the deployment
     * operation is unsuccessful.
     */
    public String deploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
               
        mLog.info(mComponentType + " " + mComponentName +
            " deployed Service Unit " + serviceUnitName + " with serviceUnitRootPath "
                + serviceUnitRootPath);
        return createDeployResult("deploy", true);
    }

    /**
     * Initialize the deployment.
     * @param serviceUnitName the name of the Service Unit being initialized.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not deployed, or is in an incorrect state.
     */
    public void init(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info(mComponentType + " " + mComponentName +
            " initialized Service Unit " + serviceUnitName + " with serviceUnitRootPath "
                + serviceUnitRootPath);
        
    }

    /**
     * Shut down the deployment.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void shutDown(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info(mComponentType + " " + mComponentName +
            " shut down Service Unit " + serviceUnitName);
    }

    /**
     * Start the deployment.
     * @param serviceUnitName the name of the Service Unit being started.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void start(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        
        mLog.info(mComponentType + " " + mComponentName +
            " started Service Unit " + serviceUnitName);
    }

    /**
     * Stop the deployment.
     * @param serviceUnitName the name of the Service Unit being stopped.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    public void stop(String serviceUnitName)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info(mComponentType + " " + mComponentName +
            " stopped Service Unit " + serviceUnitName);
    }

    /**
     * Undeploy a Service Unit from the component.
     * @param serviceUnitName the name of the Service Unit being undeployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @return an undeployment status message.
     * @throws javax.jbi.management.DeploymentException if the undeployment
     * operation is unsuccessful.
     */
    public String undeploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException
    {
        mLog.info(mComponentType + " " + mComponentName +
            " undeployed Service Unit " + serviceUnitName);
        return createDeployResult("undeploy", true);
    }

    public void onUninstall() 
        throws javax.jbi.JBIException
    {
    }

    public void onInstall() throws javax.jbi.JBIException
    {
        
    }

    public void init(javax.jbi.component.InstallationContext installationContext)
        throws javax.jbi.JBIException
    {
        
    }

    public void cleanUp() throws javax.jbi.JBIException
    {
        
    }
    
    /** Creates a (un)deployment result string.
     *  @param task 'deploy' or 'undeploy'
     */
    private String createDeployResult(String task, boolean isSuccess)
    {
        return "<component-task-result xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">"
            + "<component-name>" + mComponentName + "</component-name>"
            + "<component-task-result-details>"
            + "<task-result-details>"
            + "<task-id>" + task + "</task-id>"
            + "<task-result>" + (isSuccess ? "SUCCESS" : "FAILED") + "</task-result>"
            + "</task-result-details>"
            + "</component-task-result-details>"
            + "</component-task-result>";
    }
    
    
}

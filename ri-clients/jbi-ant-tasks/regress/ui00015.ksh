#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00015.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# Test for list-statistics
####

echo "ui00015 : jbi-list-statistics Ant Task."

. ./regress_defs.ksh

echo prepare the artifacts
ant -q  -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f ui00015.xml

SA_ARCHIVE=${JV_SVC_BLD}/regress/dist/ping-sa.jar
SA_NAME=PingApp
COMPONENT_ARCHIVE=${JV_SVC_BLD}/regress/dist/simpletestengine.jar
COMPONENT_NAME=SimpleTestEngine

run_test_against_server()
{

$JBI_ANT -Djbi.component.name=sun-http-binding start-component
$JBI_ANT -Djbi.deploy.file=$SA_ARCHIVE  deploy-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME start-service-assembly
$JBI_ANT -Djbi.install.file=$COMPONENT_ARCHIVE  install-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME start-component

$JBI_ANT -Djbi.task.fail.on.error=true list-nmr-statistics
$JBI_ANT -Djbi.task.fail.on.error=true list-framework-statistics
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=${COMPONENT_NAME} list-component-statistics
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.service.assembly.name=${SA_NAME} list-service-assembly-statistics
$JBI_ANT -propertyfile ${JV_SVC_BLD}/regress/testdata/test-statistics/endpoint.properties -Djbi.task.fail.on.error=true list-endpoint-statistics
$JBI_ANT -propertyfile ${JV_SVC_BLD}/regress/testdata/test-statistics/consumerendpoint.properties -Djbi.task.fail.on.error=true list-endpoint-statistics

$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.config.param.name=msgSvcTimingStatisticsEnabled -Djbi.config.param.value=true set-runtime-configuration
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME stop-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME start-component

$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=${COMPONENT_NAME} list-component-statistics
$JBI_ANT -propertyfile ${JV_SVC_BLD}/regress/testdata/test-statistics/endpoint.properties -Djbi.task.fail.on.error=true list-endpoint-statistics
$JBI_ANT -propertyfile ${JV_SVC_BLD}/regress/testdata/test-statistics/consumerendpoint.properties -Djbi.task.fail.on.error=true list-endpoint-statistics

$JBI_ANT -Djbi.service.assembly.name=$SA_NAME stop-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME shut-down-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_NAME undeploy-service-assembly
$JBI_ANT -Djbi.component.name=sun-http-binding shut-down-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME stop-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME shut-down-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME uninstall-component
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.config.param.name=msgSvcTimingStatisticsEnabled -Djbi.config.param.value=false set-runtime-configuration
}

run_test()
{

$JBI_ANT -Djbi.target=$TARGET -Djbi.component.name=sun-http-binding start-component
$JBI_ANT -Djbi.target=$TARGET -Djbi.deploy.file=$SA_ARCHIVE  deploy-service-assembly
$JBI_ANT -Djbi.target=$TARGET -Djbi.service.assembly.name=$SA_NAME start-service-assembly
$JBI_ANT -Djbi.target=$TARGET -Djbi.install.file=$COMPONENT_ARCHIVE  install-component
$JBI_ANT -Djbi.target=$TARGET -Djbi.component.name=$COMPONENT_NAME start-component

$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.target=$TARGET list-nmr-statistics
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.target=$TARGET list-framework-statistics
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.target=$TARGET -Djbi.component.name=${COMPONENT_NAME} list-component-statistics
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.target=$TARGET -Djbi.service.assembly.name=${SA_NAME} list-service-assembly-statistics
$JBI_ANT  -propertyfile ${JV_SVC_BLD}/regress/testdata/test-statistics/endpoint.properties -Djbi.task.fail.on.error=true -Djbi.target=$TARGET list-endpoint-statistics
$JBI_ANT  -propertyfile ${JV_SVC_BLD}/regress/testdata/test-statistics/consumerendpoint.properties -Djbi.task.fail.on.error=true -Djbi.target=$TARGET list-endpoint-statistics

$JBI_ANT -Djbi.target=$TARGET -Djbi.task.fail.on.error=true -Djbi.config.param.name=msgSvcTimingStatisticsEnabled -Djbi.config.param.value=true set-runtime-configuration
$JBI_ANT -Djbi.target=$TARGET -Djbi.component.name=$COMPONENT_NAME stop-component
$JBI_ANT -Djbi.target=$TARGET -Djbi.component.name=$COMPONENT_NAME start-component

$JBI_ANT -Djbi.target=$TARGET -Djbi.task.fail.on.error=true -Djbi.component.name=${COMPONENT_NAME} list-component-statistics
$JBI_ANT -Djbi.target=$TARGET -propertyfile ${JV_SVC_BLD}/regress/testdata/test-statistics/endpoint.properties -Djbi.task.fail.on.error=true list-endpoint-statistics
$JBI_ANT -Djbi.target=$TARGET -propertyfile ${JV_SVC_BLD}/regress/testdata/test-statistics/consumerendpoint.properties -Djbi.task.fail.on.error=true list-endpoint-statistics

$JBI_ANT -Djbi.target=$TARGET -Djbi.service.assembly.name=$SA_NAME stop-service-assembly
$JBI_ANT -Djbi.target=$TARGET -Djbi.service.assembly.name=$SA_NAME shut-down-service-assembly
$JBI_ANT -Djbi.target=$TARGET -Djbi.service.assembly.name=$SA_NAME undeploy-service-assembly
$JBI_ANT -Djbi.target=$TARGET -Djbi.component.name=sun-http-binding shut-down-component
$JBI_ANT -Djbi.target=$TARGET -Djbi.component.name=$COMPONENT_NAME stop-component
$JBI_ANT -Djbi.target=$TARGET -Djbi.component.name=$COMPONENT_NAME shut-down-component
$JBI_ANT -Djbi.target=$TARGET -Djbi.component.name=$COMPONENT_NAME uninstall-component

$JBI_ANT -Djbi.target=$TARGET -Djbi.task.fail.on.error=true -Djbi.config.param.name=msgSvcTimingStatisticsEnabled -Djbi.config.param.value=false set-runtime-configuration
}

run_test_against_server

# Run against cluster
TARGET=ant_cluster
run_test

# Run against standalone instance
TARGET=instance13
run_test

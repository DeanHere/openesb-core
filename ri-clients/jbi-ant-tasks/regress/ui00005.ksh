#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00005.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

JBI_ANT="$JBI_ANT -Djbi.task.fail.on.error=false"
echo $JBI_ANT

#
# test list target options
#

test_list_target_options()
{

### list all objects. all lists should be empty  ###############################
$JBI_ANT list-shared-libraries
$JBI_ANT list-service-engines
$JBI_ANT list-binding-components
$JBI_ANT list-service-assemblies

### install components/slibs ###################################################
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-sns1.jar install-shared-library
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-sns2.jar install-shared-library
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-binding1.jar  install-component
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-binding2.jar  install-component
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-engine1.jar  install-component
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-engine2.jar -Djbi.install.params.file=$JV_SVC_REGRESS/params.properties install-component

### change the components states ###############################################
### engine1,binding1=started, binding2=stoped, engine2=shutdown ################
$JBI_ANT -Djbi.component.name="ant_test_engine1" start-component
$JBI_ANT -Djbi.component.name="ant_test_binding1" start-component
$JBI_ANT -Djbi.component.name="ant_test_binding2" start-component
$JBI_ANT -Djbi.component.name="ant_test_binding2" stop-component

### deploy service assembly ####################################################
$JBI_ANT -Djbi.deploy.file=$UI_REGRESS_DIST_DIR/ant-test-au1.zip deploy-service-assembly

### list tasks with state option ###############################################
$JBI_ANT -Djbi.component.state=started list-service-engines
$JBI_ANT -Djbi.component.state=stopped list-service-engines
$JBI_ANT -Djbi.component.state=shutdown list-service-engines

$JBI_ANT -Djbi.component.state=started list-binding-components
$JBI_ANT -Djbi.component.state=stopped list-binding-components
$JBI_ANT -Djbi.component.state=shutdown list-binding-components

$JBI_ANT -Djbi.service.assembly.state=started list-service-assemblies
$JBI_ANT -Djbi.service.assembly.state=stopped list-service-assemblies
$JBI_ANT -Djbi.service.assembly.state=shutdown list-service-assemblies

### list tasks with single object option #######################################
$JBI_ANT -Djbi.show.descriptor=true -Djbi.shared.library.name=ant_test_sns1 list-shared-libraries
$JBI_ANT -Djbi.show.descriptor=true -Djbi.service.engine.name=ant_test_engine1 list-service-engines
$JBI_ANT -Djbi.show.descriptor=true -Djbi.binding.component.name=ant_test_binding1 list-binding-components
$JBI_ANT -Djbi.show.descriptor=true -Djbi.service.assembly.name=ant_test_assembly_unit_1 list-service-assemblies

### test for other options #####################################################
### list slibs on which a particular comp is dependent #########################
$JBI_ANT -Djbi.component.name=ant_test_binding2 list-shared-libraries
### list assembly on which a particular comp ###################################
$JBI_ANT -Djbi.component.name=ant_test_binding2 list-service-assemblies
### list comps with slib dependency ############################################
$JBI_ANT -Djbi.shared.library.name=ant_test_sns1 list-service-engines
$JBI_ANT -Djbi.shared.library.name=ant_test_sns1 list-binding-components
### list comps with assembly dependency ########################################
$JBI_ANT -Djbi.service.assembly.name=ant_test_assembly_unit_1 list-service-engines
$JBI_ANT -Djbi.service.assembly.name=ant_test_assembly_unit_1 list-binding-components

### undeploy service assembly ##################################################
$JBI_ANT -Djbi.service.assembly.name="ant_test_assembly_unit_1" undeploy-service-assembly

### stop component #############################################################
$JBI_ANT -Djbi.component.name="ant_test_binding1" stop-component
$JBI_ANT -Djbi.component.name="ant_test_engine1" stop-component

### shutdown component #############################################################
$JBI_ANT -Djbi.component.name="ant_test_binding1" shut-down-component
$JBI_ANT -Djbi.component.name="ant_test_binding2" shut-down-component
$JBI_ANT -Djbi.component.name="ant_test_engine1" shut-down-component
$JBI_ANT -Djbi.component.name="ant_test_engine2" shut-down-component

### uninstall components #######################################################
$JBI_ANT -Djbi.component.name=ant_test_engine2 uninstall-component
$JBI_ANT -Djbi.component.name=ant_test_engine1 uninstall-component
$JBI_ANT -Djbi.component.name=ant_test_binding2 uninstall-component
$JBI_ANT -Djbi.component.name=ant_test_binding1 uninstall-component
$JBI_ANT -Djbi.shared.library.name="ant_test_sns2" uninstall-shared-library
$JBI_ANT -Djbi.shared.library.name="ant_test_sns1" uninstall-shared-library

### list all objects. all lists should be empty ################################
$JBI_ANT list-shared-libraries
$JBI_ANT list-service-engines
$JBI_ANT list-binding-components
$JBI_ANT list-service-assemblies

}


run_test()
{
build_test_artifacts
test_list_target_options
}

################## MAIN ##################
####
# Execute the test
####

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test

exit 0

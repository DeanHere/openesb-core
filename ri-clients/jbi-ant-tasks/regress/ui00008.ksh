#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00008.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

JBI_ANT="$JBI_ANT -Djbi.task.fail.on.error=false"
echo $JBI_ANT

#
# test components
#

test_keep_archive_attrib()
{

### list all objects. all lists should be empty  ###############################
$JBI_ANT list-shared-libraries
$JBI_ANT list-service-engines
$JBI_ANT list-binding-components
$JBI_ANT list-service-assemblies

### install components/slibs ###################################################
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-sns1.jar install-shared-library
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-binding1.jar  install-component
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-engine1.jar  install-component

### list components should be in shutdown state. ###############################
$JBI_ANT list-shared-libraries
$JBI_ANT list-service-engines
$JBI_ANT list-binding-components

### start components ###########################################################
$JBI_ANT -Djbi.component.name="ant_test_engine1" start-component
$JBI_ANT -Djbi.component.name="ant_test_binding1" start-component

### list components should be in started state. ################################
$JBI_ANT list-service-engines
$JBI_ANT list-binding-components

### deploy service assembly ####################################################
$JBI_ANT -Djbi.deploy.file=$UI_REGRESS_DIST_DIR/ant-test-au1.zip deploy-service-assembly

### list service assembly. should be in shutdown state #########################
$JBI_ANT list-service-assemblies

### undeploy service assembly ##################################################
$JBI_ANT -Djbi.service.assembly.name="ant_test_assembly_unit_1" -Djbi.keep.archive=true undeploy-service-assembly

### stop component #############################################################
$JBI_ANT -Djbi.component.name="ant_test_binding1" stop-component
$JBI_ANT -Djbi.component.name="ant_test_engine1" stop-component

### shutdown component #############################################################
$JBI_ANT -Djbi.component.name="ant_test_binding1" shut-down-component
$JBI_ANT -Djbi.component.name="ant_test_engine1" shut-down-component

### list components should be in shutdown state. ###############################
$JBI_ANT list-service-engines
$JBI_ANT list-binding-components

### uninstall components #######################################################
$JBI_ANT -Djbi.component.name=ant_test_engine1 -Djbi.keep.archive=true uninstall-component
$JBI_ANT -Djbi.component.name=ant_test_binding1 -Djbi.keep.archive=true uninstall-component
$JBI_ANT -Djbi.shared.library.name="ant_test_sns1" -Djbi.keep.archive=true uninstall-shared-library

### list all objects. all lists should be empty for server #####################
$JBI_ANT list-shared-libraries
$JBI_ANT list-service-engines
$JBI_ANT list-binding-components
$JBI_ANT list-service-assemblies

### all objects should exist in domain #########################################
$JBI_ANT -Djbi.target=domain list-shared-libraries
$JBI_ANT -Djbi.target=domain list-service-engines
$JBI_ANT -Djbi.target=domain list-binding-components
$JBI_ANT -Djbi.target=domain list-service-assemblies

### uninstall sa/slib/components from domain ###################################
$JBI_ANT -Djbi.service.assembly.name="ant_test_assembly_unit_1" -Djbi.target=domain undeploy-service-assembly
$JBI_ANT -Djbi.component.name=ant_test_engine1 -Djbi.target=domain uninstall-component
$JBI_ANT -Djbi.component.name=ant_test_binding1 -Djbi.target=domain uninstall-component
$JBI_ANT -Djbi.shared.library.name=ant_test_sns1 -Djbi.target=domain uninstall-shared-library

### all lists should be empty for domain #######################################
$JBI_ANT -Djbi.target=domain list-shared-libraries
$JBI_ANT -Djbi.target=domain list-service-engines
$JBI_ANT -Djbi.target=domain list-binding-components
$JBI_ANT -Djbi.target=domain list-service-assemblies


}


run_test()
{
build_test_artifacts
test_keep_archive_attrib
}

################## MAIN ##################
####
# Execute the test
####

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test | tr -d '\r' | sed -e '/^$/d'

exit 0

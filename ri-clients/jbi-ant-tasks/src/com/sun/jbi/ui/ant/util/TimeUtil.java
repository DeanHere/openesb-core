/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)I18NEchoTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant.util;

public class TimeUtil
{
    /* Constants */
    private static final long DAY_BASE = 24*60*60*1000;
    private static final long HOUR_BASE = 60*60*1000;
    private static final long MIN_BASE = 60*1000;
    private static final double SEC_BASE = 1000f;

    /* Private members */
    private int mDays = 0;
    private int mHours = 0;
    private int mMins = 0;
    private double mSecs = 0;

    /*
     * public constructor
     */
    public TimeUtil(long theMs)
    {
        mDays = (int) (theMs/DAY_BASE);
        mHours = (int) ((theMs - mDays*DAY_BASE)/HOUR_BASE);
        mMins = (int) ((theMs - mDays*DAY_BASE - mHours*HOUR_BASE)/MIN_BASE);
        mSecs = ((theMs - mDays*DAY_BASE - mHours*HOUR_BASE - mMins*MIN_BASE)/SEC_BASE);
    }

    /**
     * Get the days portion
     * @return the days
     */
    public int getDays()
    {
        return mDays;
    }

    /**
     * Get the hours portion
     * @return the hours
     */
    public int getHours()
    {
        return mHours;
    }

    /**
     * Get the minutes portion
     * @return the minutes
     */
    public int getMins()
    {
        return mMins;
    }

    /**
     * Get the seconds portion
     * @return the seconds
     */
    public double getSecs()
    {
        return mSecs;
    }

    public static void main(String [] argv)
    {
        long theLong = 2005;
        TimeUtil inst = new TimeUtil(theLong);
        System.out.format("Days: %1d Hours: %2d Mins: %3d Secs: %4f\n", inst.getDays(), inst.getHours(), inst.getMins(), inst.getSecs());
   }
}

/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
package com.sun.jbi.jsf.util;

import com.sun.jbi.jsf.bean.JBIConfigBean;
import com.sun.jbi.jsf.bean.ListBean;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.common.ServiceUnitInfo;
import com.sun.jsftemplating.component.factory.tree.DynamicTreeNodeFactory;
import com.sun.jsftemplating.component.factory.tree.TreeAdaptor;
import com.sun.jsftemplating.component.factory.tree.TreeAdaptorBase;
import com.sun.jsftemplating.layout.descriptors.ComponentType;
import com.sun.jsftemplating.layout.descriptors.LayoutComponent;
import com.sun.jsftemplating.layout.descriptors.LayoutElement;
import com.sun.webui.jsf.component.TreeNode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

/**
 * <p>
 *
 * The <code>JBITreeAdaptorImpl</code> implementation must have a 
 * <code>public static JBITreeAdaptorImpl getInstance(FacesContext,
 *  LayoutComponent, UIComponent)</code> method in order to get access to an
 * instance of the <code>JBITreeAdaptorImpl</code> instance.</p> <p>
 *
 * This class is used by <code>DynamicTreeNodeFactory</code>.</p>
 *
 * @author   Sun Microsystems Inc.
 */
public class JBITreeAdaptorImpl extends TreeAdaptorBase
    implements TreeAdaptor
{

    /**
     * <p>
     *
     * This method provides access to an <code>JBITreeAdaptorImpl</code>
     * instance. Each time it is invoked, it returns a new instance.</p>
     *
     * @param ctx     Description of Parameter
     * @param desc    Description of Parameter
     * @param parent  Description of Parameter
     * @return        The Instance value
     */
    public static TreeAdaptor getInstance(FacesContext ctx, LayoutComponent desc, UIComponent parent)
    {
        TreeAdaptor result = new JBITreeAdaptorImpl(desc, parent);
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JBITreeAdaptorImpl.getInstance(" + ctx + ", " + desc + ", " + parent +
                  "),result =" + result);
        }
        return result;
    }

    /**
     * constructor used by the glassfish JBI subtree hook class: <code>JBIHookTreeAdaptor</code>
     */
    public JBITreeAdaptorImpl()
    {       
    }

    /**
     * <p>
     *
     * This constructor saves the <code>LayoutComponent</code> descriptor and
     * the <code>UIComponent</code> associated with this <code>TreeAdaptor</code>
     * . This constructor is used by the JBITreeAdaptorWrapper constructor.
     * </p>
     *
     * @param desc    Description of Parameter
     * @param parent  Description of Parameter
     */
    public JBITreeAdaptorImpl(LayoutComponent desc, UIComponent parent)
    {
        super(desc, parent);
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JBITreeAdaptorImpl.<init>(" + desc + ", " + parent + "), mJac=" + mJac + ", this=" + this);
        }
    }

    /**
     *
     * Returns child <code>TreeNode</code>s for the given <code>TreeNode</code>
     * model Object.</p>
     *
     * @param nodeObject  Description of Parameter
     * @return            The ChildTreeNodeObjects value
     */
    public List getChildTreeNodeObjects(Object nodeObject)
    {
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JBITreeAdaptorImpl getChildTreeNodeObjects(" + nodeObject + "), mAdaptorListType=" +
                  mAdaptorListType + ", this=" + this);
    	}

        List result = null;
        
        JBIConfigBean jbiConfigBean =
            BeanUtilities.getJBIConfigBean();

        ListBean listBean = BeanUtilities.getListBean();
        
        if ((null != jbiConfigBean)
            &&(jbiConfigBean.isJbiEnabled())
            &&(null != listBean)
            &&(null != nodeObject))
            {
                if (nodeObject.equals(mAdaptorListType))
                    {
                        String xmlQueryResults;

                        if ("deployments".equals(mAdaptorListType))
                            {
                                xmlQueryResults = listBean.getListServiceAssemblies();
                                
                                if (sLog.isLoggable(Level.FINER)){
                                   sLog.finer("JBITreeAdaptorImpl getChildTreeNodeObjects()" +
                                          ", xmlQueryResults=" + xmlQueryResults);
                                }
                                
                                List saInfoList =
                                    ServiceAssemblyInfo.readFromXmlTextWithProlog(xmlQueryResults);

                                mChildren = saInfoList.toArray();
                            }

                        else if ("bindingsEngines".equals(mAdaptorListType))
                            {
                                xmlQueryResults = listBean.getListBindingComponents();
                                
                                if (sLog.isLoggable(Level.FINER)){
                                   sLog.finer("JBITreeAdaptorImpl getChildTreeNodeObjects()" +
                                          ", xmlQueryResults=" + xmlQueryResults);
                                }
                                
                                List bcCompInfoList = JBIComponentInfo.readFromXmlText(xmlQueryResults);

                                xmlQueryResults = listBean.getListServiceEngines();
                                
                                if (sLog.isLoggable(Level.FINER)){
                                   sLog.finer("JBITreeAdaptorImpl getChildTreeNodeObjects()" +
                                          ", xmlQueryResults=" + xmlQueryResults);
                                }
                                
                                List seCompInfoList = JBIComponentInfo.readFromXmlText(xmlQueryResults);

                                List operableComponentsList = new ArrayList();
                                operableComponentsList.addAll(bcCompInfoList);
                                operableComponentsList.addAll(seCompInfoList);

                                mChildren = operableComponentsList.toArray();
                            }

                        else if ("libraries".equals(mAdaptorListType))
                            {
                                xmlQueryResults = listBean.getListSharedLibraries();
                                
                                if (sLog.isLoggable(Level.FINER)){
                                   sLog.finer("JBITreeAdaptorImpl getChildTreeNodeObjects()" +
                                          ", xmlQueryResults=" + xmlQueryResults);
                                }
                                
                                List slCompInfoList =
                                    JBIComponentInfo.readFromXmlText(xmlQueryResults);
                                mChildren = slCompInfoList.toArray();
                            }

                        if (mChildren != null)
                            {
                        	    if (sLog.isLoggable(Level.FINER)){
                                   sLog.finer("JBITreeAdaptorImpl getChildTreeNodeObjects()" +
                                          ", mChildren.length=" + mChildren.length);
                        	    }
                        	    
                                result = Arrays.asList((Object[]) mChildren);
                            }
                    }

                else if (nodeObject instanceof ServiceAssemblyInfo)
                    {
                        String target = JBIAdminCommands.DOMAIN_TARGET_KEY;
                        List suInfoList = new ArrayList();
                        ServiceAssemblyInfo saInfo = (ServiceAssemblyInfo) nodeObject;
                        String saName = saInfo.getName();
                        List infoList = saInfo.getServiceUnitInfoList();
                        Iterator<Object> it2 = infoList.iterator();
                        while (it2.hasNext())
                            {
                                Object suInfoObject = it2.next();
                                String compName = ((ServiceUnitInfo) suInfoObject).getTargetName();
                                ServiceUnitInfoWrapper wrapper = 
                                    new ServiceUnitInfoWrapper((ServiceUnitInfo) suInfoObject,
                                                               compName,
                                                               saName,
                                                               target);
                                suInfoList.add(wrapper);
                                wrapper.setQueryString("sa");
                            }
                        result = suInfoList;
                    }

                else if (nodeObject instanceof JBIComponentInfo)
                    {
                        JBIComponentInfo compInfo = (JBIComponentInfo) nodeObject;
                        String compName = compInfo.getName();
                        String compType = compInfo.getType();
                        String target = JBIAdminCommands.DOMAIN_TARGET_KEY;
                        List saInfoList = null;
                        List suInfoList = new ArrayList();
                        if (!(JBIConstants
                              .JBI_SHARED_LIBRARY_TYPE
                              .equals(compType)))
                            {
                        	    if (sLog.isLoggable(Level.FINER)){
                                   sLog.finer("JBITreeAdaptorImpl.getChildTreeNodeObjects(): get SUs for compType=" +
                                          compType + ", compName=" + compName);
                        	    }
                        	    
                                try
                                    {
                                        String xml = mJac.listServiceAssemblies(compName, target);
                                        saInfoList = ServiceAssemblyInfo.readFromXmlTextWithProlog(xml);
                                        
                                        // workaround for broken listServiceAssemblies when target="domain"
                                        if ((null != saInfoList)
                                            &&(0 == saInfoList.size()))
                                            {
                                                // this doesn't work for SA's deployed to BC|SEs that
                                                // are only installed to targets other than "server"
                                                xml = mJac.listServiceAssemblies(compName, 
                                                                                 JBIAdminCommands
                                                                                 .SERVER_TARGET_KEY);
                                                saInfoList = ServiceAssemblyInfo.readFromXmlTextWithProlog(xml);
                                            }

                                        if (saInfoList != null)
                                            {
                                                Iterator<Object> it = saInfoList.iterator();
                                                while (it.hasNext())
                                                    {
                                                        Object saInfoObject = it.next();
                                                        String saName = 
                                                            ((ServiceAssemblyInfo) saInfoObject)
                                                            .getName();
                                                        List infoList = 
                                                            ((ServiceAssemblyInfo) saInfoObject)
                                                            .getServiceUnitInfoList();
                                                        Iterator<Object> it2 = infoList.iterator();
                                                        while (it2.hasNext())
                                                            {
                                                                Object suInfoObject = it2.next();
                                                                String tgtName = 
                                                                    ((ServiceUnitInfo) suInfoObject)
                                                                    .getTargetName();
                                                                if (tgtName.equals(compName))
                                                                    {
                                                                        ServiceUnitInfoWrapper wrapper = 
                                                     new ServiceUnitInfoWrapper((ServiceUnitInfo) suInfoObject,
                                                                                                       compName,
                                                                                                       saName,
                                                                                                       target);
                                                                        wrapper.setQueryString("comp");
                                                                        suInfoList.add(wrapper);
                                                                    }
                                                            }
                                                    }
                                            }
                                    }
                                catch (com.sun.jbi.ui.common.JBIRemoteException jbiRemoteEx)
                                    {
                                	    if (sLog.isLoggable(Level.FINE)){
                                           sLog.fine("JBITreeAdaptorImpl.getChildTreeNodeObjects()" +
                                                  ", caught jbiRemoteEx=" + jbiRemoteEx);
                                	    }                                	    
                                    }
                            }
                        else
                            {
                        	    if (sLog.isLoggable(Level.FINER)){
                                   sLog.finer("JBITreeAdaptorImpl.getChildTreeNodeObjects() skip getting SUs for an SL");
                        	    }                        	    
                            }

                        result = suInfoList;

                    }

            }
        else
            {
        	    if (sLog.isLoggable(Level.FINER)){
                    sLog.finer("JBITreeAdaptorImpl getChildTreeNodeObjects, nodeObject=" + nodeObject);
        	    }
            }
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JBITreeAdaptorImpl getChildTreeNodeObjects, result=" + result + ", this=" + this);
        }
        
        return result;
    }

    /**
     * <p>
     *
     * This method returns the "options" that should be supplied to the
     * factory that creates the <code>TreeNode</code> for the given tree node
     * model object.</p> <p>
     *
     * Some useful options for the standard <code>TreeNode</code> component
     * include:<p>
     *
     *
     * <ul>
     *   <li> text</li>
     *   <li> url</li>
     *   <li> imageURL</li>
     *   <li> target</li>
     *   <li> action
     *   <li>
     *   <li> actionListener</li>
     *   <li> expanded</li>
     * </ul>
     * <p>
     *
     * See Tree / TreeNode component documentation for more details.</p>
     *
     * @param nodeObject  Description of Parameter
     * @return            The FactoryOptions value
     */
    public Map<String, Object> getFactoryOptions(Object nodeObject)
    {
        Map<String, Object> result = null;

        if (null != nodeObject)
            {
                LayoutComponent desc = getLayoutComponent();
                result = new HashMap<String, Object>();

                if (sLog.isLoggable(Level.FINER)){
                   sLog.finer("JBITreeAdaptorImpl getFactoryOptions (" + nodeObject + "), nodeObject.getClass()=" +
                          nodeObject.getClass() + ", desc=" + desc + ", this=" + this);
                }

                if (nodeObject instanceof String)
                    {
                        // This case deals with the top node.

                        // NOTE: All supported options must be handled here,
                        //      otherwise they'll be ignored.
                        // NOTE: Options will be evaluated later, do not eval here.
                        setProperty(result, "text", desc.getOption("text"));
                        setProperty(result, "url", desc.getOption("url"));
                        setProperty(result, "imageURL", desc.getOption("imageURL"));
                        setProperty(result, "target", desc.getOption("target"));
                        setProperty(result, "action", desc.getOption("action"));

                        // NOTE: Although actionListener is supported, LH currently
                        //       implements this to be the ActionListener of the "turner"
                        //       which is inconsistent with "action".  We should make use
                        //       of the "Handler" feature which provides a "toggle"
                        //       CommandEvent.
                        setProperty(result, "actionListener", desc.getOption("actionListener"));
                        setProperty(result, "expanded", desc.getOption("expanded"));
                        
                        if (sLog.isLoggable(Level.FINER)){
                           sLog.finer("JBITreeAdaptorImpl getFactoryOptions (parent), text=" + desc.getOption("text") +
                                  ", (partial)result=" + result);
                        }

                    }
                else
                    {
                        // This case deals with the children

                        // NOTE: All supported options must be handled here,
                        // otherwise they'll be ignored

                        // Finish setting the child properties
                        String childName = null;
                        String imageUrlSuffix = null;
                        String type = null;
                        String compType = null;
                        String urlSuffix = "type=";
                        String urlBase = (String) desc.getOption("childURLbase");

                        if (nodeObject instanceof JBIComponentInfo)
                            {
                                JBIComponentInfo compInfo = (JBIComponentInfo) nodeObject;
                                childName = compInfo.getName();
                                type = compInfo.getType();
                                
                                if (sLog.isLoggable(Level.FINER)){
                                   sLog.finer("JBITreeAdaptorImpl getFactoryOptions)" +
                                          ", childName=" + childName + ", type=" +
                                          type + ", (partial)result=" + result + ", this=" + this);
                                }
                                BeanUtilities.setStringPropertyUsingExpression(childName,
                                                                               "#{sessionScope.serviceAssemblyName}");
                            }

                        else if (nodeObject instanceof ServiceAssemblyInfo)
                            {
                                ServiceAssemblyInfo saInfo = (ServiceAssemblyInfo) nodeObject;
                                childName = saInfo.getName();
                                type =
                                    JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE;
                            }

                        else if (nodeObject instanceof ServiceUnitInfoWrapper)
                            {
                                ServiceUnitInfo suInfo = ((ServiceUnitInfoWrapper) nodeObject).getServiceUnitInfo();
                                childName = suInfo.getName();
                                type = 
                                    JBIConstants.JBI_SERVICE_UNIT_TYPE;


                                String saName = ((ServiceUnitInfoWrapper) nodeObject).getServiceAssemblyName();
                                String compName = ((ServiceUnitInfoWrapper) nodeObject).getComponentName();
                                String qs = ((ServiceUnitInfoWrapper) nodeObject).getQueryString();

                                compType = ((ServiceUnitInfoWrapper) nodeObject).getComponentType();

                                String grandChildURLBase = "/jbi/cluster/showServiceUnit.jsf?";
                                if (urlBase.indexOf("cluster") == -1)
                                    {
                                        grandChildURLBase = "/jbi/pe/showServiceUnit.jsf?";
                                    }
                                urlBase = (String) grandChildURLBase +
                                    "saname=" + saName + "&" +
                                    "qs=" + qs + "&" +
                                    "compName=" + compName + "&" +
                                    "compType=" + compType + "&";
                            }

                        if (null != childName)
                            {
                                setProperty(result, "text", childName);
                                urlSuffix += type + "&name=" + childName;
                                if (JBIConstants
                                    .JBI_SERVICE_ASSEMBLY_TYPE
                                    .equals(type))
                                    {
                                        imageUrlSuffix = 
                                            IMAGE_FILE_NAME_SA;
                                    }
                                else if (JBIConstants
                                         .JBI_BINDING_COMPONENT_TYPE
                                         .equals(type))
                                    {
                                        imageUrlSuffix = 
                                            IMAGE_FILE_NAME_BC;
                                    }
                                else if (JBIConstants
                                         .JBI_SERVICE_ENGINE_TYPE
                                         .equals(type))
                                    {
                                        imageUrlSuffix = 
                                            IMAGE_FILE_NAME_SE;
                                    }
                                else if (JBIConstants
                                         .JBI_SHARED_LIBRARY_TYPE
                                         .equals(type))
                                    {
                                        imageUrlSuffix = 
                                            IMAGE_FILE_NAME_SL;
                                    }
                                else if (JBIConstants
                                         .JBI_SERVICE_UNIT_TYPE
                                         .equals(type))
                                    {
                                        imageUrlSuffix = 
                                            IMAGE_FILE_NAME_SU;
                                    }
                            }
                        String url = urlBase + urlSuffix;
                        
                        if (sLog.isLoggable(Level.FINER)){
                            sLog.finer("JBITreeAdaptorImpl.getFactoryOptions()" +
                                  ", url=" + url + ", imageUrlSuffix=" +
                                  imageUrlSuffix + ", this=" + this);
                        }
                        
                        setProperty(result, "url", url);
                        setProperty(result, "imageURL", desc.getOption("childImageURLbase") + imageUrlSuffix);
                        setProperty(result, "target", desc.getOption("childTarget"));
                        setProperty(result, "action", desc.getOption("childAction"));
                        setProperty(result, "expanded", desc.getOption("childExpanded"));
                        
                        if (sLog.isLoggable(Level.FINER)){
                           sLog.finer("JBITreeAdaptorImpl getFactoryOptions (child), type=" + type + ", url=" + url);
                        }
                    }
            }

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JBITreeAdaptorImpl getFactoryOptions, result=" + result + ", this=" + this);
        }
        return result;
    }

    /**
     * <p>
     *
     * This method returns the <code>id</code> for the given tree node model
     * object.</p>
     *
     * @param nodeObject  Description of Parameter
     * @return            The Id value
     */
    public String getId(Object nodeObject)
    {
        String result = "nullNodeObject";

        if (null != nodeObject)
            {
                if (nodeObject instanceof ServiceAssemblyInfo)
                    {
                        result = ((ServiceAssemblyInfo) nodeObject).getName();
                    }
                else if (nodeObject instanceof JBIComponentInfo)
                    {
                        result = ((JBIComponentInfo) nodeObject).getName();
                    }
                else if (nodeObject instanceof ServiceUnitInfoWrapper)
                    {
                        ServiceUnitInfoWrapper wrapper =
                            (ServiceUnitInfoWrapper) nodeObject;

                        result = wrapper.getQueryString() +
                            "." + wrapper.getName();
                    }
                else
                    {
                        result = nodeObject.toString();
                    }
            }
        result = genId(result);
        return result;
    }

    /**
     * <p>
     *
     * This method returns any facets that should be applied to the <code>TreeNode (comp)</code>
     * . Useful facets for the sun <code>TreeNode</code> component are:
     * "content" and "image".</p> <p>
     *
     * Facets that already exist on <code>comp</code>, or facets that are
     * directly added to <code>comp</code> do not need to be returned from
     * this method.</p> <p>
     *
     * This implementation directly adds a "content" facet and returns <code>null</code>
     * from this method.</p>
     *
     * @param comp        The tree node <code>UIComponent</code>.
     * @param nodeObject  The (model) object representing the tree node.
     * @return            The Facets value
     */
    public Map<String, UIComponent> getFacets(UIComponent comp, Object nodeObject)
    {
        return null;
    }

    /**
     * <p>
     *
     * Advanced framework feature which provides better handling for things
     * such as expanding TreeNodes, beforeEncode, and other events.</p> <p>
     *
     * This method should return a <code>Map</code> of <code>List</code> of
     * <code>Handler</code> objects. Each <code>List</code> in the <code>Map</code>
     * should be registered under a key that cooresponds to to the "event" in
     * which the <code>Handler</code>s should be invoked.</p>
     *
     * @param comp        Description of Parameter
     * @param nodeObject  Description of Parameter
     * @return            The HandlersByType value
     */
    public Map getHandlersByType(UIComponent comp, Object nodeObject)
    {
        return null;
    }

    /**
     * <p>
     *
     * This method is called shortly after {@link #getInstance(FacesContext,
     * LayoutComponent, UIComponent)}. It provides a place for post-creation
     * initialization to take occur.</p>
     */
    public void init()
    {
        //initialise the logger
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JBITreeAdaptorImpl.init(), this=" + this);
    	}

        // Get the FacesContext
        FacesContext ctx = FacesContext.getCurrentInstance();

        // This is the descriptor for this dynamic TreeNode, it contains all
        // information (options) necessary for this Adaptor
        LayoutComponent desc = getLayoutComponent();

        // The parent UIComponent
        UIComponent parent = getParentUIComponent();

        // add extension subtree, if any
        addExtensionSubtree(parent);

        // Get the Method Name
        Object val = desc.getEvaluatedOption(ctx, "treeAdaptorListType", parent);
        if (val == null)
            {
                throw new IllegalArgumentException("'treeAdaptorListType' must be specified!");
            }
        mAdaptorListType = (String) val;

        setTreeNodeObject(mAdaptorListType);

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JBITreeAdaptorImpl.init(), desc=" + desc +
                  ", parent=" + parent +
                  ", mAdaptorListType=" + mAdaptorListType +
                  ", this=" + this);
        }
    }

    /**
     * <p>
     *
     * Helper method for setting Properties while avoiding NPE's.</p>
     *
     * @param props  The new Property value
     * @param key    The new Property value
     * @param value  The new Property value
     */
    void setProperty(Map props, String key, Object value)
    {
        if (value != null)
            {
                props.put(key, value);
            }
    }

    private Object[] mChildren;

    private static Logger sLog =
        JBILogger.getInstance();

    /**
     * Gets the DynamicTreeNode attribute of the JBITreeAdaptorImpl object
     *
     * @return   The DynamicTreeNode value
     */
    private UIComponent getDynamicTreeNode()
    {
        UIComponent result = null;

        FacesContext facesContext =
            FacesContext.getCurrentInstance();

        LayoutComponent currentLayout =
            getLayoutComponent();
        LayoutElement parentLayout =
            currentLayout.getParent();
        List layouts =
            parentLayout.getChildLayoutElements();
        LayoutElement parent = null;

        String id = ID_JBI_TREE_EXT;

        ComponentType type =
            new ComponentType("dynamicTreeNode",
                              "com.sun.jbi.jsf.util.JBITreeExtensionAdaptorImpl");
        LayoutComponent dynamicTreeNodeLayout =
            new LayoutComponent(parent, id, type);
        dynamicTreeNodeLayout.addOption("treeAdaptorClass",
                                        "com.sun.jbi.jsf.util.JBITreeExtensionAdaptorImpl");

        UIComponent parentComponent =
            getParentUIComponent();

        DynamicTreeNodeFactory dynamicTreeNodeFactory =
            new DynamicTreeNodeFactory();

        UIComponent extensionRoot =
            dynamicTreeNodeFactory.create(facesContext,
                                          dynamicTreeNodeLayout,
                                          parentComponent);
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JBITreeAdaptorImpl.getDynamicTreeNode(), result=" +
                  result);
        }

        return result;
    }

    /**
     * <p>
     *
     * This method generates an ID that is safe for JSF for the given String.
     * It does not guarantee that the id is unique, it is the responsibility
     * of the caller to pass in a String that will result in a UID. All
     * non-ascii characters will be stripped.</p>
     *
     * @param anIdCandidate  A non-null String to be used as the basis for a
     * legal html element id attribute value.
     * @return The usable id String with a prefix, if needed, and illegal 
     * characters replaced with underscores
     */
    private String genId(String anIdCandidate)
    {
        StringBuffer result
            = new StringBuffer("_"); // always prefix (handles numeric ids)
        int escapeCount = 0;

        // Quickly determines if the id can be used
        boolean isEscapeNeeded = false;
        char [] cAry = anIdCandidate.toCharArray();
        for (int i = 0; i < cAry.length; ++i) 
            {
                // If the character is not usable
                // indicates that (slow/expensive) escaping
                // is needed.
                if ((!Character.isLetterOrDigit(cAry[i]))
                    && ('_'!=cAry[i]))
                    {
                        isEscapeNeeded = true;
                        break;
                    }
            }
        
        // If the ID does not require escaping,
        // appends it.
        if (!isEscapeNeeded)
            {
                result.append(anIdCandidate);
            }
        // If the ID requires escaping,
        // relaces invalid characters with hex codes (e.g. "$" => "_24")
        else
            {
                for (int i = 0; i < anIdCandidate.length(); ++i)
                    {
                        try
                            {
                                int x =
                                    anIdCandidate.charAt(i);
                                if ((Character.isLetterOrDigit(x))
                                    || ('_'==x))
                                    {
                                        result
                                            .append(anIdCandidate
                                                    .substring(i,
                                                               (i + 1)));
                                    }
                                else
                                    {
                                        result 
                                            .append("_");
                                        result
                                            .append(Integer.toString(x, 16));
                                        ++escapeCount;
                                    }
                            }
                        catch (RuntimeException rEx)
                            {
                                result.append("_xx");
                            }
                    }
            }
        
        // Note: the next sets of appends are intended to handle
        // pathological cases, where "foo bc" and "foo.bc" have been
        // escaped and then "foo_20bc" and "foo_2ebc" are installed
        // (without escaping).

        // appends the escape count (to help ensure a unique ID)
        result.append("_");
        result.append(escapeCount);

        // appends the original ID length (to help ensure a unique ID)
        result.append("_");
        result.append(anIdCandidate.length());

        // At this point, collisions are unlikely:
        // "fooBc" => "_fooBc_0_5"
        // "foo bc" => "_foo_20bc_1_6"
        // "foo.bc" => "_foo_2ebc_1_6"
        // "foo_20bc" => "_foo_20bc_0_8"
        // "foo_20bc_0_8" => "_foo_20bc_0_8_0_12"
        // "foo_2ebc_1_6" => "_foo_2ebc_1_6_0_12"
        
        if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("JBITreeAdaptorImpl.genId(" + anIdCandidate +
                    ", result.toString()=" + result.toString());
        }
        
        return result.toString();
    }


    /**
     * @param aParentRef  The feature to be added to the ExtensionSubtree
     *      attribute
     */
    private void addExtensionSubtree(UIComponent aParentRef)
    {
        JBIConfigBean jbiConfigBean =
            BeanUtilities.getJBIConfigBean();

        JBITreeExtension jbiTreeExtension = null;

        if (null != jbiConfigBean)
            {
                jbiTreeExtension =
                    jbiConfigBean.getJBITreeExtension();
            }

        if (null != jbiTreeExtension)
            {
                TreeNode selfTreeNode = null;
                TreeNode newTreeNode = null;

                List siblings = aParentRef.getParent().getChildren();
                Iterator siblingsIt = siblings.iterator();
                while (siblingsIt.hasNext())
                    {
                        Object next = siblingsIt.next();
                        
                        if (sLog.isLoggable(Level.FINEST)){
                           sLog.finest("JBITreeAdaptorImpl.addExtensionSubtree(), next=" +
                                  next);
                        }
                        TreeNode sibling = null;
                        if (next instanceof TreeNode)
                            {
                                sibling =
                                    (TreeNode) next;
                                String siblingId = sibling.getId();
                                if ("JBIRoot".equals(siblingId))
                                    {
                                	    if (sLog.isLoggable(Level.FINEST)){
                                           sLog.finest("JBITreeAdaptorImpl.addExtensionSubtree()" +
                                                  ", found self treeNode=" + sibling.getText());
                                	    }
                                        selfTreeNode = sibling;
                                    }
                                else if ("JBIExtRootTreeNode".equals(siblingId))
                                    {
                                	    if (sLog.isLoggable(Level.FINEST)){
                                           sLog.finest("JBITreeAdaptorImpl.addExtensionSubtree()" +
                                                  ", found new treeNode=" + sibling.getText());
                                	    }
                                        newTreeNode = sibling;
                                    }
                                else
                                    {
                                	    if (sLog.isLoggable(Level.FINEST)){
                                           sLog.finest("JBITreeAdaptorImpl.addExtensionSubtree()" +
                                                  ", sibling=" + sibling.getText() + ", id=" + sibling.getId());
                                	    }
                                    }
                            }

                    }

                if (null != selfTreeNode)
                    {
                        if (null != newTreeNode)
                            {
                        	    if (sLog.isLoggable(Level.FINEST)){
                                    sLog.finest("JBITreeAdaptorImpl.addExtensionSubtree(), updating treeNode" +
                                          ", newTreeNode=" + newTreeNode);
                        	    }
                        	    
                                jbiTreeExtension.update(newTreeNode);
                                
                                if (sLog.isLoggable(Level.FINEST)){
                                   sLog.finest("JBITreeAdaptorImpl.addExtensionSubtree(), treeNode updated" +
                                          ", newTreeNode.getText()=" + newTreeNode.getText());
                                }
                            }
                        else
                            {
                        	    if (sLog.isLoggable(Level.FINEST)){
                                   sLog.finest("JBITreeAdaptorImpl.addExtensionSubtree(), creating new treeNode" +
                                          ", jbiTreeExtension=" + jbiTreeExtension);
                        	    }
                                if (false)
                                    {
                                        newTreeNode =
                                            (TreeNode) getDynamicTreeNode();
                                    }
                                newTreeNode =
                                    new com.sun.webui.jsf.component.TreeNode();

                                newTreeNode.setId(ID_JBI_EXT_ROOT_TREE_NODE);
                                String defaultNodeText =
                                    I18nUtilities.getResourceString("jbi.extension.default.node.text");
                                newTreeNode.setText(defaultNodeText);

                                newTreeNode.setTarget(selfTreeNode.getTarget());

                                jbiTreeExtension.init(newTreeNode);
                                
                                if (sLog.isLoggable(Level.FINEST)){
                                   sLog.finest("JBITreeAdaptorImpl.addExtensionSubtree(), new treeNode initialized" +
                                          ", newTreeNode.getText()=" + newTreeNode.getText());
                                }
                                siblings.add(newTreeNode);
                            }

                    }
            }
    }

    private static final String BLANK = " "; // NOT I18n
    private static final char CHAR_DASH = '-'; // NOT I18n
    private static final char CHAR_UNDERSCORE = '_'; // NOT I18n
    private static final String DASH = "-"; // NOT I18n
    private static final String ESCAPED_PERIOD = "\\."; // NOT I18n
    private static final String ID_JBI_EXT_ROOT_TREE_NODE = "JBIExtRootTreeNode"; // NOT I18n
    private static final String ID_JBI_TREE_EXT = "JBITreeExtension"; // NOT I18n
    private static final String IMAGE_FILE_NAME_BC = "JBIBindingComponent.gif"; // NOT I18n
    private static final String IMAGE_FILE_NAME_SA = "JBIServiceAssembly.gif"; // NOT I18n
    private static final String IMAGE_FILE_NAME_SE = "JBIServiceEngine.gif"; // NOT I18n
    private static final String IMAGE_FILE_NAME_SL = "JBISharedLibrary.gif"; // NOT I18n
    private static final String IMAGE_FILE_NAME_SU = "JBISU.gif"; // NOT I18n
    private static final String PERIOD = "."; // NOT I18n
    private static final String START_WITH_ALPHA = "A"; // NOT I18n
    private static final String UNDERSCORE = "_"; // NOT I18n


    private JBIAdminCommands mJac =
        BeanUtilities.getClient();

    private String mAdaptorListType;
    private String mNameAtt;
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  SystemLoggerUtilities.java
 */

package com.sun.jbi.jsf.util;

import com.sun.enterprise.tools.admingui.util.AMXUtil;
import com.sun.enterprise.tools.admingui.util.JMXUtil;
import com.sun.enterprise.tools.admingui.util.GuiUtil;
import com.sun.appserv.management.config.ConfigConfig;
import com.sun.appserv.management.config.ModuleLogLevelsConfig;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.webui.jsf.model.Option;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Map;
import java.io.InputStream;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;  
import org.w3c.dom.NodeList;  
import org.w3c.dom.Element;  
import org.w3c.dom.Attr;  
import java.io.IOException;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import javax.xml.parsers.ParserConfigurationException;


/**
 *
 * JBI Contstants related to the JSR-208 Specification
 *
 **/
public final class SystemLoggerUtilities
{

    public static final Hashtable loggerLabels = new Hashtable ();
    static
    {
        loggerLabels.put ("admin",           getResource("jbi.system.logger.label.admin"));
        loggerLabels.put ("ejb",             getResource("jbi.system.logger.label.ejb"));
        loggerLabels.put ("classloader",     getResource("jbi.system.logger.label.classloader"));
        loggerLabels.put ("configuration",   getResource("jbi.system.logger.label.configuration"));
        loggerLabels.put ("connector",       getResource("jbi.system.logger.label.connector"));
        loggerLabels.put ("corba",           getResource("jbi.system.logger.label.corba"));
        loggerLabels.put ("deployment",      getResource("jbi.system.logger.label.deployment"));
        loggerLabels.put ("javamail",        getResource("jbi.system.logger.label.javamail"));
        loggerLabels.put ("jaxr",            getResource("jbi.system.logger.label.jaxr"));
        loggerLabels.put ("jaxrpc",          getResource("jbi.system.logger.label.jaxrpc"));
        loggerLabels.put ("jms",             getResource("jbi.system.logger.label.jms"));
        loggerLabels.put ("jta",             getResource("jbi.system.logger.label.jta"));
        loggerLabels.put ("jts",             getResource("jbi.system.logger.label.jts"));
        loggerLabels.put ("mdb",             getResource("jbi.system.logger.label.mdb"));
        loggerLabels.put ("naming",          getResource("jbi.system.logger.label.naming"));
        loggerLabels.put ("root",            getResource("jbi.system.logger.label.root"));
        loggerLabels.put ("saaj",            getResource("jbi.system.logger.label.saaj"));
        loggerLabels.put ("security",        getResource("jbi.system.logger.label.security"));
        loggerLabels.put ("selfmanagement",  getResource("jbi.system.logger.label.selfmanagement"));
        loggerLabels.put ("server",          getResource("jbi.system.logger.label.server"));
        loggerLabels.put ("util",            getResource("jbi.system.logger.label.util"));
        loggerLabels.put ("verifier",        getResource("jbi.system.logger.label.verifier"));
        loggerLabels.put ("web",             getResource("jbi.system.logger.label.web"));
        loggerLabels.put ("jbi",             getResource("jbi.system.logger.label.jbi"));
        loggerLabels.put ("nodeagent",       getResource("jbi.system.logger.label.nodeagent"));
        loggerLabels.put ("synchronization", getResource("jbi.system.logger.label.synchronization"));
        loggerLabels.put ("gms",             getResource("jbi.system.logger.label.gms"));
    }

    public static final Hashtable loggerNames = new Hashtable ();
    static
    {
        loggerNames.put ("javax.enterprise.system.tools.admin",               "Admin");
        loggerNames.put ("javax.enterprise.system.container.ejb",             "EJB");
        loggerNames.put ("javax.enterprise.system.core.classloading",         "Classloader");
        loggerNames.put ("javax.enterprise.system.core.config",               "Configuration");
        loggerNames.put ("javax.enterprise.resource.resourceadapter",         "Connector");
        loggerNames.put ("javax.enterprise.resource.corba",                   "Corba");
        loggerNames.put ("javax.enterprise.system.tools.deployment",          "Deployment");
        loggerNames.put ("javax.enterprise.resource.javamail",                "Javamail");
        loggerNames.put ("javax.enterprise.system.webservices.registry",      "Jaxr");
        loggerNames.put ("javax.enterprise.system.webservices.rpc",           "Jaxrpc");
        loggerNames.put ("javax.enterprise.resource.webservices.jaxws.rpc",   "Jaxws");
        loggerNames.put ("javax.enterprise.resource.jms; javax.resourceadapter.mqjmsra", "Jms");
        loggerNames.put ("javax.enterprise.resource.jta",                     "Jta");
        loggerNames.put ("javax.enterprise.system.core.transaction",          "Jts");
        loggerNames.put ("javax.enterprise.system.container.ejb.mdb",         "MDB");
        loggerNames.put ("javax.enterprise.system.core.naming",               "Naming");
        loggerNames.put ("javax.enterprise",                                  "Root");
        loggerNames.put ("javax.enterprise.system.webservices.saaj",          "Saaj");
        loggerNames.put ("javax.enterprise.system.core.security",             "Security");
        loggerNames.put ("javax.enterprise.system.core.selfmanagement",       "SelfManagement");
        loggerNames.put ("javax.enterprise.system",                           "Server");
        loggerNames.put ("javax.enterprise.system.util",                      "Util");
        loggerNames.put ("javax.enterprise.system.tools.verifier",            "Verifier");
        loggerNames.put ("javax.enterprise.system.container.web; org.apache.catalina; org.apache.coyote; org.apache.jasper;","WEB");
        loggerNames.put ("com.sun.jbi",                                       "JBI");
        loggerNames.put ("javax.ee.enterprise.system.nodeagent",              "NodeAgent");
        loggerNames.put ("javax.ee.enterprise.system.tools.synchronization",  "Synchronization");
        loggerNames.put ("javax.ee.enterprise.system.gms",                    "Gms");
    }

    // Initialize the optionList with all the normal levels
    public static final ArrayList levelOptionList = new ArrayList ();
    static
    {
        levelOptionList.add(new Option("FINEST",  getResource("loglevel.FINEST")));
        levelOptionList.add(new Option("FINER",   getResource("loglevel.FINER")));
        levelOptionList.add(new Option("FINE",    getResource("loglevel.FINE")));
        levelOptionList.add(new Option("CONFIG",  getResource("loglevel.CONFIG")));
        levelOptionList.add(new Option("INFO",    getResource("loglevel.INFO")));
        levelOptionList.add(new Option("WARNING", getResource("loglevel.WARNING")));
        levelOptionList.add(new Option("SEVERE",  getResource("loglevel.SEVERE")));
        levelOptionList.add(new Option("OFF",     getResource("loglevel.OFF")));
    }

    public static final String DEFAULT_NAME = "DEFAULT";

    private static final String DEFAULT_DEFAULT_LEVEL = "INFO";
    private static final String JAXWS_MODULE_PROPERTY = "javax.enterprise.resource.webservices.jaxws";
    private static final String JBI_MODULE_PROPERTY   = "com.sun.jbi" ;

    private static Logger sLog = JBILogger.getInstance();


    private static Document parseDomDocument (InputStream documentStream)
        throws IOException, SAXException, ParserConfigurationException
    {
        // Get a JAXP parser factory object
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        // Tell the factory what kind of parser we want 
        dbf.setValidating(false);

        // Use the factory to get a JAXP parser object
        javax.xml.parsers.DocumentBuilder parser = dbf.newDocumentBuilder();

        Document document = parser.parse(documentStream);
        return document;
    }


    private static String getLoggerList(String module){
        String[] params = {module};
        String[] types = {"java.lang.String"};

        String logList = "";
        String seperator = "";
        List loggers = (List) JMXUtil.invoke(
                                            "com.sun.appserv:name=logmanager,category=runtime,server=server",
                                            "getLognames4LogModule", params, types);
        if (loggers != null)
        {
            for (int cnt = 0; cnt < loggers.size(); cnt++)
            {
                logList += loggers.get(cnt);
                logList += seperator;
                seperator ="; ";
            }
        }

        return logList;
    }    


    /**
     *	<p> Will retrieve the localized string.
     *
     *	@param	aStr resource string
     *  @return localized string
     */
    public static String getResource (String aStr)
    {
        return GuiUtil.getMessage(I18nUtilities.getResourceString(aStr));
    }


    /**
     *	<p> Will return the XML document.
     *
     *	@param	fileInputStream  The file input stream.
     *  @return The DOM document
     */
    public static Document getConfigDocument (InputStream fileInputStream)
    {
        Document document = null;
        if (fileInputStream != null)
        {
            try {
                document = parseDomDocument(fileInputStream);
            }
            catch (IOException ex) {
            	if (sLog.isLoggable(Level.FINE)){
                sLog.fine("SystemLoggerUtilities(): Unable to find the XML Document.");
            	}
            }
            catch (ParserConfigurationException ex) {
            	if (sLog.isLoggable(Level.FINE)){
                sLog.fine("SystemLoggerUtilities(): Error parsing the XML Document.");
            	}
            }
            catch (SAXException ex) {
            	if (sLog.isLoggable(Level.FINE)){
                sLog.fine("SystemLoggerUtilities(): Error parsing the XML Document.");
            	}
            }
            catch (RuntimeException ex) {
            	if (sLog.isLoggable(Level.FINE)){
                sLog.fine("SystemLoggerUtilities(): Error parsing the XML Document.");
            	}
            }
        }
        return document;
    }


    /**
     *	<p> Will add any additional System Loggers.
     *
     *	@param	aLogLevels Map which contains logger levels to add to
     *	@param	aComponentName The component name
     *	@param	aTarget The target name
     *	@param	aDocument The XML document containing the additional loggers
     *  @return Map containing the LogLevels
     */
    public static TreeMap addAdditionalSystemLoggers (TreeMap aLogLevels, 
                                                      String aComponentName,
                                                      String aTarget,
                                                      Document aDocument) 

    {
        // If the inputStream is null, then we can assume that the file xml did not
        // exist.  This is ok, we'll just continue with our processing.
        if (aDocument != null)
        {
            aTarget = ClusterUtilities.getInstanceDomainCluster(aTarget);
            String targetConfig = aTarget + "-config";

            NodeList loggerConfigList = aDocument.getElementsByTagName("logger-config");
            int loggerConfigLength = loggerConfigList.getLength();

            for (int configIndex = 0; configIndex < loggerConfigLength; configIndex++) {

                Element configElement = (Element)loggerConfigList.item(configIndex);
                NodeList loggersList = configElement.getElementsByTagName("additional-loggers");
                int loggersListLength = loggersList.getLength();

                for (int i = 0; i < loggersListLength; i++) {

                    Element componentNameElement = (Element)loggersList.item(i);
                    NodeList componentList = componentNameElement.getElementsByTagName("component-name");
                    int componentListLength = componentList.getLength();
                
                    for (int j = 0; j < componentListLength; j++) {

                        Element componentElement = (Element)componentList.item(j);
                        Attr componentNameAttr = componentElement.getAttributeNode("value");
                        String componentName = componentNameAttr.getValue();

                        if (aComponentName.equals(componentName))
                        {
                            // If the display type "type" is specified as "only" then we only
                            // want to display the optional loggers, so wipe out any information
                            // that might of been placed in the aLogLevels TreeMap.
                            Attr displayTypeAttr = componentElement.getAttributeNode("type");
                            String displayType = displayTypeAttr.getValue();
                            if (displayType.equalsIgnoreCase("only")) {
                                aLogLevels = new TreeMap();
                            }

                            NodeList moduleList = componentElement.getElementsByTagName("module-name");
                            int moduleListLength = moduleList.getLength();

                            for (int k=0; k<moduleListLength; k++)
                            {
                                Element moduleElement = (Element)moduleList.item(k);
                                Attr moduleNameAttr = moduleElement.getAttributeNode("value");
                                String moduleName = moduleNameAttr.getValue();
                                String listName = moduleName.toLowerCase();

                                if (sLog.isLoggable(Level.FINEST)){
                                sLog.finest("SystemLoggerUtilities - addAdditionalSystemLoggers: " +
                                          "componentName=" + aComponentName + 
                                          ", targetName=" + aTarget +
                                          ", moduleName=" + moduleName);
                                }

                                String loggerList = getLoggerList(listName);
                                ConfigConfig config = AMXUtil.getConfig(targetConfig);
                                ModuleLogLevelsConfig mConfig = config.getLogServiceConfig().getModuleLogLevelsConfig();
                            
                                String logLevelValue = SystemLoggerUtilities.getLogLevelValue(moduleName,targetConfig);
                                Level systemLogLevel = Level.parse(logLevelValue);
                                aLogLevels.put(loggerList,systemLogLevel);
                            }
                        }
                    }
                }
            }
        }
        return (aLogLevels);
    }


    /**
     *	<p> Using the XML config file, build the no default array list.
     *
     *	@param	aDocument  The XML Document to parse.
     *  @return An ArrayList containing the no default loggers
     */
    public static ArrayList getNoDefaultList (Document aDocument)
    {
        ArrayList noDefaultList = new ArrayList();
        if (null != aDocument)
            {
        NodeList loggerConfigList = aDocument.getElementsByTagName("logger-config");
        int loggerConfigLength = loggerConfigList.getLength();

        for (int configIndex = 0; configIndex < loggerConfigLength; configIndex++) {

            Element configElement = (Element)loggerConfigList.item(configIndex);
            NodeList loggersList = configElement.getElementsByTagName("no-default-loggers");
            int loggersListLength = loggersList.getLength();

            for (int i = 0; i < loggersListLength; i++) {

                Element loggerListElement = (Element)loggersList.item(i);
                NodeList loggeListNode = loggerListElement.getElementsByTagName("logger-name");
                int loggeListNodeLength = loggeListNode.getLength();

                for (int j = 0; j < loggeListNodeLength; j++) {
                    Element loggerNameElement = (Element)loggeListNode.item(j);
                    Attr loggerNameAttr = loggerNameElement.getAttributeNode("value");
                    String noDefaultLoggerName = loggerNameAttr.getValue();
                    noDefaultList.add(noDefaultLoggerName);
                }
            }
        }
            }
        return noDefaultList;
    }


    /**
     *	<p> Sets the Component Id using the given id tag string.</p>
     *
     *	@param	aLoggerName  The logger name.
     *	@param	aTargetConfig  The target configuration name
     *  @return A Array of Options
     */
    public static Option[] getLogLevelDropDownValues (String loggerName,
                                                      ArrayList noDefaultList) 
    {
        // Initialize the optionList with the basic values
        ArrayList<Option> optionList = new ArrayList<Option>(levelOptionList);

        // Add the DEFAULT option if the logger name is not in the noDefaultList
        if (!noDefaultList.contains(loggerName)) {
            optionList.add(new Option(DEFAULT_NAME,getResource("loglevel.DEFAULT")));
        }

        // Convert the ArrayList to an array of options, then return the array
        Option logLevelOptions[] = new Option[optionList.size()];
        logLevelOptions = optionList.toArray(logLevelOptions);
        return logLevelOptions;
    }


    /**
     *	<p> Sets the Component Id using the given id tag string.</p>
     *
     *  @param A Array of Options
     *	@param The option level label name
     *  @return The default level label name
     */
    public static String getDefaultLevel (Option[] optionList, String optionName)
    {
        String defaultLevel = DEFAULT_DEFAULT_LEVEL;
        for (int i=0; i<optionList.length; i++) {
            String o = (String)optionList[i].getValue();
            if (o.equalsIgnoreCase(optionName)) {
                defaultLevel = optionName;
                break;
            }
        }
        return defaultLevel;
    }
    

    /**
     *	<p> Sets the Component Id using the given id tag string.</p>
     *
     *	@param	aLoggerName  The logger name.
     *	@param	aTargetConfig  The target configuration name
     */
    public static String getLogLevelValue(String aLoggerName, 
                                          String aTargetConfig)
    {
        ConfigConfig config = AMXUtil.getConfig(aTargetConfig);
        ModuleLogLevelsConfig mConfig = config.getLogServiceConfig().getModuleLogLevelsConfig();
        String levelValue = "INFO";

        if (aLoggerName.equalsIgnoreCase("Admin")) {
            levelValue = mConfig.getAdmin();
        }
        else if (aLoggerName.equalsIgnoreCase("Classloader")) {
            levelValue = mConfig.getClassloader();
        }
        else if (aLoggerName.equalsIgnoreCase("Configuration")) {
            levelValue = mConfig.getConfiguration();
        }
        else if (aLoggerName.equalsIgnoreCase("Connector")) {
            levelValue = mConfig.getConnector();
        }
        else if (aLoggerName.equalsIgnoreCase("Corba")) {
            levelValue = mConfig.getCORBA();
        }
        else if (aLoggerName.equalsIgnoreCase("Deployment")) {
            levelValue = mConfig.getDeployment();
        }
        else if (aLoggerName.equalsIgnoreCase("Javamail")) {
            levelValue = mConfig.getJavamail();
        }
        else if (aLoggerName.equalsIgnoreCase("Jaxr")) {
            levelValue = mConfig.getJAXR();
        }
        else if (aLoggerName.equalsIgnoreCase("Jaxrpc")) {
            levelValue = mConfig.getJAXRPC();
        }
        else if (aLoggerName.equalsIgnoreCase("Jms")) {
            levelValue = mConfig.getJMS();
        }
        else if (aLoggerName.equalsIgnoreCase("Jta")) {
            levelValue = mConfig.getJTA();
        }
        else if (aLoggerName.equalsIgnoreCase("Jts")) {
            levelValue = mConfig.getJTS();
        }
        else if (aLoggerName.equalsIgnoreCase("MDB")) {
            levelValue = mConfig.getMDBContainer();
        }
        else if (aLoggerName.equalsIgnoreCase("Naming")) {
            levelValue = mConfig.getNaming();
        }
        else if (aLoggerName.equalsIgnoreCase("EJB")) {
            levelValue = mConfig.getEJBContainer();
        }
        else if (aLoggerName.equalsIgnoreCase("Root")) {
            levelValue = mConfig.getRoot();
        }
        else if (aLoggerName.equalsIgnoreCase("Saaj")) {
            levelValue = mConfig.getSAAJ();
        }
        else if (aLoggerName.equalsIgnoreCase("Security")) {
            levelValue = mConfig.getSecurity();
        }
        else if (aLoggerName.equalsIgnoreCase("Server")) {
            levelValue = mConfig.getServer();
        }
        else if (aLoggerName.equalsIgnoreCase("Util")) {
            levelValue = mConfig.getUtil();
        }
        else if (aLoggerName.equalsIgnoreCase("Verifier")) {
            levelValue = mConfig.getVerifier();
        }
        else if (aLoggerName.equalsIgnoreCase("WEB")) {
            levelValue = mConfig.getWebContainer();
        }
        else if (aLoggerName.equalsIgnoreCase("Jbi")) {
            if (mConfig.existsProperty(JBI_MODULE_PROPERTY))
                levelValue = mConfig.getPropertyValue(JBI_MODULE_PROPERTY);
        }
        else if (aLoggerName.equalsIgnoreCase("Jaxws")) {
            if (mConfig.existsProperty(JAXWS_MODULE_PROPERTY))
                levelValue = mConfig.getPropertyValue(JAXWS_MODULE_PROPERTY);
        }
        else if (aLoggerName.equalsIgnoreCase("NodeAgent")) {
            levelValue = mConfig.getNodeAgent();
        }
        else if (aLoggerName.equalsIgnoreCase("Synchronization")) {
            levelValue = mConfig.getSynchronization();
        }
        else if (aLoggerName.equalsIgnoreCase("Gms")) {
            levelValue = mConfig.getGroupManagementService();
        }
        return levelValue;
    }


    /**
     *	<p> Sets the Component Id using the given id tag string.</p>
     *
     *	@param	aLoggerName  The logger name.
     *	@param	aTargetConfig  The target configuration name
     */
    public static void setLogLevelValue(String aLoggerName, 
                                        String aTarget, 
                                        String aLevel)
    {
        String loggerTagName = (String)loggerNames.get(aLoggerName);

        aTarget = ClusterUtilities.getInstanceDomainCluster(aTarget);
        String targetConfig = aTarget + "-config";

        ConfigConfig config = AMXUtil.getConfig(targetConfig);
        ModuleLogLevelsConfig mConfig = config.getLogServiceConfig().getModuleLogLevelsConfig();

        if (loggerTagName.equalsIgnoreCase("Admin")) {
            mConfig.setAdmin(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Classloader")) {
            mConfig.setClassloader(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Configuration")) {
            mConfig.setConfiguration(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Connector")) {
            mConfig.setConnector(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Corba")) {
            mConfig.setCORBA(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Deployment")) {
            mConfig.setDeployment(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Javamail")) {
            mConfig.setJavamail(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Jaxr")) {
            mConfig.setJAXR(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Jaxrpc")) {
            mConfig.setJAXRPC(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Jms")) {
            mConfig.setJMS(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Jta")) {
            mConfig.setJTA(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Jts")) {
            mConfig.setJTS(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("MDB")) {
            mConfig.setMDBContainer(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Naming")) {
            mConfig.setNaming(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("EJB")) {
            mConfig.setEJBContainer(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Root")) {
            mConfig.setRoot(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Saaj")) {
            mConfig.setSAAJ(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Security")) {
            mConfig.setSecurity(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Server")) {
            mConfig.setServer(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Util")) {
            mConfig.setUtil(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Verifier")) {
            mConfig.setVerifier(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("WEB")) {
            mConfig.setWebContainer(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Jbi")) {
            if (mConfig.existsProperty(JBI_MODULE_PROPERTY))
                mConfig.setPropertyValue(JBI_MODULE_PROPERTY, aLevel);
            else
                mConfig.createProperty(JBI_MODULE_PROPERTY, aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Jaxws")) {
            if (mConfig.existsProperty(JAXWS_MODULE_PROPERTY))
                mConfig.setPropertyValue(JAXWS_MODULE_PROPERTY, aLevel);
            else
                mConfig.createProperty(JAXWS_MODULE_PROPERTY, aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("NodeAgent")) {
            mConfig.setNodeAgent(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Synchronization")) {
            mConfig.setSynchronization(aLevel);
        }
        else if (loggerTagName.equalsIgnoreCase("Gms")) {
            mConfig.setGroupManagementService(aLevel);
        }
    }


    /**
     *	<p> Retrieve the JBI Runtime System log levels.</p>
     *
     *	@param	target The target name.
     *	@param	instanceName The instance name.
     */
    public static TreeMap getJBIRuntimeLoggerLevels (String targetName,
                                                     String instanceName)
    {
        Map result = null;
        TreeMap treeMap = null;
        try
        {
            JBIAdminCommands mJac = BeanUtilities.getClient();
            if (null != mJac)
            {
            	if (sLog.isLoggable(Level.FINER)){
                sLog.finer("SystemLoggerUtilities - getJBIRuntimeLoggerLevels: " +
                          ", targetName=" + targetName +
                          ", instanceName=" + instanceName);
            	}
                result = mJac.getRuntimeLoggerLevels (targetName,
                                                      instanceName);
                treeMap = new TreeMap(result);
            }
        } catch (com.sun.jbi.ui.common.JBIRemoteException jbiRemoteEx)
        {
        	if (sLog.isLoggable(Level.FINE)){
            sLog.fine("SystemLoggerUtilities(): caught jbiRemoteEx=" + jbiRemoteEx);
        	}
        }
        return treeMap;
    }


    /**
     *	<p> Retrieve the JBI Runtime System log display name.</p>
     *
     *	@param	loggerName The logger name.
     *	@param	target The target name.
     *	@param	instanceName The instance name.
     */
    public static String getRuntimeDisplayName (String loggerName,
                                                String targetName,
                                                String instanceName)
    {
        String displayName = "";
        try
        {
            JBIAdminCommands mJac = BeanUtilities.getClient();
            if (null != mJac)
            {
            	if (sLog.isLoggable(Level.FINER)){
                sLog.finer("SystemLoggerUtilities - getRuntimeDisplayName: " +
                          ", loggerName=" + loggerName +
                          ", targetName=" + targetName +
                          ", instanceName=" + instanceName);
            	}
                displayName = mJac.getRuntimeLoggerDisplayName (loggerName,
                                                                targetName,
                                                                instanceName);
            }
        } catch (com.sun.jbi.ui.common.JBIRemoteException jbiRemoteEx)
        {
        	if (sLog.isLoggable(Level.FINE)){
            sLog.fine("SystemLoggerUtilities(): caught jbiRemoteEx=" + jbiRemoteEx);
        	}
        }
        return displayName;
    }


    /**
     *	<p> Set the JBI Runtime System log level.</p>
     *
     *	@param	loggerName The logger name.
     *	@param	logLevel The new log level value.
     *	@param	instanceName The instance name.
     *	@param	target The target name.
     */
    public static void setJBIRuntimeLoggerLevel (String loggerName,
                                                 String logLevelValueName,
                                                 String instanceName,
                                                 String targetName)
    {
        try
        {
            JBIAdminCommands mJac = BeanUtilities.getClient();
            if (null != mJac)
            {
                Level logLevel = null;
                if (!(logLevelValueName.equalsIgnoreCase(DEFAULT_NAME)))
                {
                    logLevel = Level.parse(logLevelValueName);
                }
                
                if (sLog.isLoggable(Level.FINER)){
                sLog.finer("SystemLoggerUtilities - setJBIRuntimeLoggerLevel: " +
                          ", loggerName=" + loggerName +
                          ", logLevelValueName=" + logLevelValueName +
                          ", instanceName=" + instanceName +
                          ", targetName=" + targetName);
                }
                
                //use instanceName only, the other method that uses targetName deprecated.
                mJac.setRuntimeLoggerLevel (loggerName,
                                            logLevel,                                            
                                            instanceName);
            }
        } catch (com.sun.jbi.ui.common.JBIRemoteException jbiRemoteEx)
        {
        	if (sLog.isLoggable(Level.FINE)){
            sLog.fine("SystemLoggerUtilities() - setJBIRuntimeLoggerLevel: caught jbiRemoteEx=" + jbiRemoteEx);
        	}
        }
    }

}

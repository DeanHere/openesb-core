/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  AppVarUtils.java
 *
 */

package com.sun.jbi.jsf.util;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.SharedConstants;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIManagementMessage;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * This class is used to provide utilitiies for the Component
 * Application Variable use-cases
 *
 **/

public final class AppVarUtils
{

    /**
     * Controls printing of diagnostic messages to the log
     */
    private static Logger sLog = JBILogger.getInstance();

    /**
     * deletes one or more application variables for the specified component
     * on the specified target
     * @param aComponentName String JBI Component name
     * @param anInstanceName String instance with target MBean for the component
     * @param anAppVarNamesArray String[] with one or more names to delete
     */
    public static Properties deleteCompAppVars(String aComponentName,
                           String anInstanceName,
                           String[] anAppVarNamesArray)
    {
    Properties result =
        new Properties();

    JBIManagementMessage mgmtMsg = 
        null;
    
    try
            {
        JBIAdminCommands jac = 
            BeanUtilities.getClient();
        
        String jacResult =
            jac.deleteApplicationVariables(aComponentName,
                           anInstanceName, 
                           anAppVarNamesArray);
        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("AppVarUtils.deleteCompAppVars(" + 
              aComponentName + ", " +
              anInstanceName + ", " + 
              anAppVarNamesArray+ "), jacResult=" + 
              jacResult);
        }
        
        }
    catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
        {
    	if (sLog.isLoggable(Level.FINE)){
        sLog.log(Level.FINE,
             ("AppVarUtils.deleteCompAppVars(" + 
              aComponentName + ", " +
              anInstanceName + ", " + 
              anAppVarNamesArray + "), caught jrEx=" + jrEx),
             jrEx);
    	}
    	
        }
    
    if (null != mgmtMsg)
        {
        if (mgmtMsg.isSuccessMsg())
            {
            result.put("success-result",
                   aComponentName);
            }
        else
            {
            String details = 
                mgmtMsg.getMessage();
            if (null == details)
                {
                details =
                    I18nUtilities
                    .getResourceString("jbi.internal.error.invalid.remote.exception");
                }
            result.put("failure-result",
                   details);
            }
        }
    
        if (sLog.isLoggable(Level.FINER)){
        sLog.finer("AppVarUtils.deleteCompAppVars(...), result=" +
          result);
        }
    
    return result;
    }




    /**
     * Gets the application variables for the specified component
     * on the specified target
     * @param aComponentName String JBI Component name
     * @param anInstanceName String instance with target MBean for the component
     * @returns Properties that contain a the name->[type]value properties
     */
    public static Properties getCompAppVars(String aComponentName,
                                            String anInstanceName,
                                            Properties aStatus)
    {
    
    Properties result
        = new Properties();

    JBIManagementMessage mgmtMsg = 
        null;

    try
            {
        JBIAdminCommands jac = 
            BeanUtilities.getClient();
        
        result =
            jac.getApplicationVariables(aComponentName,
                        anInstanceName);

        if (sLog.isLoggable(Level.FINER)){
        sLog.finer("AppVarUtils.getCompAppVars(" + 
              aComponentName + ", " +
              anInstanceName + "), result=" + 
              result);
        }
        
        aStatus.put("success-result",
                    aComponentName);

        }
    catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
        {
    	if (sLog.isLoggable(Level.FINE)){
        sLog.log(Level.FINE,
             ("AppVarUtils.getCompAppVars(" + 
              aComponentName + ", " +
              anInstanceName + "), caught jrEx=" + jrEx),
             jrEx);
    	}
        aStatus.put("failure-result",
                    jrEx);
        }

    return result;
    }


    /**
     * Saves a new application variable for the specified component
     * on the specified target
     * @param aComponentName String JBI Component name
     * @param anInstanceName String instance with target MBean for the component
     * @param anAppVarName String the new Application Variable name
     * @param anAppVarType String the new Application Variable type
     * @param anAppVarValue String the new Application Variable value or null
     * @returns Properties that contain a success-result key mapped to the 
     * Application Variable, or a failure-result key mapped to an 
     * I18n alert details message
     */
    public static Properties saveNewCompAppVar(String aComponentName,
                           String anInstanceName,
                           String anAppVarName,
                           String anAppVarType,
                           String anAppVarValue)
    {
    sLog.finer("AppVarUtils.saveNewCompAppVar(" +
          aComponentName + ", " +
          anInstanceName + ", " +
          anAppVarName + ", " +
          anAppVarType + ", " +
          anAppVarValue + ")");

    Properties result = 
        new Properties();

    String jacResult = null;

    JBIManagementMessage mgmtMsg = 
        null;

    // ensure variables have some default value
    if (null == anAppVarValue)
        {
        if (SharedConstants.APP_VAR_TYPE_BOOLEAN.equals(anAppVarType))
            {
            anAppVarValue = "false";
            }
        else if (SharedConstants.APP_VAR_TYPE_NUMBER.equals(anAppVarType))
            {
            anAppVarValue = "0";
            }
        else 
            {
            anAppVarValue = "";
            }
        }

    // ensure TRUE or FALSE when needed
    if (SharedConstants.APP_VAR_TYPE_BOOLEAN.equals(anAppVarType))
        {
        if ("true".equalsIgnoreCase(anAppVarValue))
            {
            anAppVarValue = "TRUE";
            }
        else
            {
            anAppVarValue = "FALSE";
            }
        }
    
    Properties aNewAppVarProps =
        new Properties();
    aNewAppVarProps.put(anAppVarName,
                (anAppVarType +
                 anAppVarValue));
    try
        {
        JBIAdminCommands jac = 
            BeanUtilities.getClient();

        jacResult = jac.addApplicationVariables(aComponentName,
                            anInstanceName,
                            aNewAppVarProps);

        if (sLog.isLoggable(Level.FINER)){
        sLog.finer("AppVarUtils.saveNewCompAppVar(...), jacResult=" +
              jacResult);
        }

        mgmtMsg =
            BeanUtilities.extractJBIManagementMessage(jacResult);
        }
    catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
        {
    	if (sLog.isLoggable(Level.FINE)){
        sLog.log(Level.FINE,
             ("AppVarUtils.saveNewCompAppVar(...) caught jrEx=" + 
              jrEx),
             jrEx);
    	}
        mgmtMsg =
            BeanUtilities.extractJBIManagementMessage(jrEx);
        }

    if (null != mgmtMsg)
        {
        if (mgmtMsg.isSuccessMsg())
            {
            result.put("success-result",
                   anAppVarName);
            }
        else
            {
            String details = 
                mgmtMsg.getMessage();
            if (null == details)
                {
                details =
                    I18nUtilities
                    .getResourceString("jbi.internal.error.invalid.remote.exception");
                }
            result.put("failure-result",
                   details);
            }
        }

        if (sLog.isLoggable(Level.FINER)){
        sLog.finer("AppVarUtils.saveNewCompAppVar(...), result=" +
          result);
        }

        return result;
    }

    /**
     * Saves a new application variable for the specified component
     * on the specified target
     * @param aComponentName String JBI Component name
     * @param anInstanceName String instance with target MBean for the component
     * @param aNewAppVarsProps Properties with new name->[type]value properties
     * @returns Properties that contain a success-result key mapped to the 
     * Application Variable, or a failure-result key mapped to an 
     * I18n alert details message
     */
    public static Properties update(String aComponentName,
                    String anInstanceName,
                    Properties aNewAppVarsProps)
    {
    if (sLog.isLoggable(Level.FINER)){
        sLog.finer("AppVarUtils.update(" +
          aComponentName + ", " +
          anInstanceName + ", " +
          aNewAppVarsProps + ")");
    }

    Properties result = 
        new Properties();

    JBIManagementMessage mgmtMsg = 
        null;

    String jacResult = null;

    try
        {
        JBIAdminCommands jac = 
            BeanUtilities.getClient();

        jacResult = jac.setApplicationVariables(aComponentName,
                            anInstanceName,
                            aNewAppVarsProps);

        if (sLog.isLoggable(Level.FINER)){
        sLog.finer("AppVarUtils.saveNewCompAppVar(...), jacResult=" +
              jacResult);
        }

        mgmtMsg =
            BeanUtilities.extractJBIManagementMessage(jacResult);
        }
    catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
        {
    	if (sLog.isLoggable(Level.FINE)){
        sLog.log(Level.FINE,
             ("AppVarUtils.saveNewCompAppVar(...) caught jrEx=" + 
              jrEx),
             jrEx);
    	}
        mgmtMsg =
            BeanUtilities.extractJBIManagementMessage(jrEx);
        }

    if (null != mgmtMsg)
        {
        if (mgmtMsg.isSuccessMsg())
            {
            result.put("success-result",
                   aComponentName);
            }
        else
            {
            String details = 
                mgmtMsg.getMessage();
            if (null == details)
                {
                details =
                    I18nUtilities
                    .getResourceString("jbi.internal.error.invalid.remote.exception");
                }
            result.put("failure-result",
                   details);
            }
        }

        if (sLog.isLoggable(Level.FINER)){
        sLog.finer("AppVarUtils.update(...), result=" +
          result);
        }

    return result;
    }

}



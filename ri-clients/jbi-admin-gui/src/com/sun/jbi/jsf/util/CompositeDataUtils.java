/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  CompositeDataUtils.java
 */
package com.sun.jbi.jsf.util;

import com.sun.jbi.ui.common.JBIStatisticsItemNames;
import com.sun.jbi.ui.common.JBITimeUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;

/**
 * Utilities to format alert messages
 *
 * @author   Sun Microsystems Inc.
 */

public final class CompositeDataUtils
{

    /**
     * @param aTableOfData    Description of Parameter
     * @param anInstanceName  Description of Parameter
     * @return                The DataForInstance value
     */
    public static CompositeData getDataForInstance(TabularData aTableOfData,
                                                   String anInstanceName)
    {
        CompositeData result =
            null;

        Collection values = null;
        if ((null != anInstanceName)
            && (null != aTableOfData))
            {
                values =
                    aTableOfData.values();
            }
        Iterator iterator =
            null;
        if (null != values)
            {
                iterator =
                    values.iterator();
            }
        instanceLoop:
        while ((null != iterator)
               && (iterator.hasNext()))
            {
                CompositeData compData =
                    (CompositeData) iterator.next();

                String compDataInstanceName =
                    (String)
                    compData.get(
                                 JBIStatisticsItemNames.INSTANCE_NAME);

                // If data is for a different instance, skip it
                if (!anInstanceName.equals(compDataInstanceName))
                    {
                        continue instanceLoop;
                    }
                else
                    {
                        result = compData;
                        break instanceLoop;
                    }
            }
        return result;
    }

    /**
     * Gets a String representation of a simple data item for the given <code>CompositeData</code>
     * for the specified key.
     *
     * @param aCompData  the composite data
     * @param anItemKey  the key to extract the object from the composite data
     * @return           the value of the specified item
     */
    public static long findCompositeCountOrZero(CompositeData aCompData,
                                                String anItemKey)
    {
        long result = 0L;

        if (null != aCompData)
            {
                CompositeType compType =
                    aCompData.getCompositeType();
                
                Set compItemSet =
                    compType.keySet();
                
                if (compItemSet.contains(anItemKey))
                    {
                        Object item =
                            aCompData.get(anItemKey);
                        
                        if ((null != item)
                            &&(item instanceof Long))
                            {
                                Long count =
                                    (Long) item;
                                result = count.longValue();
                            }
                    }
            }
        return result;
    }

    /**
     * Gets a String representation of a simple data item for the given <code>CompositeData</code>
     * for the specified key.
     *
     * @param aCompData  the composite data
     * @param anItemKey  the key to extract the object from the composite data
     * @return           the value of the specified item
     */
    public static String findCompositeItem(CompositeData aCompData,
                                           String anItemKey)
    {
        String result = null;
        
        if (null != aCompData)
            {

                CompositeType compType =
                    aCompData.getCompositeType();
                
                Set compItemSet =
                    compType.keySet();
                
                if (compItemSet.contains(anItemKey))
                    {
                        Object item =
                            aCompData.get(anItemKey);
                        
                        if (null != item)
                            {
                                result =
                                    item.toString();
                            }
                    }
            }
        return result;
    }


    /**
     * Gets a List data item for the given <code>CompositeData</code> for the
     * specified key.
     *
     * @param aCompData  the composite data
     * @param anItemKey  the key to extract the object from the composite data
     * @return           the value of the specified item as a List
     */
    public static CompositeData[] findCompositeArrayItem(CompositeData aCompData,
                                                         String anItemKey)
    {
        CompositeData[] result = null;

        CompositeType compType =
            aCompData.getCompositeType();

        Set compItemSet =
            compType.keySet();

        if (compItemSet.contains(anItemKey))
            {
                Object item =
                    aCompData.get(anItemKey);

                if ((null != item)
                    && (item instanceof CompositeData[]))
                    {
                        result = (CompositeData[])
                            item;
                    }
            }
        return result;
    }


    /**
     * Gets the response time from the given <code>CompositeData</code> for
     * the specified key.
     *
     * @param aCompData  the composite data
     * @param anItemKey  the key to extract the object from the composite data
     * @return           an <code>Object[]</code> containing the response time
     * or return <code>null</code> if the response time could not be determined.
     */
    public static Object[] findCompositeResponseTime(CompositeData aCompData,
                                                     String anItemKey)
    {
        Object[] result = null;

        if (null != aCompData)
            {
                CompositeType compType =
                    aCompData.getCompositeType();
                
                Set compItemSet =
                    compType.keySet();
                
                if (compItemSet.contains(anItemKey))
                    {
                        Object itemVal =
                            aCompData.get(anItemKey);
                        if ((null != itemVal)
                            &&(itemVal instanceof Long))
                            {
                                Long responseTime =
                                    (Long) itemVal;
                                result = 
                                    new Object[]
                                    {
                                        responseTime,
                                    };
                            }
                    }
            }
        return result;
    }

    /**
     * Gets the response Date from the given <code>CompositeData</code> for
     * the specified key.
     *
     * @param aCompData  the composite data
     * @param anItemKey  the key to extract the object from the composite data
     * @return           an <code>Date</code> containing the response time
     */
    public static Date findCompositeDate(CompositeData aCompData,
                                         String anItemKey)
    {
        Date result = null;

        if (null != aCompData)
            {
                CompositeType compType =
                    aCompData.getCompositeType();
                
                Set compItemSet =
                    compType.keySet();
                
                if (compItemSet.contains(anItemKey))
                    {
                        Object item =
                            aCompData.get(anItemKey);
                        
                        if ((null != item)
                            && (item instanceof Date))
                            {
                                result =
                                    (Date) item;
                            }
                    }
            }
        return result;
    }

    /**
     * Gets the response Date from the given <code>CompositeData</code> for
     * the specified key.
     *
     * @param aCompData  the composite data
     * @param anItemKey  the key to extract the object from the composite data
     * @return           the possibly empty String array
     */
    public static String[] findCompositeStringArray(CompositeData aCompData,
                                                    String anItemKey)
    {
        String[] result = 
            new String[0];
        
        if (null != aCompData)
            {
                CompositeType compType =
                    aCompData.getCompositeType();
                
                Set compItemSet =
                    compType.keySet();
                
                if (compItemSet.contains(anItemKey))
                    {
                        Object o =
                            aCompData.get(anItemKey);
                        if ((null != o)
                            &&(o instanceof String[]))
                            {
                                result =
                                    (String[]) o;
                            }
                    }
            }
        return result;
    }

    /**
     * Gets an elapsed time from the given <code>CompositeData</code> using
     * the specified key.
     *
     * @param aCompData  the composite data
     * @param anItemKey  the key to extract the object from the composite data
     * @return           an <code>Object[]</code> of time unit inserts (days,
     *      hours, minutes, and seconds)
     */
    public static Object[] findCompositeUpTime(CompositeData aCompData,
                                               String anItemKey)
    {
        Object[] result = null;

        if (null != aCompData)
            {
                CompositeType compType =
                    aCompData.getCompositeType();
                
                Set compItemSet =
                    compType.keySet();
                
                if (compItemSet.contains(anItemKey))
                    {
                        Long upTime =
                            (Long) aCompData.get(anItemKey);
                        
                        if (null != upTime)
                            {
                                JBITimeUtil elapsedTime =
                                    new JBITimeUtil(upTime.longValue());
                                
                                result =
                                    new Object[]
                                    {
                                        elapsedTime.getDays(),
                                        elapsedTime.getHours(),
                                        elapsedTime.getMinutes(),
                                        elapsedTime.getSeconds(),
                                    };
                            }
                    }
            }
        return result;
    }

    /**
     * Controls printing of diagnostic messages to the log
     */
    private static Logger sLog = JBILogger.getInstance();


    /**
     * Utility class - constructor not used.
     */
    private CompositeDataUtils()
    {
    }

}

/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  JSFUtils.java
 */
package com.sun.jbi.jsf.util;

import com.sun.data.provider.FieldKey;
import com.sun.data.provider.RowKey;
import com.sun.data.provider.impl.ObjectListDataProvider;
import com.sun.enterprise.tools.admingui.util.GuiUtil;
import com.sun.jbi.jsf.bean.ConfigProperty;
import com.sun.jbi.jsf.bean.ConfigPropertyGroup;
import com.sun.jbi.jsf.bean.EndpointStatsBean;
import com.sun.jbi.jsf.bean.Row;
import com.sun.jbi.ui.common.JBIStatisticsItemNames;
import com.sun.webui.jsf.component.Button;
import com.sun.webui.jsf.component.Checkbox;
import com.sun.webui.jsf.component.DropDown;
import com.sun.webui.jsf.component.HelpInline;
import com.sun.webui.jsf.component.HiddenField;
import com.sun.webui.jsf.component.Label;
import com.sun.webui.jsf.component.Listbox;
import com.sun.webui.jsf.component.PanelGroup;
import com.sun.webui.jsf.component.PasswordField;
import com.sun.webui.jsf.component.Property;
import com.sun.webui.jsf.component.PropertySheet;
import com.sun.webui.jsf.component.PropertySheetSection;
import com.sun.webui.jsf.component.StaticText;
import com.sun.webui.jsf.component.Table;
import com.sun.webui.jsf.component.TableColumn;
import com.sun.webui.jsf.component.TableRowGroup;
import com.sun.webui.jsf.component.TextField;
import com.sun.webui.jsf.model.Option;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.TabularData;
import java.util.logging.Level;

/**
 * Provides JavaServer Faces and JSFTemplating related utility methods.
 *
 * @author   Sun Microsystems Inc.
 */
public final class JSFUtils
{

    /**
     * @param aConfigPS an input <code>PropertySheet</code> with 
     * component application configuration related input widgets to be 
     * processed for user-viewed/edited values.
     * @param aConfigProperties  a <code>Properties</code> object to be 
     * updated with field names as keys and field values as values.
     */
    public static void decodeCompAppConfigProperties(PropertySheet aConfigPS,
                                                     Properties aConfigProperties)
    {
    	if (sLog.isLoggable(Level.FINER)){
            sLog.finer("JSFUtils.decodeCompAppConfigProperties()" +
                  ", aConfigPS=" + aConfigPS + 
                  ", aConfigProperties=*hidden*");
    	}
    	
        decodeCompConfigProperties(aConfigPS,
                                   aConfigProperties);
    }
        

    /**
     * @param aConfigPS an input <code>PropertySheet</code> with 
     * component configuration related input widgets to be processed 
     * for user-viewed/edited values.
     * @param aConfigProperties  a <code>Properties</code> object to be 
     * updated with field names as keys and field values as values.
     */
    public static void decodeCompConfigProperties(PropertySheet aConfigPS,
                                                  Properties aConfigProperties)
    {
    	if (sLog.isLoggable(Level.FINER)){
            sLog.finer("JSFUtils.decodeCompConfigProperties()" +
                  ", aConfigPS=" + aConfigPS + 
                  ", aConfigProperties=*hidden*");
    	}
        
        List sections = aConfigPS.getChildren();
        for (int i = 0; i < sections.size(); ++i)
            {
                PropertySheetSection section =
                    (PropertySheetSection) sections.get(i);
                
                if (sLog.isLoggable(Level.FINEST)){
                   sLog.finest("JSFUtils.decodeCompConfigProps(), section=" + section);
                }

                List properties = section.getChildren();
                for (int j = 0; j < properties.size(); ++j)
                    {
                        Property property =
                            (Property) properties.get(j);
                        
                        if (sLog.isLoggable(Level.FINEST)){
                           sLog.finest("JSFUtils.decodeCompConfigProps(), property=" + property);
                        }

                        String propKey = null;
                        String propValue = null;
                        ArrayList tableRows =
                            new ArrayList();
                        List widgets = property.getChildren();
                        for (int k = 0; k < widgets.size(); ++k)
                            {
                                UIComponent widget =
                                    (UIComponent) widgets.get(k);
                                
                                if (sLog.isLoggable(Level.FINEST)){
                                  sLog.finest("JSFUtils.decodeCompConfigProps(), widget=" + widget);
                                }
                                
                                // Note: order of checks is important
                                if (widget instanceof Table)
                                    {
                                	    if (sLog.isLoggable(Level.FINEST)){
                                           sLog.finest("JSFUtils.decodeCompConfigProps(), table");
                                	    }
                                	    
                                        propValue = "";
                                        String rowKey = "";
                                        String rowVal = "";
                                        List<UIComponent> children =
                                            ((Table)widget).getChildren();
                                        for (UIComponent child:children)
                                            {
                                                if (child instanceof TableRowGroup)
                                                    {
                                                        TableRowGroup trg =
                                                            (TableRowGroup) child;
                                                        RowKey[] renderedRows =
                                                            trg.getRenderedRowKeys();
                                                        if ((null != renderedRows)
                                                            &&(0 == renderedRows.length))
                                                            {
                                                                continue;
                                                            }

                                                        ObjectListDataProvider oldp = 
                                                            (ObjectListDataProvider) 
                                                            ((TableRowGroup)child).getSourceData();
                                                        if (null == oldp)
                                                            {
                                                                continue;
                                                            }
                                                        oldp.commitChanges();

                                                        FieldKey fkName = 
                                                            oldp
                                                            .getFieldKey(SharedConstants
                                                                         .KEY_NAME);
                                                        FieldKey fkValue = 
                                                            oldp
                                                            .getFieldKey(SharedConstants
                                                                         .KEY_VALUE);
                                                        
                                                        RowKey[] rowKeys = 
                                                            trg.getRowKeys();
                                                        for(int cnt = 0; 
                                                            cnt < rowKeys.length; 
                                                            ++cnt)
                                                            {
                                                                Properties tableRowProperties =
                                                                    new Properties(); 

                                                                rowKey = (String) oldp
                                                                    .getValue(fkName, 
                                                                              rowKeys[cnt]);
                                                                
                                                                tableRowProperties
                                                                    .setProperty(SharedConstants
                                                                                 .KEY_NAME, 
                                                                                 rowKey);
                                                                
                                                                rowVal = (String) oldp
                                                                    .getValue(fkValue, 
                                                                              rowKeys[cnt]);
                                                                
                                                                tableRowProperties
                                                                    .setProperty(SharedConstants
                                                                                 .KEY_VALUE, 
                                                                                 rowVal);

                                                                tableRows
                                                                    .add(tableRowProperties);
                                                            }
                                                    }
                                            }
                                        if (sLog.isLoggable(Level.FINEST)){
                                           sLog.finest("JSFUtils.decodeCompConfigProps()" +
                                                  ", Table propValue=" + propValue);
                                        }
                                    }
                                else if (widget instanceof Label)
                                    {
                                	    if (sLog.isLoggable(Level.FINEST)){
                                           sLog.finest("JSFUtils.decodeCompConfigProps(), skip label=" +
                                                  (String) ((Label) widget).getText());
                                	    }
                                    }
                                else if (widget instanceof DropDown)
                                    {
                                        propValue =
                                            (String) ((DropDown) widget).getValue();
                                        
                                        if (sLog.isLoggable(Level.FINEST)){
                                            sLog.finest("JSFUtils.decodeCompConfigProps()" +
                                                  ", DropDown propValue=" + propValue);
                                        }
                                    }
                                else if (widget instanceof PasswordField)
                                    {
                                        propValue =
                                            (String) ((PasswordField) widget).getValue();
                                        
                                        if (sLog.isLoggable(Level.FINEST)){
                                           sLog.finest("JSFUtils.decodeCompConfigProps()" +
                                                  ", PasswordField propValue=*hidden*");
                                        }
                                    }
                                else if (widget instanceof TextField)
                                    {
                                        propValue =
                                            (String) ((TextField) widget).getText();
                                        
                                        if (sLog.isLoggable(Level.FINEST)){
                                           sLog.finest("JSFUtils.decodeCompConfigProps()" +
                                                  ", TextField propValue=" + propValue);
                                        }
                                    }
                                else if (widget instanceof HiddenField)
                                    {
                                        propKey =
                                            (String) ((HiddenField) widget).getText();
                                        
                                        if (sLog.isLoggable(Level.FINEST)){
                                           sLog.finest("JSFUtils.decodeCompConfigProps()" +
                                                  ", propKey=" + propKey);
                                        }
                                    }
                            }
                        if (null != propKey)
                            {
                                // If processing a non-nested (non-array) property
                                // adds the property key and value
                                if (0 == tableRows.size())
                                    {
                                        if (null != propValue)
                                            {
                                        	    if (sLog.isLoggable(Level.FINEST)){
                                                   sLog.finest("JSFUtils.decodeCompConfigProps()" +
                                                          ", aConfigProperties.put(" + propKey +
                                                          ", " + propValue + ")");
                                        	    }
                                        	    
                                                aConfigProperties.put(propKey,
                                                                      propValue);
                                            }
                                    }
                                // If processing a nested (array) property
                                // adds the property key and value for each row in the table
                                else
                                    {
                                        String propKeyRoot = 
                                            propKey;
                                        for (int k = 0; k < tableRows.size(); ++k)
                                            {
                                                propKey = 
                                                    propKeyRoot + "." + k;
                                                
                                                Properties tableRowProperties =
                                                    (Properties)
                                                    tableRows.get(k);

                                                String rowKey =
                                                    (String) tableRowProperties
                                                    .getProperty(SharedConstants
                                                                 .KEY_NAME);
                                                String rowValue =
                                                    (String) tableRowProperties
                                                    .getProperty(SharedConstants
                                                                 .KEY_VALUE);

                                                propValue = rowKey +
                                                    "=" + rowValue;

                                                if (sLog.isLoggable(Level.FINEST)){
                                                   sLog.finest("JSFUtils.decodeCompConfigProps()" +
                                                          ", (tableRows) k=" + k +
                                                          ", propKey=" + propKey +
                                                          ", propValue=" + propValue);
                                                }
                                                
                                                aConfigProperties.put(propKey,
                                                                      propValue);
                                            }
                                    }
                            }
                    }
            }
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JSFUtils.decodeCompConfigProps(), aConfigProperties=*hidden*");
        }
    }

    /**
     * creates a Component Application Configuration Propery Sheet
     * @param anInstanceName the name of a running instance
     * @param aCompName the name of a started component
     * @param aCompType one of: <code>binding-component</code> or
     * <code>service-engine</code>
     * @param aCompAppConfigName an application configuration name
     * @param aListOfGroups a list of <code>ConfigPropertyGroup</code>
     * objects for this component's application configuration properties
     * @return  a <code>PropertySheet</code> of input widgets for viewing
     * and editing the component application configuration properties
     */
    public static PropertySheet getCompAppCompConfigPS(String anInstanceName,
                                                       String aCompName,
                                                       String aCompType,
                                                       String aCompAppConfigName,
                                                       List<ConfigPropertyGroup> aListOfGroups)
    {
        PropertySheet result =
            createPS("compAppConfig");

        Properties compAppConfigProps =
            null;

        if (!"".equals(aCompAppConfigName))
            {
                compAppConfigProps =
                    JBIUtils.getCompAppConfigProps(aCompName,
                                                   anInstanceName,
                                                   aCompAppConfigName);
            }

        addPropertiesToCompConfigPS(aCompName,
                                    aCompType,
                                    ESBConstants.CONFIG_TYPE_COMP_APP,
                                    anInstanceName,
                                    aListOfGroups,
                                    compAppConfigProps,
                                    result);

        return result;
    }

    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for
     * component installation configuration properties
     *
     * @param aCompName              the component name
     * @param aCompType              Description of Parameter
     * @param aListOfPropertyGroups  Description of Parameter
     * @return                       a property sheet with editable
     *      installation configuration properties
     */
    //@SuppressWarnings("unchecked")
    public static PropertySheet getCompInstallationConfigPS(String aCompName,
                                                            String aCompType,
                                                            List<ConfigPropertyGroup> aListOfPropertyGroups)
    {
        PropertySheet result =
            createPS("compInstallationConfig");
 
        Properties unusedCurrentValues = null;
        String unusedInstanceName = null;

        // add summary pss

        // if cluster-profile, add target pss

        addPropertiesToCompConfigPS(aCompName,
                                    aCompType,
                                    ESBConstants.CONFIG_TYPE_COMP_INSTALL,
                                    unusedInstanceName,
                                    aListOfPropertyGroups,
                                    unusedCurrentValues,
                                    result);

        return result;
    }

    

    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for
     * component runtime component configuration properties
     *
     * @param anInstanceName   the name of a running instance
     * @param aCompName        the name of a started component
     * @param aCompType        one of: <code>binding-component</code> or
     * <code>service-engine</code>
     * @param aListOfPropertyGroups the specified component's
     * runtime configuration properties in one or more groups
     * @return                 a <code>PropertySheet</code> to view or
     * edit the component's runtime configuration properties
     */
    public static PropertySheet getCompRuntimeCompConfigPS(String anInstanceName,
                                                           String aCompName,
                                                           String aCompType,
                                                           List<ConfigPropertyGroup> aListOfPropertyGroups)
    {
        PropertySheet result =
            createPS("compRuntimeConfig");

        Properties compConfigProps =
            JBIUtils.getCompConfigProps(aCompName,
                                        anInstanceName);

        addPropertiesToCompConfigPS(aCompName,
                                    aCompType,
                                    ESBConstants.CONFIG_TYPE_COMP_RUNTIME,
                                    anInstanceName,
                                    aListOfPropertyGroups,
                                    compConfigProps,
                                    result);

        return result;
    }

    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for
     * component configuration properties when they are unavailable
     *
     * @param aConfigType one of <code>config-type-comp-app</code>,
     * <code>config-type-comp-install</code> or
     * <code>config-type-comp-runtime</code>
     * @param aCompName the JBI component name
     * @param aCompType one of <code>binding-component</code> or
     * <code>service-engine</code>
     * @return  a placeholder property sheet with a warning message.
     */
    //@SuppressWarnings("unchecked")
    public static PropertySheet getConfigUnavailablePS(String aConfigType,
                                                       String aCompName,
                                                       String aCompType)
    {
        PropertySheet result =
            getPlaceholderPS(aConfigType,
                             aCompName,
                             aCompType,
                             "jbi.config.props.not.available.prop");
        return result;
    }

    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for
     * component configuration properties when there is no configuration
     *
     * @param aConfigType one of <code>config-type-comp-app</code>,
     * <code>config-type-comp-install</code> or
     * <code>config-type-comp-runtime</code>
     * @param aCompName the JBI component name
     * @param aCompType one of <code>binding-component</code> or
     * <code>service-engine</code>
     * @return  a placeholder property sheet with a warning message.
     */
    //@SuppressWarnings("unchecked")
    public static PropertySheet getNoConfigPS(String aConfigType,
                                              String aCompName,
                                              String aCompType)
    {
        PropertySheet result =
            getPlaceholderPS(aConfigType,
                             aCompName,
                             aCompType,
                             "jbi.config.props.not.applicable.prop");
        return result;
    }

    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for
     * component configuration properties when a warning message needs to
     * be displayed (instead of providing editiable fields)
     *
     * @param aConfigType one of <code>config-type-comp-app</code>,
     * <code>config-type-comp-install</code> or
     * <code>config-type-comp-runtime</code>
     * @param aCompName the JBI component name
     * @param aCompType one of <code>binding-component</code> or
     * <code>service-engine</code>
     * @param aMessageKey used to provide the warning message
     * @return  a placeholder property sheet with a warning message.
     */
    //@SuppressWarnings("unchecked")
    public static PropertySheet getPlaceholderPS(String aConfigType,
                                                 String aCompName,
                                                 String aCompType,
                                                 String aMessageKey)
    {
        PropertySheet result =
            createPS("placeholderConfig");

        String displayName =
            I18nUtilities.getResourceString("jbi.config.props.general.section.display.name");

        String displayDescription =
            I18nUtilities.getResourceString("jbi.config.props.general.section.display.description");
        if (ESBConstants.CONFIG_TYPE_COMP_APP.equals(aConfigType))
            {
                displayDescription =
                    I18nUtilities.getResourceString("jbi.config.props.general.section.app.display.description");
            }


        Object[] titleArgs =
            {
                displayName,
                displayDescription,
            };

        String sectionTitle =
            GuiUtil.getMessage(
                               I18nUtilities.getResourceString("jbi.config.props.section.title"),
                               titleArgs);

        PropertySheetSection sum =
            createPSS("sum",
                      sectionTitle,
                      result);

        String configTypeInsert = 
            I18nUtilities.getResourceString("jbi.config.props.type.runtime");

        if (ESBConstants.CONFIG_TYPE_COMP_APP.equals(aConfigType))
            {
                configTypeInsert =
                    I18nUtilities.getResourceString("jbi.config.props.type.app");
            }
        else if (ESBConstants.CONFIG_TYPE_COMP_INSTALL.equals(aConfigType))
            {
                configTypeInsert =
                    I18nUtilities.getResourceString("jbi.config.props.type.install");
            }

        Object[] propArgs =
            {
                aCompName,
                configTypeInsert,
            };

        String noConfigProps =
            I18nUtilities.getResourceString("jbi.config.props.no.config.props");

        String i18nMessageWithInserts = 
            I18nUtilities.getResourceString(aMessageKey);

        String placeholderProp =
            GuiUtil.getMessage(i18nMessageWithInserts,
                               propArgs);

        Property info =
            createStaticTextProperty("info",
                                     noConfigProps,
                                     placeholderProp,
                                     sum);
        return result;
    }


    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for the
     * Component Statistics
     *
     * @param aTableOfCompStats a table of component statistics
     * @param aConsumingEndpointsTD consuming endpoints tabular data
     * @param aProvidingEndpointsTD providing endpoints tabular data
     * @param aCompName the component name for these statistics
     * @param anInstanceName display statistics only for this instance
     * @return a property sheet for viewing the Component Statistics
     */
    public static PropertySheet getCompStatsPS(TabularData aTableOfCompStats,
                                               TabularData aConsumingEndpointsTD,
                                               TabularData aProvidingEndpointsTD,
                                               String aCompName,
                                               String anInstanceName)
    {
    	if (sLog.isLoggable(Level.FINER)){
    	   sLog.finer("JSFUtils.getCompStatsPS(" +
                  aTableOfCompStats + ", " +
                  aConsumingEndpointsTD + ", " +
                  aProvidingEndpointsTD + ", " +
                  aCompName + ", " +
                  anInstanceName + ")");
    	}

        PropertySheet result =
            createPS("compStatsPS");

        CompositeData compData =
            CompositeDataUtils.getDataForInstance(aTableOfCompStats,
                                                  anInstanceName);

        if (null != compData)
            {

            CompositeType compType = compData.getCompositeType();
            Set compItemSet        = compType.keySet();

                FacesContext facesContext =
                    FacesContext.getCurrentInstance();

                //
                // property sheet section for Summary Statistics
                //
                PropertySheetSection pss1 =
                    createPSS("compStatsPss1",
                              "jbi.comp.stats.props.summary.pss.label",
                              result);

                Property compName =
                    createStaticTextProperty("compName",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.name.label"),
                                             aCompName,
                                             pss1);
                Property instanceName =
                    createStaticTextProperty("instanceName",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.instance.name.label"),
                                             anInstanceName,
                                             pss1);

                Object[] componentUpTime =
                    CompositeDataUtils.findCompositeUpTime(compData,
                                                           JBIStatisticsItemNames.COMPONENT_UPTIME);
                String upTimeValue =
                    GuiUtil.getMessage(
                                       I18nUtilities.getResourceString("jbi.stats.uptime.format.dd.hh.mm.ss"),
                                       componentUpTime);

                Property compUpTimeProperty =
                    createStaticTextProperty("compUpTime",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.uptime.label"),
                                             upTimeValue,
                                             pss1);

                String activeEndpoints =
                    CompositeDataUtils.findCompositeItem(compData,
                                                         JBIStatisticsItemNames.NUMBER_OF_ACTIVATED_ENDPOINTS);
                Property compActiveEndpoints =
                    createStaticTextProperty("compActiveEndpoints",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.active.endpoints.label"),
                                             activeEndpoints,
                                             pss1);

                String completedExchanges =
                    CompositeDataUtils.findCompositeItem(compData,
                                                         JBIStatisticsItemNames.NUMBER_OF_COMPLETED_EXCHANGES);

                Property compCompletedExchanges =
                    createStaticTextProperty("compCompletedExchanges",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.completed.exchanges.label"),
                                             completedExchanges,
                                             pss1);
                String activeExchanges =
                    CompositeDataUtils.findCompositeItem(compData,
                                                         JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES);
                Property compActiveExchanges =
                    createStaticTextProperty("compActiveExchanges",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.active.exchanges.label"),
                                             activeExchanges,
                                             pss1);
                String activeExchangesMax =
                    CompositeDataUtils.findCompositeItem(compData,
                                                         JBIStatisticsItemNames.NUMBER_OF_ACTIVE_EXCHANGES_MAX);
                Property compActiveExchangesMax =
                    createStaticTextProperty("compActiveExchangesMax",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.active.exchanges.max.label"),
                                             activeExchangesMax,
                                             pss1);
                String queuedExchanges =
                    CompositeDataUtils.findCompositeItem(compData,
                                                         JBIStatisticsItemNames.NUMBER_OF_QUEUED_EXCHANGES);
                Property compQueuedExchanges =
                    createStaticTextProperty("compQueuedExchanges",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.queued.exchanges.label"),
                                             queuedExchanges,
                                             pss1);
                String queuedExchangesMax =
                    CompositeDataUtils.findCompositeItem(compData,
                                                         JBIStatisticsItemNames.NUMBER_OF_QUEUED_EXCHANGES_MAX);
                Property compQueuedExchangesMax =
                    createStaticTextProperty("compQueuedExchangesMax",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.queued.exchanges.max.label"),
                                             queuedExchangesMax,
                                             pss1);
                String errorExchanges =
                    CompositeDataUtils.findCompositeItem(compData,
                                                         JBIStatisticsItemNames.NUMBER_OF_ERROR_EXCHANGES);
                Property compErrorExchanges =
                    createStaticTextProperty("compErrorExchanges",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.error.exchanges.label"),
                                             errorExchanges,
                                             pss1);

                //
                // property sheet section for Message Exchange Statistics
                //
                PropertySheetSection pss2 =
                    createPSS("compStatsPss2",
                              "jbi.comp.stats.props.msg.exchange.pss.label",
                              result);

                String compMEResponseTimeValue =
                    formatTime(compData,
                               JBIStatisticsItemNames
                               .MESSAGE_EXCHANGE_RESPONSE_TIME,
                               VALUE_NOT_AVAILABLE);

                Property compMEResponseTime =
                    createStaticTextProperty("compMEResponseTime",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.me.response.time.label"),
                                             compMEResponseTimeValue,
                                             pss2);

                String compMEResponseTimeHelpText =
                    compData.getCompositeType()
                    .getDescription(
                                    JBIStatisticsItemNames.MESSAGE_EXCHANGE_RESPONSE_TIME);

                compMEResponseTime.setHelpText(compMEResponseTimeHelpText);

                String compMEInCompTimeValue =
                    formatTime(compData,
                               JBIStatisticsItemNames
                               .MESSAGE_EXCHANGE_COMPONENT_TIME,
                               VALUE_NOT_AVAILABLE);

                Property compMEInCompTime =
                    createStaticTextProperty("compMEInCompTime",
                                             I18nUtilities
                                             .getResourceString("jbi.comp.stats.props.comp.me.in.component.label"),
                                             compMEInCompTimeValue,
                                             pss2);

                String compMEInCompTimeHelpText =
                    compData.getCompositeType()
                    .getDescription(
                                    JBIStatisticsItemNames.MESSAGE_EXCHANGE_COMPONENT_TIME);

                compMEInCompTime.setHelpText(compMEInCompTimeHelpText);

                String compMEInDeliveryChannelTimeValue =
                    formatTime(compData,
                               JBIStatisticsItemNames
                               .MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME,
                               VALUE_NOT_AVAILABLE);

                String deliveryChannelKey =
                    "jbi.comp.stats.props.comp.me.in.delivery.channel.label";
                Property compMEInDeliveryChannelTime =
                    createStaticTextProperty("compMEInDeliveryChannelTime",
                                             I18nUtilities
                                             .getResourceString(deliveryChannelKey),
                                             compMEInDeliveryChannelTimeValue,
                                             pss2);

                String compMEInDeliveryChannelTimeHelpText =
                    compData.getCompositeType()
                    .getDescription(
                                    JBIStatisticsItemNames.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME);

                compMEInDeliveryChannelTime.setHelpText(compMEInDeliveryChannelTimeHelpText);

                String compMEInMessageServiceTimeValue =
                    formatTime(compData,
                               JBIStatisticsItemNames
                               .MESSAGE_EXCHANGE_NMR_TIME,
                               VALUE_NOT_AVAILABLE);

                String inMessageServiceKey =
                    "jbi.comp.stats.props.comp.me.in.message.service.label";
                Property compMEInMessageServiceTime =
                    createStaticTextProperty("compMEInMessageServiceTime",
                                             I18nUtilities
                                             .getResourceString(inMessageServiceKey),
                                             compMEInMessageServiceTimeValue,
                                             pss2);

                String compMEInMessageServiceTimeHelpText =
                    compData.getCompositeType()
                    .getDescription(
                                    JBIStatisticsItemNames.MESSAGE_EXCHANGE_NMR_TIME);

                compMEInMessageServiceTime.setHelpText(compMEInMessageServiceTimeHelpText);

                //
                // property sheet section for Endpoint Statistics
                //
                PropertySheetSection pss3 =
                    createPSS("compStatsPss3",
                              "jbi.comp.stats.props.endpoint.pss.label",
                              result);

                Property endpointStats =
                    createProperty("endpointStats",
                                   null,
                                   "",
                                   false,
                                   pss3);
                
                CompositeData consumingEndpointsCD =
                    CompositeDataUtils.getDataForInstance(aConsumingEndpointsTD,
                                                          anInstanceName);

                CompositeData providingEndpointsCD =
                    CompositeDataUtils.getDataForInstance(aProvidingEndpointsTD,
                                                          anInstanceName);

                endpointStats.getChildren()
                    .add(addEndpointStatisticsTable(consumingEndpointsCD,
                                                    providingEndpointsCD,
                                                    aCompName,
                                                    anInstanceName));


                //
                // property sheet section for Componnt Provided Statistics
                //
                if (compItemSet.contains(JBIStatisticsItemNames.COMPONENT_EXTENSION_STATS))
                {
                    PropertySheetSection pss5 =
                        createPSS("compStatsPss5",
                                  "jbi.comp.stats.props.component.extension",
                                  result);

                    Property componentExtensionStats =
                        createProperty("componentExtensionStats",
                                       null,
                                       "",
                                       false,
                                       pss5);

                    CompositeData  componentExtensionStatsCD = 
                        (CompositeData) compData.get(JBIStatisticsItemNames.
                                                        COMPONENT_EXTENSION_STATS);
                    if (componentExtensionStatsCD != null)
                    {
                        componentExtensionStats.getChildren()
                            .add(addComponentExtensionStatisticsTable(componentExtensionStatsCD));
                    }

                }

            }

        return result;
    }

    
    /**
     * creates a HashMap used to construct TableRowGroups
     * for the component extension statistics.
     * @param aStatsReport the source data
     * @param aGroupName table row group name
     * @param aTableRowGroupMap to store the info into
     * @return the table row group info
     */
    private static HashMap constructCompositeDataTable (TabularData aStatsReport,
                                                     String aGroupName,
                                                     HashMap aTableRowGroupMap)
    {
        HashMap hashMap = null;
        Set names = aStatsReport.keySet();
        for ( Object name : names )
        {
            Object[] key = ( (java.util.List) name).toArray();
            CompositeData compData = aStatsReport.get(key);
            hashMap = constructCompositeDataTable (compData, 
                                                   aGroupName, 
                                                   aTableRowGroupMap,
                                                   1);
        }
        return hashMap;
    }        


    /**
     * Will create a HashMap that will be used to construct the TableRowGroups 
     * for the component extension statistics.
     *
     * @param aCompData            Composit Data
     * @param aGroupName           The table row group name
     * @param aTableRowGroupMap    The HashMap that will be uses to store the info in
     * @param aRecursiveLevel      The recursion level
     * @return                     a HashMap contain the TableRowGroup info
     */
    private static HashMap constructCompositeDataTable (CompositeData aCompData,
                                                        String aGroupName,
                                                        HashMap aTableRowGroupMap,
                                                        int aRecursiveLevel)
    {

        aRecursiveLevel++;
        if (aRecursiveLevel > 1) {
            aGroupName = aGroupName + "_" + aRecursiveLevel;
        }

        // If the groupMap does not currently exist, then create it.
        HashMap groupMap = (HashMap)aTableRowGroupMap.get(aGroupName);
        if (groupMap == null) {
            groupMap = new HashMap();
        }

        Hashtable arrayHashTable = new Hashtable();
        CompositeType compType = aCompData.getCompositeType();
        Set compItemSet = compType.keySet();
        Iterator itr = compItemSet.iterator();

        while (itr.hasNext())
        {
            String keyName = (String) itr.next();
            OpenType openType = compType.getType(keyName);
            Object itemValue = aCompData.get(keyName);
            String className = null;
            if (itemValue != null)
            {
                className = itemValue.getClass().getName();
            }
            
            if (openType.isArray())
            {
                if ((openType.getTypeName() != null) &&
                    (openType.getTypeName().compareTo("[Ljava.lang.String;") == 0))
                {
                    String [] strArray = (String []) aCompData.get(keyName);
                    ArrayList arrayList = new ArrayList();
                    for (int i=0; i < strArray.length; ++i)
                    {   arrayList.add(strArray[i]);
                    }
                    arrayHashTable.put(keyName,arrayList);
                }
                else if ((openType.getTypeName() != null) &&       
                        (openType.getTypeName().compareTo("[Ljavax.management.openmbean.CompositeData;") == 0))
                {
                    CompositeData [] compDataArray = (CompositeData []) aCompData.get(keyName);
                    for (int i=0; i < compDataArray.length; ++i)
                    {
                        constructCompositeDataTable(compDataArray[i], 
                                                    aGroupName, 
                                                    aTableRowGroupMap,
                                                    aRecursiveLevel);
                    }
                }
            }

            else
            {
                if (className != null &&
                    (className.equals("javax.management.openmbean.CompositeDataSupport") || 
                    className.equals("javax.management.openmbean.CompositeData")))
                {
                    constructCompositeDataTable((CompositeData) aCompData.get(keyName), 
                                                aGroupName, 
                                                aTableRowGroupMap,
                                                aRecursiveLevel);
                }
                else if (className != null &&
                    (className.equals("javax.management.openmbean.TabularDataSupport") || 
                    className.equals("javax.management.openmbean.TabularData")))
                {
                    constructCompositeDataTable((TabularData)aCompData.get(keyName), 
                                                aGroupName, 
                                                aTableRowGroupMap);
                }
                else
                {
                    String value = "" + itemValue;
                    groupMap.put(keyName, value);
                }
            }
        }

        // Place all the array entries into their own HashMap, then place
        // the HashMap into the aTableRowGroupMap.
        for (Enumeration e = arrayHashTable.keys(); e.hasMoreElements();) {
            String arrayName = (String)e.nextElement();
            ArrayList list = (ArrayList)arrayHashTable.get(arrayName);
            HashMap arrayMap = new HashMap();
            for (int i=0; i<list.size(); i++) {
                String arrayKey   = "[" + i + "]";
                String arrayValue = (String)list.get(i);
                arrayMap.put(arrayKey, arrayValue);
            }
            aTableRowGroupMap.put(arrayName,arrayMap);
        }

        // Place the groupMap into the aTableRowGroupMap
        aTableRowGroupMap.put(aGroupName,groupMap);

        return aTableRowGroupMap;
    }        


    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for the
     * Service Assembly Statistics
     *
     * @param aTableOfSaStats        a table of Service Asssemly statistics
     * @param anInstanceName         display statistics only for this instance
     * @param anSaName               the Service Assembly name for these
     *      statistics
     * @return                       a property sheet for viewing the Service
     *      Unit Statistics
     */
    //@SuppressWarnings("unchecked")
    public static PropertySheet getSaStatsPS(TabularData aTableOfSaStats,
                                             String anInstanceName,
                                             String anSaName)
    {
    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JSFUtils.getSaStatsPS(" +
                  aTableOfSaStats + ", " +
                  anInstanceName + ", " +
                  anSaName + ")");
    	}

        PropertySheet result =
            createPS("monSa");
        CompositeData compData =
            CompositeDataUtils.getDataForInstance(aTableOfSaStats,
                                                  anInstanceName);
        if (null != compData)
            {
                //
                // Summary
                //
                PropertySheetSection sum =
                    createPSS("sum",
                              "jbi.monitor.service.assembly.summary.pss.label",
                              result);

                Property instanceName =
                    createStaticTextProperty("instanceName",
                                             I18nUtilities
                                             .getResourceString("jbi.monitor.service.assembly.instance.name.label"),
                                             anInstanceName,
                                             sum);
                Property saName =
                    createStaticTextProperty("saName",
                                             I18nUtilities
                                             .getResourceString("jbi.monitor.service.assembly.name.label"),
                                             anSaName,
                                             sum);

                //
                // Lifecycle statistics
                //
                PropertySheetSection lc =
                    createPSS("lc",
                              "jbi.monitor.service.assembly.life.cycle.pss.label",
                              result);

                String saLastStartUpTimeValue =
                    formatDate(compData,
                               JBIStatisticsItemNames.SERVICE_ASSEMBLY_LAST_STARTUP_TIME,
                               VALUE_NOT_AVAILABLE);

                Property saLastStartUpTime =
                    createStaticTextProperty("lastStartUpTime",
                                             I18nUtilities
                                             .getResourceString("jbi.monitor.service.assembly.last.start.time.label"),
                                             saLastStartUpTimeValue,
                                             lc);

                String saStartUpTimeValue =
                    formatTime(compData,
                               JBIStatisticsItemNames.SERVICE_ASSEMBLY_STARTUP_TIME,
                               VALUE_NOT_AVAILABLE);

                Property saStartTime =
                    createStaticTextProperty("avgStartUpTime",
                                             I18nUtilities
                                             .getResourceString("jbi.monitor.service.assembly.start.time.label"),
                                             saStartUpTimeValue,
                                             lc);
                String saStopTimeValue =
                    formatTime(compData,
                               JBIStatisticsItemNames.SERVICE_ASSEMBLY_STOP_TIME,
                               VALUE_NOT_AVAILABLE);

                Property saStopTime =
                    createStaticTextProperty("avgStopTime",
                                             I18nUtilities
                                             .getResourceString("jbi.monitor.service.assembly.stop.time.label"),
                                             saStopTimeValue,
                                             lc);

                String saShutDownTimeValue =
                    formatTime(compData,
                               JBIStatisticsItemNames.SERVICE_ASSEMBLY_SHUTDOWN_TIME,
                               VALUE_NOT_AVAILABLE);

                Property saShutDownTime =
                    createStaticTextProperty("avgShutDownTime",
                                             I18nUtilities
                                             .getResourceString("jbi.monitor.service.assembly.shut.down.time.label"),
                                             saShutDownTimeValue,
                                             lc);
            }
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JSFUtils.getSaStatsPS(), result=" +
                  result);
        }
        
        return result;
    }

    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for the
     * Service Unit Statistics
     *
     * @param aTableOfSaStats        a table of Service Asssemly statistics
     * @param anInstanceName         display statistics only for this instance
     * @param anSaName               the Service Assembly name for these
     *      statistics
     * @param anSuName               the Service Unit name for these
     *      statistics
     * @return                       a property sheet for viewing the Service
     *      Unit Statistics
     */
    //@SuppressWarnings("unchecked")
    public static PropertySheet getSuStatsPS(TabularData aTableOfSaStats,
                                             String anInstanceName,
                                             String anSaName,
                                             String anSuName)
    {
    	if (sLog.isLoggable(Level.FINER)){
          sLog.finer("JSFUtils.getSuStatsPS(" +
                  aTableOfSaStats + ", " +
                  anInstanceName + ", " +
                  anSaName + ", " +
                  anSuName + ")");
    	}

        PropertySheet result =
            createPS("monSu");
        CompositeData compData =
            CompositeDataUtils.getDataForInstance(aTableOfSaStats,
                                                  anInstanceName);
        if (null != compData)
            {
                //
                // Summary
                //
                PropertySheetSection sum =
                    createPSS("sum",
                              "jbi.monitor.service.unit.summary.pss.label",
                              result);

                Property instanceName =
                    createStaticTextProperty("instanceName",
                                             I18nUtilities
                                             .getResourceString("jbi.monitor.service.unit.instance.name.label"),
                                             anInstanceName,
                                             sum);
                Property saName =
                    createStaticTextProperty("saName",
                                             I18nUtilities
                                             .getResourceString("jbi.monitor.service.unit.service.assembly.name.label"),
                                             anSaName,
                                             sum);
                Property suName =
                    createStaticTextProperty("suName",
                                             I18nUtilities
                                             .getResourceString("jbi.monitor.service.unit.name.label"),
                                             anSuName,
                                             sum);

                //
                // Lifecycle statistics
                //
                PropertySheetSection lc =
                    createPSS("lc",
                              "jbi.monitor.service.unit.life.cycle.pss.label",
                              result);

                CompositeData[] suStatsData =
                    CompositeDataUtils.findCompositeArrayItem(compData,
                                                              JBIStatisticsItemNames.SERVICE_ASSEMBLY_SU_STATISTICS);
                serviceUnitsLoop:
                for (int i = 0; i < suStatsData.length; ++i)
                    {
                        String suNameI =
                            CompositeDataUtils.findCompositeItem(suStatsData[i],
                                                                 JBIStatisticsItemNames.SERVICE_UNIT_NAME);
                        if (!anSuName.equals(suNameI))
                            {
                                continue serviceUnitsLoop;
                            }

                        String suStartUpTimeValue =
                            formatTime(suStatsData[i],
                                       JBIStatisticsItemNames.SERVICE_UNIT_STARTUP_TIME,
                                       VALUE_NOT_AVAILABLE);

                        Property suStartTime =
                            createStaticTextProperty("startTime",
                                                     I18nUtilities
                                                     .getResourceString("jbi.monitor.service.unit.start.time.label"),
                                                     suStartUpTimeValue,
                                                     lc);

                        String suStopTimeValue =
                            formatTime(suStatsData[i],
                                       JBIStatisticsItemNames.SERVICE_UNIT_STOP_TIME,
                                       VALUE_NOT_AVAILABLE);

                        Property suStopTime =
                            createStaticTextProperty("stopTime",
                                                     I18nUtilities
                                                     .getResourceString("jbi.monitor.service.unit.stop.time.label"),
                                                     suStopTimeValue,
                                                     lc);

                        String suShutDownTimeValue =
                            formatTime(suStatsData[i],
                                       JBIStatisticsItemNames.SERVICE_UNIT_SHUTDOWN_TIME,
                                       VALUE_NOT_AVAILABLE);

                        String shutDownKey =
                            "jbi.monitor.service.unit.shut.down.time.label";
                        Property suShutDownTime =
                            createStaticTextProperty("shutDownTime",
                                                     I18nUtilities
                                                     .getResourceString(shutDownKey),
                                                     suShutDownTimeValue,
                                                     lc);
                    }
            }
        
        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JSFUtils.getSuStatsPS(), result=" +
                  result);
        }
        
        return result;
    }


    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for
     * component installation configuration properties
     *
     * @param aInstanceName      the instance name
     * @param aData              TabularData
     * @return                   a property sheet
     */
    public static PropertySheet getNMRPS(String aInstanceName, TabularData aData)
    {

    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JSFUtils.getNMRPS(" + aData + ")");
    	}
        
        CompositeData compData = CompositeDataUtils.getDataForInstance(aData,aInstanceName);

        PropertySheet result = createPS("frameworkPS");

        PropertySheetSection nmrPSS = createPSS("nmr", 
                                                "jbi.root.monitoring.nmr.props.heading",
                                                result);
            
        if (compData != null)
        {
            Property nmrStats = createProperty("nmrStats",null,"",false,nmrPSS);
            nmrStats.getChildren()
                    .add(addNMRStatisticsTable(compData));
        }

        return result;

    }


    /**
     * Dynamically populates a <code>sun:propertySheet</code> element for
     * component installation configuration properties
     *
     * @param aInstanceName      the instance name
     * @param aData              TabularData
     * @return                   a property sheet
     */
    public static PropertySheet getFrameworkPS(String aInstanceName, TabularData aData)
    {

    	if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JSFUtils.getFrameworkPS(" + aData + ")");
    	}

        CompositeData compData = CompositeDataUtils.getDataForInstance(aData,aInstanceName);

        PropertySheet result = createPS("frameworkPS");

        PropertySheetSection frameworkPSS =
            createPSS("framework",
                      "jbi.root.monitoring.framework.props.heading",
                      result);

        String instanceName = CompositeDataUtils.findCompositeItem
                                   (compData,JBIStatisticsItemNames.INSTANCE_NAME);

        String startUpTime  = CompositeDataUtils.findCompositeItem
                                   (compData,JBIStatisticsItemNames.FRAMEWORK_STARTUP_TIME);

        Object[] frameworkUpTime =
            CompositeDataUtils.findCompositeUpTime
                                   (compData,JBIStatisticsItemNames.FRAMEWORK_UPTIME);
        String frameworkUpTimeValue =
            GuiUtil.getMessage(I18nUtilities.getResourceString("jbi.root.monitoring.fw.up.time.elapsed"),
                               frameworkUpTime);

        Property instanceNameProp =
            createStaticTextProperty("instanceName",
                                     I18nUtilities.getResourceString("jbi.monitor.service.unit.instance.name.label"),
                                     instanceName,
                                     frameworkPSS);
        Property startUpTimeProp =
            createStaticTextProperty("startUpTime",
                                     I18nUtilities.getResourceString("jbi.root.monitoring.fw.startup.time.label"),
                                     startUpTime,
                                     frameworkPSS);

        Property frameworkUpTimeProp =
            createStaticTextProperty("upTime",
                                     I18nUtilities.getResourceString("jbi.root.monitoring.fw.up.time.label"),
                                     frameworkUpTimeValue,
                                     frameworkPSS);

        return result;

    }


    /**
     * create a labeled property with a read-only or editable text property
     *
     * @param anIdPrefix      String used to create unique element IDs
     * @param anI18nLabelKey  String used to lookup an I18n String for the
     *      label
     * @param aTextValueExpr  String used to set the text value
     * @param isEditable      boolean if true, creates editable TextField,
     *      otherwise creates read-only StaticText
     * @return                Property containing a label and either a
     *      TextField or a StaticText
     */
    public static Property createI18nTextProperty(String anIdPrefix,
                                                  String anI18nLabelKey,
                                                  String aTextValueExpr,
                                                  boolean isEditable)
    {
        Property result =
            new Property();
        result.setId(anIdPrefix + "Property");

        String labelStr =
            I18nUtilities.getResourceString(anI18nLabelKey);

        Label label =
            createLabel(anIdPrefix,
                        isEditable,
                        labelStr,
                        result);

        if (!isEditable)
            {
                StaticText staticText =
                    new StaticText();
                staticText.setId(anIdPrefix + "StaticText");

                String staticTextStr = "";

                if (null != aTextValueExpr)
                    {
                        staticTextStr =
                            BeanUtilities.getStringPropertyUsingExpression(aTextValueExpr);
                    }

                staticText.setText(staticTextStr);
                result.getChildren().add(staticText);
            }
        else
            {
                TextField textField =
                    new TextField();
                textField.setId(anIdPrefix + "TxtFld");
                textField.setStyleClass(FIELD_REQUIRED);

                String textFieldStr = "";

                if (null != aTextValueExpr)
                    {
                        textFieldStr =
                            BeanUtilities.getStringPropertyUsingExpression(aTextValueExpr);
                    }

                textField.setText(textFieldStr);
                result.getChildren().add(textField);
            }
        return result;
    }

    /**
     * Input parameter for <code>getCompAppConfigPropertySheet</code> when
     * creating a new Component Application Configuration.
     */
    public static final String PS_CREATE = "create";

    /**
     * Input parameter for <code>getCompAppConfigPropertySheet</code> when
     * viewing/editing an existing Component Application Configuration.
     */
    public static final String PS_EDIT = "edit";

       
    /**
     * Adds property sheet sections of configuration properties to a 
     * component application|installation|runtime configuration property sheet
     * @param aCompName the JBI component name
     * @param aCompType one of: <code>binding-component</code> or
     * <code>service-engine</code>
     * @param aConfigType one of: <code>config-type-comp-app</code>,
     * <code>config-type-comp-install</code>, or
     * <code>config-type-comp-runtime</code>
     * @param anInstanceName the glassfish instance
     * @param aListOfPropertyGroups the configuration property groups
     * to be processed (for property metadata)
     * @param aSetOfCurrentValues the properties values to be displayed
     * @param aParent the parent property sheet to add the one or more
     * property sheet sections to
     */
    private static void addPropertiesToCompConfigPS(String aCompName,
                                                    String aCompType,
                                                    String aConfigType,
                                                    String anInstanceName,
                                                    List<ConfigPropertyGroup> aListOfPropertyGroups,
                                                    Properties aSetOfCurrentValues,
                                                    PropertySheet aParent)
    {

        // if processing an application configuration,
        // create a summary prolog
        if (ESBConstants
            .CONFIG_TYPE_COMP_APP
            .equals(aConfigType))
            {
                PropertySheetSection prolog;
                String instanceLabel = 
                    I18nUtilities
                    .getResourceString("jbi.edit.comp.app.config.instance.label");

                String compLabel = 
                    I18nUtilities
                    .getResourceString("jbi.edit.comp.app.config.component.label");

                String appConfigNameLabel = 
                    I18nUtilities
                    .getResourceString("jbi.edit.comp.app.config.name.label");

                // if creating the application configuration
                // create a section for adding 
                if (null == aSetOfCurrentValues)
                    {
                        String sectionTitle = 
                            I18nUtilities
                            .getResourceString("jbi.edit.comp.app.config.props.pss.selection.label");

                        prolog =
                            createPSS(("createAppCfgSum"),
                                      sectionTitle,
                                      aParent);
                    }

             
                // else if editing an existing application configuration
                // create a section for viewing/editing
                else
                    {
                        String sectionTitle = 
                            I18nUtilities
                            .getResourceString("jbi.edit.comp.app.config.props.pss.selection.label");

                        prolog =
                            createPSS(("editAppCfgSum"),
                                      sectionTitle,
                                      aParent);
                    }

                // if running in cluster-profile,
                // add a read-pnly instance name property
                if (ClusterUtilities.isClusterProfile())
                    {
                        Property instanceP =
                            createStaticTextProperty("instance",
                                                     instanceLabel,
                                                     anInstanceName,
                                                     prolog);
                    }

                // add a read-only component name property
                Property compP =
                    createStaticTextProperty("comp",
                                             compLabel,
                                             aCompName,
                                             prolog);

                Property appConfigNameP;

                // if creating an application configuration
                // add a required text input field to get the name
                if (null == aSetOfCurrentValues)
                    {
                        appConfigNameP =
                            createTextInputProperty("appCfgName",
                                                    ESBConstants
                                                    .APP_CONFIG_NAME_PROP,
                                                    appConfigNameLabel,
                                                    FIELD_REQUIRED,
                                                    null,
                                                    prolog);
                    }
                // if viewing/editing an existiong application configuration
                // determine the name and add a read-only name property
                else
                    {
                        String appConfigName =
                            BeanUtilities
                            .getStringPropertyUsingExpression("#{jbiAppConfigName}");

                        appConfigNameP =
                            createStaticTextProperty("appCfgName",
                                                     appConfigNameLabel,
                                                     appConfigName,
                                                     prolog);
                    }
            }
        // else not app config--skip prolog

        if (null != aListOfPropertyGroups)
            {
                for (int i = 0; i < aListOfPropertyGroups.size(); ++i)
                    {
                        ConfigPropertyGroup currentGroup =
                            aListOfPropertyGroups.get(i);
                        
                        Object[] args =
                            {
                                currentGroup.getDisplayName(),
                                currentGroup.getDisplayDescription(),
                            };
                        
                        String sectionTitle =
                            GuiUtil
                            .getMessage(
                                        I18nUtilities
                                        .getResourceString("jbi.config.props.section.title"),
                                        args);

                        PropertySheetSection groupPSSection =
                            createPSS(("grp" + i),
                                      sectionTitle,
                                      aParent);
                        
                        List<ConfigProperty> currentGroupProperties =
                            currentGroup.getConfigProperties();
                        if (null != currentGroupProperties)
                            {
                                for (int j = 0; j < currentGroupProperties.size(); ++j)
                                    {
                                        ConfigProperty configProperty =
                                            (ConfigProperty) currentGroupProperties.get(j);
                                        String value = "";
                                        List<Row> valuesList = 
                                            new ArrayList();
                                        
                                        // determine if the current property is to
                                        // be displayed for the current use-case
                                        String showDisplay = null;
                                        if (null != configProperty)
                                            {
                                                showDisplay =
                                                    configProperty.getShowDisplay();
                                            }
                                        boolean isDisplayable = false; // default for below check

                                        // if property is always-to-be displayed,
                                        // or install-only display and this is an install config PS
                                        // or runtime-only display and this is a runtime conig PS
                                        // indicates displayable (else use default of not displayable)
                                        if ((null != showDisplay)
                                            && ((ESBConstants
                                                 .ATTR_SHOW_DISPLAY_VALUE_ALL
                                                 .equals(showDisplay))
                                                ||((ESBConstants
                                                    .ATTR_SHOW_DISPLAY_VALUE_INSTALL
                                                    .equals(showDisplay))
                                                   &&(ESBConstants
                                                      .CONFIG_TYPE_COMP_INSTALL
                                                      .equals(aConfigType)))
                                                ||((ESBConstants
                                                    .ATTR_SHOW_DISPLAY_VALUE_RUNTIME
                                                    .equals(showDisplay))
                                                   &&(ESBConstants
                                                      .CONFIG_TYPE_COMP_RUNTIME
                                                      .equals(aConfigType)))))
                                            {
                                                isDisplayable = true;
                                            }

                                        if (isDisplayable)
                                            {
                                                // if there is the possibility of a current value
                                                // use it
                                                if ((null != aSetOfCurrentValues)
                                                    &&((ESBConstants
                                                        .CONFIG_TYPE_COMP_RUNTIME.equals(aConfigType))
                                                       ||(ESBConstants
                                                          .CONFIG_TYPE_COMP_APP.equals(aConfigType))))
                                                    {
                                                        Object prop =
                                                            aSetOfCurrentValues
                                                            .get(configProperty.getName());
                                                        if ((null != prop)
                                                            &&(prop instanceof String))
                                                            {
                                                                value = 
                                                                    (String) prop;
                                                            }
                                                        if (configProperty.getIsVariableArray())
                                                            {
                                                                boolean isPossiblyMore = true;
                                                                int index = 0;
                                                                String rootName =
                                                                    configProperty
                                                                    .getName();
                                                                valuesList =
                                                                    new ArrayList<Row>();
                                                                while (isPossiblyMore)
                                                                    {
                                                                        Object subprop =
                                                                            aSetOfCurrentValues
                                                                            .get(rootName + 
                                                                                 "." +
                                                                                 index);
                                                                        if ((null != subprop)
                                                                            &&(subprop instanceof String))
                                                                            {
                                                                                Row row =
                                                                                    createRow((String)
                                                                                              subprop);
                                                                                valuesList
                                                                                    .add(row);
                                                                                ++index;
                                                                            }
                                                                        else
                                                                            {
                                                                                isPossiblyMore = 
                                                                                    false;
                                                                            }
                                                                    }
                                                            }

                                                    }
                                                // if there is a default value, use it
                                                else if (null != configProperty.getValue())
                                                    {
                                                        Object o =
                                                            configProperty.getValue();
                                                        if ((!configProperty.getIsVariableArray())
                                                            &&(o instanceof String))
                                                            {
                                                                value =
                                                                    (String) o;
                                                            }
                                                        if ((configProperty.getIsVariableArray()
                                                             &&(o instanceof List)))
                                                            {
                                                                valuesList =
                                                                    (List<Row>) o;
                                                            }
                                                    }
                                                // else value remains empty ("")
                                                // and valuesList remains empty List


                                                Property prop;
                                        
                                                // if showing component install/runtime property
                                                // create input widget to edit the property
                                                if (!ESBConstants
                                                    .CONFIG_TYPE_COMP_APP
                                                    .equals(aConfigType))
                                                    {
                                                        prop =
                                                            createInputWidget(configProperty,
                                                                              value,
                                                                              groupPSSection,
                                                                              j);
                                                    }
                                                else
                                                    {
                                                        // if showing application configuration props,
                                                        // and processing a property other than config name
                                                        // create input widget to edit the property
                                                        if (!ESBConstants
                                                            .APP_CONFIG_NAME_PROP
                                                            .equals(configProperty.getName()))
                                                            {
                                                                if (!configProperty.getIsVariableArray())
                                                                    {
                                                                        prop =
                                                                            createInputWidget(configProperty,
                                                                                              value,
                                                                                              groupPSSection,
                                                                                              j);
                                                                    }
                                                                else
                                                                    {
                                                                        prop =
                                                                            createEditableTable(configProperty,
                                                                                                valuesList,
                                                                                                groupPSSection,
                                                                                                j);
                                                                    }
                                                            }
                                                        // else if showing application configuration props,
                                                        // and processing config name, 
                                                        // skip this property
                                                        // (instead of allowing edit of config name)
                                                    }
                                            }
                                    }
                            }
                    }
            }
    }
   

    /**
     * creates an editable tablet property
     * @param aConfigProperty describes the field
     * @param aValueList the zero-or-more field values to be displayed|edited
     * @param aParent the property sheet section to add the widget to
     * @param anIdSuffix for unique ID
     * @return a property containing an editable table
     */
    private static Property createEditableTable(ConfigProperty aConfigProperty,
                                                List<Row> aValuesList,
                                                PropertySheetSection aParent,
                                                int anIdSuffix)
    {
        Property result = 
            createProperty(("edTblPrp" + anIdSuffix),
                           aConfigProperty.getName(),
                           "", false, aParent);

        result.setLabel(aConfigProperty.getDisplayName());

        result.getChildren()
            .add(addConfigPropertyEditableTable(aValuesList,
                                                ("" + anIdSuffix)));
        
        HelpInline fieldHelp =
            createFieldHelp(("propFldHelp" + anIdSuffix),
                            aConfigProperty.getDisplayDescription(),
                            result);
        return result;
    }

    /**
     * creates an input widget property
     * @param aConfigProperty describes the field
     * @param aValue the field value to be displayed|edited
     * @param aParent the property sheet section to add the widget to
     * @param anIdSuffix for unique ID
     * @return a property containing an input widget of the appropriate type
     */
    private static Property createInputWidget(ConfigProperty aConfigProperty,
                                              String aValue,
                                              PropertySheetSection aParent,
                                              int anIdSuffix)
    {
        Property result = null;

        String xsdType =
            aConfigProperty.getXSDType();
        if (ESBConstants.XSD_TYPE_STRING.equals(xsdType))
            {
                boolean isEncrypted =
                    aConfigProperty.getIsEncrypted();
                if (!isEncrypted)
                    {
                        List<String> constrainedStrings =
                            aConfigProperty.getConstraintEnumeration();
                        // if no enumeration, prompt for text
                        if (null == constrainedStrings)
                            {
                                String requiredStyle = null;
                                if (aConfigProperty.getIsRequired())
                                    {
                                        requiredStyle =
                                            FIELD_REQUIRED;
                                    }

                                result =
                                    createTextInputProperty((PROP_PREFIX + anIdSuffix),
                                                            aConfigProperty
                                                            .getName(),
                                                            aConfigProperty
                                                            .getDisplayName(),
                                                            requiredStyle,
                                                            aValue,
                                                            aParent);
                            }
                        // if enumeration, display selection list (choosing default)
                        else
                            {
                                result =
                                    createDropDownProperty((PROP_PREFIX + anIdSuffix),
                                                           aConfigProperty.getName(),
                                                           aConfigProperty.getDisplayName(),
                                                           constrainedStrings,
                                                           aValue,
                                                           IS_NOT_SUBMIT_FORM,
                                                           aParent);
                            }
                    }
                else
                    {
                        result =
                            createPasswordInputProperty((PROP_PREFIX + anIdSuffix),
                                                        aConfigProperty.getName(),
                                                        aConfigProperty.getDisplayName(),
                                                        aConfigProperty.getIsRequired(),
                                                        aValue,
                                                        aParent);
                    }
            }
        else if ((ESBConstants.XSD_TYPE_INT.equals(xsdType))
                 ||(ESBConstants.XSD_TYPE_POSITIVE_INTEGER.equals(xsdType)))
            {
                boolean isPositiveOnly = false;
                if (ESBConstants.XSD_TYPE_POSITIVE_INTEGER.equals(xsdType))
                    {
                        isPositiveOnly = true;
                    }
                result =
                    createNumericInputProperty((PROP_PREFIX + anIdSuffix),
                                               aConfigProperty.getName(),
                                               aConfigProperty.getDisplayName(),
                                               isPositiveOnly,
                                               aConfigProperty.getIsRequired(),
                                               aValue,
                                               aParent);
            }
        else if (ESBConstants.XSD_TYPE_BOOLEAN.equals(xsdType))
            {
                result =
                    createBooleanInputProperty((PROP_PREFIX + anIdSuffix),
                                               aConfigProperty.getName(),
                                               aConfigProperty.getDisplayName(),
                                               aValue,
                                               aParent);
            }
        else
            {
        	    if (sLog.isLoggable(Level.FINER)){
                   sLog.finer("JSFUtils.createInputWidget(), unsupported xsdType=" +
                          xsdType);
        	    }
            }
            
        // If no supported type widget created yet,
        // creates default (read-only) StaticText widget
        if (null == result)
            {
                Object[] args = { aConfigProperty.getDisplayDescription() };
                String uneditableType =
                    I18nUtilities
                    .getResourceString("jbi.comp.config.unable.to.edit.property");
                String displayDescription =
                    GuiUtil.getMessage(uneditableType, args);

                result =
                    createStaticTextProperty((PROP_PREFIX + anIdSuffix),
                                             aConfigProperty.getDisplayName(),
                                             displayDescription,
                                             aParent);
            }

        HelpInline fieldHelp =
            createFieldHelp(("propFldHelp" + anIdSuffix),
                            aConfigProperty.getDisplayDescription(),
                            result);
        return result;
    }    

    /**
     * adds columns to a group so that the fields in the columns get the data
     * from a provider built from the supplied data
     *
     * @param aSelector           The new ColumnData value
     * @param aGroup              to add the columns to
     * @param aNameToStats mapping of names to statistics
     * @return the number of rows
     */
    private static int setColumnData(String aSelector,
                                     TableRowGroup aGroup,
                                     Map<String, CompositeData> aNameToStats)
    {
        final String[][] colIdsValExprs =
            {
                {"endPoint", "#{sourceVar.value.endpointName}"},
                {"receivedRequests", "#{sourceVar.value.receivedRequests}"},
                {"receivedReplies", "#{sourceVar.value.receivedReplies}"},
                {"receivedErrors", "#{sourceVar.value.receivedErrors}"},
                {"receivedFaults", "#{sourceVar.value.receivedFaults}"},
                {"receivedDones", "#{sourceVar.value.receivedDones}"},
                {"sentRequests", "#{sourceVar.value.sentRequests}"},
                {"sentReplies", "#{sourceVar.value.sentReplies}"},
                {"sentErrors", "#{sourceVar.value.sentErrors}"},
                {"sentFaults", "#{sourceVar.value.sentFaults}"},
                {"sentDones", "#{sourceVar.value.sentDones}"},
            };

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JSFUtils.setColumnData(" +
                  aSelector + ", " +
                  aGroup + ", " +
                  aNameToStats + ")");
        }

        aGroup.setSourceVar("sourceVar");

        List children =
            aGroup.getChildren();

        for (int i = 0; i < colIdsValExprs.length; ++i)
            {

                children.add(addTableColumnData(aSelector + colIdsValExprs[i][INDEX_ID],
                                                colIdsValExprs[i][INDEX_EXPR]));
            }

        ArrayList rows =
            new ArrayList();

        Iterator endpointsIt =
            aNameToStats.keySet().iterator();
        while (endpointsIt.hasNext())
            {
                String epName =
                    (String) endpointsIt.next();
                CompositeData stats =
                    (CompositeData) aNameToStats.get(epName);

                EndpointStatsBean row =
                    new EndpointStatsBean();

                row.setEndpointName(epName);

                row.setReceivedDones(CompositeDataUtils
                                     .findCompositeItem(stats,
                                                        JBIStatisticsItemNames
                                                        .NUMBER_OF_RECEIVED_DONES));

                row.setReceivedErrors(CompositeDataUtils
                                      .findCompositeItem(stats,
                                                         JBIStatisticsItemNames
                                                         .NUMBER_OF_RECEIVED_ERRORS));

                row.setReceivedFaults(CompositeDataUtils
                                      .findCompositeItem(stats,
                                                         JBIStatisticsItemNames
                                                         .NUMBER_OF_RECEIVED_FAULTS));

                row.setReceivedReplies(CompositeDataUtils
                                       .findCompositeItem(stats,
                                                          JBIStatisticsItemNames
                                                          .NUMBER_OF_RECEIVED_REPLIES));

                row.setReceivedRequests(CompositeDataUtils
                                        .findCompositeItem(stats,
                                                           JBIStatisticsItemNames
                                                           .NUMBER_OF_RECEIVED_REQUESTS));

                row.setSentDones(CompositeDataUtils
                                 .findCompositeItem(stats,
                                                    JBIStatisticsItemNames
                                                    .NUMBER_OF_SENT_DONES));

                row.setSentErrors(CompositeDataUtils
                                  .findCompositeItem(stats,
                                                     JBIStatisticsItemNames
                                                     .NUMBER_OF_SENT_ERRORS));

                row.setSentFaults(CompositeDataUtils
                                  .findCompositeItem(stats,
                                                     JBIStatisticsItemNames
                                                     .NUMBER_OF_SENT_FAULTS));

                row.setSentReplies(CompositeDataUtils
                                   .findCompositeItem(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_SENT_REPLIES));

                row.setSentRequests(CompositeDataUtils
                                    .findCompositeItem(stats,
                                                       JBIStatisticsItemNames
                                                       .NUMBER_OF_SENT_REQUESTS));

                rows.add(row);
            }

        int result = 
            rows.size();

        setToggleByCount(aGroup,
                         result);


        ObjectListDataProvider oldp =
            new ObjectListDataProvider(rows);

        aGroup.setSourceData(oldp);
        
        return result;
    }

    /**
     * creates a Row bean from a variable array value
     * @param aKeyAndValue of the form "key=value"
     * @param a Row bean with the name set to the key part and the value
     * set to the value part.
     */
    private static Row createRow(String aKeyAndValue)
    {
        Row result = 
            new Row();

        int equalsIndex =
            aKeyAndValue.indexOf("=");

        if (0 < equalsIndex)
            {
                String key =
                    aKeyAndValue.substring(0, equalsIndex);
                result.setName(key);
                
                String value =
                    aKeyAndValue.substring(equalsIndex + 1);
                result.setValue(value);
            }
                
        return result;
    }

    /**
     * adds columns to a group so that the fields in the columns get the data
     * from a provider built from the supplied data
     *
     * @param aSelector           The new ColumnData value
     * @param aGroup              to add the columns to
     * @param aConsumingNameToStats mapping consuming endpoint names to stats data
     * @param aProvidingNameToStats mapping providing endpoint names to stats data
     */
    private static void setColumnTotalsData(String aSelector,
                                            TableRowGroup aGroup,
                                            Map<String, CompositeData> aConsumingNameToStats,
                                            Map<String, CompositeData> aProvidingNameToStats)
    {
        final String[][] colIdsValExprs =
            {
                {"totals", ""},
                {"receivedRequests", "#{sourceVar.value.receivedRequests}"},
                {"receivedReplies", "#{sourceVar.value.receivedReplies}"},
                {"receivedErrors", "#{sourceVar.value.receivedErrors}"},
                {"receivedFaults", "#{sourceVar.value.receivedFaults}"},
                {"receivedDones", "#{sourceVar.value.receivedDones}"},
                {"sentRequests", "#{sourceVar.value.sentRequests}"},
                {"sentReplies", "#{sourceVar.value.sentReplies}"},
                {"sentErrors", "#{sourceVar.value.sentErrors}"},
                {"sentFaults", "#{sourceVar.value.sentFaults}"},
                {"sentDones", "#{sourceVar.value.sentDones}"},
            };

        if (sLog.isLoggable(Level.FINER)){
           sLog.finer("JSFUtils.setColumnTotalsData(" +
                  aSelector + ", " +
                  aGroup + ", " +
                  aConsumingNameToStats + ", " +
                  aProvidingNameToStats + ")");
        }

        aGroup.setSourceVar("sourceVar");

        List children =
            aGroup.getChildren();

        for (int i = 0; i < colIdsValExprs.length; ++i)
            {

                children.add(addTableColumnData(aSelector + colIdsValExprs[i][INDEX_ID],
                                                colIdsValExprs[i][INDEX_EXPR]));
            }

        long totalReceivedDones = 0;
        long totalReceivedErrors = 0;
        long totalReceivedFaults = 0;
        long totalReceivedReplies = 0;
        long totalReceivedRequests = 0;
        long totalSentDones = 0;
        long totalSentErrors = 0;
        long totalSentFaults = 0;
        long totalSentReplies = 0;
        long totalSentRequests = 0;

        // sum stats obtained from the two provided maps
        for (int i = 0; i < 2; ++i)
            {
                Map<String, CompositeData> map;
                if (0 == i)
                    {
                        map = aConsumingNameToStats;
                    }
                else
                    {
                        map = aProvidingNameToStats;
                    }
                Iterator endpointsIt =
                    map.keySet().iterator();
                
                while (endpointsIt.hasNext())
                    {
                        String epName =
                            (String) endpointsIt.next();
                        CompositeData stats =
                            (CompositeData) map.get(epName);

                        
                        totalReceivedDones +=
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_RECEIVED_DONES);
                        totalReceivedErrors += 
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_RECEIVED_ERRORS);
                        totalReceivedFaults += 
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_RECEIVED_FAULTS);
                        
                        totalReceivedReplies +=
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_RECEIVED_REPLIES);
                        
                        totalReceivedRequests +=
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_RECEIVED_REQUESTS);
                        
                        totalSentDones +=
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_SENT_DONES);
                        
                        totalSentErrors +=
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_SENT_ERRORS);
                        
                        totalSentFaults +=
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_SENT_FAULTS);
                        
                        totalSentReplies +=
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_SENT_REPLIES);
                        
                        totalSentRequests += 
                            CompositeDataUtils
                            .findCompositeCountOrZero(stats,
                                                      JBIStatisticsItemNames
                                                      .NUMBER_OF_SENT_REQUESTS);
                    }
            }
        
        ArrayList rows =
            new ArrayList();

        EndpointStatsBean totalsRow =
            new EndpointStatsBean();
        
        totalsRow.setEndpointName(""); // no name for totals
        totalsRow.setReceivedDones(Long.toString(totalReceivedDones));
        totalsRow.setReceivedErrors(Long.toString(totalReceivedErrors));
        totalsRow.setReceivedFaults(Long.toString(totalReceivedFaults));
        totalsRow.setReceivedReplies(Long.toString(totalReceivedReplies));
        totalsRow.setReceivedRequests(Long.toString(totalReceivedRequests));
        totalsRow.setSentDones(Long.toString(totalSentDones));
        totalsRow.setSentErrors(Long.toString(totalSentErrors));
        totalsRow.setSentFaults(Long.toString(totalSentFaults));
        totalsRow.setSentReplies(Long.toString(totalSentReplies));
        totalsRow.setSentRequests(Long.toString(totalSentRequests));

        rows.add(totalsRow);

        ObjectListDataProvider oldp =
            new ObjectListDataProvider(rows);

        aGroup.setSourceData(oldp);
    }

    /**
     * Gets a value expression object for an expression string
     *
     * @param aValueExpression  the expression string for fetching, and possibly
     * storing changes to, a field
     * @return the value expression to be set on a <code>StaticText</code>
     * or on a <code>TextField</code> via a <code>setValueExpression</code> call
     */
    private static ValueExpression getValueExpression(String aValueExpression)
    {
        FacesContext facesContext =
            FacesContext.getCurrentInstance();
        ValueExpression result =
            facesContext.getApplication().getExpressionFactory()
            .createValueExpression(
                                   facesContext.getELContext(),
                                   aValueExpression,
                                   Object.class);
        return result;
    }

    /**
     * Description of the Method
     *
     * @param anIdPrefix     Description of Parameter
     * @param isRequired     Description of Parameter
     * @param aDisplayLabel  Description of Parameter
     * @param aParent        Description of Parameter
     * @return               Description of the Returned Value
     */
    private static Label createLabel(String anIdPrefix,
                                     boolean isRequired,
                                     String aDisplayLabel,
                                     Property aParent)
    {
        Label result =
            new Label();
        result.setId(anIdPrefix + "Label");
        result.setRequiredIndicator(isRequired);
        aParent.getChildren().add(result);
        aParent.setLabel(aDisplayLabel);
        return result;
    }

    /**
     * Description of the Method
     *
     * @param anIdPrefix       Description of Parameter
     * @param aColumnWidth     Description of Parameter
     * @param aToolTip         Description of Parameter
     * @param isRequiredField  Description of Parameter
     * @param aValue           Description of Parameter
     * @param aSiblingsList    Description of Parameter
     * @return                 Description of the Returned Value
     */
    private static PasswordField createPasswordField(String anIdPrefix,
                                                     int aColumnWidth,
                                                     String aToolTip,
                                                     boolean isRequiredField,
                                                     String aValue,
                                                     List aSiblingsList)
    {
        PasswordField result =
            new PasswordField();
        result.setId(anIdPrefix + "PasswordField");
        result.setColumns(aColumnWidth);
        result.setToolTip(aToolTip);
        if (isRequiredField)
            {
                result.setStyleClass(FIELD_REQUIRED);
            }
        result.setText(aValue);
        aSiblingsList.add(result);
        return result;
    }


    /**
     * Description of the Method
     *
     * @param anId           Description of Parameter
     * @param anI18nMessage  Description of Parameter
     * @param aParent        Description of Parameter
     * @return               Description of the Returned Value
     */
    private static HelpInline createFieldHelp(String anId,
                                              String anI18nMessage,
                                              UIComponent aParent)
    {

        HelpInline result =
            new HelpInline();
        result.setId("hlpInln" + anId);
        result.setText(anI18nMessage);
        result.setType(HELP_INLINE_TYPE_FIELD);
        aParent.getChildren().add(result);
        return result;
    }

    /**
     * creates a config property editable table
     * for variable array properties, supporting
     * adding/editing/deleting of rows
     * @param aKeyToValueMap a list of selectable Row beans, with key names and values
     * @param aUniqueId used to create unique widget identifiers
     * @return the editable table 
     */
    private static Table addConfigPropertyEditableTable(List<Row> aKeyToValueMap,
                                                        String aUniqueId)
    {
        final String[][] colIdsValExprs =
        {
            {"CfgPrpRwSlctd", "#{sourceVar.value.selected}"},
            {"CfgPrpRwNm", "#{sourceVar.value.name}"},
            {"CfgPrpRwVl", "#{sourceVar.value.value}"},
        };

        Table result = new Table();

        // Renders multiple selection and deselection buttons
        String onClick =
            "setTimeout('changeThreeTableButtons()', 0)";
        result.setDeselectMultipleButton(true);
        result.setDeselectMultipleButtonOnClick(onClick);
        result.setPaginateButton(true);
        result.setPaginationControls(true);
        result.setSelectMultipleButton(true);
        result.setSelectMultipleButtonOnClick(onClick);

        // Renders a table title (element count added automagically)
        
        String tableRowHeader =
            I18nUtilities
            .getResourceString("jbi.comp.config.var.array.prop.rows");
        
        result.setTitle(tableRowHeader);

        // Renders Add and Remove buttons
        PanelGroup buttonsPg =
            new PanelGroup();
        buttonsPg.setId("buttonsPg" + aUniqueId);

        Button addBtn =
            new Button();
        addBtn.setId("addBtn" + aUniqueId);
        addBtn.setText(I18nUtilities
                       .getResourceString("jbi.comp.config.var.array.prop.add"));
        addBtn.setDisabled(true); // adding rows is not yet implemented
        buttonsPg.getChildren().add(addBtn);

        Button removeBtn =
            new Button();
        removeBtn.setId("removeBtn" + aUniqueId);
        removeBtn.setText(I18nUtilities
                          .getResourceString("jbi.comp.config.var.array.prop.remove"));
        removeBtn.setDisabled(true); // removing rows is not yet implemented
        buttonsPg.getChildren().add(removeBtn);
        
        UIComponent facet
            = result.getFacets()
            .get(Table
                 .ACTIONS_TOP_FACET);
        if (null != facet)
            {
                facet.getChildren()
                    .add(buttonsPg);
            }
        else
            {
                result.getFacets()
                    .put(Table
                         .ACTIONS_TOP_FACET,
                         buttonsPg);
            }

        // Creates the header group and adds it to the table.
        TableRowGroup headerGroup = addConfigPropertyEditableTableHeadersGroup();
        result.getChildren().add(headerGroup);

        TableRowGroup tableRowGroup = new TableRowGroup();
        tableRowGroup.setId("trg" + aUniqueId);

        //tableRowGroup.setHeaderText(tableRowHeader);
        tableRowGroup.setSourceVar("sourceVar");
        
        List children = tableRowGroup.getChildren();
        for (int i=0; i<colIdsValExprs.length; ++i)
            {
                if (0 == i)
                    {
                        children
                            .add(addTableColumnCheckbox(("VAR_ARY_PRP_TBL" +
                                                         colIdsValExprs[i][0]),
                                                        colIdsValExprs[i][1]));
                    }
                else 
                    {
                        children
                            .add(addTableColumnEditField(("VAR_ARY_PRP_TBL" + 
                                                          colIdsValExprs[i][0]),
                                                         colIdsValExprs[i][1]));
                    }
            }

        ArrayList rows =
            new ArrayList();
        for (Row row:aKeyToValueMap)
            {
                Row newRow =
                    new Row();
                newRow.setName(row.getName());
                newRow.setValue(row.getValue());
                rows.add(newRow);
            }
        ObjectListDataProvider oldp = 
            new ObjectListDataProvider(rows);
        tableRowGroup.setSourceData(oldp);
        result.getChildren().add(tableRowGroup);

        return result;
    }

    /**
     * creates an NMR statistics table
     * @param aCompData the stats data
     * @return the NMR statistics table
     */
    private static Table addNMRStatisticsTable(CompositeData aCompData)
    {
        final String[][] colIdsValExprs =
        {
            {"ComponentExtensionName", "#{sourceVar.value.name}"},
            {"ComponentExtensionValue", "#{sourceVar.value.value}"},
        };

        Table result = new Table();

        // Create the header group and add them to the table.
        TableRowGroup headerGroup = addNMRHeadersGroup();
        result.getChildren().add(headerGroup);

        String[] activeChannels  = (String []) aCompData.get(JBIStatisticsItemNames.NMR_STATS_ACTIVE_CHANNELS);
        String[] activeEndpoints = (String []) aCompData.get(JBIStatisticsItemNames.NMR_STATS_ACTIVE_ENDPOINTS);

        ArrayList rows = new ArrayList();
        TableRowGroup tableRowGroup = new TableRowGroup();

        int rowCount = 0;

        if (activeChannels != null)
        {
            rowCount =
                activeChannels.length;
        }

        Object[] activeChannelsHeaderArgs =
            {
                rowCount,
            };
        
        String tableRowHeader =
            GuiUtil
            .getMessage(I18nUtilities
                        .getResourceString("jbi.statistics.nmr.active.channels"),
                        activeChannelsHeaderArgs);
        
        tableRowGroup.setHeaderText(tableRowHeader);
        tableRowGroup.setSourceVar("sourceVar");

        List children = tableRowGroup.getChildren();
        for (int i=0; i<colIdsValExprs.length; ++i)
            {
                children
                    .add(addTableColumnData(("NMR_ACTIVE_CHANNELS" + 
                                            colIdsValExprs[i][0]),
                                            colIdsValExprs[i][1]));
            }
        if (activeChannels != null)
        {
            for (int i=0; i<activeChannels.length; i++)
                {
                    String keyName = (i+1) + ":";
                    String value  = activeChannels[i];
                    Row row = new Row();
                    row.setName (keyName);
                    row.setValue (value);
                    rows.add(row);
                }
            setToggleByCount(tableRowGroup,rows.size());
            ObjectListDataProvider oldp = new ObjectListDataProvider(rows);
            tableRowGroup.setSourceData(oldp);
            result.getChildren().add(tableRowGroup);
        }

        rows = new ArrayList();
        tableRowGroup = new TableRowGroup();


        rowCount = 0;

        if (activeEndpoints != null)
            {
                rowCount =
                    activeEndpoints.length;
            }
        
        Object[] activeEndpointsHeaderArgs =
            {
                rowCount,
            };

        tableRowHeader =
            GuiUtil
            .getMessage(I18nUtilities
                        .getResourceString("jbi.statistics.nmr.active.endpoints"),
                        activeEndpointsHeaderArgs);

        tableRowGroup.setHeaderText(tableRowHeader);
        tableRowGroup.setSourceVar("sourceVar");
        children = tableRowGroup.getChildren();

        for (int i=0; i<colIdsValExprs.length; ++i)
            {
                children
                    .add(addTableColumnData(("NMR_ACTIVE_Endpoints" + 
                                            colIdsValExprs[i][0]),
                                            colIdsValExprs[i][1]));
            }

        if (activeEndpoints != null)
            {
                for (int i=0; i<activeEndpoints.length; i++)
                    {
                        String keyName = (i+1) + ":";
                        String value  = activeEndpoints[i];
                        Row row = new Row();
                        row.setName (keyName);
                        row.setValue (value);
                        rows.add(row);
                    }
                setToggleByCount(tableRowGroup,rows.size());
                ObjectListDataProvider oldp = new ObjectListDataProvider(rows);
                tableRowGroup.setSourceData(oldp);
                result.getChildren().add(tableRowGroup);
            }
        
        return result;
    }


    /**
     * creates a table of component-provided statisitcs
     * @param aComponentExtensionsCD the stats provided by a component
     * @return the component stats table
     */
    private static Table addComponentExtensionStatisticsTable(CompositeData aComponentExtensionsCD)
    {
        final String[][] colIdsValExprs =
        {
            {"ComponentExtensionName", "#{sourceVar.value.name}"},
            {"ComponentExtensionValue", "#{sourceVar.value.value}"},
        };

        Table result = new Table();

        // Create the header group and add them to the table.
        TableRowGroup headerGroup = addComponentExtensionHeadersGroup();
        result.getChildren().add(headerGroup);

        HashMap groupMap = new HashMap();

        groupMap = 
            constructCompositeDataTable(aComponentExtensionsCD, 
                                        "", // outer group named later
                                        groupMap,
                                        0);

        // Processes the groups twice,
        // on the first pass, processes the unnamed (top-level/General) group 
        // on the second pass, processes the remaining (component-provided) groups
        for (int i = 0; i <2; ++i)
            {
                // Constructs the  tableRowGroups for this pass
                // and adds them to the table
                Iterator groupMapIt = groupMap.keySet().iterator();
                while (groupMapIt.hasNext())
                    {
                        String groupName = (String) groupMapIt.next();
                        
                        // If this is the first pass and the unnamed group is next,
                        // or if this is the second pass and a named group is next,
                        // processes the group (i.e. skip groups not appropriate
                        // for this pass, to ensure General appears first)
                        if (((0 == i)
                             &&("".equals(groupName)))
                            ||((1 == i)
                               &&(!"".equals(groupName))))
                            {
                                HashMap entryMap = (HashMap) groupMap.get(groupName);
                        
                                ArrayList rows = new ArrayList();
                                TableRowGroup tableRowGroup = new TableRowGroup();
                                tableRowGroup.setSourceVar("sourceVar");
                        
                                List children = tableRowGroup.getChildren();
                                for (int j=0; j<colIdsValExprs.length; ++j)
                                    {
                                        children.add(
                                                     addTableColumnData("COMPONENT_PROVIDED" + 
                                                                        colIdsValExprs[j][0],colIdsValExprs[j][1]));
                                    }
                                
                                // If processing the unnamed (top-leve/general) group,
                                // sorts the rows, and provides a count insert for the header summary
                                // Note: this is only done on the first pass of the outer loop
                                if ("".equals(groupName))
                                    {
                                        
                                        // Sorts the table by key name
                                        List namesList = 
                                            new ArrayList(entryMap.keySet());
                                        
                                        Collections.sort(namesList,
                                                         String.CASE_INSENSITIVE_ORDER);
                                        
                                        Iterator it =
                                            namesList.iterator();
                                        
                                        while (it.hasNext()) {
                                            String keyName = (String) it.next();
                                            String value = (String) entryMap.get(keyName);
                                            Row row = new Row();
                                            row.setName (keyName);
                                            row.setValue (value);
                                            rows.add(row);
                                        }
                                        
                                        Object[] headerArgs =
                                            {
                                                rows.size(),
                                            };
                                        
                                        String headerTextWithCount =
                                            GuiUtil
                                            .getMessage(I18nUtilities
                                                        .getResourceString("jbi.statistics.component.general"),
                                                        headerArgs);
                                        tableRowGroup.setHeaderText(headerTextWithCount);
                                    }
                                
                                // If processing a component-provided (i.e. named/nested) group,
                                // sets its component-provided name and (unsorted) contents
                                // Note: this is only done on the second pass of the outer loop.
                                else
                                    {
                                        String groupHeaderTitle = groupName + " (" + entryMap.size() + ")";
                                        tableRowGroup.setHeaderText(groupHeaderTitle);
                                        Iterator entryMapIt =
                                            entryMap.keySet().iterator();
                                        while (entryMapIt.hasNext())
                                            {
                                                String keyName =
                                                    (String) entryMapIt.next();
                                                String value =
                                                    (String) entryMap.get(keyName);
                                                Row row = new Row();
                                                row.setName(keyName);
                                                row.setValue(value);
                                                rows.add(row);
                                            }
                                    }

                                // add toggle if there are rows
                                // collapse if there are too many rows
                                setToggleByCount(tableRowGroup,
                                                 rows.size());
                                
                                ObjectListDataProvider oldp = new ObjectListDataProvider(rows);
                                tableRowGroup.setSourceData(oldp);
                                result.getChildren().add(tableRowGroup);
                            }
                    }
            }                
        
        return result;
    }



    /**
     * @param aConsumingEndpointsCD  the stats for endpoints consumed-by the
     * component on the instance
     * @param aProvidingEndpointsCD  the stats for endpoints provided-by the
     * component on the instance
     * @param aCompName  the JBI component name
     * @param anInstanceName the GlassFish instance name
     * @return the endpoints statistics property sheet table
     */
    private static Table addEndpointStatisticsTable(CompositeData aConsumingEndpointsCD,
                                                    CompositeData aProvidingEndpointsCD,
                                                    String aCompName,
                                                    String anInstanceName)
    {
        Table result =
            new Table();

        TableRowGroup headerGroup =
            addHeadersGroup();

        result.getChildren().add(headerGroup);

        // 1. providing endpoints
        Map<String, CompositeData> nameToProvidingEndpointStats =
            createNameToEndpointStatsMap(aProvidingEndpointsCD,
                                         anInstanceName);

        TableRowGroup providingEndpointsGroup =
            addProvidingEndpointsGroup(nameToProvidingEndpointStats);
        result.getChildren().add(providingEndpointsGroup);
        
        // 2. consuming endpoints
        Map<String, CompositeData> nameToConsumingEndpointStats =
            createNameToEndpointStatsMap(aConsumingEndpointsCD,
                                         anInstanceName);

        TableRowGroup consumingEndpointsGroup =
            addConsumingEndpointsGroup(nameToConsumingEndpointStats);
        result.getChildren().add(consumingEndpointsGroup);
        
        // 3. totals
        TableRowGroup totalsGroup =
            addTotalsGroup(nameToConsumingEndpointStats,
                           nameToProvidingEndpointStats);
        result.getChildren().add(totalsGroup);

        return result;
    }

    /**
     * @return   Description of the Returned Value
     */
    private static TableRowGroup addConfigPropertyEditableTableHeadersGroup()
    {
        final String[][] colHdngsLblKeys =
        {
            {"varAryCfgPrpSlctd", ""},
            {"varAryCfgPrpName", "jbi.row.name.label"},
            {"varAryCfgPrpValue", "jbi.row.value.label"},
        };

        TableRowGroup result = new TableRowGroup();
        List children = result.getChildren();

        for (int i = 0; i < colHdngsLblKeys.length; ++i)
        {
            children.add(addTableColumnHeader(colHdngsLblKeys[i][0],
                   I18nUtilities.getResourceString(colHdngsLblKeys[i][1])));
        }

        result.setCollapsed(true);
        result.setGroupToggleButton(false);
        return result;
    }

    /**
     * @return   Description of the Returned Value
     */
    private static TableRowGroup addNMRHeadersGroup()
    {
        final String[][] colHdngsLblKeys =
        {
            {"NMRName", "jbi.row.name.label"},
            {"NMRValue", "jbi.row.value.label"},
        };

        TableRowGroup result = new TableRowGroup();
        List children = result.getChildren();

        for (int i = 0; i < colHdngsLblKeys.length; ++i)
        {
            children.add(addTableColumnHeader(colHdngsLblKeys[i][0],
                   I18nUtilities.getResourceString(colHdngsLblKeys[i][1])));
        }

        result.setCollapsed(true);
        result.setGroupToggleButton(false);
        return result;
    }


    /**
     * @return   Description of the Returned Value
     */
    private static TableRowGroup addComponentExtensionHeadersGroup()
    {
        final String[][] colHdngsLblKeys =
        {
            {"ComponentExtensionName", "jbi.row.name.label"},
            {"ComponentExtensionValue", "jbi.row.value.label"},
        };

        TableRowGroup result = new TableRowGroup();
        List children = result.getChildren();

        for (int i = 0; i < colHdngsLblKeys.length; ++i)
        {
            children.add(addTableColumnHeader(colHdngsLblKeys[i][0],
                   I18nUtilities.getResourceString(colHdngsLblKeys[i][1])));
        }

        result.setCollapsed(true);
        result.setGroupToggleButton(false);

        return result;
    }



    /**
     * @return   Description of the Returned Value
     */
    private static TableRowGroup addHeadersGroup()
    {
        final String[][] colHdngsLblKeys =
            {
                {"endPoint", "jbi.statistics.endpoint"},
                {"receivedRequests", "jbi.statistics.receivedRequests"},
                {"receivedReplies", "jbi.statistics.receivedReplies"},
                {"receivedErrors", "jbi.statistics.receivedErrors"},
                {"receivedFaults", "jbi.statistics.receivedFaults"},
                {"receivedDones", "jbi.statistics.receivedDones"},
                {"sentRequests", "jbi.statistics.sentRequests"},
                {"sentReplies", "jbi.statistics.sentReplies"},
                {"sentErrors", "jbi.statistics.sentErrors"},
                {"sentFaults", "jbi.statistics.sentFaults"},
                {"sentDones", "jbi.statistics.sentDones"},
            };
        TableRowGroup result =
            new TableRowGroup();
        result.setCollapsed(true);
        result.setGroupToggleButton(false);
        result.setEmptyDataMsg("|");

        List children =
            result.getChildren();

        for (int i = 0; i < colHdngsLblKeys.length; ++i)
            {

                children.add(addTableColumnHeader(colHdngsLblKeys[i][INDEX_ID],
                                                  I18nUtilities.getResourceString(colHdngsLblKeys[i][INDEX_HEADING])));
            }

        return result;
    }

    /**
     * @param aConsumingNameToStats mapping of zero or more consuming endpoint names
     * (map to their stats)
     * @return the collapsed table row group to be added to the components endpoints table
     */
    private static TableRowGroup addConsumingEndpointsGroup(Map<String, CompositeData> aConsumingNameToStats)
    {
        TableRowGroup result =
            new TableRowGroup();

        int rowCount =
            setColumnData(SELECT_CONSUMING,
                          result,
                          aConsumingNameToStats);

        Object[] headerArgs =
            {
                rowCount,
            };

        String headerTextWithCount =
            GuiUtil
            .getMessage(I18nUtilities
                        .getResourceString("jbi.statistics.consumingEndpoint"),
                        headerArgs);


        result.setHeaderText(headerTextWithCount);


        return result;
    }


    /**
     * @param aProvidingNameToStats mapping of zero or more providing endpoint names
     * (map to their stats)
     * @return the collapsed table row group to be added to the components endpoints table
     */
    private static TableRowGroup addProvidingEndpointsGroup(Map<String, CompositeData> aProvidingNameToStats)
    {
        TableRowGroup result =
            new TableRowGroup();

        int count =
            setColumnData(SELECT_PROVIDING,
                          result,
                          aProvidingNameToStats);

        Object[] headerArgs =
            {
                count,
            };

        String headerTextWithCount =
            GuiUtil
            .getMessage(I18nUtilities
                        .getResourceString("jbi.statistics.provisioningEndpoint"),
                        headerArgs);


        result.setHeaderText(headerTextWithCount);

        return result;
    }

    /**
     * creates the endpoint stats totals table row group
     * @param aConsumingNameToStats mapping of consuming endpoint names to stats
     * @param aProvidingNameToStats mapping of providing endpoint names to stats
     * @return the single-row totals table row group to be added to the components endpoints table
     */
    private static TableRowGroup addTotalsGroup(Map<String, CompositeData> aConsumingNameToStats,
                                                Map<String, CompositeData> aProvidingNameToStats)
    {
        TableRowGroup result =
            new TableRowGroup();

        result.setHeaderText(
                             I18nUtilities.getResourceString("jbi.statistics.totals"));
        result.setGroupToggleButton(false);

        setColumnTotalsData(SELECT_TOTALS,
                            result,
                            aConsumingNameToStats,
                            aProvidingNameToStats);

        return result;
    }

    /**
     * @param anId         The feature to be added to the TableColumnHeader
     *      attribute
     * @param aHeaderText  The feature to be added to the TableColumnHeader
     *      attribute
     * @return             Description of the Returned Value
     */
    private static TableColumn addTableColumnHeader(String anId,
                                                    String aHeaderText)
    {
        TableColumn result =
            new TableColumn();
        result.setId(anId + "TCHeader");
        result.setHeaderText(aHeaderText);
        result.setRowHeader(true);
        return result;
    }

    /**
     * initializes a table column to obtain data from a provider
     *
     * @param anId          The feature to be added to the TableColumnData
     *      attribute
     * @param anExpression  used to fetch data
     * @return              Description of the Returned Value
     */
    private static TableColumn addTableColumnData(String anId,
                                                  String anExpression)
    {
        TableColumn result =
            new TableColumn();
        result.setId(anId + "TCData");

        StaticText data =
            new StaticText();
        data.setId(anId + "StaticText");

        data.setValueExpression("text", 
                                getValueExpression(anExpression));

        result.getChildren().add(data);
        return result;
    }


    /**
     * initializes a table column to contain a selection checkbox
     * (for removing rows).  Note that the checkbox ID must be
     * "select" in order for select-all and deselect-all use-cases
     * to work properly (also, multiple nested-tables per config
     * are not supported)
     *
     * @param anId unique prefix for widget identifiers
     * @param anExpression  used to fetch/store selection state
     * @return the new table column widget
     */
    private static TableColumn addTableColumnCheckbox(String anId,
                                                      String anExpression)
    {
        TableColumn result =
            new TableColumn();
        result.setId(anId + "TCCB");
        result.setSelectId("select");

        Checkbox cb =
            new Checkbox();
        cb.setId("select"); // "select" is the only ID that will work

        cb.setValueExpression("text", 
                              getValueExpression(anExpression));

        result.getChildren().add(cb);
        return result;
    }

    /**
     * initializes a table column to obtain editable data from a provider
     *
     * @param anId unique prefix for widget identifiers
     * @param anExpression  used to fetch/store editable/edited data
     * @return the new table column widget
     */
    private static TableColumn addTableColumnEditField(String anId,
                                                       String anExpression)
    {
        TableColumn result =
            new TableColumn();
        result.setId(anId + "TCEditField");

        TextField field =
            new TextField();
        field.setId(anId + "TxtFld");

        field.setValueExpression("text", 
                                 getValueExpression(anExpression));

        result.getChildren().add(field);
        return result;
    }


    /**
     * creates a property sheet
     * @param anId for unique ID
     * @return the new property sheet 
     */
    private static PropertySheet createPS(String anId)
    {
        final String ps = "Ps";
        PropertySheet result =
            new PropertySheet();
        result.setId(anId + ps);
        return result;
    }

    /**
     * creates a property sheet section with a heading
     * @param anId for unique ID
     * @param aHeadingKey an I18n message key used to create the heading
     * @param aParent the existing property sheet to add the new property sheet section to
     * @return the new property sheet section
     */
    private static PropertySheetSection createPSS(String anId,
                                                  String aHeadingKey,
                                                  PropertySheet aParent)
    {
        final String pss = "Pss";
        PropertySheetSection result =
            new PropertySheetSection();
        result.setId(anId + pss);
        if (!(aHeadingKey.equals(""))) {
            String pssHeading = I18nUtilities.getResourceString(aHeadingKey);
            result.setLabel(pssHeading);
        }
        aParent.getChildren().add(result);
        return result;
    }

    /**
     * creates a labeled property, with optional required indicator
     * @param anIdPrefix for unique ID
     * @param aName the name of the property
     * @param aLabel the label to display for the property
     * @param isRequired if true, adds a required indicator to the property
     * @param aParent the existing property sheet section to add the new property to
     * @return the new property
     */
    private static Property createProperty(String anIdPrefix,
                                           String aName,
                                           String aLabel,
                                           boolean isRequired,
                                           PropertySheetSection aParent)
    {
        final String prop = "Prop";
        Property result =
            new Property();
        result.setId(anIdPrefix + prop);
        HiddenField propNameForDecode =
            createHiddenField(anIdPrefix,
                              aName,
                              result);

        Label label =
            createLabel(anIdPrefix,
                        isRequired,
                        aLabel,
                        result);
        aParent.getChildren().add(result);
        return result;
    }

    /**
     * creates a hidden field
     * @param anIdPrefix for unique ID
     * @param aNameForDecode a name used for retrieval
     * @param aParent the existing property to add the new hidden field to
     * @return the new hidden field property
     */
    private static HiddenField createHiddenField(String anIdPrefix,
                                                 String aNameForDecode,
                                                 Property aParent)
    {
        final String hdnFld = "HdnFld";
        HiddenField result =
            new HiddenField();
        result.setId(anIdPrefix + hdnFld);
        result.setText(aNameForDecode);
        aParent.getChildren().add(result);
        return result;
    }

    /**
     * creates a boolean input property (dropdown true|false)
     * @param anIdPrefix for unique ID
     * @param aNameForDecode to retrive the value during decode
     * @param aLabel to display for this property
     * @param aValue the current value to display|edit
     * @param aParent the existing property sheet section to add the new boolean dropdown property to
     * @return the new boolean dropdown property
     */
    private static Property createBooleanInputProperty(String anIdPrefix,
                                                       String aNameForDecode,
                                                       String aLabel,
                                                       String aValue,
                                                       PropertySheetSection aParent)
    {
        Property result;
        ArrayList trueFalse =
            new ArrayList();
        trueFalse.add(Boolean.toString(true));
        trueFalse.add(Boolean.toString(false));

        result = createDropDownProperty(anIdPrefix,
                                        aNameForDecode,
                                        aLabel,
                                        trueFalse,
                                        aValue,
                                        IS_NOT_SUBMIT_FORM,
                                        aParent);
        return result;
    }

    /**
     * @param anIdPrefix         Description of Parameter
     * @param aNameForDecode     Description of Parameter
     * @param aLabel          Description of Parameter
     * @param aSelectionList     Description of Parameter
     * @param aCurrentSelection  Description of Parameter
     * @param isSubmitForm       Description of Parameter
     * @param aParent            Description of Parameter
     * @return                   Description of the Returned Value
     */
    private static Property createDropDownProperty(String anIdPrefix,
                                                   String aNameForDecode,
                                                   String aLabel,
                                                   List<String> aSelectionList,
                                                   String aCurrentSelection,
                                                   boolean isSubmitForm,
                                                   PropertySheetSection aParent)
    {
        Property result;

        result =
            createProperty(anIdPrefix,
                           aNameForDecode,
                           aLabel,
                           false,
                           aParent);

        ArrayList<Option> optionsList =
            new ArrayList<Option>();
        for (int i = 0; i < aSelectionList.size(); ++i)
            {
                String selection = aSelectionList.get(i);
                String label = selection;
                Option option =
                    new Option(selection,
                               label);
                optionsList.add(option);
            }

        Option[] optionsArray =
            new Option[0];
        optionsArray =
            optionsList.toArray(optionsArray);

        DropDown dropDown =
            new DropDown();
        dropDown.setId(anIdPrefix + "dropDown");
        dropDown.setItems(optionsArray);

        dropDown.setSelected(aCurrentSelection);
        dropDown.setSubmitForm(isSubmitForm);

        result.getChildren().add(dropDown);

        return result;
    }


    /**
     * creates a labeled listbox property
     * @param anIdPrefix for unique ID
     * @param aNameForDecode to find value later
     * @param aLabelKey to display
     * @param aMinSize minimum width 
     * @param aMaxSize maximum width
     * @param aSelectionList choices to list
     * @param aCurrentSelection  current choice to highlight
     * @param aParent an existing property sheet section to add the new listbox property to
     * @return the new listbox property
     */
    private static Property createListboxProperty (String anIdPrefix,
                                                   String aNameForDecode,
                                                   String aLabelKey,
                                                   int aMinSize,
                                                   int aMaxSize,
                                                   List<String> aSelectionList,
                                                   String aCurrentSelection,
                                                   PropertySheetSection aParent)
    {
        Property result = createProperty (anIdPrefix,
                                          aNameForDecode,
                                          aLabelKey,
                                          false,
                                          aParent);

        ArrayList<Option> optionsList = new ArrayList<Option>();
        for (int i = 0; i < aSelectionList.size(); ++i)
        {
            String label = aSelectionList.get(i);
            Option option = new Option(label);
            optionsList.add(option);
        }

        Option[] optionsArray = new Option[0];
        optionsArray = optionsList.toArray(optionsArray);

        Listbox listBox = new Listbox();
        listBox.setId(anIdPrefix + "listBox");
        int size = aMinSize;
        if (aMinSize > aMaxSize)
            {
                size = aMaxSize;
            }
        listBox.setRows(size);
        listBox.setItems(optionsArray);
        result.getChildren().add(listBox);

        return result;
    }

    /**
     * creates a mapping of a zero or more endpoint names to their stats
     * @param anEndpointsCD with zero or more (consuming or providing) endpoint names
     * @param anInstanceName the glassfish instance name
     * @return map of endpoint name to its stats data
     */
    private static Map<String, CompositeData> createNameToEndpointStatsMap(CompositeData anEndpointsCD,
                                                                           String anInstanceName)
    {
        HashMap<String, CompositeData> result =
            new HashMap<String, CompositeData>();
        
        String[] endpointNames =
            CompositeDataUtils.findCompositeStringArray(anEndpointsCD,
                                                        JBIStatisticsItemNames
                                                        .ENDPOINTS_LIST_ITEM_NAME);
        
        for (int i = 0; i < endpointNames.length; ++i)
            {
                Properties status =
                    new Properties();
                
                RuntimeMonitoringUtils.addEndpointStatsToMap(endpointNames[i],
                                                             anInstanceName,
                                                             result,
                                                             status);
            }
        return result;
    }


    /**
     * creates a labeled numeric input property, with optional required indicator
     * and optional (limited) validation
     * @param anIdPrefix for unique ID
     * @param aNameForDecode for retrieving value later
     * @param aLabel to display
     * @param isPositiveOnly if true, style field for client-side javascript validation
     * @param isRequired if true, display required indicator
     * @param aValue current value to display|edit
     * @param aParent an existing property sheet section to add the new numeric input property to
     * @return the new numeric input field property
     */
    private static Property createNumericInputProperty(String anIdPrefix,
                                                       String aNameForDecode,
                                                       String aLabel,
                                                       boolean isPositiveOnly,
                                                       boolean isRequired,
                                                       String aValue,
                                                       PropertySheetSection aParent)
    {
        String requiredStyle;
        if (isPositiveOnly)
            {
                requiredStyle =
                    FIELD_POSITIVE_INTEGER;
            }
        else
            {
                requiredStyle =
                    FIELD_INT_ALLOW_MINUS;
            }
        if (isRequired)
            {
                requiredStyle =
                    FIELD_REQUIRED +
                    " " +
                    requiredStyle;
            }
        Property result =
            createTextInputProperty(anIdPrefix,
                                    aNameForDecode,
                                    aLabel,
                                    requiredStyle,
                                    aValue,
                                    aParent);

        return result;
    }

    /**
     * creates a labeled password input property, with optional required indicator
     * @param anIdPrefix for unique ID
     * @param aNameForDecode  for later retrieval
     * @param aLabel to display
     * @param isRequired if true, add required indicator
     * @param aValue current value (usually placholder "***" which is never displayed)
     * @param aParent an existing property sheet section to add the password input property to
     * @return the new password input property
     */
    private static Property createPasswordInputProperty(String anIdPrefix,
                                                        String aNameForDecode,
                                                        String aLabel,
                                                        boolean isRequired,
                                                        String aValue,
                                                        PropertySheetSection aParent)
    {
        final String pwFld = "PwFld";
        Property result =
            createProperty(anIdPrefix,
                           aNameForDecode,
                           aLabel,
                           isRequired,
                           aParent);
        PasswordField passwordField =
            new PasswordField();
        passwordField.setId(anIdPrefix + pwFld);
        if (isRequired)
            {
                passwordField.setStyleClass(FIELD_REQUIRED);
            }
        passwordField.setPassword(aValue);
        result.getChildren().add(passwordField);
        return result;
    }


    /**
     * creates a labeled (read-only) static text property
     * @param anIdPrefix for unique ID
     * @param aLabel to display
     * @param aValue current value to display (read-only)
     * @param aParent the existing property sheet section to add the new static text property to
     * @return the new static text property
     */
    private static Property createStaticTextProperty(String anIdPrefix,
                                                     String aLabel,
                                                     String aValue,
                                                     PropertySheetSection aParent)
    {
        final String noDecode = "no decode; not an input";
        // Not I18n
        final String staticText = "StTxt";
        Property result =
            createProperty(anIdPrefix,
                           noDecode,
                           aLabel,
                           false,
                           aParent);
        StaticText text =
            new StaticText();
        text.setId(anIdPrefix + staticText);
        text.setText(aValue);
        result.getChildren().add(text);
        return result;
    }

    /**
     * creates a property containing a labelled text input field.
     * @param anIdPrefix used to create unique property IDs
     * @param aNameForDecode used to find the edited value after form submission
     * @param aLabel used to specify the label to be displayed
     * @param aRequiredStyle used for input validation, can be null (for optional
     * text strings) or contain one of more substrings: <code>required</code>,
     * <code>integer</code> and/or <code>port</code>
     * @param aValue the value to initialize the text input field
     * @param aParent the property sheet section that contains this property
     * @return a new Property with a label and a text field decorated with
     * required styles, if any, and a required indicator, if needed, and an
     * initial value, if provided.
     */
    private static Property createTextInputProperty(String anIdPrefix,
                                                    String aNameForDecode,
                                                    String aLabel,
                                                    String aRequiredStyle,
                                                    String aValue,
                                                    PropertySheetSection aParent)
    {
        final String txtFld = "TxtFld";
        boolean isRequired = false;
        if ((null != aRequiredStyle)
            &&(aRequiredStyle.contains(FIELD_REQUIRED)))
            {
                isRequired = true;
            }
        Property result =
            createProperty(anIdPrefix,
                           aNameForDecode,
                           aLabel,
                           isRequired,
                           aParent);
        TextField textField =
            new TextField();
        textField.setId(anIdPrefix + txtFld);
        if (null != aRequiredStyle)
            {
                textField.setStyleClass(aRequiredStyle);
            }
        textField.setText(aValue);
        result.getChildren().add(textField);
        return result;
    }


    /**
     * @param aCompositeData  Description of Parameter
     * @param aKey            Description of Parameter
     * @param aDefaultValue   Description of Parameter
     * @return                Description of the Returned Value
     */
    private static String formatDate(CompositeData aCompositeData,
                                     String aKey,
                                     String aDefaultValue)
    {
        String result =
            aDefaultValue;

        Date date =
            CompositeDataUtils.findCompositeDate(aCompositeData,
                                                 aKey);

        if (null != date)
            {
                DateFormat localizedDateFormat =
                    DateFormat.getDateInstance(DateFormat.LONG);

                DateFormat localizedTimeFormat =
                    DateFormat.getTimeInstance(DateFormat.LONG);
                result =
                    localizedDateFormat.format(date) +
                    " " +
                    localizedTimeFormat.format(date);
            }
        return result;
    }

    /**
     * @param aCompositeData  Description of Parameter
     * @param aKey            Description of Parameter
     * @param aDefaultValue   Description of Parameter
     * @return                Description of the Returned Value
     */
    private static String formatTime(CompositeData aCompositeData,
                                     String aKey,
                                     String aDefaultValue)
    {

        Object[] timeArray =
            CompositeDataUtils.findCompositeResponseTime(aCompositeData,
                                                         aKey);
        String result =
            aDefaultValue;

        if ((null != timeArray)
            && (0 < timeArray.length))
            {
                Object o =
                    timeArray[0];
                if (o instanceof Long)
                    {
                        Long time =
                            (Long) o;
                        result =
                            GuiUtil.getMessage(
                                               I18nUtilities
                                               .getResourceString("jbi.stats.response.time.format.nanoseconds"),
                                               timeArray);
                    }
            }
        return result;
    }

    /**
     * If there is data, adds a toggle to the table row group display,
     * and if there is "too much" data, sets the toggle to collapsed.
     * @param aTableRowGroup to set based on its count of rows
     * @param aCount the row count to consider
     */
    private static void setToggleByCount(TableRowGroup aTableRowGroup,
                                         int aCount)
    {
        
        // If there is data in the table,
        // provides a toggle button
        if (0 < aCount)
            {
                aTableRowGroup.setGroupToggleButton(true);
            }
        
        // If there is a lot of data in the table,
        // collapses it by default
        if (TRG_COLLAPSE_THRESHOLD < aCount)
            {
                aTableRowGroup.setCollapsed(true);
            }
    }
    
    
     
    /**
     * Default table width
     */
    private static final String DEFAULT_TABLE_WIDTH = "55";

    /**
     * Default max rows before collapsing a table row group
     */
    private static final int TRG_COLLAPSE_THRESHOLD = 25;

    /**
     * Default text columns
     */
    private static final int DEFAULT_TEXT_COLUMNS = 55;

    /**
     * Set up a flag for cluster profile support.
     */
    private static final boolean IS_CLUSTER_PROFILE =
        ClusterUtilities.isClusterProfile();

    /**
     * Set up the <code>com.sun.jbi.jsf</code> subcomponent logger.
     */
    private static Logger sLog = JBILogger.getInstance();

    /*
     *  used to filter data for table groups
     */
    private static final String SELECT_CONSUMING = "CONSUMING";
    private static final String SELECT_PROVIDING = "PROVIDING";
    private static final String SELECT_TOTALS = "TOTALS";

    private static final String PROP_PREFIX = "prop";

    private static final String FIELD_POSITIVE_INTEGER = "integer";

    // note replace intAllowMinus to permit negative inputs
    // (to workaround bug in some versions of glassfish js/adminjsf.js)
    // the downside is that no numeric checking is done in browser
    //private static final String FIELD_INT_ALLOW_MINUS = "intAllowMinus";
    private static final String FIELD_INT_ALLOW_MINUS = ""; // workaround

    private static final String FIELD_REQUIRED = "required";
    private static final String HELP_INLINE_TYPE_FIELD = "field";
    private static final int INDEX_EXPR = 1;
    private static final int INDEX_HEADING = 1;
    private static final int INDEX_ID = 0;
    private static final boolean IS_NOT_SUBMIT_FORM = false;

    private static final String VALUE_NOT_AVAILABLE = I18nUtilities
        .getResourceString("jbi.stats.value.not.available");

    /**
     * prevents subclassing and instantiation
     */
    private JSFUtils()
    {
    }
}

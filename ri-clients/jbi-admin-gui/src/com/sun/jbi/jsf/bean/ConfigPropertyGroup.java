/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  ConfigPropertyGroup.java
 */
package com.sun.jbi.jsf.bean;

import java.util.List;
import java.util.ArrayList;

/**
 * Defines when a set of related configuration properties are
 * to be grouped under a section title, based on the optional
 * Open ESB configuration extension elements (e.g.
 * <code>&lt;config:PropertyGroup ...&gt;</code> in a JBI
 * component's jbi.xml descriptor.
 *
 * @author   Sun Microsystems Inc.
 */
public class ConfigPropertyGroup
{
    // === constructor(s) ===

    /**
     * Empty Constructor (use setters after instantiation)
     */
    public ConfigPropertyGroup()
    {
    }

    // === getters ===

    /**
     * Gets the configuration properties in this group.
     *
     * @return  a List of <code>ConfigProperty</code> instances.
     */
    public List<ConfigProperty> getConfigProperties()
    {
    	if (mConfigProperties == null) {
    		mConfigProperties = new ArrayList<ConfigProperty>();
    	}
        return mConfigProperties;
    }

    /**
     * Gets the Display Name
     *
     * @return   The section name to be displayed when grouping properties
     *           in user interface
     */
    public String getDisplayName()
    {
        return mDisplayName;
    }

    /**
     * Gets the Display Description 
     *
     * @return   The DisplayDescription I18n value to be used for 
     *           group/section inline help or tool tips
     */
    public String getDisplayDescription()
    {
        return mDisplayDescription;
    }

    /**
     * Gets the Name attribute
     *
     * @return   The non-I18n Name of this property group
     */
    public String getName()
    {
        return mName;
    }

    // === setters ===

    /**
     * Sets the configuration properties in this group.
     *
     * @param aConfigProperties A list of <code>ConfigProperty</code>
     *                          instances contained in this group
     */
    public void setConfigProperties(List<ConfigProperty> aConfigProperties)
    {
        mConfigProperties = aConfigProperties;
    }

    /**
     * Sets the Display Name attribute
     *
     * @param aDisplayName  The I18n String to be displayed in the 
     *                      user interface for this group/section
     */
    public void setDisplayName(String aDisplayName)
    {
        mDisplayName = aDisplayName;
    }

    /**
     * Sets the Display Description attribute
     *
     * @param aDisplayDescription  an I18n String to describe this group
     *                             e.g. for inline help or tooltip
     */
    public void setDisplayDescription(String aDisplayDescription)
    {
    	if (aDisplayDescription != null) {
            mDisplayDescription = aDisplayDescription;    		
    	}
    }

    /**
     * Sets the Name attribute
     *
     * @param aName  a non-I18n String to name this group
     */
    public void setName(String aName)
    {
        mName = aName;
    }

    /**
     * Converts to a String representation of the object.
     *
     * @return   A non-I18n String for logging the state of this object
     */
    public String toString()
    {
        StringBuffer result =
            new StringBuffer(CN);
        result.append(", mConfigProperties=[");
        if (null != mConfigProperties)
            {
                boolean first = true;
                for (int i = 0; i < mConfigProperties.size(); ++i)
                    {
                        if (!first)
                            {
                                result.append(", ");
                            }
                        else
                            {
                                first = false;
                            }
                        result.append(mConfigProperties.get(i));
                    }
            }
        result.append("], mDisplayDescription=");
        result.append(mDisplayDescription);
        result.append(", mDisplayName=");
        result.append(mDisplayName);
        result.append(", mName=");
        result.append(mName);

        return result.toString();
    }

    // === static fields ===

    private static final String CN = ConfigPropertyGroup.class.getName();

    // === member variables ===

    private String mDisplayDescription = "";
    private String mDisplayName;
    private List<ConfigProperty> mConfigProperties = null;
    private String mName;
}


/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  ArchiveBean.java
 */

package com.sun.jbi.jsf.bean;

import com.sun.jbi.jsf.util.JBILogger;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ArchiveBean
{

//Get Logger to log fine mesages for debugging
private static Logger sLog = JBILogger.getInstance();

public String getArchiveAbsolutePath()
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.getArchiveAbsolutePath(), result=" + mArchiveAbsolutePath);
	}
    return mArchiveAbsolutePath;
}

public String getArchiveDisplayName()
{
	if (sLog.isLoggable(Level.FINEST)){
	    sLog.finest("ArchiveBean.getArchiveDisplayName(), result=" + mArchiveDisplayName);
	}
    return mArchiveDisplayName;
}

public String getDescription()
{
	if (sLog.isLoggable(Level.FINEST)){
       sLog.finest("ArchiveBean.getDescription(), result=" + mDescription);
	}
    return mDescription;
}

public String getJbiName()
{
	if (sLog.isLoggable(Level.FINEST)){
	    sLog.finest("ArchiveBean.getJbiName(), result=" + mJbiName);
	}
    return mJbiName;
}

public String getJbiType()
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.getJbiType(), result=" + mJbiType);
	}
    return mJbiType;
}

/**
 * Get true if the provided  zip archive is not readable else false
 * @return - true if there is zipFile reading/processing error
 
 */
public  boolean getZipFileReadError ()
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.getZipFileReadError(), zReadError=" + mZipFileReadError);
	}
	
    return mZipFileReadError;
}

/**
 * Get true if the provided  archive file is readable 
 * @return - true if there is error in reading the provided file
 *         - false otherwise
 */

public boolean getFileReadError ()
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.getFileReadError(), fReadError=" + mFileReadError);
	}
	
    return mFileReadError;
}

/**
 * Get if the provided zip archive has jbi xml
 * @return  true if archive has jbi.xml file
 */
public  boolean getHasJbiXml ()
{
	if (sLog.isLoggable(Level.FINEST)){
       sLog.finest("ArchiveBean.getHasJbiXml(), archHasJbi=" + mHasJbiXml);
	}
    return mHasJbiXml;
}

/*
 * Get the version of the archive that specified in jbi.xml
 * 
 * @return  mVersion - the version + build number of the archive
 */
public String getVersion() {
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.getVersion(), Version=" + mVersion);
	}
	
	return mVersion;
}

/*
 * Get the version of the archive that specified in jbi.xml
 * 
 * @return  mBuildNumber - the version + build number of the archive
 */
public String getBuildNumber() {
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.getBuildNumber(), Build Number=" + mBuildNumber); 
	}
	return mBuildNumber;
}

/**
 * Gets the ShowVersion attribute of the ArchiveBean object
 *
 * @return   The ShowVersion value
 */
public Boolean getShowVersionInfo() {
    return Boolean.valueOf(mShowVersionInfo);	
}


public boolean isArchiveValid()
{
	if (sLog.isLoggable(Level.FINEST)){
       sLog.finest("ArchiveBean.isArchiveValid), result=" + mIsArchiveValid); 
	}
    return mIsArchiveValid;

}

public void setArchiveAbsolutePath(String anArchiveAbsolutePath)
{
	if (sLog.isLoggable(Level.FINEST)){
      sLog.finest("ArchiveBean.setArchiveAbsolutePath(" + anArchiveAbsolutePath + ")");
	}
    mArchiveAbsolutePath = anArchiveAbsolutePath;
}

public void setArchiveDisplayName(String anArchiveDisplayName)
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.setArchiveDisplayName(" + anArchiveDisplayName + ")");
	}
    mArchiveDisplayName = anArchiveDisplayName;
}

public void setIsArchiveValid(boolean isArchiveValid)
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.setIsArchiveValid(" + isArchiveValid + ")");
	}
    mIsArchiveValid = isArchiveValid;
}

public void setDescription(String aDescription)
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.setDescription(" + aDescription + ")");
	}
    mDescription = aDescription;
}

public void setJbiName(String aJbiName)
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.setJbiName(" + aJbiName + ")");
	}
    mJbiName = aJbiName;
}

public void setJbiType (String aJbiType)
{
	if (sLog.isLoggable(Level.FINEST)){
       sLog.finest("ArchiveBean.setJbiType(" + aJbiType + ")");
	}
    if (null == aJbiType)
	{
	    throw new NullPointerException("ArchiveBean.setJbiType(null)");
	}
    mJbiType = aJbiType;
}

/*
 * Set to true after archive entered by user is invalidated because it cannot be read
 * @pararm aFileReadError - true , if input archive file cannot be read
 */
public void setFileReadError (boolean aFileReadError)
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.setFileReadError(" + aFileReadError + ")"); 
	}
    mFileReadError = aFileReadError;
}


/*
 * Set to true  after zip archive entered by user is invalidated when it cannot be unzipped
 *@pararm aFileReadError - true , if input archive file is not recognised as zip file by ZipInputStream
 */
public void setZipFileReadError(boolean aZipFileReadError)
{
	if (sLog.isLoggable(Level.FINEST)){
         sLog.finest("ArchiveBean.setZipFileReadError(" + aZipFileReadError + ")");
	}
    mZipFileReadError = aZipFileReadError;
}


/*
 * Set true after zip archive entered by user is invalidated because it does not have decriptor file
 * @param aHasJbiXml - true , if input archive file misses jbi.xml in its contents
 */
public void setHasJbiXml(boolean aHasJbiXml)
{
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.setHasJbiXml(" + aHasJbiXml + ")"); 
	}
    mHasJbiXml = aHasJbiXml;
}


/*
 * Set the version of the archive that specified in jbi.xml
 * @param aVersion - the version of the archive
 */
public void setVersion(String aVersion) {
	if (sLog.isLoggable(Level.FINEST)){
        sLog.finest("ArchiveBean.setVersion(" + aVersion + ")"); 
	}
	mVersion = aVersion;
}

/*
 * Set the build number of the archive that specified in jbi.xml
 * @param aBuildNumber - the build number of the archive
 */
public void setBuildNumber(String aBuildNumber) {
	if (sLog.isLoggable(Level.FINEST)){
       sLog.finest("ArchiveBean.setBuildNumber(" + aBuildNumber + ")"); 
	}
	mBuildNumber = aBuildNumber;
}

/*
 * Set the showversion flag
 * @param bool - true, iff to display the component's version
 */
public void setShowVersionInfo(boolean bool) {
	mShowVersionInfo = bool;
}

private String mArchiveAbsolutePath;
private String mArchiveDisplayName;
private String mDescription;
private boolean mIsArchiveValid;
private String mJbiName;
private String mJbiType;
private boolean mFileReadError = false;
private boolean mZipFileReadError = false;
private boolean mHasJbiXml = true;
private boolean mShowVersionInfo = false;
private String mVersion;
private String mBuildNumber;
}


/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  SelectableAppConfig.java
 */

package com.sun.jbi.jsf.bean;

/**
 * represents a named Application Configuration
 * to be displayed in a selectable table row
 * in support of select-for-deletion and
 * click-name for viewing/editing use-cases.
 */
public class SelectableAppConfig
{

    /**
     * Constructs a selectable named row, initially not selected.
     * @param aName representing a named application configuration 
     */
    public SelectableAppConfig(String aName)
    {
        mName = aName;
    }

    /**
     * Gets the application configuration name
     * @return  name representing a named application configuration 
     */
    public String getName()
    {
        return mName;
    }

    /**
     * Gets the row selection state
     * @return  true if the row is selected
     */
    public boolean getSelected()
    {
        return mSelected;
    }


    /**
     * Sets the row selection state
     * @param aSelection <code>true</code> if the row is selected,
     * or <code>false</code> if the row is not selected
     */
    public void setSelected (boolean aSelection)
    {
        mSelected = aSelection;
    }

    private String             mName;
    private boolean            mSelected = false;
}


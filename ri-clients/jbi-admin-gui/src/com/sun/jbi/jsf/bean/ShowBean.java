/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  ShowBean.java
 */
package com.sun.jbi.jsf.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.openmbean.TabularData;
import com.sun.data.provider.TableDataProvider;
import com.sun.data.provider.impl.ObjectListDataProvider;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.ClusterUtilities;
import com.sun.jbi.jsf.util.CompStatsUtils;
import com.sun.jbi.jsf.util.I18nUtilities;
import com.sun.jbi.jsf.util.JBIConstants;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.JBIUtils;
import com.sun.jbi.jsf.util.JSFUtils;
import com.sun.jbi.jsf.util.RuntimeMonitoringUtils;
import com.sun.jbi.jsf.util.SaStatsUtils;
import com.sun.jbi.jsf.util.SharedConstants;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.webui.jsf.component.DropDown;
import com.sun.webui.jsf.component.Label;
import com.sun.webui.jsf.component.Property;
import com.sun.webui.jsf.component.PropertySheet;
import com.sun.webui.jsf.component.PropertySheetSection;
import com.sun.webui.jsf.component.TextField;
import com.sun.webui.jsf.model.Option;

/**
 * Provides properties used to populate JBI Show view properties and metadata
 *
 * @author   Sun Microsystems Inc.
 */
public class ShowBean
{

    /**
     * Constructor for the ShowBean object
     */
    public ShowBean()
    {
        mJac = BeanUtilities.getClient();
    }


    // getters

    /**
     * get contents of /META-INF/jbi.xml for this "component"
     *
     * @return   the JBI deployment descriptor in a (validated) XML String
     */
    public String getDeploymentOrInstallationDescriptor()
    {
        String result =
            queryDeploymentOrInstallationDescriptor();

        if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.getDeploymentOrInstallationDescriptor(),result=" + result);
        }
        return result;
    }

    /**
     * get the "component" description
     *
     * @return   the JBI "component" description
     */
    public String getDescription()
    {
        String result = 
            queryDescription();

        if (sLog.isLoggable(Level.FINEST)){
          sLog.finest("ShowBean.getDescription(), result=" + result);
        }
        return result;
    }

    /**
     * get the "component" state
     * 
     * @return the JBI "component" state
     */
    public String getState() 
    {
    	return mState;
    }    
    
    /**
     * get the "component" status summary of clustered environment
     * 
     * @return the JBI "component" status summary of clustered environment
     */
    public String getSummaryStatus() 
    {
    	return mSummaryStatus;
    }
    
    /**
     * get the constant of shutdown state 
     * @return the constant of shutdown state 
     */
    public String getSHUTDOWN_STATE(){
    	return JBIComponentInfo.SHUTDOWN_STATE;
    }
    
    
    /**
     * get the constant of stopped state 
     * @return the constant of stopped state 
     */
    public String getSTOPPED_STATE(){
    	return JBIComponentInfo.STOPPED_STATE;
    }
    
    /**
     * get the constant of started state 
     * @return the constant of started state 
     */
    public String getSTARTED_STATE(){
    	return JBIComponentInfo.STARTED_STATE;
    }
    
    /**
     * get the constant of unknown state 
     * @return the constant of unknown state 
     */
    public String getUNKNOWN_STATE(){
    	return JBIComponentInfo.UNKNOWN_STATE;
    }

    /**
     * Gets the no target attribute of the ShowBean object
     *
     * @return  true if there is no target associated with this component
     */
    public boolean isNoTarget() {
    	String noTarget = I18nUtilities.getResourceString("jbi.operations.no.targets");    	
    	if (noTarget != null && noTarget.equals(mSummaryStatus)) {
            return true;    		
    	}
    	
        return false;    	
    }
    
    
    /**
     * Gets the ShowVersion attribute of the ShowBean object
     *
     * @return   The ShowVersion value
     */
    public Boolean getShowVersionInfo() {    	
        return Boolean.valueOf(mShowVersionInfo);	
    }
    
    
    /**
     * get the "component" version
     *
     * @return   the JBI "component" version
     */
    public String getVersion()
    {
    	return mVersion;
    }
    
    /**
     * get the "component" version
     *
     * @return   the JBI "component" version
     */
    public String getBuildNumber()
    {
    	return mBuildNumber;
    }
    
    /**
     * get the "component" name
     *
     * @return   the JBI "component" name
     */
    public String getName()
    {
        return mName;
    }

    /**
     * Gets the SharedTargetsTableData attribute of the ShowBean object
     *
     * @return   The SharedTargetsTableData value
     */
    public TableDataProvider getSharedTargetsTableData()
    {
        TableDataProvider result = 
        	          new ObjectListDataProvider(mTargetsList);
        
        if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.getSharedTargetsTableData(): result=" + result);
        }
        return result;
    }


    /**
     * Gets the CompAppConfigSupported attribute of the ShowBean object
     *
     * @return   The CompAppConfigSupported value
     */
    public boolean isCompAppConfigSupported()
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.isCompAppConfigSupported(), result=" + mIsCompAppConfigSupported);
    	}
        return mIsCompAppConfigSupported;
    }
    
    /**
     * Sets the CompAppConfigSupported attribute 
     *
     * @param isCompAppConfigSupported indicates if the current 
     * component has declared supported for the component
     * application configuration (optional) extension
     */
    public void setCompAppConfigSupported(boolean isCompAppConfigSupported)
    {
        mIsCompAppConfigSupported = isCompAppConfigSupported;
        
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.setIsCompAppConfigSupported(" + isCompAppConfigSupported + ")");
    	}
    }


    /**
     * provides table data with list of application configuration names for
     * the current component.
     *
     * @return   TableDataProvider with selectable row beans
     */
    public TableDataProvider getCompAppConfigNamesTableData()
    {
        TableDataProvider result = null;

        if (null != mCompAppConfigNames)
        {
            SelectableAppConfigList appConfigList =
                        new SelectableAppConfigList(mCompAppConfigNames);

            result =
                        appConfigList.getListAppConfigsTableData();

        }

    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.getCompAppConfigNamesTableData(), result=" + result);
    	}
        return result;
    }



    /**
     * get the targets list
     *
     * @return   a List of zero or more targets for this component/deployment.
     *      An empty list implies installed/deployed to 'domain' only.
     */
    public List getTargetsList()
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.getTargetsList(), mTargetsList=" + mTargetsList);
    	}
        return mTargetsList;
    }

    /**
     * get the "component" type
     *
     * @return   the JBI "component" type (one of: <code>binding-component,
     *service-assembly, service-engine, shared-library</code>)
     */
    public String getType()
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.getType(), mType=" + mType);
    	}
        return mType;
    }


    /**
     * get the target names
     *
     * @return   an Array of zero or more target names for this
     *      component/deployment.
     */
    public String[] getTargetNames()
    {
        return mTargetNames;
    }


    /**
     * get the target names
     *
     * @return   a Array of zero or more target names for this
     *      component/deployment.
     */
    public String[] getOriginalTargetNames()
    {
        return mOriginalTargetNames;
    }


    /**
     * Gets the FilterTargetComponentState attribute of the ShowBean object
     *
     * @return   The FilterTargetComponentState value
     */
    public String getFilterTargetComponentState()
    {
        return mFilterTargetComponentState;
    }


    /**
     * Gets the FilterTargetComponentStates attribute of the ShowBean object
     *
     * @return   The FilterTargetComponentStates value
     */
    public ArrayList getFilterTargetComponentStates()
    {
        return mFilterTargetComponentStates;
    }


    /**
     * Gets the FilterTargetAssemblyState attribute of the ShowBean object
     *
     * @return   The FilterTargetAssemblyState value
     */
    public String getFilterTargetAssemblyState()
    {
        return mFilterTargetAssemblyState;
    }


    /**
     * Gets the FilterTargetAssemblyStates attribute of the ShowBean object
     *
     * @return   The FilterTargetAssemblyStates value
     */
    public ArrayList getFilterTargetAssemblyStates()
    {
        return mFilterTargetAssemblyStates;
    }


    //    public void setRenderAlertOff() {
    //    renderAlertMessage = false;
    //}



    /**
     * Gets the CompStatsPropertySheet attribute of the ShowBean object
     *
     * @return   The CompStatsPropertySheet value
     */
    public PropertySheet getCompStatsPropertySheet()
    {
        String clusterNameOrSaInstanceName =
            ClusterUtilities.getInstanceDomainCluster(mInstanceName);

        TabularData componentTabularData =
            CompStatsUtils.getStats(mName,
                                    clusterNameOrSaInstanceName);

        Properties statusProps =
            new Properties();

        TabularData nmrTabularData =
            RuntimeMonitoringUtils.getNMRStats(clusterNameOrSaInstanceName,
                                               statusProps);

        TabularData consumingEndpointsTabularData =
            JBIUtils
            .getEndpoints(SharedConstants
                          .ENDPOINTS_TYPE_CONSUMING,
                          mName,
                          mInstanceName);
           
        TabularData providingEndpointsTabularData =
            JBIUtils
            .getEndpoints(SharedConstants
                          .ENDPOINTS_TYPE_PROVIDING,
                          mName,
                          mInstanceName);

        PropertySheet result =
            JSFUtils.getCompStatsPS(componentTabularData,
                                    consumingEndpointsTabularData,
                                    providingEndpointsTabularData,
                                    mName,
                                    mInstanceName);
        return result;
    }

    /**
     * Gets the ServiceAssemblyStatsPS attribute of the ShowBean object
     *
     * @return   The ServiceAssemblyStatsPS value
     */
    public PropertySheet getServiceAssemblyStatsPS()
    {
        PropertySheet result =
            null;

        String targetName =
            ClusterUtilities.getInstanceDomainCluster(mInstanceName);

        TabularData saTabularData =
            SaStatsUtils.getStats(targetName,
                                  mName);

        result =
            JSFUtils.getSaStatsPS(saTabularData,
                                  mInstanceName,
                                  mName);
        return result;
    }

    /**
     * Gets the CreateCompAppVarsPropertySheet attribute of the ShowBean
     * object
     *
     * @return   The CreateCompAppVarsPropertySheet value
     */
    public PropertySheet getCreateCompAppVarsPropertySheet()
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.getCreateCompAppVarsPropertySheet()");
    	}
        return getCompAppVarsPropertySheet("create");
    }

    /**
     * Gets the EditCompAppVarsPropertySheet attribute of the ShowBean object
     *
     * @return   The EditCompAppVarsPropertySheet value
     */
    public PropertySheet getEditCompAppVarsPropertySheet()
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.getEditCompAppVarsPropertySheet()");
    	}
        return getCompAppVarsPropertySheet("edit");
    }

    /**
     * Sets the CompAppConfigNamesTableData attribute of the ShowBean object
     *
     * @param aCompName       The new CompAppConfigNamesTableData value
     * @param aCompType       The new CompAppConfigNamesTableData value
     * @param anInstanceName  The new CompAppConfigNamesTableData value
     */
    public void setCompAppConfigNamesTableData(String aCompName,
                String aCompType,
                String anInstanceName)
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.setCompAppConfigNamesTableData(" +
                    aCompName + ", " + aCompType +
                    ", " + anInstanceName);
    	}

        mName = aCompName;
        mType = aCompType;
        mInstanceName = anInstanceName;
        mCompAppConfigNames = null;

        try
        {
            mCompAppConfigNames =
                        mJac.listApplicationConfigurationNames(aCompName,
                        anInstanceName);
        }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
        {
            sLog.log(Level.FINE,
                        ("ShowBean.setCompAppConfigNamesTableData() caught jrEx=" +
                        jrEx),
                        jrEx);
        }
        
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.setCompAppConfigNamesTableData(...), mCompAppConfigNames=" +
                    mCompAppConfigNames);
    	}

    }

    /**
     * Creates an empty set of Application Configuration properties for the
     * specified component
     *
     * @param aCompName       String with the component name
     * @param anInstanceName  String with the instance name
     */
    public void setNewCompAppConfigProps(String aCompName,
                String anInstanceName)
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.setNewCompAppConfigProps(" + aCompName +
                    ", " + anInstanceName);
    	}    

        mInstanceName = anInstanceName;
        mCompAppConfigName = null;
    }


    /**
     * Sets the Application Configuration properties for the specified
     * component
     *
     * @param aCompName        String with the component name
     * @param anInstanceName   String with the instance name
     * @param anAppConfigName  String with the Application Configuration name
     */
    public void setCompAppConfigProps(String aCompName,
                String anInstanceName,
                String anAppConfigName)
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.setCompAppConfigProps(" + aCompName +
                    ", " + anInstanceName +
                    ", " + anAppConfigName + ")");
    	}

        mName = aCompName;
        mInstanceName = anInstanceName;
        mCompAppConfigName = anAppConfigName;
    }

    /**
     * Sets the Component Statistics properties for the specified component
     *
     * @param aCompName       String with the component name
     * @param anInstanceName  String with the instance name
     */
    public void setCompStatsProps(String aCompName,
                String anInstanceName)
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.setCompStatsProps(" + aCompName +
                    ", " + anInstanceName + ")");
    	}

        mName = aCompName;
        mInstanceName = anInstanceName;
    }

    /**
     * Sets the Service Assembly Statistics properties for the specified
     * component
     *
     * @param anSaName        String with the Service Assembly name
     * @param anInstanceName  String with the instance name
     */
    public void setSaStatsProps(String anSaName,
                String anInstanceName)
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.setSaStatsProps(" + anSaName +
                    ", " + anInstanceName + ")");
    	}

        mName = anSaName;
        mInstanceName = anInstanceName;
    }

    // setters

    /**
     * @param ignored  The new DeploymentOrInstallationDescriptor value
     */
    public void setDeploymentOrInstallationDescriptor(String ignored)
    {
        // required for introspection, but never should be called
        throw new
            RuntimeException("ShowBean.setDeploymentOrInstallationDescriptor(String)" +
                             " not supported (read-only property)");
    }

    /**
     * set the "component" description
     *
     * @param ignored       The new Description value
     */
    public void setDescription(String ignored)
    {
        throw new RuntimeException("ShowBean.setDescription(String) not supported (read-only property)");
    }

    /**
     * set the "component" version and build number
     *
     */
    public void setJBIBeanInfo()
    {
    	queryJBIBeanInfo();
    }
    
        
    /**
     * set the "component" name
     *
     * @param aName  a JBI "component" name
     */
    public void setName(String aName)
    {
        mName = aName;
    }

    /**
     * set the targets list
     *
     * @param aTargetsList  a List of zero or more targets for this
     *      component/deployment. An empty list implies installed/deployed to
     *      'domain' only.
     */
    public void setTargetsList(List aTargetsList)
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.setTargetsList(" + aTargetsList + ")");
    	}
        mTargetsList = aTargetsList;
    }

    /**
     * set the "component" type
     *
     * @param aType  a "component" type (one of: binding-component,
     *      service-assembly, service-engine, shared-library)
     */
    public void setType(String aType)
    {
        mType = aType;
    }


    /**
     * Sets the TargetNames attribute of the ShowBean object
     *
     * @param aTargetNames  The new TargetNames value
     */
    public void setTargetNames(String[] aTargetNames)
    {
        mTargetNames = aTargetNames;
    }


    /**
     * Sets the FilterTargetComponentState attribute of the ShowBean object
     *
     * @param filterState  The new FilterTargetComponentState value
     */
    public void setFilterTargetComponentState(String filterState)
    {
        mFilterTargetComponentState = filterState;
    }


    /**
     * Sets the FilterTargetComponentState attribute of the ShowBean object
     *
     * @param filterState   The new FilterTargetComponentState value
     * @param filterStates  The new FilterTargetComponentState value
     */
    public void setFilterTargetComponentState(String filterState,
                String filterStates)
    {
        mFilterTargetComponentState = filterState;
        filterStates = filterStates.replace('[', ' ');
        filterStates = filterStates.replace(']', ' ');
        String[] states = filterStates.split("\\,");
        for (int i = 0; i < states.length; i++)
        {
            states[i] = states[i].trim();
        }
        mFilterTargetComponentStates = new
                    ArrayList(Arrays.asList(states));
    }


    /**
     * Sets the FilterTargetAssemblyState attribute of the ShowBean object
     *
     * @param filterState  The new FilterTargetAssemblyState value
     */
    public void setFilterTargetAssemblyState(String filterState)
    {
        mFilterTargetAssemblyState = filterState;
    }


    /**
     * Sets the FilterTargetAssemblyState attribute of the ShowBean object
     *
     * @param filterState   The new FilterTargetAssemblyState value
     * @param filterStates  The new FilterTargetAssemblyState value
     */
    public void setFilterTargetAssemblyState(String filterState,
                String filterStates)
    {
        mFilterTargetAssemblyState = filterState;
        filterStates = filterStates.replace('[', ' ');
        filterStates = filterStates.replace(']', ' ');
        String[] states = filterStates.split("\\,");
        for (int i = 0; i < states.length; i++)
        {
            states[i] = states[i].trim();
        }
        mFilterTargetAssemblyStates = new ArrayList(Arrays.asList(states));
    }

    /**
     * Sets the ServiceAssemblyStatsPS attribute of the ShowBean object
     *
     * @param aPropertySheet  The new ServiceAssemblyStatsPS value
     */
    public void setServiceAssemblyStatsPS(PropertySheet aPropertySheet)
    {
        // no-op
    }


    /**
     * Required but not used setter for read only property sheet.
     *
     * @param anIgnoredArg  not used
     */
    public void setCompStatsPropertySheet(PropertySheet anIgnoredArg)
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.setCompStatsPropertySheet(" +
                    anIgnoredArg + ")");
    	}
    }

    /**
     * required, but not used
     *
     * @param aPropertySheet  The new CreateCompAppConfigPropertySheet value
     */
    public void setCreateCompAppConfigPropertySheet(PropertySheet aPropertySheet)
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.setCreateCompAppConfigPropertySheet(" +
                    aPropertySheet + ")");
    	}
    }

    /**
     * required, but not used
     *
     * @param aPropertySheet  The new EditCompAppConfigPropertySheet value
     */
    public void setEditCompAppConfigPropertySheet(PropertySheet aPropertySheet)
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.setEditCompAppConfigPropertySheet(" +
                    aPropertySheet + ")");
    	}
    }


    /**
     * required, but not used
     *
     * @param aPropertySheet  The new CreateCompAppVarsPropertySheet value
     */
    public void setCreateCompAppVarsPropertySheet(PropertySheet aPropertySheet)
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.setCreateCompAppVarsPropertySheet(" +
                    aPropertySheet + ")");
    	}
    }

    /**
     * required, but not used
     *
     * @param aPropertySheet  The new EditCompAppVarsPropertySheet value
     */
    public void setEditCompAppVarsPropertySheet(PropertySheet aPropertySheet)
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.setEditCompAppVarsPropertySheet(" +
                    aPropertySheet + ")");
    	}
    }

  
    
    /**
     * @param aTarget  Description of Parameter
     * @return         String - status (or null if not installed/deployed)
     */
    public String check(String aTarget)
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.check(" + aTarget + ")");
    	}
    	
        String result = null;
        // "not found"

        String queryResult = null;

        boolean isServiceAssembly = false;
        boolean isSharedLibrary = false;

        if (null != mJac)
        {
            try
            {

                if (JBIConstants.JBI_BINDING_COMPONENT_TYPE.equals(mType))
                {
                    queryResult =
                                mJac.showBindingComponent(mName,
                                null, /* no state check */
                                null, /* no library check */
                                null, /* no deployment check */
                                aTarget);
                }
                else if (JBIConstants.JBI_SERVICE_ENGINE_TYPE.equals(mType))
                {
                    queryResult =
                                mJac.showServiceEngine(mName,
                                null, /* no state check */
                                null, /* no library check */
                                null, /* no deployment check */
                                aTarget);

                }
                else if (JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE.equals(mType))
                {
                    queryResult =
                                mJac.showServiceAssembly(mName,
                                null, /* no state check */
                                null, /* no component check */
                                aTarget);
                    isServiceAssembly = true;
                }
                else if (JBIConstants.JBI_SHARED_LIBRARY_TYPE.equals(mType))
                {
                    queryResult =
                                mJac.showSharedLibrary(mName,
                                null, /* no component check */
                                aTarget);
                    isSharedLibrary = true;
                }
                else
                {
                	if (sLog.isLoggable(Level.FINEST)){
                       sLog.finest("ShowBean.check--unrecognized mType=" + mType);
                	}
                }

                if (null != queryResult)
                {
                	if (sLog.isLoggable(Level.FINEST)){
                        sLog.finest("ShowBean.check(" + aTarget + "), queryResult=" + queryResult);
                	}
                	
                    List list;
                    if (isServiceAssembly)
                    {
                        list =
                                    ServiceAssemblyInfo.readFromXmlTextWithProlog(queryResult);
                        if (1 == list.size())
                        {
                            ServiceAssemblyInfo saInfo
                                         = (ServiceAssemblyInfo) list.get(0);
                            String state = saInfo.getState();

                            if (JBIComponentInfo.STARTED_STATE.equals(state))
                            {
                                result
                                            = I18nUtilities.getResourceString("jbi.operations.comp.started");
                            }
                            else
                            {
                                result = "Disabled(" + state + ")";
                            }
                        }
                    }
                    else
                    {
                        list = JBIComponentInfo.readFromXmlText(queryResult);

                        if (1 == list.size())
                        {
                            JBIComponentInfo compInfo =
                                        (JBIComponentInfo) list.get(0);
                            
                            if (isSharedLibrary)
                            {
                                // has no lifecycle state

                                result = I18nUtilities.getResourceString("jbi.operations.start.enabled");
                            }
                            else
                            {
                                // BC or SE (has lifecycle state)

                                String state = compInfo.getState();

                                if (JBIComponentInfo.STARTED_STATE.equals(state))
                                {
                                    result =
                                                I18nUtilities.getResourceString("jbi.operations.comp.started");
                                }
                                else if (JBIComponentInfo.STOPPED_STATE.equals(state))
                                {
                                    result =
                                                I18nUtilities.getResourceString("jbi.operations.comp.stopped");
                                }
                                else if (JBIComponentInfo.SHUTDOWN_STATE.equals(state))
                                {
                                    result =
                                                I18nUtilities.getResourceString("jbi.operations.comp.shutdown");
                                }
                                else
                                {
                                    result =
                                                I18nUtilities.getResourceString("jbi.operations.comp.state.unknown");
                                }
                            }
                        }
                    }
                }
            }
            catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
                sLog.log(Level.FINE,
                            ("ShowBean.check() caught jrEx=" +
                            jrEx),
                            jrEx);
            }
        }
        
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.check(" + aTarget +
                    "), mName=" + mName +
                    ", mType=" + mType +
                    ", queryResult=" + queryResult +
                    ", result = " + result);
    	}
        return result;
    }

    /**
     * initialize the member varible target name list, which is dynamic and
     * also initialize the original target name list, which is static.
     */
    public void initTargetNames()
    {
        List targets = getTargetsList();
        mTargetNames = new String[targets.size()];
        mOriginalTargetNames = new String[targets.size()];
        for (int i = 0; i < targets.size(); i++)
        {
            SelectableTargetInfo tgtInfo =
                        (SelectableTargetInfo) targets.get(i);
            mTargetNames[i] = tgtInfo.getName();
            mOriginalTargetNames[i] = tgtInfo.getName();
        }
    }

    private static final boolean IS_CLUSTER_PROFILE = ClusterUtilities.isClusterProfile();

    /**
     * default result for queries when no data found
     */
    private static final String DEFAULT_RESULT = "";

    /**
     * Gets the CompAppVarsPropertySheet attribute of the ShowBean object
     *
     * @param anAction  Description of Parameter
     * @return          The CompAppVarsPropertySheet value
     */
    private PropertySheet getCompAppVarsPropertySheet(String anAction)
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.getEditCompAppVarsPropertySheet(" +
                    anAction + ")");
    	}

        PropertySheet propertySheet =
                    new PropertySheet();
        propertySheet.setId("editCompAppVarsPropertySheet");

        // Property sheet section for selection of instance/component/configuration
        List propertySectionList =
                    propertySheet.getChildren();

        PropertySheetSection propertySheetSection1 =
                    new PropertySheetSection();
        propertySheetSection1.setId("editCompAppVarsPss1");
        String propsPss1Label =
                    I18nUtilities.getResourceString("jbi.edit.comp.app.config.props.pss.selection.label");
        propertySheetSection1.setLabel(propsPss1Label);

        propertySectionList.add(propertySheetSection1);

        List propertyList1 = propertySheetSection1.getChildren();

        //
        if (IS_CLUSTER_PROFILE)
        {
            Property instanceNameProperty =
                        JSFUtils.createI18nTextProperty("instanceName",
                        "jbi.edit.comp.app.config.instance.label",
                        "#{jbiSelectedInstanceValue}",
                        false);
            propertyList1.add(instanceNameProperty);
        }

        //
        Property componentNameProperty =
                    JSFUtils.createI18nTextProperty("compName",
                    "jbi.edit.comp.app.config.component.label",
                    "#{sharedShowName}",
                    false);
        propertyList1.add(componentNameProperty);

        //
        // property sheet section for creating/editing properties
        //
        PropertySheetSection propertySheetSection2 =
                    new PropertySheetSection();
        propertySheetSection2.setId("editCompAppVarsPss2");
        String propsPss2Label =
                    I18nUtilities.getResourceString("jbi.edit.comp.app.vars.pss.new.var.label");
        propertySheetSection2.setLabel(propsPss2Label);

        propertySectionList.add(propertySheetSection2);

        List propertyList2 = propertySheetSection2.getChildren();

        if (null != mJac)
        {
            Properties appVars = null;
            try
            {
                appVars =
                            mJac.getApplicationVariables(mName,
                            mInstanceName);
                
            	if (sLog.isLoggable(Level.FINEST)){
                    sLog.finest("ShowBean.getEditCompAppVarsPropertySheet(" +
                            anAction + "), mName=" + mName +
                            ", mInstanceName=" + mInstanceName +
                            ", appVars=" + appVars);
            	}

            }
            catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
            {
                sLog.log(Level.FINE,
                            ("ShowBean.getEditCompAppVarsPropertySheet(" +
                            anAction + ") caught jrEx=" + jrEx),
                            jrEx);
            }
        }

        // Name
        Property nameProperty =
                    new Property();
        nameProperty.setId("NameProperty");
        List namePropSubElts =
                    nameProperty.getChildren();

        Label nameLabel =
                    new Label();
        nameLabel.setId("nameLabel");
        String nameLabelStr =
                    I18nUtilities.getResourceString("jbi.edit.comp.app.vars.new.var.name.label");
        nameLabel.setText(nameLabelStr);
        namePropSubElts.add(nameLabel);

        TextField nameValue =
                    new TextField();
        nameValue.setId("nameValue");
        nameValue.setText("");
        namePropSubElts.add(nameValue);

        propertyList2.add(nameProperty);

        // Type
        Property typeProperty =
                    new Property();
        typeProperty.setId("TypeProperty");
        List typePropSubElts =
                    typeProperty.getChildren();

        Label typeLabel =
                    new Label();
        typeLabel.setId("typeLabel");
        String typeLabelStr =
                    I18nUtilities.getResourceString("jbi.edit.comp.app.vars.new.var.type.label");
        typeLabel.setText(typeLabelStr);
        typePropSubElts.add(typeLabel);

        String booleanLabel =
                    I18nUtilities.getResourceString("jbi.show.comp.app.vars.typeIsBoolean");
        Option booleanOption =
                    new Option(SharedConstants.APP_VAR_TYPE_BOOLEAN,
                    booleanLabel);
        String numberLabel =
                    I18nUtilities.getResourceString("jbi.show.comp.app.vars.typeIsNumber");
        Option numberOption =
                    new Option(SharedConstants.APP_VAR_TYPE_NUMBER,
                    numberLabel);
        String passwordLabel =
                    I18nUtilities.getResourceString("jbi.show.comp.app.vars.typeIsPassword");
        Option passwordOption =
                    new Option(SharedConstants.APP_VAR_TYPE_PASSWORD,
                    passwordLabel);
        String stringLabel =
                    I18nUtilities.getResourceString("jbi.show.comp.app.vars.typeIsString");
        Option stringOption =
                    new Option(SharedConstants.APP_VAR_TYPE_STRING,
                    stringLabel);

        Option[] options = {booleanOption,
                    numberOption,
                    passwordOption,
                    stringOption};

        DropDown typeValue =
                    new DropDown();
        typeValue.setId("typeValue");

        typeValue.setItems(options);

        typeValue.setSelected(SharedConstants.APP_VAR_TYPE_STRING);
        typeValue.setSubmitForm(true);

        typePropSubElts.add(typeValue);

        propertyList2.add(typeProperty);

        // Value
        Property valueProperty =
                    new Property();
        valueProperty.setId("ValueProperty");
        List valuePropSubElts =
                    valueProperty.getChildren();

        Label valueLabel =
                    new Label();
        valueLabel.setId("valueLabel");
        String valueLabelStr =
                    I18nUtilities.getResourceString("jbi.edit.comp.app.vars.new.var.value.label");
        valueLabel.setText(valueLabelStr);
        valuePropSubElts.add(valueLabel);

        TextField valueValue =
                    new TextField();
        valueValue.setId("valueValue");
        valueValue.setText("");
        valuePropSubElts.add(valueValue);

        propertyList2.add(valueProperty);

    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.getEditCompAppVarsPropertySheet(" +
                    anAction + "), propertySheet=" +
                    propertySheet);
    	}

        return propertySheet;
    }


    // private methods

    /**
     * helper to initialize the "component" deployment/installation metadata
     *
     * @return   the contents of the /META-INF/jbi.xml in a validated XML
     *      String
     */
    private String queryDeploymentOrInstallationDescriptor()
    {
        String result = DEFAULT_RESULT;
        String descriptor = DEFAULT_RESULT;
        try
        {

            JBIAdminCommands client = BeanUtilities.getClient();

            if (null != client)
            {

            	if (sLog.isLoggable(Level.FINEST)){
                    sLog.fine("ShowBean.queryDeploymentOrInstallationDescriptor(), client=" +
                            client + ", mType=" + mType + ", mName=" + mName);
            	}    
                    
                if (JBIConstants.JBI_BINDING_COMPONENT_TYPE.equals(mType) ||
                            JBIConstants.JBI_SERVICE_ENGINE_TYPE.equals(mType))
                {
                    descriptor =
                                client.getComponentInstallationDescriptor(mName);
                }
                else if (JBIConstants.JBI_SHARED_LIBRARY_TYPE.equals(mType))
                {
                    descriptor =
                                client.getSharedLibraryInstallationDescriptor(mName);
                }
                else if (JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE.equals(mType))
                {
                    descriptor =
                                client.getServiceAssemblyDeploymentDescriptor(mName);
                }
                result = descriptor;
            }
        }
        catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
        {
            sLog.log(Level.FINE,
                        ("ShowBeanqueryDeploymentOrInstallationDescriptor(), caught jrEx=" +
                        jrEx),
                        jrEx);
        }
        
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.queryDeploymentOrInstallationDescriptor(), result=" + result);
    	}
        return result;
    }
    
    /**
     * initializes the bean info, state, version and build number etc 
     * 
     */
    private void queryJBIBeanInfo() 
    {    	
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.queryVersionInfo(), mType=" + mType);
    	}
    	
        ListBean listBean = BeanUtilities.getListBean();
        SelectableJBIComponentInfo jbiComponentInfo = null;
        SelectableJBIServiceAssemblyInfo jbiSAInfo = null;
        
        if (JBIConstants.JBI_BINDING_COMPONENT_TYPE.equals(mType))
        {
            List<SelectableJBIComponentInfo> bindings =
                        listBean.getBindingComponentsInfoList();
            jbiComponentInfo = listBean.getJBIComponentInfo(mName, bindings);
        } 
        else if (JBIConstants.JBI_SERVICE_ENGINE_TYPE.equals(mType))
        {
            List<SelectableJBIComponentInfo> engines =
                        listBean.getServiceEnginesInfoList();
            jbiComponentInfo = listBean.getJBIComponentInfo(mName, engines);
        }
        else if (JBIConstants.JBI_SHARED_LIBRARY_TYPE.equals(mType))
        {
            List<SelectableJBIComponentInfo> libraries =
                        listBean.getSharedLibrariesInfoList();
            jbiComponentInfo = listBean.getJBIComponentInfo(mName, libraries);
        }
        else if (JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE.equals(mType))
        {
            List<SelectableJBIServiceAssemblyInfo> serviceAssemblies =
                listBean.getServiceAssembliesInfoList();
            jbiSAInfo = listBean.getJBISAInfo(mName, serviceAssemblies);        	
        }
        	

    	if (jbiComponentInfo != null) {
    		if (jbiComponentInfo.getComponentVersion() != null && !jbiComponentInfo.getComponentVersion().equals("")) {
    			mVersion = jbiComponentInfo.getComponentVersion();
    			mBuildNumber = jbiComponentInfo.getBuildNumber();
    			mShowVersionInfo = true;
    		}
    		
    	    mState = jbiComponentInfo.getState();
    	    mSummaryStatus = jbiComponentInfo.getSummaryStatus();    		
    	}
        
    	if (jbiSAInfo != null) {
    		mShowVersionInfo = false;
    	    mState = jbiSAInfo.getState();
    	    mSummaryStatus = jbiSAInfo.getSummaryStatus();    		
    	}
    }
    
    
    /**
     * initializes the "component" description
     *
     * @return   the description (or an empty string if not found)
     */
    private String queryDescription()
    {
        String result = DEFAULT_RESULT;

        ListBean listBean = BeanUtilities.getListBean();

    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("ShowBean.queryDeploymentOrInstallationDescriptor(), mType=" +
                    mType);
    	}

        if (JBIConstants.JBI_BINDING_COMPONENT_TYPE.equals(mType))
        {
            List<SelectableJBIComponentInfo> bindings =
                        listBean.getBindingComponentsInfoList(mFilterTargetComponentState,
                        mFilterTargetComponentStates);
            result = listBean.findDescription(mName, bindings);
        }
        else if (JBIConstants.JBI_SERVICE_ASSEMBLY_TYPE.equals(mType))
        {
            List<SelectableJBIServiceAssemblyInfo> deployments =
                        listBean.getServiceAssembliesInfoList(mFilterTargetAssemblyState,
                        mFilterTargetAssemblyStates);
            result = listBean.findDescription(mName, deployments);
        }
        else if (JBIConstants.JBI_SERVICE_ENGINE_TYPE.equals(mType))
        {
            List<SelectableJBIComponentInfo> engines =
                        listBean.getServiceEnginesInfoList(mFilterTargetComponentState,
                        mFilterTargetComponentStates);
            result = listBean.findDescription(mName, engines);
        }
        else if (JBIConstants.JBI_SHARED_LIBRARY_TYPE.equals(mType))
        {
            List<SelectableJBIComponentInfo> libraries =
                        listBean.getSharedLibrariesInfoList();
            result = listBean.findDescription(mName, libraries);
        }

    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("ShowBean.queryDescription(), result=" + result);
    	}
    	
        return result;
    }

    //Get logger to log fine, info level messages in server.log file
    private Logger sLog = JBILogger.getInstance();


    // member variables

    /**
     * cached JBI Admin Commands client
     */
    private JBIAdminCommands mJac;

    /**
     * JBI "component" name
     */
    private String mName;

    /**
     * JBI "component" type (one of: binding-component, service-assembly,
     * service-engine, shared-library)
     */
    private String mType;

    /**
     * targets for this component (zero or more cluster name Strings and zero
     * or more Stand-Alone Instance name Strings.
     */
    private List mTargetsList = new ArrayList();

    /**
     * Holds the filter type for the components table
     */
    private String mFilterTargetComponentState = SharedConstants.DROP_DOWN_TYPE_SHOW_ALL;
    private ArrayList mFilterTargetComponentStates = null;

    /**
     * Holds the filter type for the components table
     */
    private String mFilterTargetAssemblyState = SharedConstants.DROP_DOWN_TYPE_SHOW_ALL;
    private ArrayList mFilterTargetAssemblyStates = null;

    private String[] mTargetNames = null;
    private String[] mOriginalTargetNames = null;

    private String mCompAppConfigName;
    private String[] mCompAppConfigNames;
    private String mInstanceName;
    private boolean mIsCompAppConfigSupported;
    private String mVersion;
    private String mBuildNumber;
    private boolean mShowVersionInfo;
    private String mState;
    private String mSummaryStatus;
}

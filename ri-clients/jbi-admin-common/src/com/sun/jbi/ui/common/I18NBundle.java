/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)I18NBundle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


/** This class reads the i18n strings from locale specific bundle from the
 * Bundle[locale].properties or bundle[locale].properties file in a specified
 * package. This class has methods for formating the messages.
 *
 * @author Sun Microsystems, Inc.
 */
public class I18NBundle
{
    /** package name */
    private String mBundlePackageName = null;
    /** resource bundle */
    private ResourceBundle mBundle = null;
    /**
     * constructor
     * @param packageName packe name ( e.g. com.sun.mypackage ) in which
     * to look for Bundle.properties file
     */
    public I18NBundle(String packageName)
    {
        this.mBundlePackageName = packageName;
        this.mBundle = null;
        
    }
    /**
     * loads the bundle
     * @param bundleName bundle name
     * @param packageName packe name ( e.g. com.sun.mypackage ) in which
     * to look for Bundle.properties file
     */
    private void loadBundle(String packageName, String bundleName)
    {
        
        String bundleBaseName = packageName + "." + bundleName;
        ResourceBundle resBundle = null;
        try
        {
            resBundle = ResourceBundle.getBundle(bundleBaseName);
        } catch (MissingResourceException ex )
        {
            // Try with locale independent defaultBundle
            try
            {
                resBundle = ResourceBundle.getBundle(bundleBaseName, new Locale(""));
            } catch ( Exception anyEx )
            {
                Util.logDebug(anyEx);
            }
        }
        
        if ( resBundle != null )
        {
            this.mBundle = resBundle;
        }
    }
    /**
     * gets the loaded resource bundle
     * @return resource bundle
     */
    public ResourceBundle getBundle()
    {
        // lazzy init
        if ( this.mBundle == null )
        {
            loadBundle(this.mBundlePackageName, "Bundle");
            // try to load the bundle with lower case first letter
            if ( this.mBundle == null )
            {
                loadBundle(this.mBundlePackageName, "bundle");
            }
        }
        return this.mBundle;
    }
    /** gets the i18n message
     * @param aI18NMsg String.
     * @param aArgs Object[]
     * @return formated i18n string.
     */
    public static String getFormattedMessage(
    String aI18NMsg, Object[] aArgs )
    {
        String formattedI18NMsg = aI18NMsg;
        try
        {
            MessageFormat mf = new MessageFormat(aI18NMsg);
            formattedI18NMsg = mf.format(aArgs);
        } catch (Exception ex)
        { }
        return formattedI18NMsg;
    }
    
    /** gets the i18n message
     * @param aI18NKey i18n key
     * @param anArgsArray array of arguments for the formatted string
     * @return formatted i18n string
     */
    public String getMessage( String aI18NKey, Object[] anArgsArray )
    {
        String i18nMessage = getBundle().getString(aI18NKey);
        if ( anArgsArray != null )
        {
            return getFormattedMessage(i18nMessage, anArgsArray);
        } else
        {
            return i18nMessage;
        }
    }
    
    /** gets the i18n message
     * @param aI18NKey i18n key
     * @return i18n string
     */
    public String getMessage( String aI18NKey )
    {
        return getMessage( aI18NKey, null );
    }
    
    /** gets the i18n message
     * @param aI18NKey i18n key
     * @param arg1 argrument object to message
     * @return i18n string
     */
    public String getMessage( String aI18NKey, Object arg1 )
    {
        Object[] args = { arg1 };
        return getMessage( aI18NKey, args );
    }

    /** gets the i18n message
     * @param aI18NKey i18n key
     * @param arg1 argrument object to message
     * @param arg2 argrument object to message
     * @return i18n string
     */
    public String getMessage( String aI18NKey, Object arg1, Object arg2 )
    {
        Object[] args = { arg1, arg2 };
        return getMessage( aI18NKey, args );
    }
    
    /** gets the i18n message
     * @param aI18NKey i18n key
     * @param arg1 argrument object to message
     * @param arg2 argrument object to message
     * @param arg3 argrument object to message
     * @return i18n string
     */
    public String getMessage( String aI18NKey, Object arg1, Object arg2, Object arg3 )
    {
        Object[] args = { arg1, arg2, arg3 };
        return getMessage( aI18NKey, args );
    }
    
    
    /**
     * main
     * @param args string array.
     */
    public static void main(String[] args )
    {
        Locale l = new Locale("");
        System.out.println("Locale : " + l);
        System.out.println("Default Locale : " + Locale.getDefault());
        I18NBundle bundle = new I18NBundle("com.sun.jbi.ui.ant");
        System.out.println(
        bundle.getMessage("jbi.ui.ant.jmx.msg.jmxmp.connected", new String[]
        {"xyz"}));
    }
    
}

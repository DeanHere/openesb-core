/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIAdminCommandsClientImpl.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.client;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.sun.esb.management.api.administration.AdministrationService;
import com.sun.esb.management.api.configuration.ConfigurationService;
import com.sun.esb.management.api.deployment.DeploymentService;
import com.sun.esb.management.api.installation.InstallationService;
import com.sun.esb.management.api.performance.PerformanceMeasurementService;
import com.sun.esb.management.api.runtime.RuntimeManagementService;
import com.sun.esb.management.client.ManagementClient;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.ApplicationVerificationReport;
import com.sun.esb.management.common.data.ComponentStatisticsData;
import com.sun.esb.management.common.data.EndpointStatisticsData;
import com.sun.esb.management.common.data.FrameworkStatisticsData;
import com.sun.esb.management.common.data.IEndpointStatisticsData;
import com.sun.esb.management.common.data.NMRStatisticsData;
import com.sun.esb.management.common.data.ServiceAssemblyStatisticsData;
import com.sun.esb.management.common.data.helper.ApplicationVerificationReportReader;
import com.sun.esb.management.common.data.helper.ComponentStatisticsDataReader;
import com.sun.esb.management.common.data.helper.EndpointStatisticsDataReader;
import com.sun.esb.management.common.data.helper.FrameworkStatisticsDataReader;
import com.sun.esb.management.common.data.helper.NMRStatisticsDataReader;
import com.sun.esb.management.common.data.helper.ServiceAssemblyStatisticsDataReader;
import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIArchive;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.ServiceAssemblyDD;

/**
 * Implements the JBIAdminCommands interfaces on the client side.
 * 
 * @author graj
 * 
 */
public class JBIAdminCommandsClientImpl implements Serializable,
        JBIAdminCommands {
    
    private static String                        CANNED_RESPONSE  = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                                                          + "<jbi-task version=\"1.0\" xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">"
                                                                          + "<jbi-task-result><frmwk-task-result>"
                                                                          + "<frmwk-task-result-details>"
                                                                          + "<task-result-details>"
                                                                          + "<task-id>OPERATION</task-id>"
                                                                          + "<task-result>SUCCESS</task-result>"
                                                                          + "<message-type>INFO</message-type>"
                                                                          + "</task-result-details>"
                                                                          + "<locale>en</locale>"
                                                                          + "</frmwk-task-result-details>"
                                                                          + "</frmwk-task-result></jbi-task-result>"
                                                                          + "</jbi-task>";
    
    /** i18n */
    private static I18NBundle                    sI18NBundle      = null;
    
    /** remote MBeanServer connection */
    transient MBeanServerConnection              remoteConnection;
    
    transient ManagementClient                   client;
    
    /** is this a local or remote connection */
    boolean                                      isRemoteConnection;
    
    /** last uploaded object reference */
    protected Object                             mLastUploadId    = null;
    
    protected transient AdministrationService    administrationService;
    
    protected transient ConfigurationService     configurationService;
    
    protected transient DeploymentService        deploymentService;
    
    protected transient InstallationService      installationService;
    
    protected transient RuntimeManagementService runtimeManagementService;
    
    protected transient PerformanceMeasurementService performanceMeasurementService;
    
    /**
     * Determines if a de-serialized file is compatible with this class.
     * 
     * 1. Run serialver -show from the command line 2. Point the tool to the
     * class file including the package, for example:
     * com.sun.jbi.ui.client.JBIAdminCommandsClientImpl - without the .class The
     * serialver docs for Windows OS is at:
     * http://java.sun.com/j2se/1.5.0/docs/tooldocs/windows/serialver.html and
     * Unix OS is at:
     * http://java.sun.com/j2se/1.5.0/docs/tooldocs/solaris/serialver.html
     * 
     * Maintainers must change this value if and only if the new version of this
     * class is not compatible with old versions. See Sun docs for <a
     * href=http://java.sun.com/products/jdk/1.1/docs/guide
     * /serialization/spec/version.doc.html> details. </a>
     */
    static final long                            serialVersionUID = 22798309571083089L;
    
    /** Constructor */
    public JBIAdminCommandsClientImpl() {
        this(null, false);
    }
    
    /** Constructor */
    public JBIAdminCommandsClientImpl(MBeanServerConnection serverConnection) {
        this(serverConnection, false);
    }
    
    /** Constructor */
    public JBIAdminCommandsClientImpl(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        this.client = new ManagementClient(serverConnection, isRemoteConnection);
    }
    
    /**
     * @return the administrationService
     * @throws JBIRemoteException
     */
    protected AdministrationService getAdministrationService()
            throws JBIRemoteException {
        if (this.administrationService == null) {
            this.administrationService = this.client.getAdministrationService();
        }
        return this.administrationService;
    }
    
    /**
     * @return the configurationService
     * @throws JBIRemoteException
     */
    protected ConfigurationService getConfigurationService()
            throws JBIRemoteException {
        if (this.configurationService == null) {
            this.configurationService = this.client.getConfigurationService();
        }
        return this.configurationService;
    }
    
    /**
     * @return the deploymentService
     * @throws JBIRemoteException
     */
    protected DeploymentService getDeploymentService()
            throws JBIRemoteException {
        if (this.deploymentService == null) {
            this.deploymentService = this.client.getDeploymentService();
        }
        return this.deploymentService;
    }
    
    /**
     * @return the installationService
     * @throws JBIRemoteException
     */
    protected InstallationService getInstallationService()
            throws JBIRemoteException {
        if (this.installationService == null) {
            this.installationService = this.client.getInstallationService();
        }
        return this.installationService;
    }
    
    /**
     * @return the runtimeManagementService
     * @throws JBIRemoteException
     */
    protected RuntimeManagementService getRuntimeManagementService()
            throws JBIRemoteException {
        if (this.runtimeManagementService == null) {
            this.runtimeManagementService = this.client
                    .getRuntimeManagementService();
        }
        return this.runtimeManagementService;
    }
    
    /**
     * @return the performanceMeasurementService
     * @throws JBIRemoteException
     */
    protected PerformanceMeasurementService getPerformanceMeasurementService()
            throws JBIRemoteException {
        if (this.performanceMeasurementService == null) {
            this.performanceMeasurementService = this.client
                    .getPerformanceMeasurementService();
        }
        return this.performanceMeasurementService;
    }
    
    /**
     * is this a local or remote connection
     * 
     * @return true if remote, false if local
     */
    boolean isRemoteConnection() {
        // return this.isRemoteConnection;
        return this.client.isRemoteConnection();
    }
    
    /**
     * deploys service assembly
     * 
     * @return result as a management message xml text
     * @param zipFilePath
     *            file path
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#deployServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String deployServiceAssembly(String zipFilePath, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.deployServiceAssembly(zipFilePath,
                targetName);
        return resultObject;
    }
    
    /**
     * Retrieve the Service Assembly Name
     * 
     * @param zipFilePath
     * @return the name of the Service Assembly or null
     */
    String validateServiceAssembly(String zipFilePath) {
        JBIArchive archive = null;
        ServiceAssemblyDD descriptor = null;
        String name = null;
        
        try {
            archive = new JBIArchive(zipFilePath);
            if (archive.isServiceAssemblyArchive() == true) {
                descriptor = (ServiceAssemblyDD) archive.getJbiDescriptor();
                name = descriptor.getName();
            }
        } catch (IOException ioException) {
        } catch (Exception exception) {
        }
        
        return name;
    }
    
    /**
     * deploys service assembly
     * 
     * @return result as a management message xml text
     * @param zipFilePath
     *            file path
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#deployServiceAssembly(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> deployServiceAssembly(String zipFilePath,
            String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.deployServiceAssembly(zipFilePath,
                targetNames);
        return resultObject;
    }
    
    /**
     * Retrieve the Component Installation descriptor for the server target.
     * 
     * The Installation descriptor will never be different for a component name
     * in a JBI system because there's only one component allowed with that same
     * name. Therefore, this implies a registry lookup for the server target.
     * 
     * @param Component
     *            name as a string
     * @return the Component Installation descriptor as a string
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#getComponentInstallationDescriptor(java.lang.String)
     */
    public String getComponentInstallationDescriptor(String componentName)
            throws JBIRemoteException {
        String resultObject = null;
        AdministrationService administrationService = getAdministrationService();
        resultObject = administrationService
                .getComponentInstallationDescriptor(componentName);
        return resultObject;
    }
    
    /**
     * Retrieve the Service Assembly Deployment descriptor for the domain
     * target.
     * 
     * The deployment descriptor will never be different for a service assembly
     * name in a JBI system because there's only one Service Assembly allowed
     * with that same name. Therefore, this implies a registry lookup for the
     * domain target.
     * 
     * @param service
     *            assembly name as a string
     * @return the service assembly deployment descriptor as a string
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#getServiceAssemblyDeploymentDescriptor(java.lang.String)
     */
    public String getServiceAssemblyDeploymentDescriptor(
            String serviceAssemblyName) throws JBIRemoteException {
        String resultObject = null;
        AdministrationService administrationService = getAdministrationService();
        resultObject = administrationService
                .getServiceAssemblyDeploymentDescriptor(serviceAssemblyName);
        return resultObject;
    }
    
    /**
     * Retrieve the Shared Library Installation descriptor for the server
     * target.
     * 
     * The Installation descriptor will never be different for a Shared Library
     * name in a JBI system because there's only one Shared Library allowed with
     * that same name. Therefore, this implies a registry lookup for the server
     * target.
     * 
     * @param Shared
     *            Library name as a string
     * @return the Shared Library Installation descriptor as a string
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#getSharedLibraryInstallationDescriptor(java.lang.String)
     */
    public String getSharedLibraryInstallationDescriptor(
            String sharedLibraryName) throws JBIRemoteException {
        String resultObject = null;
        AdministrationService administrationService = getAdministrationService();
        resultObject = administrationService
                .getSharedLibraryInstallationDescriptor(sharedLibraryName);
        return resultObject;
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @return name of the component.
     * @param paramProps
     *            Properties object contains name/value pair.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#installComponent(java.lang.String,
     *      java.util.Properties, java.lang.String)
     */
    public String installComponent(String zipFilePath, Properties paramProps,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installComponent(zipFilePath,
                paramProps, targetName);
        return resultObject;
    }
    
    /**
     * installs component (service engine, binding component)
     * 
     * @return name of the component.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#installComponent(java.lang.String,
     *      java.lang.String)
     */
    public String installComponent(String zipFilePath, String targetName)
            throws JBIRemoteException {
        return this.installComponent(zipFilePath, new Properties(), targetName);
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @return name of the component.
     * @param paramProps
     *            Properties object contains name/value pair.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#installComponent(java.lang.String,
     *      java.util.Properties, java.lang.String[])
     */
    public Map<String, String> installComponent(String zipFilePath,
            Properties paramProps, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installComponent(zipFilePath,
                paramProps, targetNames);
        return resultObject;
    }
    
    /**
     * installs component (service engine, binding component)
     * 
     * @return name of the component.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#installComponent(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> installComponent(String zipFilePath,
            String[] targetNames) throws JBIRemoteException {
        
        return this
                .installComponent(zipFilePath, new Properties(), targetNames);
    }
    
    /**
     * installs shared library
     * 
     * @return shared library name.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#installSharedLibrary(java.lang.String,
     *      java.lang.String)
     */
    public String installSharedLibrary(String zipFilePath, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installSharedLibrary(zipFilePath,
                targetName);
        return resultObject;
    }
    
    /**
     * installs shared library
     * 
     * @return shared library name.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#installSharedLibrary(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> installSharedLibrary(String zipFilePath,
            String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installSharedLibrary(zipFilePath,
                targetNames);
        return resultObject;
    }
    
    /**
     * Test whether the JBI Runtime is enabled.
     * 
     * @return true when the mbean is registered, false otherwise
     * @throws JBIRemoteException
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#isJBIRuntimeEnabled()
     */
    public boolean isJBIRuntimeEnabled() throws JBIRemoteException {
        
        boolean resultObject = false;
        AdministrationService administrationService = getAdministrationService();
        resultObject = administrationService.isJBIRuntimeEnabled();
        return resultObject;
    }
    
    /**
     * return component info xml text that has only binding component infos.
     * 
     * @return the component info xml text.
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listBindingComponents(java.lang.String)
     */
    public String listBindingComponents(String targetName)
            throws JBIRemoteException {
        return listBindingComponents(null, null, null, targetName);
    }
    
    /**
     * return component info xml text that has only binding component infos
     * which satisfies the options passed to the method.
     * 
     * @param state
     *            return all the binding components that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the binding components that have a dependency on
     *            the specified shared library. null value to ignore this
     *            option.
     * @param serviceAssemblyName
     *            return all the binding components that have the specified
     *            service assembly deployed on them. null value to ignore this
     *            option.
     * @param targetName
     * @return xml text contain the list of binding component infos
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listBindingComponents(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String listBindingComponents(String state, String sharedLibraryName,
            String serviceAssemblyName, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listBindingComponents(state,
                sharedLibraryName, serviceAssemblyName, targetName);
        return resultObject;
    }
    
    /**
     * return component info xml text that has only binding component infos.
     * 
     * @return the component info xml text.
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listBindingComponents(java.lang.String[])
     */
    public Map<String, String> listBindingComponents(String[] targetNames)
            throws JBIRemoteException {
        return this.listBindingComponents(null, null, null, targetNames);
    }
    
    /**
     * return component info xml text that has only binding component infos
     * which satisfies the options passed to the method.
     * 
     * @param state
     *            return all the binding components that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the binding components that have a dependency on
     *            the specified shared library. null value to ignore this
     *            option.
     * @param serviceAssemblyName
     *            return all the binding components that have the specified
     *            service assembly deployed on them. null value to ignore this
     *            option.
     * @param targetNames
     * @return xml text contain the list of binding component infos
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listBindingComponents(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String[])
     */
    public Map<String, String> listBindingComponents(String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listBindingComponents(state,
                sharedLibraryName, serviceAssemblyName, targetNames);
        return resultObject;
    }
    
    /**
     * returns a list of Service Assembly Infos in a xml format.
     * 
     * @param targetName
     * @return xml text containing the Service Assembly infos
     * @throws JBIRemoteException
     *             on error
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceAssemblies(java.lang.String)
     */
    public String listServiceAssemblies(String targetName)
            throws JBIRemoteException {
        return listServiceAssemblies(null, null, targetName);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetName
     * @return xml string contain the list of service assembly infos
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceAssemblies(java.lang.String,
     *      java.lang.String)
     */
    public String listServiceAssemblies(String componentName, String targetName)
            throws JBIRemoteException {
        return listServiceAssemblies(null, componentName, targetName);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param state
     *            to return all the service assemblies that are in the specified
     *            state. JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or
     *            null for ANY state
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetName
     * @return xml string contain the list of service assembly infos
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceAssemblies(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String listServiceAssemblies(String state, String componentName,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listServiceAssemblies(state,
                componentName, targetName);
        return resultObject;
    }
    
    /**
     * returns a list of Service Assembly Infos in a xml format.
     * 
     * @param targetNames
     * @return xml text containing the Service Assembly infos
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceAssemblies(java.lang.String[])
     */
    public Map<String, String> listServiceAssemblies(String[] targetNames)
            throws JBIRemoteException {
        return listServiceAssemblies(null, null, targetNames);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetNames
     * @return xml string contain the list of service assembly infos
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceAssemblies(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> listServiceAssemblies(String componentName,
            String[] targetNames) throws JBIRemoteException {
        return listServiceAssemblies(null, componentName, targetNames);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param state
     *            to return all the service assemblies that are in the specified
     *            state. JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or
     *            null for ANY state
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetNames
     * @return xml string contain the list of service assembly infos
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceAssemblies(java.lang.String,
     *      java.lang.String, java.lang.String[])
     */
    public Map<String, String> listServiceAssemblies(String state,
            String componentName, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listServiceAssemblies(state,
                componentName, targetNames);
        return resultObject;
    }
    
    /**
     * return component info xml text that has only service engine infos.
     * 
     * @param targetName
     * @return the component info xml text.
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceEngines(java.lang.String)
     */
    public String listServiceEngines(String targetName)
            throws JBIRemoteException {
        return this.listServiceEngines(null, null, null, targetName);
    }
    
    /**
     * return component info xml text that has only service engine infos which
     * satisfies the options passed to the method.
     * 
     * @param state
     *            return all the service engines that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the service engines that have a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return all the service engines that have the specified service
     *            assembly deployed on them. null value to ignore this option.
     * @param targetName
     * @return xml text contain the list of service engine component infos
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceEngines(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String listServiceEngines(String state, String sharedLibraryName,
            String serviceAssemblyName, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listServiceEngines(state,
                sharedLibraryName, serviceAssemblyName, targetName);
        return resultObject;
    }
    
    /**
     * return component info xml text that has only service engine infos.
     * 
     * @param targetNames
     * @return the component info xml text.
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceEngines(java.lang.String[])
     */
    public Map<String, String> listServiceEngines(String[] targetNames)
            throws JBIRemoteException {
        return this.listServiceEngines(null, null, null, targetNames);
    }
    
    /**
     * return component info xml text that has only service engine infos which
     * satisfies the options passed to the method.
     * 
     * @param state
     *            return all the service engines that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the service engines that have a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return all the service engines that have the specified service
     *            assembly deployed on them. null value to ignore this option.
     * @param targetNames
     * @return xml text contain the list of service engine component infos
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listServiceEngines(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String[])
     */
    public Map<String, String> listServiceEngines(String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listServiceEngines(state,
                sharedLibraryName, serviceAssemblyName, targetNames);
        return resultObject;
    }
    
    /**
     * return component info xml text that has only shared library infos.
     * 
     * @param targetName
     * @return the component info xml text.
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listSharedLibraries(java.lang.String)
     */
    public String listSharedLibraries(String targetName)
            throws JBIRemoteException {
        return this.listSharedLibraries(null, targetName);
    }
    
    /**
     * returns the list of Shared Library infos in the in a xml format
     * 
     * @param componentName
     *            to return only the shared libraries that are this component
     *            dependents. null for listing all the shared libraries in the
     *            system.
     * @param targetName
     * @return xml string contain the list of componentinfos for shared
     *         libraries.
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listSharedLibraries(java.lang.String,
     *      java.lang.String)
     */
    public String listSharedLibraries(String componentName, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listSharedLibraries(
                componentName, targetName);
        return resultObject;
    }
    
    /**
     * return component info xml text that has only shared library infos.
     * 
     * @param targetNames
     * @return the component info xml text.
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listSharedLibraries(java.lang.String[])
     */
    public Map<String, String> listSharedLibraries(String[] targetNames)
            throws JBIRemoteException {
        return this.listSharedLibraries(null, targetNames);
    }
    
    /**
     * returns the list of Shared Library infos in the in a xml format
     * 
     * @param componentName
     *            to return only the shared libraries that are this component
     *            dependents. null for listing all the shared libraries in the
     *            system.
     * @param targetNames
     * @return xml string contain the list of componentinfos for shared
     *         libraries.
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listSharedLibraries(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> listSharedLibraries(String componentName,
            String[] targetNames) throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listSharedLibraries(
                componentName, targetNames);
        return resultObject;
    }
    
    /**
     * returns a list of Binding Component and Service Engine infos in xml
     * format, that are dependent upon a specified Shared Library
     * 
     * @param sharedLibraryName
     *            the shared library name
     * @param targetName
     * @return xml string containing the list of componentInfos
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listSharedLibraryDependents(java.lang.String,
     *      java.lang.String)
     */
    public String listSharedLibraryDependents(String sharedLibraryName,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listSharedLibraryDependents(
                sharedLibraryName, targetName);
        return resultObject;
        
    }
    
    /**
     * returns a list of Binding Component and Service Engine infos in xml
     * format, that are dependent upon a specified Shared Library
     * 
     * @param sharedLibraryName
     *            the shared library name
     * @param targetNames
     * @return xml string containing the list of componentInfos
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#listSharedLibraryDependents(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> listSharedLibraryDependents(
            String sharedLibraryName, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.listSharedLibraryDependents(
                sharedLibraryName, targetNames);
        return resultObject;
    }
    
    /**
     * return component info xml text for the specified binding component if
     * exists. If no binding component with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the binding component to lookup
     * @param state
     *            return the binding component that is in the specified state.
     *            valid states are JBIComponentInfo.STARTED, STOPPED, INSTALLED
     *            or null for ANY state
     * @param sharedLibraryName
     *            return the binding component that has a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return the binding component that has the specified service
     *            assembly deployed on it. null value to ignore this option.
     * @param targetName
     * @return xml text contain the binding component info that confirms to the
     *         component info list xml grammer.
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#showBindingComponent(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String)
     */
    public String showBindingComponent(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.showBindingComponent(name,
                state, sharedLibraryName, serviceAssemblyName, targetName);
        return resultObject;
    }
    
    /**
     * return component info xml text for the specified binding component if
     * exists. If no binding component with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the binding component to lookup
     * @param state
     *            return the binding component that is in the specified state.
     *            valid states are JBIComponentInfo.STARTED, STOPPED, INSTALLED
     *            or null for ANY state
     * @param sharedLibraryName
     *            return the binding component that has a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return the binding component that has the specified service
     *            assembly deployed on it. null value to ignore this option.
     * @param targetNames
     * @return xml text contain the binding component info that confirms to the
     *         component info list xml grammer.
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#showBindingComponent(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> showBindingComponent(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.showBindingComponent(name,
                state, sharedLibraryName, serviceAssemblyName, targetNames);
        return resultObject;
    }
    
    /**
     * return service assembly info xml text for the specified service assembly
     * if exists. If no service assembly with that name exists, it returns the
     * xml with empty list.
     * 
     * @param name
     *            name of the service assembly to lookup
     * @param state
     *            return the service assembly that is in the specified state.
     *            JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or null for
     *            ANY state
     * @param componentName
     *            return the service assembly that has service units on this
     *            component.
     * @param targetName
     * @return xml string contain service assembly info that confirms to the
     *         service assembly list xml grammer.
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#showServiceAssembly(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String showServiceAssembly(String name, String state,
            String componentName, String targetName) throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.showServiceAssembly(name,
                state, componentName, targetName);
        return resultObject;
    }
    
    /**
     * return service assembly info xml text for the specified service assembly
     * if exists. If no service assembly with that name exists, it returns the
     * xml with empty list.
     * 
     * @param name
     *            name of the service assembly to lookup
     * @param state
     *            return the service assembly that is in the specified state.
     *            JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or null for
     *            ANY state
     * @param componentName
     *            return the service assembly that has service units on this
     *            component.
     * @param targetNames
     * @return xml string contain service assembly info that confirms to the
     *         service assembly list xml grammer.
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#showServiceAssembly(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String[])
     */
    public Map<String, String> showServiceAssembly(String name, String state,
            String componentName, String[] targetNames)
            throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.showServiceAssembly(name,
                state, componentName, targetNames);
        return resultObject;
    }
    
    /**
     * return component info xml text for the specified service engine if
     * exists. If no service engine with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the service engine to lookup
     * @param state
     *            to return all the service engines that are in the specified
     *            state. JBIComponentInfo.STARTED, STOPPED, INSTALLED or null
     *            for ANY state
     * @param sharedLibraryName
     *            to return all the service engines that have a dependency on
     *            the specified shared library. Could be null for not filtering
     *            the service engines for this dependency.
     * @param serviceAssemblyName
     *            to return all the service engines that have the specified
     *            service assembly deployed on them. Could be null for not
     *            filtering the service engines for this dependency.
     * @param targetName
     * @return xml string contain service engine component info
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#showServiceEngine(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String)
     */
    public String showServiceEngine(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws JBIRemoteException {
        
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.showServiceEngine(name, state,
                sharedLibraryName, serviceAssemblyName, targetName);
        return resultObject;
    }
    
    /**
     * return component info xml text for the specified service engine if
     * exists. If no service engine with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the service engine to lookup
     * @param state
     *            to return all the service engines that are in the specified
     *            state. JBIComponentInfo.STARTED, STOPPED, INSTALLED or null
     *            for ANY state
     * @param sharedLibraryName
     *            to return all the service engines that have a dependency on
     *            the specified shared library. Could be null for not filtering
     *            the service engines for this dependency.
     * @param serviceAssemblyName
     *            to return all the service engines that have the specified
     *            service assembly deployed on them. Could be null for not
     *            filtering the service engines for this dependency.
     * @param targetNames
     * @return xml string contain service engine component info
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#showServiceEngine(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> showServiceEngine(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.showServiceEngine(name, state,
                sharedLibraryName, serviceAssemblyName, targetNames);
        return resultObject;
    }
    
    /**
     * return component info xml text for the specified shared library if
     * exists. If no shared library with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the shared library to lookup
     * @param componentName
     *            return the shared library that is this component dependents.
     *            null to ignore this option.
     * @param targetName
     * @return xml string contain shared library component info that confirms to
     *         the component info list xml grammer.
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#showSharedLibrary(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String showSharedLibrary(String name, String componentName,
            String targetName) throws JBIRemoteException {
        
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.showSharedLibrary(name,
                componentName, targetName);
        return resultObject;
    }
    
    /**
     * return component info xml text for the specified shared library if
     * exists. If no shared library with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the shared library to lookup
     * @param componentName
     *            return the shared library that is this component dependents.
     *            null to ignore this option.
     * @param targetNames
     * @return xml string contain shared library component info that confirms to
     *         the component info list xml grammer.
     * @throws JBIRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#showSharedLibrary(java.lang.String,
     *      java.lang.String, java.lang.String[])
     */
    public Map<String, String> showSharedLibrary(String name,
            String componentName, String[] targetNames)
            throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.showSharedLibrary(name,
                componentName, targetNames);
        return resultObject;
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#shutdownComponent(java.lang.String,
     *      java.lang.String)
     */
    public String shutdownComponent(String componentName, String targetName)
            throws JBIRemoteException {
        boolean force = false;
        return this.shutdownComponent(componentName, force, targetName);
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param force
     *            true if component should be shutdown in any case, else false.
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#shutdownComponent(java.lang.String,
     *      boolean, java.lang.String)
     */
    public String shutdownComponent(String componentName, boolean force,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.shutdownComponent(
                componentName, force, targetName);
        return resultObject;
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#shutdownComponent(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> shutdownComponent(String componentName,
            String[] targetNames) throws JBIRemoteException {
        boolean force = false;
        return this.shutdownComponent(componentName, force, targetNames);
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param force
     *            true if component should be shutdown in any case, else false.
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#shutdownComponent(java.lang.String,
     *      boolean, java.lang.String[])
     */
    public Map<String, String> shutdownComponent(String componentName,
            boolean force, String[] targetNames) throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.shutdownComponent(
                componentName, force, targetNames);
        return resultObject;
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#shutdownServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String shutdownServiceAssembly(String serviceAssemblyName,
            String targetName) throws JBIRemoteException {
        
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.shutdownServiceAssembly(
                serviceAssemblyName, targetName);
        return resultObject;
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceShutdown
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws JBIRemoteException
     *             on error
     */
    public String shutdownServiceAssembly(String serviceAssemblyName,
            boolean forceShutdown, String targetName) throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.shutdownServiceAssembly(
                serviceAssemblyName, forceShutdown, targetName);
        return resultObject;
        
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceShutdown
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> shutdownServiceAssembly(
            String serviceAssemblyName, boolean forceShutdown,
            String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.shutdownServiceAssembly(
                serviceAssemblyName, forceShutdown, targetNames);
        return resultObject;
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#shutdownServiceAssembly(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> shutdownServiceAssembly(
            String serviceAssemblyName, String[] targetNames)
            throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.shutdownServiceAssembly(
                serviceAssemblyName, targetNames);
        return resultObject;
    }
    
    /**
     * starts component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @throws JBIRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#startComponent(java.lang.String,
     *      java.lang.String)
     */
    public String startComponent(String componentName, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.startComponent(componentName,
                targetName);
        return resultObject;
    }
    
    /**
     * @see com.sun.jbi.ui.common.JBIAdminCommands#startComponent(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> startComponent(String componentName,
            String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.startComponent(componentName,
                targetNames);
        return resultObject;
    }
    
    /**
     * starts service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#startServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String startServiceAssembly(String serviceAssemblyName,
            String targetName) throws JBIRemoteException {
        
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.startServiceAssembly(
                serviceAssemblyName, targetName);
        return resultObject;
    }
    
    /**
     * starts service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#startServiceAssembly(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> startServiceAssembly(String serviceAssemblyName,
            String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.startServiceAssembly(
                serviceAssemblyName, targetNames);
        return resultObject;
    }
    
    /**
     * stops component (service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#stopComponent(java.lang.String,
     *      java.lang.String)
     */
    public String stopComponent(String componentName, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.stopComponent(componentName,
                targetName);
        return resultObject;
    }
    
    /**
     * stops component (service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#stopComponent(java.lang.String,
     *      java.lang.String)
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#stopComponent(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> stopComponent(String componentName,
            String[] targetNames) throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.stopComponent(componentName,
                targetNames);
        return resultObject;
    }
    
    /**
     * stops service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#stopServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String stopServiceAssembly(String serviceAssemblyName,
            String targetName) throws JBIRemoteException {
        
        String resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.stopServiceAssembly(
                serviceAssemblyName, targetName);
        return resultObject;
    }
    
    /**
     * stops service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#stopServiceAssembly(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> stopServiceAssembly(String serviceAssemblyName,
            String[] targetNames) throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        RuntimeManagementService runtimeManagementService = getRuntimeManagementService();
        resultObject = runtimeManagementService.stopServiceAssembly(
                serviceAssemblyName, targetNames);
        return resultObject;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * 
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#undeployServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.undeployServiceAssembly(
                serviceAssemblyName, targetName);
        return resultObject;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * 
     * @return result as a management message xml text
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#undeployServiceAssembly(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> undeployServiceAssembly(
            String serviceAssemblyName, String[] targetNames)
            throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.undeployServiceAssembly(
                serviceAssemblyName, targetNames);
        return resultObject;
    }
    
    /**
     * uninstalls component (service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     * @return name of the component.
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#uninstallComponent(java.lang.String,
     *      java.lang.String)
     */
    public String uninstallComponent(String componentName, String targetName)
            throws JBIRemoteException {
        
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallComponent(componentName,
                targetName);
        return resultObject;
    }
    
    /**
     * uninstalls component (service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @return name of the component.
     * @throws JBIRemoteException
     *             on error
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#uninstallComponent(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> uninstallComponent(String componentName,
            String[] targetNames) throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallComponent(componentName,
                targetNames);
        return resultObject;
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param targetName
     * @throws JBIRemoteException
     *             on error
     * @return shared library name.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#uninstallSharedLibrary(java.lang.String,
     *      java.lang.String)
     */
    public String uninstallSharedLibrary(String sharedLibraryName,
            String targetName) throws JBIRemoteException {
        
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallSharedLibrary(
                sharedLibraryName, targetName);
        return resultObject;
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param targetNames
     * @throws JBIRemoteException
     *             on error
     * @return shared library name.
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#uninstallSharedLibrary(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> uninstallSharedLibrary(String sharedLibraryName,
            String[] targetNames) throws JBIRemoteException {
        
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallSharedLibrary(
                sharedLibraryName, targetNames);
        return resultObject;
    }
    
    /**
     * Returns the jbi.xml Deployment Descriptor for a Service Unit.
     * 
     * @param the
     *            name of the Service Assembly
     * @param the
     *            name of the Service Unit
     * @return the jbi.xml deployment descriptor of the archive
     * @throws JBIRemoteException
     *             if error or exception occurs.
     */
    public String getServiceUnitDeploymentDescriptor(
            String serviceAssemblyName, String serviceUnitName)
            throws JBIRemoteException {
        String resultObject = null;
        AdministrationService administrationService = getAdministrationService();
        resultObject = administrationService
                .getServiceUnitDeploymentDescriptor(serviceAssemblyName,
                        serviceUnitName);
        return resultObject;
        
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component.
     * @param targetName
     *            name of the target for this operation
     * @return result.
     * @throws JBIRemoteException
     *             on error
     */
    public String installComponentFromDomain(String componentName,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installComponentFromDomain(
                componentName, targetName);
        return resultObject;
        
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component.
     * @param component
     *            configuration properties
     * @param targetName
     *            name of the target for this operation
     * @return result.
     * @throws JBIRemoteException
     *             on error
     */
    public String installComponentFromDomain(String componentName,
            Properties properties, String targetName) throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installComponentFromDomain(
                componentName, properties, targetName);
        return resultObject;
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and component name strings.
     * @throws JBIRemoteException
     *             on error
     */
    
    public String uninstallComponent(String componentName, boolean forceDelete,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallComponent(componentName,
                targetName);
        return resultObject;
    }
    
    /**
     * installs shared library
     * 
     * @param libraryName
     *            Shared Library Name
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws JBIRemoteException
     *             on error
     */
    public String installSharedLibraryFromDomain(String libraryName,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installSharedLibraryFromDomain(
                libraryName, targetName);
        return resultObject;
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws JBIRemoteException
     *             on error
     */
    public String uninstallSharedLibrary(String sharedLibraryName,
            boolean forceDelete, String targetName) throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallSharedLibrary(
                sharedLibraryName, forceDelete, targetName);
        return resultObject;
    }
    
    /**
     * deploys service assembly
     * 
     * @param assemblyName
     *            service assembly name
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and management message xml text strings.
     * @throws JBIRemoteException
     *             on error
     */
    public String deployServiceAssemblyFromDomain(String assemblyName,
            String targetName) throws JBIRemoteException {
        String resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.deployServiceAssemblyFromDomain(
                assemblyName, targetName);
        return resultObject;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws JBIRemoteException
     *             on error
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            boolean forceDelete, String targetName) throws JBIRemoteException {
        String resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.undeployServiceAssembly(
                serviceAssemblyName, forceDelete, targetName);
        return resultObject;
    }
    
    /**
     * installs component from domain ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and component name strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> installComponentFromDomain(
            String componentName, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installComponentFromDomain(
                componentName, targetNames);
        return resultObject;
    }
    
    /**
     * installs component from domain ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param component
     *            configuration properties
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and component name strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> installComponentFromDomain(
            String componentName, Properties properties, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installComponentFromDomain(
                componentName, properties, targetNames);
        return resultObject;
    }
    
    /**
     * Forcefully uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and component name strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> uninstallComponent(
            String componentName, boolean forceDelete, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallComponent(componentName,
                forceDelete, targetNames);
        return resultObject;
    }
    
    /**
     * installs shared library from domain
     * 
     * @param libraryName
     *            name of the library
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> installSharedLibraryFromDomain(
            String libraryName, String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.installSharedLibraryFromDomain(
                libraryName, targetNames);
        return resultObject;
    }
    
    /**
     * Forcefully uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> uninstallSharedLibrary(
            String sharedLibraryName, boolean forceDelete, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallSharedLibrary(
                sharedLibraryName, forceDelete, targetNames);
        return resultObject;
    }
    
    /**
     * deploys service assembly from domain target
     * 
     * @param assemblyName
     *            name of the service assembly
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and management message xml text strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> deployServiceAssemblyFromDomain(
            String assemblyName, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.deployServiceAssemblyFromDomain(
                assemblyName, targetNames);
        return resultObject;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> undeployServiceAssembly(
            String serviceAssemblyName, boolean forceDelete,
            String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.undeployServiceAssembly(
                serviceAssemblyName, forceDelete, targetNames);
        return resultObject;
    }
    
    // //////////////////////////////////
    // Retain in Domain
    // //////////////////////////////////
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return component name string.
     * @throws JBIRemoteException
     *             on error
     */
    
    public String uninstallComponent(String componentName, boolean forceDelete,
            boolean retainInDomain, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallComponent(componentName,
                forceDelete, retainInDomain, targetName);
        return resultObject;
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return shared library name string.
     * @throws JBIRemoteException
     *             on error
     */
    public String uninstallSharedLibrary(String sharedLibraryName,
            boolean forceDelete, boolean retainInDomain, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallSharedLibrary(
                sharedLibraryName, forceDelete, retainInDomain, targetName);
        return resultObject;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return result as a management message xml text string.
     * @throws JBIRemoteException
     *             on error
     */
    public String undeployServiceAssembly(String serviceAssemblyName,
            boolean forceDelete, boolean retainInDomain, String targetName)
            throws JBIRemoteException {
        String resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.undeployServiceAssembly(
                serviceAssemblyName, forceDelete, retainInDomain, targetName);
        return resultObject;
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and component name strings.
     * @throws JBIRemoteException
     *             on error
     */
    
    public Map<String /* targetName */, String /* targetResult */> uninstallComponent(
            String componentName, boolean forceDelete, boolean retainInDomain,
            String[] targetNames) throws JBIRemoteException {
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallComponent(componentName,
                forceDelete, retainInDomain, targetNames);
        return resultObject;
        
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and shared library name strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> uninstallSharedLibrary(
            String sharedLibraryName, boolean forceDelete,
            boolean retainInDomain, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.uninstallSharedLibrary(
                sharedLibraryName, forceDelete, retainInDomain, targetNames);
        return resultObject;
    }
    
    /**
     * undeploys service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceDelete
     *            forces deletion of the assembly if true, false if not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> undeployServiceAssembly(
            String serviceAssemblyName, boolean forceDelete,
            boolean retainInDomain, String[] targetNames)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        DeploymentService deploymentService = getDeploymentService();
        resultObject = deploymentService.undeployServiceAssembly(
                serviceAssemblyName, forceDelete, retainInDomain, targetNames);
        return resultObject;
    }
    
    /**
     * Gets the component custom loggers and their levels
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* loggerCustomName */, Level /* logLevel */> getComponentLoggerLevels(
            String componentName, String targetName, String targetInstanceName)
            throws JBIRemoteException {
        Map<String, Level> resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getComponentLoggerLevels(
                componentName, targetName, targetInstanceName);
        return resultObject;
    }
    
    /**
     * Gets the component custom loggers and their display names.
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their display names
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* loggerCustomName */, String /* displayName */> getComponentLoggerDisplayNames(
            String componentName, String targetName, String targetInstanceName)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getComponentLoggerDisplayNames(
                componentName, targetName, targetInstanceName);
        return resultObject;
    }
    
    /**
     * Sets the component log level for a given custom logger
     * 
     * @param componentName
     *            name of the component
     * @param loggerCustomName
     *            the custom logger name
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @throws JBIRemoteException
     *             on error
     */
    public void setComponentLoggerLevel(String componentName,
            String loggerCustomName, Level logLevel, String targetName,
            String targetInstanceName) throws JBIRemoteException {
        ConfigurationService configurationService = getConfigurationService();
        configurationService.setComponentLoggerLevel(componentName,
                loggerCustomName, logLevel, targetName, targetInstanceName);
    }
    
    /**
     * Gets all the runtime loggers and their levels
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws JBIRemoteException
     *             on error
     * @deprecated
     */
    public Map<String /* runtimeLoggerName */, Level /* logLevel */> getRuntimeLoggerLevels(
            String targetName, String targetInstanceName)
            throws JBIRemoteException {
        Map<String, Level> resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getRuntimeLoggerLevels(targetName,
                targetInstanceName);
        return resultObject;
    }
    
    /**
     * Lookup the level of one runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger (e.g. com.sun.jbi.framework
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws JBIRemoteException
     *             on error
     * @deprecated
     */
    public Level getRuntimeLoggerLevel(String runtimeLoggerName,
            String targetName, String targetInstanceName)
            throws JBIRemoteException {
        Level resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getRuntimeLoggerLevel(
                runtimeLoggerName, targetName, targetInstanceName);
        return resultObject;
    }
    
    /**
     * Sets the log level for a given runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @throws JBIRemoteException
     *             on error
     * @deprecated
     */
    public void setRuntimeLoggerLevel(String runtimeLoggerName, Level logLevel,
            String targetName, String targetInstanceName)
            throws JBIRemoteException {
        ConfigurationService configurationService = getConfigurationService();
        configurationService.setRuntimeLoggerLevel(runtimeLoggerName, logLevel,
                targetName, targetInstanceName);
    }
    
    /**
     * Gets all the runtime loggers and their display names
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of display names to their logger names
     * @throws JBIRemoteException
     *             on error
     * @deprecated
     */
    public Map<String /* display name */, String /* logger name */> getRuntimeLoggerNames(
            String targetName, String targetInstanceName)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getRuntimeLoggerNames(targetName,
                targetInstanceName);
        return resultObject;
    }
    
    /**
     * Return the display name for a runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the logger (e.g. com.sun.jbi.framework)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return the display name for the given logger
     * @throws JBIRemoteException
     *             on error
     * @deprecated
     */
    public String getRuntimeLoggerDisplayName(String runtimeLoggerName,
            String targetName, String targetInstanceName)
            throws JBIRemoteException {
        String resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getRuntimeLoggerDisplayName(
                runtimeLoggerName, targetName, targetInstanceName);
        return resultObject;
    }
    
        
    /**
     * Gets all the runtime loggers and their levels
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* runtimeLoggerName */, Level /* logLevel */> getRuntimeLoggerLevels(
            String targetName)
            throws JBIRemoteException {
        Map<String, Level> resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getRuntimeLoggerLevels(targetName);
        return resultObject;
    }

    /**
     * Lookup the level of one runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger (e.g.  com.sun.jbi.framework
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws JBIRemoteException
     *             on error
     */
    public Level getRuntimeLoggerLevel(
            String runtimeLoggerName,
            String targetName)
            throws JBIRemoteException {
        Level resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getRuntimeLoggerLevel(runtimeLoggerName, targetName);

        return resultObject;
    }

    /**
     * Sets the log level for a given runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the runtime logger
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @throws JBIRemoteException
     *             on error
     */
    public void setRuntimeLoggerLevel(String runtimeLoggerName,
            Level logLevel, String targetName)
            throws JBIRemoteException {
        ConfigurationService configurationService = getConfigurationService();
        configurationService.setRuntimeLoggerLevel(runtimeLoggerName, logLevel, targetName);
    }

    /**
     * Gets all the runtime loggers and their display names
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of display names to their logger names
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* display name */, String /* logger name */> getRuntimeLoggerNames(
            String targetName)
            throws JBIRemoteException {
        Map<String, String> resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getRuntimeLoggerNames(targetName);

        return resultObject;
    }

    /**
     * Return the display name for a runtime logger
     * 
     * @param runtimeLoggerName
     *            name of the logger (e.g. com.sun.jbi.framework)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return the display name for the given logger
     * @throws JBIRemoteException
     *             on error
     */
    public String getRuntimeLoggerDisplayName(String runtimeLoggerName,
            String targetName)
            throws JBIRemoteException {
        String resultString = null;
        ConfigurationService configurationService = getConfigurationService();
        resultString = configurationService.getRuntimeLoggerDisplayName(runtimeLoggerName, targetName);

        return resultString;
    }
    
    /**
     * Gets the extension MBean object names
     * 
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return
     * @throws JBIRemoteException
     *             on error
     */
    public Map<String /* targetInstanceName */, ObjectName[]> getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName)
            throws JBIRemoteException {
        Map<String, ObjectName[]> resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService
                .getComponentExtensionMBeanObjectNames(componentName,
                        extensionName, targetName);
        return resultObject;
    }
    
    /**
     * Gets the extension MBean object names
     * 
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return an array of ObjectName(s)
     * @throws JBIRemoteException
     *             on error
     */
    public ObjectName[] getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName,
            String targetInstanceName) throws JBIRemoteException {
        ObjectName[] resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService
                .getComponentExtensionMBeanObjectNames(componentName,
                        extensionName, targetName, targetInstanceName);
        return resultObject;
    }
    
    /**
     * Retrieve component configuration
     * 
     * @param componentName
     * @param targetName
     * @return the targetName as key and the name/value pairs as properties
     */
    public Properties getComponentConfiguration(String componentName,
            String targetName) throws JBIRemoteException {
        Properties resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getComponentConfiguration(
                componentName, targetName);
        return resultObject;
    }
    
    /**
     * Will return jbi mgmt message with success, failure, or partial success
     * per instance. The Map entry per instance will have value as Properties
     * object. String object should be returned when there is a failure to
     * retrieve the configuration on a particular instance. The other way is to
     * have Properties object in both success and failure case, but in the
     * failure case, the Properties object will have a predefined name, value
     * pair ("com.sun.jbi.cluster.instance.error", "xml text" ) in the
     * properties object.
     * 
     * @param componentName
     * @param configurationValue
     * @param targetName
     * @return
     * @deprecated
     */
    public Properties /* targetName, success/failure */
    setComponentConfiguration(String componentName,
            Properties /* configProps */configurationValues, String targetName)
            throws JBIRemoteException {
        Properties resultObject = new Properties();
        String result = "";
        ConfigurationService configurationService = getConfigurationService();
        result = (String) configurationService.setComponentConfiguration(
                componentName, configurationValues, targetName);
        if (result == null) {
            result = "";
        }
        resultObject.put("result", result);
        return resultObject;
    }
    
    /**
     * Will return jbi mgmt message with success, failure, or partial success
     * per instance. The entry per instance will have value as part of the
     * management message (XML) String.
     * 
     * @param targetName
     *            the name of the JBI target
     * @param componentName
     *            the component name
     * @param configurationValues
     *            the configuration properties
     * @return
     */
    public String setComponentConfiguration(String targetName,
            String componentName, Properties configurationValues)
            throws JBIRemoteException {
        String resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.setComponentConfiguration(
                componentName, configurationValues, targetName);
        return resultObject;
    }
    
    /**
     * updates component (service engine, binding component)
     * 
     * @param componentName
     *            Name of the component to update.
     * @param zipFilePath
     *            archive file in a zip format
     * @return The name of the component if successful
     * @throws JBIRemoteException
     *             on error
     */
    public String updateComponent(String componentName, String zipFilePath)
            throws JBIRemoteException {
        String resultObject = null;
        InstallationService installationService = getInstallationService();
        resultObject = installationService.upgradeComponent(componentName,
                zipFilePath);
        return resultObject;
    }
    
    /**
     * Checks to see if the Target (server, cluster) is up or down.
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return true if Target is up, false if not
     * @throws JBIRemoteException
     *             if error or exception occurs.
     */
    public boolean isTargetUp(String targetName) throws JBIRemoteException {
        boolean resultObject = false;
        AdministrationService administrationService = getAdministrationService();
        resultObject = administrationService.isTargetUp(targetName);
        return resultObject;
    }
    
    // ///////////////////////////////////////////////////////
    // -- Runtime Configuration --
    // ///////////////////////////////////////////////////////
    
    /**
     * This method returns the runtime configuration metadata associated with
     * the specified property. The metadata contain name-value pairs like:
     * default, descriptionID, descriptorType, displayName, displayNameId,
     * isStatic, name, resourceBundleName, tooltip, tooltipId, etc.
     * 
     * @param propertyKeyName
     * @return Properties that represent runtime configuration metadata
     * @throws JBIRemoteException
     */
    public Properties getRuntimeConfigurationMetaData(String propertyKeyName)
            throws JBIRemoteException {
        Properties resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService
                .getRuntimeConfigurationMetaData(propertyKeyName);
        return resultObject;
    }
    
    /**
     * This method sets one or more configuration parameters on the runtime with
     * a list of name/value pairs passed as a properties object. The property
     * name in the properties object should be an existing configuration
     * parameter name. If user try to set the parameter that is not in the
     * configuration parameters list, this method will throw an exception.
     * 
     * The value of the property can be any object. If the value is non string
     * object, its string value (Object.toString()) will be used as a value that
     * will be set on the configuration.
     * 
     * This method first validates whether all the paramters passed in
     * properties object exist in the runtime configuration or not. If any one
     * the parameters passed is not existing, it will return an error without
     * settings the parameters that are passed in the properties including a
     * valid parameters.
     * 
     * If there is an error in setting a paramter, this method throws an
     * exception with the list of parameters that were not set.
     * 
     * @param Map
     *            <String, Object> params Properties object that contains
     *            name/value pairs corresponding to the configuration parameters
     *            to be set on the runtime.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return true if server restart is required, false if not
     * 
     * @throws JBIRemoteException
     *             if there is a jmx error or a invalid parameter is passed in
     *             the params properties object. In case of an error setting the
     *             a particular parameter, the error message should list the
     *             invalid parameters.
     */
    public boolean setRuntimeConfiguration(Properties parameters,
            String targetName) throws JBIRemoteException {
        boolean resultObject = false;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.setRuntimeConfiguration(parameters,
                targetName);
        return resultObject;
        
    }
    
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Map<String, Object> that represents the list of configuration
     *         parameter descriptors.
     * 
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Properties getRuntimeConfiguration(String targetName)
            throws JBIRemoteException {
        Properties resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getRuntimeConfiguration(targetName);
        return resultObject;
    }
    
    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     * 
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     * 
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     * 
     * @return Map<String, Object> that represents the list of configuration
     *         parameter descriptors.
     * 
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Properties getDefaultRuntimeConfiguration()
            throws JBIRemoteException {
        Properties resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getDefaultRuntimeConfiguration();
        return resultObject;
    }
    
    /**
     * checks if the server need to be restarted to apply the changes made to
     * some of the configuration parameters.
     * 
     * @return true if server need to be restarted for updated configuration to
     *         take effect. false if no server restart is needed.
     */
    public boolean isServerRestartRequired() throws JBIRemoteException {
        boolean resultObject = false;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.isServerRestartRequired();
        return resultObject;
    }

    /*----------------------------------------------------------------------------------*\
     *              Operations for Application Configuration Management                 *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for component configuration. This method
     * returns true if the component has a configuration MBean with configurable
     * attributes
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean has configuration 
     *              attributes
     * @throws JBIRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isComponentConfigSupported(String componentName, String targetName)  
        throws JBIRemoteException
     {
        Boolean resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.isComponentConfigSupported(
                componentName, targetName);
        return resultObject;
     }
     
    /*----------------------------------------------------------------------------------*\
     *              Operations for Application Variable Management                      *
    \*----------------------------------------------------------------------------------*/

    /**
     * Detect the components support for application variables. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application variable management.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean implements all the
     *         operations for application variables.
     * @throws JBIRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppVarsSupported(String componentName, String targetName)  
        throws JBIRemoteException
     {
        Boolean resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.isAppVarsSupported(
                componentName, targetName);
        return resultObject;
     }
     
    /**
     * Add application variables to a component installed on a given target. If
     * even a variable from the set is already set on the component, this
     * operation fails.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param appVariables -
     *            set of application variables to add. The values of the
     *            application variables have the application variable type and
     *            value and the format is "[type]value"
     * @return a JBI Management message indicating the status of the operation.
     *         In case a variable is not added the management message has a
     *         ERROR task status message giving the details of the failure.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String addApplicationVariables(String componentName,
            String targetName, Properties appVariables)
            throws JBIRemoteException {
        String resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.addApplicationVariables(
                componentName, targetName, appVariables);
        return resultObject;
    }
    
    /**
     * Set application variables on a component installed on a given target. If
     * even a variable from the set has not been added to the component, this
     * operation fails.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param appVariables -
     *            set of application variables to update. The values of the
     *            application variables have the application variable type and
     *            value and the format is "[type]value"
     * @return a JBI Management message indicating the status of the operation.
     *         In case a variable is not set the management message has a ERROR
     *         task status message giving the details of the failure.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String setApplicationVariables(String componentName,
            String targetName, Properties appVariables)
            throws JBIRemoteException {
        String resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.setApplicationVariables(
                componentName, appVariables, targetName);
        return resultObject;
    }
    
    /**
     * Delete application variables from a component installed on a given
     * target. If even a variable from the set has not been added to the
     * component, this operation fails.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param appVariableNames -
     *            names of application variables to delete.
     * @return a JBI Management message indicating the status of the operation.
     *         In case a variable is not deleted the management message has a
     *         ERROR task status message giving the details of the failure.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String deleteApplicationVariables(String componentName,
            String targetName, String[] appVariableNames)
            throws JBIRemoteException {
        String resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.deleteApplicationVariables(
                componentName, targetName, appVariableNames);
        return resultObject;
    }
    
    /**
     * Get all the application variables set on a component.
     * 
     * @return all the application variables et on the component. The return
     *         proerties set has the name="[type]value" pairs for the
     *         application variables.
     * @return a JBI Management message indicating the status of the operation.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Properties getApplicationVariables(String componentName,
            String targetName) throws JBIRemoteException {
        Properties resultObject = new Properties();
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getApplicationVariables(
                componentName, targetName);
        return resultObject;
    }
    
    /*----------------------------------------------------------------------------------*\
     *              Operations for Application Configuration Management                 *
    \*----------------------------------------------------------------------------------*/
    
    /**
     * Detect the components support for application configuration. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application configuration management.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean implements all the
     *         operations for application configuration.
     * @throws JBIRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppConfigSupported(String componentName, String targetName)  
        throws JBIRemoteException
     {
        Boolean resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.isAppConfigSupported(
                componentName, targetName);
        return resultObject;
     }
     
    /**
     * Get the CompositeType definition for the components application configuration 
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return the CompositeType for the components application configuration.
     */
     public CompositeType queryApplicationConfigurationType
         (String componentName, String targetName)
             throws JBIRemoteException
     {
        CompositeType resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.queryApplicationConfigurationType(
                componentName, targetName);
        return resultObject;
     }
     
    /**
     * Add a named application configuration to a component installed on a given
     * target.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param name
     *            application configuration name
     * @param config
     *            application configuration represented as a set of properties.
     * @return a JBI Management message indicating the status of the operation.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String addApplicationConfiguration(String componentName,
            String targetName, String name, Properties config)
            throws JBIRemoteException {
        String resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.addApplicationConfiguration(
                componentName, targetName, name, config);
        return resultObject;
    }
    
    /**
     * Update a named application configuration in a component installed on a
     * given target.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param name
     *            application configuration name
     * @param config
     *            application configuration represented as a set of properties.
     * @return a JBI Management message indicating the status of the operation.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String setApplicationConfiguration(String componentName,
            String targetName, String name, Properties config)
            throws JBIRemoteException {
        String resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.setApplicationConfiguration(
                componentName, name, config, targetName);
        return resultObject;
    }
    
    /**
     * Delete a named application configuration in a component installed on a
     * given target.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param name
     *            name of application configuration to be deleted
     * @return a JBI Management message indicating the status of the operation.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String deleteApplicationConfiguration(String componentName,
            String targetName, String name) throws JBIRemoteException {
        String resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.deleteApplicationConfiguration(
                componentName, targetName, name);
        return resultObject;
    }
    
    /**
     * List all the application configurations in a component.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return an array of names of all the application configurations.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public String[] listApplicationConfigurationNames(String componentName,
            String targetName) throws JBIRemoteException {
        String[] resultObject = new String[] {};
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.listApplicationConfigurationNames(
                componentName, targetName);
        return resultObject;
    }
    
    /**
     * Get a specific named configuration. If the named configuration does not
     * exist in the component the returned properties is an empty set.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return the application configuration represented as a set of properties.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Properties getApplicationConfiguration(String componentName,
            String targetName, String name) throws JBIRemoteException {
        Properties resultObject = new Properties();
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getApplicationConfiguration(
                componentName, name, targetName);
        return resultObject;
    }
    
    /**
     * Get all the application configurations set on a component.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a map of all the application configurations keyed by the
     *         configuration name.
     * @throws JBIRemoteException
     *             if there is a jmx error accessing the instance
     */
    public Map<String, Properties> getApplicationConfigurations(
            String componentName, String targetName) throws JBIRemoteException {
        Map<String, Properties> resultObject = new HashMap<String, Properties>();
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.getApplicationConfigurations(
                componentName, targetName);
        return resultObject;
    }
    
     
    /**
     * This method is used to verify if the application variables and 
     * application configuration objects used in the given 
     * application are available in JBI runtime in the specified target. 
     * Also this method verifies if all necessary  components are installed.
     * If generateTemplates is true templates for missing application variables 
     * and application configurations are generated. A command script that uses
     * the template files to set configuration objects is generated.
     *
     * @param applicationURL the URL for the application zip file
     * @param targetName the target on which the application has to be verified
     * @param generateTemplates true if templates have to be generated
     * @param templateDir the dir to store the generated templates
     * @param includeDeployCommand true if the generated script should include
     * deploy command 
     *
     * @returns CompositeData the verification report
     * 
     * CompositeType of verification report
     *  String          - "ServiceAssemblyName",
     *  String          - "ServiceAssemblyDescription",
     *  Integer         - "NumServiceUnits",
     *  Boolean         - "AllComponentsInstalled",
     *  String[]        - "MissingComponentsList",
     *  CompositeData[] - "EndpointInfo",
     *  CompositeData[] - "JavaEEVerifierReport"
     *  String          - "TemplateZIPID"
     * 
     * CompositeType of each EndpointInfo
     *  String    - "EndpointName",
     *  String    - "ServiceUnitName",
     *  String    - "ComponentName",
     *  String    - "Status"
     *  String[]  - "MissingApplicationVariables"
     *  String[]  - "MissingApplicationConfigurations"
     * 
     * CompositeType of each JavaEEVerifierReport
     *  String        - "ServiceUnitName"
     *  TabularData   - "JavaEEVerifierReport"
     *
     * TabularType of each JavaEEVerifierReport
     * 
     * SimpleType.STRING  - "Ear Filename"
     * SimpleType.STRING  - "Referrence By"
     * SimpleType.STRING  - "Referrence Class"
     * SimpleType.STRING  - "JNDI Name"
     * SimpleType.STRING  - "JNDI Class Type"
     * SimpleType.STRING  - "Message"
     * SimpleType.INTEGER - "Status"
     * 
     * @throws JBIRemoteException if the application could not be verified
     * 
     * Note: param templateDir is used between ant/cli and common client client
     * TemplateZIPID is used between common client server and common client client
     */
    public CompositeData verifyApplication(String applicationURL,
            String targetName, boolean generateTemplates, String templateDir,
            boolean includeDeployCommand) throws JBIRemoteException {
        CompositeData resultObject = null;
        String rawXMLData = null;
        ApplicationVerificationReport report = null;
        ConfigurationService configurationService = getConfigurationService();
        rawXMLData = configurationService.verifyApplication(applicationURL,
                generateTemplates, templateDir, includeDeployCommand,
                targetName, applicationURL);
        if ((rawXMLData != null) && (rawXMLData.length() > 0)) {
            try {
                report = ApplicationVerificationReportReader
                        .parseFromXMLData(rawXMLData);
            } catch (MalformedURLException e) {
                throw new JBIRemoteException(e);
            } catch (ParserConfigurationException e) {
                throw new JBIRemoteException(e);
            } catch (SAXException e) {
                throw new JBIRemoteException(e);
            } catch (URISyntaxException e) {
                throw new JBIRemoteException(e);
            } catch (IOException e) {
                throw new JBIRemoteException(e);
            }
        }
        resultObject = report.generateCompositeData();
        return resultObject;
    }
    
    /**
     * This method is used to export the application variables and application
     * configuration objects used by the given application in the specified
     * target.
     * 
     * @param applicationName
     *            the name of the application
     * @param targetName
     *            the target whose configuration has to be exported
     * @param configDir
     *            the dir to store the configurations
     * @returns String the id for the zip file with exported configurations
     * 
     * @throws JBIRemoteException
     *             if the application configuration could not be exported
     * 
     * Note: param configDir is used between ant/cli and common client client.
     * The return value is used between common client server and common client
     * client.
     */
    public String exportApplicationConfiguration(String applicationName,
            String targetName, String configDir) throws JBIRemoteException {
        String resultObject = null;
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.exportApplicationConfiguration(
                applicationName, targetName, configDir);
        return resultObject;
    }
    
    /*---------------------------------------------------------------------------------*\
     *            Operations Component Configuration meta-data Management              *
    \*---------------------------------------------------------------------------------*/

    /**
     * Retrieves the component specific configuration schema.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a String containing the configuration schema.
     * @throws ManagementRemoteException
     *             on errors.
     */
    public String retrieveConfigurationDisplaySchema(String componentName,
            String targetName) throws JBIRemoteException {
        String resultObject = "";
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.retrieveConfigurationDisplaySchema(componentName, targetName);
        return resultObject;
    }
    
    /**
     * Retrieves the component configuration metadata. The XML data conforms to
     * the component configuration schema.
     * 
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a String containing the configuration metadata.
     * @throws ManagementRemoteException
     *             on errors
     */
    public String retrieveConfigurationDisplayData(String componentName,
            String targetName) throws JBIRemoteException {
        String resultObject = "";
        ConfigurationService configurationService = getConfigurationService();
        resultObject = configurationService.retrieveConfigurationDisplayData(componentName, targetName);
        return resultObject;
    }
    
    /**
     * This method is used to provide JBIFramework statistics in the
     * given target.
     * @param target target name.
     * @return TabularData table of framework statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getFrameworkStats(String targetName)
    throws JBIRemoteException {
        PerformanceMeasurementService profilingService = this.getPerformanceMeasurementService();
        return profilingService.getFrameworkStatisticsAsTabularData(targetName);
    }
    
    /**
     * This method is used to provide statistics for the given component
     * in the given target
     * @param targetName target name
     * @param componentName component name
     * @return TabularData table of component statistics
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     *
     */
    public TabularData getComponentStats(String componentName, String targetName)
    throws JBIRemoteException {
        PerformanceMeasurementService profilingService = this.getPerformanceMeasurementService();
        return profilingService.getComponentStatisticsAsTabularData(componentName, targetName);
    }
    
                
    /**
     * This method is used to provide statistic information about the given 
     * endpoint in the given target
     * @param targetName target name
     * @param endpointName the endpoint Name
     * @return TabularData table of endpoint statistics
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getEndpointStats(String endpointName, String targetName)
    throws JBIRemoteException {
        PerformanceMeasurementService profilingService = this.getPerformanceMeasurementService();
        return profilingService.getEndpointStatisticsAsTabularData(endpointName, targetName);
    }
    
    /**
     * This method is used to provide statistics about the message service in the
     * given target.
     * @param target target name.
     * @return TabularData table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getNMRStats(String targetName)
    throws JBIRemoteException {
        PerformanceMeasurementService profilingService = this.getPerformanceMeasurementService();
        return profilingService.getNMRStatisticsAsTabularData(targetName);
    }
    
    
    /**
     * This method is used to provide statistics about a Service Assembly
     * in the given target.
     * @param target target name.
     * @param saName the service assembly name.
     * @return TabularData table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getServiceAssemblyStats(String saName, String targetName)
    throws JBIRemoteException {
        PerformanceMeasurementService profilingService = this.getPerformanceMeasurementService();
        return profilingService.getServiceAssemblyStatisticsAsTabularData(saName, targetName);
    }
    
   /**
     * This method is used to provide a list of consuming endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of consuming endpoints 
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "InstanceName",
     *  String[]          - "Endpoints",
     */
    public TabularData getConsumingEndpointsForComponent(String componentName, String targetName)
    throws JBIRemoteException
    {
        PerformanceMeasurementService profilingService = this.getPerformanceMeasurementService();
        return profilingService.getConsumingEndpointsForComponentAsTabularData(componentName, targetName);
    }
    
    /**
     * This method is used to provide a list of provisioning endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of provisioning endpoints 
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "InstanceName",
     *  String[]          - "Endpoints",
     */
    public TabularData getProvidingEndpointsForComponent(String componentName, String targetName)
    throws JBIRemoteException
    {
        PerformanceMeasurementService profilingService = this.getPerformanceMeasurementService();
        return profilingService.getProvidingEndpointsForComponentAsTabularData(componentName, targetName);
    }
    
    /**
     * Scaffolding TDB when actual application variables and configuration are
     * supported.
     */
    private String getSuccessManagementMessage(String opName) {
        return CANNED_RESPONSE.replace("OPERATION", opName);
    }
    
    // //////////////////////////////////////////////////////
    // -- Common Operations --
    // //////////////////////////////////////////////////////
    /**
     * gives the I18N bundle
     * 
     * @return I18NBundle object
     */
    protected static I18NBundle getI18NBundle() {
        // lazzy initialize the JBI Client
        if (sI18NBundle == null) {
            sI18NBundle = new I18NBundle("com.sun.jbi.ui.client");
        }
        return sI18NBundle;
    }
    
    public static String formatTargetNames(String target) {
        return target;
    }
    
    public static String formatTargetNames(String[] target) {
        String result = "{ ";
        for (int index = 0; index < target.length; index++) {
            result += target[index];
            result += "  ";
        }
        result += "}";
        return result;
    }
    
    /**
     * Displays the result
     * 
     * @param result
     */
    public static void displayResult(String testingOperation, String result) {
        System.out.println(" ** Testing: " + testingOperation + " **");
        System.out.println("Result is:");
        System.out.println(result);
        System.out.println("==============================");
    }
    
    /**
     * Displays the result map
     * 
     * @param resultObject
     */
    public static void displayResult(String testingOperation,
            Map<String, String> resultObject) {
        System.out.println(" ** Testing: " + testingOperation + " **");
        System.out.println("Result is:");
        Set<String> keySet = resultObject.keySet();
        for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            if (key != null) {
                String value = (String) resultObject.get(key);
                System.out.println("Key: " + key);
                System.out.println("Value: " + value);
            }
        }
        System.out.println("==============================");
    }
    
    /**
     * Prepare Single target test cases
     * 
     * @param commands
     * @param bindingName
     * @param bindingArtifactFile
     * @param engineName
     * @param engineArtifactFile
     * @param libraryName
     * @param libraryArtifactFile
     * @param targetName
     */
    public static void testPrepareSingleTargetCases(JBIAdminCommands commands,
            String bindingName, String bindingArtifactFile, String engineName,
            String engineArtifactFile, String libraryName,
            String libraryArtifactFile, String assemblyName,
            String assemblyArtifactFile, String targetName) {
        
        String result = null;
        String states[] = { "started", "stopped", "shutdown", "unknown" };
        
        if (commands != null) {
            try {
                result = commands.installSharedLibrary(libraryArtifactFile,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "installSharedLibrary("
                                + libraryArtifactFile
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                result = commands
                        .getSharedLibraryInstallationDescriptor(libraryName);
                JBIAdminCommandsClientImpl.displayResult(
                        "getSharedLibraryInstallationDescriptor(" + libraryName
                                + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                Properties properties = new Properties();
                properties.setProperty("OutboundThreads", "15");
                result = commands.installComponent(bindingArtifactFile,
                        properties, targetName);
                JBIAdminCommandsClientImpl.displayResult("installComponent("
                        + bindingArtifactFile
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            // try {
            // result = commands.installComponent(bindingArtifactFile,
            // targetName);
            // JBIAdminCommandsClientImpl.displayResult("installComponent("
            // + bindingArtifactFile
            // + ", "
            // + JBIAdminCommandsClientImpl
            // .formatTargetNames(targetName) + ")", result);
            // } catch (JBIRemoteException e) {
            // e.printStackTrace();
            // }
            try {
                result = commands
                        .getComponentInstallationDescriptor(bindingName);
                JBIAdminCommandsClientImpl.displayResult(
                        "getComponentInstallationDescriptor(" + bindingName
                                + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                result = commands.startComponent(bindingName, targetName);
                JBIAdminCommandsClientImpl.displayResult("startComponent("
                        + bindingName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.installComponent(engineArtifactFile,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult("installComponent("
                        + engineArtifactFile
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.startComponent(engineName, targetName);
                JBIAdminCommandsClientImpl.displayResult("startComponent("
                        + engineName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                result = commands.deployServiceAssembly(assemblyArtifactFile,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "deployServiceAssembly("
                                + assemblyArtifactFile
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands
                        .startServiceAssembly(assemblyName, targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "startServiceAssembly("
                                + assemblyName
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Cleanup Single target test cases
     * 
     * @param commands
     * @param bindingName
     * @param bindingArtifactFile
     * @param engineName
     * @param engineArtifactFile
     * @param libraryName
     * @param libraryArtifactFile
     * @param targetName
     */
    public static void testCleanupSingleTargetCases(JBIAdminCommands commands,
            String bindingName, String bindingArtifactFile, String engineName,
            String engineArtifactFile, String libraryName,
            String libraryArtifactFile, String assemblyName,
            String assemblyArtifactFile, String targetName) {
        
        String result = null;
        String states[] = { "started", "stopped", "shutdown", "unknown" };
        
        if (commands != null) {
            try {
                result = commands.stopServiceAssembly(assemblyName, targetName);
                JBIAdminCommandsClientImpl.displayResult("stopServiceAssembly("
                        + assemblyName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.shutdownServiceAssembly(assemblyName,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "shutdownServiceAssembly("
                                + assemblyName
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                result = commands.undeployServiceAssembly(assemblyName,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "undeployServiceAssembly("
                                + assemblyName
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.stopComponent(bindingName, targetName);
                JBIAdminCommandsClientImpl.displayResult("stopComponent("
                        + bindingName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.shutdownComponent(bindingName, targetName);
                JBIAdminCommandsClientImpl.displayResult("shutdownComponent("
                        + bindingName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.uninstallComponent(bindingName, targetName);
                JBIAdminCommandsClientImpl.displayResult("uninstallComponent("
                        + bindingName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.stopComponent(engineName, targetName);
                JBIAdminCommandsClientImpl.displayResult("stopComponent("
                        + engineName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.shutdownComponent(engineName, targetName);
                JBIAdminCommandsClientImpl.displayResult("shutdownComponent("
                        + engineName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.uninstallComponent(engineName, targetName);
                JBIAdminCommandsClientImpl.displayResult("uninstallComponent("
                        + engineName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.uninstallSharedLibrary(libraryName,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "uninstallSharedLibrary("
                                + libraryName
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * list/show Single target test cases
     * 
     * @param commands
     * @param bindingName
     * @param bindingArtifactFile
     * @param engineName
     * @param engineArtifactFile
     * @param libraryName
     * @param libraryArtifactFile
     * @param targetName
     */
    public static void testShowSingleTargetCases(JBIAdminCommands commands,
            String bindingName, String bindingArtifactFile, String engineName,
            String engineArtifactFile, String libraryName,
            String libraryArtifactFile, String assemblyName,
            String assemblyArtifactFile, String targetName) {
        String result = null;
        String states[] = { "started", "stopped", "shutdown", "unknown" };
        String frameworkStates[] = { JBIComponentInfo.SHUTDOWN_STATE,
                JBIComponentInfo.STARTED_STATE, JBIComponentInfo.STOPPED_STATE };
        
        if (commands != null) {
            
            try {
                result = commands.listBindingComponents(targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "listBindingComponents("
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.listServiceEngines(targetName);
                JBIAdminCommandsClientImpl.displayResult("listServiceEngines("
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                result = commands.listSharedLibraries(targetName);
                JBIAdminCommandsClientImpl.displayResult("listSharedLibraries("
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                result = commands.listServiceAssemblies(targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "listServiceAssemblies("
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                result = commands.showSharedLibrary(libraryName, bindingName,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult("showSharedLibrary("
                        + libraryName
                        + ", "
                        + bindingName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.showSharedLibrary(libraryName, engineName,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult("showSharedLibrary("
                        + libraryName
                        + ", "
                        + engineName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            for (int index = 0; index < frameworkStates.length; index++) {
                
                try {
                    result = commands.listBindingComponents(
                            frameworkStates[index], libraryName, assemblyName,
                            targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "listBindingComponents("
                                    + states[index]
                                    + ", "
                                    + libraryName
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                try {
                    result = commands.listServiceEngines(
                            frameworkStates[index], libraryName, assemblyName,
                            targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "listServiceEngines("
                                    + states[index]
                                    + ", "
                                    + libraryName
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                
                try {
                    result = commands.showBindingComponent(bindingName,
                            frameworkStates[index], null, assemblyName,
                            targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "showBindingComponent("
                                    + bindingName
                                    + ", "
                                    + frameworkStates[index]
                                    + ", "
                                    + null
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                
                try {
                    result = commands.showServiceEngine(engineName,
                            frameworkStates[index], null, assemblyName,
                            targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "showServiceEngine("
                                    + engineName
                                    + ", "
                                    + frameworkStates[index]
                                    + ", "
                                    + null
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                
                try {
                    result = commands.showServiceAssembly(assemblyName,
                            frameworkStates[index], bindingName, targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "showServiceAssembly("
                                    + assemblyName
                                    + ", "
                                    + frameworkStates[index]
                                    + ", "
                                    + bindingName
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                
                try {
                    result = commands.showServiceAssembly(assemblyName,
                            frameworkStates[index], engineName, targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "showServiceAssembly("
                                    + assemblyName
                                    + ", "
                                    + frameworkStates[index]
                                    + ", "
                                    + engineName
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                
            }
            try {
                result = commands.showSharedLibrary(libraryName, null,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult("showSharedLibrary("
                        + libraryName
                        + ", "
                        + null
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                result = commands
                        .getServiceAssemblyDeploymentDescriptor(assemblyName);
                JBIAdminCommandsClientImpl.displayResult(
                        "getServiceAssemblyDeploymentDescriptor("
                                + assemblyName + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Testing Single Target Use-Cases
     * 
     * @param commands
     * @param bindingName
     * @param bindingArtifactFile
     * @param engineName
     * @param engineArtifactFile
     * @param libraryName
     * @param libraryArtifactFile
     * @param assemblyName
     * @param assemblyArtifactFile
     * @param targetName
     */
    public static void testSingleTargetCases(JBIAdminCommands commands,
            String bindingName, String bindingArtifactFile, String engineName,
            String engineArtifactFile, String libraryName,
            String libraryArtifactFile, String assemblyName,
            String assemblyArtifactFile, String targetName) {
        testPrepareSingleTargetCases(commands, bindingName,
                bindingArtifactFile, engineName, engineArtifactFile,
                libraryName, libraryArtifactFile, assemblyName,
                assemblyArtifactFile, targetName);
        testShowSingleTargetCases(commands, bindingName, bindingArtifactFile,
                engineName, engineArtifactFile, libraryName,
                libraryArtifactFile, assemblyName, assemblyArtifactFile,
                targetName);
        testCleanupSingleTargetCases(commands, bindingName,
                bindingArtifactFile, engineName, engineArtifactFile,
                libraryName, libraryArtifactFile, assemblyName,
                assemblyArtifactFile, targetName);
    }
    
    /**
     * Testing Multiple Targets Use-Cases
     * 
     * @param commands
     * @param bindingName
     * @param bindingArtifactFile
     * @param engineName
     * @param engineArtifactFile
     * @param libraryName
     * @param libraryArtifactFile
     * @param assemblyName
     * @param assemblyArtifactFile
     * @param targetName
     */
    public static void testMultipleTargetsCases(JBIAdminCommands commands,
            String bindingName, String bindingArtifactFile, String engineName,
            String engineArtifactFile, String libraryName,
            String libraryArtifactFile, String assemblyName,
            String assemblyArtifactFile, String[] targetName) {
        
        Map<String, String> result = null;
        String states[] = { "started", "stopped", "shutdown", "unknown" };
        if (commands != null) {
            try {
                result = commands.installSharedLibrary(libraryArtifactFile,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "installSharedLibrary("
                                + libraryArtifactFile
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.installComponent(bindingArtifactFile,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult("installComponent("
                        + bindingArtifactFile
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.installComponent(engineArtifactFile,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult("installComponent("
                        + engineArtifactFile
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.listBindingComponents(targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "listBindingComponents("
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.listServiceEngines(targetName);
                JBIAdminCommandsClientImpl.displayResult("listServiceEngines("
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            for (int index = 0; index < states.length; index++) {
                try {
                    result = commands.listBindingComponents(states[index],
                            libraryName, assemblyName, targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "listBindingComponents("
                                    + states[index]
                                    + ", "
                                    + libraryName
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                try {
                    result = commands.listServiceEngines(states[index],
                            libraryName, assemblyName, targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "listServiceEngines("
                                    + states[index]
                                    + ", "
                                    + libraryName
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                
                // try {
                // result = commands.listServiceAssemblies(states[index],
                // bindingName, targetName);
                // JBIAdminCommandsClientImpl.displayResult("listServiceAssemblies("
                // + states[index]
                // + ", "
                // + bindingName
                // + ", "
                // + JBIAdminCommandsClientImpl
                // .formatTargetNames(targetName) + ")", result);
                // } catch (JBIRemoteException e) {
                // e.printStackTrace();
                // }
                // try {
                // result = commands.listServiceAssemblies(states[index],
                // engineName, targetName);
                // JBIAdminCommandsClientImpl.displayResult("listServiceAssemblies("
                // + states[index]
                // + ", "
                // + engineName
                // + ", "
                // + JBIAdminCommandsClientImpl
                // .formatTargetNames(targetName) + ")", result);
                // } catch (JBIRemoteException e) {
                // e.printStackTrace();
                // }
                try {
                    result = commands.showBindingComponent(bindingName,
                            states[index], libraryName, assemblyName,
                            targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "showBindingComponent("
                                    + bindingName
                                    + ", "
                                    + states[index]
                                    + ", "
                                    + libraryName
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                try {
                    result = commands.showServiceEngine(engineName,
                            states[index], libraryName, assemblyName,
                            targetName);
                    JBIAdminCommandsClientImpl.displayResult(
                            "showServiceEngine("
                                    + engineName
                                    + ", "
                                    + states[index]
                                    + ", "
                                    + libraryName
                                    + ", "
                                    + assemblyName
                                    + ", "
                                    + JBIAdminCommandsClientImpl
                                            .formatTargetNames(targetName)
                                    + ")", result);
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                // try {
                // result = commands.showServiceAssembly(assemblyName,
                // states[index], bindingName, targetName);
                // JBIAdminCommandsClientImpl.displayResult("showServiceAssembly("
                // + assemblyName
                // + ", "
                // + states[index]
                // + ", "
                // + bindingName
                // + ", "
                // + JBIAdminCommandsClientImpl
                // .formatTargetNames(targetName) + ")", result);
                // } catch (JBIRemoteException e) {
                // e.printStackTrace();
                // }
            } // end of states for loop
            
            // try {
            // result = commands.listServiceAssemblies(bindingName, targetName);
            // JBIAdminCommandsClientImpl.displayResult("listServiceAssemblies("
            // + bindingName
            // + ", "
            // + JBIAdminCommandsClientImpl
            // .formatTargetNames(targetName) + ")", result);
            // } catch (JBIRemoteException e) {
            // e.printStackTrace();
            // }
            // try {
            // result = commands.listServiceAssemblies(engineName, targetName);
            // JBIAdminCommandsClientImpl.displayResult("listServiceAssemblies("
            // + engineName
            // + ", "
            // + JBIAdminCommandsClientImpl
            // .formatTargetNames(targetName) + ")", result);
            // } catch (JBIRemoteException e) {
            // e.printStackTrace();
            // }
            try {
                result = commands.showSharedLibrary(libraryName, bindingName,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult("showSharedLibrary("
                        + libraryName
                        + ", "
                        + bindingName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.showSharedLibrary(libraryName, engineName,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult("showSharedLibrary("
                        + libraryName
                        + ", "
                        + engineName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            
            try {
                result = commands.listSharedLibraries(targetName);
                JBIAdminCommandsClientImpl.displayResult("listSharedLibraries("
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.deployServiceAssembly(assemblyArtifactFile,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "deployServiceAssembly("
                                + assemblyArtifactFile
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.listServiceAssemblies(targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "listServiceAssemblies("
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.undeployServiceAssembly(assemblyName,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "undeployServiceAssembly("
                                + assemblyName
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.uninstallComponent(bindingName, targetName);
                JBIAdminCommandsClientImpl.displayResult("uninstallComponent("
                        + bindingName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.uninstallComponent(engineName, targetName);
                JBIAdminCommandsClientImpl.displayResult("uninstallComponent("
                        + engineName
                        + ", "
                        + JBIAdminCommandsClientImpl
                                .formatTargetNames(targetName) + ")", result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
            try {
                result = commands.uninstallSharedLibrary(libraryName,
                        targetName);
                JBIAdminCommandsClientImpl.displayResult(
                        "uninstallSharedLibrary("
                                + libraryName
                                + ", "
                                + JBIAdminCommandsClientImpl
                                        .formatTargetNames(targetName) + ")",
                        result);
            } catch (JBIRemoteException e) {
                e.printStackTrace();
            }
        } // end if commands != null
        
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        JBIAdminCommands commands = null;
        String hostName = "localhost";
        // int port = 18451;
        // int port = 18449;
        int port = 5651; // CAS
        // port = 5649; // JBITest
        // port = 4849;
        String userName = "admin";
        String password = "adminadmin";
        String dummyName = "foo";
        String targetName = JBIAdminCommands.SERVER_TARGET_KEY; // JBIAdminCommands.DOMAIN_TARGET_KEY;
        String[] targetNames = { targetName };
        boolean isJBIRuntimeEnabled = false;
        
        String bindingName = null;
        String bindingArtifactFile = null;
        String engineName = null;
        String engineArtifactFile = null;
        String libraryName = null;
        String libraryArtifactFile = null;
        String assemblyName = null;
        String assemblyArtifactFile = null;
        
        bindingArtifactFile = "C:/cygwin/a/open-esb/ri-clients/jbi-admin-common/regress/testdata/artifacts/httpsoapbc.jar";
        engineArtifactFile = "C:/cygwin/a/open-esb/ri-clients/jbi-admin-common/regress/testdata/artifacts/bpelserviceengine.jar";
        libraryArtifactFile = "C:/cygwin/a/open-esb/ri-clients/jbi-admin-common/regress/testdata/artifacts/wsdlsl.jar";
        assemblyArtifactFile = "C:/cygwin/a/open-esb/ri-clients/jbi-admin-common/regress/testdata/artifacts/HelloCompositeApp.zip";
        // assemblyArtifactFile =
        // "C:/cygwin/a/open-esb/ri-clients/jbi-admin-common/regress/testdata/artifacts/riskscoreProject.zip";
        
        bindingName = "sun-http-binding";
        engineName = "com.sun.bpelse-1.0-2";
        libraryName = "sun-wsdl-library";
        assemblyName = "HelloCompositeApp";
        // assemblyName = "01000000-D5BBB89E090100-0A12431F-01";
        
        // Test 5
        try {
            commands = JBIAdminCommandsClientFactory.getInstance(hostName,
                    port, userName, password);
            if (commands != null) {
                try {
                    isJBIRuntimeEnabled = commands.isJBIRuntimeEnabled();
                } catch (JBIRemoteException e) {
                    e.printStackTrace();
                }
                System.out.println("The JBI Framework is "
                        + (isJBIRuntimeEnabled ? "Enabled." : "NOT Enabled."));
                
                System.out.println(" ===================================");
                System.out.println(" === Testing Single Target Cases ===");
                System.out.println(" ===================================");
                JBIAdminCommandsClientImpl.testSingleTargetCases(commands,
                        bindingName, bindingArtifactFile, engineName,
                        engineArtifactFile, libraryName, libraryArtifactFile,
                        assemblyName, assemblyArtifactFile, targetName);
                // JBIAdminCommandsClientImpl.testInstall(commands,
                // bindingName, bindingArtifactFile, engineName,
                // engineArtifactFile, libraryName, libraryArtifactFile,
                // targetName);
                // JBIAdminCommandsClientImpl.testUninstall(commands,
                // bindingName, bindingArtifactFile, engineName,
                // engineArtifactFile, libraryName, libraryArtifactFile,
                // targetName);
                
                // System.out.println("
                // ======================================");
                // System.out.println(" === Testing Multiple Targets Cases
                // ===");
                // System.out.println("
                // ======================================");
                // JBIAdminCommandsClientImpl.testMultipleTargetsCases(commands,
                // bindingName, bindingArtifactFile, engineName,
                // engineArtifactFile, libraryName, libraryArtifactFile,
                // assemblyName, assemblyArtifactFile, targetNames);
                
            }
        } catch (Exception e) {
            
            e.printStackTrace();
        }
    }
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdministrationService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.api.administration;

import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.management.MBeanServerConnection;

import com.sun.esb.management.common.ManagementRemoteException;

/**
 * Defines operations for common administration services.
 * 
 * @author graj
 */
public interface AdministrationService {
    
    public static final String SYSTEM_INFORMATION_KEY ="SystemInfo"; 
    public static final String BUILD_NUMBER_KEY = "BuildNumber"; 
    public static final String COPYRIGHT_KEY = "Copyright"; 
    public static final String FULL_PRODUCT_NAME_KEY = "FullProductName";
    public static final String MAJOR_VERSION_KEY = "MajorVersion";
    public static final String MINOR_VERSION_KEY = "MinorVersion";
    public static final String SHORT_PRODUCT_NAME_KEY = "ShortProductName";    
    
    /**
     * Returns a map of target names to an array of target instance names.
     * In the case cluster targets, the key contains the cluster target name, 
     * and the the value contains an array of the "target instance" names.
     * If it is not a cluster target, the key contains the targetName, and 
     * the value is null.
     * 
     * @return map of target names to array of target instance names
     * @throws ManagementRemoteException
     */
    public Map<String /*targetName*/, String[] /*targetInstanceNames*/> listTargetNames() throws ManagementRemoteException;
    
    /**
     * Checks to see if the Target (server, cluster) is up or down.
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return true if Target is up, false if not
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    public boolean isTargetUp(String targetName)
            throws ManagementRemoteException;
    
    
    /**
     * Returns the jbi.xml Installation Descriptor for a Service Engine/Binding
     * Component.
     * 
     * @param the
     *            name of the ServiceEngine or Binding Component
     * @return the jbi.xml deployment descriptor of the archive
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    public String getComponentInstallationDescriptor(String componentName)
            throws ManagementRemoteException;
    
    /**
     * Gets the component state
     * 
     * @param componentName
     * @param targetName
     * @return the state of the component
     * @throws ManagementRemoteException
     */
    public String getComponentState(String componentName, String targetName)  throws ManagementRemoteException;
    
    /**
     * Gets the component type
     * 
     * @param componentName
     * @param targetName
     * @return the type of the component (binding-component, service-engine, or
     *         shared-library)
     * @throws ManagementRemoteException
     */
    public String getComponentType(String componentName, String targetName) throws ManagementRemoteException;
    
    /**
     * Get the list of consuming endpoints 
     * @param componentName
     * @param targetName
     * @return Returns the consumingEndpoints.
     * @throws ManagementRemoteException
     */
    public String[] getConsumingEndpoints(String componentName, String targetName) throws ManagementRemoteException;
    
    /**
     * Get the list of provisioning endpoints 
     * @param componentName
     * @param targetName
     * @return Returns the provisioningEndpoints.
     * @throws ManagementRemoteException
     */
    public String[] getProvisioningEndpoints(String componentName, String targetName) throws ManagementRemoteException;

    /**
     * Returns the jbi.xml Deployment Descriptor for a Service Assembly.
     * 
     * @param the
     *            name of the Service Assembly
     * @return the jbi.xml deployment descriptor of the archive
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    public String getServiceAssemblyDeploymentDescriptor(
            String serviceAssemblyName) throws ManagementRemoteException;
    
    /**
     * Gets the Service Assembly state
     * 
     * @param serviceAssemblyName
     * @param targetName
     * @return the state of the Service Assembly
     * @throws ManagementRemoteException
     */
    public String getServiceAssemblyState(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException;

    /**
     * Returns the jbi.xml Deployment Descriptor for a Service Unit.
     * 
     * @param the
     *            name of the Service Assembly
     * @param the
     *            name of the Service Unit
     * @return the jbi.xml deployment descriptor of the archive
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    public String getServiceUnitDeploymentDescriptor(
            String serviceAssemblyName, String serviceUnitName)
            throws ManagementRemoteException;

    /**
     * Returns the jbi.xml Installation Descriptor for a Shared Library.
     * 
     * @param the
     *            name of the Shared Library
     * @return the jbi.xml deployment descriptor of the archive
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    public String getSharedLibraryInstallationDescriptor(
            String sharedLibraryName) throws ManagementRemoteException;
    
    /**
     * Retrieves the primary WSDL associated with the specified endpoint.
     * @param componentName
     * @param endpoint
     * @param targetName
     * @return primary WSDL associated with the endpoint
     * @throws ManagementRemoteException
     */
    public String getWSDLDefinition(String componentName, String endpoint, String targetName) throws ManagementRemoteException;

    /**
     * Retrieves the WSDL or XSD associated with the specified endpoint and targetNamespace
     * @param componentName
     * @param endpoint
     * @param targetNamespace
     * @param targetName
     * @return wsdl or xsd
     * @throws ManagementRemoteException
     */
    public String getWSDLImportedResource(String componentName, String endpoint, String targetNamespace, String targetName) throws ManagementRemoteException;
    
    /**
     * check for BindingComponent
     * 
     * @param the
     *            name of the ServiceEngine or Binding Component
     * @return true if it is a Binding Component else false.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    public boolean isBindingComponent(String componentName)
            throws ManagementRemoteException;
    
    /**
     * Checks to see if a component/library is installed on a target
     *  
     * @param componentName
     * @param targetName
     * @return true if installed, false if not
     * @throws ManagementRemoteException
     */
    public boolean isJBIComponentInstalled(String componentName,
            String targetName) throws ManagementRemoteException;
    
    /**
     * Checks to see if the JBI Runtime is enabled.
     * 
     * @return true if JBI Runtime Framework is available, false if not
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    public boolean isJBIRuntimeEnabled() throws ManagementRemoteException;
    
    /**
     * Checks to see if a Service Assembly is deployed on a target
     * 
     * @param serviceAssemblyName
     * @param targetName
     * @return true if deployed, false if not
     * @throws ManagementRemoteException
     */
    public boolean isServiceAssemblyDeployed(String serviceAssemblyName, String targetName) throws ManagementRemoteException;
    
    /**
     * check for ServiceEngine
     * 
     * @param the
     *            name of the ServiceEngine or Binding Component
     * @return true if it is a Service Engine else false.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    public boolean isServiceEngine(String componentName)
            throws ManagementRemoteException;
    
    /**
     * Retrieve runtime version information
     * @return map of targetName to Properties elements detailing version info
     * @throws ManagementRemoteException
     */
    public Map<String /*targetName*/, Properties /*version information*/> getRuntimeDetails() throws ManagementRemoteException;
    
    /**
     * Get the instance name of the platform's administration server.  If the
     * platform does not provide a separate administration server, then this 
     * method returns the name of the local instance.
     * @return instance name of the administration server
     * @throws ManagementRemoteException
     */
    public String getAdminServerName() throws ManagementRemoteException;
    
    /**
     * Determine whether this instance is the administration server instance.
     * @return <CODE>true</CODE> if this instance is the administration server,
     * <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isAdminServer() throws ManagementRemoteException;
    
    /**
     * Get the name of this instance.
     * @return the name of this server instance.
     * @throws ManagementRemoteException
     */
    public String getInstanceName() throws ManagementRemoteException;
        
    /**
     * Determine if the specified instance is up.
     * @return true if the instance is up and running, false otherwise
     * @throws ManagementRemoteException
     */
    public boolean isInstanceUp(String instanceName) throws ManagementRemoteException;
    
    /**
     * Determine whether multiple servers are permitted within this AS
     * installation.
     * @return true if multiple servers are permitted.
     * @throws ManagementRemoteException
     */
    public boolean supportsMultipleServers() throws ManagementRemoteException;
    
    /**
     * Get the Target Name. If the instance is not a clustered instance then
     * the target name is the instance name. If the instance is part of a
     * cluster then the target name is the cluster name.
     *
     * @return the target name. 
     * @throws ManagementRemoteException
     */
    public String getTargetName() throws ManagementRemoteException;

    /**
     * Get the Target Name for a specified instance. If the instance is not
     * clustered the instance name is returned. This operation is invoked by
     * the JBI instance MBeans only.
     *
     * @return the target name. 
     * @throws ManagementRemoteException
     */
    public String getTargetName(String instanceName) throws ManagementRemoteException;
    
    /**
     * Get a set of the names of all the standalone servers in the domain.
     * @return a set of names of standalone servers in the domain.
     * @throws ManagementRemoteException
     */
    public Set<String> getStandaloneServerNames() throws ManagementRemoteException;
    
    /**
     * Get a set of the names of all the clustered servers in the domain.
     * @return a set of names of clustered servers in the domain.
     * @throws ManagementRemoteException
     */
    public Set<String> getClusteredServerNames() throws ManagementRemoteException;
    /**
     * Get a set of the names of all the clusters in the domain.
     * @return a set of names of clusters in the domain.
     * @throws ManagementRemoteException
     */
    public Set<String> getClusterNames() throws ManagementRemoteException;
    
    /**
     * Get a set of the names of all the servers in the specified cluster.
     * @return a set of names of servers in the cluster.
     * @throws ManagementRemoteException
     */
    public Set<String> getServersInCluster(String clusterName) throws ManagementRemoteException;
    
    /**
     * Determine whether a target is a valid server or cluster name.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a valid
     * standalone server name or cluster name, <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isValidTarget(String targetName) throws ManagementRemoteException;
    
    /**
     * Determine whether a target is a cluster.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a cluster,
     * <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isCluster(String targetName) throws ManagementRemoteException;
    
    /**
     * Determine whether a target is a standalone server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a standalone
     * server, <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isStandaloneServer(String targetName) throws ManagementRemoteException;
    
    /**
     * Determine whether the target is a clustered server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a clustered
     * server, <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isClusteredServer(String targetName) throws ManagementRemoteException;
    
    /**
     * Determine whether or not an instance is clustered.
     * @return <CODE>true</CODE> if the instance is clustered,
     * <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isInstanceClustered(String instanceName) throws ManagementRemoteException;

    /**
     * Get a string representation of the DAS JMX RMI connector port.
     * @return the JMX RMI connector port as a (CODE>String</CODE>.
     * @throws ManagementRemoteException
     */
    public String getJmxRmiPort() throws ManagementRemoteException;
    
    /**
     * Get the full path to the platform's instance root directory.
     * @return platform instance root
     * @throws ManagementRemoteException
     */
    public String getInstanceRoot() throws ManagementRemoteException;
    
    /**
     * Get the full path to the platform's instaall root directory.
     * @return platform install root
     * @throws ManagementRemoteException
     */
    public String getInstallRoot() throws ManagementRemoteException;
    
    /**
     * Get the MBean server connection for a particular instance.
     * @return the <CODE>MBeanServerConnection</CODE> for the specified instance.
     * @throws ManagementRemoteException
     */
    public MBeanServerConnection getConnectionForInstance(String instanceName) throws ManagementRemoteException;
    
}

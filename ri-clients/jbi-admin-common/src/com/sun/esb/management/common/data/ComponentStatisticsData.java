/* 
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentStatisticsData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.helper.ComponentStatisticsDataCreator;
import com.sun.esb.management.common.data.helper.ComponentStatisticsDataWriter;

/**
 * Provides Component Statistics
 * 
 * @author graj
 * 
 */
public class ComponentStatisticsData implements Serializable {
    static final long          serialVersionUID               = -1L;
    
    public static final String INSTANCENAME_KEY               = "InstanceName";
    
    public static final String COMPONENTUPTIME_KEY            =  "ComponentUpTime (ms)";
    
    public static final String NUMACTIVATEDENDPOINTS_KEY      = "NumActivatedEndpoints";
    
    public static final String NUMRECEIVEDREQUESTS_KEY        = "NumReceivedRequests";
    
    public static final String NUMSENTREQUESTS_KEY            = "NumSentRequests";
    
    public static final String NUMRECEIVEDREPLIES_KEY         = "NumReceivedReplies";
    
    public static final String NUMSENTREPLIES_KEY             = "NumSentReplies";
    
    public static final String NUMRECEIVEDDONES_KEY           = "NumReceivedDONEs";
    
    public static final String NUMSENTDONES_KEY               = "NumSentDONEs";
    
    public static final String NUMRECEIVEDFAULTS_KEY          = "NumReceivedFaults";
    
    public static final String NUMSENTFAULTS_KEY              = "NumSentFaults";
    
    public static final String NUMRECEIVEDERRORS_KEY          = "NumReceivedErrors";
    
    public static final String NUMSENTERRORS_KEY              = "NumSentErrors";
    
    public static final String NUMCOMPLETEDEXCHANGES_KEY      = "NumCompletedExchanges";
    
    public static final String NUMACTIVEEXCHANGES_KEY         = "NumActiveExchanges";
    
    public static final String NUMERROREXCHANGES_KEY          = "NumErrorExchanges";
    
    public static final String ME_RESPONSETIME_AVG_KEY        =  "MessageExchangeResponseTime Avg (ns)";
    
    public static final String ME_COMPONENTTIME_AVG_KEY       =  "MessageExchangeComponentTime Avg (ns)";
    
    public static final String ME_DELIVERYCHANNELTIME_AVG_KEY = "MessageExchangeDeliveryTime Avg (ns)";
    
    public static final String ME_MESSAGESERVICETIME_AVG_KEY  = "MessageExchangeNMRTime Avg (ns)";    
    
    public static final String COMPONENTEXTENSIONSTATS_KEY    = "ComponentExtensionStats";
    
    protected String                     instanceName;
    
    protected long                       componentUpTime;
    
    protected long                       numberOfActivatedEndpoints;
    
    protected long                       numberOfReceivedRequests;
    
    protected long                       numberOfSentRequests;
    
    protected long                       numberOfReceivedReplies;
    
    protected long                       numberOfSentReplies;
    
    protected long                       numberOfReceivedDones;
    
    protected long                       numberOfSentDones;
    
    protected long                       numberOfReceivedFaults;
    
    protected long                       numberOfSentFaults;
    
    protected long                       numberOfReceivedErrors;
    
    protected long                       numberOfSentErrors;
    
    protected long                       numberOfCompletedExchanges;
    
    protected long                       numberOfActiveExchanges;
    
    protected long                       numberOfErrorExchanges;
    
    protected long                     messageExchangeResponseTimeAverage;
    
    protected long                     messageExchangeComponentTimeAverage;
    
    protected long                     messageExchangeDeliveryChannelTimeAverage;
    
    protected long                     messageExchangeMessageServiceTimeAverage;
    
    protected CompositeData              componentExtensionStatus;
    
    protected String                     componentExtensionStatusAsString;
    
    protected boolean                   extendedTimingStatisticsFlagEnabled;
    
    /** Constructor - creates a ComponentStatisticsData object */
    public ComponentStatisticsData() {
    }
    
    /**
     * Generate Tabular Data for this object
     * @return tabular data of this object
     */
    static public TabularData generateTabularData(Map<String /* instanceName */, ComponentStatisticsData> map) {
        TabularData tabularData = null;
        try {
            tabularData = ComponentStatisticsDataCreator.createTabularData(map);
        } catch (ManagementRemoteException e) {
        }
        return tabularData;
    }
    
    /**
     * Retrieves the Component Statistics Data Map
     * 
     * @param tabularData
     * @return Component Statistics Data
     */
    @SuppressWarnings("unchecked")
    static public Map<String /* instanceName */, ComponentStatisticsData> retrieveDataMap(
            TabularData tabularData) {
        ComponentStatisticsData data = null;
        Map<String /* instanceName */, ComponentStatisticsData> dataMap = null;
        dataMap = new HashMap<String /* instanceName */, ComponentStatisticsData>();
        for (Iterator dataIterator = tabularData.values().iterator(); dataIterator
                .hasNext();) {
            CompositeData compositeData = (CompositeData) dataIterator.next();
            CompositeType compositeType = compositeData.getCompositeType();
            data = new ComponentStatisticsData();
            for (Iterator<String> itemIterator = compositeType.keySet()
                    .iterator(); itemIterator.hasNext();) {
                String item = (String) itemIterator.next();
                if (true == item
                        .equals(ComponentStatisticsData.INSTANCENAME_KEY)) {
                    String value = (String) compositeData.get(item);
                    data.setInstanceName(value);
                }
                if (true == item
                        .equals(ComponentStatisticsData.COMPONENTUPTIME_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setComponentUpTime(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMACTIVATEDENDPOINTS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfActivatedEndpoints(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMRECEIVEDREQUESTS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfReceivedRequests(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMSENTREQUESTS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfSentRequests(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMRECEIVEDREPLIES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfReceivedReplies(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMSENTREPLIES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfSentReplies(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMRECEIVEDDONES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfReceivedDones(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMSENTDONES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfSentDones(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMRECEIVEDFAULTS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfReceivedFaults(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMSENTFAULTS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfSentFaults(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMRECEIVEDERRORS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfReceivedErrors(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMSENTERRORS_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfSentErrors(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMCOMPLETEDEXCHANGES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfCompletedExchanges(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMACTIVEEXCHANGES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfActiveExchanges(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.NUMERROREXCHANGES_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setNumberOfErrorExchanges(value.longValue());
                }
                if (true == item
                        .equals(ComponentStatisticsData.ME_RESPONSETIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setMessageExchangeResponseTimeAverage(value
                            .longValue());
                    data.setExtendedTimingStatisticsFlagEnabled(true);
                }
                if (true == item
                        .equals(ComponentStatisticsData.ME_COMPONENTTIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setMessageExchangeComponentTimeAverage(value
                            .longValue());
                    data.setExtendedTimingStatisticsFlagEnabled(true);
                }
                if (true == item
                        .equals(ComponentStatisticsData.ME_DELIVERYCHANNELTIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setMessageExchangeDeliveryChannelTimeAverage(value
                            .longValue());
                    data.setExtendedTimingStatisticsFlagEnabled(true);
                }
                if (true == item
                        .equals(ComponentStatisticsData.ME_MESSAGESERVICETIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setMessageExchangeMessageServiceTimeAverage(value
                            .longValue());
                    data.setExtendedTimingStatisticsFlagEnabled(true);
                }
                if (true == item
                        .equals(ComponentStatisticsData.COMPONENTEXTENSIONSTATS_KEY)) {
                    CompositeData value = (CompositeData) compositeData
                            .get(item);
                    data.setComponentExtensionStatus(value);
                }
            }
            dataMap.put(data.getInstanceName(), data);
            
        }
        return dataMap;
    }
    
    /**
     * Converts a Component Statistics Data to an XML String
     * 
     * @param map
     * @return XML string representing a Component Statistics Datum
     * @throws ManagementRemoteException
     */
    static public String convertDataMapToXML(
            Map<String /* instanceName */, ComponentStatisticsData> map)
            throws ManagementRemoteException {
        String xmlText = null;
        try {
            xmlText = ComponentStatisticsDataWriter.serialize(map);
        } catch (ParserConfigurationException e) {
            throw new ManagementRemoteException(e);
        } catch (TransformerException e) {
            throw new ManagementRemoteException(e);
        }
        
        return xmlText;
    }
    
    /**
     * @return the instanceName
     */
    public String getInstanceName() {
        return this.instanceName;
    }
    
    /**
     * @param instanceName
     *            the instanceName to set
     */
    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
    
    /**
     * @return the componentUpTime
     */
    public long getComponentUpTime() {
        return this.componentUpTime;
    }
    
    /**
     * @param componentUpTime
     *            the componentUpTime to set
     */
    public void setComponentUpTime(long componentUpTime) {
        this.componentUpTime = componentUpTime;
    }
    
    /**
     * @return the extendedTimingStatisticsFlagEnabled
     */
    public boolean isExtendedTimingStatisticsFlagEnabled() {
        return this.extendedTimingStatisticsFlagEnabled;
    }

    /**
     * @param extendedTimingStatisticsFlagEnabled the extendedTimingStatisticsFlagEnabled to set
     */
    public void setExtendedTimingStatisticsFlagEnabled(
            boolean extendedTimingStatisticsFlagEnabled) {
        this.extendedTimingStatisticsFlagEnabled = extendedTimingStatisticsFlagEnabled;
    }

    /**
     * @return the numberOfActivatedEndpoints
     */
    public long getNumberOfActivatedEndpoints() {
        return this.numberOfActivatedEndpoints;
    }
    
    /**
     * @param numberOfActivatedEndpoints
     *            the numberOfActivatedEndpoints to set
     */
    public void setNumberOfActivatedEndpoints(long numberOfActivatedEndpoints) {
        this.numberOfActivatedEndpoints = numberOfActivatedEndpoints;
    }
    
    /**
     * @return the numberOfReceivedRequests
     */
    public long getNumberOfReceivedRequests() {
        return this.numberOfReceivedRequests;
    }
    
    /**
     * @param numberOfReceivedRequests
     *            the numberOfReceivedRequests to set
     */
    public void setNumberOfReceivedRequests(long numberOfReceivedRequests) {
        this.numberOfReceivedRequests = numberOfReceivedRequests;
    }
    
    /**
     * @return the numberOfSentRequests
     */
    public long getNumberOfSentRequests() {
        return this.numberOfSentRequests;
    }
    
    /**
     * @param numberOfSentRequests
     *            the numberOfSentRequests to set
     */
    public void setNumberOfSentRequests(long numberOfSentRequests) {
        this.numberOfSentRequests = numberOfSentRequests;
    }
    
    /**
     * @return the numberOfReceivedReplies
     */
    public long getNumberOfReceivedReplies() {
        return this.numberOfReceivedReplies;
    }
    
    /**
     * @param numberOfReceivedReplies
     *            the numberOfReceivedReplies to set
     */
    public void setNumberOfReceivedReplies(long numberOfReceivedReplies) {
        this.numberOfReceivedReplies = numberOfReceivedReplies;
    }
    
    /**
     * @return the numberOfSentReplies
     */
    public long getNumberOfSentReplies() {
        return this.numberOfSentReplies;
    }
    
    /**
     * @param numberOfSentReplies
     *            the numberOfSentReplies to set
     */
    public void setNumberOfSentReplies(long numberOfSentReplies) {
        this.numberOfSentReplies = numberOfSentReplies;
    }
    
    /**
     * @return the numberOfReceivedDones
     */
    public long getNumberOfReceivedDones() {
        return this.numberOfReceivedDones;
    }
    
    /**
     * @param numberOfReceivedDones
     *            the numberOfReceivedDones to set
     */
    public void setNumberOfReceivedDones(long numberOfReceivedDones) {
        this.numberOfReceivedDones = numberOfReceivedDones;
    }
    
    /**
     * @return the numberOfSentDones
     */
    public long getNumberOfSentDones() {
        return this.numberOfSentDones;
    }
    
    /**
     * @param numberOfSentDones
     *            the numberOfSentDones to set
     */
    public void setNumberOfSentDones(long numberOfSentDones) {
        this.numberOfSentDones = numberOfSentDones;
    }
    
    /**
     * @return the numberOfReceivedFaults
     */
    public long getNumberOfReceivedFaults() {
        return this.numberOfReceivedFaults;
    }
    
    /**
     * @param numberOfReceivedFaults
     *            the numberOfReceivedFaults to set
     */
    public void setNumberOfReceivedFaults(long numberOfReceivedFaults) {
        this.numberOfReceivedFaults = numberOfReceivedFaults;
    }
    
    /**
     * @return the numberOfSentFaults
     */
    public long getNumberOfSentFaults() {
        return this.numberOfSentFaults;
    }
    
    /**
     * @param numberOfSentFaults
     *            the numberOfSentFaults to set
     */
    public void setNumberOfSentFaults(long numberOfSentFaults) {
        this.numberOfSentFaults = numberOfSentFaults;
    }
    
    /**
     * @return the numberOfReceivedErrors
     */
    public long getNumberOfReceivedErrors() {
        return this.numberOfReceivedErrors;
    }
    
    /**
     * @param numberOfReceivedErrors
     *            the numberOfReceivedErrors to set
     */
    public void setNumberOfReceivedErrors(long numberOfReceivedErrors) {
        this.numberOfReceivedErrors = numberOfReceivedErrors;
    }
    
    /**
     * @return the numberOfSentErrors
     */
    public long getNumberOfSentErrors() {
        return this.numberOfSentErrors;
    }
    
    /**
     * @param numberOfSentErrors
     *            the numberOfSentErrors to set
     */
    public void setNumberOfSentErrors(long numberOfSentErrors) {
        this.numberOfSentErrors = numberOfSentErrors;
    }
    
    /**
     * @return the numberOfCompletedExchanges
     */
    public long getNumberOfCompletedExchanges() {
        return this.numberOfCompletedExchanges;
    }
    
    /**
     * @param numberOfCompletedExchanges
     *            the numberOfCompletedExchanges to set
     */
    public void setNumberOfCompletedExchanges(long numberOfCompletedExchanges) {
        this.numberOfCompletedExchanges = numberOfCompletedExchanges;
    }
    
    /**
     * @return the numberOfActiveExchanges
     */
    public long getNumberOfActiveExchanges() {
        return this.numberOfActiveExchanges;
    }
    
    /**
     * @param numberOfActiveExchanges
     *            the numberOfActiveExchanges to set
     */
    public void setNumberOfActiveExchanges(long numberOfActiveExchanges) {
        this.numberOfActiveExchanges = numberOfActiveExchanges;
    }
    
    /**
     * @return the numberOfErrorExchanges
     */
    public long getNumberOfErrorExchanges() {
        return this.numberOfErrorExchanges;
    }
    
    /**
     * @param numberOfErrorExchanges
     *            the numberOfErrorExchanges to set
     */
    public void setNumberOfErrorExchanges(long numberOfErrorExchanges) {
        this.numberOfErrorExchanges = numberOfErrorExchanges;
    }
    
    /**
     * @return the messageExchangeResponseTimeAverage
     */
    public long getMessageExchangeResponseTimeAverage() {
        return this.messageExchangeResponseTimeAverage;
    }
    
    /**
     * @param messageExchangeResponseTimeAverage
     *            the messageExchangeResponseTimeAverage to set
     */
    public void setMessageExchangeResponseTimeAverage(
            long messageExchangeResponseTimeAverage) {
        this.messageExchangeResponseTimeAverage = messageExchangeResponseTimeAverage;
    }
    
    /**
     * @return the messageExchangeComponentTimeAverage
     */
    public long getMessageExchangeComponentTimeAverage() {
        return this.messageExchangeComponentTimeAverage;
    }
    
    /**
     * @param messageExchangeComponentTimeAverage
     *            the messageExchangeComponentTimeAverage to set
     */
    public void setMessageExchangeComponentTimeAverage(
            long messageExchangeComponentTimeAverage) {
        this.messageExchangeComponentTimeAverage = messageExchangeComponentTimeAverage;
    }
    
    /**
     * @return the messageExchangeDeliveryChannelTimeAverage
     */
    public long getMessageExchangeDeliveryChannelTimeAverage() {
        return this.messageExchangeDeliveryChannelTimeAverage;
    }
    
    /**
     * @param messageExchangeDeliveryChannelTimeAverage
     *            the messageExchangeDeliveryChannelTimeAverage to set
     */
    public void setMessageExchangeDeliveryChannelTimeAverage(
            long messageExchangeDeliveryChannelTimeAverage) {
        this.messageExchangeDeliveryChannelTimeAverage = messageExchangeDeliveryChannelTimeAverage;
    }
    
    /**
     * @return the messageExchangeMessageServiceTimeAverage
     */
    public long getMessageExchangeMessageServiceTimeAverage() {
        return this.messageExchangeMessageServiceTimeAverage;
    }
    
    /**
     * @param messageExchangeMessageServiceTimeAverage
     *            the messageExchangeMessageServiceTimeAverage to set
     */
    public void setMessageExchangeMessageServiceTimeAverage(
            long messageExchangeMessageServiceTimeAverage) {
        this.messageExchangeMessageServiceTimeAverage = messageExchangeMessageServiceTimeAverage;
    }
    
    /**
     * @return the componentExtensionStatus
     */
    public CompositeData getComponentExtensionStatus() {
        return this.componentExtensionStatus;
    }
    
    /**
     * @param componentExtensionStatus
     *            the componentExtensionStatus to set
     */
    public void setComponentExtensionStatus(
            CompositeData componentExtensionStatus) {
        this.componentExtensionStatus = componentExtensionStatus;
        if(componentExtensionStatus != null) {
            this.componentExtensionStatusAsString = componentExtensionStatus.toString();
        }
    }
    
    /**
     * @return the componentExtensionStatusAsString
     */
    public String getComponentExtensionStatusAsString() {
        return this.componentExtensionStatusAsString;
    }

    /**
     * @param componentExtensionStatusAsString the componentExtensionStatusAsString to set
     */
    public void setComponentExtensionStatusAsString(
            String componentExtensionStatusAsString) {
        this.componentExtensionStatusAsString = componentExtensionStatusAsString;
    }

    /**
     * Return a displayable string of values
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n    Instance Name" + "=" + this.getInstanceName());
        buffer.append("\n    Component UpTime" + "="
                + this.getComponentUpTime());
        buffer.append("\n    Number Of Activated Endpoints" + "="
                + this.getNumberOfActivatedEndpoints());
        buffer.append("\n    Number Of Active Exchanges" + "="
                + this.getNumberOfActiveExchanges());
        buffer.append("\n    Number Of Completed Exchanges" + "="
                + this.getNumberOfCompletedExchanges());
        buffer.append("\n    Number Of Error Exchanges" + "="
                + this.getNumberOfErrorExchanges());
        buffer.append("\n    Number Of Received Dones" + "="
                + this.getNumberOfReceivedDones());
        buffer.append("\n    Number Of Received Errors" + "="
                + this.getNumberOfReceivedErrors());
        buffer.append("\n    Number Of Received Faults" + "="
                + this.getNumberOfReceivedFaults());
        buffer.append("\n    Number Of Received Replies" + "="
                + this.getNumberOfReceivedReplies());
        buffer.append("\n    Number Of Received Requests" + "="
                + this.getNumberOfReceivedRequests());
        buffer.append("\n    Number Of Sent Dones" + "="
                + this.getNumberOfSentDones());
        buffer.append("\n    Number Of Sent Errors" + "="
                + this.getNumberOfSentErrors());
        buffer.append("\n    Number Of Sent Faults" + "="
                + this.getNumberOfSentFaults());
        buffer.append("\n    Number Of Sent Replies" + "="
                + this.getNumberOfSentReplies());
        buffer.append("\n    Number Of Sent Requests" + "="
                + this.getNumberOfSentRequests());
        buffer.append("\n    Extended Timing Statistics are "+(this.isExtendedTimingStatisticsFlagEnabled()?"":"NOT")+" Enabled.");
        buffer.append("\n    \tMessage Exchange Component Time Average" + "="
                + this.getMessageExchangeComponentTimeAverage());
        buffer.append("\n    \tMessage Exchange Delivery Channel Time Average"
                + "=" + this.getMessageExchangeDeliveryChannelTimeAverage());
        buffer.append("\n    \tMessage Exchange Message Service Time Average"
                + "=" + this.getMessageExchangeMessageServiceTimeAverage());
        buffer.append("\n    \tMessage Exchange Response Time Average" + "="
                + this.getMessageExchangeResponseTimeAverage());
        buffer.append("\n    Component Extension Status" + "="
                + this.getComponentExtensionStatus());
        buffer.append("\n");
        return buffer.toString();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}

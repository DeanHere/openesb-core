/* 
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyStatisticsData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.helper.ServiceAssemblyStatisticsDataCreator;
import com.sun.esb.management.common.data.helper.ServiceAssemblyStatisticsDataWriter;

/**
 * Provides Service Assembly Statistics
 * 
 * @author graj
 * 
 */
public class ServiceAssemblyStatisticsData implements Serializable {
    static final long               serialVersionUID            = -1L;
    
    public static final String      INSTANCE_NAME_KEY           = "InstanceName";
    
    public static final String      NAME_KEY                    = "ServiceAssemblyName";
    
    public static final String      LAST_STARTUP_TIME_KEY       = "LastStartupTime";
    
    public static final String      STARTUP_TIME_AVG_KEY        = "StartupTime Avg (ms)";
    
    public static final String      STOP_TIME_AVG_KEY            = "StopTime Avg (ms)";
    
    public static final String      SHUTDOWN_TIME_AVG_KEY       = "ShutdownTime Avg (ms)";
    
    public static final String      UP_TIME_KEY                 = "UpTime (ms)";

    public static final String      SERVICE_UNIT_STATISTICS_KEY = "ServiceUnitStatistics";
    
    String                          instanceName;
    
    String                          name;
    
    Date                            lastStartupTime;
    
    long                          startupTimeAverage;
    
    long                          stopTimeAverage;
    
    long                          shutdownTimeAverage;
    
    long                            upTime;
    
    List<ServiceUnitStatisticsData> serviceUnitStatisticsList   = new ArrayList<ServiceUnitStatisticsData>();
    
    /** Constructor - creates a ServiceAssemblyStatisticsData object */
    public ServiceAssemblyStatisticsData() {
    }
    
    /**
     * Generate Tabular Data for this object
     * 
     * @param map
     * @return tabular data of this object
     */
    static public TabularData generateTabularData(
            Map<String /* instanceName */, ServiceAssemblyStatisticsData> map) {
        TabularData tabularData = null;
        try {
            tabularData = ServiceAssemblyStatisticsDataCreator.createTabularData(map);
        } catch (ManagementRemoteException e) {
        }
        return tabularData;
    }
    
    /**
     * Retrieves the Service Assembly Statistics Data
     * 
     * @param tabularData
     * @return Service Assembly Statistics Data
     */
    @SuppressWarnings("unchecked")
    static public Map<String /* instanceName */, ServiceAssemblyStatisticsData> retrieveDataMap(
            TabularData tabularData) {
        ServiceAssemblyStatisticsData data = null;
        Map<String /* instanceName */, ServiceAssemblyStatisticsData> map = null;
        map = new HashMap<String /* instanceName */, ServiceAssemblyStatisticsData>();
        for (Iterator dataIterator = tabularData.values().iterator(); dataIterator
                .hasNext();) {
            CompositeData compositeData = (CompositeData) dataIterator.next();
            CompositeType compositeType = compositeData.getCompositeType();
            data = new ServiceAssemblyStatisticsData();
            for (Iterator<String> itemIterator = compositeType.keySet()
                    .iterator(); itemIterator.hasNext();) {
                String item = (String) itemIterator.next();
                if (true == item.equals(ServiceAssemblyStatisticsData.INSTANCE_NAME_KEY)) {
                    String value = (String) compositeData.get(item);
                    data.setInstanceName(value);
                }
                if (true == item.equals(ServiceAssemblyStatisticsData.LAST_STARTUP_TIME_KEY)) {
                    Date value = (Date) compositeData.get(item);
                    data.setLastStartupTime(value);
                }
                if (true == item.equals(ServiceAssemblyStatisticsData.NAME_KEY)) {
                    String value = (String) compositeData.get(item);
                    data.setName(value);
                }
                if (true == item.equals(ServiceAssemblyStatisticsData.SHUTDOWN_TIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setShutdownTimeAverage(value.longValue());
                }
                if (true == item.equals(ServiceAssemblyStatisticsData.STARTUP_TIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        data.setStartupTimeAverage(value.longValue());
                    }
                }
                if (true == item.equals(ServiceAssemblyStatisticsData.STOP_TIME_AVG_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        data.setStopTimeAverage(value.longValue());
                    }
                }
                if (true == item.equals(ServiceAssemblyStatisticsData.UP_TIME_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    if(data != null) {
                        data.setUpTime(value.longValue());
                    }
                }
                if (true == item.equals(ServiceAssemblyStatisticsData.SERVICE_UNIT_STATISTICS_KEY)) {
                    CompositeData[] values = (CompositeData[]) compositeData.get(item);
                    if(values != null) {
                        ServiceUnitStatisticsData unit = null;
                        // Retrieve Service Unit data
                        for(CompositeData value : values) {
                            CompositeType valueType = value.getCompositeType();
                            unit = new ServiceUnitStatisticsData();
                            for (Iterator<String> valueIterator = valueType.keySet()
                                    .iterator(); valueIterator.hasNext();) {
                                String key = (String) valueIterator.next();
                                if (true == key.equals(ServiceUnitStatisticsData.ENDPOINTS_KEY)) {
                                    String[] datum = (String[]) value.get(key);
                                    unit.setEndpointNameList(datum);
                                }
                                if (true == key.equals(ServiceUnitStatisticsData.NAME_KEY)) {
                                    String datum = (String) value.get(key);
                                    unit.setName(datum);
                                }
                                if (true == key.equals(ServiceUnitStatisticsData.SHUTDOWN_TIME_AVG_KEY)) {
                                    Long datum = (Long) value.get(key);
                                    unit.setShutdownTimeAverage(datum.longValue());
                                }
                                if (true == key.equals(ServiceUnitStatisticsData.STARTUP_TIME_AVG_KEY)) {
                                    Long datum = (Long) value.get(key);
                                    unit.setStartupTimeAverage(datum.longValue());
                                }
                                if (true == key.equals(ServiceUnitStatisticsData.STOP_TIME_AVG_KEY)) {
                                    Long datum = (Long) value.get(key);
                                    unit.setStopTimeAverage(datum.longValue());
                                }
                            }
                            data.getServiceUnitStatisticsList().add(unit);
                        }
                    }
                }
            }
            map.put(data.getInstanceName(), data);
        }
        return map;
    }
    
    /**
     * Converts a Service Assembly Statistics Data to an XML String
     * 
     * @param map
     * @return XML string representing a Service Assembly Statistics Datum
     * @throws ManagementRemoteException
     */
    static public String convertDataMapToXML(
            Map<String /* instanceName */, ServiceAssemblyStatisticsData> map)
            throws ManagementRemoteException {
        String xmlText = null;
        try {
            xmlText = ServiceAssemblyStatisticsDataWriter.serialize(map);
        } catch (ParserConfigurationException e) {
            throw new ManagementRemoteException(e);
        } catch (TransformerException e) {
            throw new ManagementRemoteException(e);
        }
        return xmlText;
    }
    
    
    
    /**
     * @return the instanceName
     */
    public String getInstanceName() {
        return this.instanceName;
    }
    
    /**
     * @param instanceName
     *            the instanceName to set
     */
    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
    
    /**
     * @return the lastStartupTime
     */
    public Date getLastStartupTime() {
        return this.lastStartupTime;
    }
    
    /**
     * @param lastStartupTime
     *            the lastStartupTime to set
     */
    public void setLastStartupTime(Date lastStartupTime) {
        this.lastStartupTime = lastStartupTime;
    }
    
    /**
     * @return the upTime
     */
    public long getUpTime() {
        return this.upTime;
    }

    /**
     * @param upTime the upTime to set
     */
    public void setUpTime(long upTime) {
        this.upTime = upTime;
    }

    /**
     * @return the serviceAssemblyName
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * @param serviceAssemblyName
     *            the serviceAssemblyName to set
     */
    public void setName(String serviceAssemblyName) {
        this.name = serviceAssemblyName;
    }
    
    /**
     * @return the startupTimeAverage
     */
    public long getStartupTimeAverage() {
        return this.startupTimeAverage;
    }
    
    /**
     * @param startupTimeAverage
     *            the startupTimeAverage to set
     */
    public void setStartupTimeAverage(long serviceAssemblyStartTime) {
        this.startupTimeAverage = serviceAssemblyStartTime;
    }
    
    /**
     * @return the stopTimeAverage
     */
    public long getStopTimeAverage() {
        return this.stopTimeAverage;
    }
    
    /**
     * @param stopTimeAverage
     *            the stopTimeAverage to set
     */
    public void setStopTimeAverage(long serviceAssemblyStopTime) {
        this.stopTimeAverage = serviceAssemblyStopTime;
    }
    
    /**
     * @return the shutdownTimeAverage
     */
    public long getShutdownTimeAverage() {
        return this.shutdownTimeAverage;
    }
    
    /**
     * @param shutdownTimeAverage
     *            the shutdownTimeAverage to set
     */
    public void setShutdownTimeAverage(long serviceAssemblyShutdownTime) {
        this.shutdownTimeAverage = serviceAssemblyShutdownTime;
    }
    
    /**
     * @return the serviceUnitStatisticsList
     */
    public List<ServiceUnitStatisticsData> getServiceUnitStatisticsList() {
        return this.serviceUnitStatisticsList;
    }
    
    /**
     * @return the serviceUnitStatisticsArray
     */
    public ServiceUnitStatisticsData[] getServiceUnitStatisticsArray() {
        ServiceUnitStatisticsData[] result = null;
        result = toArray(this.serviceUnitStatisticsList,
                ServiceUnitStatisticsData.class);
        return result;
        
    }
    
    /**
     * @param serviceUnitStatisticsList
     *            the serviceUnitStatisticsList to set
     */
    public void setServiceUnitStatisticsList(
            List<ServiceUnitStatisticsData> serviceUnitStatisticsList) {
        this.serviceUnitStatisticsList = serviceUnitStatisticsList;
    }
    
    /**
     * @param serviceUnitStatisticsArray
     *            the serviceUnitStatisticsArray to set
     */
    public void setServiceUnitStatisticsList(
            ServiceUnitStatisticsData[] serviceUnitStatisticsArray) {
        for (ServiceUnitStatisticsData unitData : serviceUnitStatisticsArray) {
            this.serviceUnitStatisticsList.add(unitData);
        }
    }
    
    /**
     * Convert a collection to an array
     * 
     * @param collection
     * @param componentType
     * @return
     */
    @SuppressWarnings("unchecked")
    static protected <Type> Type[] toArray(Collection<Type> collection,
            Class<Type> componentType) {
        // unchecked cast
        Type[] array = (Type[]) java.lang.reflect.Array.newInstance(
                componentType, collection.size());
        int index = 0;
        for (Type value : collection) {
            array[index++] = value;
        }
        return array;
    }
    
    /**
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n  Instance Name" + "=" + this.getInstanceName());
        buffer.append("\n  Service Assembly Name" + "=" + this.getName());
        buffer.append("\n  Service Assembly Last Startup Time" + "="
                + this.getLastStartupTime());
        buffer.append("\n  Service Assembly Shutdown Time Average" + "="
                + this.getShutdownTimeAverage());
        buffer.append("\n  Service Assembly Start Time Average" + "="
                + this.getStartupTimeAverage());
        buffer.append("\n  Service Assembly Stop Time Average" + "="
                + this.getStopTimeAverage());
        buffer.append("\n  Service Assembly Up Time" + "="
                + this.getUpTime());
        if ((this.getServiceUnitStatisticsList() != null)
                && (this.getServiceUnitStatisticsList().size() > 0)) {
            buffer.append("\n  Service Unit Statistics List:");
            for (ServiceUnitStatisticsData data : this
                    .getServiceUnitStatisticsList()) {
                buffer.append(data.getDisplayString());
            }
        }
        buffer.append("\n  ========================================\n");
        return buffer.toString();
        
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}

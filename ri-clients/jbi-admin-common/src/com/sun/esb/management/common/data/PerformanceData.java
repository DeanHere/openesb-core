/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PerformanceData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.helper.PerformanceDataMapWriter;

/**
 * @author graj
 * 
 */
public class PerformanceData implements Serializable {
    
    static final long          serialVersionUID                   = -1L;
    
    /**
     * Number of measurements (or N), i.e. the number of dt-s, i.e. the number
     * of times that Measurement.begin() - end() was called.
     */
    public static final String NUMBEROFMEASUREMENTS_KEY           = "n";
    
    /** total time (ms) the sum of all dt-s */
    public static final String TOTALTIME_KEY                      = "total time (ms)";
    
    /**
     * average' (ms) (the sum of all dt-s minus the first dt) divided by N. The
     * first measurement is discounted because it typically includes
     * classloading times and distorts the results considerably. If there's only
     * one measurement, the first measurement is not discounted and the value
     * should be equal to total time.
     */
    public static final String AVERAGEWITHOUTFIRSTMEASUREMENT_KEY = "average' (ms)";
    
    /**
     * act the number of measurement objects on which begin() was called but not
     * end(). This indicates the number of active measurements. Caveat: there's
     * a problem in the this accounting when the subtopic of the measurement is
     * changed.
     */
    public static final String NUMBEROFMEASUREMENTOBJECTS_KEY     = "act";
    
    /** first the first dt */
    public static final String FIRSTMEASUREMENTTIME_KEY           = "first (ms)";
    
    /**
     * average sum of all dt-s divided by N; this does not discount the first
     * measurement
     */
    public static final String AVERAGE_KEY                        = "average (ms)";
    
    /**
     * throughput N divided by (tlast - tfirst); this is the average throughput.
     * This number is meaningful if there were no long pauses in processing.
     */
    public static final String THROUGHPUT_KEY                     = "throughput (s-1)";
    
    /**
     * tlast - tfirst the wallclock time of the first measurement's begin()
     * method is tracked as tfirst and the wallclock time of the last
     * measurement's end() method is tracked as tlast
     */
    public static final String TIMETAKEN_KEY                      = "last-first (ms)";
    
    /**
     * Load The sum of all dt-s divided by (tlast - tfirst). This is a measure
     * of concurrency: the higher the number, the greater the concurrency. In a
     * single threaded scenario this number can never exceed 1.
     */
    public static final String LOAD_KEY                           = "load";
    
    public static final String MEDIAN_KEY                         = "median (ms)";
    
    public static final String SOURCE_KEY                         = "source";
    
    /**
     * sub topic the name of the measurement specified in the second argument of
     * begin() or in setSubTopic().
     */
    public static final String SUB_TOPIC_KEY                      = "sub topic";
    
    /**
     * topic the name of the measurement specified in the first argument of
     * begin() or in setSubTopic().
     */
    public static final String TOPIC_KEY                          = "topic";
    
    double                     median;
    
    /** The name of the class that is instrumented with performance statistics gathering */
    String                     sourceClassName;

    /** The endpoint for which the statistics are reported for */
    String                     endpoint;
    
    /**
     * sub topic the name of the measurement specified in the second argument of
     * begin() or in setSubTopic().
     */
    String                     category;
    
    /**
     * average sum of all dt-s divided by N; this does not discount the first
     * measurement
     */
    double                     average;
    
    /**
     * average' (ms) (the sum of all dt-s minus the first dt) divided by N. The
     * first measurement is discounted because it typically includes
     * classloading times and distorts the results considerably. If there's only
     * one measurement, the first measurement is not discounted and the value
     * should be equal to total time.
     */
    double                     averageWithoutFirstMeasurement;
    
    /** first the first dt */
    double                     firstMeasurementTime;
    
    /**
     * Load The sum of all dt-s divided by (tlast - tfirst). This is a measure
     * of concurrency: the higher the number, the greater the concurrency. In a
     * single threaded scenario this number can never exceed 1.
     */
    double                     load;
    
    /**
     * act the number of measurement objects on which begin() was called but not
     * end(). This indicates the number of active measurements. Caveat: there's
     * a problem in the this accounting when the subtopic of the measurement is
     * changed.
     */
    int                        numberOfMeasurementObjects;
    
    /**
     * Number of measurements (or N), i.e. the number of dt-s, i.e. the number
     * of times that Measurement.begin() - end() was called.
     */
    int                        numberOfMeasurements;
    
    /**
     * throughput N divided by (tlast - tfirst); this is the average throughput.
     * This number is meaningful if there were no long pauses in processing.
     */
    double                     throughput;
    
    /**
     * tlast - tfirst the wallclock time of the first measurement's begin()
     * method is tracked as tfirst and the wallclock time of the last
     * measurement's end() method is tracked as tlast
     */
    double                     timeTaken;
    
    /** total time (ms) the sum of all dt-s */
    double                     totalTime;
    
    /**
     * Retrieves the Performance Data Map
     * 
     * @param valueObject
     * @return Performance Data Map
     */
    @SuppressWarnings("unchecked")
    public static Map<String /* Category */, PerformanceData> retrieveDataMap(
            TabularData tabularData) {
        Map<String /* Category */, PerformanceData> dataMap = new HashMap<String /* Category */, PerformanceData>();
        for (Iterator dataIterator = tabularData.values().iterator(); dataIterator
                .hasNext();) {
            CompositeData compositeData = (CompositeData) dataIterator.next();
            CompositeType compositeType = compositeData.getCompositeType();
            PerformanceData performanceData = new PerformanceData();
            for (Iterator<String> itemIterator = compositeType.keySet()
                    .iterator(); itemIterator.hasNext();) {
                String item = (String) itemIterator.next();
                if (true == item.equals(PerformanceData.AVERAGE_KEY)) {
                    Double value = (Double) compositeData.get(item);
                    performanceData.setAverage(value.doubleValue());
                }
                if (true == item
                        .equals(PerformanceData.AVERAGEWITHOUTFIRSTMEASUREMENT_KEY)) {
                    Double value = (Double) compositeData.get(item);
                    performanceData.setAverageWithoutFirstMeasurement(value
                            .doubleValue());
                }
                if (true == item
                        .equals(PerformanceData.FIRSTMEASUREMENTTIME_KEY)) {
                    Double value = (Double) compositeData.get(item);
                    performanceData
                            .setFirstMeasurementTime(value.doubleValue());
                }
                if (true == item.equals(PerformanceData.LOAD_KEY)) {
                    Double value = (Double) compositeData.get(item);
                    performanceData.setLoad(value.doubleValue());
                }
                if (true == item
                        .equals(PerformanceData.NUMBEROFMEASUREMENTOBJECTS_KEY)) {
                    Integer value = (Integer) compositeData.get(item);
                    performanceData.setNumberOfMeasurementObjects(value
                            .intValue());
                }
                if (true == item
                        .equals(PerformanceData.NUMBEROFMEASUREMENTS_KEY)) {
                    Integer value = (Integer) compositeData.get(item);
                    performanceData.setNumberOfMeasurements(value.intValue());
                }
                if (true == item.equals(PerformanceData.THROUGHPUT_KEY)) {
                    Double value = (Double) compositeData.get(item);
                    performanceData.setThroughput(value.doubleValue());
                }
                if (true == item.equals(PerformanceData.TIMETAKEN_KEY)) {
                    Double value = (Double) compositeData.get(item);
                    performanceData.setTimeTaken(value.doubleValue());
                }
                if (true == item.equals(PerformanceData.TOTALTIME_KEY)) {
                    Double value = (Double) compositeData.get(item);
                    performanceData.setTotalTime(value.doubleValue());
                }
                if (true == item.equals(PerformanceData.MEDIAN_KEY)) {
                    Double value = (Double) compositeData.get(item);
                    performanceData.setMedian(value.doubleValue());
                }
                if (true == item.equals(PerformanceData.SOURCE_KEY)) {
                    String value = (String) compositeData.get(item);
                    performanceData.setSourceClassName(value);
                }
                if (true == item.equals(PerformanceData.SUB_TOPIC_KEY)) {
                    String value = (String) compositeData.get(item);
                    performanceData.setCategory(value);
                }
                if (true == item.equals(PerformanceData.TOPIC_KEY)) {
                    String value = (String) compositeData.get(item);
                    performanceData.setEndpoint(value);
                }
            }
            dataMap.put(performanceData.getCategory(), performanceData);
        }
        
        return dataMap;
    }
    
    /**
     * Converts a Performance Data Map to an XML String
     * 
     * @param dataMap
     * @return XML string representing a performance data map
     * @throws ManagementRemoteException
     */
    public static String convertDataMapToXML(
            Map<String /* Category */, PerformanceData> dataMap)
            throws ManagementRemoteException {
        String xmlText = null;
        try {
            xmlText = PerformanceDataMapWriter.serialize(dataMap);
        } catch (ParserConfigurationException e) {
            throw new ManagementRemoteException(e);
        } catch (TransformerException e) {
            throw new ManagementRemoteException(e);
        }
        
        return xmlText;
        
    }
    
    
    
    /**
     * @return the average
     */
    public double getAverage() {
        return this.average;
    }
    
    /**
     * @return the averageWithoutFirstMeasurement
     */
    public double getAverageWithoutFirstMeasurement() {
        return this.averageWithoutFirstMeasurement;
    }
    
    /**
     * @return the firstMeasurementTime
     */
    public double getFirstMeasurementTime() {
        return this.firstMeasurementTime;
    }
    
    /**
     * @return the load
     */
    public double getLoad() {
        return this.load;
    }
    
    /**
     * @return the numberOfMeasurementObjects
     */
    public int getNumberOfMeasurementObjects() {
        return this.numberOfMeasurementObjects;
    }
    
    /**
     * @return the numberOfMeasurements
     */
    public int getNumberOfMeasurements() {
        return this.numberOfMeasurements;
    }
    
    /**
     * @return the throughput
     */
    public double getThroughput() {
        return this.throughput;
    }
    
    /**
     * @return the timeTaken
     */
    public double getTimeTaken() {
        return this.timeTaken;
    }
    
    /**
     * @return the totalTime
     */
    public double getTotalTime() {
        return this.totalTime;
    }
    
    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
    
    /**
     * @param average
     *            the average to set
     */
    public void setAverage(double average) {
        this.average = average;
    }
    
    /**
     * @param averageWithoutFirstMeasurement
     *            the averageWithoutFirstMeasurement to set
     */
    public void setAverageWithoutFirstMeasurement(
            double averageWithoutFirstMeasurement) {
        this.averageWithoutFirstMeasurement = averageWithoutFirstMeasurement;
    }
    
    /**
     * @param firstMeasurementTime
     *            the firstMeasurementTime to set
     */
    public void setFirstMeasurementTime(double firstMeasurementTime) {
        this.firstMeasurementTime = firstMeasurementTime;
    }
    
    /**
     * @param load
     *            the load to set
     */
    public void setLoad(double load) {
        this.load = load;
    }
    
    /**
     * @param numberOfMeasurementObjects
     *            the numberOfMeasurementObjects to set
     */
    public void setNumberOfMeasurementObjects(int numberOfMeasurementObjects) {
        this.numberOfMeasurementObjects = numberOfMeasurementObjects;
    }
    
    /**
     * @param numberOfMeasurements
     *            the numberOfMeasurements to set
     */
    public void setNumberOfMeasurements(int numberOfMeasurements) {
        this.numberOfMeasurements = numberOfMeasurements;
    }
    
    /**
     * @param throughput
     *            the throughput to set
     */
    public void setThroughput(double throughput) {
        this.throughput = throughput;
    }
    
    /**
     * @param timeTaken
     *            the timeTaken to set
     */
    public void setTimeTaken(double timeTaken) {
        this.timeTaken = timeTaken;
    }
    
    /**
     * @param totalTime
     *            the totalTime to set
     */
    public void setTotalTime(double totalTime) {
        this.totalTime = totalTime;
    }
    
    /**
     * @return the median
     */
    public double getMedian() {
        return this.median;
    }
    
    /**
     * @param median
     *            the median to set
     */
    public void setMedian(double median) {
        this.median = median;
    }
    
    /**
     * @return the sourceClassName
     */
    public String getSourceClassName() {
        return this.sourceClassName;
    }
    
    /**
     * @param sourceClassName
     *            the sourceClassName to set
     */
    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }
    
    /**
     * @return the endpoint
     */
    public String getEndpoint() {
        return this.endpoint;
    }
    
    /**
     * @param endpoint
     *            the endpoint to set
     */
    public void setEndpoint(String anEndpoint) {
        this.endpoint = anEndpoint;
    }
    
    /**
     * @return the category
     */
    public String getCategory() {
        return this.category;
    }
    
    /**
     * @param category
     *            the category to set
     */
    public void setCategory(String subTopic) {
        this.category = subTopic;
    }
    
    /**
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n  Category" + "=" + this.getCategory());
        buffer.append("\n  Endpoint" + "=" + this.getEndpoint());
        buffer.append("\n  Source Class Name" + "=" + this.getSourceClassName());
        buffer.append("\n  Median" + "=" + this.getMedian());
        buffer.append("\n  Average" + "=" + this.getAverage());
        buffer.append("\n  Average Without First Measurement" + "="
                + this.getAverageWithoutFirstMeasurement());
        buffer.append("\n  First Measurement Time" + "="
                + this.getFirstMeasurementTime());
        buffer.append("\n  Load" + "=" + this.getLoad());
        buffer.append("\n  Number Of Measurement Objects" + "="
                + this.getNumberOfMeasurementObjects());
        buffer.append("\n  Number Of Measurements" + "="
                + this.getNumberOfMeasurements());
        buffer.append("\n  Throughput" + "=" + this.getThroughput());
        buffer.append("\n  Time Taken" + "=" + this.getTimeTaken());
        buffer.append("\n  Total Time" + "=" + this.getTotalTime());
        buffer.append("\n  ========================================\n");
        return buffer.toString();
        
    }
    
}

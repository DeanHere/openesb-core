/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ManagementRemoteException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;

import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIRemoteException;

/**
 * A Remote Exception class that saves the cause exception stack trace in a
 * buffer for serialization. By throwing this exception on jmx server side on
 * mbean operations and attributes, we don't have to include the cause exception
 * classes on the client side. This class also allows the server side code to
 * add error code, so that the remote client can get the error code and display
 * the message from its local bundle.
 *  
 * @author graj
 */
public class ManagementRemoteException extends JBIRemoteException implements
        Serializable {
    
    static final long serialVersionUID = 4431187824092164710L;
    
    
    /**
     * @param message
     */
    public ManagementRemoteException(String message) {
        super(message);
    }
    
    /**
     * @param cause
     */
    public ManagementRemoteException(Throwable cause) {
        super(cause);
    }
    
    /**
     * @param message
     * @param cause
     */
    public ManagementRemoteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 
     * @param jbiRemoteException
     */
    public ManagementRemoteException(JBIRemoteException jbiRemoteException) {
        super(jbiRemoteException);
    }

    /**
     * Return an instance of JBIRemoteException
     * @return JBIRemoteException
     */
    public JBIRemoteException getJBIRemoteException() {
        return this;
    }
    
    /**
     * initializes the stacktrace and messages from cause
     * 
     * @param aCause
     *            a cause
     */
    public void initCauseTrace(Throwable aCause) {
        super.initCauseTrace(aCause);
    }

    /**
     * Returns the detail message string of this throwable.
     * 
     * @return the detail message string of this <tt>Throwable</tt> instance
     *         (which may be <tt>null</tt>). + the cause messages
     */
    public String getMessage() {
        return super.getMessage();
    }

    /**
     * gets the cuase trace in a string buffer
     * 
     * @return trace in a string buffer
     */
    public String[] getCauseMessageTrace() {
        return super.getCauseMessageTrace();
    }

    /**
     * gets the cuase trace in a string buffer
     * 
     * @return trace in a string buffer
     */
    public StringBuffer getCauseStackTrace() {
        return super.getCauseStackTrace();
    }

    /**
     * override method
     * 
     * @param s
     *            writer
     */
    public void printStackTrace(PrintWriter s) {
        super.printStackTrace(s);
    }

    /**
     * override method
     * 
     * @param s
     *            stream
     */
    public void printStackTrace(PrintStream s) {
        super.printStackTrace(s);
    }

    /**
     * retrieves the exception message and try to construct the jbi mgmt message
     * 
     * @return JBIManagementMessage object
     */
    public JBIManagementMessage extractJBIManagementMessage() {
        return super.extractJBIManagementMessage();
    }
    
    /**
     * filters the jmx exception and wraps the root cause user exception in the
     * JBIRemoteException
     * 
     * @param jmxEx
     *            exception
     * @return remote exception
     */
    public static ManagementRemoteException filterJmxExceptions(Exception jmxEx) {
        JBIRemoteException exception = JBIRemoteException.filterJmxExceptions(jmxEx);
        return new ManagementRemoteException(exception);
    }
    
    
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ApplicationVerificationReportWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.sun.esb.management.common.data.ApplicationVerificationReport;
import com.sun.esb.management.common.data.EndpointInformation;
import com.sun.esb.management.common.data.JavaEEVerifierReport;
import java.util.HashMap;



/**
 * Writes Application Verification report data to a String or file as XML
 * 
 * @author graj
 * 
 */
public class ApplicationVerificationReportWriter implements
        ApplicationVerificationReportXMLConstants, Serializable {
    private static final long serialVersionUID = 1L;
    
    static final String       FILE_NAME_KEY    = "ApplicationVerificationReport.xml";
    
    /** Constructor - Creates an ApplicationVerificationReportWriter */
    public ApplicationVerificationReportWriter() {
    }
    
    /**
     * 
     * @param document
     * @param directoryPath
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws Exception
     */
    public static void writeToFile(Document document, String directoryPath)
            throws TransformerConfigurationException, TransformerException,
            Exception {
        File file = new File(directoryPath);
        if ((file.isDirectory() == false) || (file.exists() == false)) {
            throw new Exception("Directory Path: " + directoryPath
                    + " is invalid.");
        }
        String fileLocation = file.getAbsolutePath() + File.separator
                + FILE_NAME_KEY;
        System.out.println("Writing out to file: " + fileLocation);
        File outputFile = new File(fileLocation);
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(outputFile);
        
        // indent the Output to make it more legible...
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
        transformer.transform(source, result);
    }
    
    /**
     * Change the contents of text file in its entirety, overwriting any
     * existing text. This style of implementation throws all exceptions to the
     * caller.
     * 
     * @param aFile
     *            is an existing file which can be written to.
     * @throws IllegalArgumentException
     *             if param does not comply.
     * @throws FileNotFoundException
     *             if the file does not exist.
     * @throws IOException
     *             if problem encountered during write.
     */
    public static void setContents(File aFile, String aContents)
            throws FileNotFoundException, IOException {
        if (aFile == null) {
            throw new IllegalArgumentException("File should not be null.");
        }
        if (!aFile.exists()) {
            aFile.createNewFile();
        }
        if (!aFile.isFile()) {
            throw new IllegalArgumentException("Should not be a directory: "
                    + aFile);
        }
        if (!aFile.canWrite()) {
            throw new IllegalArgumentException("File cannot be written: "
                    + aFile);
        }
        
        // declared here only to make visible to finally clause; generic
        // reference
        Writer output = null;
        try {
            // use buffering
            // FileWriter always assumes default encoding is OK!
            output = new BufferedWriter(new FileWriter(aFile));
            output.write(aContents);
        } finally {
            // flush and close both "aspectOutput" and its underlying FileWriter
            if (output != null) {
                output.close();
            }
        }
    }
    
    /**
     * 
     * @param performance
     *            data Map<String, PerforamanceData>
     * @return
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static String serialize(ApplicationVerificationReport report)
            throws ParserConfigurationException, TransformerException {
        Document document = null;
        PerformanceDataMapWriter writer = new PerformanceDataMapWriter();
        if (report != null) {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument(); // Create from whole cloth
            
            // ////////////////////////////////
            // <ApplicationVerificationReport>
            Element root = (Element) document
                    .createElement(APPLICATION_VERIFICATION_REPORT_KEY);
            // xmlns="http://java.sun.com/xml/ns/esb/management/ApplicationVerificationReport"
            root.setAttribute(NAMESPACE_KEY, NAMESPACE_VALUE);
            // version = "1.0"
            root.setAttribute(VERSION_KEY, VERSION_VALUE);
            
            // <ServiceAssemblyName>
            Element serviceAssemblyNameElementChild = document
                    .createElement(SERVICE_ASSEMBLY_NAME_KEY);
            if (serviceAssemblyNameElementChild != null) {
                serviceAssemblyNameElementChild.setTextContent(report
                        .getServiceAssemblyName());
            }
            // </ServiceAssemblyName>
            root.appendChild(serviceAssemblyNameElementChild);
            
            // <ServiceAssemblyDescription>
            Element serviceAssemblyDescriptionElementChild = document
                    .createElement(SERVICE_ASSEMBLY_DESCRIPTION_KEY);
            if (serviceAssemblyDescriptionElementChild != null) {
                serviceAssemblyDescriptionElementChild.setTextContent(report
                        .getServiceAssemblyDescription());
            }
            // </ServiceAssemblyDescription>
            root.appendChild(serviceAssemblyDescriptionElementChild);
            
            // <NumberOfServiceUnits>
            Element numberOfServiceUnitsElementChild = document
                    .createElement(NUMBER_OF_SERVICE_UNITS_KEY);
            if (numberOfServiceUnitsElementChild != null) {
                numberOfServiceUnitsElementChild.setTextContent(report
                        .getNumberOfServiceUnits()
                        + "");
            }
            // </NumberOfServiceUnits>
            root.appendChild(numberOfServiceUnitsElementChild);
            
            // <AllComponentsInstalled>
            Element allComponentsInstalledElementChild = document
                    .createElement(ALL_COMPONENTS_INSTALLED_KEY);
            if (allComponentsInstalledElementChild != null) {
                allComponentsInstalledElementChild.setTextContent(report
                        .areAllComponentsInstalled()
                        + "");
            }
            // </AllComponentsInstalled>
            root.appendChild(allComponentsInstalledElementChild);
            
            // <TemplateZIPID>
            Element templateZipIdElementChild = document
                    .createElement(TEMPLATE_ZIPID_KEY);
            if (templateZipIdElementChild != null) {
                templateZipIdElementChild.setTextContent(report
                        .getTemplateDirectory());
            }
            // </TemplateZIPID>
            root.appendChild(templateZipIdElementChild);
            
            // <MissingComponentsList>
            Element missingComponentsElementChild = createMissingComponentsList(
                    document, report);
            if (missingComponentsElementChild != null) {
                root.appendChild(missingComponentsElementChild);
            }
            // </MissingComponentsList>
            
            // <EndpointInformationList>
            Element endpointInformationListElementChild = createEndpointInformationList(
                    document, report);
            if (endpointInformationListElementChild != null) {
                root.appendChild(endpointInformationListElementChild);
            }
            // </EndpointInformationList>
            
            //if the list of javaee verifier reports is not empty append them
            if (report.getJavaEEVerifierReports().size() > 0)
            {
                // <JavaEEVerifierReports>
                Element javaEEVerifierReportsElementChild = createJavaEEVerifierReports(
                    document, report);
                if (javaEEVerifierReportsElementChild != null) {
                    root.appendChild(javaEEVerifierReportsElementChild);
                }
                // </JavaEEVerifierReports>
            }
            
            
            // ////////////////////////////////
            // </ApplicationVerificationReport>
            document.appendChild(root);
            // ////////////////////////////////
            
        }
        return writer.writeToString(document);
    }
    
    /**
     * Create Missing Components List
     * 
     * @param document
     * @param data
     * @return
     */
    protected static Element createMissingComponentsList(Document document,
            ApplicationVerificationReport data) {
        Element missingComponentsListElement = null;
        if ((document != null) && (data != null)) {
            // <MissingComponentsList>
            missingComponentsListElement = document
                    .createElement(MISSING_COMPONENTS_LIST_KEY);
            
            for (String missingComponentName : data.getMissingComponentsList()) {
                // <MissingComponentName>
                Element missingComponentNameElementChild = document
                        .createElement(MISSING_COMPONENT_NAME_KEY);
                if (missingComponentNameElementChild != null) {
                    missingComponentNameElementChild
                            .setTextContent(missingComponentName);
                }
                // </MissingComponentName>
                missingComponentsListElement
                        .appendChild(missingComponentNameElementChild);
            }
        }
        return missingComponentsListElement;
    }
    
    /**
     * Create Endpoint Information List
     * 
     * @param document
     * @param data
     * @return
     */
    protected static Element createEndpointInformationList(Document document,
            ApplicationVerificationReport data) {
        Element endpointInformationListElement = null;
        if ((document != null) && (data != null)) {
            // <EndpointInformationList>
            endpointInformationListElement = document
                    .createElement(ENDPOINT_INFORMATION_LIST_KEY);
            
            for (EndpointInformation endpointInformation : data
                    .getEndpointInformationList()) {
                // <Endpoint>
                Element endpointElementChild = createEndpointInformation(
                        document, endpointInformation);
                if (endpointElementChild != null) {
                    endpointInformationListElement
                            .appendChild(endpointElementChild);
                }
                // </Endpoint>
            }
        }
        return endpointInformationListElement;
    }
    
    /**
     * Create Endpoint Information
     * 
     * @param document
     * @param data
     * @return
     */
    protected static Element createEndpointInformation(Document document,
            EndpointInformation data) {
        Element endpointInformationElement = null;
        if ((document != null) && (data != null)) {
            // <Endpoint>
            endpointInformationElement = document.createElement(ENDPOINT_KEY);
            
            // <EndpointName>
            Element endpointNameElementChild = document
                    .createElement(ENDPOINT_NAME_KEY);
            if (endpointNameElementChild != null) {
                endpointNameElementChild.setTextContent(data.getEndpointName());
            }
            // </EndpointName>
            endpointInformationElement.appendChild(endpointNameElementChild);
            
            // <ServiceUnitName>
            Element serviceUnitNameElementChild = document
                    .createElement(SERVICE_UNIT_NAME_KEY);
            if (serviceUnitNameElementChild != null) {
                serviceUnitNameElementChild.setTextContent(data
                        .getServiceUnitName());
            }
            // </ServiceUnitName>
            endpointInformationElement.appendChild(serviceUnitNameElementChild);
            
            // <ComponentName>
            Element componentNameElementChild = document
                    .createElement(COMPONENT_NAME_KEY);
            if (componentNameElementChild != null) {
                componentNameElementChild.setTextContent(data
                        .getComponentName());
            }
            // </ComponentName>
            endpointInformationElement.appendChild(componentNameElementChild);
            
            // <Status>
            Element statusElementChild = document.createElement(STATUS_KEY);
            if (statusElementChild != null) {
                statusElementChild.setTextContent(data.getStatus());
            }
            // </Status>
            endpointInformationElement.appendChild(statusElementChild);
         
            // <MissingApplicationVariables>
            Element missingAppVarsElementChild = document.createElement(MISSING_APPVARS_KEY);
            
            String[] missingAppVars = data.getMissingApplicationVariables();
            if (missingAppVars != null && missingAppVars.length > 0)
            {
                for (int i=0; i<missingAppVars.length; i++) 
                {
                    // <ApplicationVariable>
                    Element appVariableElementChild = document
                        .createElement(MISSING_APPVAR_NAME_KEY);
                    if (appVariableElementChild != null) 
                    {
                        appVariableElementChild
                            .setTextContent(missingAppVars[i]);
                    }
                    //</ApplicationVariable>
                    missingAppVarsElementChild
                        .appendChild(appVariableElementChild);
                }
            }
            
            //</MissingApplicationVariables>
            endpointInformationElement.appendChild(missingAppVarsElementChild);
            
            // <MissingApplicationConfigurations>
            Element missingAppConfigsElementChild = document.createElement(MISSING_APPCONFIGS_KEY);
            
            String[] missingAppConfigs = data.getMissingApplicationConfigurations();
            if (missingAppConfigs != null && missingAppConfigs.length > 0)
            {
                for (int i=0; i<missingAppConfigs.length; i++) 
                {
                    // <ApplicationConfiguration>
                    Element appConfigurationElementChild = document
                        .createElement(MISSING_APPCONFIG_NAME_KEY);
                    if (appConfigurationElementChild != null) 
                    {
                        appConfigurationElementChild
                            .setTextContent(missingAppConfigs[i]);
                    }
                    //</ApplicationConfiguration>
                    missingAppConfigsElementChild
                        .appendChild(appConfigurationElementChild);
                }   
            }
            
            //</MissingApplicationConfigurations>
            endpointInformationElement.appendChild(missingAppConfigsElementChild);
                     
        }
        return endpointInformationElement;
    }
    
    
    /**
     * Creates JavaEEVerifierReports, list of all 
     * JavaEEVerifier Reports
     * 
     * @param document
     * @param data
     * @return
     */
    protected static Element createJavaEEVerifierReports(
            Document document,
            ApplicationVerificationReport data) 
    {
        Element javaEEVerifierReportsListElement = null;
        if ((document != null) && (data != null)) {
            // <JavaEEVerifierReports>
            javaEEVerifierReportsListElement = document
                    .createElement(JAVAEE_VERIFIER_REPORTS_LIST_KEY);
            
            for (JavaEEVerifierReport javaEEReport : data
                    .getJavaEEVerifierReports()) {
                // <JavaEEVerifierReport>
                Element reportElementChild = createJavaEEVerifierReport(
                        document, javaEEReport);
                if (reportElementChild != null) {
                    javaEEVerifierReportsListElement
                            .appendChild(reportElementChild);
                }
                // </JavaEEVerifierReport>
            }
        }
        return javaEEVerifierReportsListElement;
    }
    
    /**
     * Create JavaEEVerifierReport
     * 
     * @param document
     * @param data
     * @return
     */
    protected static Element createJavaEEVerifierReport(
            Document document,
            JavaEEVerifierReport data) 
    {
        Element javaEEReportElement = null;
        if ((document != null) && (data != null)) 
        {
            // <JavaEEVerifierReport>
            javaEEReportElement = document.createElement(JAVAEE_VERIFIER_REPORT_KEY);
            
            // <ServiceUnitName>
            Element serviceUnitNameChild = document.createElement(JAVAEE_VERIFIER_SERVICE_UNIT_NAME);
            if (serviceUnitNameChild != null) {
                serviceUnitNameChild.setTextContent(data.getServiceUnitName());
            }
            // </ServiceUnitName>
            javaEEReportElement.appendChild(serviceUnitNameChild);
            
            // <ReportTable>
            Element reportTableChildElement = document
                    .createElement(JAVAEE_VERIFIER_REPORT_TABLE_KEY);
            
            ArrayList<JavaEEVerifierReport.JavaEEReportItem> reportTable = 
                    data.getJavaEEVerifierReport();
            for (JavaEEVerifierReport.JavaEEReportItem item : reportTable) 
            {

                // <ReportItem>
                Element reportItemChildElement = document
                    .createElement(JAVAEE_VERIFIER_REPORT_ITEM_KEY);                
                
                HashMap<String, String> fields = item.getReportItems();
                for(String key : fields.keySet())
                {
                    Element element = document.createElement(JAVAEE_VERIFIER_REPORT_ITEM_CONTENT_KEY);
                    if (element != null) 
                    {
                        element.setAttribute(JAVAEE_VERIFIER_REPORT_ITEM_CONTENT_NAME_KEY, key);
                        element.setAttribute(JAVAEE_VERIFIER_REPORT_ITEM_CONTENT_VALUE_KEY, fields.get(key));
                    }
                    reportItemChildElement.appendChild(element);
                }
         
                reportTableChildElement.appendChild(reportItemChildElement);
                // </ReportItem>
            }
            javaEEReportElement.appendChild(reportTableChildElement);                            
        }
        return javaEEReportElement;
    }
    
            
           
    /**
     * @param document
     * @return
     * @throws TransformerException
     */
    protected String writeToString(Document document)
            throws TransformerException {
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        
        // indent the aspectOutput to make it more legible...
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
        
        return result.getWriter().toString();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // String uri = "C:/test/schema/verification/instance.xml";
        String uri = "C:/test/schema/verification/ApplicationVerificationReport.xml";
        try {
            ApplicationVerificationReport report = ApplicationVerificationReportReader
                    .parseFromFile(uri);
            System.out.println(report.getDisplayString());
            String content = ApplicationVerificationReportWriter
                    .serialize(report);
            System.out.println(content);
            ApplicationVerificationReportWriter.setContents(new File(uri),
                    content);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}

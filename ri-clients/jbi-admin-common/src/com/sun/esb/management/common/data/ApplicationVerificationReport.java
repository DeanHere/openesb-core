/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ApplicationVerificationReport.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.helper.VerificationDataCreator;

/**
 * Represents the application verification report
 * 
 * @author graj
 */
public class ApplicationVerificationReport implements Serializable {
    static final long                 serialVersionUID        = -1L;
    
    String                            serviceAssemblyName;
    
    String                            serviceAssemblyDescription;
    
    int                               numberOfServiceUnits;
    
    boolean                           allComponentsInstalled;
    
    List<String /* ComponentName */> missingComponentsList   = new ArrayList<String /* ComponentName */>();
    
    List<EndpointInformation>         endpointInformationList = new ArrayList<EndpointInformation>();
    
    String                            templateZipId;
    
    List<JavaEEVerifierReport>        javaEEVerifierReports   = new ArrayList<JavaEEVerifierReport>();
    
    /** Constructor - Creates an ApplicationVerificationReport */
    public ApplicationVerificationReport() {
    }
    
    /**
     * Constructor - Creates an ApplicationVerificationReport
     * 
     * @param compositeData
     */
    public ApplicationVerificationReport(CompositeData compositeData) {
        ApplicationVerificationReport report = retrieveApplicationVerificationReport(compositeData);
        
        this.serviceAssemblyName = report.getServiceAssemblyName();
        this.serviceAssemblyDescription = report
                .getServiceAssemblyDescription();
        this.numberOfServiceUnits = report.getNumberOfServiceUnits();
        this.allComponentsInstalled = report.areAllComponentsInstalled();
        this.missingComponentsList = report.getMissingComponentsList();
        this.endpointInformationList = report.getEndpointInformationList();
        this.templateZipId = report.getTemplateZipId();
        this.javaEEVerifierReports = report.getJavaEEVerifierReports();
    }
    
    /**
     * Generate Composite Data for this object
     * @return composite data of this object
     */
    public CompositeData generateCompositeData() {
        CompositeData compositeData = null;
        try {
            compositeData = VerificationDataCreator.createCompositeData(this);
        } catch (ManagementRemoteException e) {
        }
        return compositeData;
    }
    
    /**
     * @return the serviceAssemblyName
     */
    public String getServiceAssemblyName() {
        return this.serviceAssemblyName;
    }
    
    /**
     * @param serviceAssemblyName
     *            the serviceAssemblyName to set
     */
    public void setServiceAssemblyName(String serviceAssemblyName) {
        this.serviceAssemblyName = serviceAssemblyName;
    }
    
    /**
     * @return the serviceAssemblyDescription
     */
    public String getServiceAssemblyDescription() {
        return this.serviceAssemblyDescription;
    }
    
    /**
     * @param serviceAssemblyDescription
     *            the serviceAssemblyDescription to set
     */
    public void setServiceAssemblyDescription(String serviceAssemblyDescription) {
        this.serviceAssemblyDescription = serviceAssemblyDescription;
    }
    
    /**
     * @return the numberOfServiceUnits
     */
    public int getNumberOfServiceUnits() {
        return this.numberOfServiceUnits;
    }
    
    /**
     * @param numberOfServiceUnits
     *            the numberOfServiceUnits to set
     */
    public void setNumberOfServiceUnits(int numberOfServiceUnits) {
        this.numberOfServiceUnits = numberOfServiceUnits;
    }
    
    /**
     * @return the allComponentsInstalled
     */
    public boolean areAllComponentsInstalled() {
        return this.allComponentsInstalled;
    }
    
    /**
     * @param allComponentsInstalled
     *            the allComponentsInstalled to set
     */
    public void setAllComponentsInstalled(boolean allComponentsInstalled) {
        this.allComponentsInstalled = allComponentsInstalled;
    }
    
    /**
     * @return the missingComponentsList
     */
    public List<String /* ComponentName */> getMissingComponentsList() {
        return this.missingComponentsList;
    }
    
    /**
     * 
     * @param collection
     * @param componentType
     * @return
     */
    @SuppressWarnings("unchecked")
    static protected <Type> Type[] toArray(Collection<Type> collection,
            Class<Type> componentType) {
        // unchecked cast
        Type[] array = (Type[]) java.lang.reflect.Array.newInstance(componentType,
                collection.size());
        int index = 0;
        for (Type value : collection) {
            array[index++] = value;
        }
        return array;
    }
    
    /**
     * @return the missingComponentsArray
     */
    public String[] getMissingComponentsArray() {
        String[] result = toArray(this.missingComponentsList, String.class);
        return result;
    }
    
    /**
     * @param missingComponentsList
     *            the missingComponentsList to set
     */
    public void setMissingComponentsList(String[] missingComponentsArray) {
        for (String componentName : missingComponentsArray) {
            this.missingComponentsList.add(componentName);
        }
    }
    
    /**
     * @param missingComponentsList
     *            the missingComponentsList to set
     */
    public void setMissingComponentsList(
            List<String /* ComponentName */> missingComponentsArray) {
        this.missingComponentsList = missingComponentsArray;
    }
    
    /**
     * @return the endpointInformationList
     */
    public List<EndpointInformation> getEndpointInformationList() {
        return this.endpointInformationList;
    }
    
    /**
     * @param endpointInformationList
     *            the endpointInformationList to set
     */
    public void setEndpointInformationList(
            EndpointInformation[] endpointInformationArray) {
        for (EndpointInformation endpointInformation : endpointInformationArray) {
            this.endpointInformationList.add(endpointInformation);
        }
    }
    
    /**
     * @param endpointInformationList
     *            the endpointInformationList to set
     */
    public void setEndpointInformationList(
            List<EndpointInformation> endpointInformationArray) {
        this.endpointInformationList = endpointInformationArray;
    }
    
    
    /**
     * Sets the JavaEEVerifierReports
     * @param javaEEVerifierReports the javaeeverifier reports
     */
    public void setJavaEEVerifierReports(
            List<JavaEEVerifierReport> javaEEVerifierReports) 
    {
        this.javaEEVerifierReports = javaEEVerifierReports;
    }
    
    /**
     * Gets the JavaEEVerifierReports
     * @return javaEEVerifierReport javaEEVerifier report
     */
    public List<JavaEEVerifierReport> getJavaEEVerifierReports()
    {
        return javaEEVerifierReports;
    }
        
    
    /**
     * @return the templateZipId
     */
    public String getTemplateZipId() {
        return this.templateZipId;
    }
    
    /**
     * @param templateZipId
     *            the templateZipId to set
     */
    public void setTemplateZipId(String templateZIPID) {
        this.templateZipId = templateZIPID;
    }
    
    /**
     * @return the templateZipId
     */
    public String getTemplateDirectory() {
        return this.templateZipId;
    }
    
    /**
     * @param templateZipId
     *            the templateZipId to set
     */
    public void setTemplateDirectory(String templateZIPID) {
        this.templateZipId = templateZIPID;
    }
    
    /**
     * Return a displayable string of values
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n  Service Assembly Name" + "="
                + this.getServiceAssemblyName());
        buffer.append("\n  Service Assembly Description" + "="
                + this.getServiceAssemblyDescription());
        buffer.append("\n  Number of Service Units" + "="
                + this.getNumberOfServiceUnits());
        buffer.append("\n  Are All Components Installed" + "="
                + this.areAllComponentsInstalled());
        buffer.append("\n  Template Directory or TemplateZipID" + "="
                + this.getTemplateDirectory());
        for (String missingComponentName : this.getMissingComponentsList()) {
            buffer.append("\n    missing component name" + "="
                    + missingComponentName);
        }
        for (EndpointInformation information : this
                .getEndpointInformationList()) {
            buffer.append(information.getDisplayString());
        }
        for (JavaEEVerifierReport verifierReport : this.getJavaEEVerifierReports()) 
        {
            buffer.append(verifierReport.getDisplayString());
        }        
        return buffer.toString();
    }
    
    /**
     * retrieve report object from composite data
     * 
     * @param compositeData
     * @return
     */
    @SuppressWarnings("unchecked")
    protected static ApplicationVerificationReport retrieveApplicationVerificationReport(
            CompositeData compositeData) {
        ApplicationVerificationReport report = new ApplicationVerificationReport();
        // //////////////////////////////////////////
        // CompositeType of verification report:
        // String - "ServiceAssemblyName",
        // String - "ServiceAssemblyDescription",
        // Integer - "NumServiceUnits",
        // Boolean - "AllComponentsInstalled",
        // String[] - "MissingComponentsList",
        // CompositeData[] - "EndpointInfo",
        // String - "TemplateZIPID"
        // CompositeData[] - "JavaEEVerifierReport"
        // //////////////////////////////////////////
        CompositeType compositeType = compositeData.getCompositeType();
        for (Iterator<String> itemIterator = compositeType.keySet().iterator(); itemIterator
                .hasNext();) {
            String item = (String) itemIterator.next();
            if (true == item.equals("ServiceAssemblyName")) {
                report.setServiceAssemblyName((String) compositeData.get(item));
            }
            if (true == item.equals("ServiceAssemblyDescription")) {
                report.setServiceAssemblyDescription((String) compositeData
                        .get(item));
            }
            if (true == item.equals("NumServiceUnits")) {
                Integer value = (Integer) compositeData.get(item);
                report.setNumberOfServiceUnits(value.intValue());
            }
            if (true == item.equals("AllComponentsInstalled")) {
                Boolean value = (Boolean) compositeData.get(item);
                report.setAllComponentsInstalled(value.booleanValue());
            }
            if (true == item.equals("TemplateZIPID")) {
                report.setTemplateZipId((String) compositeData.get(item));
            }
            if (true == item.equals("MissingComponentsList")) {
                String[] missingComponentNames = (String[]) compositeData
                        .get(item);
                report.setMissingComponentsList(missingComponentNames);
            }
            if (true == item.equals("EndpointInfo")) {
                List<EndpointInformation> list = retrieveEndpointInformationList((CompositeData[]) compositeData
                        .get(item));
                report.setEndpointInformationList(list);
            }
            if (true == item.equals("JavaEEVerifierReport")) {
                List<JavaEEVerifierReport> list = 
                        retrieveJavaEEVerifierReports((CompositeData[]) compositeData.get(item));
                report.setJavaEEVerifierReports(list);
            }            
        }
        return report;
    }
    
    /**
     * Retrieve endpoing infoemation from composite data
     * 
     * @param compositeDataArray
     * @return
     */
    @SuppressWarnings("unchecked")
    protected static List<EndpointInformation> retrieveEndpointInformationList(
            CompositeData[] compositeDataArray) {
        List<EndpointInformation> result = new ArrayList<EndpointInformation>();
        // //////////////////////////////////////////
        // CompositeType of each EndpointInfo:
        // String - "EndpointName",
        // String - "ServiceUnitName",
        // String - "ComponentName",
        // String - "Status"
        // //////////////////////////////////////////
        
        for (CompositeData compositeData : compositeDataArray) {
            EndpointInformation information = null;
            if (compositeData.values().size() > 0) {
                information = new EndpointInformation();
            } else {
                // If no values, skip parsing this element
                continue;
            }
            CompositeType compositeType = compositeData.getCompositeType();
            for (Iterator<String> itemIterator = compositeType.keySet()
                    .iterator(); itemIterator.hasNext();) {
                String item = (String) itemIterator.next();
                if (true == item.equals("EndpointName")) {
                    information.setEndpointName((String) compositeData
                            .get(item));
                }
                if (true == item.equals("ServiceUnitName")) {
                    information.setServiceUnitName((String) compositeData
                            .get(item));
                }
                if (true == item.equals("ComponentName")) {
                    information.setComponentName((String) compositeData
                            .get(item));
                }
                if (true == item.equals("Status")) {
                    information.setStatus((String) compositeData.get(item));
                }
                if (true == item.equals("MissingApplicationVariables")) {
                    information.setMissingApplicationVariables((String[]) compositeData.get(item));
                }
                if (true == item.equals("MissingApplicationConfigurations")) {
                    information.setMissingApplicationConfigurations((String[]) compositeData.get(item));
                }                
            }
            if (information != null) {
                result.add(information);
            }
        }
        
        return result;
    }
    
    /**
     * This method is used to retrieve the list of JavaEEVerification reports
     * from the verifier report
     * @param compositeDataArray
     * @return
     */
    protected static List<JavaEEVerifierReport> retrieveJavaEEVerifierReports(
            CompositeData[] compositeDataArray) {
        List<JavaEEVerifierReport> result = new ArrayList<JavaEEVerifierReport>();
        // //////////////////////////////////////////
        // CompositeType of each EndpointInfo:
        // String      - "ServiceUnitName",
        // TabularData - "JavaEEVerifierReport",
        // //////////////////////////////////////////
        
        for (CompositeData compositeData : compositeDataArray) 
        {
            JavaEEVerifierReport report = null;
            if (compositeData.values().size() > 0) 
            {
                report = new JavaEEVerifierReport();
            } else {
                // If no values, skip parsing this element
                continue;
            }
            CompositeType compositeType = compositeData.getCompositeType();
            for (Iterator<String> itemIterator = compositeType.keySet()
                    .iterator(); itemIterator.hasNext();) 
            {
                String item = (String) itemIterator.next();
                if (true == item.equals("ServiceUnitName")) 
                {
                    report.setServiceUnitName((String) compositeData.get(item));
                }
                if (true == item.equals("JavaEEVerifierReport")) 
                {
                    report.setJavaEEVerifierReport((TabularData) compositeData.get(item));
                }
            }
            if (report != null) {
                result.add(report);
            }
        }
        
        return result;
    }
    
    /**
     * This method is used to get the CompositeType for 
     * JavaEEVerifierReport
     * @return CompositeType the type for JavaEEVerifier Report
     */
    public CompositeType getJavaEEVerifierReportCompositeType()
    throws OpenDataException
    {
        if (javaEEVerifierReports != null && javaEEVerifierReports.size() > 0 )
        {
            return javaEEVerifierReports.get(0).getCompositeType();
        }
        return null;
    }    
    
}

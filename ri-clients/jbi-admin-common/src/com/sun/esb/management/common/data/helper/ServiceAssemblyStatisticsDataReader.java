/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyStatisticsDataReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sun.esb.management.common.data.ServiceAssemblyStatisticsData;
import com.sun.esb.management.common.data.ServiceUnitStatisticsData;

/**
 * @author graj
 *
 */
public class ServiceAssemblyStatisticsDataReader extends DefaultHandler implements
        ServiceAssemblyStatisticsDataXMLConstants, Serializable {
    static final long                                          serialVersionUID = -1L;
    
    // Private members needed to parse the XML document
    
    // keep track of parsing
    private boolean                                            parsingInProgress;
    
    // keep track of QName
    private Stack<String>                                      qNameStack       = new Stack<String>();
    
    private String                                             serviceAssemblyStatisticsDataListVersion;
    
    private String                                             serviceUnitStatisticsDataListVersion;

    private ServiceAssemblyStatisticsData                                  data;
    
    private Map<String /* instanceName */, ServiceAssemblyStatisticsData>  dataMap;
    
    private ServiceUnitStatisticsData                                  unitData;

    private List<ServiceUnitStatisticsData> unitDataList;

    private List<String /* endpointName */>                 endpointNameList;
    
    
    /** Constructor - creates a new instance of ServiceAssemblyStatisticsDataReader */
    public ServiceAssemblyStatisticsDataReader() {
    }
    
    /**
     * @return the dataMap
     */
    public Map<String /* instanceName */, ServiceAssemblyStatisticsData> getServiceAssemblyStatisticsDataMap() {
        return this.dataMap;
    }  
    
    
    /**
     * Start of document processing.
     * 
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void startDocument() throws SAXException {
        parsingInProgress = true;
        qNameStack.removeAllElements();
    }
    
    /**
     * End of document processing.
     * 
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void endDocument() throws SAXException {
        parsingInProgress = false;
        // We have encountered the end of the document. Do any processing that
        // is desired, for example dump all collected element2 values.
    }

    /**
     * Process the new element.
     * 
     * @param uri
     *            is the Namespace URI, or the empty string if the element has
     *            no Namespace URI or if Namespace processing is not being
     *            performed.
     * @param localName
     *            is the The local name (without prefix), or the empty string if
     *            Namespace processing is not being performed.
     * @param qName
     *            is the qualified name (with prefix), or the empty string if
     *            qualified names are not available.
     * @param attributes
     *            is the attributes attached to the element. If there are no
     *            attributes, it shall be an empty Attributes object.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        if (qName != null) {
            if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.ASSEMBLY_STATISTICS_DATA_LIST_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if ((attributes != null) && (attributes.getLength() > 0)) {
                    String namespace = attributes.getValue(NAMESPACE_KEY);
                    // //////////////////////////////////////////////////////
                    // Read serviceAssemblyStatisticsDataListVersion attribute and 
                    // ensure you store the right serviceAssemblyStatisticsDataListVersion
                    // of the report map list
                    // //////////////////////////////////////////////////////
                    this.serviceAssemblyStatisticsDataListVersion = attributes
                            .getValue(VERSION_KEY);
                    if ((serviceAssemblyStatisticsDataListVersion != null)
                            && (ServiceAssemblyStatisticsDataXMLConstants.ASSEMBLY_VERSION_VALUE.equals(serviceAssemblyStatisticsDataListVersion))) {
                        this.dataMap = new HashMap<String /* instanceName */, ServiceAssemblyStatisticsData>();
                    } else {
                        // Invalid serviceAssemblyStatisticsDataListVersion.
                        // Not storing it
                    }
                }
            } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.ASSEMBLY_STATISTICS_DATA_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if (this.dataMap != null) {
                    this.data = new ServiceAssemblyStatisticsData();
                }
            } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.UNIT_STATISTICS_DATA_LIST_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if ((attributes != null) && (attributes.getLength() > 0)) {
                    String namespace = attributes.getValue(NAMESPACE_KEY);
                    // //////////////////////////////////////////////////////
                    // Read serviceAssemblyStatisticsDataListVersion attribute and 
                    // ensure you store the right serviceAssemblyStatisticsDataListVersion
                    // of the report map list
                    // //////////////////////////////////////////////////////
                    this.serviceUnitStatisticsDataListVersion = attributes
                            .getValue(VERSION_KEY);
                    if ((serviceUnitStatisticsDataListVersion != null)
                            && (ServiceAssemblyStatisticsDataXMLConstants.UNIT_VERSION_VALUE.equals(serviceUnitStatisticsDataListVersion))) {
                        if ((this.dataMap != null) && (this.data != null)) {
                            this.unitDataList = new ArrayList<ServiceUnitStatisticsData>();
                        }
                        
                    }
                } else {
                    // Invalid serviceAssemblyStatisticsDataListVersion.
                    // Not storing it
                }
            } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.UNIT_STATISTICS_DATA_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if ((this.dataMap != null) 
                        && (this.data != null) 
                        && (this.unitDataList != null)) {
                    this.unitData = new ServiceUnitStatisticsData();
                }
            } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_ENDPOINT_NAME_LIST_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if ((this.dataMap != null) 
                        && (this.data != null) 
                        && (this.unitDataList != null) 
                        && (this.unitData != null)) {
                    this.endpointNameList = new ArrayList<String /*endpointName*/>();
                }
            }
            // Keep track of QNames
            qNameStack.push(qName);
        }
    }
    
    /**
     * Process the character report for current tag.
     * 
     * @param ch
     *            are the element's characters.
     * @param start
     *            is the start position in the character array.
     * @param length
     *            is the number of characters to use from the character array.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        String qName;
        String chars = new String(ch, start, length);
        // Get current QName
        qName = (String) qNameStack.peek();
        if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.INSTANCE_NAME_KEY)) {
            if ((this.dataMap != null) && (this.data != null)) {
                this.data.setInstanceName(chars);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_NAME_KEY)) {
            if ((this.dataMap != null) && (this.data != null)) {
                this.data.setName(chars);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_LAST_STARTUP_TIME_KEY)) {
            Date value = null;
            // Mon Dec 03 02:19:09 PST 2007
            // Make a SimpleDateFormat for toString()'s output. This
            // has short (text) date, a space, short (text) month, a space,
            // 2-digit date, a space, hour (0-23), minute, second, a space,
            // short timezone, a final space, and a long year.
            SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");            
            try {
                value = format.parse(chars);
            } catch (ParseException e) {
            }
            if ((this.dataMap != null) && (this.data != null)) {
                this.data.setLastStartupTime(value);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_SHUTDOWN_TIME_AVERAGE_KEY)) {
            long value = Long.valueOf(chars).longValue(); 
            if ((this.dataMap != null) && (this.data != null)) {
                this.data.setShutdownTimeAverage(value);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_STARTUP_TIME_AVERAGE_KEY)) {
            long value = Long.valueOf(chars).longValue(); 
            if ((this.dataMap != null) && (this.data != null)) {
                this.data.setStartupTimeAverage(value);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_STOP_TIME_AVERAGE_KEY)) {
            Long value = Long.valueOf(chars).longValue(); 
            if ((this.dataMap != null) && (this.data != null)) {
                this.data.setStopTimeAverage(value);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_UP_TIME_KEY)) {
            Long value = Long.valueOf(chars).longValue(); 
            if ((this.dataMap != null) && (this.data != null)) {
                this.data.setUpTime(value);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_NAME_KEY)) {
            if ((this.dataMap != null) 
                    && (this.data != null) 
                    && (this.unitDataList != null) 
                    && (this.unitData != null)) {
                this.unitData.setName(chars);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_SHUTDOWN_TIME_AVERAGE_KEY)) {
            long value = Long.valueOf(chars).longValue(); 
            if ((this.dataMap != null) 
                    && (this.data != null) 
                    && (this.unitDataList != null) 
                    && (this.unitData != null)) {
                this.unitData.setShutdownTimeAverage(value);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_STARTUP_TIME_AVERAGE_KEY)) {
            long value = Long.valueOf(chars).longValue(); 
            if ((this.dataMap != null) 
                    && (this.data != null) 
                    && (this.unitDataList != null) 
                    && (this.unitData != null)) {
                this.unitData.setStartupTimeAverage(value);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_STOP_TIME_AVERAGE_KEY)) {
            long value = Long.valueOf(chars).longValue(); 
            if ((this.dataMap != null) 
                    && (this.data != null) 
                    && (this.unitDataList != null) 
                    && (this.unitData != null)) {
                this.unitData.setStopTimeAverage(value);
            }
        } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_ENDPOINT_NAME_KEY)) {
            if ((this.dataMap != null) 
                    && (this.data != null) 
                    && (this.unitDataList != null) 
                    && (this.unitData != null) 
                    && (this.endpointNameList != null)) {
                this.endpointNameList.add(chars);
            }
        }
    }
    
    /**
     * Process the end element tag.
     * 
     * @param uri
     *            is the Namespace URI, or the empty string if the element has
     *            no Namespace URI or if Namespace processing is not being
     *            performed.
     * @param localName
     *            is the The local name (without prefix), or the empty string if
     *            Namespace processing is not being performed.
     * @param qName
     *            is the qualified name (with prefix), or the empty string if
     *            qualified names are not available.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        // Pop QName, since we are done with it
        qNameStack.pop();
        if (qName != null) {
            if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.ASSEMBLY_STATISTICS_DATA_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) && (this.data != null)) {
                    this.dataMap.put(this.data.getInstanceName(), this.data);
                    this.data = null;
                    this.unitDataList = null;
                    this.unitData = null;
                    this.endpointNameList = null;
                }
            } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.UNIT_STATISTICS_DATA_LIST_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) && (this.data != null) && (this.unitDataList != null)) {
                    this.data.setServiceUnitStatisticsList(this.unitDataList);
                    this.unitDataList = null;
                    this.unitData = null;
                    this.endpointNameList = null;
                }
            } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.UNIT_STATISTICS_DATA_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) 
                        && (this.data != null) 
                        && (this.unitDataList != null) 
                        && (this.unitData != null)) {
                    this.unitDataList.add(unitData);
                    this.unitData = null;
                    this.endpointNameList = null;
                }
            } else if (qName.endsWith(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_ENDPOINT_NAME_LIST_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) 
                        && (this.data != null) 
                        && (this.unitDataList != null) 
                        && (this.unitData != null)
                        && (this.endpointNameList != null)) {
                    this.unitData.setEndpointNameList(this.endpointNameList);
                    this.endpointNameList = null;
                }
            }
        }
    }
    
    /**
     * 
     * @param rawXMLData
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ServiceAssemblyStatisticsData> parseFromXMLData(String rawXMLData)
            throws MalformedURLException, ParserConfigurationException,
            SAXException, URISyntaxException, IOException {
        // System.out.println("Parsing file: "+uriString);
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the XML Document InputStream
        Reader reader = new StringReader(rawXMLData);
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(reader);
        
        // Parse the aspectInput XML document stream, using my event handler
        ServiceAssemblyStatisticsDataReader parser = new ServiceAssemblyStatisticsDataReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getServiceAssemblyStatisticsDataMap();
        
    }
    
    /**
     * 
     * @param fileName
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ServiceAssemblyStatisticsData> parseFromFile(String fileName)
            throws MalformedURLException, ParserConfigurationException,
            SAXException, URISyntaxException, IOException {
        File file = new File(fileName);
        return parseFromFile(file);
    }
    
    /**
     * 
     * @param fileName
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ServiceAssemblyStatisticsData> parseFromFile(File file)
            throws MalformedURLException, ParserConfigurationException,
            SAXException, URISyntaxException, IOException {
        
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the URI and XML Document InputStream
        InputStream inputStream = new FileInputStream(file);
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(inputStream);
        
        // Parse the aspectInput XML document stream, using my event handler
        ServiceAssemblyStatisticsDataReader parser = new ServiceAssemblyStatisticsDataReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getServiceAssemblyStatisticsDataMap();
    }
    
    /**
     * 
     * @param uriString
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ServiceAssemblyStatisticsData> parseFromURI(String uriString)
            throws MalformedURLException, ParserConfigurationException,
            SAXException, URISyntaxException, IOException {
        URI uri = new URI(uriString);
        return parseFromURI(uri);
    }
    
    /**
     * 
     * @param uri
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, ServiceAssemblyStatisticsData> parseFromURI(URI uri)
            throws MalformedURLException, ParserConfigurationException,
            SAXException, URISyntaxException, IOException {
        
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the URI and XML Document InputStream
        InputStream inputStream = uri.toURL().openStream();
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(inputStream);
        
        // Parse the aspectInput XML document stream, using my event handler
        ServiceAssemblyStatisticsDataReader parser = new ServiceAssemblyStatisticsDataReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getServiceAssemblyStatisticsDataMap();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/assemblystatistics/ServiceAssemblyStatisticsData.xml";
        try {
            Map<String /* instanceName */, ServiceAssemblyStatisticsData> map = null;
            map = ServiceAssemblyStatisticsDataReader.parseFromFile(uri);
            for (String instanceName : map.keySet()) {
                System.out.println(map.get(instanceName).getDisplayString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}

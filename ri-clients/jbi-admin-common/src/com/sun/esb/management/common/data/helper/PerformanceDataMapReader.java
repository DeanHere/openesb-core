/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PerformanceDataMapReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sun.esb.management.common.data.PerformanceData;

/**
 * Parses Performance Measurement data from an XML String or file
 *  
 * @author graj
 */
public class PerformanceDataMapReader extends DefaultHandler implements
        PerformanceDataXMLConstants, Serializable {
    
    static final long                                    serialVersionUID = -1L;
    
    // Private members needed to parse the XML document
    
    // keep track of parsing
    private boolean                                      parsingInProgress;
    
    // keep track of QName
    private Stack<String>                                qNameStack       = new Stack<String>();
    
    private PerformanceData                              data;
    
    private Map<String /* category */, PerformanceData> dataMap;
    
    private String                                       performanceDataListVersion;
    
    /** Constructor - creates a new instance of PerformanceDataMapReader */
    public PerformanceDataMapReader() {
    }
    
    /**
     * @return the dataMap
     */
    public Map<String /* category */, PerformanceData> getPerformanceDataMap() {
        return this.dataMap;
    }
    
    /**
     * Start of document processing.
     * 
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void startDocument() throws SAXException {
        parsingInProgress = true;
        qNameStack.removeAllElements();
    }
    
    /**
     * End of document processing.
     * 
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void endDocument() throws SAXException {
        parsingInProgress = false;
        // We have encountered the end of the document. Do any processing that
        // is desired, for example dump all collected element2 values.
        
    }
    
    /**
     * Process the new element.
     * 
     * @param uri
     *            is the Namespace URI, or the empty string if the element has
     *            no Namespace URI or if Namespace processing is not being
     *            performed.
     * @param localName
     *            is the The local name (without prefix), or the empty string if
     *            Namespace processing is not being performed.
     * @param qName
     *            is the qualified name (with prefix), or the empty string if
     *            qualified names are not available.
     * @param attributes
     *            is the attributes attached to the element. If there are no
     *            attributes, it shall be an empty Attributes object.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        if (qName != null) {
            if (qName.endsWith(PERFORMANCE_MEASUREMENT_DATA_LIST_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if ((attributes != null) && (attributes.getLength() > 0)) {
                    String namespace = attributes.getValue(NAMESPACE_KEY);
                    ////////////////////////////////////////////////////////
                    // Read performanceDataListVersion attribute and ensure you store the right
                    // performanceDataListVersion of the data map list
                    ////////////////////////////////////////////////////////
                    this.performanceDataListVersion = attributes.getValue(VERSION_KEY);
                    if ((performanceDataListVersion != null) && (VERSION_VALUE.equals(performanceDataListVersion))) {
                        this.dataMap = new HashMap<String /* category */, PerformanceData>();
                    } else {
                        // Invalid performanceDataListVersion. Not storing it
                    }
                }
                
            } else if (qName.endsWith(PERFORMANCE_MEASUREMENT_DATA_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if (this.dataMap != null) {
                    this.data = new PerformanceData();
                }
            }
            // Keep track of QNames
            qNameStack.push(qName);
        }
    }
    
    /**
     * Process the character data for current tag.
     * 
     * @param ch
     *            are the element's characters.
     * @param start
     *            is the start position in the character array.
     * @param length
     *            is the number of characters to use from the character array.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        String qName;
        String chars = new String(ch, start, length);
        // Get current QName
        qName = (String) qNameStack.peek();
        if (qName.endsWith(CATEGORY_KEY)) {
            if (this.data != null) {
                this.data.setCategory(chars);
            }
        } else if (qName.endsWith(ENDPOINT_KEY)) {
            if (this.data != null) {
                this.data.setEndpoint(chars);
            }
        } else if (qName.endsWith(SOURCE_CLASS_NAME_KEY)) {
            if (this.data != null) {
                this.data.setSourceClassName(chars);
            }
        } else if (qName.endsWith(NUMBER_OF_MEASUREMENT_OBJECTS_KEY)) {
            if (this.data != null) {
                int numberOfMeasurementObjects = Integer.valueOf(chars)
                        .intValue();
                this.data
                        .setNumberOfMeasurementObjects(numberOfMeasurementObjects);
            }
        } else if (qName.endsWith(NUMBER_OF_MEASUREMENTS_KEY)) {
            if (this.data != null) {
                int numberOfMeasurements = Integer.valueOf(chars).intValue();
                this.data.setNumberOfMeasurements(numberOfMeasurements);
            }
        } else if (qName.endsWith(AVERAGE_KEY)) {
            if (this.data != null) {
                double average = Double.valueOf(chars).doubleValue();
                this.data.setAverage(average);
            }
        } else if (qName.endsWith(AVERAGE_WITHOUT_FIRST_MEASUREMENT_KEY)) {
            if (this.data != null) {
                double averageWithoutFirstMeasurement = Double.valueOf(chars)
                        .doubleValue();
                this.data
                        .setAverageWithoutFirstMeasurement(averageWithoutFirstMeasurement);
            }
        } else if (qName.endsWith(FIRST_MEASUREMENT_TIME_KEY)) {
            if (this.data != null) {
                double firstMeasurementTime = Double.valueOf(chars)
                        .doubleValue();
                this.data.setFirstMeasurementTime(firstMeasurementTime);
            }
        } else if (qName.endsWith(LOAD_KEY)) {
            if (this.data != null) {
                double load = Double.valueOf(chars).doubleValue();
                this.data.setLoad(load);
            }
        } else if (qName.endsWith(MEDIAN_KEY)) {
            if (this.data != null) {
                double median = Double.valueOf(chars).doubleValue();
                this.data.setMedian(median);
            }
        } else if (qName.endsWith(THROUGHPUT_KEY)) {
            if (this.data != null) {
                double throughput = Double.valueOf(chars).doubleValue();
                this.data.setThroughput(throughput);
            }
        } else if (qName.endsWith(TIME_TAKEN_KEY)) {
            if (this.data != null) {
                double timeTaken = Double.valueOf(chars).doubleValue();
                this.data.setTimeTaken(timeTaken);
            }
        } else if (qName.endsWith(TOTAL_TIME_KEY)) {
            if (this.data != null) {
                double totalTime = Double.valueOf(chars).doubleValue();
                this.data.setTotalTime(totalTime);
            }
        }
    }
    
    /**
     * Process the end element tag.
     * 
     * @param uri
     *            is the Namespace URI, or the empty string if the element has
     *            no Namespace URI or if Namespace processing is not being
     *            performed.
     * @param localName
     *            is the The local name (without prefix), or the empty string if
     *            Namespace processing is not being performed.
     * @param qName
     *            is the qualified name (with prefix), or the empty string if
     *            qualified names are not available.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        // Pop QName, since we are done with it
        qNameStack.pop();
        if (qName != null) {
            if (qName.endsWith(PERFORMANCE_MEASUREMENT_DATA_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) && (this.data != null)) {
                    this.dataMap.put(this.data.getCategory(), this.data);
                    this.data = null;
                }
            }
        }
    }
    
    /**
     * 
     * @param rawXMLData
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* category */, PerformanceData> parseFromXMLData(
            String rawXMLData) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        // System.out.println("Parsing file: "+uriString);
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the XML Document InputStream
        Reader reader = new StringReader(rawXMLData);
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(reader);
        
        // Parse the aspectInput XML document stream, using my event handler
        PerformanceDataMapReader parser = new PerformanceDataMapReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getPerformanceDataMap();
        
    }
    
    /**
     * 
     * @param fileName
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* category */, PerformanceData> parseFromFile(
            String fileName) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        File file = new File(fileName);
        return parseFromFile(file);
    }
    
    /**
     * 
     * @param fileName
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* category */, PerformanceData> parseFromFile(
            File file) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the URI and XML Document InputStream
        InputStream inputStream = new FileInputStream(file);
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(inputStream);
        
        // Parse the aspectInput XML document stream, using my event handler
        PerformanceDataMapReader parser = new PerformanceDataMapReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getPerformanceDataMap();
    }
    
    /**
     * 
     * @param uriString
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* category */, PerformanceData> parseFromURI(
            String uriString) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        URI uri = new URI(uriString);
        return parseFromURI(uri);
    }
    
    /**
     * 
     * @param uri
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* category */, PerformanceData> parseFromURI(
            URI uri) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the URI and XML Document InputStream
        InputStream inputStream = uri.toURL().openStream();
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(inputStream);
        
        // Parse the aspectInput XML document stream, using my event handler
        PerformanceDataMapReader parser = new PerformanceDataMapReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getPerformanceDataMap();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // String uri = "C:/test/performance/instance.xml";
        String uri = "C:/test/performance/PerformanceMeasurementDataList.xml";
        try {
            Map<String, PerformanceData> map = PerformanceDataMapReader
                    .parseFromFile(uri);
            for (String category : map.keySet()) {
                System.out.println(map.get(category).getDisplayString());
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}

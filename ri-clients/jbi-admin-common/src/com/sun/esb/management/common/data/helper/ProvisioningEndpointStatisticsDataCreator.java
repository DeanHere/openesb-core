/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ProvisioningEndpointStatisticsDataCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.IEndpointStatisticsData;
import com.sun.esb.management.common.data.ProvisioningEndpointStatisticsData;

/**
 * @author graj
 *
 */
public class ProvisioningEndpointStatisticsDataCreator {
    /**  table index   */
    static String[] STATS_TABLE_INDEX = new String[] { "InstanceName" };
    
    /**
     * 
     */
    public ProvisioningEndpointStatisticsDataCreator() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * 
     * @param data
     * @return
     * @throws ManagementRemoteException
     */
    public static TabularData createTabularData(
            Map<String /* instanceName */, IEndpointStatisticsData> map)
            throws ManagementRemoteException {
        Set<String> instanceNames = map.keySet();
        
        TabularType endpointStatsTableType = null;
        TabularData endpointsStatsTable = null;
        CompositeData[] endpointStats = new CompositeData[instanceNames.size()];
        int index = 0;
        for (String instanceName : instanceNames) {
            IEndpointStatisticsData value = map.get(instanceName);
            ProvisioningEndpointStatisticsData data = (ProvisioningEndpointStatisticsData) value;
            endpointStats[index++] = composeProviderEndpointStats(data);
        }
        // the tabular type could be constructed only after we have the endpoint
        // stats composite type so we need atleast one entry
        try {
            if (index > 0 && endpointStats[0] != null) {
                endpointStatsTableType = new TabularType("EndpointStats",
                        "Endpoint Statistic Information", endpointStats[0]
                                .getCompositeType(), STATS_TABLE_INDEX);
                endpointsStatsTable = new TabularDataSupport(
                        endpointStatsTableType);
                
                for (int innerIndex = 0; innerIndex < index; innerIndex++) {
                    endpointsStatsTable.put(endpointStats[innerIndex]);
                }
            }
        } catch (OpenDataException e) {
            throw new ManagementRemoteException(e);
        }
        
        return endpointsStatsTable;
    }
    
    /**
     * This method is used to compose provider endpoint stats
     * @param instanceName
     * @param commonValues values common to both provider and consumer
     * @param providerValues values specific to provider
     * @param ojcStats table of performance data
     * @return CompositeData composite data
     *
     */
    protected static CompositeData composeProviderEndpointStats(
            ProvisioningEndpointStatisticsData data)
            throws ManagementRemoteException {
        
        TabularType ojcStatsType = null;
        if (data.getCategoryToPerformanceDataMap() != null) {
            //ojcStatsType = ojcStats.getTabularType();
        }
        
        ArrayList<String> providerItemNames = new ArrayList<String>();
        providerItemNames.add("InstanceName");
        providerItemNames.add("ActivationTime");
        providerItemNames.add("UpTime");
        providerItemNames.add("NumActiveExchanges");
        providerItemNames.add("NumReceivedRequests");
        providerItemNames.add("NumSentReplies");
        providerItemNames.add("NumReceivedDONEs");
        providerItemNames.add("NumSentDONEs");
        providerItemNames.add("NumReceivedFaults");
        providerItemNames.add("NumSentFaults");
        providerItemNames.add("NumReceivedErrors");
        providerItemNames.add("NumSentErrors");
        providerItemNames.add("ComponentName");
        providerItemNames.add("ME-ResponseTime-Avg");
        providerItemNames.add("ME-ComponentTime-Avg");
        providerItemNames.add("ME-DeliveryChannelTime-Avg");
        providerItemNames.add("ME-MessageServiceTime-Avg");
        if (ojcStatsType != null) {
            providerItemNames.add("PerformanceMeasurements");
        }
        
        ArrayList<String> providerItemDescriptions = new ArrayList<String>();
        providerItemDescriptions.add("Instance Name");
        providerItemDescriptions.add("Time taken for activation");
        providerItemDescriptions.add("Endpoint upTime");
        providerItemDescriptions.add("Number of Active Exchanges");
        providerItemDescriptions.add("Number of Received Requests");
        providerItemDescriptions.add("Number of Sent Replies");
        providerItemDescriptions.add("Number of Received DONEs");
        providerItemDescriptions.add("Number of Sent DONEs");
        providerItemDescriptions.add("Number of Received Faults");
        providerItemDescriptions.add("Number of Sent Faults");
        providerItemDescriptions.add("Number of Received Errors");
        providerItemDescriptions.add("Number of Sent Errors");
        providerItemDescriptions.add("Component Name");
        providerItemDescriptions.add("Message Exchange ResponseTime Avg in ns");
        providerItemDescriptions
                .add("Message Exchange ComponentTime Avg in ns");
        providerItemDescriptions
                .add("Message Exchange DeliveryChannelTime Avg in ns");
        providerItemDescriptions
                .add("Message Exchange MessageServiceTime  Avg in ns");
        if (ojcStatsType != null) {
            providerItemDescriptions
                    .add("Performance Measurements recorded by OJC Components");
        }
        
        ArrayList<OpenType> providerItemTypes = new ArrayList<OpenType>();
        providerItemTypes.add(SimpleType.STRING);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.STRING);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        providerItemTypes.add(SimpleType.LONG);
        if (ojcStatsType != null) {
            providerItemTypes.add(ojcStatsType);
        }
        ;
        
        ArrayList<Object> providerItemValues = new ArrayList<Object>();
        providerItemValues.add(data.getInstanceName());
        providerItemValues.add(data.getActivationTime()); //ActivationTimestamp
        providerItemValues.add(new Long(Long.MIN_VALUE)); //todo calculate uptime
        providerItemValues.add(data.getNumberOfActiveExchanges()); //ActiveExchanges
        providerItemValues.add(data.getNumberOfReceivedRequests()); //ReceiveRequest
        providerItemValues.add(data.getNumberOfSentReplies()); //SendReply
        providerItemValues.add(data.getNumberOfReceivedDones()); //ReceiveDONE
        providerItemValues.add(data.getNumberOfSentDones()); //SendDONE
        providerItemValues.add(data.getNumberOfReceivedFaults()); //ReceiveFault
        providerItemValues.add(data.getNumberOfSentFaults()); //SendFault
        providerItemValues.add(data.getNumberOfReceivedErrors()); //ReceiveERROR                        
        providerItemValues.add(data.getNumberOfSentErrors()); //SendERROR
        providerItemValues.add(data.getComponentName()); //OwningChannel
        providerItemValues.add(data.getMessageExchangeResponseTimeAverage()); //Response Time
        providerItemValues.add(data
                .getMessageExchangeDeliveryChannelTimeAverage()); //ChannelTimeAvg
        providerItemValues.add(data.getMessageExchangeComponentTimeAverage()); //ComponenTimeAvg
        providerItemValues.add(data.getMessageExchangeServiceTimeAverage()); //NMRTimeAvg
        if (ojcStatsType != null) {
            //providerItemValues.add(ojcStats);
        }
        
        try {
            return new CompositeDataSupport(new CompositeType(
                    "ProviderEndpointStats", "Provider Endpoint Statistics",
                    (String[]) providerItemNames.toArray(new String[] {}),
                    (String[]) providerItemDescriptions
                            .toArray(new String[] {}),
                    (SimpleType[]) providerItemTypes
                            .toArray(new SimpleType[] {})),
                    (String[]) providerItemNames.toArray(new String[] {}),
                    (Object[]) providerItemValues.toArray(new Object[] {}));
        } catch (OpenDataException ode) {
            throw new ManagementRemoteException(ode);
        }
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}

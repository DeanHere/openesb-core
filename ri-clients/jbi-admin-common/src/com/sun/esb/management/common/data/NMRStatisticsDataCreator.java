/**
 * 
 */
package com.sun.esb.management.common.data;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Set;

import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.helper.NMRStatisticsDataReader;

/**
 * @author graj
 *
 */
public class NMRStatisticsDataCreator {
    
    /** nmr CompositeType item names   */
    static String[] NMR_STATS_ITEM_NAMES        = { "InstanceName",
            "ListActiveChannels", "ListActiveEndpoints" };
    
    /** nmr CompositeType item descriptions   */
    static String[] NMR_STATS_ITEM_DESCRIPTIONS = { "Instance Name",
            "List of active delivery channels", "List of active endpoints" };
    
    /** nmr stats table index   */
    static String[] NMR_STATS_TABLE_INDEX       = new String[] { "InstanceName" };
    
    /** nmr stats active channels */
    static String   NMR_ACTIVE_CHANNELS         = "ActiveChannels";
    
    /** nmr stats active endpoints */
    static String   NMR_ACTIVE_ENDPOINTS        = "ActiveEndpoints";
    
    /**
     * 
     */
    public NMRStatisticsDataCreator() {
    }
    
    /**
     * 
     * @param data
     * @return
     * @throws ManagementRemoteException
     */
    public static TabularData createTabularData(Map<String /* instanceName */, NMRStatisticsData> map)
            throws ManagementRemoteException {
        TabularData nmrStatsTable = null;
        
        try {
            ArrayType activeChannelsArray = new ArrayType(1, SimpleType.STRING);
            ArrayType activeEndpointsArray = new ArrayType(1, SimpleType.STRING);
            
            OpenType[] nmrStatsItemTypes = { SimpleType.STRING,
                    activeChannelsArray, activeEndpointsArray };
            CompositeType NMRStatsEntriesType = new CompositeType(
                    "NMRStatistics", "NMR Statistics", NMR_STATS_ITEM_NAMES,
                    NMR_STATS_ITEM_DESCRIPTIONS, nmrStatsItemTypes);
            
            TabularType nmrStatsType = new TabularType("NMRStats",
                    "NMR Statistic Information", NMRStatsEntriesType,
                    NMR_STATS_TABLE_INDEX);
            
            nmrStatsTable = new TabularDataSupport(nmrStatsType);
            
            Set<String> instances = map.keySet();
            for(String instanceName : instances) {
                NMRStatisticsData data = map.get(instanceName);
                nmrStatsTable.put(getNMRStatistics(NMRStatsEntriesType, data));
            }
            
        } catch (OpenDataException e) {
            throw new ManagementRemoteException(e);
        }
        return nmrStatsTable;
        
    }
    
    /**
     * This method is used to provide query framework statistics from the
     * given NMRStatisticsData
     * 
     * @param NMRStatsEntriesType
     * @param data
     * @return
     * @throws ManagementRemoteException
     */
    protected static CompositeData getNMRStatistics(
            CompositeType NMRStatsEntriesType, NMRStatisticsData data)
            throws ManagementRemoteException {
        try {
            Object[] values = { data.getInstanceName(),
                    data.getActiveChannelsArray(),
                    data.getActiveEndpointsArray() };
            
            return new CompositeDataSupport(NMRStatsEntriesType,
                    NMR_STATS_ITEM_NAMES, values);
            
        } catch (Exception ex) {
            throw new ManagementRemoteException(ex);
        }
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/nmrstatistics/NMRStatisticsData.xml";
        try {
            
            Map<String /* instanceName */, NMRStatisticsData> map = null;
            map = NMRStatisticsDataReader.parseFromFile(uri);
            for (String instanceName : map.keySet()) {
                System.out.println(map.get(instanceName).getDisplayString());
            }
            TabularData data = NMRStatisticsDataCreator.createTabularData(map);
            System.out.println(data);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ManagementRemoteException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}

/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BaseServiceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.management.base.services;

import java.io.File;
import java.io.Serializable;

import javax.management.MBeanServerConnection;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.ui.common.FileTransferManager;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;

/**
 * Common methods for uploading and downloading files
 * 
 * @author graj
  */
public abstract class BaseServiceImpl extends AbstractServiceImpl implements
        Serializable {
    
    private static final long serialVersionUID = -1L;
    
    /** last uploaded object reference */
    protected Object          mLastUploadId    = null;
    
    /** Constructor - Constructs a new instance of AbstractServiceImpl */
    public BaseServiceImpl() {
        super(null, false);
    }
    
    /**
     * Constructor - Constructs a new instance of AbstractServiceImpl
     * 
     * @param serverConnection
     */
    public BaseServiceImpl(MBeanServerConnection serverConnection) {
        super(serverConnection, false);
    }
    
    /**
     * Constructor - Constructs a new instance of AbstractServiceImpl
     * 
     * @param serverConnection
     * @param isRemoteConnection
     */
    public BaseServiceImpl(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        super(serverConnection, isRemoteConnection);
    }
    
    /**
     * this method uploads the local file to the server using
     * FileTransferManager and returns the path to the file on the server's file
     * system which can be used to do any operations on the file/file data on
     * the server side. This is used to upload components ( se, bc), shared
     * library or service assembly archive files to the server for remote
     * install/deploy commands.
     * 
     * @param localFilePath
     *            file path on the local file system.
     * @return String for file path of the uploaded file on the server file
     *         system
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    protected String uploadFile(String localFilePath)
            throws ManagementRemoteException {
        File uploadFile = new File(localFilePath);

        if (!uploadFile.exists())
        {
            String jbiMgmtMsg = JBIResultXmlBuilder.createJbiResultXml(
                    getI18NBundle(), "jbi.ui.client.remote.file.upload.not.exists",
                    new String [] { uploadFile.getAbsolutePath() }, null);
            throw new ManagementRemoteException(new Exception(jbiMgmtMsg));
        }

        String name = uploadFile.getName();
        
        try {
            FileTransferManager ftpManager = new FileTransferManager(this
                    .getMBeanServerConnection());
            String uploadedFilePath = ftpManager.uploadArchive(uploadFile);
            this.mLastUploadId = ftpManager.getLastUploadId();
            return uploadedFilePath;
        } catch (Exception ex) {
            String jbiMgmtMsg = JBIResultXmlBuilder.createJbiResultXml(
                    getI18NBundle(), "jbi.ui.client.remote.file.upload.error",
                    new Object[] { name }, ex);
            throw new ManagementRemoteException(new Exception(jbiMgmtMsg));
        } finally {
            
        }
    }
    
    /**
     * this method tries to remove the last uploaded file on the server. It
     * ingores all errrors if it can not remove file as the server will take
     * care of removing the files during restarts. Since this call can be called
     * any where we need to have local instance of ftmgr to talk to server.
     */
    protected void removeUploadedFile() {
        if (this.mLastUploadId == null) {
            return;
        }
        
        try {
            FileTransferManager ftpManager = new FileTransferManager(this
                    .getMBeanServerConnection());
            ftpManager.removeUploadedArchive(this.mLastUploadId);
            this.mLastUploadId = null;
        } catch (Exception ex) {
            // ignore any exceptions as removing the uploaded file is optional.
        } finally {
            this.mLastUploadId = null;
        }
    }
    
    /**
     * this method downloads a file from the server
     * @param dir  dir to download the file into
     * @param serverFilePath the file path in the server
     * @returns String the name of the downloaded file
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    public String downloadFile(String dir, String serverFilePath)
            throws ManagementRemoteException {
        try {
            FileTransferManager ftpManager = new FileTransferManager(this
                    .getMBeanServerConnection());
            String fileName = new File(serverFilePath).getName();
            ftpManager.downloadArchive(serverFilePath, new File(dir, fileName));
            return fileName;
        } catch (Exception ex) {
            String jbiMgmtMsg = JBIResultXmlBuilder.createJbiResultXml(
                    getI18NBundle(),
                    "jbi.ui.client.remote.file.download.error",
                    new Object[] { serverFilePath }, ex);
            throw new ManagementRemoteException(new Exception(jbiMgmtMsg));
        } finally {
            
        }
    }
}

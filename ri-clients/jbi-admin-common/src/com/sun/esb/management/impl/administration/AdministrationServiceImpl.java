/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdministrationServiceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.management.impl.administration;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import com.sun.esb.management.api.administration.AdministrationService;
import com.sun.esb.management.base.services.AbstractServiceImpl;
import com.sun.esb.management.common.ManagementRemoteException;

/**
 * Defines client operations for common administration services for the client.
 * 
 * @author graj
 */
public class AdministrationServiceImpl extends AbstractServiceImpl implements
        Serializable, AdministrationService {
    
    static final long serialVersionUID = -1L;
    
    /** Constructor - Constructs a new instance of AdministrationServiceImpl */
    public AdministrationServiceImpl() {
        super(null, false);
    }
    
    /**
     * Constructor - Constructs a new instance of AdministrationServiceImpl
     * 
     * @param serverConnection
     */
    public AdministrationServiceImpl(MBeanServerConnection serverConnection) {
        super(serverConnection, false);
    }
    
    /**
     * Constructor - Constructs a new instance of AdministrationServiceImpl
     * 
     * @param serverConnection
     * @param isRemoteConnection
     */
    public AdministrationServiceImpl(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        super(serverConnection, isRemoteConnection);
    }
    
    /**
     * Returns a map of target names to an array of target instance names.
     * In the case cluster targets, the key contains the cluster target name, 
     * and the the value contains an array of the "target instance" names.
     * If it is not a cluster target, the key contains the targetName, and 
     * the value is null.
     * 
     * @return map of target names to array of target instance names
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    public Map<String /*targetName*/, String[] /*targetInstanceNames*/> listTargetNames() throws ManagementRemoteException {
        return super.listTargetNames();
    }
    
    /**
     * Retrieve runtime version information
     * @return map of targetName to Properties elements detailing version info
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    public Map<String /*targetName*/, Properties /*version information*/> getRuntimeDetails() throws ManagementRemoteException {
        Map<String /*targetName*/, Properties /*version information*/> result = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = null;
        String[] signature = null;
        result = (Map<String /*targetName*/, Properties /*version information*/>) this.invokeMBeanOperation(mbeanName,
                "getRuntimeDetails", params, signature);
        
        return result;
    }
    
    /**
     * Checks to see if the Target (server, cluster) is up or down.
     * 
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return true if Target is up, false if not
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#isTargetUp(java.lang.String)
     */
    public boolean isTargetUp(String targetName)
    throws ManagementRemoteException {
        boolean result = false;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Boolean resultObject = null;
        Object[] params = new Object[1];
        params[0] = targetName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        resultObject = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isTargetUp", params, signature);
        if (resultObject != null) {
            result = resultObject.booleanValue();
        }
        
        return result;
    }
    
    
    /**
     * Retrieve the Component Installation descriptor for the server target.
     * 
     * The Installation descriptor will never be different for a component name
     * in a JBI system because there's only one component allowed with that same
     * name. Therefore, this implies a registry lookup for the server target.
     * 
     * @param Component
     *            name as a string
     * @return the Component Installation descriptor as a string
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getComponentInstallationDescriptor(java.lang.String)
     */
    public String getComponentInstallationDescriptor(String componentName)
            throws ManagementRemoteException {
        String result = null;
        Object resultObject = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[1];
        params[0] = componentName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "getComponentInstallationDescriptor", params, signature);
        
        if (resultObject != null) {
            result = resultObject.toString();
        }
        return result;
    }
    
    /**
     * Returns the jbi.xml Deployment Descriptor for a Service Assembly.
     * 
     * @param the
     *            name of the Service Assembly
     * @return the jbi.xml deployment descriptor of the archive
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getServiceAssemblyDeploymentDescriptor(java.lang.String)
     */
    public String getServiceAssemblyDeploymentDescriptor(
            String serviceAssemblyName) throws ManagementRemoteException {
        Object resultObject = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[1];
        params[0] = serviceAssemblyName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "getServiceAssemblyDeploymentDescriptor", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * Returns the jbi.xml Deployment Descriptor for a Service Unit.
     * 
     * @param the
     *            name of the Service Assembly
     * @param the
     *            name of the Service Unit
     * @return the jbi.xml deployment descriptor of the archive
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getServiceUnitDeploymentDescriptor(java.lang.String,
     *      java.lang.String)
     */
    public String getServiceUnitDeploymentDescriptor(
            String serviceAssemblyName, String serviceUnitName)
            throws ManagementRemoteException {
        Object resultObject = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = serviceUnitName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "getServiceUnitDeploymentDescriptor", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * Returns the jbi.xml Installation Descriptor for a Shared Library.
     * 
     * @param the
     *            name of the Shared Library
     * @return the jbi.xml deployment descriptor of the archive
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getSharedLibraryInstallationDescriptor(java.lang.String)
     */
    public String getSharedLibraryInstallationDescriptor(
            String sharedLibraryName) throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[1];
        params[0] = sharedLibraryName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getSharedLibraryInstallationDescriptor", params, signature);
        
        return result;
    }
    
    /**
     * Checks to see if the JBI Runtime is enabled.
     * 
     * @return true if JBI Runtime Framework is available, false if not
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isJBIRuntimeEnabled()
     */
    public boolean isJBIRuntimeEnabled() throws ManagementRemoteException {
        boolean result = false;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        result = this.isRegistered(mbeanName);
        return result;
    }
    
    /**
     * check for ServiceEngine
     * 
     * @param the
     *            name of the ServiceEngine or Binding Component
     * @return true if it is a Service Engine else false.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isServiceEngine(java.lang.String)
     */
    public boolean isServiceEngine(String componentName)
            throws ManagementRemoteException {
        boolean result = false;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean resultObject = null;
        
        Object[] params = new Object[1];
        params[0] = componentName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        resultObject = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isServiceEngine", params, signature);
        
        if (resultObject != null) {
            result = resultObject.booleanValue();
        }
        
        return result;
    }
    
    /**
     * check for BindingComponent
     * 
     * @param the
     *            name of the ServiceEngine or Binding Component
     * @return true if it is a Binding Component else false.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isBindingComponent(java.lang.String)
     */
    public boolean isBindingComponent(String componentName)
            throws ManagementRemoteException {
        boolean result = false;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean resultObject = null;
        
        Object[] params = new Object[1];
        params[0] = componentName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        resultObject = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isBindingComponent", params, signature);
        
        if (resultObject != null) {
            result = resultObject.booleanValue();
        }
        
        return result;
    }
    
    /**
     * Get the list of consuming endpoints
     * 
     * @param componentName
     * @param targetName
     * @return Returns the consumingEndpoints.
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getConsumingEndpoints(java.lang.String,
     *      String)
     */
    public String[] getConsumingEndpoints(String componentName,
            String targetName) throws ManagementRemoteException {
        String[] result = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        result = (String[]) this.invokeMBeanOperation(mbeanName,
                "getConsumingEndpoints", params, signature);
        
        return result;
    }
    
    /**
     * Get the list of provisioning endpoints
     * 
     * @param componentName
     * @param targetName
     * @return Returns the provisioningEndpoints.
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getProvisioningEndpoints(java.lang.String,
     *      String)
     */
    public String[] getProvisioningEndpoints(String componentName,
            String targetName) throws ManagementRemoteException {
        String[] result = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        result = (String[]) this.invokeMBeanOperation(mbeanName,
                "getProvisioningEndpoints", params, signature);
        
        return result;
    }
    
    /**
     * Retrieves the primary WSDL associated with the specified endpoint.
     * 
     * @param componentName
     * @param endpoint
     * @param targetName
     * @return primary WSDL associated with the endpoint
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getWSDLDefinition(String,String,String)
     */
    public String getWSDLDefinition(String componentName, String endpoint,
            String targetName) throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = endpoint;
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getWSDLDefinition", params, signature);
        
        return result;
    }
    
    /**
     * Retrieves the WSDL or XSD associated with the specified endpoint and
     * targetNamespace
     * 
     * @param componentName
     * @param endpoint
     * @param targetNamespace
     * @param targetName
     * @return wsdl or xsd
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getWSDLImportedResource(String,
     *      String, String, String)
     */
    public String getWSDLImportedResource(String componentName,
            String endpoint, String targetNamespace, String targetName)
            throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = endpoint;
        params[2] = targetNamespace;
        params[3] = targetName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getWSDLImportedResource", params, signature);
        
        return result;
    }
    
    /**
     * Gets the component state
     * 
     * @param componentName
     * @param targetName
     * @return the state of the component
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getComponentState(java.lang.String,
     *      java.lang.String)
     */
    public String getComponentState(String componentName, String targetName)
            throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getComponentState", params, signature);
        
        return result;
    }
    
    /**
     * Gets the component type
     * 
     * @param componentName
     * @param targetName
     * @return the type of the component (binding-component, service-engine, or
     *         shared-library)
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getComponentType(java.lang.String,
     *      java.lang.String)
     */
    public String getComponentType(String componentName, String targetName)
            throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getComponentType", params, signature);
        
        return result;
    }
    
    /**
     * Gets the Service Assembly state
     * 
     * @param serviceAssemblyName
     * @param targetName
     * @return the state of the Service Assembly
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#getServiceAssemblyState(java.lang.String,
     *      java.lang.String)
     */
    public String getServiceAssemblyState(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        String result = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getServiceAssemblyState", params, signature);
        
        return result;
    }
    
    /**
     * Checks to see if a component/library is installed on a target
     * 
     * @param componentName
     * @param targetName
     * @return true if installed, false if not
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isJBIComponentInstalled(java.lang.String,
     *      java.lang.String)
     */
    public boolean isJBIComponentInstalled(String componentName,
            String targetName) throws ManagementRemoteException {
        boolean result = false;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isJBIComponentInstalled", params, signature);
        
        if (resultObject != null) {
            result = resultObject.booleanValue();
        }
        
        return result;
    }
    
    /**
     * Checks to see if a Service Assembly is deployed on a target
     * 
     * @param serviceAssemblyName
     * @param targetName
     * @return true if deployed, false if not
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.administration.AdministrationService#isServiceAssemblyDeployed(java.lang.String,
     *      java.lang.String)
     */
    public boolean isServiceAssemblyDeployed(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        boolean result = false;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isServiceAssemblyDeployed", params, signature);
        
        if (resultObject != null) {
            result = resultObject.booleanValue();
        }
        
        return result;
    }
    
    /**
     * Get the instance name of the platform's administration server.  If the
     * platform does not provide a separate administration server, then this 
     * method returns the name of the local instance.
     * @return instance name of the administration server
     * @throws ManagementRemoteException
     */
    public String getAdminServerName() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        String result = null;
        Object[] params = null;
        String[] signature = null;
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getAdminServerName", params, signature);
        return result;
    }
    
    
    /**
     * Determine whether this instance is the administration server instance.
     * @return <CODE>true</CODE> if this instance is the administration server,
     * <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isAdminServer() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean result = Boolean.FALSE;
        Object[] params = null;
        String[] signature = null;
        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isAdminServer", params, signature);
        return result.booleanValue();
    }
    
    
    /**
     * Get the name of this instance.
     * @return the name of this server instance.
     * @throws ManagementRemoteException
     */
    public String getInstanceName() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        String result = null;
        Object[] params = null;
        String[] signature = null;
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getInstanceName", params, signature);
        return result;
    }
    
    
    /**
     * Determine if the specified instance is up.
     * @return true if the instance is up and running, false otherwise
     * @throws ManagementRemoteException
     */
    public boolean isInstanceUp(String instanceName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean result = Boolean.FALSE;
        Object[] params = new Object[1];
        params[0] = instanceName;
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isInstanceUp", params, signature);
        return result.booleanValue();
    }
    
    
    /**
     * Determine whether multiple servers are permitted within this AS
     * installation.
     * @return true if multiple servers are permitted.
     * @throws ManagementRemoteException
     */
    public boolean supportsMultipleServers() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean result = Boolean.FALSE;
        Object[] params = null;
        String[] signature = null;
        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "supportsMultipleServers", params, signature);
        return result.booleanValue();
    }
    
    
    /**
     * Get the Target Name. If the instance is not a clustered instance then
     * the target name is the instance name. If the instance is part of a
     * cluster then the target name is the cluster name.
     *
     * @return the target name. 
     * @throws ManagementRemoteException
     */
    public String getTargetName() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        String result = null;
        Object[] params = null;
        String[] signature = null;
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getTargetName", params, signature);
        return result;
        
    }
    
    
    /**
     * Get the Target Name for a specified instance. If the instance is not
     * clustered the instance name is returned. This operation is invoked by
     * the JBI instance MBeans only.
     *
     * @return the target name. 
     * @throws ManagementRemoteException
     */
    public String getTargetName(String instanceName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        String result = null;
        Object[] params = new Object[1];
        params[0] = instanceName;
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getTargetName", params, signature);
        return result;
    }
    
    
    /**
     * Get a set of the names of all the standalone servers in the domain.
     * @return a set of names of standalone servers in the domain.
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    public Set<String> getStandaloneServerNames() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Set<String> result = null;
        Object[] params = null;
        String[] signature = null;
        result = (Set<String>) this.invokeMBeanOperation(mbeanName,
                "getStandaloneServerNames", params, signature);
        return result;
    }
    
    
    /**
     * Get a set of the names of all the clustered servers in the domain.
     * @return a set of names of clustered servers in the domain.
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    public Set<String> getClusteredServerNames() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Set<String> result = null;
        Object[] params = null;
        String[] signature = null;
        result = (Set<String>) this.invokeMBeanOperation(mbeanName,
                "getClusteredServerNames", params, signature);
        return result;
    }
    
    /**
     * Get a set of the names of all the clusters in the domain.
     * @return a set of names of clusters in the domain.
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    public Set<String> getClusterNames() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Set<String> result = null;
        Object[] params = null;
        String[] signature = null;
        result = (Set<String>) this.invokeMBeanOperation(mbeanName,
                "getClusterNames", params, signature);
        return result;
    }
    
    
    /**
     * Get a set of the names of all the servers in the specified cluster.
     * @return a set of names of servers in the cluster.
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    public Set<String> getServersInCluster(String clusterName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Set<String> result = null;
        Object[] params = new Object[1];
        params[0] = clusterName;
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        result = (Set<String>) this.invokeMBeanOperation(mbeanName,
                "getTargetName", params, signature);
        return result;
    }
    
    
    /**
     * Determine whether a target is a valid server or cluster name.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a valid
     * standalone server name or cluster name, <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isValidTarget(String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean result = Boolean.FALSE;
        Object[] params = new Object[1];
        params[0] = targetName;
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isValidTarget", params, signature);
        return result.booleanValue();
    }
    
    
    /**
     * Determine whether a target is a cluster.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a cluster,
     * <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isCluster(String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean result = Boolean.FALSE;
        Object[] params = new Object[1];
        params[0] = targetName;
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isCluster", params, signature);
        return result.booleanValue();
    }
    
    
    /**
     * Determine whether a target is a standalone server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a standalone
     * server, <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isStandaloneServer(String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean result = Boolean.FALSE;
        Object[] params = new Object[1];
        params[0] = targetName;
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isStandaloneServer", params, signature);
        return result.booleanValue();
    }
    
    
    /**
     * Determine whether the target is a clustered server.
     * @return <CODE>true</CODE> if <CODE>targetName</CODE> is a clustered
     * server, <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isClusteredServer(String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean result = Boolean.FALSE;
        Object[] params = new Object[1];
        params[0] = targetName;
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isClusteredServer", params, signature);
        return result.booleanValue();
    }
    
    
    /**
     * Determine whether or not an instance is clustered.
     * @return <CODE>true</CODE> if the instance is clustered,
     * <CODE>false</CODE> if not.
     * @throws ManagementRemoteException
     */
    public boolean isInstanceClustered(String instanceName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Boolean result = Boolean.FALSE;
        Object[] params = new Object[1];
        params[0] = instanceName;
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isInstanceClustered", params, signature);
        return result.booleanValue();
    }
    
    
    /**
     * Get a string representation of the DAS JMX RMI connector port.
     * @return the JMX RMI connector port as a (CODE>String</CODE>.
     * @throws ManagementRemoteException
     */
    public String getJmxRmiPort() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        String result = null;
        Object[] params = null;
        String[] signature = null;
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getJmxRmiPort", params, signature);
        return result;
    }
    
    /**
     * Get the full path to the platform's instance root directory.
     * @return platform instance root
     * @throws ManagementRemoteException
     */
    public String getInstanceRoot() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        String result = null;
        Object[] params = null;
        String[] signature = null;
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getInstanceRoot", params, signature);
        return result;
    }
    
    
    /**
     * Get the full path to the platform's instaall root directory.
     * @return platform install root
     * @throws ManagementRemoteException
     */
    public String getInstallRoot() throws ManagementRemoteException {
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        String result = null;
        Object[] params = null;
        String[] signature = null;
        result = (String) this.invokeMBeanOperation(mbeanName,
                "getInstallRoot", params, signature);
        return result;
    }
    
    /**
     * Get the MBean server connection for a particular instance.
     * @return the <CODE>MBeanServerConnection</CODE> for the specified instance.
     * @throws ManagementRemoteException
     */
    public MBeanServerConnection getConnectionForInstance(String instanceName) throws ManagementRemoteException {
        MBeanServerConnection connection = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Object[] params = new Object[1];
        params[0] = instanceName;
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        connection = (MBeanServerConnection) this.invokeMBeanOperation(mbeanName,
                "getConnectionForInstance", params, signature);
        
        return connection;
    }
}

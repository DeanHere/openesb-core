/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
* 
* Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
* 
* The contents of this file are subject to the terms of either the GNU
* General Public License Version 2 only ("GPL") or the Common Development
* and Distribution License("CDDL") (collectively, the "License").  You
* may not use this file except in compliance with the License. You can obtain
* a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
* or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
* language governing permissions and limitations under the License.
* 
* When distributing the software, include this License Header Notice in each
* file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
* Sun designates this particular file as subject to the "Classpath" exception
* as provided by Sun in the GPL Version 2 section of the License file that
* accompanied this code.  If applicable, add the following below the License
* Header, with the fields enclosed by brackets [] replaced by your own
* identifying information: "Portions Copyrighted [year]
* [name of copyright owner]"
* 
* Contributor(s):
* 
* If you wish your version of this file to be governed by only the CDDL or
* only the GPL Version 2, indicate your decision by adding "[Contributor]
* elects to include this software in this distribution under the [CDDL or GPL
* Version 2] license."  If you don't indicate a single choice of license, a
* recipient has the option to distribute your version of this file under
* either the CDDL, the GPL Version 2 or to extend the choice of license to
* its licensees as provided above.  However, if you add GPL Version 2 code
* and therefore, elected the GPL Version 2 license, then the option applies
* only if the new code is made subject to such option by the copyright
* holder.
*/

package com.sun.jbi.cli.commands;

/**
*  Will define the JBI Command Constants
*  @version  $Revision: 1.1 $
*/
public abstract class JBICommandConstants
{
    // ------------------------------------------------------------------
    // JBI asadmin Command Names
    // ------------------------------------------------------------------
    public static final String UPGRADE_JBI_COMPONENT              = "upgrade-jbi-component";
    public static final String SHOW_JBI_SERVICE_ENGINE            = "show-jbi-service-engine";
    public static final String SHOW_JBI_BINDING_COMPONENT         = "show-jbi-binding-component";
    public static final String SHOW_JBI_SHARED_LIBRARY            = "show-jbi-shared-library";
    public static final String SHOW_JBI_SERVICE_ASSEMBLY          = "show-jbi-service-assembly";
    public static final String SHOW_JBI_RUNTIME_CONFIGURATION     = "show-jbi-runtime-configuration";
    public static final String SHOW_JBI_APPLICATION_CONFIGURATION = "show-jbi-application-configuration";
    public static final String SHOW_JBI_RUNTIME_LOGGERS           = "show-jbi-runtime-loggers";
    public static final String SHOW_JBI_STATISTICS                = "show-jbi-statistics";
    public static final String SET_JBI_RUNTIME_LOGGER             = "set-jbi-runtime-logger";
    public static final String SET_JBI_COMPONENT_LOGGER           = "set-jbi-component-logger";
    public static final String SET_JBI_RUNTIME_CONFIGURATION      = "set-jbi-runtime-configuration";
    public static final String SET_JBI_COMPONENT_CONFIGURATION    = "set-jbi-component-configuration";
    public static final String CREATE_APPLICATION_CONFIGURATION   = "create-jbi-application-configuration";
    public static final String CREATE_APPLICATION_VARIABLE        = "create-jbi-application-variable";
    public static final String UPDATE_APPLICATION_CONFIGURATION   = "update-jbi-application-configuration";
    public static final String UPDATE_APPLICATION_VARIABLE        = "update-jbi-application-variable";
    public static final String DELETE_APPLICATION_CONFIGURATION   = "delete-jbi-application-configuration";
    public static final String DELETE_APPLICATION_VARIABLE        = "delete-jbi-application-variable";
    public static final String LIST_APPLICATION_CONFIGURATIONS    = "list-jbi-application-configurations";
    public static final String LIST_APPLICATION_VARIABLES         = "list-jbi-application-variables";
    public static final String LIST_SERVICE_ENGINES               = "list-jbi-service-engines";
    public static final String LIST_BINDING_COMPONENTS            = "list-jbi-binding-components";
    public static final String LIST_SHARED_LIBRARIES              = "list-jbi-shared-libraries";
    public static final String LIST_SERVICE_ASSEMBLIES            = "list-jbi-service-assemblies";
    public static final String VERIFY_JBI_APPLICATION_ENVIRONMENT = "verify-jbi-application-environment";
    public static final String EXPORT_JBI_APPLICATION_ENVIRONMENT = "export-jbi-application-environment";
    public static final String INSTALL_COMPONENT                  = "install-jbi-component";
    public static final String INSTALL_SHARED_LIBRARY             = "install-jbi-shared-library";
    public static final String DEPLOY_SERVICE_ASSEMBLY            = "deploy-jbi-service-assembly";
    public static final String UNINSTALL_COMPONENT                = "uninstall-jbi-component";
    public static final String UNINSTALL_SHARED_LIBRARY           = "uninstall-jbi-shared-library";
    public static final String UNDEPLOY_SERVICE_ASSEMBLY          = "undeploy-jbi-service-assembly";
    public static final String START_COMPONENT                    = "start-jbi-component";
    public static final String STOP_COMPONENT                     = "stop-jbi-component";
    public static final String SHUT_DOWN_COMPONENT                = "shut-down-jbi-component";
    public static final String START_SERVICE_ASSEMBLY             = "start-jbi-service-assembly";
    public static final String STOP_SERVICE_ASSEMBLY              = "stop-jbi-service-assembly";
    public static final String SHUT_DOWN_SERVICE_ASSEMBLY         = "shut-down-jbi-service-assembly";
    

    // ------------------------------------------------------------------
    // Command Option String Values
    // ------------------------------------------------------------------
    public static final String TERSE_OPTION            = "terse";
    public static final String TARGET_OPTION           = "target";
    public static final String LIBRARY_NAME_OPTION     = "libraryname";
    public static final String ASSEMBLY_NAME_OPTION    = "assemblyname";
    public static final String COMPONENT_NAME_OPTION   = "componentname";
    public static final String UPLOAD_OPTION           = "upload";
    public static final String ENABLED_OPTION          = "enabled";
    public static final String LIFECYCLE_STATE_OPTION  = "lifecyclestate";
    public static final String FORCE_OPTION            = "force";
    public static final String KEEP_ARCHIVE_OPTION     = "keeparchive";
    public static final String PROPERTIES_OPTION       = "properties";
    public static final String UPGRADE_FILE_OPTION     = "upgradefile";
    public static final String GENERAL_OPTION          = "general";
    public static final String DISPLAY_OPTION          = "display";
    public static final String LOGGERS_OPTION          = "loggers";
    public static final String CONFIGURATION_OPTION    = "configuration";
    public static final String DESCRIPTOR_OPTION       = "descriptor";
    public static final String COMPONENT_OPTION        = "component";
    public static final String CONFIG_NAME_OPTION      = "configname";
    public static final String FRAMEWORK_OPTION        = "framework";
    public static final String NMR_OPTION              = "nmr";
    public static final String ENDPOINT_OPTION         = "endpoint";
    public static final String SERVICE_ASSEMBLY_OPTION = "serviceassembly";
    public static final String TEMPLATE_DIR_OPTION     = "templatedir";
    public static final String CONFIG_DIR_OPTION       = "configdir";
    public static final String INCLUDE_DEPLOY_OPTION   = "includedeploy";


    // ------------------------------------------------------------------
    // Application Variables Text Type Constants
    // ------------------------------------------------------------------
    public static final String STRING_TYPE   = "[STRING]";
    public static final String BOOLEAN_TYPE  = "[BOOLEAN]";
    public static final String NUMBER_TYPE   = "[NUMBER]";
    public static final String PASSWORD_TYPE = "[PASSWORD]";


    // ------------------------------------------------------------------
    // Specified in JBIStatisticsMBeanImpl.java, Not parameterized so we need
    // to define them here for our use.
    // ------------------------------------------------------------------
    public static final String  PROVIDER_ENDPOINT_TYPE_NAME = "ProviderEndpointStats";
    public static final String  COMSUMER_ENDPOINT_TYPE_NAME = "ConsumerEndpointStats";

}
#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
  #build_test_application_artifacts
  #test_cleanup
    
  #--------------------------------------------------------------------------------
  # Make sure http binding component is started. (output redirected to TEMP file)
  #--------------------------------------------------------------------------------
  start_component server sun-http-binding
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Set Component Logger - Invalid logger level"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-logger --component=sun-http-binding sun-http-binding.com.sun.jbi.httpsoapbc.HttpNormalizer=fred"
  $AS8BASE/bin/asadmin set-jbi-component-logger --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding.com.sun.jbi.httpsoapbc.HttpNormalizer=fred
     
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Negative Set Component Logger - Invalid logger level"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-component-logger --component=sun-http-binding sun-http-binding.com.sun.jbi.httpsoapbc.HttpNormalizer.fred=FINE"
  $AS8BASE/bin/asadmin set-jbi-component-logger --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS sun-http-binding.com.sun.jbi.httpsoapbc.HttpNormalizer.fred=FINE
      
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00022"
TEST_DESCRIPTION="Negative Component loggers and configuration tests"
. ./regress_defs.ksh
run_test

exit 0



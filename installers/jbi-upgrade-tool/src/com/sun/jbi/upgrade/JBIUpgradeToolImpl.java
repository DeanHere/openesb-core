/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIUpgradeToolImpl.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.upgrade;

import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Enumeration;
import java.util.logging.Logger;
import java.util.ResourceBundle;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;

import org.xml.sax.SAXException;
import javax.xml.parsers.SAXParserFactory; 
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.FactoryConfigurationError;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Attr;

import org.xml.sax.InputSource;

/**
 * This class has the main entry point and the entry methods
 * to the upgrade tool.  The main entry will be called from
 * the command line, and the entry methods will be called 
 * from the app server's installer and the app server's 
 * upgrade tool.
 */
public class JBIUpgradeToolImpl implements JBIUpgradeTool
{
    /** Private logger, it will be created if no logger is passed in */
    private Logger           mLogger           = null;
    /** Resource bundle for I18N */
    private ResourceBundle   mResourceBundle   = null;
    /** XML SAX paser factory */
    private SAXParserFactory mSAXParserFactory = null;
    /** Utility class which takes care of copy, unjar and etc. */
    private UpgradeToolUtil  mUtil             = null;

    /**
     * Constructor for JBIUpgradeToolImpl
     * @param logger logger which is passed in,
     *               if it is null, the class
     *               will create its own.
     *
     */
    public JBIUpgradeToolImpl(Logger logger)
    {
        if (logger == null)
        {
            mLogger = createLocalLogger();
        }
        else
        {
            mLogger = logger;
        }

        mResourceBundle =
            ResourceBundle.getBundle(UpgradeToolConstants.RESOURCE_BUNDLE);

        mSAXParserFactory = SAXParserFactory.newInstance();
        mSAXParserFactory.setNamespaceAware(true);

        mUtil = new UpgradeToolUtil(mResourceBundle);
    }

    /**
     * main method, the main method will be used when this tool
     * is invoked from command line.
     *
     */
    public static void main(String [] argv)
    {
        JBIUpgradeTool upgradeTool = JBIUpgradeToolFactory.getJBIUpgradeTool(null);

        try
        {
            if (!(argv.length == 2))
            {
                String expMsg =
                    ((JBIUpgradeToolImpl) upgradeTool).getUtil().getI18NMessage(
                                                         UpgradeToolConstants.USAGE);
                throw new JBIUpgradeException(expMsg);
            }

            upgradeTool.upgradeJBISystemComponentsInDomain(argv[0], argv[1]);
            // upgradeTool.migrateJBIArtifacts(argv[0], argv[1]);
        }
        catch(JBIUpgradeException exp)
        {
            String expMsg = 
                ((JBIUpgradeToolImpl) upgradeTool).getUtil().getI18NMessage(
                    UpgradeToolConstants.EXCEPTION_SUMMARY,
                    new String [] { exp.getMessage() } );
            System.out.println(expMsg);
        }
    }

    /**
     * Get logger
     * @return Logger
     *
     */
    public Logger getLogger()
    {
        return mLogger;
    }

    /**
     * Get UpgradeToolUtil instance which will be used to do
     * utility job, such copy, unjar, getting a I18N string
     * and etc.
     * @return UpgradeToolUtil
     *
     */
    public UpgradeToolUtil getUtil()
    {
        return mUtil;
    }

    /**
     * This method is the contract between SJSAS installer and
     * JBI upgrade tool. When the user perfoms a binary upgrade SJSAS
     * installer will update the JBI binaries in AS_INSTALL/jbi.
     * After that, for each of the domains in the default domain dir,
     * the installer will invoke this method to upgrade
     * the JBI system components in that domain.
     *
     * @param appserverInstallRoot appserver installation root
     * @param domainRoot the root dir for the domain
     *
     * @throws JBIUpgradeException if the upgrade tool encountered an error
     */
    public void upgradeJBISystemComponentsInDomain(
            String appServerInstallRoot,
            String domainRoot)
        throws JBIUpgradeException
    {
        if (!(new File(""+appServerInstallRoot)).exists())
        {
            String expMsg =
                mUtil.getI18NMessage(
                    UpgradeToolConstants.INSTALL_ROOT_NOT_FOUND,
                    new String [] { appServerInstallRoot });
            mLogger.warning(expMsg);
            throw new JBIUpgradeException(expMsg);
        }

        if (!(new File(""+domainRoot)).exists())
        {
            String expMsg =
                mUtil.getI18NMessage(
                    UpgradeToolConstants.DOMAIN_ROOT_NOT_FOUND,
                    new String [] { domainRoot });
            mLogger.warning(expMsg);
            throw new JBIUpgradeException(expMsg);
        }

        mLogger.info(mUtil.getI18NMessage(
                         UpgradeToolConstants.PARSING_JBI_REGISTRY));

        Document registryDom = parseJBIRegistryXML(domainRoot);
        NodeList componentsList = registryDom.getElementsByTagName(
                                    UpgradeToolConstants.JBI_COMPONENTS_DIR);
        NodeList componentList = null;
        if (componentsList.getLength() >= 1)
        {
            componentList = ((Element) componentsList.item(0)).getElementsByTagName(
                                    UpgradeToolConstants.JBI_COMPONENT);
        }

        NodeList sharedLibsList = registryDom.getElementsByTagName(
                                    UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR);
        NodeList sharedLibList = null;
        if (sharedLibsList.getLength() >= 1)
        {
            sharedLibList = ((Element) sharedLibsList.item(0)).getElementsByTagName(
                                    UpgradeToolConstants.JBI_SHARED_LIBRARY);
        }

        mLogger.info(mUtil.getI18NMessage(
                         UpgradeToolConstants.UPGRADING_SYSTEM_COMPONENTS));

        for (int i=0; (componentList != null) && (i < componentList.getLength()); ++i)
        {
            Element compElement = (Element) componentList.item(i);
            String defaultInstallAttr = compElement.getAttribute(UpgradeToolConstants.DEFAULT_INSTALL);
            if (defaultInstallAttr == null)
            {
                continue;
            }
            else if (defaultInstallAttr.toLowerCase().compareTo("true") != 0)
            {
                continue;
            }

            String sysCompName = (String) compElement.getAttribute("name");

            mLogger.info(
                mUtil.getI18NMessage(
                    UpgradeToolConstants.STARTING_COMPONENT_UPGRADE,
                    new String [] { sysCompName }));

            // Start to compare the two version numbers
            // get the version from the existing domain
            String oldBuildNumber =
                getExistingBuildNumber(domainRoot,
                                       sysCompName,
                                       UpgradeToolConstants.JBI_COMPONENTS_DIR);

            // get the version from the new jar file
            String newBuildNumber =
                getNewBuildNumber(appServerInstallRoot,
                                  UpgradeToolConstants.JBI_COMPONENTS_DIR,
                                  sysCompName);

            // compare the two versions
            if ((oldBuildNumber == null) || 
                (oldBuildNumber.compareTo(""+newBuildNumber) < 0))
            {
                // Bring the new one over
                try
                {
                    upgradeSystemComponent(appServerInstallRoot,
                                  domainRoot,
                                  UpgradeToolConstants.JBI_COMPONENTS_DIR,
                                  sysCompName,
                                  oldBuildNumber,
                                  compElement);

                    mLogger.info(
                        mUtil.getI18NMessage(
                            UpgradeToolConstants.UPGRADE_COMPONENT_SUCCESSFUL,
                            new String [] { sysCompName } ));
                }
                catch(JBIUpgradeException exp)
                {
                    mLogger.warning(exp.getMessage());
                }
            }
            else
            {
                mLogger.warning(
                    mUtil.getI18NMessage(
                       UpgradeToolConstants.NEWER_VERSION_COMPONENT_FOUND,
                       new String [] { sysCompName }));
            }
        }

        mLogger.info(mUtil.getI18NMessage(
                         UpgradeToolConstants.UPGRADING_SYSTEM_SHARED_LIBRARIES));

        for (int i=0; (sharedLibList != null) && (i < sharedLibList.getLength()); ++i)
        {
            Element slElement = (Element) sharedLibList.item(i);
            String defaultInstallAttr = slElement.getAttribute(UpgradeToolConstants.DEFAULT_INSTALL);
            if (defaultInstallAttr == null)
            {
                continue;
            }
            else if (defaultInstallAttr.toLowerCase().compareTo("true") != 0)
            {
                continue;
            }

            String sysSharedLibName = (String) slElement.getAttribute("name");

            mLogger.info(
                mUtil.getI18NMessage(
                    UpgradeToolConstants.STARTING_SHARED_LIBRARY_UPGRADE,
                    new String [] { sysSharedLibName }));

            // Start to compare the two version numbers
            // get the version from the existing domain
            String oldBuildNumber =
                getExistingBuildNumber(domainRoot,
                                       sysSharedLibName,
                                       UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR);

            // get the version from the new jar file
            String newBuildNumber =
                getNewBuildNumber(appServerInstallRoot,
                                  UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR,
                                  sysSharedLibName);

            // compare the two versions
            if ((oldBuildNumber == null) ||
                (oldBuildNumber.compareTo(""+newBuildNumber) < 0))
            {
                // Bring the new one over
                try
                {
                    upgradeSystemComponent(appServerInstallRoot,
                                  domainRoot,
                                  UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR,
                                  sysSharedLibName,
                                  oldBuildNumber,
                                  slElement);

                    mLogger.info(
                        mUtil.getI18NMessage(
                            UpgradeToolConstants.UPGRADE_SHARED_LIBRARY_SUCCESSFUL,
                            new String [] { sysSharedLibName } ));
                }
                catch(JBIUpgradeException exp)
                {
                    mLogger.warning(exp.getMessage());
                }
            }
            else
            {
                mLogger.warning(
                    mUtil.getI18NMessage(
                       UpgradeToolConstants.NEWER_VERSION_SHARED_LIBRARY_FOUND,
                       new String [] { sysSharedLibName }));
            }
        }

        try
        {
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer trans = transFactory.newTransformer();

            File jbiRegistryFile = new File(domainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        "config" +
                                        File.separator +
                                        "jbi-registry.xml");
            File jbiRegistryBackupFile = new File(domainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        "config" +
                                        File.separator +
                                        "jbi-registry-backup.xml");

            
            if (jbiRegistryFile.exists())
            {
                jbiRegistryFile.delete();
            }
            StringWriter strWriter = new StringWriter();
            StreamResult resultStream = new StreamResult(strWriter);
            trans.setOutputProperty(OutputKeys.STANDALONE, "yes");
            trans.setOutputProperty(OutputKeys.METHOD, "xml");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.transform(new DOMSource(registryDom), resultStream);
            String outString = strWriter.getBuffer().toString() + "\n";
            PrintStream outputStream = new PrintStream(jbiRegistryFile);
            outputStream.print(outString);
            outputStream.close();
            strWriter.close();

            if (jbiRegistryBackupFile.exists())
            {
                jbiRegistryBackupFile.delete();
            }
            strWriter = new StringWriter();
            resultStream = new StreamResult(strWriter);
            trans.setOutputProperty(OutputKeys.STANDALONE, "yes");
            trans.setOutputProperty(OutputKeys.METHOD, "xml");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.transform(new DOMSource(registryDom), resultStream);
            outString = strWriter.getBuffer().toString() + "\n";
            outputStream = new PrintStream(jbiRegistryBackupFile);
            outputStream.print(outString);
            outputStream.close();
            strWriter.close();
        }
        catch(Exception exp)
        {
            String expMsg =
                mUtil.getI18NMessage(
                    UpgradeToolConstants.ERROR_WHEN_UPDATING_JBI_REGISTRY,
                    new String [] { domainRoot });
            mLogger.warning(expMsg);
        }
    }

    /**
     * This method is the contract between appserver upgrade tool and
     * JBI upgrade tool.
     * When the user performs an in-place upgrade or a side-by-side
     * upgrade upgrade tool will invoke the JBIUpgrade tool to
     * migrate the existing JBI artifacts (components and applications)
     * to the newly created domain.
     *
     * @param sourceDomainRoot the root dir for the source domain
     * @param destinationDomainRoot the root dir for the destination domain
     * @throws JBIUpgradeException if JBIUpgradetool encountered an error condition
     */
    public void migrateJBIArtifacts(
            String sourceDomainRoot,
            String destinationDomainRoot)
        throws JBIUpgradeException
    {
        // Get the installed non-system components in dest 
        if (!(new File(""+destinationDomainRoot)).exists())
        {
            String expMsg =
                mUtil.getI18NMessage(
                    UpgradeToolConstants.DESTINATION_DOMAIN_ROOT_NOT_FOUND,
                    new String [] { destinationDomainRoot } );
            mLogger.severe(expMsg);
            throw new JBIUpgradeException(expMsg);
        }

        if (!(new File(""+sourceDomainRoot)).exists())
        {
            String expMsg =
                mUtil.getI18NMessage(
                    UpgradeToolConstants.SOURCE_DOMAIN_ROOT_NOT_FOUND,
                    new String [] { sourceDomainRoot } );
            mLogger.severe(expMsg);
            throw new JBIUpgradeException(expMsg);
        }

        mLogger.info(mUtil.getI18NMessage(
                         UpgradeToolConstants.MIGRATING_NON_SYSTEM_COMPONENTS));

        Document srcRegistryDom = parseJBIRegistryXML(sourceDomainRoot);
        Document destRegistryDom = parseJBIRegistryXML(destinationDomainRoot);
        NodeList componentsList = srcRegistryDom.getElementsByTagName(
                                    UpgradeToolConstants.JBI_COMPONENTS_DIR);
        NodeList componentList = null;
        if (componentsList.getLength() >= 1)
        {
            componentList =
               ((Element) componentsList.item(0)).getElementsByTagName(
                         UpgradeToolConstants.JBI_COMPONENT);
        }

        NodeList sharedLibsList = srcRegistryDom.getElementsByTagName(
                                    UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR);
        NodeList sharedLibList = null;
        if (sharedLibsList.getLength() >= 1)
        {
            sharedLibList = ((Element) sharedLibsList.item(0)).getElementsByTagName(
                                    UpgradeToolConstants.JBI_SHARED_LIBRARY);
        }

        NodeList sasList = srcRegistryDom.getElementsByTagName(
                                    UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR);
        NodeList saList = null;
        if (sasList.getLength() >= 1)
        {
            saList = ((Element) sasList.item(0)).getElementsByTagName(
                                    UpgradeToolConstants.JBI_SERVICE_ASSEMBLY);
        }

        // Prepare for the desctination directories
        File checkDestCompsDir = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_COMPONENTS_DIR);
        if (!checkDestCompsDir.exists())
        {
            checkDestCompsDir.mkdir();
        }

        File checkDestSharedLibsDir = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR);
        if (!checkDestSharedLibsDir.exists())
        {
            checkDestSharedLibsDir.mkdir();
        }

        File checkDestSAsDir = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR);
        if (!checkDestSAsDir.exists())
        {
            checkDestSAsDir.mkdir();
        }

        for (int i=0; (componentList != null) && (i < componentList.getLength()); ++i)
        {
            Element srcCompNode = (Element) componentList.item(i);
            String defaultInstallFlag = srcCompNode.getAttribute(UpgradeToolConstants.DEFAULT_INSTALL);
            if ((defaultInstallFlag != null) &&
                (defaultInstallFlag.toLowerCase().compareTo("true") == 0))
            {
                continue;
            }
            String componentName = (String) srcCompNode.getAttribute("name");

            mLogger.info(mUtil.getI18NMessage(
                         UpgradeToolConstants.STARTING_COMPONENT_UPGRADE,
                         new String [] { componentName } ));

            File destCompInstallDir = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_COMPONENTS_DIR +
                                        File.separator +
                                        componentName);
            if (!destCompInstallDir.exists())
            {
                File srcCompInstallDir = new File(sourceDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_COMPONENTS_DIR +
                                        File.separator +
                                        componentName);
                if (!srcCompInstallDir.exists())
                {
                    mLogger.warning(
                        mUtil.getI18NMessage(
                            UpgradeToolConstants.COMPONENT_INSTALL_JAR_NOT_FOUND,
                            new String [] { componentName } ));
                }
                else
                {
                    mUtil.copyDir(srcCompInstallDir.toString(),
                                  destCompInstallDir.toString());

                    try
                    {
                        // Migrate JBI registry info
                        String jarFileName =
                            srcCompNode.getAttribute(UpgradeToolConstants.FILE_NAME);
                        File jarFile = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_COMPONENTS_DIR +
                                        File.separator +
                                        componentName + 
                                        File.separator +
                                        jarFileName);

                        Element destCompNode = (Element) srcCompNode.cloneNode(true);
                        // destCompNode.setAttribute(UpgradeToolConstants.TIME_STAMP,
                        //                          Long.toString(jarFile.lastModified()));
                        NodeList destCompsNodeList = destRegistryDom.getElementsByTagName(
                                    UpgradeToolConstants.JBI_COMPONENTS_DIR);
                        if (destCompsNodeList.getLength() == 0)
                        {
                            // Search for the position
                            Element positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR);
                            if (positionNode == null)
                            {
                                 positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR);
                            }

                            if (positionNode == null)
                            {
                                 positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.SERVERS);
                            }

                            if (positionNode == null)
                            {
                                 positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.CLUSTERS);
                            }

                            if (positionNode == null)
                            {
                                 positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.CONFIGS);
                            }

                            if (positionNode != null)
                            {
                                destRegistryDom.getDocumentElement().insertBefore(
                                    (Element) destRegistryDom.createElement(
                                        UpgradeToolConstants.JBI_COMPONENTS_DIR),
                                    positionNode);
                            }
                            else
                            {
                                destRegistryDom.getDocumentElement().appendChild(
                                    (Element) destRegistryDom.createElement(
                                        UpgradeToolConstants.JBI_COMPONENTS_DIR));
                            }
                        }
 
                        Element destCompsElem = (Element) destCompsNodeList.item(0);
                        destCompsElem.appendChild(destRegistryDom.adoptNode(destCompNode));

                        // Update refs
                        // Search for component-ref in <servers/>
                        Element srcCompRefElem = null;
                        Element srcServersElem =
                            getChildElement(srcRegistryDom.getDocumentElement(),
                                       UpgradeToolConstants.SERVERS);
                        
                        if (srcServersElem != null)
                        {
                            Element srcServerElem = getChildElement(srcServersElem,
                                                       UpgradeToolConstants.SERVER);
                            if (srcServerElem != null)
                            {
                                srcCompRefElem = getChildElement(srcServerElem,
                                                       UpgradeToolConstants.COMPONENT_REF,
                                                       componentName);
                            }
                        }

                        if (srcCompRefElem == null)
                        {
                            // Search for component-ref in <clusters/>
                            Element srcClustersElem =
                                getChildElement(srcRegistryDom.getDocumentElement(),
                                       UpgradeToolConstants.CLUSTERS);
                        
                            if (srcClustersElem != null)
                            {
                                Element srcClusterElem = getChildElement(srcServersElem,
                                                       UpgradeToolConstants.CLUSTER);
                                if (srcClusterElem != null)
                                {
                                    srcCompRefElem = getChildElement(srcClusterElem,
                                                       UpgradeToolConstants.COMPONENT_REF,
                                                       componentName);
                                }
                            }
                        }

                        if (srcCompRefElem != null)
                        {
                            Element destCompRefElem = (Element) destRegistryDom.adoptNode(
                                                            srcCompRefElem.cloneNode(true));
                            Element destServersElem =
                                        getChildElement(destRegistryDom.getDocumentElement(),
                                                   UpgradeToolConstants.SERVERS);
                            NodeList destServerElemList =
                                        destServersElem.getElementsByTagName(
                                                   UpgradeToolConstants.SERVER);

                            for (int j = 0; j < destServerElemList.getLength(); ++j)
                            {
                                Element destServerElem = (Element) destServerElemList.item(j);
                                // Look for the right positon to insert the element
                                Element thePostionNode = getChildElement(destServerElem,
                                                       UpgradeToolConstants.SHARED_LIBRARY_REF);
                                if (thePostionNode == null)
                                {
                                     thePostionNode = getChildElement(destServerElem,
                                                       UpgradeToolConstants.SERVICE_ASSEMBLY_REF);
                                }

                                if (thePostionNode != null)
                                {
                                    destServerElem.insertBefore(destCompRefElem, thePostionNode);
                                }
                                else
                                { 
                                    destServerElem.appendChild(destCompRefElem);
                                }
                            }

                            Element destClustersElem =
                                        getChildElement(destRegistryDom.getDocumentElement(),
                                                   UpgradeToolConstants.CLUSTERS);
                            NodeList destClusterElemList =
                                        destServersElem.getElementsByTagName(
                                                   UpgradeToolConstants.CLUSTER);
                            for (int j = 0; j < destClusterElemList.getLength(); ++j)
                            {
                                Element destClusterElem = (Element) destClusterElemList.item(j);
                                // Look for the right positon to insert the element
                                Element thePostionNode = getChildElement(destClusterElem,
                                                       UpgradeToolConstants.SHARED_LIBRARY_REF);
                                if (thePostionNode == null)
                                {
                                     thePostionNode = getChildElement(destClusterElem,
                                                       UpgradeToolConstants.SERVICE_ASSEMBLY_REF);
                                }

                                if (thePostionNode != null)
                                {
                                    destClusterElem.insertBefore(destCompRefElem, thePostionNode);
                                }
                                else
                                {
                                    destClusterElem.appendChild(destCompRefElem);
                                }
                            }
                        }

                        mLogger.info(mUtil.getI18NMessage(
                            UpgradeToolConstants.UPGRADE_COMPONENT_SUCCESSFUL,
                            new String [] { componentName } ));
                    }
                    catch(Exception exp)
                    {
                        exp.printStackTrace();
                        mLogger.warning(mUtil.getI18NMessage(
                            UpgradeToolConstants.ERROR_WHEN_UPDATING_JBI_REGISTRY));
                    }
                }
            }
            else
            {
                mLogger.warning(
                    mUtil.getI18NMessage(
                        UpgradeToolConstants.COMPONENT_FOUND_IN_DEST_DOMAIN,
                        new String [] { componentName, destinationDomainRoot } ));
            }
        }

        mLogger.info(mUtil.getI18NMessage(
                         UpgradeToolConstants.MIGRATING_NON_SYSTEM_SHARED_LIBRARIES));

        for (int i=0; (sharedLibList != null) && (i < sharedLibList.getLength()); ++i)
        {
            Element srcSharedLibNode = (Element) sharedLibList.item(i);
            String defaultInstall = srcSharedLibNode.getAttribute(UpgradeToolConstants.DEFAULT_INSTALL);
            if ((defaultInstall != null) && 
                    (defaultInstall.toLowerCase().compareTo("true") == 0))
            {
                continue;
            }
            String sharedLibName = (String) srcSharedLibNode.getAttribute("name");

            mLogger.info(mUtil.getI18NMessage(
                         UpgradeToolConstants.STARTING_SHARED_LIBRARY_UPGRADE,
                         new String [] { sharedLibName } ));

            File destSharedLibInstallDir = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR +
                                        File.separator +
                                        sharedLibName);
            if (!destSharedLibInstallDir.exists())
            {
                File srcSharedLibInstallDir = new File(sourceDomainRoot +
                                   File.separator +
                                   UpgradeToolConstants.JBI_INSTALL_DIR +
                                   File.separator +
                                   UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR +
                                   File.separator +
                                   sharedLibName);

                if (!srcSharedLibInstallDir.exists())
                {
                    mLogger.warning(mUtil.getI18NMessage(
                            UpgradeToolConstants.SHARED_LIB_INSTALL_ROOT_NOT_FOUND,
                            new String [] { srcSharedLibInstallDir.toString() } ));
                }
                else
                {
                    mUtil.copyDir(srcSharedLibInstallDir.toString(),
                                  destSharedLibInstallDir.toString());

                    // Migrate JBI Registry info
                    try
                    {
                        // Migrate JBI registry info
                        String jarFileName =
                            srcSharedLibNode.getAttribute(UpgradeToolConstants.FILE_NAME);
                        File jarFile = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR +
                                        File.separator +
                                        sharedLibName + 
                                        File.separator +
                                        jarFileName);

                        Element destSharedLibNode = (Element) srcSharedLibNode.cloneNode(true);
                        destSharedLibNode.setAttribute(UpgradeToolConstants.TIME_STAMP,
                                                  Long.toString(jarFile.lastModified()));
                        NodeList destSharedLibsNodeList = destRegistryDom.getElementsByTagName(
                                    UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR);
                        if (destSharedLibsNodeList.getLength() == 0)
                        {
                            // Search for the position
                            Element positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR);

                            if (positionNode == null)
                            {
                                 positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.SERVERS);
                            }

                            if (positionNode == null)
                            {
                                 positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.CLUSTERS);
                            }

                            if (positionNode == null)
                            {
                                 positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.CONFIGS);
                            }

                            if (positionNode != null)
                            {
                                destRegistryDom.getDocumentElement().insertBefore(
                                    (Element) destRegistryDom.createElement(
                                        UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR),
                                    positionNode);
                            }
                            else
                            {
                                destRegistryDom.getDocumentElement().appendChild(
                                    (Element) destRegistryDom.createElement(
                                        UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR));
                            }
                        }
 
                        Element destSharedLibsElem = (Element) destSharedLibsNodeList.item(0);
                        destSharedLibsElem.appendChild(destRegistryDom.adoptNode(destSharedLibNode));

                        // Update refs
                        // Search for shared-library-ref in <servers/>
                        Element srcSharedLibRefElem = null;
                        Element srcServersElem =
                            getChildElement(srcRegistryDom.getDocumentElement(),
                                       UpgradeToolConstants.SERVERS);
                        
                        if (srcServersElem != null)
                        {
                            Element srcServerElem = getChildElement(srcServersElem,
                                                       UpgradeToolConstants.SERVER);
                            if (srcServerElem != null)
                            {
                                srcSharedLibRefElem = getChildElement(srcServerElem,
                                                       UpgradeToolConstants.SHARED_LIBRARY_REF,
                                                       sharedLibName);
                            }
                        }

                        if (srcSharedLibRefElem == null)
                        {
                            // Search for shared-library-ref in <clusters/>
                            Element srcClustersElem =
                                getChildElement(srcRegistryDom.getDocumentElement(),
                                       UpgradeToolConstants.CLUSTERS);
                        
                            if (srcClustersElem != null)
                            {
                                Element srcClusterElem = getChildElement(srcServersElem,
                                                       UpgradeToolConstants.CLUSTER);
                                if (srcClusterElem != null)
                                {
                                    srcSharedLibRefElem = getChildElement(srcClusterElem,
                                                       UpgradeToolConstants.SHARED_LIBRARY_REF,
                                                       sharedLibName);
                                }
                            }
                        }

                        if (srcSharedLibRefElem != null)
                        {
                            Element destSharedLibRefElem = (Element) destRegistryDom.adoptNode(
                                                            srcSharedLibRefElem.cloneNode(true));
                            Element destServersElem =
                                        getChildElement(destRegistryDom.getDocumentElement(),
                                                   UpgradeToolConstants.SERVERS);
                            NodeList destServerElemList =
                                        destServersElem.getElementsByTagName(
                                                   UpgradeToolConstants.SERVER);
                            for (int j = 0; j < destServerElemList.getLength(); ++j)
                            {
                                Element destServerElem = (Element) destServerElemList.item(j);
                                // Look for the right positon to insert the element
                                Element thePostionNode = getChildElement(destServerElem,
                                                       UpgradeToolConstants.SERVICE_ASSEMBLY_REF);

                                if (thePostionNode != null)
                                {
                                    destServerElem.insertBefore(destSharedLibRefElem, thePostionNode);
                                }
                                else
                                {
                                    destServerElem.appendChild(destSharedLibRefElem);
                                }
                            }

                            Element destClustersElem =
                                        getChildElement(destRegistryDom.getDocumentElement(),
                                                   UpgradeToolConstants.CLUSTERS);
                            NodeList destClusterElemList =
                                        destServersElem.getElementsByTagName(
                                                   UpgradeToolConstants.CLUSTER);
                            for (int j = 0; j < destClusterElemList.getLength(); ++j)
                            {
                                Element destClusterElem = (Element) destClusterElemList.item(j);
                                // Look for the right positon to insert the element
                                Element thePostionNode = getChildElement(destClusterElem,
                                                       UpgradeToolConstants.SERVICE_ASSEMBLY_REF);

                                if (thePostionNode != null)
                                {
                                    destClusterElem.insertBefore(destSharedLibRefElem, thePostionNode);
                                }
                                else
                                {
                                    destClusterElem.appendChild(destSharedLibRefElem);
                                }
                            }
                        }
                
                        mLogger.info(
                            mUtil.getI18NMessage(
                                UpgradeToolConstants.UPGRADE_SHARED_LIBRARY_SUCCESSFUL,
                                    new String [] { sharedLibName } ));
                    }
                    catch(Exception exp)
                    {
                        mLogger.warning(mUtil.getI18NMessage(
                            UpgradeToolConstants.ERROR_WHEN_UPDATING_JBI_REGISTRY));
                    }
                }
            }
            else
            {
                mLogger.warning(
                    mUtil.getI18NMessage(
                        UpgradeToolConstants.SHARED_LIB_FOUND_IN_DEST_DOMAIN,
                        new String [] { sharedLibName, destinationDomainRoot } ));
            }
        }

        mLogger.info(mUtil.getI18NMessage(
                        UpgradeToolConstants.MIGRATING_SERVICE_ASSEMBLIES));

        for (int i=0; (saList != null) && (i < saList.getLength()); ++i)
        {
            Element srcSANode = (Element) saList.item(i);
            String saName = (String) srcSANode.getAttribute("name");

            mLogger.info(mUtil.getI18NMessage(
                        UpgradeToolConstants.STARTING_SERVICE_ASSEMBLIY_UPGRADE,
                        new String [] { saName } ));

            File destSAInstallDir = new File(destinationDomainRoot +
                                            File.separator +
                                            UpgradeToolConstants.JBI_INSTALL_DIR +
                                            File.separator +
                                            UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR +
                                            File.separator +
                                            saName);
            if (!destSAInstallDir.exists())
            {
                File srcSAInstallDir = new File(sourceDomainRoot +
                                   File.separator +
                                   UpgradeToolConstants.JBI_INSTALL_DIR +
                                   File.separator +
                                   UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR +
                                   File.separator +
                                   saName.trim());

                if (!srcSAInstallDir.exists())
                {
                    mLogger.warning(mUtil.getI18NMessage(
                            UpgradeToolConstants.SERVICE_ASSEMBLY_INSTALL_ROOT_NOT_FOUND,
                            new String [] { saName } ));
                }
                else
                {
                    mUtil.copyDir(srcSAInstallDir.toString(),
                                  destSAInstallDir.toString());

                    // Migrate JBI Registry info
                    try
                    {
                        // Migrate JBI registry info
                        String zipFileName =
                            srcSANode.getAttribute(UpgradeToolConstants.FILE_NAME);
                        File zipFile = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR +
                                        File.separator +
                                        saName + 
                                        File.separator +
                                        zipFileName);

                        Element destSANode = (Element) srcSANode.cloneNode(true);
                        destSANode.setAttribute(UpgradeToolConstants.TIME_STAMP,
                                                  Long.toString(zipFile.lastModified()));
                        NodeList destSAsNodeList = destRegistryDom.getElementsByTagName(
                                    UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR);
                        if (destSAsNodeList.getLength() == 0)
                        {
                            // Search for the position
                            Element positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.SERVERS);

                            if (positionNode == null)
                            {
                                 positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.CLUSTERS);
                            }

                            if (positionNode == null)
                            {
                                 positionNode = getChildElement(destRegistryDom.getDocumentElement(),
                                                       UpgradeToolConstants.CONFIGS);
                            }

                            if (positionNode != null)
                            {
                                destRegistryDom.getDocumentElement().insertBefore(
                                    (Element) destRegistryDom.createElement(
                                        UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR),
                                    positionNode);
                            }
                            else
                            {
                                destRegistryDom.getDocumentElement().appendChild(
                                    (Element) destRegistryDom.createElement(
                                        UpgradeToolConstants.JBI_SERVICE_ASSEMBLIES_DIR));
                            }
                        }
 
                        Element destSAsElem = (Element) destSAsNodeList.item(0);
                        destSAsElem.appendChild(destRegistryDom.adoptNode(destSANode));

                        // Update refs
                        // Search for service-assembly-ref in <servers/>
                        Element srcSARefElem = null;
                        Element srcServersElem =
                            getChildElement(srcRegistryDom.getDocumentElement(),
                                       UpgradeToolConstants.SERVERS);
                        
                        if (srcServersElem != null)
                        {
                            Element srcServerElem = getChildElement(srcServersElem,
                                                       UpgradeToolConstants.SERVER);
                            if (srcServerElem != null)
                            {
                                srcSARefElem = getChildElement(srcServerElem,
                                                       UpgradeToolConstants.SERVICE_ASSEMBLY_REF,
                                                       saName);
                            }
                        }

                        if (srcSARefElem == null)
                        {
                            // Search for component-ref in <clusters/>
                            Element srcClustersElem =
                                getChildElement(srcRegistryDom.getDocumentElement(),
                                       UpgradeToolConstants.CLUSTERS);
                        
                            if (srcClustersElem != null)
                            {
                                Element srcClusterElem = getChildElement(srcServersElem,
                                                       UpgradeToolConstants.CLUSTER);
                                if (srcClusterElem != null)
                                {
                                    srcSARefElem = getChildElement(srcClusterElem,
                                                       UpgradeToolConstants.SERVICE_ASSEMBLY_REF,
                                                       saName);
                                }
                            }
                        }

                        if (srcSARefElem != null)
                        {
                            Element destSARefElem = (Element) destRegistryDom.adoptNode(
                                                            srcSARefElem.cloneNode(true));
                            Element destServersElem =
                                        getChildElement(destRegistryDom.getDocumentElement(),
                                                   UpgradeToolConstants.SERVERS);
                            NodeList destServerElemList =
                                        destServersElem.getElementsByTagName(
                                                   UpgradeToolConstants.SERVER);
                            for (int j = 0; j < destServerElemList.getLength(); ++j)
                            {
                                Element destServerElem = (Element) destServerElemList.item(j);
                                destServerElem.appendChild(destSARefElem);
                            }

                            Element destClustersElem =
                                        getChildElement(destRegistryDom.getDocumentElement(),
                                                   UpgradeToolConstants.CLUSTERS);
                            NodeList destClusterElemList =
                                        destServersElem.getElementsByTagName(
                                                   UpgradeToolConstants.CLUSTER);
                            for (int j = 0; j < destClusterElemList.getLength(); ++j)
                            {
                                Element destClusterElem = (Element) destClusterElemList.item(j);
                                destClusterElem.appendChild(destSARefElem);
                            }
                        }

                        mLogger.info(mUtil.getI18NMessage(
                            UpgradeToolConstants.UPGRADE_SERVICE_ASSEMBLY_SUCCESSFUL,
                                new String [] { saName } ));
                    }
                    catch(Exception exp)
                    {
                        mLogger.warning(mUtil.getI18NMessage(
                            UpgradeToolConstants.ERROR_WHEN_UPDATING_JBI_REGISTRY));
                    }
                }
            }
            else
            {
                mLogger.warning(
                    mUtil.getI18NMessage(
                        UpgradeToolConstants.SERVICE_ASSEMBLY_FOUND_IN_DEST_DOMAIN,
                        new String [] { saName, destinationDomainRoot } ));
            }
        }

        try
        {
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer trans = transFactory.newTransformer();

            File jbiRegistryFile = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        "config" +
                                        File.separator +
                                        "jbi-registry.xml");
            File jbiRegistryBackupFile = new File(destinationDomainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        "config" +
                                        File.separator +
                                        "jbi-registry-backup.xml");

    
            if (jbiRegistryFile.exists())
            {
                jbiRegistryFile.delete();
            }
            StringWriter strWriter = new StringWriter();
            StreamResult resultStream = new StreamResult(strWriter);
            trans.setOutputProperty(OutputKeys.STANDALONE, "yes");
            trans.setOutputProperty(OutputKeys.METHOD, "xml");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.transform(new DOMSource(destRegistryDom), resultStream);
            String outString = strWriter.getBuffer().toString() + "\n";
            PrintStream outputStream = new PrintStream(jbiRegistryFile);
            outputStream.print(outString);
            outputStream.close();
            strWriter.close();

            if (jbiRegistryBackupFile.exists())
            {
                jbiRegistryBackupFile.delete();
            }
            strWriter = new StringWriter();
            resultStream = new StreamResult(strWriter);
            trans.setOutputProperty(OutputKeys.STANDALONE, "yes");
            trans.setOutputProperty(OutputKeys.METHOD, "xml");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.transform(new DOMSource(destRegistryDom), resultStream);
            outString = strWriter.getBuffer().toString() + "\n";
            outputStream = new PrintStream(jbiRegistryBackupFile);
            outputStream.print(outString);
            outputStream.close();
            strWriter.close();
        }
        catch(Exception exp)
        {
            String expMsg =
                mUtil.getI18NMessage(
                    UpgradeToolConstants.ERROR_WHEN_UPDATING_JBI_REGISTRY,
                    new String [] { destinationDomainRoot });
            mLogger.warning(expMsg);
        }
    }

    /**
     * Method upgradeSystemComponent
     * @param appServerInstallRoot
     * @param domainRoot
     * @param componentDir
     * @param sysCompName
     * @param oldBuildNumbe
     * @param compElement
     */
    private void upgradeSystemComponent(
                     String appServerInstallRoot,
                     String domainRoot,
                     String componentDir,
                     String sysCompName,
                     String oldBuildNumber,
                     Element compElement)
        throws JBIUpgradeException
    {
        File compInstallJar = mUtil.getJarPathInComponentRoot(
                                        domainRoot,
                                        componentDir,
                                        sysCompName);

        //   Rename the old component install jar
        if (compInstallJar != null)
        {
            oldBuildNumber = (oldBuildNumber==null || oldBuildNumber.length()==0) ?
                UpgradeToolConstants.ORIGINAL_VERSION_NUMBER : oldBuildNumber;

            File newFile = new File(compInstallJar.toString() + "." + oldBuildNumber);
            if (newFile.exists())
            {
                newFile.delete();
            }

            compInstallJar.renameTo(newFile);
        }

        // Copy the new component install jar
        FileOutputStream output = null;
        FileInputStream input = null;
        File inputFile = null;
        try
        {
            if (compInstallJar.createNewFile())
            {
                output = new FileOutputStream(compInstallJar);
            }
            
            String inputFileString = mUtil.getInstallJarPathInAppServerInstallRoot(
                                         appServerInstallRoot,
                                         componentDir,
                                         sysCompName);

            if (inputFileString == null)
            {
                 String expMsg = mUtil.getI18NMessage(
                        UpgradeToolConstants.COMPONENT_INSTALL_JAR_NOT_FOUND,
                        new String [] { sysCompName });
                 throw new JBIUpgradeException(expMsg);
            }
            else
            {
                inputFile = new File(inputFileString);
                if (!inputFile.exists())
                {
                    String expMsg = mUtil.getI18NMessage(
                        UpgradeToolConstants.COMPONENT_INSTALL_JAR_NOT_FOUND,
                        new String [] { sysCompName });
                    throw new JBIUpgradeException(expMsg);
                }
            }
            input = new FileInputStream(inputFile);
            mUtil.transfer(input, output);
            if (componentDir.compareTo(UpgradeToolConstants.JBI_COMPONENTS_DIR) == 0)
            {
                updateUpgradeNumber(compElement);
            }
            else if (componentDir.compareTo(UpgradeToolConstants.JBI_SHARED_LIBRARIES_DIR) == 0)
            {
                compElement.setAttribute(UpgradeToolConstants.TIME_STAMP,
                                   Long.toString(compInstallJar.lastModified()));
            }
        }
        catch(IOException exp)
        {
            String expMsg =
                mUtil.getI18NMessage(
                    UpgradeToolConstants.IO_EXCEPTION_DURING_FILE_COPY,
                    new String [] { inputFile.toString(), compInstallJar.toString() } );
            mLogger.severe(expMsg);
            throw new JBIUpgradeException(expMsg);

        }
        finally
        {
            if (input != null)
            {
                try { input.close(); } catch (IOException exp) { ; }
            }
            if (output != null)
            {
                try { output.close(); } catch (IOException exp) { ; }
            }
        }

        File compInstallRootDir = new File(domainRoot +
                                   File.separator +
                                   UpgradeToolConstants.JBI_INSTALL_DIR +
                                   File.separator +
                                   componentDir +
                                   File.separator +
                                   sysCompName.trim() +
                                   File.separator +
                                   UpgradeToolConstants.INSTALL_ROOT); 

        if (!compInstallRootDir.exists())
        {
            String expMsg =
                mUtil.getI18NMessage(
                    UpgradeToolConstants.COMPONENT_INSTALL_ROOT_NOT_FOUND,
                    new String [] { compInstallRootDir.toString() } );
            throw new JBIUpgradeException(expMsg);
        }

        mUtil.clearComponentInstallRootDir(compInstallRootDir, true);

        // Then Unjar
        try
        {
            mUtil.unJar(compInstallJar,
                            compInstallRootDir.toString());
        }
        catch (Exception ex)
        {
            String expMsg =
                mUtil.getI18NMessage(
                    UpgradeToolConstants.UNJAR_ERROR,
                    new String [] { compInstallJar.toString() } );
            throw new JBIUpgradeException(expMsg);
        }
    }

    /**
     * Private method parseJBIRegistryXML
     * @param domainRoot
     * @return Document
     * @throws JBIUpgradeException
     */
    private Document parseJBIRegistryXML(String domainRoot)
        throws JBIUpgradeException
    {
        try
        {
            File jbiRegistryFile = new File(domainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        "config" +
                                        File.separator +
                                        "jbi-registry.xml");

            if (!jbiRegistryFile.exists())
            {
                String expMsg =
                    this.getResourceBundle().getString(UpgradeToolConstants.JBI_REGISTRY_NOT_FOUND);

                throw new JBIUpgradeException(expMsg);
            }

            DocumentBuilderFactory domFactory =
                     DocumentBuilderFactory.newInstance();
            DocumentBuilder tmpBuilder = domFactory.newDocumentBuilder();
            domFactory.setNamespaceAware(false);
            return tmpBuilder.parse(jbiRegistryFile);
        }
        catch (Exception ex)
        {
            String expMsg =
                mUtil.getI18NMessage(UpgradeToolConstants.JBI_REGISTRY_PARSING_ERROR);
            mLogger.severe(expMsg);
            throw new JBIUpgradeException(expMsg);
        }
    }

    /** getChildElement
     * @param element
     * @param tag
     * @return child element
     */
    private Element getChildElement(Element element, String tag)
    {
        Element rtnElem = null;
        NodeList childNodeList = element.getElementsByTagName(tag);
        if (childNodeList.getLength() > 0)
        {
            rtnElem = (Element) childNodeList.item(0);
        }

        return rtnElem;
    }

    /** getChildElement
     * @param element
     * @param tag
     * @param nameRef
     * @return child element
     */
    private Element getChildElement(Element element, String tag, String nameRef)
    {
        Element rtnElem = null;
        NodeList childNodeList = element.getElementsByTagName(tag);
        for (int i = 0; i < childNodeList.getLength(); ++i)
        {
            rtnElem = (Element) childNodeList.item(i);
            String tmpRef = rtnElem.getAttribute(UpgradeToolConstants.NAME_REF);
            if ((""+tmpRef).compareTo(nameRef) == 0)
            {
                return rtnElem;
            }
        }

        return null;
    }

    /** Update the update number
     * @param element
     */
    private void updateUpgradeNumber(Element element)
    {
        // Update the update-number
        Attr upNoAttr = 
                element.getAttributeNode(UpgradeToolConstants.UPGRADE_NUMBER);
        if (upNoAttr == null)
        {
            element.setAttribute(UpgradeToolConstants.UPGRADE_NUMBER, "1");
        }
        else
        {
            int theNumber = Integer.parseInt(upNoAttr.getNodeValue());
            theNumber = theNumber + 1;
            element.setAttribute(UpgradeToolConstants.UPGRADE_NUMBER, Integer.toString(theNumber));
        }
    }

    /**
     * Private method getNewBuildNumber
     * @param appServerInstallRoot
     * @param componentDir
     * @param compName
     * @return String
     * @throws JBIUpgradeException
     */
    private String getNewBuildNumber(String appServerInstallRoot,
                                     String componentDir,
                                     String compName)
        throws JBIUpgradeException
    {
        JBIXMLHandler jbiXMLHandler = new JBIXMLHandler();
        try
        {
            String jarPath = mUtil.getInstallJarPathInAppServerInstallRoot(
                                     appServerInstallRoot,
                                     componentDir,
                                     compName);

            if (jarPath == null)
            {
                return null;
            }

            InputStream jbiXMLStream =
                (new JarFile(new File(jarPath))).getInputStream(
                    new JarEntry(UpgradeToolConstants.JBI_META_INF_DIR +
                                 "/jbi.xml"));

            SAXParser saxParser = mSAXParserFactory.newSAXParser();
            saxParser.parse(jbiXMLStream, jbiXMLHandler);
        }
        catch(IOException exp)
        {
            String expMsg =
                mUtil.getI18NMessage(UpgradeToolConstants.UNKNOWN_IO_EXCEPTION);
            mLogger.severe(expMsg);
            throw new JBIUpgradeException(expMsg);
        }
        catch(ParserConfigurationException exp)
        {
            String expMsg =
                mUtil.getI18NMessage(UpgradeToolConstants.JBI_XML_PARSING_EXCEPTION);
            mLogger.severe(expMsg);
            throw new JBIUpgradeException(UpgradeToolConstants.JBI_XML_PARSING_EXCEPTION);
        }
        catch(SAXException exp)
        {
            String expMsg =
                mUtil.getI18NMessage(UpgradeToolConstants.JBI_XML_PARSING_EXCEPTION);
            mLogger.severe(expMsg);
            throw new JBIUpgradeException(expMsg);
        }

        return jbiXMLHandler.getBuildNumber();
    }

    /**
     * Private method getExistingBuildNumber
     * @param domainRoot
     * @param compName
     * @param componentDir
     * @return String
     * @throws JBIUpgradeException
     */
    private String getExistingBuildNumber(String domainRoot,
                                          String compName,
                                          String componentDir)
        throws JBIUpgradeException
    {
        JBIXMLHandler jbiXMLHandler = new JBIXMLHandler();
        try
        {
            File jbiXMLFile = new File(domainRoot +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_DIR +
                                        File.separator +
                                        componentDir +
                                        File.separator +
                                        compName.trim() +
                                        File.separator +
                                        UpgradeToolConstants.JBI_INSTALL_ROOT_DIR +
                                        File.separator +
                                        UpgradeToolConstants.JBI_META_INF_DIR +
                                        File.separator +
                                        "jbi.xml");

            if (!jbiXMLFile.exists())
            {
                String expMsg =
                    this.getResourceBundle().getString(
                        UpgradeToolConstants.JBI_XML_NOT_FOUND);

                throw new JBIUpgradeException(expMsg);
            }

            SAXParser saxParser = mSAXParserFactory.newSAXParser();
            saxParser.parse(jbiXMLFile, jbiXMLHandler);
        }
        catch (Exception ex)
        {
            String expMsg = mUtil.getI18NMessage(
                    UpgradeToolConstants.JBI_XML_PARSING_EXCEPTION);
            mLogger.severe(expMsg);
            throw new JBIUpgradeException(expMsg);
        }

        return jbiXMLHandler.getBuildNumber();
    }

    /**
     * This method is used to get ResourceBundle
     * @return ResourceBundle
     */
    public ResourceBundle getResourceBundle(){
        return mResourceBundle;
    }

    /**
     * Private method createLocalLogger
     * @return Logger
     */
    private Logger createLocalLogger()
    {
        return Logger.getLogger(UpgradeToolConstants.DEFAULT_LOGGER_NAME);
    }
}

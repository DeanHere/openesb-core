/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBICoreInstaller.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.installer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Date;
import java.util.zip.ZipException;
import java.io.FileReader;
import java.io.BufferedReader;

/**
 * This class is the main entry point to the installer for
 * JBI Addons Lite.
 */
public class JBICoreInstaller {

    public static final String DATE_FORMAT = "yyyyMMdd";
    private String mAntScriptName = null;
    private String mAntBaseDir = null;
    private Logger mLogger = null;
    private Properties mCmdLineParams = new Properties();
    private boolean mIsJBICoreInstalled = true;
    private String isErrorMessage = "";
	private boolean mIsJBICoreUninstalled = true;
	private ResourceBundle resourceBundle = null;

    /**
     * The main method of the JBI components Addons installer.
	 * This method takes the appserver installation dir as the
     * first argument
     * @param args, the first argument should be appserver install dir
     *
     */
    public static void main(String[] args)
    {
        JBICoreInstaller jbiCoreInstaller = null;
        try
        {
            jbiCoreInstaller = new JBICoreInstaller();
            jbiCoreInstaller.setResourceBundle();
            jbiCoreInstaller.setLogger();

            if (args.length < 2) {
                jbiCoreInstaller.getLogger().severe(jbiCoreInstaller.getResourceBundle().getString("usage"));
				return;
            }

			String asInstallDir = args[0];

			boolean overwrite = false;
			if (args.length == 3) {
				if (args[2].equalsIgnoreCase("overwrite")) {
					overwrite = true;
				}
			}

            if (!asInstallDir.equals(".") && args[1].equalsIgnoreCase("install")) {
				jbiCoreInstaller.install(asInstallDir, overwrite);
				if (jbiCoreInstaller.mIsJBICoreInstalled){
					 jbiCoreInstaller.getLogger().info(
							jbiCoreInstaller.getResourceBundle().getString(
							"installation-successful") + asInstallDir + File.separator +
							InstallConstants.JBI_INSTALL_DIR);
				} else {
					 jbiCoreInstaller.getLogger().severe(
							jbiCoreInstaller.getResourceBundle().getString("installation-failed") + jbiCoreInstaller.isErrorMessage);
				}
			} else if (!asInstallDir.equals(".") && args[1].equalsIgnoreCase("uninstall")) {
				System.out.println("Uninstall is not supported at this time.");

				/*  jbiCoreInstaller.uninstall(asInstallDir);
					if (jbiCoreInstaller.mIsJBICoreUninstalled){
						 jbiCoreInstaller.getLogger().info(
								jbiCoreInstaller.getResourceBundle().getString(
											"uninstallation-successful") + asInstallDir + File.separator +
											InstallConstants.JBI_INSTALL_DIR);
					} else {
						 jbiCoreInstaller.getLogger().severe(
								jbiCoreInstaller.getResourceBundle().getString("uninstallation-failed"));
					} */
			} else {
				jbiCoreInstaller.getLogger().severe(
				jbiCoreInstaller.getResourceBundle().getString("usage"));
				return;
			}

        } catch(Exception e) {
            jbiCoreInstaller.getLogger().severe(e.getMessage());
        }

    }

    /**
     * This method is used to install JBI components
     * @param InstallationContext ic
     */
    public void install(String appServerInstallRoot, boolean overwrite)
    {
        String timeStamp = "";
        try {
            String pattern = "yyyyMMddHHmm";
            String underscore = "_";
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            String result = df.format(new Date());
            timeStamp = underscore + result;
        } catch (Exception e) {
        //Here we do not do anything and the timestamp will be an empty string
        }

        mLogger = Logger.getLogger("com.sun.jbi.installer.JBICoreInstaller");


		// Check the AS is complete installed and configured in order to run this installer
        String asenv="";
		if(System.getProperty("os.name").contains("Windows")) {
			asenv = appServerInstallRoot + File.separator + "config" + File.separator + "asenv.bat";
		} else {
			asenv = appServerInstallRoot + File.separator + "config" + File.separator + "asenv.conf";
		}
		File asenvFile = new File(asenv);
		if (!asenvFile.exists()) {
        	mIsJBICoreInstalled = false;
			isErrorMessage = getResourceBundle().getString("Unable-to-execute-ant-script");
			return;
		}


        mAntBaseDir = System.getProperty("java.io.tmpdir") +
                          File.separator + "jbi-core-installer" + timeStamp;

        mAntScriptName = mAntBaseDir + File.separator +
                            InstallConstants.JBI_INSTALL_SCRIPT;

        String currentPath = getCurrentPath();
        currentPath = new File(currentPath).getAbsolutePath();
        String jbiCoreInstallerJar = currentPath;

        File baseDir = new File(mAntBaseDir);
        if(!baseDir.exists()) {
           baseDir.mkdirs();
        }

        try {
            JarFactory jrFctry = new JarFactory(mAntBaseDir);
            jrFctry.unJar(new File(jbiCoreInstallerJar));
        } catch (ZipException zipEx) {
            mIsJBICoreInstalled = false;
            mLogger.severe(getResourceBundle().getString("jar-exception") + jbiCoreInstallerJar +  zipEx.getMessage());
        } catch (IOException ioEx) {
            mIsJBICoreInstalled = false;
            mLogger.severe(getResourceBundle().getString("copy-jar-to-as-dir") + jbiCoreInstallerJar + ioEx.getMessage());
        }

        if (!overwrite) {
			try {
        		FileReader reader = new FileReader(appServerInstallRoot + File.separator + "jbi" + File.separator + "jbi.ver");
        		BufferedReader br = new BufferedReader(reader);
        		String inputLine = null;
        		String buildNumber = null;
        		while ((inputLine = br.readLine()) != null) {
            	// build number should be BUILD_NUMBER="071127_2"
					if (inputLine.contains("BUILD_NUMBER")) {
		    			buildNumber = inputLine;
						break;
					}
        		}
        		br.close();
        		reader.close();
        		String asBuildNumber = "20" + buildNumber.substring(14, 20);

        		File jbiFile = new File(mAntBaseDir + File.separator + "jbi-install.jar");
        		Date jbiDate = new Date(jbiFile.lastModified());
        		SimpleDateFormat SDFDATE = new SimpleDateFormat(DATE_FORMAT);
        		SDFDATE.setLenient(false);
        		String jbiDateString = SDFDATE.format(jbiDate);

        		if (asBuildNumber.compareTo(jbiDateString) > 0) {
					System.out.println("\nThe installer is too old to replace the JBI in the application server.\n");
					mIsJBICoreInstalled = false;
					isErrorMessage = "The installer is too old to replace the JBI in the application server\n";
            		try {
        				deleteDirAndContents(baseDir, true);
					} catch (Exception e) {
						mLogger.warning("Removing temp folder failed:\n" +
    									e.getMessage());
					}
					return;
        		}
			} catch (FileNotFoundException fnfe) {
			} catch (IOException ioe) {
			}
		}

        // if AS_INSTALL for windows contains \, replace with / and
        // set them in build.properties file
        if (appServerInstallRoot.indexOf("\\") != -1) {
		    appServerInstallRoot = appServerInstallRoot.replace('\\', '/');
        }

        mCmdLineParams.setProperty(
            InstallConstants.AS_INSTALL_DIR, appServerInstallRoot);


        //the log entries in tmp dir and then move it to jbi dir
        String tempLogFile = mAntBaseDir + File.separator +
                             "jbi-core-installer.log";

        try {
	    AntRunner ant = new AntRunner();
            if (!ant.runAnt(appServerInstallRoot, mAntScriptName,
                InstallConstants.JBI_INSTALL_TARGET, mCmdLineParams, tempLogFile))
            {
                mLogger.severe(getResourceBundle().getString(
                        "ant-script-failed") + mAntScriptName);
            }
        } catch (Exception e) {
            mIsJBICoreInstalled = false;
            mLogger.severe(getResourceBundle().getString(
                       "Unable-to-execute-ant-script") + e.getMessage());
        }

        String logFile = appServerInstallRoot + File.separator +
                    	InstallConstants.JBI_INSTALL_DIR +File.separator +
			"jbi-core-installer.log";
        try {
				File tmpLogFile = new File(tempLogFile);
            InstallerUtilities.copyFile(tmpLogFile, new File(logFile));
				tmpLogFile = null;

        } catch (Exception e) {
             mLogger.warning(getResourceBundle().getString(
                       "error-in-moving-log-file") + logFile + "\n" +  e.getMessage());
        }
        try {
        		deleteDirAndContents(baseDir, true);
        } catch (Exception e) {
             mLogger.warning("Removing temp folder failed:\n" +
									e.getMessage());
        }
    }

    /*
     * This method is used to get the current path from the java.class.path
     */
	private String getCurrentPath() {
		String delimiter = "";
		String javaClassPath = System.getProperty("java.class.path");
		if (System.getProperty("os.name").contains("SunOS") ||
			System.getProperty("os.name").contains("Linux") ||
            System.getProperty("os.name").contains("Mac OS X")) {
				delimiter = ":";
		} else if (System.getProperty("os.name").contains("Windows")) {
					delimiter = ";";
		}
		String[] paths = javaClassPath.split(delimiter);
		for (int i = 0; i < paths.length; i++) {
			if (paths[i].contains("jbi-core-installer.jar")) {
				return paths[i];
			}
		}
		return javaClassPath;
	}


    /**
     * This method is used to uninstall JBI Addons Lite
     * @param InstallationContext ic
     */
    public void uninstall(String appServerInstallRoot)
    {
    	mLogger = Logger.getLogger("com.sun.jbi.installer.JBICoreInstaller");

		try {
			String uninstallDirStr = appServerInstallRoot + File.separator + "jbi";
			File uninstallDir = new File(uninstallDirStr);
			deleteDirAndContents(uninstallDir, true);

			String uninstallFileStr = appServerInstallRoot + File.separator + "jbi-install.jar";
			File uninstallFile = new File(uninstallFileStr);
			uninstallFile.delete();

	      mLogger.info(
	            getResourceBundle().getString(
	            "uninstallation-successful") + appServerInstallRoot);

		} catch (Exception e) {
			mIsJBICoreUninstalled = false;
			mLogger.severe(getResourceBundle().getString(
                       "error-in-uninstall-core") + e.getMessage());
		}
    }

    public boolean deleteDirAndContents(File path, boolean recursive) {
        if(path.exists()) {
            File[] files = path.listFiles();
            for(int i=0; i<files.length; i++) {
                if(files[i].isDirectory()) {
                    if (recursive) {
                        deleteDirAndContents(files[i], recursive);
                    }
                } else {
                    boolean deleted = files[i].delete();
						  if (!deleted) {
								files[i].deleteOnExit();
						  }
                }
            }
        }
        boolean removed = path.delete();
		  if (!removed) {
				path.deleteOnExit();
		  }
		  return true;
    }

  /**
   * This method is used to set Logger
   */
  private void setLogger()
  {
     mLogger = Logger.getLogger("com.sun.jbi.installer.JBIComponentsInstaller");
  }

  /**
   * This method is used to get Logger
   */
  private Logger getLogger()
  {
     return mLogger;
  }

  /**
   * This method is used to set ResourceBundle
   */
  private void setResourceBundle(){
      resourceBundle =
              ResourceBundle.getBundle(InstallConstants.RESOURCE_BUNDLE);
  }

  /**
   * This method is used to get ResourceBundle
   */
  public ResourceBundle getResourceBundle(){
      return resourceBundle;
  }

}

<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>

<% 
  WikiContext wikiContext = WikiContext.findContext(pageContext);
  String pagename = wikiContext.getPage().getName(); 

  String username = null;
  //username = wikiContext.getEngine().getVariable( wikiContext, "username" );
  username = wikiContext.getWikiSession().getUserPrincipal().getName();                           
  if( username == null ) username = "";
  String myFav = username + "Favorites";

  String prefShowCalendar = (String) session.getAttribute("prefShowCalendar");
%>
<div id="favorites">

<%-- username --%>
<div class="username">G'day,
  <wiki:UserCheck status="anonymous">
    <wiki:Link page='Login'
              title="Hi, Anonymous Guest">Anonymous Guest</wiki:Link>
  </wiki:UserCheck>
  <%-- FIXME: asserted user sometimes gets "NULL" for wiki:UserName --%>
  <wiki:UserCheck status="asserted">
    <a href="<wiki:LinkTo page='UserPreferences' format='url' />"
      title="G'day <wiki:UserName/> (not logged in)"><wiki:UserName/></a>         
  </wiki:UserCheck>
  <wiki:UserCheck status="authenticated">
    <wiki:Translate>[<wiki:UserName />]</wiki:Translate>
  </wiki:UserCheck>
</div>  

<%-- myfavorites --%>
<wiki:UserCheck status="known">
  <wiki:PageExists page="<%=myFav%>">
    <div class="collapsebox">
       <h4><wiki:Link page="<%=myFav%>" >My Favorites</wiki:Link></h4>
      <wiki:InsertPage page="<%=myFav%>"/>
    </div>
  </wiki:PageExists>
</wiki:UserCheck>

<%-- calendar stuff : only shown when showCalendar variable is set --%>
<% if( prefShowCalendar.equals("yes") ) { %>
<div class="collapsebox">
  <h4>Calendar</h4>
  <div align='center'>
  <wiki:Calendar pageformat="<%="\'"+pagename+"_blogentry_'ddMMyy'_1'"%>"
                  urlformat="'Wiki.jsp?page=%p&weblog.startDate='ddMMyy'&weblog.days=1'"  
             monthurlformat="'Wiki.jsp?page=%p&weblog.startDate='ddMMyy'&weblog.days=%d'" />
  </div>
</div>
<% } %>

<%-- LeftMenu is automatically generated from a Wiki page called "LeftMenu" --%>
<wiki:InsertPage page="LeftMenu" />
<wiki:NoSuchPage page="LeftMenu">
  <div class="error">
    <wiki:EditLink page="LeftMenu">Please make a LeftMenu.</wiki:EditLink>
  </div>
</wiki:NoSuchPage>

<%-- <wiki:Include page="PageActions.jsp"/> --%>
  
<wiki:InsertPage page="LeftMenuFooter" />
<wiki:NoSuchPage page="LeftMenuFooter">
  <div class="error">
    <wiki:EditLink page="LeftMenuFooter">Please make a LeftMenuFooter.</wiki:EditLink>
  </div>
</wiki:NoSuchPage>

<div class="wikiversion"><%=Release.APPNAME%> v<%=Release.getVersionString()%></div>

<div class="rssfeed"><wiki:RSSImageLink title="Aggregate the RSS feed of the entire wiki" /></div>

<div style="clear:both; height:0px;" > </div>

</div>
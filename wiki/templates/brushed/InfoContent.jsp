<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%
  WikiContext wikiContext = WikiContext.findContext(pageContext);  
  int attCount = wikiContext.getEngine().getAttachmentManager()
                            .listAttachments( wikiContext.getPage() ).size();
  String attTitle = "Attach";
  if( attCount != 0 ) attTitle += " (" + attCount + ")";
  boolean showAttachTab = (attCount > 0 );
%>
<wiki:Permission permission="upload">
<% showAttachTab = true; %>
</wiki:Permission>

<wiki:TabbedSection defaultTab="pageinfo">  
  <wiki:PageType type="page">
    <wiki:Tab id="pagecontent" title="View" accesskey="v">
      <wiki:Include page="PageTab.jsp"/>
    </wiki:Tab> 
    <wiki:PageExists>
    <% if( showAttachTab ) { %>
    <wiki:Tab id="attachments" title="<%= attTitle %>" accesskey="a">
      <wiki:Include page="AttachmentTab.jsp"/>
    </wiki:Tab>
    <% } %>
    <wiki:Tab id="pageinfo" title="Info" accesskey="i">
      <wiki:Include page="InfoTab.jsp"/>
    </wiki:Tab>
    </wiki:PageExists>
  </wiki:PageType>
  <wiki:PageType type="attachment">
    <wiki:Tab id="pageinfo" title="Attachment Info" >
      <wiki:Include page="InfoTab.jsp"/>
    </wiki:Tab>
  </wiki:PageType>
</wiki:TabbedSection>
<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="com.ecyrd.jspwiki.ui.*" %>
<% /* see commonheader.jsp */
   String prefSkinName       = (String) session.getAttribute("prefSkinName");
   String prefDateFormat     = (String) session.getAttribute("prefDateFormat");
   String prefTimeZone       = (String) session.getAttribute("prefTimeZone");
   String prefEditAreaHeight = (String) session.getAttribute("prefEditAreaHeight");
   String prefEditorType     = (String) session.getAttribute("prefEditorType");
   String prefShowQuickLinks = (String) session.getAttribute("prefShowQuickLinks");
   String prefShowCalendar   = (String) session.getAttribute("prefShowCalendar");
%>

<h3>Set your user preferences here.</h3>

<form action="<wiki:BaseURL/>UserPreferences.jsp?tab=prefs" 
       class="wikiform"
      method="post" accept-charset="<wiki:ContentEncoding />" 
    onsubmit="document.setCookie( 'JSPWikiUserPrefs',
             this.prefSkin[this.prefSkin.selectedIndex].value + Wiki.DELIM +
             this.prefTimeFormat[this.prefTimeFormat.selectedIndex].value + Wiki.DELIM +
             this.prefTimeZone[this.prefTimeZone.selectedIndex].value + Wiki.DELIM +
             this.prefEditSize[this.prefEditSize.selectedIndex].text + Wiki.DELIM +
             ( this.prefShowQuickLinks.checked ? 'yes' : 'no' ) + Wiki.DELIM +
             ( this.prefShowCalendar.checked ? 'yes' : 'no' ) + Wiki.DELIM +
             this.prefEditorType[this.prefEditorType.selectedIndex].value
             ); 
             return WikiForm.submitOnce( this ); " >

  <% if( "prefs".equals( request.getParameter( "tab" ) ) ) { %>
  <wiki:Messages div="error" topic="profile" prefix="Could not save preferences: "/>
  <% } %>

  <table>

  <wiki:UserCheck status="anonymous">
  <tr>
  <td><label for="username" accesskey="N">Wiki <u>n</u>ame</label></td>
  <td>
  <input type="text" id="assertedName" name="assertedName" size="24" value="<wiki:UserName/>" />
  <div class="formhelp">
  This must be a proper <wiki:LinkTo page="WikiName">WikiName</wiki:LinkTo>, no punctuation.
  If you haven't created a user profile yet, you can tell 
  <wiki:Variable var="applicationname" /> 
  who you are by 'asserting' an identity. You wouldn't lie to us would you?
  <br />
  Note that setting your user name this way isn't a particularly trustworthy method of
  authentication, and the wiki may grant you fewer privileges as a result. 
  <a href="javascript://nop/" 
     onclick="TabbedSection.onclick('userProfile'); return false" 
     title="Create new user profile">Create a user profile</a> 
  if you'd prefer a traditional username and password, which is more secure.
  </div>
  </td>
  </tr>
  </wiki:UserCheck>
  
  <tr>
  <td><label for="prefSkin" accesskey="S"><u>S</u>kin</label></td>
  <td>
  <select id="prefSkin" name="prefSkin">
    <%
      WikiContext wikiContext = WikiContext.findContext(pageContext);
      TemplateManager mgr = wikiContext.getEngine().getTemplateManager();
      java.util.Set skins = mgr.listSkins(pageContext, wikiContext.getTemplate());
      //java.util.Collections.sort(skins);
      for( java.util.Iterator i = skins.iterator(); i.hasNext(); )
      {
        String skinName = (String)i.next();
        /* FIXME: listSkins lists everything -- hidden dirs, but also non-dirs! */
        if( skinName.startsWith( "." ) ) continue;
        String selected = ( prefSkinName.equals( skinName) ? " selected='selected'" : "" ) ;
     %>
        <option value="<%= skinName %>" <%= selected%> ><%= skinName %></option>
     <%
      }
     %>
  </select>
  </td>
  </tr>
  
  <tr>
  <td><label for="prefEditorType" accesskey="e"><u>E</u>ditor Type</label></td>
  <td>
  <select id="prefEditorType" name="prefEditorType" >
    <option <%= (prefEditorType.equals("plain")) ? "selected=\'selected\'" : "" %> value="plain">Standard wiki-markup editor</option>
    <%-- uncomment to activate -- dont forget to install fck --%>
    <option <%= (prefEditorType.equals("FCK")) ? "selected=\'selected\'" : "" %> value="FCK">WYSIWIG editor (FCK)</option>
  </select>
  </td>
  </tr>

  <tr>
  <td><label for="prefEditSize" accesskey="h">Editor area <u>h</u>eight</label></td>
  <td>
  <select id="prefEditSize" name="prefEditSize" >
    <%
      String[] editsizes = { "20", "24", "28", "32", "36", "72" } ;
      for( int i=0; i < editsizes.length; i++ )
      {
        String selected = ( prefEditAreaHeight.equals( editsizes[i] ) ? " selected='selected'" : "" ) ;
     %>
        <option <%=selected%> ><%= editsizes[i] %></option>
     <%
      }
    %>
  </select>
  </td>
  </tr>

  <tr>
  <td><label for="prefTimeFormat" accesskey="T">Select <u>T</u>ime Format</label></td>
  <td>
  <select id="prefTimeFormat" name="prefTimeFormat" >
    <%
      String[] arrTimeFormat = 
      {"d/MM"
      ,"d/MM/yy"
      ,"d/MM/yyyy"
      ,"dd/MM/yy"
      ,"dd/MM/yyyy"
      ,"EEE, dd/MM/yyyy"
      ,"EEE, dd/MM/yyyy, Z"
      ,"EEE, dd/MM/yyyy, zzzz"
      ,"d/MM/yy hh:mm"
      ,"d/MM/yy HH:mm a"
      ,"d/MM/yy HH:mm a, Z"
      ,"dd-MMM"
      ,"dd-MMM-yy"
      ,"dd-MMM-yyyy"
      ,"EEE, dd-MMM-yyyy"
      ,"EEE, dd-MMM-yyyy, Z"
      ,"EEE, dd-MMM-yyyy, zzzz"
      ,"dd-MMM-yy hh:mm"
      ,"dd-MMM-yy HH:mm a"
      ,"dd-MMM-yy HH:mm a, Z"
      ,"MMMM dd, yyyy"
      ,"MMMM dd, yyyy hh:mm"
      ,"MMMM dd, yyyy HH:mm a"
      ,"MMMM, EEE dd,yyyy HH:mm a"
      ,"MMMM, EEEE dd,yyyy HH:mm a"
      } ;
      java.util.Date d = new java.util.Date() ;  // Now.
      for( int i=0; i < arrTimeFormat.length; i++ )
      {
        String f = arrTimeFormat[i];
        String selected = ( prefDateFormat.equals( f ) ? " selected='selected'" : "" ) ;
        try
        {
          java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( f );
          java.util.TimeZone tz = java.util.TimeZone.getDefault();
          try 
          {
            tz.setRawOffset( Integer.parseInt( prefTimeZone ) );
          }
          catch( Exception e) { /* dont care */ } ;
          fmt.setTimeZone( tz );
    %>
          <option value="<%= f %>" <%= selected%> ><%= fmt.format(d) %></option>
   <%
        }
        catch( IllegalArgumentException e ) { } // skip parameter
      }
    %>
  </select>
  </td>
  </tr>

  <tr>
  <td><label for="prefTimeZone" accesskey="Z">Select Time <u>Z</u>one</label></td>
  <td>
  <select id='prefTimeZone' name='prefTimeZone' class='select'>
    <% 
       String[][] tzs = 
       { { "-43200000" , "(UTC-12) Enitwetok, Kwajalien" }
       , { "-39600000" , "(UTC-11) Nome, Midway Island, Samoa" }
       , { "-36000000" , "(UTC-10) Hawaii" }
       , { "-32400000" , "(UTC-9) Alaska" }
       , { "-28800000" , "(UTC-8) Pacific Time" }
       , { "-25200000" , "(UTC-7) Mountain Time" }
       , { "-21600000" , "(UTC-6) Central Time, Mexico City" }
       , { "-18000000" , "(UTC-5) Eastern Time, Bogota, Lima, Quito" }
       , { "-14400000" , "(UTC-4) Atlantic Time, Caracas, La Paz" }
       , { "-12600000" , "(UTC-3:30) Newfoundland" }
       , { "-10800000" , "(UTC-3) Brazil, Buenos Aires, Georgetown, Falkland Is." }
       , {  "-7200000" , "(UTC-2) Mid-Atlantic, Ascention Is., St Helena" }
       , {  "-3600000" , "(UTC-1) Azores, Cape Verde Islands" }
       , {         "0" , "(UTC) Casablanca, Dublin, Edinburgh, London, Lisbon, Monrovia" }
       , {   "3600000" , "(UTC+1) Berlin, Brussels, Copenhagen, Madrid, Paris, Rome" }
       , {   "7200000" , "(UTC+2) Kaliningrad, South Africa, Warsaw" }
       , {  "10800000" , "(UTC+3) Baghdad, Riyadh, Moscow, Nairobi" }
       , {  "12600000" , "(UTC+3.30) Tehran" }
       , {  "14400000" , "(UTC+4) Adu Dhabi, Baku, Muscat, Tbilisi" }
       , {  "16200000" , "(UTC+4:30) Kabul" }
       , {  "18000000" , "(UTC+5) Islamabad, Karachi, Tashkent" }
       , {  "19800000" , "(UTC+5:30) Bombay, Calcutta, Madras, New Delhi" }
       , {  "21600000" , "(UTC+6) Almaty, Colomba, Dhakra" }
       , {  "25200000" , "(UTC+7) Bangkok, Hanoi, Jakarta" }
       , {  "28800000" , "(UTC+8) Beijing, Hong Kong, Perth, Singapore, Taipei" }
       , {  "32400000" , "(UTC+9) Osaka, Sapporo, Seoul, Tokyo, Yakutsk" }
       , {  "34200000" , "(UTC+9:30) Adelaide, Darwin" }
       , {  "36000000" , "(UTC+10) Melbourne, Papua New Guinea, Sydney, Vladivostok" }
       , {  "39600000" , "(UTC+11) Magadan, New Caledonia, Solomon Islands" }
       , {  "43200000" , "(UTC+12) Auckland, Wellington, Fiji, Marshall Island" }
       };
       String servertz = Integer.toString( java.util.TimeZone.getDefault().getRawOffset() ) ;
       String selectedtz = servertz;
       for( int i=0; i < tzs.length; i++ )
       {
         if( prefTimeZone.equals( tzs[i][0] ) ) selectedtz = prefTimeZone;
       }
       for( int i=0; i < tzs.length; i++ )
       {
         String selected = ( selectedtz.equals( tzs[i][0] ) ? " selected='selected'" : "" ) ;
         String server = ( servertz.equals( tzs[i][0] ) ? " [SERVER]" : "" ) ;
    %>
        <option value="<%= tzs[i][0] %>" <%= selected%> ><%= tzs[i][1]+server %></option>
   <%
       }
    %>    
  </select>
  </td>
  </tr>
   
  <tr>
  <td><label for="prefLanguage">Select Language</label></td>
  <td>
  <select id="prefLanguage" name="prefLanguage" >
    <option value="">English</option>
  </select>
  </td>
  </tr>
  
  <tr>
  <td><label for="prefShowQuickLinks">Show Quick Links</label></td>
  <td>
  <input class='checkbox' type='checkbox' id='prefShowQuickLinks' name='prefShowQuickLinks' 
         <%= (prefShowQuickLinks.equals("yes") ? "checked='checked'" : "") %> />
         <span class="quicklinks"><span 
               class='quick2Top'><a href='#wikibody' title='Go to Top' >&laquo;</a></span><span 
               class='quick2Prev'><a href='#' title='Go to Previous Section'>&lsaquo;</a></span><span 
               class='quick2Edit'><a href='#' title='Edit this section'>&bull;</a></span><span 
               class='quick2Next'><a href='#' title='Go to Next Section'>&rsaquo;</a></span><span 
               class='quick2Bottom'><a href='#footer' title='Go to Bottom' >&raquo;</a></span></span>
  </td>
  </tr>

  <tr>
  <td><label for="prefShowCalendar">Show Calendar</label></td>
  <td>
    <input class='checkbox' type='checkbox' id='prefShowCalendar' name='prefShowCalendar' 
            <%= (prefShowCalendar.equals("yes") ? "checked='checked'": "") %> >
  </td>
  </tr>

  <tr>
  <td>
    <input type="submit" value="Save User Preferences" name="ok" style="display:none;"/>
    <input type="button" value="Save User Preferences" name="proxy1" onclick="this.form.ok.click();" />
    <input type="hidden" name="action" value="setAssertedName" />
    &nbsp;&nbsp;&nbsp;&nbsp;
  </td>
  <td class="formhelp">Your choices will be saved in your browser as cookies.</td>
  </tr>

  </table>
  
</form>

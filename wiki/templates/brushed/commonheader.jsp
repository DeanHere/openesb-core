<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="com.ecyrd.jspwiki.ui.*" %>
<%@ page import="java.util.*" %>
<%
  // preferences cookie
  // here are the DEFAULT user preferences -- change these values if you want different defaults
  String DELIM              = "\u00a4"; 
  String prefDateFormat     = "dd-MMM-yy HH:mm a, Z";
  String prefSkinName       = "Smart";
  String prefTimeZone       = "";
  String prefEditAreaHeight = "24";
  String prefEditorType     = "plain";
  String prefShowQuickLinks = "no";
  String prefShowCalendar   = "no" ;

  Cookie[] cookies = request.getCookies();  
  if (cookies != null)
  {
    for (int i = 0; i < cookies.length; i++) 
    {   
       if( "JSPWikiUserPrefs".equals( cookies[i].getName() ) ) 
       {
         String s = TextUtil.urlDecodeUTF8( cookies[i].getValue() ) ;

         String[] ss = s.split( DELIM );
         int sslen = ss.length;
         
         if( sslen > 1 ) prefSkinName       = ss[0]; //more robust -- at least 2 parms recognised
         if( sslen > 1 ) prefDateFormat     = ss[1];
         if( sslen > 2 ) prefTimeZone       = ss[2];
         if( sslen > 3 ) prefEditAreaHeight = ss[3];
         if( sslen > 4 ) prefShowQuickLinks = ss[4];
         if( sslen > 5 ) prefShowCalendar   = ss[5];
         if( sslen > 6 ) prefEditorType     = ss[6];
                       
         break;
       }
    } 
  }  
  session.setAttribute("prefSkinName",       prefSkinName );
  session.setAttribute("prefDateFormat",     prefDateFormat );
  session.setAttribute("prefTimeZone",       prefTimeZone );
  session.setAttribute("prefEditAreaHeight", prefEditAreaHeight );
  session.setAttribute("prefEditorType",     prefEditorType );
  session.setAttribute("prefShowQuickLinks", prefShowQuickLinks );
  session.setAttribute("prefShowCalendar",   prefShowCalendar );
 
 %>

<link rel="stylesheet" type="text/css" media="screen, projection" 
     href="<wiki:Link format='url' templatefile='jspwiki.css'/>" />

<!-- browser specific cssinclude not required in brushed template
<script type="text/javascript"
         src="<wiki:Link format='url' templatefile='cssinclude.js'/>"></script>
<script type="text/javascript">
//<![CDATA[
  Wiki.loadBrowserSpecificCSS("<wiki:TemplateDir/>");
//]]>
</script>
-->

<%--
<script src="<wiki:BaseURL/>scripts/search_highlight.js" type="text/javascript"></script>
<script src="<wiki:BaseURL/>scripts/jspwiki-common.js" type="text/javascript"></script>
--%>
<%-- FIXME: check is prefSkinName exists -- otherwise fall back to PlainVanilla --%>

<link rel="stylesheet" type="text/css" 
     href="<wiki:BaseURL/>templates/<wiki:TemplateDir/>/skins/<%= prefSkinName %>/skin.css" />
<wiki:IncludeResources type="stylesheet"/>

<%-- hack to show transparent png's in IE --%>
<!--[if IE]>
<style type="text/css" media="screen">
img, a, div { behavior: url("templates/<wiki:TemplateDir/>/scripts/iepngfix.htc"); }
</style>
<![endif]-->

<meta http-equiv="Content-Type" content="text/html; charset=<wiki:ContentEncoding />" />
<link rel="search" 
     href="<wiki:LinkTo format="url" page="FindPage"/>"
    title="Search <wiki:Variable var="ApplicationName" />" />
<link rel="help"   
     href="<wiki:LinkTo format="url" page="TextFormattingRules"/>" 
    title="Help" />
<link rel="start"  
     href="<wiki:LinkTo format="url" page="Main"/>"                
    title="Front page" />
<!--<link rel="stylesheet" type="text/css" media="print"
     href="<wiki:Link format='url' templatefile='jspwiki_print.css'/>" />
<link rel="alternate stylesheet" type="text/css"
     href="<wiki:Link format='url' templatefile='jspwiki_print.css'/>" 
    title="Print friendly" />-->
<link rel="alternate stylesheet" type="text/css" 
     href="<wiki:Link format='url' templatefile='jspwiki.css'/>" 
    title="Standard" />
<link rel="icon" type="image/png" href="<wiki:Link format='url' jsp='images/openesb16x16.png'/>" />
<%-- support categories / fixme --%>
<link rel="template" href="<wiki:BaseURL/>templates/<wiki:TemplateDir/>" id="template-dir"/>
<wiki:FeedDiscovery />

<%-- add stuff for the brushed template --%>
<script type="text/javascript"
         src="<wiki:Link format='url' templatefile='scripts/prototype.js'/>" ></script>
<script type="text/javascript" 
         src="<wiki:Link format='url' templatefile='scripts/brushed.js'/>" ></script>

<wiki:CheckRequestContext context='edit|comment'>
<script type="text/javascript" 
         src="<wiki:Link format='url' templatefile='scripts/brushedEdit.js'/>" ></script>
</wiki:CheckRequestContext>

<script type="text/javascript">
//<![CDATA[
  Wiki.setBaseURL ( "<wiki:BaseURL />" );
  Wiki.setPageName( "<wiki:Variable var="pagename" />" ); /* pagename without blanks */
  Wiki.setUserName( "<wiki:UserName />" );

  Wiki.setDELIM              ( "<%=DELIM %>" );
  Wiki.setPrefDateFormat     ( "<%=prefDateFormat %>" );
  Wiki.setPrefSkinName       ( "<%=prefSkinName %>" );
  Wiki.setPrefTimeZone       ( "<%=prefTimeZone %>" );
  Wiki.setPrefEditAreaHeight ( "<%=prefEditAreaHeight %>" );
  Wiki.setPrefShowQuickLinks ( "<%=prefShowQuickLinks %>" );
  Wiki.setPrefSowCalendar    ( "<%=prefShowCalendar   %>" );
//]]>
</script>
<script type="text/javascript" 
         src="<wiki:BaseURL/>templates/<wiki:TemplateDir/>/skins/<%= prefSkinName %>/skin.js"></script>

<wiki:IncludeResources type="script"/>
